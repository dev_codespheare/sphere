; install-debug-32-bit.iss
; Inno Setup 5

#pragma option -v+
#pragma verboselevel 9

#define ApplicationFilename "ais.exe"
#define ApplicationPath "..\..\bin\vc9\win32\debug\
#define ApplicationName GetStringFileInfo(AddBackslash(SourcePath) + {#ApplicationPath}ais.exe", "ProductName")
#define ApplicationVersionComplete GetFileVersion(AddBackslash(SourcePath) + {#ApplicationPath}ais.exe")
; Trim version string, return only the major, minor, revision.
#define StripVersion(str v) Copy(v, 1, RPos(".", v)-1)
#define ApplicationVersion StripVersion(ApplicationVersionComplete)


[Setup]
AppName={#ApplicationName}
AppVersion={#ApplicationVersion}
DefaultDirName={pf}\{#ApplicationName}
DefaultGroupName={#ApplicationName}
UninstallDisplayIcon={app}\{#ApplicationFilename}
;LicenseFile=COPYING
VersionInfoVersion={#ApplicationVersion}
SourceDir={#ApplicationPath}
OutputBaseFilename=debug-{#ApplicationVersion}-setup
OutputDir=..\dist\installer\debug
SetupLogging=yes

[InstallDelete]
Type: files; Name: "{app}\{#ApplicationFilename}"
Name: {group}\{#ApplicationName}.lnk; Type: files
Name: {commondesktop}\{#ApplicationName}.lnk; Type: files

[Dirs]
Name: "{app}\.negerns"; Attribs: hidden

[Files]
Source: {#ApplicationFilename}; DestDir: "{app}"; DestName: {#ApplicationFilename};
Source: "as.conf"; DestDir: "{app}";
;Source: "res\*.theme"; DestDir: "{app}";
;Source: "README"; DestDir: "{app}";
;Source: "COPYING"; DestDir: "{app}";
;Source: "release-notes.txt"; DestDir: "{app}";
;Source: "..\dist\runtime\9.0.30729.17\vcredist_x86.exe"; DestDir: "{app}"; Flags: deleteafterinstall

[Icons]
Name: "{group}\{#ApplicationName}"; Filename: "{app}\{#ApplicationFilename}"; WorkingDir: "{app}"
Name: "{group}\Uninstall {#ApplicationName}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#ApplicationName}"; Filename: "{app}\{#ApplicationFilename}"; WorkingDir: "{app}"

[Messages]
SetupAppTitle=Setup {#ApplicationName} {#ApplicationVersion}
SetupWindowTitle=%1 {#ApplicationVersion}
; *** "Welcome" wizard page
WelcomeLabel1=[name] {#ApplicationVersion} Setup Wizard
WelcomeLabel2=This setup wizard will guide you through the installation of [name/ver] on your computer.%n%nIt is recommended that you close all running instances of the application before continuing with the installation.
; *** "Select Destination Location" wizard page
SelectDirDesc=Where will [name/ver] be installed?
SelectDirLabel3=Setup will install [name/ver] into its default destination folder.
SelectDirBrowseLabel=To install to a different location, click Browse. Click Next to continue.
; *** "Ready to Install" wizard page
ReadyLabel1=Setup is now ready to install [name/ver] on your computer.
; *** Common wizard text
ClickNext=Click Next to continue.
; *** Setup common messages
ExitSetupMessage=Setup did not complete the installation of {#ApplicationName} {#ApplicationVersion}.%n%nIf you exit now, the program will not be installed. You may run Setup again at another time to complete the installation. Do you want to exit Setup?
; Bevel
BeveledLabel= {#ApplicationName} {#ApplicationVersion}

[Tasks]
Name: desktopicon; Description: "Create &desktop icon";
Name: quicklaunchicon; Description: "Create &Quick Launch icon"; Flags: unchecked

[Run]
;Filename: {app}\vcredist_x86.exe; Parameters: "/passive /Q:a /c:""msiexec /qbn /i vcredist.msi"" "; StatusMsg: Installing 2008 C/C++ Runtime...; Check: VCRedistNeedsInstall
Filename: "{app}\{#ApplicationFilename}"; Description: "Launch application"; Flags: postinstall nowait skipifsilent unchecked

[Code]
#IFDEF UNICODE
  #DEFINE AW "W"
#ELSE
  #DEFINE AW "A"
#ENDIF
type
  INSTALLSTATE = Longint;
const
  INSTALLSTATE_INVALIDARG = -2;  // An invalid parameter was passed to the function.
  INSTALLSTATE_UNKNOWN = -1;     // The product is neither advertised or installed.
  INSTALLSTATE_ADVERTISED = 1;   // The product is advertised but not installed.
  INSTALLSTATE_ABSENT = 2;       // The product is installed for a different user.
  INSTALLSTATE_DEFAULT = 5;      // The product is installed for the current user.

  VC_2005_REDIST_X86 = '{A49F249F-0C91-497F-86DF-B2585E8E76B7}';
  VC_2005_REDIST_X64 = '{6E8E85E8-CE4B-4FF5-91F7-04999C9FAE6A}';
  VC_2005_REDIST_IA64 = '{03ED71EA-F531-4927-AABD-1C31BCE8E187}';
  VC_2005_SP1_REDIST_X86 = '{7299052B-02A4-4627-81F2-1818DA5D550D}';
  VC_2005_SP1_REDIST_X64 = '{071C9B48-7C32-4621-A0AC-3F809523288F}';
  VC_2005_SP1_REDIST_IA64 = '{0F8FB34E-675E-42ED-850B-29D98C2ECE08}';
  VC_2005_SP1_ATL_SEC_UPD_REDIST_X86 = '{837B34E3-7C30-493C-8F6A-2B0F04E2912C}';
  VC_2005_SP1_ATL_SEC_UPD_REDIST_X64 = '{6CE5BAE9-D3CA-4B99-891A-1DC6C118A5FC}';
  VC_2005_SP1_ATL_SEC_UPD_REDIST_IA64 = '{85025851-A784-46D8-950D-05CB3CA43A13}';

  VC_2008_REDIST_X86 = '{FF66E9F6-83E7-3A3E-AF14-8DE9A809A6A4}';
  VC_2008_REDIST_X64 = '{350AA351-21FA-3270-8B7A-835434E766AD}';
  VC_2008_REDIST_IA64 = '{2B547B43-DB50-3139-9EBE-37D419E0F5FA}';
  VC_2008_SP1_REDIST_X86 = '{9A25302D-30C0-39D9-BD6F-21E6EC160475}';
  VC_2008_SP1_REDIST_X64 = '{8220EEFE-38CD-377E-8595-13398D740ACE}';
  VC_2008_SP1_REDIST_IA64 = '{5827ECE1-AEB0-328E-B813-6FC68622C1F9}';
  VC_2008_SP1_ATL_SEC_UPD_REDIST_X86 = '{1F1C2DFC-2D24-3E06-BCB8-725134ADF989}';
  VC_2008_SP1_ATL_SEC_UPD_REDIST_X64 = '{4B6C7001-C7D6-3710-913E-5BC23FCE91E6}';
  VC_2008_SP1_ATL_SEC_UPD_REDIST_IA64 = '{977AD349-C2A8-39DD-9273-285C08987C7B}';
  VC_2008_SP1_MFC_SEC_UPD_REDIST_X86 = '{9BE518E6-ECC6-35A9-88E4-87755C07200F}';
  VC_2008_SP1_MFC_SEC_UPD_REDIST_X64 = '{5FCE6D76-F5DC-37AB-B2B8-22AB8CEDB1D4}';
  VC_2008_SP1_MFC_SEC_UPD_REDIST_IA64 = '{515643D1-4E9E-342F-A75A-D1F16448DC04}';

  VC_2010_REDIST_X86 = '{196BB40D-1578-3D01-B289-BEFC77A11A1E}';
  VC_2010_REDIST_X64 = '{DA5E371C-6333-3D8A-93A4-6FD5B20BCC6E}';
  VC_2010_REDIST_IA64 = '{C1A35166-4301-38E9-BA67-02823AD72A1B}';
  VC_2010_SP1_REDIST_X86 = '{F0C3E5D1-1ADE-321E-8167-68EF0DE699A5}';
  VC_2010_SP1_REDIST_X64 = '{1D8E6291-B0D5-35EC-8441-6616F567A0F7}';
  VC_2010_SP1_REDIST_IA64 = '{88C73C1C-2DE5-3B01-AFB8-B46EF4AB41CD}';

function MsiQueryProductState(szProduct: string): INSTALLSTATE;
  external 'MsiQueryProductState{#AW}@msi.dll stdcall';

function VCVersionInstalled(const ProductID: string): Boolean;
begin
  Result := MsiQueryProductState(ProductID) = INSTALLSTATE_DEFAULT;
end;

function VCRedistNeedsInstall: Boolean;
begin
  // here the Result must be True when you need to install your VCRedist
  // or False when you don't need to, so now it's upon you how you build
  // this statement, the following won't install your VC redist only when
  // the Visual C++ 2010 Redist (x86) and Visual C++ 2010 SP1 Redist(x86)
  // are installed for the current user
  Result := not (VCVersionInstalled(VC_2008_REDIST_X86) and
    VCVersionInstalled(VC_2008_SP1_REDIST_X86));
end;
