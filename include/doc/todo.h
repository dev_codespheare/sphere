/*******************************************************************************
 * File:    doc/todo.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 20 Apr 2013 12:23 PM
 *
 * Comment: To do items
 ******************************************************************************/

/*!

\page page_todo_ To Do Items

This chapter describes the to do items for the Sphere System.



\tableofcontents



\section section_introduction Introduction

The to do items is a generalized list of task to ponder and implement for the
entire project.



\section section_framework Framework

To do items for the framework library.

\li Subclass wxDateTime and add a constructor for creating dates in the format
    yyyy mm dd to synchronize with ISO 8601 format.
\li Add more validators.
    \see ChoiceValidator
    \see DatePickerValidator
\li Implement InfoTip. Something like a tool tip window but shows when the
    user hit F1 or something.
\li Implement system check on application startup. I'm thinking of implementing
    this as static function called from a base class like SystemCheckBase. But
    using a static function might require a lot of refactoring of other classes.



\section section_sphere Sphere

To do items for the Sphere project.

\li Holiday Calendar which allows users to setup standard holiday calendar.
    This is used to set the default values in Work Calendar. This will be useful
    when a company has its own calendar of events. This could also be used to
    keep track of events etc.

*/ // end of documentation
