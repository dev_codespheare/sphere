/*******************************************************************************
 * File:    doc/configuration.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 23 Apr 2013 3:42 AM
 *
 * Comment: Sphere Configuration subsystem documentation
 ******************************************************************************/

/*!

\page page_configuration Sphere Configuration Subsystem

This chapter describes the Sphere Configuration Subsystem.



\tableofcontents



\section section_introduction Introduction

This section provides an overview of the configuration subsystem and introduces
the classes involved.

The following is a class diagram of the configuration subsystem.

@htmlonly<div class="diagram">@endhtmlonly
@image html configuration.png
@htmlonly</div>@endhtmlonly

\li ConfigSource
\li ConfigFile
\li ConfigDatabase
\li ConfigBase
\li Config



\section section_details Details

This section discusses the configuration subsystem in more detail.



\subsection subsection_configuration_options Configuration Options


\subsubsection subsubsection_logging Logging options

\li level - Set this to zero (0) to disable logging. Setting it to a number
            greater than zero (0) enables logging and sets the debugging level
            used by the application. For possible values to use see \ref page_logging.

\li filename - The value of logging.filename is appended with the date and time
               the application was executed. If logging.filename is empty, then
               the name of the executable is used. Format is 'Sphere-%Y%m%d-%H%M%S.log'
               (Sphere-01012011-010101.log). For formatting see \ref page_logging.

\li datetime.format - The value of datetime.format is used in the log output to
                      format the date/time. For formatting see \ref page_logging.



*/ // end of documentation
