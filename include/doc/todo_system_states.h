/*******************************************************************************
 * File:    doc/todo_system_states.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 25 Apr 2013 2:32 AM
 *
 * Comment: Sphere To Do Item System States
 ******************************************************************************/

/*!

\page page_todo_system_states Sphere System States

This chapter describes the to do items for the Sphere System States.



\tableofcontents



\section section_introduction Introduction

The System States design is a generalization of the possible states that an
application is in. It takes into consideration that the application is or
uses multi-threading. The design relies on the new feature in wxWidgets 2.9.5,
specifically, the \c wxEvtHandler::CallAfter.

The System States design allows the creation of System States as required by
the application. The System States must be registered so components of the
system can query it anywhere. Ideally, the System States could be managed
in the \c MediatorBase class.

@htmlonly<div class="diagram">@endhtmlonly
@image html todo_system_states.png
@htmlonly</div>@endhtmlonly



\section section_requirements Requirements

This section discusses the requirements of the System States design.

\li States can only move forward.
\li States can be interrupted and termintated.
\li The final state should call an event handler to do the application-specific
    processes. An example would be to popup a notification window.
\li As much as possible the event handler that is called after the final state
    must be asynchronous. It may be via the idle event or via another thread
    or the use of the \c CallAfter method.



\section section_class_diagram Class Diagram

The following class diagram provides an overview of the design.

@htmlonly<div class="diagram">@endhtmlonly
@image html todo_system_states_class_diagram.png
@htmlonly</div>@endhtmlonly



\subsection subsection_state State

\li A \c State has a value and a corresponding method to be called when the
new state is entered.



\subsection subsection_statebase StateBase

\li The \c StateBase::index determines the start value in \c states.
\li Each \c StateBase subclass increments the static counter \c index in the 
    \c StateBase constructor.
\li Each \c StateBase subclass adds a state via \c AddState. \c AddState
    increments the last value in \c states by \c StateBase::increment.
\li Each \c StateBase subclass registers an event handler that is called
    after the final state.
\li \c Start is the entry point for processing. \c Start loops through all the
    states. It takes into consideration the value of \c terminate, which if
    true, aborts processing the next state.



\subsection subsection_systemupdate System Update (StateBase Subclass Example)

\li \c System \c Update defines two states, \c Connect and \c Download. The
    \c IsNewVersionAvailable is a private method internally called by the
    \c Download method.



\note Let the system check if all \c StateBase subclasses have registered a
      final state event handler.



\section section_usage Usage

The following code fragments demonstrate how the design may be used.

\code
SystemUpdate::SystemUpdate()
{
    AddState(&SystemUpdate::Connect);
    AddState(&SystemUpdate::Download);
}

bool SystemUpdate::Connect()
{
    ...
}

void SystemUpdate::Download()
{
}

bool SystemUpdate::IsNewVersionAvailable()
{
    ...
    if (newVersion) {
        message = wxString::Format("New version has been downloaded.\nVersion %s", version);
    } else {
        message = "There is no newer version of the application.";
    }
}

\endcode

And somewhere,

\code
void Mediator::Mediator()
{
    SystemUpdate* sysupd = new SystemUpdate();
    ManagerBase::SetIt("SystemUpdate", sysupd);
}

void Mediator::OnSystemUpdateMenu(wxCommandEvent& e)
{
    SystemUpdate* sysupd = ManagerBase::GetIt("SystemUpdate");
    sysupd->Start();
}

void Mediator::OnSystemUpdateDone(wxCommandEvent& e)
{
    SystemUpdate* sysupd = ManagerBase::GetIt("SystemUpdate");
    wxstring s(sysupd->GetNotificationMessage());
    notification->Notify(s);
}
\endcode



*/ // end of documentation
