/*******************************************************************************
 * File:    doc/logging.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 23 Apr 2013 1:19 AM
 *
 * Comment: Sphere Logging documentation
 ******************************************************************************/
// page page_logging Sphere Logging documentation
// defgroup group_logging Sphere Logging Facility

/*!

\page page_logging Sphere Logging Facility

This chapter describes the Sphere Logging Facility.



\tableofcontents



\section section_introduction Introduction

This section provides an overview of the logging facility and introduces the
classes involved.

The following is a class diagram of the logging facility. The Logger class
is inherited from wxLog and CustomLogFormatter is inherited from wxLogFormatter.
The Logger class uses the CustomLogFormatter class indirectly through the wxLog
class.

@htmlonly<div class="diagram">@endhtmlonly
@image html logging.png
@htmlonly</div>@endhtmlonly



\section section_details Details

This section discusses the Sphere Logging facility in more detail.



\subsection subsection_log_format Log Output Format

The log output has been formatted to facilitate easier parsing.

The figure below is an example of a log output. Note that log information is
separated by space(s).

\code
2013 04 21 09:14:41 [2396] Message  Logging started.
|        | |      | |    | |     |  |
+--------+ +------+ +----+ +-----+  +---------------
   date      time   thread message        text
\endcode

\li \b date - The date format is \c yyyy \c mm \c dd.
\li \b time - The time format is \c hh:mm:ss.
\li \b thread - Thread ID enclosed in square brackets.
\li \b message - One of DEBUG | MESSAGE | ERROR
\li \b text - Is the log text which does not have length limit. The log text may
           span multiple lines.



\subsection subsection_log_levels Log Levels

The logging facility uses log levels to control what is sent to the log output.
The following table shows the log levels and their meaning:

| Level  |    Text     | Description                                           |
| :----: | :---------: | :---------------------------------------------------- |
| 0      |             | disable logging                                       |
| 1      | Fatal Error | program can't continue, abort immediately             |
| 2      | Error       | a serious error, user must be informed about it       |
| 3      | Warning     | user is normally informed about it but may be ignored |
| 4      | Message     | normal message (i.e. normal output of a non GUI app)  |
| 5      | ---         | not used                                              |
| 6      | Info        | informational message (a.k.a. 'Verbose')              |
| 7      | Debug       | never shown to the user, disabled in release mode     |
| 8      | ---         | not used                                              |
| 9      | ---         | not used                                              |
| 10     | User        | user defined levels start here                        |


\subsection subsection_log_config Configuration Options

Some logging facility configuration can be modified through the Sphere configuration
file. This subsection provides information about these configuration options that
can be modified to customize some of the logging facility inner workings.

\subsubsection subsubsection_output_filename Output Filename

The default output filename of the logging facility is:

\code
sphere-yyyymmdd-hhmmss.log
\endcode

\li yyyymmdd - corresponds to date (year in 4-digit format, month in numerical,
               2-digit format and the 2-digit day).
\li hhmmss - corresponds to time (2-digit hour, minutes and seconds in 24-hour
             format).

The filename format can be modified using combinations of the following
date/time format specifiers:

| Specifier |    Description                                           |
| :-------: | :------------------------------------------------------- |
| %a        | abbreviated weekday name (e.g. Fri) |
| %A        | full weekday name (e.g. Friday) |
| %b        | abbreviated month name (e.g. Oct) |
| %B        | full month name (e.g. October) |
| %c        | the standard date and time string |
| %d        | day of the month, as a number (1-31) |
| %H        | hour, 24 hour format (0-23) |
| %I        | hour, 12 hour format (1-12) |
| %j        | day of the year, as a number (1-366) |
| %m        | month as a number (1-12) |
| %M        | minute as a number (0-59) |
| %p        | locale's equivalent of AM or PM |
| %S        | second as a number (0-59) |
| %U        | week of the year, (0-53), where week 1 has the first Sunday |
| %w        | weekday as a decimal (0-6), where Sunday is 0 |
| %W        | week of the year, (0-53), where week 1 has the first Monday |
| %x        | standard date string |
| %X        | standard time string |
| %y        | year in decimal, without the century (0-99) |
| %Y        | year in decimal, with the century |
| %Z        | time zone name |
| %%        | a percent sign |

The following is an example of how to use the format specifiers:

\code
filename = 'sphere-%Y%m%d-%H%M%S.log'
\endcode



\subsubsection subsubsection_datetime_format Date/Time Format

The default date/time format of the logging facility is:

\code
%Y %m %d %H:%M:%S
\endcode

To customize the date/time format, refer to the date/time format specifiers
above.


*/ // end of documentation
