/*******************************************************************************
 * File:    doc/system_overview.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 23 Apr 2013 4:17 AM
 *
 * Comment: Sphere System Overview
 ******************************************************************************/

/*!

\page page_system_overview Sphere System Overview

This chapter discusses the Sphere System.



\tableofcontents



\section section_introduction Introduction

This section provides an overview of Sphere.

The following is a system-level diagram of Sphere.

@htmlonly<div class="diagram">@endhtmlonly
@image html system_overview.png
@htmlonly</div>@endhtmlonly



\section section_details Details

This section discusses the Sphere System in more detail.



*/ // end of documentation
