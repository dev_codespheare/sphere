/*******************************************************************************
 * File:    doc/main.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 20 Apr 2013 4:21 PM
 *
 * Comment: Main page of Sphere documentation
 ******************************************************************************/

/*!

\mainpage Sphere Documentation

\project_author

\project_date

\section manual_user User Manual

\li \subpage page_introduction
\li \subpage page_copyright
\li \subpage page_utils
\li \subpage page_samples
\li \subpage page_libs
\li \subpage page_class_cat
\li \subpage page_topics
\li \subpage page_multiplatform


\section manual_reference Reference Manual

\li \subpage page_system_overview
\li \subpage page_configuration
\li \subpage page_logging
\li \subpage page_framework
\li \subpage page_database
\li \subpage page_user_interface

\section todo To Do Items

\li \subpage page_todo_system_states


*/ // end of documentation
