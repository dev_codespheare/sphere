/*******************************************************************************
 * File:    negerns/gui/panel.h
 * Created: 21 Dec 2013 2:26 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/scrolwin.h>
#include <negerns/core/defs.h>
#include <negerns/core/declspec.h>

#include <negerns/negerns.h>

#ifndef NEGERNS_GUI_PANEL_H
#define NEGERNS_GUI_PANEL_H

namespace negerns {
namespace gui {

//! Ancestor panel class
class NEGERNS_DECL Panel : public wxScrolledWindow
{
public:
    Panel();
    Panel(wxWindow* parent,
        wxWindowID id = wxID_ANY,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxTAB_TRAVERSAL,
        const wxString &name = wxPanelNameStr);
    ~Panel();
    Panel(const Panel& pb) = delete;

    void Create(wxWindow* parent,
        wxWindowID id = wxID_ANY,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxTAB_TRAVERSAL,
        const wxString &name = wxPanelNameStr);

    //! Sets the internal wxBoxSizer orientation.
    //! The default is vertical orientation.
    virtual void SetDirection(n::Orientation o = n::Orientation::Vertical) final;

    virtual wxBoxSizer *GetSizer() final;

    //! Add sizer.
    void Add(wxSizer *sizer, const wxSizerFlags &flags);

    //! Add sizer.
    void Add(wxSizer *sizer,
        negerns::Proportion proportion = Proportion::Static,
        int flag = 0,
        int border = 0,
        wxObject *userData = NULL);

    //! Add sizer inside a static box.
    void AddInStaticBox(wxSizer *sizer,
        n::Orientation orientation = n::Orientation::Horizontal,
        negerns::Proportion proportion = Proportion::Static,
        int flag = 0,
        int border = 8);

    //! Add sizer inside a static box.
    void AddInStaticBox(wxSizer *sizer,
        const std::string &label = "",
        n::Orientation orientation = n::Orientation::Horizontal,
        negerns::Proportion proportion = Proportion::Static,
        int flag = 0,
        int border = 8);

protected:

    void OnSize(wxSizeEvent&);

private:

    n::Orientation orientation = n::Orientation::Vertical;
    wxBoxSizer *sizer = nullptr;

};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_PANEL_H
