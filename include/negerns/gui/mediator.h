/*******************************************************************************
 * File:    negerns/gui/mediator.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 10 Apr 2013 1:37 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_MEDIATOR_H
#define NEGERNS_GUI_MEDIATOR_H

#include <wx/event.h>
#include <negerns/data/datasource.h>
#include <negerns/gui/component.h>
#include <negerns/gui/framemanager.h>

namespace negerns {
namespace gui {

//! Mediator class.
class NEGERNS_DECL Mediator : public Component
{
public:
    Mediator(FrameManager* fm);
    virtual ~Mediator();

    //! @{
    //! \name IControl Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    virtual void PostCreateControl() override;

    virtual void DestroyControl() override;

    //! \copydoc IControl::ApplyConfiguration
    //! Apply configuration settings to the frame window and all panes.
    virtual void ApplyConfiguration() override;

    //! @}

    //! Log into the database.
    virtual bool LogIn(const negerns::data::DataSource &ds);

#ifdef _DEBUG
    //! Automatic log in to the database only in debug builds.
    virtual bool AutoLogIn();
#endif

    //! Log out from the database.
    virtual bool LogOut();

    virtual void ProcessToolBarClick(wxCommandEvent &);

    //! \brief Set the FrameManagerBase-derived object instance
    virtual void SetFrameManager(FrameManager *fm) final { frameManager = fm; }

    //! \brief Return the FrameManagerBase-derived object instance.
    virtual FrameManager* GetFrameManager() final { return frameManager; }

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    //! @}

    //! Subclass must override this function.
    virtual void OnLogIn(wxCommandEvent&) = 0;
    void OnLogOut(wxCommandEvent&);

protected:

    FrameManager* frameManager;
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_MEDIATOR_H
