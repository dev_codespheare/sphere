/*******************************************************************************
 * File:    negerns/gui/listview.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 5 Nov 2013 1:19 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_LISTVIEW_H
#define NEGERNS_GUI_LISTVIEW_H

#include <vector>
#include <memory>
#include <functional>
#include <wx/object.h>
#include <wx/string.h>
#include <wx/variant.h>
#include <wx/icon.h>
#include <wx/dataview.h>
#include <negerns/core/declspec.h>
#include <negerns/core/memory.h>
#include <negerns/core/defs.h>
#include <negerns/gui/component.h>
#include <negerns/data/datastore.h>

#include <negerns/poco.h>

namespace negerns {
namespace gui {

//! Convenience macro for passing the member function and class instance
//! intended to be used for \c SetDisplayRowFunction. This avoids the
//! cumbersome way of passing the arguments using the raw std::bind call.
//!
//! ~~~~~~~~~~{.cpp}
//! // foo is an instance of class Foo
//! listview->SetDisplayRowFunction(LVCFUNC(&Foo::Func, foo));
//! ~~~~~~~~~~
//!
//! \see SetDisplayRowFunction
#define LV_ROWDISPLAY_EVENT_FUNC(pmf, instance) std::bind(pmf, instance, \
    std::placeholders::_1, \
    std::placeholders::_2, \
    std::placeholders::_3)

#define LV_MOUSE_EVENT_FUNC(pmf, instance) std::bind(pmf, instance, std::placeholders::_1)

class ListViewModel;

class NEGERNS_DECL ListView : public Component
{
public:
    friend class ListViewModel;

    enum CellMode {
        Inert = wxDataViewCellMode::wxDATAVIEW_CELL_INERT,
        Activatable = wxDataViewCellMode::wxDATAVIEW_CELL_ACTIVATABLE,
        Editable = wxDataViewCellMode::wxDATAVIEW_CELL_EDITABLE
    };

    enum ColumnFlags {
        None = 0,
        Resizable = wxDataViewColumnFlags::wxDATAVIEW_COL_RESIZABLE,
        Sortable = wxDataViewColumnFlags::wxDATAVIEW_COL_SORTABLE,
        Reorderable = wxDataViewColumnFlags::wxDATAVIEW_COL_REORDERABLE,
        Hidden = wxDataViewColumnFlags::wxDATAVIEW_COL_HIDDEN
    };

    enum ColumnWidth {
        Default = wxDVC_DEFAULT_WIDTH,
        Toggle = wxDVC_TOGGLE_DEFAULT_WIDTH,
        Date = 70,
        Time = 75,
        DateTime = 140
    };

    enum class DataSource {
        Original,
        Modified,
        Inserted
    };

    typedef std::function< void(wxMouseEvent &) > MouseEventFunction;
    typedef std::function< bool(std::size_t, std::size_t, wxVariant &) > RowDisplayEventFunction;

    ListView();
    virtual ~ListView();

    //! @{
    //! \name IControl Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl();

    //! Return the list control.
    //!
    //! \see IControl::GetControl
    virtual wxWindow* GetControl() override;

    //! @}

    //! Set the data model to be used.
    //!
    //! \note Set the data model first before binding an event handler.
    //! Otherwise, it will crash the application.
    //!
    //! \see Bind
    void SetModelData(negerns::data::DataStore *, DataSource ds = DataSource::Original);

    virtual void SetBorder(bool b) final;

    virtual void SetRowHeight(int) final;

    //! Append a toggle column.
    //!
    //! The \c column argument is not the column index of the control. It is
    //! the zero-based data model column index.
    //!
    //! The specified \c width will be set as the minimum width of the column.
    wxDataViewColumn *AppendToggleColumn(unsigned int modelColumn,
        const wxString &label = wxEmptyString,
        int width = ColumnWidth::Toggle,
        negerns::Alignment align = negerns::Alignment::Center,
        CellMode mode = CellMode::Inert,
        int flags = ColumnFlags::None);

    //! Append a text column.
    //!
    //! The \c column argument is not the column index of the control. It is
    //! the zero-based data model column index.
    //!
    //! The specified \c width will be set as the minimum width of the column.
    wxDataViewColumn *AppendTextColumn(unsigned int modelColumn,
        const wxString &label,
        int width = ColumnWidth::Default,
        negerns::Alignment align = negerns::Alignment::Not,
        CellMode mode = CellMode::Inert,
        int flags = ColumnFlags::Resizable);

    //! Append an icon text column.
    //!
    //! The \c column argument is not the column index of the control. It is
    //! the zero-based data model column index.
    //!
    //! The specified \c width will be set as the minimum width of the column.
    wxDataViewColumn *AppendIconTextColumn(unsigned int modelColumn,
        const wxString &label,
        int width = ColumnWidth::Default,
        negerns::Alignment align = negerns::Alignment::Not,
        CellMode mode = CellMode::Inert,
        int flags = ColumnFlags::Resizable);

    //! Append a date column.
    //!
    //! The \c column argument is not the column index of the control. It is
    //! the zero-based data model column index.
    //!
    //! The specified \c width will be set as the minimum width of the column.
    wxDataViewColumn *AppendDateColumn(unsigned int modelColumn,
        const wxString &label,
        int width = ColumnWidth::Date,
        negerns::Alignment align = negerns::Alignment::Center,
        CellMode mode = CellMode::Activatable,
        int flags = ColumnFlags::None);

    //! Append a time column.
    //!
    //! The \c column argument is not the column index of the control. It is
    //! the zero-based data model column index.
    //!
    //! The specified \c width will be set as the minimum width of the column.
    wxDataViewColumn *AppendTimeColumn(unsigned int modelColumn,
        const wxString &label,
        int width = ColumnWidth::Time,
        negerns::Alignment align = negerns::Alignment::Center,
        CellMode mode = CellMode::Activatable,
        int flags = ColumnFlags::None);

    //! Append a progress bar column.
    //!
    //! The \c column argument is not the column index of the control. It is
    //! the zero-based data model column index.
    //!
    //! The specified \c width will be set as the minimum width of the column.
    wxDataViewColumn *AppendProgressColumn(unsigned int modelColumn,
        const wxString &label,
        int width = ColumnWidth::Default,
        negerns::Alignment align = negerns::Alignment::Center,
        CellMode mode = CellMode::Inert,
        int flags = ColumnFlags::Resizable);

    //! Append a choice column.
    //!
    //! The \c column argument is not the column index of the control. It is
    //! the zero-based data model column index.
    //!
    //! The specified \c width will be set as the minimum width of the column.
    wxDataViewColumn *AppendChoiceColumn(unsigned int modelColumn,
        const wxString &label,
        int width = ColumnWidth::Default,
        negerns::Alignment align = negerns::Alignment::Not,
        CellMode mode = CellMode::Inert,
        int flags = ColumnFlags::Resizable);

    //! Append a choice index column. The values to be displayed are taken from
    //! a list of values accessed by index.
    //!
    //! The \c column argument is not the column index of the control. It is
    //! the zero-based data model column index.
    //!
    //! The specified \c width will be set as the minimum width of the column.
    wxDataViewColumn *AppendChoiceIndexColumn(unsigned int modelColumn,
        const wxString &label,
        int width = ColumnWidth::Default,
        negerns::Alignment align = negerns::Alignment::Not,
        CellMode mode = CellMode::Inert,
        int flags = ColumnFlags::Resizable);

    //! Return the sum of all viewable column widths of the control.
    size_t GetTotalColumnWidth();


    //! Return the current wxDataViewItem object.
    wxDataViewItem GetCurrentItem() const;

    //! Return the wxDataViewItem object at the specified index.
    wxDataViewItem GetItem(std::size_t) const;

    //! Return a blank Row.
    virtual negerns::data::Row GetBlankRow() final;

    //! Return the Row of the currently selected item in the list.
    virtual negerns::data::Row GetCurrentRow() final;

    //! Return the Row of the specified wxDataViewItem object.
    virtual negerns::data::Row GetRow(const wxDataViewItem &) final;

    //! Return the row index of the currently selected item.
    std::size_t GetCurrentIndex() const;

    //! Return the row index of the specified wxDataViewItem object.
    std::size_t GetIndex(const wxDataViewItem &) const;

    void Select(std::size_t);

    virtual unsigned int GetItemCount() final;

    //! Programmatically put the specified item column into editing mode.
    //! Does nothing if the column is not editable.
    void EditItem(const wxDataViewItem &, const wxDataViewColumn *);

    //! Function to be called from the model when displaying a row.
    //! For convenience, the macro \c LVCFUNC(pmf, instance) is provided for
    //! passing the member function and class instance.
    //!
    //! The function to be bound must return a boolean value and have the
    //! following arguments:
    //!
    //! \li wxVariant &variant - result where to put the element to display
    //! \li unsigned int row - index to the data model row
    //! \li unsigned int column - index to the data model column
    //!
    //! ~~~~~~~~~~{.cpp}
    //! bool OnRowDisplay(wxVariant &variant, unsigned int row, unsigned int column);
    //! ~~~~~~~~~~
    //!
    //! The return value of the function to be bound determines whether the
    //! model should continue the default processing after the call to the
    //! function.
    //!
    //! \todo Discuss the default processing of how the model displays rows
    //! and columns in the list.
    //!
    //! \see LVCFUNC
    void SetRowDisplayEventFunction(RowDisplayEventFunction f);

    //! Set the function to be called on mouse events.
    //!
    //! The client function must have the following signature:
    //!
    //! ~~~~~~~~~~{.cpp}
    //! void(wxMouseEvent &)
    //! ~~~~~~~~~~
    //!
    //! The client function can be registered like in the following code:
    //!
    //! ~~~~~~~~~~{.cpp}
    //! list->SetMouseEventFunction(std::bind(&Client::OnMouse, this, std::placeholders::_1));
    //! ~~~~~~~~~~
    void SetMouseEventFunction(MouseEventFunction f);

    //! Bind a member function to the specified event. It will be called when
    //! the event is triggered.
    //!
    //! %Event handlers must return void and have a \c wxDataViewEvent& argument.
    //!
    //! The following events are may be bound:
    //!
    //! \li \c wxEVT_DATAVIEW_SELECTION_CHANGED - selection has changed
    //! \li \c wxEVT_DATAVIEW_ITEM_ACTIVATED - item is activated (doubleclicked)
    //! \li \c wxEVT_DATAVIEW_ITEM_CONTEXT_MENU - right click event
    //!
    //! ~~~~~~~~~~{.cpp}
    //! // The object listview is an instance of ListViewComponent.
    //! listview->Bind(wxEVT_DATAVIEW_SELECTION_CHANGED, &ListView::OnSelectionChanged, listview);
    //! listview->Bind(wxEVT_DATAVIEW_ITEM_ACTIVATED, &ListView::OnSelectionActivated, listview);
    //! listview->Bind(wxEVT_DATAVIEW_ITEM_CONTEXT_MENU, &ListView::OnContextMenu, listview);
    //! ~~~~~~~~~~
    //!
    //! \c wxEVT_DATAVIEW_ITEM_CONTEXT_MENU event is triggered even if a right
    //! click did not occur on an item. To check whether the event occured over
    //! an item, use \c wxDataViewItem::IsOk which returns true.
    //!
    //! ~~~~~~~~~~{.cpp}
    //!     wxDataViewItem item = event.GetItem();
    //!     if (item.IsOk()) {
    //!         ...
    //!     }
    //! ~~~~~~~~~~
    //!
    //! \note The data model must be set first before binding an event handler.
    //! Otherwise, the application will crash.
    //!
    //! \see SetModelData
    template <typename EventTag, typename Class, typename EventHandler>
    void Bind(const EventTag &eventType,
        void(Class::*method)(wxDataViewEvent &),
        EventHandler *handler)
    {
        wxASSERT_MSG(control, "Control must be created before calling Bind.");
        wxASSERT_MSG(model, "Model data must be set before calling Bind.");
        control->Bind(eventType, method, handler);
    }

    void SetSize(const wxSize &);
    void SetMinSize(const wxSize &);
    void SetMaxSize(const wxSize &);

    void SetSingleSelection(bool b = true);
    void SetMultipleSelection(bool b = true);

private:

    void OnMouse(wxMouseEvent &);

protected:

    wxDataViewListCtrl* control;
    wxObjectDataPtr<ListViewModel> model;

    // TODO: Clean up after creating the control.
    std::vector<wxDataViewColumn*> columns;

    //! Column index in the Rowset.
    std::vector<unsigned int> dataColumns;

private:

    bool drawBorder;

    int rowHeight;

    wxSize size;

    bool multipleSelection;

    MouseEventFunction mouseEventFunc;
    RowDisplayEventFunction rowDisplayEventFunc;
};



class NEGERNS_DECL ListViewModel : public wxDataViewVirtualListModel
{
public:
    friend class ListViewComponent;

    ListViewModel(ListView *);
    virtual ~ListViewModel();

    //! @{
    //! \name wxDataViewVirtualListModel Virtual Functions

    virtual unsigned int GetColumnCount() const override;

    //! Return the data type in the specified column.
    //! Check the column using constants from Poco/Data/MetaColumn.h
    //! and return the data type as string.
    //!
    //! The following values are from wxVariant::GetType:
    //!     "bool"
    //!     "char"
    //!     "datetime"
    //!     "double"
    //!     "list"
    //!     "long"
    //!     "longlong"
    //!     "string"
    //!     "ulonglong"
    //!     "arrstring"
    //!     "void*"
    virtual wxString GetColumnType(unsigned int col) const override;
    virtual void GetValue(wxVariant &, const wxDataViewItem &, unsigned int col) const;
    virtual void GetValueByRow(wxVariant &variant, unsigned int row, unsigned int col) const override;
    virtual bool GetAttrByRow(unsigned int row, unsigned int col, wxDataViewItemAttr &attr) const override { return true; }
    virtual bool SetValueByRow(const wxVariant &variant, unsigned int row, unsigned int col) override;

    //! @}

    //! Set the items to be use by the model.
    void Set(negerns::data::DataStore *, ListView::DataSource source);

    //! Return a blank Row.
    negerns::data::Row Get();

    //! Return the current Row.
    negerns::data::Row GetCurrent();

    //! Return the Row specified by the wxDataViewItem.
    negerns::data::Row Get(const wxDataViewItem& item) const;

    //! Return the Row specified by the wxDataViewItem.
    negerns::data::Row Get(const unsigned int index) const;

    //! Return the row index of the specified wxDataViewItem object.
    std::size_t GetIndex(const wxDataViewItem &) const;

    //! Delete all items being used by the model.
    void Clear();

private:

    //! Return a pointer to the appropriate buffer of the \c DataStore object.
    //!
    //! \li ListView::DataSource::Original returns \c original buffer.
    //! \li ListView::DataSource::Inserted returns \c inserted buffer.
    //! \li ListView::DataSource::Modified returns \c modified buffer.
    //!
    //! \see source
    //! \see ListView::DataSource
    //! \see negerns::data::DataStore
    negerns::data::IRows * GetDataSourceRows() const;

private:

    ListView *component;
    std::vector<bool> markers;
    //! Data to be displayed in the control.
    negerns::data::DataStore *items;

    ListView::DataSource source;

    wxIcon icon;
}; // class Model

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_LISTVIEW_H
