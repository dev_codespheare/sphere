/*******************************************************************************
 * File:    negerns/gui/framemanager.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 10 Apr 2013 1:38 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_FRAMEMANAGER_H
#define NEGERNS_GUI_FRAMEMANAGER_H

#include <wx/event.h>
#include <wx/frame.h>
#include <wx/xrc/xmlres.h>
#include <negerns/gui/component.h>
#include <negerns/gui/manager/pane.h>

#define BindMenu(w, m, i, s) w->Bind(wxEVT_COMMAND_MENU_SELECTED, m, i, XRCID(s))

namespace negerns {
namespace gui {

//! Base class of all frame managers. There is at most one frame
//! manager per application.
class NEGERNS_DECL FrameManager : public Component, public manager::Pane
{
public:
    FrameManager();
    virtual ~FrameManager();

    //! @{
    //! \name IControl Public Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    virtual void PostCreateControl() override;

    virtual void DestroyControl() override;

    //! \copydoc IControl::ApplyConfiguration
    //! Apply configuration settings to the frame window and all panes.
    virtual void ApplyConfiguration() override;

    //! @}

    //! @{
    //! \name Event Handlers

    //! Processing to be called before the frame is closed.
    //!
    //! Override this method for any application-specific exit procedures.
    //!
    //! \note This method is called by an exit menu event.
    //!
    //! In the following example, \c FrameManager is a subclass of
    //! \c FrameManagerBase and the \c this is a pointer to the
    //! \c FrameManager instance.
    //!
    //! ~~~~~~~~~~{.cpp}
    //! frame->Bind(wxEVT_COMMAND_MENU_SELECTED, &FrameManager::OnQuit, this, MENU_ITEM_EXIT);
    //! ~~~~~~~~~~
    virtual void OnQuit(wxCommandEvent&);

    //! Function to be called when the frame is about to be closed.
    //!
    //! Override this method for any application-specific exit procedures.
    //! 
    //! \note This method is internally called in FrameManagerBase::OnQuit().
    virtual void OnClose(wxCloseEvent&);

    //! Display the default about box.
    //!
    //! Subclasses may override this method.
    virtual void OnAbout(wxCommandEvent&);

    virtual void OnToggleToolBar(wxCommandEvent &);

    virtual void OnToggleStatusBar(wxCommandEvent &);

    virtual void OnToggleStayOnTop(wxCommandEvent &);

    //! Display frame in fullscreen.
    virtual void OnToggleFullscreen(wxCommandEvent&);

    //! @}

    //! Display the frame.
    //!
    //! The application frame is initially hidden to allow controls to be
    //! created without flicker.
    //!
    //! \return \c true if the window has been shown or \c false if nothing
    //! was done because it was already shown.
    virtual bool Show();

    //! Return a reference to the frame instance.
    wxFrame* GetFrame();

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    //! @}

    virtual void ProcessToolBarClick(wxCommandEvent &) final;

protected:

    //! Application frame.
    wxFrame* frame;
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_FRAMEMANAGER_H
