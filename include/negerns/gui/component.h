/*******************************************************************************
 * File:    negerns/gui/component.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 10 Apr 2013 1:36 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_COMPONENT_H
#define NEGERNS_GUI_COMPONENT_H

#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <memory>
#include <vector>
#include <wx/window.h>
#include <wx/utils.h>
#include <wx/bitmap.h>
#include <negerns/core/declspec.h>
#include <negerns/core/identifier.h>
#include <negerns/core/log.h>
#include <negerns/core/message.h>
#include <negerns/gui/base.h>
#include <negerns/gui/panel.h>
#include <negerns/gui/icontrol.h>
#include <negerns/gui/artprovider.h>
#include <negerns/gui/bar/toolbar.h>
#include <negerns/gui/bar/statusbar.h>
#include <negerns/data/datastore.h>

class wxWindow;

namespace negerns {
namespace gui {

class Mediator;
class Pane;

//! Ancestor class of all GUI components.
//!
//! \remarks Components that require AUI features should inherit from
//! AuiComponent.
//!
//! \todo Check if SetTransactionManager, SetEventManager can be made private
//!       and still be called from ancestor classes by qualifying it like
//!       Component::
class NEGERNS_DECL Component : public Base, public negerns::Identifier, public IControl
{
public:
    typedef std::vector<Component*> Components;

    Component();
    virtual ~Component();

    //! Set a reference to a Mediator subclass.
    //!
    //! \see Component::GetMediatorInstance
    static void SetMediatorInstance(Mediator*);

    //! Return the MediatorBase-derived instance.
    //!
    //! \see Component::SetMediatorInstance
    static Mediator* GetMediatorInstance(Mediator* m = NULL);

    //! Add Component-derived object to the component list.
    //!
    //! The Component-derived reference is only added to the component
    //! list if it is not NULL and it has a unique identifier.
    //!
    //! \see Component::FindComponent
    //! \see Component::components
    static void AddComponent(Component* cb);

    //! Find the Component-derived object in the component list with the
    //! specified identifier and return a reference to it.
    //!
    //! Returns a reference to the Component-derived object if it
    //! is found. Otherwise, returns NULL. After the call, the callee may
    //! check for a null result just to be safe specially when components
    //! are dynamically created and destroyed during the application lifetime.
    //!
    //! It is likely that calling routine needs to cast the returned value
    //! to its specific object type.
    //!
    //! \see Component::AddComponent
    //! \see Component::components
    static Component* FindComponent(const wxString& id);

    //! @{
    //! \name IControl Public Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    virtual void PostCreateControl() override;

    virtual void DestroyControl() override;

    //! \copydoc IControl::RefreshControl
    virtual void RefreshControl() override;

    //! \copydoc IControl::ClearContent
    virtual void ClearContent() override;

    //! \copydoc IControl::RefreshContent
    virtual void RefreshContent() override;

    //! \copydoc IControl::ApplyConfiguration
    virtual void ApplyConfiguration() override;

    //! \copydoc IControl::ApplyTheme
    virtual void ApplyTheme() override;

    //! \copydoc IControl::HasFocus
    virtual bool HasFocus() const override { return false; }

    //! copydoc IControl::SetFocus
    virtual bool SetFocus(const std::string &) override { return false; }

    //! @}

    //! @{
    //! \name Controls and Sizers Function

    //! Add a control.
    void Add(wxWindow *, int proportion = 0, int flag = 0, int border = 0);

    //! Add a sizer object.
    void Add(wxSizer *, int proportion = 0, int flag = 0, int border = 0);

    //! Insert a control before the specified index.
    void Insert(int index, wxWindow *, int proportion, int flag, int border = 0);

    //! Insert a sizer object before the specified index.
    void Insert(int index, wxSizer *, int proportion, int flag, int border = 0);

    //! Layout the contents of the component.
    void Layout();

    //! Fit window contents.
    //!
    //! Resizes the window (frame or dialog) to fit the contents.
    //! This makes the window non-resizeable.
    void FitContent(wxWindow *);

    //! @}

    //! @{
    //! \name ToolBar Functions

    //! Set the toolbar.
    //!
    //! If the flag is set to true (default), then the toolbar will be created
    //! when CreateControl is called. Otherwise, the client is responsible for
    //! creating it.
    virtual negerns::gui::bar::ToolBar* SetToolBar(negerns::gui::bar::ToolBar*,
        bool flag = true) final;

    //! Get a reference to the toolbar.
    virtual negerns::gui::bar::ToolBar* GetToolBar() const final;

    //! Returns true if the toolbar is set. Otherwise, returns false.
    virtual bool IsToolBarSet() const final;

    //! @}

    //! @{
    //! \name StatusBar Functions

    //! Set the status bar.
    //!
    //! If the flag is set to true (default), then the status bar will be
    //! created when CreateControl is called. Otherwise, the client is
    //! responsible for creating it.
    virtual negerns::gui::bar::StatusBar* SetStatusBar(negerns::gui::bar::StatusBar*,
        bool flag = true) final;

    //! Get a reference to the status bar.
    virtual negerns::gui::bar::StatusBar* GetStatusBar() const final;

    //! Returns true if the status bar is set. Otherwise, returns false.
    virtual bool IsStatusBarSet() const final;

    //! @}

    //! Returns true if closing is to proceeed.
    virtual bool CanClose();

    void Freeze();
    void Thaw();

    //! Set the name of the pane component.
    //!
    //! Each Component subclass should have a unique name
    //! \see Component::name
    virtual void SetName(const wxString&);

    //! Returns the name of the component.
    //!
    //! \see Component::name
    virtual wxString GetName(void) const;

    //! Set the bitmap to use by the component.
    //!
    //! \see Component::bitmap;
    virtual void SetBitmap(const wxBitmap& bmp) final;

    //! Get the bitmap used by the component.
    //!
    //! \see Component::bitmap;
    virtual wxBitmap GetBitmap() const final;

    //! Set the parent object or container.
    void SetParent(wxWindow*);

    //! Return the reference to the parent object.
    wxWindow* GetParent();
#if 0
    //! Set the panel object.
    void SetPanel(wxPanel*);
#endif
    //! Return the reference to the panel object.
    wxPanel* GetPanel();

#if 0
    //! Set the sizer object.
    void SetSizer(wxBoxSizer*);
    //! Return the reference to the sizer object.
    wxBoxSizer* GetSizer();
#endif

    void SetPane(Pane *);

    Pane* GetPane();

protected:

    //! @{
    //! \name IControl Public Virtual Functions

    virtual bool OnInitContent() override;

    //! @}

    //! Transfer data from data source to controls.
    virtual void XferDataToWindow() final;

    //! Transfer data from data source to controls on the specified window.
    virtual void XferDataToWindow(wxWindow *) final;

    //! Transfer data from controls to data source.
    virtual void XferDataFromWindow() final;

    //! Transfer data from controls on the specified window to data source.
    virtual void XferDataFromWindow(wxWindow *) final;

#ifdef _DEBUG
    static void LogEventSummary();
#endif

private:

    //! List of component object references.
    //!
    //! This list allows the application to query a registered component
    //! and used for communications between components.
    //!
    //! \see Component::AddComponent
    //! \see Component::GetComponent
    static Components components;

    //! Parent control.
    //!
    //! This is not set or initialized in this class.
    //! It is intended for subclasses to do this. ControlBase::CreateControl
    //! automatically initializes this reference.
    //!
    //! \see Component::CreateControl
    wxWindow* parent;

    //! Container control is the parent of any child controls.
    //!
    //! This is not set or initialized in this class.
    //! It is intended for subclasses to do this. Component::CreateControl
    //! automatically initializes this reference.
    //!
    //! \see Component::CreateControl
    wxPanel* panel;

    //! Sizer object.
    //!
    //! This is not set or initialized in this class.
    //! It is intended for subclasses to do this. Component::CreateControl
    //! automatically initializes this reference.
    //!
    //! \see Component::CreateControl
    wxBoxSizer* sizer;

    //! Reference to the ToolBar object.
    //!
    //! Toolbars are best created in the Component::CreateControl.
    //!
    //! \see Component::SetToolBar
    //! \see Component::GetToolBar
    //! \see Component::IsToolBarSet
    negerns::gui::bar::ToolBar* toolbar;

    //! Flag whether this class handles the creation of the toolbar or not.
    //!
    //! Default is true; let this class handle the creation of the toolbar.
    bool autoCreateToolBar;

    //! Reference to the StatusBar object.
    //!
    //! Status bars are best created in the Component::CreateControl.
    //!
    //! \see Component::SetStatusBar
    //! \see Component::GetStatusBar
    //! \see Component::IsStatusBarSet
    negerns::gui::bar::StatusBar* statusbar;

    //! Flag whether this class handles the creation of the status bar or not.
    //!
    //! Default is true; let this class handle the creation of the status bar.
    bool autoCreateStatusBar;

    //! Unique name for the pane component.
    //!
    //! Set the name of the pane component using SetName.
    //!
    //! \see Component::SetName
    //! \see Component::GetName
    wxString name;

    //! Image used to display in the pane tab control.
    //!
    //! \see Component::SetBitmap
    //! \see Component::GetBitmap
    wxBitmap bitmap;

    Pane *pane;
};

#ifdef _DEBUG
#define LOG_EVENT_SUMMARY()     negerns::gui::Component::LogEventSummary();
#else
#define LOG_EVENT_SUMMARY()     (void)0
#endif

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_COMPONENT_H
