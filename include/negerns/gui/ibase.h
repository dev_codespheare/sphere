/*******************************************************************************
 * File:    negerns/gui/ibase.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 11 Apr 2013 4:48 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_IBASE_H
#define NEGERNS_GUI_IBASE_H

#include <negerns/core/declspec.h>

namespace negerns {
namespace gui {

//! Interface base
class NEGERNS_DECL IBase
{
public:
    IBase() { }
    virtual ~IBase() { }
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_IBASE_H
