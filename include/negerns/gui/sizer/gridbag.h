/*******************************************************************************
 * File:    negerns/gui/sizer/gridbag.h
 * Created: 13 Jun 2014 6:13 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#include <vector>
#include <wx/gbsizer.h>
#include <negerns/gui/statictext.h>
#include <negerns/gui/textctrl.h>
#include <negerns/gui/spinctrl.h>
#include <negerns/gui/datepickerctrl.h>
#include <negerns/gui/choicectrl.h>

#include <negerns/core/declspec.h>

#ifndef NEGERNS_GUI_SIZER_GRIDBAG_H
#define NEGERNS_GUI_SIZER_GRIDBAG_H

namespace negerns {
namespace gui {
namespace sizer {

class NEGERNS_DECL GridBag : public wxGridBagSizer
{
public:
    GridBag(
        int vgap = 0, int hgap = 0,
        wxWindow *parent = NULL);
    ~GridBag();

    void SetParent(wxWindow *p);
private:

    wxWindow *parent;

    std::vector<wxControl *> controls;
};

} //_ namespace sizer
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_SIZER_GRIDBAG_H
