/*******************************************************************************
 * File:    negerns/gui/sizer/vstaticbox.h
 * Created: 13 Jun 2014 11:36 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_GUI_SIZER_VSTATICBOX_H
#define NEGERNS_GUI_SIZER_VSTATICBOX_H

#include <wx/sizer.h>
#include <negerns/core/string.h>

namespace negerns {
namespace gui {
namespace sizer {

class NEGERNS_DECL VStaticBox : public wxStaticBoxSizer
{
public:
    using wxBoxSizer::wxBoxSizer;

    VStaticBox(wxWindow *parent, const std::string &s = std::string());
    ~VStaticBox();

    void SetParent(wxWindow *parent);

private:

    wxWindow *parent = nullptr;
};

} //_ namespace sizer
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_SIZER_VSTATICBOX_H
