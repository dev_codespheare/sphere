/*******************************************************************************
 * File:    negerns/gui/sizer/flexgrid.h
 * Created: 20 Dec 2013 12:30 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#include <vector>
#include <wx/sizer.h>
#include <negerns/gui/statictext.h>
#include <negerns/gui/textctrl.h>
#include <negerns/gui/spinctrl.h>
#include <negerns/gui/datepickerctrl.h>
#include <negerns/gui/choicectrl.h>
#include <negerns/gui/component.h>

#include <negerns/core/declspec.h>

#ifndef NEGERNS_GUI_SIZER_FLEXGRID_H
#define NEGERNS_GUI_SIZER_FLEXGRID_H

namespace negerns {
namespace gui {
namespace sizer {

bool NEGERNS_DECL IsBitSet(int source, int flag);

class NEGERNS_DECL FlexGrid : public wxFlexGridSizer
{
public:
    FlexGrid(
        int columns,
        const wxSize &gap = wxSize(0, 0),
        wxWindow *parent = nullptr);
    ~FlexGrid();

    void SetParent(wxWindow *p);

    wxWindow* GetParent();

    //! Set the spacer size in pixels for both horizontal and vertical
    //! orientation.
    void SetSpacer(int size);

    //! Add a static spacer.
    virtual wxSizerItem* AddSpacer(int size = 0) override;

    //! Add a wxSizer object.
    void AddSizer(wxSizer *);

    //! Add a text label.
    negerns::gui::StaticText* AddStaticText(
        const std::string &label,
        int style = Alignment::Left | Alignment::CenterVertical,
        Proportion proportion = Proportion::Dynamic,
        int border = wxNO_BORDER,
        wxObject *userData = nullptr);

    //! Add a SpinCtrl with a label on its left.
    negerns::gui::SpinCtrl* AddSpin(
        const std::string &label,
        const wxSize &size,
        int labelStyle = Alignment::Left | Alignment::CenterVertical,
        int controlStyle = Alignment::Left,
        wxObject *userData = nullptr);

    //! Add a TextCtrl with a label on its left.
    negerns::gui::TextCtrl* AddText(
        const std::string &label,
        const wxSize &size,
        int labelStyle = Alignment::Left | Alignment::CenterVertical,
        int controlStyle = Alignment::Left,
        wxObject *userData = nullptr);

    //! Add a ChoiceCtrl with a label on its left.
    negerns::gui::ChoiceCtrl* AddChoice(
        const std::string &label,
        const wxSize &size,
        int labelStyle = Alignment::Left | Alignment::CenterVertical,
        int controlStyle = Alignment::Left,
        wxObject *userData = nullptr);

    //! Add a DatePickerCtrl with a label on its left.
    negerns::gui::DatePickerCtrl* AddDatePicker(
        const std::string &label,
        const wxSize &size,
        int labelStyle = Alignment::Left | Alignment::CenterVertical,
        int controlStyle = Alignment::Left,
        wxObject *userData = nullptr);

    negerns::gui::Component* AddComponent(negerns::gui::Component *,
        int controlStyle = Alignment::Left);
#if 0
    //! Add control.
    //!
    //! \code
    //! fp->Add<wxTextCtrl>(new wxTextCtrl(parent, wxID_ANY));
    //! \endcode
    template <typename Class>
    Class *Add(
        Class *window,
        int proportion = Proportion::Dynamic,
        int flag = wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL,
        int border = wxBORDER_NONE,
        wxObject *userData = NULL)
    {
        wxFlexGridSizer::Add(window, proportion, flag, border, userData);
        return window;
    }
#endif

#if 0
    //! Add a stretchable control accepting a pointer to a variable that will
    //! be used for data transfer. Optionally allows the control style to be
    //! supplied.
    //!
    //! \code
    //! fp->Add<negerns::gui::TextCtrl>(&name);
    //! \endcode
    template <typename Class, typename ExchangeType>
    Class *Add(
        ExchangeType *value,
        int style = Style::None,
        wxObject *userData = NULL)
    {
        wxASSERT_MSG(parent, "Parent is null.");
        auto control = new Class(parent, wxDefaultSize, style, value);
        int proportion = Proportion::Dynamic;
        int flag = wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxEXPAND;
        int border = wxBORDER_NONE;
        wxFlexGridSizer::Add(control, proportion, flag, border, userData);
        return control;
    }
#endif

#if 0
    //! Add a text label and stretchable control accepting a pointer to a
    //! variable that will be used for data transfer. Optionally allows
    //! the control style to be supplied.
    //!
    //! \code
    //! fp->Add<negerns::gui::TextCtrl>("Name: ", &name);
    //! \endcode
    template <typename Class, typename ExchangeType>
    Class *Add(
        const std::string &label,
        ExchangeType *value,
        int style = Style::None,
        wxObject *userData = NULL)
    {
        wxASSERT_MSG(parent, "Parent is null.");
        int flag = wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL;
        int border = wxBORDER_NONE;
        wxFlexGridSizer::Add(new StaticText(parent, label), Proportion::Static, flag, border);
        auto control = new Class(parent, wxDefaultSize, style, value);
        int proportion = Proportion::Dynamic;
        flag |= wxEXPAND;
        wxFlexGridSizer::Add(control, proportion, flag, border, userData);
        return control;
    }
#endif

#if 0
    //! Add a fixed-size control accepting a pointer to a variable that will
    //! be used for data transfer. Optionally allows the control style to be
    //! supplied.
    //!
    //! \code
    //! fp->Add<negerns::gui::TextCtrl>(&name, wxSize(200, wxDefaultCoord));
    //! \endcode
    template <typename Class, typename ExchangeType>
    Class *Add(
        ExchangeType *value,
        const wxSize &size,
        int style = Style::None,
        wxObject *userData = NULL)
    {
        wxASSERT_MSG(parent, "Parent is null.");
        auto control = new Class(parent, wxDefaultSize, style, value);
        int flag = wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL;
        int proportion = Proportion::Static;
        int border = wxBORDER_NONE;
        wxFlexGridSizer::Add(control, proportion, flag, border, userData);
        return control;
    }
#endif
#if 0
    //! Add a fixed-sized control. It optionally allows the control style to
    //! be supplied.
    //!
    //! A validator may be set later on.
    //!
    //! \code
    //! fp->Add<negerns::gui::TextCtrl>(wxSize(200, wxDefaultCoord));
    //! \endcode
    template <typename Class>
    Class* Add(
        const wxSize &size = wxDefaultSize,
        int controlStyle = Alignment::Left ,
        wxObject *userData = nullptr)
    {
        wxASSERT_MSG(parent, "Parent is null.");
        int flag = controlStyle;
        int proportion = Proportion::Static;
        int border = wxBORDER_NONE;
        auto *control = new Class(parent, size, Style::None);
        wxFlexGridSizer::Add(control, proportion, flag, border, userData);
        //controls.push_back(control);
        return control;
    }

    //! Add a fixed-sized control with a label on its left. It optionally
    //! allows the control style to be supplied.
    //!
    //! A validator may be set later on.
    //!
    //! \code
    //! fp->Add<negerns::gui::TextCtrl>("Name:", &name, wxSize(200, wxDefaultCoord));
    //! \endcode
    template <typename Class>
    Class* Add(
        const std::string &label,
        const wxSize &size,
        int labelStyle = Alignment::Left,
        int controlStyle = Alignment::Left,
        wxObject *userData = nullptr)
    {
        wxASSERT_MSG(parent, "Parent is null.");
        int proportion = Proportion::Static;
        int border = wxBORDER_NONE;
        auto *text = new negerns::gui::StaticText(parent, label);
        auto *control = new Class(parent, size, Style::None);
        wxFlexGridSizer::Add(text, proportion, labelStyle, border);
        wxFlexGridSizer::Add(control, proportion, controlStyle, border, userData);
        //controls.push_back(control);
        return control;
    }
#endif
#if 0
    //! Add a fixed-sized control with a label on its left. It accepts a
    //! pointer to a variable which is used for storing the value of the
    //! control. It optionally allows the control style to be supplied.
    //!
    //! \code
    //! fp->Add<negerns::gui::TextCtrl>("Name:", &name, wxSize(200, wxDefaultCoord));
    //! \endcode
    template <typename Class, typename ExchangeType>
    Class *Add(
        const std::string &label,
        ExchangeType *value,
        const wxSize &size,
        int style = Style::None,
        wxObject *userData = NULL)
    {
        wxASSERT_MSG(parent, "Parent is null.");
        int flag = wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL;
        int proportion = Proportion::Static;
        int border = wxBORDER_NONE;
        wxFlexGridSizer::Add(new negerns::gui::StaticText(parent, label), proportion, flag, border);
        auto *control = new Class(parent, size, style, value);
        wxFlexGridSizer::Add(control, proportion, flag, border, userData);
        //controls.push_back(control);
        return control;
    }
#endif
#if 0
    void Clear();
#endif

private:

    wxControl* AddControl(
        const std::string &label,
        int labelStyle = Alignment::Left,
        int controlStyle = Alignment::Left,
        wxObject *userData = nullptr,
        wxControl *control = nullptr);

private:

    wxWindow *parent = nullptr;

    int spacer = 0;

    std::vector<wxControl *> controls;
};

} //_ namespace sizer
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_SIZER_FLEXGRID_H
