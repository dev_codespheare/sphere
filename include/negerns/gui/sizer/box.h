/*******************************************************************************
 * File:    negerns/gui/sizer/box.h
 * Created: 22 Dec 2013 2:02 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_GUI_SIZER_BOX_H
#define NEGERNS_GUI_SIZER_BOX_H

#include <wx/sizer.h>
#include <negerns/core/defs.h>
#include <negerns/gui/statictext.h>
#include <negerns/gui/textctrl.h>
#include <negerns/gui/datepickerctrl.h>
#include <negerns/gui/choicectrl.h>
#include <negerns/core/declspec.h>

#include <negerns/negerns.h>

namespace negerns {
namespace gui {
namespace sizer {

class NEGERNS_DECL Box : public wxBoxSizer
{
public:
    using wxBoxSizer::wxBoxSizer;

    Box(wxWindow *parent, n::Orientation o = n::Orientation::Horizontal);
    ~Box();

    void SetParent(wxWindow *parent);

#if 0
    //! Add a text label.
    negerns::gui::StaticText *AddStaticText(
        const std::string &label,
        int style = wxALIGN_LEFT,
        int proportion = Proportion::Dynamic,
        int flag = wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL,
        int border = wxNO_BORDER,
        wxObject *userData = NULL);

    //! Add a stretchable control accepting a pointer to a variable that will
    //! be used for data transfer.
    //!
    //! \code
    //! fp->Add<negerns::gui::TextCtrl>(&name);
    //! \endcode
    template <typename Class, typename ExchangeType>
    Class *Add(
        ExchangeType *value,
        int style = Style::None,
        wxObject *userData = NULL)
    {
        auto *control = new Class(parent, wxDefaultSize, style, value);
        int proportion = Proportion::Dynamic;
        int flag = wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL,
        int border = wxBORDER_NONE,
        wxBoxSizer::Add(control, proportion, flag, border, userData);
        return control;
    }

    //! Add a text label and stretchable control accepting a pointer to a
    //! variable that will be used for data transfer.
    //!
    //! \code
    //! fp->Add<negerns::gui::TextCtrl>("Name: ", &name);
    //! \endcode
    template <typename Class, typename ExchangeType>
    Class *Add(
        const std::string &label,
        ExchangeType *value,
        int style = Style::None,
        wxObject *userData = NULL)
    {
        int flag = wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL;
        int border = wxBORDER_NONE;
        fgSizer->Add(new StaticText(this, label), Proportion::Static, flag, border);
        auto *control = new Class(parent, wxDefaultSize, style, value);
        int proportion = Proportion::Dynamic;
        flag |= wxEXPAND;
        wxBoxSizer::Add(control, proportion, flag, border, userData);
        return control;
    }

    //! Add a fixed-size control accepting a pointer to a variable that will
    //! be used for data transfer. Optionally allows the control style to be
    //! supplied.
    //!
    //! \code
    //! fp->Add<negerns::gui::TextCtrl>(&name, wxSize(200, wxDefaultCoord));
    //! \endcode
    template <typename Class, typename ExchangeType>
    Class *Add(
        ExchangeType *value,
        const wxSize size,
        int style = Style::None,
        wxObject *userData = NULL)
    {
        auto *control = new Class(parent, size, style, value);
        int flag = wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL;
        int proportion = Proportion::Static;
        int border = wxBORDER_NONE;
        wxBoxSizer::Add(control, proportion, flag, border, userData);
        return control;
    }

    //! Add a text label and fixed-size control accepting a pointer to a
    //! variable that will be used for data transfer. Optionally allows
    //! the control style to be supplied.
    //!
    //! \code
    //! fp->Add<negerns::gui::TextCtrl>("Name:", &name, wxSize(200, wxDefaultCoord));
    //! \endcode
    template <typename Class, typename ExchangeType>
    Class *Add(
        const std::string &label,
        ExchangeType *value,
        const wxSize size,
        int style = Style::None,
        wxObject *userData = NULL)
    {
        int flag = wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL;
        int proportion = Proportion::Static;
        int border = wxBORDER_NONE;
        wxBoxSizer::Add(new StaticText(parent, label), proportion, flag, border);
        auto *control = new Class(parent, size, style, value);
        wxBoxSizer::Add(control, proportion, flag, border, userData);
        return control;
    }
#endif

private:

    wxWindow *parent = nullptr;
};

} //_ namespace sizer
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_SIZER_BOX_H
