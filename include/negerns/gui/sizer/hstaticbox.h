/*******************************************************************************
 * File:    negerns/gui/sizer/hstaticbox.h
 * Created: 13 Jun 2014 11:43 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_GUI_SIZER_HSTATICBOX_H
#define NEGERNS_GUI_SIZER_HSTATICBOX_H

#include <wx/sizer.h>
#include <negerns/core/string.h>

namespace negerns {
namespace gui {
namespace sizer {

class NEGERNS_DECL HStaticBox : public wxStaticBoxSizer
{
public:
    using wxBoxSizer::wxBoxSizer;

    HStaticBox(wxWindow *parent, const std::string &s = std::string());
    ~HStaticBox();

    void SetParent(wxWindow *parent);

private:

    wxWindow *parent = nullptr;
};

} //_ namespace sizer
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_SIZER_HSTATICBOX_H
