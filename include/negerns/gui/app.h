/*******************************************************************************
 * File:    negerns/gui/app.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 16 Feb 2013 6:21 AM
 *
 * Comment: Application
 ******************************************************************************/

#ifndef NEGERNS_GUI_APP_H
#define NEGERNS_GUI_APP_H

#include <string>
#include <map>
#include <wx/app.h>
#include <wx/apptrait.h>
#include <negerns/core/declspec.h>
#include <negerns/gui/framemanager.h>
#include <negerns/gui/aboutinfo.h>
#include <negerns/poco.h>

#if !defined wxUSE_ON_FATAL_EXCEPTION || wxUSE_ON_FATAL_EXCEPTION == 0
#pragma message("INFO: Fatal exception handling is not being caught.\n"
    "The operating system will handle all fatal exceptions.\n"
    "This may usually mean just terminating the application.\n")
#endif

class wxCmdLineParser;

namespace negerns {
namespace gui {

//! Application base class.
class NEGERNS_DECL Application : public wxApp
{
public:
    Application();

    //! @{
    //! \name Application Events

    //! Automatically called on application start.
    virtual bool OnInit(void) override;

    //! Post initialization processing. Automatically called from OnInit after
    //! OnInit has finished processing. Returns true if successful. If the
    //! function returns false, the application will terminate, calling OnExit.
    //!
    //! If subclasses needs to perform any post initialization processing then
    //! subclasses should override this function and must explicitly call the
    //! parent class \c OnPostInit function from within the OnPostInit override
    //! function first before processing any subclass OnPostInit processing.
    //!
    //! ~~~~~~~~~~{.cpp}
    //! if (negerns::gui::Application::OnPostInit()) {
    //!     // subclass OnPostInit processing
    //! }
    //! ~~~~~~~~~~
    virtual bool OnPostInit();

    //! Called before the application terminates. wxWidgets calls this
    //! method after all controls were destroyed but before the wxWidgets
    //! cleanup code.
    //!
    //! \see wxAppConsole::OnExit
    virtual int OnExit(void) override;

    //! \copydoc wxApp::OnInitCmdLine
    virtual void OnInitCmdLine(wxCmdLineParser&) override;
    virtual bool OnCmdLineParsed(wxCmdLineParser&) override;
    virtual bool OnCmdLineHelp(wxCmdLineParser&) override;
    virtual bool OnCmdLineError(wxCmdLineParser&) override;

    //! Internally called when the application encountered an assertion.
    //!
    //! The assertion is automatically logged if the logging framework
    //! has been initialized.
    virtual void OnAssertFailure(const wxChar* file,
        int line,
        const wxChar *func,
        const wxChar *cond,
        const wxChar *msg) override;

    //! This method will be called whenever a fatal exception occurs (also
    //! (known as general protection faults under Windows or segmentation
    //! violations in Unix and Unix-like operating systems).
    virtual void OnFatalException() override;

    //! @}

    //! Display the application information to the user.
    //!
    //! The Application subclass should implement the specific logic on how
    //! to achieve this.
    //!
    //! \todo Implement a generic about dialog.
    virtual void OnAbout(wxCommandEvent&) { }

    virtual void SetFrameManager(FrameManager *);
    virtual FrameManager* GetFrameManager() const;

    void SetAboutInfo(const AboutInfo& ai) { aboutInfo = ai; }
    AboutInfo GetAboutInfo() const { return aboutInfo; }

    void SetConfigPath(const std::string& p) { configPath = p; }
    std::string GetConfigPath() const { return configPath; }

protected:

    //! Create custom traits for the application.
    virtual wxAppTraits* CreateTraits() override;

    //! Initialize the configuration facility.
    //!
    //! The framework does not implement configuration initialization since
    //! it does not know any application-specifics. Application subclass(es)
    //! should implement application-specific configuration initialization.
    //! Subclasses must call the base class method after the initialization
    //! of application-specific configuration. This is necessary to make the
    //! configuration available to the rest of the system.
    //!
    //! \see Component::SetConfigurationInstance
    //! \see Component::GetConfigurationInstance
    virtual void InitConfiguration();

    //! Initialize the database in subclass(es).
    //!
    //! Subclasses must override this method to initialize application-
    //! specific database subsystem.
    virtual void InitDatabase() { }

protected:

    AboutInfo aboutInfo;

private:

    enum Priority {
        Configuration = 1,
        Trace,
    };

    static const char* configFile;
#ifdef _DEBUG
    static const char* traceFile;
#endif

    FrameManager *frameManager = nullptr;

    //! Relative path for configuration files.
    //!
    //! The application uses the path to search for configuration files.
    //! If the path is empty then configuration files are searched in the
    //! directory of the executable path.
    //!
    //! The configuration path is initialized to an empty path.
    std::string configPath;
};



inline
FrameManager* Application::GetFrameManager() const
{
    return frameManager;
}



// ----------------------------------------------------------------------------------------



class NEGERNS_DECL ApplicationTraits : public wxGUIAppTraits
{
public:
    virtual bool ShowAssertDialog(const wxString &msg) override;
};


} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_APP_H
