/*******************************************************************************
 * File:    negerns/gui/artprovider.h
 * Created: 20 Nov 2013 7:20 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_GUI_ARTPROVIDER_H
#define	NEGERNS_GUI_ARTPROVIDER_H

#include <wx/artprov.h>
#include <wx/bitmap.h>
#include <wx/icon.h>
#include <wx/xrc/xmlres.h>
#include <negerns/core/declspec.h>
#include <negerns/gui/shellfileinfo.h>

#define MAKE_ART_ID(id) (#id)

#define ART_ICON_NEGERNS_NONE               MAKE_ART_ID(ART_ICON_NEGERNS_NONE)
#define ART_ICON_NEGERNS                    MAKE_ART_ID(ART_ICON_NEGERNS)

#define ART_ICON_NEGERNS_HOME               MAKE_ART_ID(ART_ICON_NEGERNS_HOME)

#define ART_ICON_NEGERNS_NEW                MAKE_ART_ID(ART_ICON_NEGERNS_NEW)
#define ART_ICON_NEGERNS_FILE_CLOSE         MAKE_ART_ID(ART_ICON_NEGERNS_FILE_CLOSE)
#define ART_ICON_NEGERNS_FILE_CLOSE_ALL     MAKE_ART_ID(ART_ICON_NEGERNS_FILE_CLOSE_ALL)
#define ART_ICON_NEGERNS_EDIT               MAKE_ART_ID(ART_ICON_NEGERNS_EDIT)
#define ART_ICON_NEGERNS_UNDO               MAKE_ART_ID(ART_ICON_NEGERNS_UNDO)
#define ART_ICON_NEGERNS_REDO               MAKE_ART_ID(ART_ICON_NEGERNS_REDO)
#define ART_ICON_NEGERNS_SAVE               MAKE_ART_ID(ART_ICON_NEGERNS_SAVE)
#define ART_ICON_NEGERNS_REFRESH            MAKE_ART_ID(ART_ICON_NEGERNS_REFRESH)
#define ART_ICON_NEGERNS_CLEAR              MAKE_ART_ID(ART_ICON_NEGERNS_CLEAR)
#define ART_ICON_NEGERNS_EXPAND             MAKE_ART_ID(ART_ICON_NEGERNS_EXPAND)
#define ART_ICON_NEGERNS_COLLAPSE           MAKE_ART_ID(ART_ICON_NEGERNS_COLLAPSE)

#if 0
#define ART_ICON_NEGERNS_FOLDER     wxART_MAKE_ART_ID(ART_ICON_NEGERNS_FOLDER)
#endif

namespace negerns {
namespace gui {

//! Centralized art provider for look and feel customization.
class NEGERNS_DECL ArtProvider : public wxArtProvider
{
public:
    ArtProvider();
    ~ArtProvider();
protected:
    virtual wxBitmap CreateBitmap(const wxArtID& id, const wxArtClient& client, const wxSize& size);
private:
#ifdef __WXMSW__
    negerns::gui::ShellFileInfo fileInfo;
#endif
    wxXmlResource* resource;
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_ARTPROVIDER_H
