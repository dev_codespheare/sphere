/*******************************************************************************
 * File:    negerns/gui/timepickerctrl.h
 * Created: 2 Jun 2014 4:57 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#include <wx/timectrl.h>
#include <wx/datectrl.h>    // For wxDatePickerCtrl styles
#include <wx/datetime.h>
#include <negerns/core/defs.h>
#include <negerns/core/variant.h>
#include <negerns/gui/validator/dataxfer.h>
#include <negerns/core/declspec.h>

#include <negerns/poco.h>

#ifndef NEGERNS_GUI_TIMEPICKERCTRL_H
#define NEGERNS_GUI_TIMEPICKERCTRL_H

namespace negerns {
namespace gui {

class NEGERNS_DECL TimePickerCtrl : public wxTimePickerCtrl
{
public:
    TimePickerCtrl();

    TimePickerCtrl(wxWindow *parent,
        wxWindowID id = wxID_ANY,
        const wxDateTime &value = wxDefaultDateTime,
        const wxPoint &pos = wxDefaultPosition,
        const wxSize &size = wxDefaultSize,
        long style = wxDP_DEFAULT | wxDP_SHOWCENTURY,
        const wxValidator &validator = wxDefaultValidator,
        const wxString &name = "timectrl");

    TimePickerCtrl(wxWindow *parent,
        const wxSize &size = wxDefaultSize,
        long style = Style::None);

    virtual ~TimePickerCtrl();

    void SetValidator(Var *val, long style = wxFILTER_NONE);

    void SetValidator(Var &val, long style = wxFILTER_NONE);

    void Create(wxWindow *parent,
        wxWindowID id = wxID_ANY,
        const wxDateTime &value = wxDefaultDateTime,
        const wxPoint &pos = wxDefaultPosition,
        const wxSize &size = wxDefaultSize,
        long style = wxDP_DEFAULT | wxDP_SHOWCENTURY,
        const wxValidator &validator = wxDefaultValidator,
        const wxString &name = "timectrl");
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_TIMEPICKERCTRL_H
