/*******************************************************************************
 * File:    negerns/gui/datepickerctrl.h
 * Created: 20 Dec 2013 12:31 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#include <wx/datectrl.h>
#include <wx/datetime.h>
#include <negerns/core/defs.h>
#include <negerns/core/var.h>
#include <negerns/gui/validator/dataxfer.h>
#include <negerns/core/declspec.h>
#include <negerns/poco.h>

#ifndef NEGERNS_GUI_DATEPICKERCTRL_H
#define NEGERNS_GUI_DATEPICKERCTRL_H

namespace negerns {
namespace gui {

class NEGERNS_DECL DatePickerCtrl : public wxDatePickerCtrl
{
public:
    DatePickerCtrl();

    DatePickerCtrl(wxWindow *parent,
        wxWindowID id = wxID_ANY,
        const wxDateTime &value = wxDefaultDateTime,
        const wxPoint pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxDP_DEFAULT,
        const wxValidator& validator = wxDefaultValidator,
        const wxString& name = wxDatePickerCtrlNameStr);

#if 0
    DatePickerCtrl(wxWindow *parent,
        const wxSize& size = wxDefaultSize,
        long style = Style::None,
        wxDateTime *val = NULL);
#endif

    DatePickerCtrl(wxWindow *parent,
        const wxSize& size = wxDefaultSize,
        long style = Style::None);

    virtual ~DatePickerCtrl();

    void SetValidator(negerns::Var *val, long style = wxFILTER_NONE);

    void SetValidator(negerns::Var &val, long style = wxFILTER_NONE);

    void Create(wxWindow *parent,
        wxWindowID id = wxID_ANY,
        const wxDateTime &value = wxDefaultDateTime,
        const wxPoint pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style =wxDP_DEFAULT,
        const wxValidator& validator = wxDefaultValidator,
        const wxString& name = wxDatePickerCtrlNameStr);

#if 0
    //! Create without id, position and choices
    void Create(wxWindow *parent,
        const wxSize& size = wxDefaultSize,
        long style = Style::None,
        wxDateTime *val = NULL);
#endif
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_DATEPICKERCTRL_H
