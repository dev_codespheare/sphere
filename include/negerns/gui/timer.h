/*******************************************************************************
 * File:    negerns/gui/timer.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 21 May 2013 1:07 AM
 *
 * Comment: 
 ******************************************************************************/

#ifdef _DEBUG

#include <wx/stopwatch.h>
#include <negerns/core/log.h>


#define DeclareTimer(n) wxStopWatch stopwatch##n
#define StartTimer(n) stopwatch##n.Start()
#define PauseTimer(n) stopwatch##n.Pause()
#define ReportTimer(n, label) negerns::Log::Debug("%s: %ldms.", label, stopwatch##n.Time())

#else

#define DeclareTimer(n) 0
#define StartTimer(n) 0
#define PauseTimer(n) 0
#define ReportTimer(n, label) 0

#endif
