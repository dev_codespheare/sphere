/*******************************************************************************
 * File:    negerns/gui/auicomponent.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 28 Apr 2013 8:44 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_AUICOMPONENT_H
#define NEGERNS_GUI_AUICOMPONENT_H

#include <wx/window.h>
#include <negerns/core/declspec.h>

class wxAuiManager;

namespace negerns {
namespace gui {

//! Advance User Interface (AUI) component base class.
//!
//! Components that require AUI features is recommended to inherit this
//! class.
//!
//! \internal
//! This class defines wxAuiManager as a member and automatically
//! creates the wxAuiManager instance and calls wxAuiManager::UnInit in the
//! class destructor.
//!
//! Inheriting this class is a better choice than doing the same things in
//! the component as it enhances encapsulation of common behavior and helps
//! in maintenance.
//! \endinternal
class NEGERNS_DECL AuiComponent
{
public:
    AuiComponent();
    
    //! Uninitialize the AUI manager.
    //!
    //! \internal
    //! Uninitialize the wxAuiManager instance. The wxAuiManager must
    //! be uninitialize before a managed window is destroyed.
    //! \endinternal
    virtual ~AuiComponent();

    //! Set the window object to be managed.
    void SetManagedWindow(wxWindow*);

    //! Uninitialize the AUI manager.
    //!
    //! This must be called before the application exits.
    void UnInit();

    //! Return the AUI Manager
    wxAuiManager* GetAuiManager();
protected:
    //! \internal wxAuiManager for child controls.
    wxAuiManager* auiManager;
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_AUICOMPONENT_H
