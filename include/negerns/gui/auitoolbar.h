/*******************************************************************************
 * File:    negerns/gui/bar/auitoolbar.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: August 18, 2011 3:12 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_GUI_BAR_AUITOOLBAR_H
#define NEGERNS_GUI_BAR_AUITOOLBAR_H

// Compiler complains about wxBitmap undefined class in wxAuiToolBar.
// Defining wxBitmap here before wxAuiToolBar keeps the compiler quiet.
#include <wx/bitmap.h>
#include <wx/aui/auibar.h>
#include <negerns/core/patch.h>
#include <negerns/core/declspec.h>

namespace negerns {
namespace gui {

// The following enumerations are defined in auibar.cpp. The other 
// menu or toolbar enumerations are defined in defs.cpp. The following
// are defined here until they are defined in some definition file.
enum
{
    wxITEM_CONTROL = wxITEM_MAX,
    wxITEM_LABEL,
    wxITEM_SPACER
};

//! \brief Custom AUI Toolbar
//!
//! It provides updating of a toolbar item.
class NEGERNS_DECL AuiToolBar : public wxAuiToolBar
{
public:
    AuiToolBar();
    AuiToolBar(wxWindow* parent,
        wxWindowID id = wxID_ANY,
        const wxPoint& position = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxAUI_TB_DEFAULT_STYLE);
    virtual ~AuiToolBar() { }

    //! Create the control when using two-step creation.
    //!
    //! \note The default style is set to -1 contrary to the wxWidgets default
    //! value of wxAUI_TB_DEFAULT_STYLE. This is to address a bug in the
    //! wxWidgets code which overrides the style which was previously set
    //! using SetWindowStyleFlag. There is no way to determine if the user
    //! intentionally wants to override the style flag because the default
    //! uses wxAUI_TB_DEFAULT_STYLE. Making a new default style value makes
    //! checking of the intent of the user to be concrete.
    //!
    //! Bug: wxAUI_TB_PLAIN_BACKGROUND not honored in two-step creation.
    //!      wxWidget Ticket 15751
    //!      So for now we set the style default to -1.
    void Create(wxWindow* parent,
        wxWindowID id = wxID_ANY,
        const wxPoint& position = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
#ifdef PATCH_15751
        long style = -1);
#else
        long style = wxAUI_TB_DEFAULT_STYLE);
#endif

    //! Update a toolbar item specified by a toolbar item identifier.
    void UpdateTool(int toolId);

    //! Update a toolbar item specified by its index.
    void UpdateToolByIndex(int idx);

    bool Realize();
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_BAR_AUITOOLBAR_H
