/*******************************************************************************
* File:    negerns/gui/auitabart.h
* Author:  rmaicle (rmaicle@gmail.com)
* Created: 06 Dec 2013 3:57 AM
*
* Comment:
******************************************************************************/

#ifndef NEGERNS_GUI_AUITABART_H
#define NEGERNS_GUI_AUITABART_H

#include <wx/aui/auibook.h>
#include <negerns/core/declspec.h>

namespace negerns {
namespace gui {

class NEGERNS_DECL AuiTabArt : public wxAuiGenericTabArt
{
public:
    AuiTabArt();
    virtual ~AuiTabArt();

    virtual wxAuiTabArt* Clone() override;

    virtual void DrawBorder(wxDC& dc, wxWindow* wnd, const wxRect& rect) override;

    virtual void DrawBackground(wxDC& dc,
        wxWindow* WXUNUSED(wnd),
        const wxRect& rect) override;

    virtual void AuiTabArt::DrawTab(wxDC& dc,
        wxWindow* wnd,
        const wxAuiNotebookPage& page,
        const wxRect& in_rect,
        int close_button_state,
        wxRect* out_tab_rect,
        wxRect* out_button_rect,
        int* x_extent) override;

    void SetBorder(bool b);

    void SetLowerBorderOnly(bool b)
    {
        drawLowerBorderOnly = b;
    }

private:

    bool drawBorder;

    bool drawLowerBorderOnly;
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_AUITABART_H
