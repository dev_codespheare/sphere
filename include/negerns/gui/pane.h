/*******************************************************************************
 * File:    negerns/gui/pane.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 10 Apr 2013 1:37 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_PANE_H
#define NEGERNS_GUI_PANE_H

#include <wx/vector.h>
#include <wx/hashmap.h>
#include <wx/bitmap.h>
#include <negerns/core/declspec.h>
#include <negerns/gui/auicomponent.h>
#include <negerns/gui/manager/auinotebook.h>

namespace negerns {
namespace gui {

//! Base class of all panes.
class NEGERNS_DECL Pane : public AuiComponent, public manager::AuiNotebook
{
public:
    Pane();
    virtual ~Pane();

    //! @{
    //! \name IControl Public Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    virtual void PostCreateControl() override;

    virtual void DestroyControl() override;

    //! \copydoc IControl::ApplyConfiguration
    virtual void ApplyConfiguration() override;

    //! @}

    //! @{
    //! \name Visibility Functions

    //! \brief Toggle visibility
    //! \return New display status
    virtual bool ToggleDisplay(void);
    //! Return whether the pane is visible or not.
    bool IsVisible(void);
    //! Show
    void Show(void);
    //! Hide
    void Hide(void);
    //! @}

    //! @{
    //! \name Caption Functions

    //! Show/hide pane caption
    void TogglePaneCaption(void);
    //! Show pane caption
    void ShowPaneCaption(bool status = true);
    //! Hide pane caption
    void HidePaneCaption(void);
    //! \brief Returns true if the pane caption is shown or visible. Otherwise,
    //! returns false.
    bool IsPaneCaptionVisible();

    //! Set the name of the pane component.
    //!
    //! \see PaneBase::paneInfo
    virtual void SetName(const wxString&) override;

    //! Returns the name of the component.
    //!
    //! \see PaneBase::Info
    virtual wxString GetName(void) const override;

    //! @}

    wxAuiPaneInfo* GetPaneInfo() { return paneInfo; }

    //! Return the notebook control.
    //!
    //! \see PaneBase::GetControl
    virtual wxWindow* GetControl();

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    //! @}

protected:

    //! Pane settings
    wxAuiPaneInfo* paneInfo;
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_PANE_H
