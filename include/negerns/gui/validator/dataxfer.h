/*******************************************************************************
 * File:    negerns/gui/validator/dataxfer.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 10 Jan 2014 3:41 PM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_VALIDATOR_DATAXFER_H
#define NEGERNS_GUI_VALIDATOR_DATAXFER_H

#include <string>
#include <wx/validate.h>
// For style definitions only
#include <wx/valtext.h>
#include <negerns/core/var.h>
#include <negerns/core/declspec.h>

namespace negerns {
namespace gui {

#if wxUSE_VALIDATORS && (wxUSE_TEXTCTRL || wxUSE_COMBOBOX || wxUSE_DATEPICKCTRL)

// NOTE:
//
// BrianHV suggested changing the macro GetWindowCtrl(T)
// wxDynamicCast(m_validatorWindow, T) ? (T*) m_validatorWindow : nullptr
#define GetWindowCtrl(T) (T*)(wxDynamicCast(m_validatorWindow, T))

//! \c DataXfer is a subclass of \c wxValidator. Its main purpose is to transfer
//! data to and from a control. It does not do any input validation as other
//! \c wxValidator subclasses.
//!
//! It accepts negerns::Variant which holds the data to and from a control.
//! It is primarily intended to interface with a \c DataStore row class to
//! display data retrieved from a database and accept user input that can be
//! sent back to the database.
//!
//! Controls that can be used:
//!
//! \li wxTextCtrl
//! \li wxComboCtrl
//! \li wxComboBox
//! \li wxDatePickerCtrl
//! \li wxTimePickerCtrl
//!
//! Code from wxWidgets wxTextValidator
class NEGERNS_DECL DataXfer : public wxValidator
{
public:
    enum class Control { CheckBox, Text, Numeric, Money, Date, Time, Choice };

    DataXfer(negerns::Var *var, Control ctrl, long style = wxFILTER_NONE);
    DataXfer(const DataXfer& val);
    virtual ~DataXfer() { }

    //! Make a clone of this validator (or return NULL).
    //!
    //! Currently necessary if you're passing a reference to a validator.
    //! Another possibility is to always pass a pointer to a new validator
    //! (so the calling code can use a copy constructor of the relevant class).
    virtual wxObject *Clone() const { return new DataXfer(*this); }

    //! Called internally when the value in the window must be validated.
    //!
    //! This function can pop up an error message but we do not implement it
    //! because this is just a data transfer mechanism.
    virtual bool Validate(wxWindow *parent);

    //! Called to transfer data to the window.
    virtual bool TransferToWindow();

    //! Called to transfer data from the window.
    virtual bool TransferFromWindow();

    bool HasFlag(wxTextValidatorStyle style) const {
        return (m_validatorStyle & style) != 0;
    }

protected:

    //! Returns the error message if the content of 'val' is invalid.
    virtual wxString IsValid(const wxString& val) const;

private:

    DataXfer& operator=(const DataXfer&) { return *this; }

protected:

    Control control;
    negerns::Var *variant;

    long m_validatorStyle;
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ wxUSE_VALIDATORS && (wxUSE_TEXTCTRL || wxUSE_COMBOBOX)

#endif //_ NEGERNS_GUI_VALIDATOR_DATAXFER_H
