/*******************************************************************************
* File:    negerns/gui/auidockart.h
* Author:  rmaicle (rmaicle@gmail.com)
* Created: 16 Apr 2014 8:07 AM
*
* Comment:
******************************************************************************/

#ifndef NEGERNS_GUI_AUIDOCKART_H
#define NEGERNS_GUI_AUIDOCKART_H

#include <wx/aui/aui.h>
#include <negerns/core/declspec.h>

namespace negerns {
namespace gui {

class NEGERNS_DECL AuiDockArt : public wxAuiDefaultDockArt
{
public:
    AuiDockArt();
    virtual ~AuiDockArt();
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_AUIDOCKART_H