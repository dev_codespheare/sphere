/*******************************************************************************
 * File:    negerns/gui/icontrol.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 28 Apr 2013 10:11 PM
 *
 * Comment: Interface for components using controls
 ******************************************************************************/

#ifndef NEGERNS_GUI_ICONTROL_H
#define NEGERNS_GUI_ICONTROL_H

#include <wx/string.h>
#include <wx/vector.h>
#include <negerns/gui/ibase.h>

class wxWindow;
class wxPanel;
class wxBoxSizer;

namespace negerns {
namespace gui {

//! Interface for all components that uses GUI controls.
class NEGERNS_DECL IControl : public IBase
{
public:
    //! Create control(s).
    //!
    //! \see PostCreateControl
    //! \see RefreshControl
    //! \see InitContent
    virtual void CreateControl() = 0;

    //! Post CreateControl processing.
    //!
    //! \internal
    //! Some control settings can only be applied after the controls have been
    //! created.
    //! \endinternal
    virtual void PostCreateControl() = 0;

    //! Destroy all child controls.
    //!
    //! \internal
    //! Necessary since wxWidgets windows are destroyed via a call to \c Destroy.
    //! \endinternal
    virtual void DestroyControl() = 0;

    //! Destroys and recreate the control.
    //!
    //! \see CreateControl
    //! \see InitContent
    virtual void RefreshControl() = 0;

    //! Create initial contents of the controls. Intended to be explicitly
    //! called after \c CreateControl, \c RefreshControl or \c RefreshContent
    //! to repopulate the controls with data.
    //!
    //! \see CreateControl
    //! \see RefreshControl
    //! \see RefreshContent
    virtual void InitContent() final;

    //! Clear contents of control(s).
    virtual void ClearContent() = 0;

    //! Refresh contents of control(s).
    virtual void RefreshContent() = 0;

    //! Apply current configuration settings.
    //!
    //! This method is used when the configuration settings changed and the
    //! application needs to reflect these changes at runtime.
    //!
    //! \note There could be configuration settings that may not be applied
    //! at runtime. A good example of this is a configuration setting that
    //! requires the application to run a single instance. It is intended to
    //! be checked during application startup.
    virtual void ApplyConfiguration() = 0;

    //! Apply current theme settings.
    //!
    //! This method is used when theme settings changed and the application
    //! needs to reflect these changes at runtime.
    virtual void ApplyTheme() = 0;

    //! Returns true if the pane has the current focus. Otherwise,
    //! returns false. The pane has focus if one of its child windows or
    //! controls have focus.
    virtual bool HasFocus() const = 0;

    //! Set focus on the control with the specified name. If the control
    //! name is found then the method returns true. Otherwise, returns false.
    virtual bool SetFocus(const std::string &) = 0;

    //! Return pointer to the main control.
    //!
    //! Subclasses should implement this as needed.
    virtual wxWindow* GetControl();

    //! Add the reference to the control to the control list.
    //!
    //! The reference is only added to the control list if it is not NULL.
    //!
    //! \see IControl::controls
    static void AddControl(wxWindow* w);

    //! Find the control in the control list with the specified identifier
    //! and return a reference to the control.
    //!
    //! Returns a reference to the control if the specified identifier
    //! is found. Otherwise, returns NULL. After the call, the callee must
    //! check for a null result.
    //!
    //! \see IControl::controls
    static wxWindow* FindControl(const std::string& id);

protected:

    virtual bool PreInitContent();
    virtual bool OnInitContent();
    virtual void PostInitContent();

private:

    //! List of control object references.
    //!
    //! \internal
    //! This list allows the application to query a registered control and may
    //! control and may be used to communicate between controls. This avoids
    //! maintaining references to controls by the callee control.
    //! \endinternal
    //!
    //! \see IControl::AddControl
    //! \see IControl::GetControl
    static wxVector<wxWindow*> controls;
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_ICONTROL_H
