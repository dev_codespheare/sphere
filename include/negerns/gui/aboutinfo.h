/*******************************************************************************
 * File:    negerns/gui/aboutinfo.h
 * Created: 13 Aug 2013 9:30 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_GUI_ABOUTINFO_H
#define NEGERNS_GUI_ABOUTINFO_H

#include <wx/aboutdlg.h>
#include <negerns/core/declspec.h>

namespace negerns {
namespace gui {

class NEGERNS_DECL AboutInfo
{
public:
    AboutInfo();
    ~AboutInfo();


    //! Name of the application
    //!
    //! \internal If not used defaults to wxApp::GetAppDisplayName()
    void SetName(const std::string& name);
    std::string GetName() const;

    //! Version should contain program version without "version" word (e.g.,
    //! "1.2" or "RC2") while longVersion may contain the full version including
    //! "version" word (e.g., "Version 1.2" or "Release Candidate 2").
    //!
    //! If longVersion is empty, it is automatically constructed from version.
    //!
    //! \internal
    //! Generic and GTK native: use short version only, as a suffix to the
    //! program name msw and osx native: use long version.
    //! \endinternal
    void SetVersion(const std::string& version,
                    const std::string& longVersion = std::string());

    bool HasVersion() const;
    std::string GetVersion() const;
    std::string GetLongVersion() const;

    //! Application description.
    //! Brief, but possibly multiline, description of the program.
    void SetDescription(const std::string& desc);
    bool HasDescription() const;
    std::string GetDescription() const;

    //! Short string containing the program copyright information.
    void SetCopyright(const std::string& copyright);
    bool HasCopyright() const;
    std::string GetCopyright() const;

    //! Long, multiline string containing the text of the program licence
    void SetLicense(const std::string& licence);
    bool HasLicense() const;
    std::string GetLicense() const;

    // icon to be shown in the dialog, defaults to the main frame icon
    void SetIcon(const wxIcon& icon);
    bool HasIcon() const;
    wxIcon GetIcon() const;

    // Web site for the program and its description (defaults to URL itself if
    // empty)
    void SetWebSite(const std::string& url, const std::string& desc = std::string());
    bool HasWebSite() const;
    std::string GetWebSiteURL() const;
    std::string GetWebSiteDescription() const;

    //! List of developers of the program
    void SetDevelopers(const wxArrayString& developers);
    void AddDeveloper(const std::string& developer);
    bool HasDevelopers() const;
    const wxArrayString& GetDevelopers() const;

    //! List of documentation writers
    void SetDocWriters(const wxArrayString& docwriters);
    void AddDocWriter(const std::string& docwriter);
    bool HasDocWriters() const;
    const wxArrayString& GetDocWriters() const;

    //! List of artists for the program art
    void SetArtists(const wxArrayString& artists);
    void AddArtist(const std::string& artist);
    bool HasArtists() const;
    const wxArrayString& GetArtists() const;

    //! List of translators
    void SetTranslators(const wxArrayString& translators);
    void AddTranslator(const std::string& translator);
    bool HasTranslators() const;
    const wxArrayString& GetTranslators() const;


    // implementation only
    // -------------------

    // "simple" about dialog shows only textual information (with possibly
    // default icon but without hyperlink nor any long texts such as the
    // licence text)
    bool IsSimple() const;

    // Get the description and credits (i.e. all of developers, doc writers,
    // artists and translators) as a one long multiline string
    std::string GetDescriptionAndCredits() const;

    // returns the copyright with the (C) string substituted by the Unicode
    // character U+00A9
    std::string GetCopyrightToDisplay() const;



    
    void SetEdition(const std::string &s) { edition = s; }
    std::string GetEdition() const { return edition; }
    bool HasEdition() const { return !edition.empty(); }

    std::string GetBuildInfo() { return std::string(platform).append(" ").append(configuration).append(" Build"); }

    const std::string& GetPlatform() const { return platform; }
    const std::string& GetConfiguration() const { return configuration; }

    void SetLicensee(const std::string &s) { licensee = s; }
    const std::string& GetLicensee() const { return licensee; }

    void SetEvaluation(bool b) { evaluation = b; }
    bool IsEvaluation() const { return evaluation == true; }

    bool IsDebug() const { return is_debug == true; }
    bool IsRelease() const { return is_debug == false; }

protected:

    wxAboutDialogInfo about;

    std::string edition;
    std::string platform;
    std::string configuration;
    std::string licensee;

    bool evaluation;
    bool is_debug;
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_ABOUTINFO_H
