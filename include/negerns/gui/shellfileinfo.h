/*******************************************************************************
 * File:    negerns/gui/shellfileinfo.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: September 12, 2011 5:00 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_GUI_SHELLFILEINFO_H
#define NEGERNS_GUI_SHELLFILEINFO_H

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <shellapi.h>
#ifdef WIN_XP
  #include <shlobj.h>
#else
  #ifdef __MINGW32__
    // @todo Fix this. Undefined symbols like HICON.
  #endif
#endif
#include <wx/string.h>

namespace negerns {
namespace gui {

class ShellFileInfo
{
public:
    static HICON GetFolderIconHandle(bool smallIcon);
    static HICON GetFileIconHandle(LPCTSTR filename);
    static wxString GetFileType(const char* filename);
};  // class ShellFileInfo

#undef _CRT_SECURE_NO_WARNINGS

} //_ namespace gui
} //_ namespace negerns

#endif //_ GUI_SHELLFILEINFO_H
