/*******************************************************************************
 * File:    negerns/gui/base.h
 * Created: 15 Feb 2013 4:32 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_GUI_BASE_H
#define NEGERNS_GUI_BASE_H

#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <wx/msgdlg.h>
#include <wx/debug.h>
#include <wx/defs.h>
//#include <negerns/core/memory.h>
//#include <negerns/core/system.h>
//#include <negerns/core/message.h>
//#include <negerns/core/log.h>
//#include <negerns/core/logger.h>
#include <negerns/core/declspec.h>

namespace negerns {
namespace gui {

//! GUI base class.
class NEGERNS_DECL Base
{
public:
    Base();
    virtual ~Base();
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_BASE_H
