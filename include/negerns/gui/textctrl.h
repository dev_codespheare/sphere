/*******************************************************************************
 * File:    negerns/gui/textctrl.h
 * Created: 20 Dec 2013 12:30 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#include <wx/textctrl.h>
#include <negerns/core/defs.h>
#include <negerns/core/variant.h>
#include <negerns/core/declspec.h>
#include <negerns/gui/validator/dataxfer.h>
#include <negerns/poco.h>

#ifndef NEGERNS_GUI_TEXTCTRL_H
#define NEGERNS_GUI_TEXTCTRL_H

namespace negerns {
namespace gui {

class NEGERNS_DECL TextCtrl : public wxTextCtrl
{
public:
    TextCtrl();
#if 0
    TextCtrl(wxWindow *parent,
        wxWindowID id = wxID_ANY,
        const wxString &value = wxEmptyString,
        const wxPoint pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = Style::None,
        const wxValidator& validator = wxDefaultValidator,
        const wxString& name = wxTextCtrlNameStr);

    TextCtrl(wxWindow *parent,
        const wxSize& size = wxDefaultSize,
        long style = Style::None,
        std::string *val = NULL);

    TextCtrl(wxWindow *parent,
        const wxSize& size = wxDefaultSize,
        long style = Style::None,
        unsigned int *val = NULL);
#endif


    TextCtrl(wxWindow *parent,
        const wxSize& size = wxDefaultSize,
        long style = Style::None);

    virtual ~TextCtrl();

    void SetValidator(Var *val, long style = wxFILTER_NONE);
    void SetValidator(Var &val, long style = wxFILTER_NONE);

#if 0
    void Create(wxWindow *parent,
        wxWindowID id = wxID_ANY,
        const wxString &value = wxEmptyString,
        const wxPoint pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = Style::None,
        const wxValidator& validator = wxDefaultValidator,
        const wxString& name = wxTextCtrlNameStr);

    //! Create without id, position and choices
    void Create(wxWindow *parent,
        const wxSize& size = wxDefaultSize,
        long style = Style::None,
        std::string *val = NULL);

    //! Create without id, position and choices
    void Create(wxWindow *parent,
        const wxSize& size = wxDefaultSize,
        long style = Style::None,
        unsigned int *val = NULL);
#endif
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_TEXTCTRL_H
