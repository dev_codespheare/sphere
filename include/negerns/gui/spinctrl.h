/*******************************************************************************
 * File:    negerns/gui/spinctrl.h
 * Created: 25 May 2014 2:02 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#include <wx/textctrl.h>    // For wxTE_PROCESS_ENTER only
#include <wx/spinctrl.h>
#include <negerns/core/defs.h>
#include <negerns/core/variant.h>
#include <negerns/core/declspec.h>
#include <negerns/gui/validator/dataxfer.h>
#include <negerns/poco.h>

#ifndef NEGERNS_GUI_SPINCTRL_H
#define NEGERNS_GUI_SPINCTRL_H

namespace negerns {
namespace gui {

class NEGERNS_DECL SpinCtrl : public wxSpinCtrl
{
public:
    enum Style {
        Horizontal = wxSP_HORIZONTAL,
        Vertical = wxSP_VERTICAL,
        ArrowKeys = wxSP_ARROW_KEYS,
        Wrap = wxSP_WRAP,
        // Text alignment
        AlignLeft = wxALIGN_LEFT,
        AlignRight = wxALIGN_RIGHT,
        AlignCenter = wxALIGN_CENTRE_HORIZONTAL,
        // Enter key
        ProcessEnterKey = wxTE_PROCESS_ENTER
    };

    SpinCtrl();

    SpinCtrl(wxWindow *parent,
        const wxSize& size = wxDefaultSize,
        long style = Style::Vertical | Style::AlignRight | Style::ArrowKeys);

    virtual ~SpinCtrl();

    void SetValidator(Var *val, long style = wxFILTER_NONE);
    void SetValidator(Var &val, long style = wxFILTER_NONE);
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_SPINCTRL_H
