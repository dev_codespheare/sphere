/*******************************************************************************
 * File:    negerns/gui/dialog/dialog.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 24 Oct 2013 03:08
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_DIALOG_DIALOG_H
#define NEGERNS_GUI_DIALOG_DIALOG_H

#include <wx/string.h>
#include <wx/gdicmn.h>
#include <wx/dialog.h>
#include <wx/window.h>
#include <negerns/core/declspec.h>

namespace negerns {
namespace gui {

//! A wxDialog subclass for framework use only. Sits between DialogBase
//! class and wxDialog class. Its only purpose is to be able keep record
//! of an instance of wxDialog-subclass so subclasses could reference it
//! later.
//!
//! It may well be considered a temporary workaround because a
//! reference to the wxDialog could not be acquired when directly
//! inheriting a wxDialog and Component class at this inheritance
//! level. This will be an interim solution until a better one is found.
class NEGERNS_DECL Dialog : public wxDialog
{
public:
    Dialog();
    Dialog(wxWindow* parent,
        wxWindowID id,
        const wxString& title,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxDEFAULT_DIALOG_STYLE);
    bool Create(wxWindow* parent,
        wxWindowID id,
        const wxString& title,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxDEFAULT_DIALOG_STYLE);
    virtual ~Dialog();

    //! Set the parent window.
    static void SetParent(wxWindow* p);

protected:

    //! Get a reference to the wxDialog-based instance.
    virtual Dialog* GetThis() final { return dialog; }

protected:

    //! Parent window which must be set before using this class.
    //!
    //! Must be called during application startup.
    static wxWindow* parent;

private:

    //! Reference to the wxDialog-based instance.
    //! Initialized in the constructor only.
    Dialog* dialog;
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_DIALOG_DIALOG_H
