/*******************************************************************************
 * File:    negerns/gui/dialog/dialogbase.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 9 Apr 2013 9:35 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_GUI_DIALOG_DIALOGBASE_H
#define NEGERNS_GUI_DIALOG_DIALOGBASE_H

#include <negerns/gui/dialog/dialog.h>
#include <negerns/gui/component.h>
#include <negerns/core/declspec.h>

namespace negerns {
namespace gui {

//! %Base class of all dialog windows implementing a 2-step creation process
//! internally. The 2-step creation is intentional so that we could set
//! the wxWS_EX_VALIDATE_RECURSIVELY extra style flag for use with
//! wxValidator functionality. All subclasses therefore uses the wxValidator
//! internally. Subclasses may choose not to use such functionality.
class NEGERNS_DECL DialogBase : public Dialog, public Component
{
public:
    DialogBase();
    DialogBase(wxWindow* parent,
        wxWindowID id,
        const wxString& title,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxDEFAULT_DIALOG_STYLE);

    virtual ~DialogBase();

    bool Create(wxWindow* parent,
        wxWindowID id,
        const wxString& title,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxDEFAULT_DIALOG_STYLE);
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_DIALOG_DIALOGBASE_H
