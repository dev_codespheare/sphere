/*******************************************************************************
* File:    negerns/gui/dialog/propertydialog.h
* Created: 18 Dec 2013 6:35 AM
* Author:  rmaicle <rmaicle@gmail.com>
*
* Comment:
******************************************************************************/

#ifndef NEGERNS_GUI_DIALOG_PROPERTYDIALOG_H
#define NEGERNS_GUI_DIALOG_PROPERTYDIALOG_H

#include <negerns/core/declspec.h>
#include <negerns/gui/dialog/dialogbase.h>
#include <negerns/gui/manager/auinotebook.h>

namespace negerns {
namespace gui {

//! A dialog window using tab controls to display its components.
class NEGERNS_DECL PropertyDialog : public DialogBase
{
public:
    PropertyDialog();
    PropertyDialog(wxWindow* parent,
        wxWindowID id,
        const wxString& title,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxDEFAULT_DIALOG_STYLE | wxWS_EX_VALIDATE_RECURSIVELY | wxRESIZE_BORDER);
    virtual ~PropertyDialog();

    //! @{
    //! \name IControl Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    //! @}

    //! Add notebook components.
    virtual void AddComponent(Component *) final;

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    //! @}

    //! Cancel button event handler.
    virtual void OnCancelClicked(wxCommandEvent &);

private:

    //! Initialize the notebook manager.
    void InitNotebook();

protected:

    //! Sizer container for buttons.
    wxBoxSizer *buttonSizer;

    manager::AuiNotebook *notebook;
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_DIALOG_PROPERTYDIALOG_H
