/*******************************************************************************
 * File:    negerns/gui/dialog/logindialogbase.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 9 Apr 2013 9:38 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_GUI_DIALOG_LOGINDIALOGBASE_H
#define NEGERNS_GUI_DIALOG_LOGINDIALOGBASE_H

#include <negerns/gui/dialog/dialogbase.h>
#include <negerns/core/declspec.h>

namespace negerns {
namespace gui {

//! %Base class of all log in dialog windows.
class NEGERNS_DECL LogInDialogBase : public DialogBase
{
public:
    LogInDialogBase(wxWindow* parent,
        wxWindowID id,
        const wxString& title,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxDEFAULT_DIALOG_STYLE);
    virtual ~LogInDialogBase() { }

    virtual void OnLogin(wxCommandEvent&);
    virtual void OnClose(wxCloseEvent&);

    wxString GetUser() const { return uid; }
    void SetUser(const wxString& u) { uid = u; }
    wxString GetPassword() const { return pwd; }
    void SetPassword(const wxString& p) { pwd = p; }
    wxString GetDataSource() const { return src; }
    void SetDataSource(const wxString& ds) { src = ds; }

protected:

    //! Return a pointer to the internal username member variable.
    //! The return value is primarily intended to be passed to a wxValidator
    //! constructor.
    wxString* GetUserPtr() { return &uid; }

    //! Return a pointer to the internal username member variable.
    //! The return value is primarily intended to be passed to a wxValidator
    //! constructor.
    wxString* GetPasswordPtr() { return &pwd; }

protected:

    //! Data source user identifier
    wxString uid;

    //! Data source User password
    wxString pwd;

    //! \brief Data source which is either an ODBC Data Source Name (DSN) or
    //! a filename to the local database.
    //! \see Connection::datasource
    wxString src;
};

} // namesapce gui
} // namesapce negerns

#endif //_ NEGERNS_GUI_DIALOG_LOGINDIALOGBASE_H
