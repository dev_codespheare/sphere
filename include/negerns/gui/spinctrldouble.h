/*******************************************************************************
 * File:    negerns/gui/spinctrldouble.h
 * Created: 2 Jun 2014 5:28 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#include <wx/spinctrl.h>
#include <negerns/core/defs.h>
#include <negerns/core/variant.h>
#include <negerns/core/declspec.h>
#include <negerns/gui/spinctrl.h>   // For styles only
#include <negerns/gui/validator/dataxfer.h>

#include <negerns/poco.h>

#ifndef NEGERNS_GUI_SPINCTRLDOUBLE_H
#define NEGERNS_GUI_SPINCTRLDOUBLE_H

namespace negerns {
namespace gui {

class NEGERNS_DECL SpinCtrlDouble : public wxSpinCtrlDouble
{
public:

    SpinCtrlDouble();

    SpinCtrlDouble(wxWindow *parent,
        const wxSize& size = wxDefaultSize,
        long style = SpinCtrl::Style::Horizontal | SpinCtrl::Style::AlignRight);

    virtual ~SpinCtrlDouble();

    void SetValidator(Var *val, long style = wxFILTER_NONE);
    void SetValidator(Var &val, long style = wxFILTER_NONE);
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_SPINCTRLDOUBLE_H
