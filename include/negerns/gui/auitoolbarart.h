/*******************************************************************************
 * File:    negerns/gui/auitoolbarart.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 07 Dec 2013 3:32 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_GUI_AUITOOLBARART_H
#define NEGERNS_GUI_AUITOOLBARART_H

#include <wx/aui/auibar.h>
#include <negerns/gui/bar/bar.h>
#include <negerns/core/declspec.h>

namespace negerns {
namespace gui {
namespace bar {

class NEGERNS_DECL AuiToolBarArt : public wxAuiDefaultToolBarArt
{
public:
    AuiToolBarArt();
    virtual ~AuiToolBarArt();

    virtual wxAuiToolBarArt* AuiToolBarArt::Clone() override;

    virtual void DrawBackground(wxDC& dc,
        wxWindow* wnd,
        const wxRect& rect) override;

    virtual void DrawPlainBackground(wxDC& dc,
        wxWindow* wnd,
        const wxRect& rect) override;

    void SetBorder(Bar::Border b);

    void DrawBorder(wxDC &dc, const wxRect &rect);

    virtual void DrawLabel(
        wxDC& dc,
        wxWindow* wnd,
        const wxAuiToolBarItem& item,
        const wxRect& rect) override;

    virtual void DrawButton(
        wxDC& dc,
        wxWindow* wnd,
        const wxAuiToolBarItem& item,
        const wxRect& rect) override;

private:

    Bar::Border border;
};

} //_ namespace bar
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_AUITOOLBARART_H
