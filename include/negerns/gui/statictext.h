/*******************************************************************************
 * File:    negerns/gui/statictext.h
 * Created: 21 Dec 2013 5:08 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#include <wx/stattext.h>
#include <negerns/core/defs.h>
#include <negerns/core/declspec.h>

#ifndef NEGERNS_GUI_STATICTEXT_H
#define NEGERNS_GUI_STATICTEXT_H

namespace negerns {
namespace gui {

class NEGERNS_DECL StaticText : public wxStaticText
{
public:
    StaticText();
    StaticText(wxWindow *parent,
        wxWindowID id,
        const wxString &label,
        const wxPoint pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = Style::None,
        const wxString& name = wxStaticTextNameStr);

    StaticText(wxWindow *parent,
        const wxString &label,
        long style = Style::None);

    ~StaticText();
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_STATICTEXT_H
