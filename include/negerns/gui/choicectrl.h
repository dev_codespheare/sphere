/*******************************************************************************
 * File:    negerns/gui/choicectrl.h
 * Created: 20 Dec 2013 12:31 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#include <vector>
#include <map>
#include <wx/choice.h>
#include <negerns/core/defs.h>
#include <negerns/core/variant.h>
#include <negerns/core/declspec.h>
#include <negerns/gui/validator/dataxfer.h>
#include <negerns/data/datastore.h>

#include <negerns/negerns.h>
#include <negerns/poco.h>

#ifndef NEGERNS_GUI_CHOICECTRL_H
#define NEGERNS_GUI_CHOICECTRL_H

namespace negerns {
namespace gui {

class NEGERNS_DECL ChoiceCtrl : public wxChoice
{
public:
    ChoiceCtrl();
#if 0
    ChoiceCtrl(wxWindow *parent,
        wxWindowID id = wxID_ANY,
        const wxPoint pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        int n = 0, const wxString choices[] = NULL,
        long style = Style::None,
        const wxValidator& validator = wxDefaultValidator,
        const wxString& name = wxChoiceNameStr);

    ChoiceCtrl(wxWindow *parent,
        const wxSize& size = wxDefaultSize,
        long style = Style::None,
        std::string *val = NULL);
#endif

    ChoiceCtrl(wxWindow *parent,
        const wxSize& size = wxDefaultSize,
        long style = Style::None);

    virtual ~ChoiceCtrl();

    void SetValidator(Var *val, long style = wxFILTER_NONE);

    void SetValidator(Var &val, long style = wxFILTER_NONE);

#if 0
    void Create(wxWindow *parent,
        wxWindowID id = wxID_ANY,
        const wxPoint pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        int n = 0, const wxString choices[] = NULL,
        long style = Style::None,
        const wxValidator& validator = wxDefaultValidator,
        const wxString& name = wxChoiceNameStr);

    //! Create without id, position and choices
    void Create(wxWindow *parent,
        const wxSize& size = wxDefaultSize,
        long style = Style::None,
        std::string *val = NULL);
#endif

    //! @{
    //! \name Value Getter/Setter Functions

    //! Set the selection to the specified value or equivalent label.
    //!
    //! The specified value must exist in the selections otherwise the function
    //! will not do anything and return false.
    bool SetValue(const std::string &);

    //! Returns the value of the current selection.
    Var GetValue() const;

    //! Return the value of the specified item.
    //!
    //! The control items must have been set prior.
    Var GetValue(std::size_t) const;

    //! Set the item choices.
    //! The string is the label and value.
    std::size_t SetValues(const std::vector<std::string> &);

    //! Set the item choices.
    //! The map key is the value and map value is the display label.
    std::size_t SetValuesMap(const std::map<std::size_t, std::string> &);

    //! Set the item choices.
    //! Both map key and value are strings.
    std::size_t SetValuesMap(const std::map<std::string, std::string> &);

    std::size_t SetValues(::negerns::data::DataStore *,
        const ::negerns::data::Column &label);

    std::size_t SetValues(::negerns::data::DataStore *,
        const std::string &label);

    std::size_t SetValues(::negerns::data::DataStore *,
        const ::negerns::data::Column &label,
        const ::negerns::data::Column &value);

    std::size_t SetValues(::negerns::data::DataStore *,
        const std::string &label,
        const std::string &value);

    //! @}

private:

    ::negerns::data::Column labelColumn;
    ::negerns::data::Column valueColumn;

};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_CHOICECTRL_H
