/*******************************************************************************
 * File:    negerns/gui/outputctrl.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 30 Jul 2013 4:35 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_GUI_MEDIATORCORE_H
#define NEGERNS_GUI_MEDIATORCORE_H

#include <string>
#include <wx/window.h>
#include <negerns/core/declspec.h>
#include <negerns/core/outputsink.h>

class wxStyledTextCtrl;

namespace negerns {
namespace gui {

class NEGERNS_DECL OutputCtrl : public OutputSink
{
public:

    OutputCtrl(wxWindow* parent);
    virtual ~OutputCtrl();

    virtual void Send(const std::string &s) override;

    virtual void Clear() override;

    wxWindow* GetControl() { return (wxWindow *) control; }

private:

    //! Output control.
    wxStyledTextCtrl* control;
};

} // gui
} // negerns

#endif //_ NEGERNS_GUI_MEDIATORCORE_H
