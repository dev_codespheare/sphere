/*******************************************************************************
 * File:    negerns/gui/data/list.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 29 May 2014 2:49 PM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_DATA_LIST_H
#define NEGERNS_GUI_DATA_LIST_H

#include <negerns/gui/data/base.h>
#include <negerns/gui/listview.h>

#include <negerns/negerns.h>

namespace negerns {
namespace gui {
namespace data {

class NEGERNS_DECL List : public Base
{
public:
    List();
    virtual ~List();

    //! @{
    //! \name IControl Public Virtual Functions

    virtual void CreateControl() override;

    virtual void PostCreateControl() override;

    virtual void DestroyControl() override;
    
    virtual void ApplyConfiguration() override;

    //! @}

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;
    virtual void PostInitContent() override;

    //! @}

    //! @{
    //! \name ICRUD Virtual Functions

    virtual void OnEdit(wxCommandEvent &) override;
    virtual bool OnRefresh(wxCommandEvent &) override;

    //! @}

    //! Function called to customize the list display. Return true to continue
    //! default processing.
    //!
    //! \see ListViewComponent::OnDisplayRow
    virtual bool OnRowDisplay(std::size_t, std::size_t, wxVariant &) = 0;

    n::gui::ListView * GetListCtrl();

private:

    //! @{
    //! \name ListViewComponent Event Handlers

    void OnSelectionChanged(wxDataViewEvent &);
    void OnSelectionActivated(wxDataViewEvent &);
    void OnMouse(wxMouseEvent &);

    //! @}

protected:

    wxDataViewItem current;

private:

    n::gui::ListView *list = nullptr;

};

} //_ namespace data
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_DATA_LIST_H
