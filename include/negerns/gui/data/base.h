/*******************************************************************************
 * File:    negerns/gui/data/base.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 02 Oct 2013 5:31 PM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_DATA_BASE_H
#define NEGERNS_GUI_DATA_BASE_H

#include <negerns/core/declspec.h>
#include <negerns/core/system.h>
#include <negerns/core/icrud.h>
#include <negerns/core/member.h>
#include <negerns/gui/component.h>
#include <negerns/data/helper.h>

namespace negerns {
namespace gui {
namespace data {

class NEGERNS_DECL Base :
    public Component,
    public negerns::ICrud
{
public:

    //! Type of presentation.
    enum class Presentation {
        List,
        Form
    };

    //! Type of operation.
    enum class Operation {
        Create,
        Edit,
        Display
    };

    static negerns::data::DataStore * CreateDataStore();

    Base();
    virtual ~Base();

    //! @{
    //! \name IControl Public Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    virtual void PostCreateControl() override;

    virtual void DestroyControl() override;

    virtual void ApplyConfiguration() override;

    //! @}

    void Create(const negerns::data::Parameters &p = {});

    void Read(const negerns::data::Parameters &);

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    //! Initializes the data source and SQL statements.
    //!
    //! The base class function must be called first to initialize the data
    //! source. The client code can then set the SQL statements to be used.
    virtual bool OnInitContent() override;

    //! Initialize the controls with data values from the data source.
    //!
    //! This is called after data has been retrieved from a data source but
    //! before being displayed. The controls will display their data after a
    //! call to \c XferDataToWindow which is automatically called after this
    //! function.
    virtual void PostInitContent() override;

    //! @}

    //! @{
    //! \name ICRUD Virtual Functions

    virtual bool PreCreate(wxCommandEvent &) override;
    virtual bool OnCreate(wxCommandEvent &) override;
    virtual bool PreRead(wxCommandEvent &) override;
    virtual bool OnRead(wxCommandEvent &) override;
    virtual bool PreRefresh(wxCommandEvent &) override;
    virtual bool OnRefresh(wxCommandEvent &) override;
    virtual bool PreUpdate(wxCommandEvent &) override;
    virtual bool OnUpdate(wxCommandEvent &) override;
    virtual bool PostUpdate(wxCommandEvent &) override;

    //! @}

    //! @{
    //! \name Data-Related Functions
    //!
    //! SQL statements used in database transactions. The statements are
    //! not checked for validity. It is up to the client to ensure that
    //! the SQL statements are well formed.

    //! Set the \c Parameters to be used for data retrieval.
    void SetParameters(const negerns::data::Parameters &);

    //! Display the error message.
    //!
    //! First item is the summary and followed by the detailed messages.
    int ShowErrorMessage(int);

    n::data::DataStore * GetDataStore();

    //! @}

    virtual void OnInitialRetrieve();

    //! Set unique identifier of the component.
    //!
    //! This is used by Pane manager when opening new components as tab pages
    //! of the same type. Pane manager could first check whether a component
    //! is already opened as a tab page. If it is the Pane manager could set
    //! focus or activate that tab page. Otherwise, it opens a new tab page.
    void SetIdentifier(const std::string &s = std::string());

    std::string FormatMessage(const std::string &);

protected:

    Presentation presentation;
    Operation operation;
    negerns::data::Parameters parameters;
    negerns::member::String subject;

private:

    std::string rootIdentifier;

    n::data::DataStore *datastore = nullptr;
};

} //_ namespace data
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_DATA_BASE_H

