/*******************************************************************************
 * File:    negerns/gui/data/input/single.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 26 May 2014 9:31 PM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_DATA_INPUT_SINGLE_H
#define NEGERNS_GUI_DATA_INPUT_SINGLE_H

#include <negerns/gui/data/base.h>
#include <negerns/gui/sizer/flexgrid.h>

#include <negerns/negerns.h>

#define DEFAULT_PROMPT_MESSAGE "Save Information"
#define DEFAULT_PROMPT_CREATE_EMESSAGE  \
    "The newly created information contains user input(s).\n" \
    "Would you like to save the new information?"
#define DEFAULT_PROMPT_EDIT_EMESSAGE \
    "The information contains modification(s).\n" \
    "Would you like to save the modified information?"

#define ALT_PROMPT_MESSAGE "Save %1 Information"
#define ALT_PROMPT_CREATE_EMESSAGE  \
    "The newly created %1 information contains user input(s).\n" \
    "Would you like to save the new %1 information?"
#define ALT_PROMPT_EDIT_EMESSAGE \
    "The %1 information contains user modification(s).\n" \
    "Would you like to save the modified %1 information?"

namespace negerns {
namespace gui {
namespace data {
namespace input {

class NEGERNS_DECL Single : public Base
{
public:
    Single();
    virtual ~Single();

    //! @{
    //! \name IControl Public Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    virtual void DestroyControl() override;

    //! @}

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual void PostInitContent() override;

    //! @}

    //! @{
    //! \name ICRUD Virtual Functions

    virtual bool OnCreate(wxCommandEvent &) override;
    virtual bool OnRead(wxCommandEvent &) override;
    virtual bool OnRefresh(wxCommandEvent &) override;
    virtual bool OnUpdate(wxCommandEvent &) override;
    virtual bool PostUpdate(wxCommandEvent &) override;

    //! @}

    virtual bool CanClose() override;

    //! Set the control to have first focus.
    //!
    //! The specified control gets focus on initial display or after a refresh
    //! operation.
    void SetFirstFocus(wxWindow *);

    //! Set focus to the control that was set to have the first focus.
    //!
    //! \see SetFirstFocus
    void GotoFirstFocus();

private:

    //! Display a message if the user wants to save the data modifications.
    //! Returns the numeric value of the selected operation.
    n::Operation ShowUpdateMessage();

protected:

    n::data::Row original;
    n::data::Row current;

private:

    wxWindow *firstFocus = nullptr;
};

} //_ namespace input
} //_ namespace data
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_DATA_INPUT_SINGLE_H
