/*******************************************************************************
 * File:    negerns/gui/Splitter.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 02 Oct 2013 5:31 PM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_SPLITTER_H
#define NEGERNS_GUI_SPLITTER_H

#include <wx/splitter.h>
#include <negerns/core/declspec.h>

namespace negerns {
namespace gui {

class NEGERNS_DECL Splitter : public wxSplitterWindow
{
public:
    Splitter(wxWindow* parent);
    virtual ~Splitter();
    void Split(wxWindow* control1, wxWindow* control2, int mode = wxSPLIT_VERTICAL);
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_SPLITTER_H
