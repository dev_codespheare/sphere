/*******************************************************************************
 * File:    negerns/gui/renderer/choice.h
 * Created: 21 Jun 2014 7:30 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#include <vector>
#include <wx/dataview.h>
#include <negerns/core/declspec.h>
#include <negerns/core/variant.h>
#include <negerns/gui/choicectrl.h>
#include <negerns/gui/listview.h>

#include <negerns/negerns.h>

#ifndef NEGERNS_GUI_RENDERER_CHOICE_H
#define NEGERNS_GUI_RENDERER_CHOICE_H

namespace negerns {
namespace gui {
namespace renderer {

class NEGERNS_DECL Choice : public wxDataViewCustomRenderer
{
public:

    Choice(const std::vector<std::string> &items,
        n::gui::ListView::CellMode mode = n::gui::ListView::CellMode::Inert,
        int alignment = wxDVR_DEFAULT_ALIGNMENT);

    virtual ~Choice();

    //! @{
    //! \name wxDataViewCustomRenderer Virtual Functions

    virtual bool SetValue(const wxVariant &) override;

    virtual bool GetValue(wxVariant &) const override;

    virtual wxSize GetSize() const override;

    virtual bool Render(wxRect cell, wxDC *dc, int state) override;

    virtual wxWindow* CreateEditorCtrl(wxWindow *parent, wxRect labelRect, const wxVariant &value) override;

    virtual bool GetValueFromEditorCtrl(wxWindow* editor, wxVariant &value) override;

    //! @}

    //! Set the value.
    bool SetValue(const std::string &);

    std::size_t SetValues(const std::vector<std::string> &);

    //! Return the value.
    std::string GetValue() const;

protected:

    n::gui::ChoiceCtrl *control = nullptr;

    std::vector<std::string> choices;

    //! A buffer for the control value.
    //! Used by SetValue and GetValue overloads.
    std::string value;
};

} //_ namespace renderer
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_RENDERER_CHOICE_H
