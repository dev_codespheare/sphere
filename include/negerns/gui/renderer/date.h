/*******************************************************************************
 * File:    negerns/gui/renderer/date.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 20 May 2014 12:31 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_RENDERER_DATE_H
#define NEGERNS_GUI_RENDERER_DATE_H

#include <string>
#include <wx/dataview.h>
#include <negerns/core/declspec.h>
#include <negerns/core/datetime.h>
#include <negerns/gui/listview.h>

namespace negerns {
namespace gui {
namespace renderer {

class NEGERNS_DECL Date : public wxDataViewCustomRenderer
{
public:
    Date(const wxString &varianttype = wxT("datetime"),
        ListView::CellMode mode = ListView::CellMode::Editable,
        int align = negerns::Alignment::Center);
    virtual ~Date();

    virtual bool SetValue(const wxVariant &value) override;
    virtual bool GetValue(wxVariant& value) const override;
    virtual bool Render(wxRect cell, wxDC *dc, int state) override;
    virtual wxSize GetSize() const override;

    void SetFormat(const std::string &fmt);

private:

    wxDateTime date;

    std::string format;
};

} //_ namespace renderer
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_RENDERER_DATE_H
