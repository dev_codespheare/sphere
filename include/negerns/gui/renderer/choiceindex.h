/*******************************************************************************
 * File:    negerns/gui/renderer/choiceindex.h
 * Created: 22 Jun 2014 6:13 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#include <vector>
#include <wx/dataview.h>
#include <negerns/core/declspec.h>
#include <negerns/gui/choicectrl.h>
#include <negerns/gui/listview.h>

#include <negerns/negerns.h>

#ifndef NEGERNS_GUI_RENDERER_CHOICEINDEX_H
#define NEGERNS_GUI_RENDERER_CHOICEINDEX_H

namespace negerns {
namespace gui {
namespace renderer {

class NEGERNS_DECL ChoiceIndex : public wxDataViewCustomRenderer
{
public:

    ChoiceIndex(const std::vector<std::string> &items,
        n::gui::ListView::CellMode mode = n::gui::ListView::CellMode::Inert,
        int alignment = wxDVR_DEFAULT_ALIGNMENT);

    virtual ~ChoiceIndex();

    //! @{
    //! \name Choice Virtual Functions

    virtual bool SetValue(const wxVariant &) override;

    virtual bool GetValue(wxVariant &) const override;

    virtual wxSize GetSize() const override;

    virtual bool Render(wxRect cell, wxDC *dc, int state) override;

    virtual wxWindow* CreateEditorCtrl(wxWindow *parent, wxRect labelRect, const wxVariant &value) override;

    virtual bool GetValueFromEditorCtrl(wxWindow* editor, wxVariant &value) override;

    virtual bool StartEditing(const wxDataViewItem &, wxRect) override;

    //! @}

    //! Set the value.
    bool SetValue(unsigned int);

    std::size_t SetValues(const std::vector<std::string> &);

    //! Return the value.
    unsigned int GetValue() const;

protected:

    n::gui::ChoiceCtrl *control = nullptr;

    std::vector<std::string> choices;

    //! A buffer for the control value.
    //! Used by SetValue and GetValue overloads.
    unsigned int value;

private:

    //! Buffer variable only so we do not create it every time \c Render is
    //! called by the framework.
    std::string display;
};

} //_ namespace renderer
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_RENDERER_CHOICEINDEX_H
