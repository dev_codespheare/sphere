/*******************************************************************************
 * File:    negerns/gui/resource.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 19 Sep 2013 4:06 AM
 *
 ******************************************************************************/

#ifndef NEGERNS_GUI_RESOURCE_H
#define	NEGERNS_GUI_RESOURCE_H

#include <string>
#include <map>
#include <negerns/core/declspec.h>

namespace negerns {
namespace gui {

//! Container for resource files and resource path.
class NEGERNS_DECL Resource
{
public:

    Resource();
    ~Resource();

    //! \brief Add a resource file entry.
    static void Add(const std::string& id, const std::string& value);
    //! \brief Remove a resource file entry.
    static void Remove(const std::string& id);
    //! Clear all resource file entries.
    static void Clear();
    //! \brief Find a resource file entry.
    static bool Find(const std::string& id);
    //! \brief Get a resource file entry.
    static std::string Get(const std::string& id);
    //! \brief Set the path where resource files are located.
    static void SetPath(const std::string& p);
    //! \brief Return the path of the resource files.
    static std::string GetPath();
    //! \brief Build the resource URL
    //! \details returns resbinfile + '#zip:' + resfile
    static std::string BuildUrl(const std::string &resbinfile, const std::string &resfile);

    //! Shutdown the resource management component.
    static void Shutdown();

private:

    class ResourceImp;

    static ResourceImp* Implementation(ResourceImp * = nullptr);
};

} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_RESOURCE_H
