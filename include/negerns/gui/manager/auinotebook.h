/*******************************************************************************
 * File:    negerns/gui/manager/auinotebook.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 03 Oct 2013 8:33 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_MANAGER_AUINOTEBOOK_H
#define NEGERNS_GUI_MANAGER_AUINOTEBOOK_H

#include <wx/vector.h>
#include <wx/bitmap.h>
#include <wx/aui/auibook.h>
#include <negerns/gui/component.h>

namespace negerns {
namespace gui {
namespace manager {

#if 0
WX_DECLARE_HASH_MAP(wxString, Component*, wxStringHash, wxStringEqual, MapComponents);
#endif

//! Advance User Interface Notebook manager class.
//!
//! The class displays Component subclasses as notebook tab pages.
//!
//! \internal
//! The implementation of this class could have been implemented in the Pane
//! class. But the functionality was segreggated into a separate class to allow
//! Component subclasses to use a notebook tab control.
//! \endinternal
class NEGERNS_DECL AuiNotebook : public virtual Component
{
public:

    AuiNotebook();
    virtual ~AuiNotebook();

    //! @{
    //! \name IControl Public Virtual functions

    virtual void CreateControl() override;

    virtual void PostCreateControl() override;

    virtual void DestroyControl() override;

    virtual void ApplyConfiguration() override;

    //! @}

    //! @{
    //! \name Component and TabPage Management Functions

    //! Add a pane component.
    //!
    //! \param Component subclass object.
    void AddComponent(Component*);

    //! Returns the pane component with the specified name. If the specified name
    //! is empty then it returns the active pane component.
    negerns::gui::Component* GetComponent(const std::string &name = "");

    std::size_t GetComponentIndex(const std::string &name = "");

    bool IsComponentPresent(const std::string &name);

    //! Add a Component subclass as tab page.
    //! If the freeze argument determines whether the notebook control freezes
    //! user interface updates until before the function returns. This could be
    //! useful for visual appearance optimizations.
    virtual void AddPage(Component *, bool freeze = true);

    void ClosePage();
    void ClosePage(std::size_t);
    void CloseAllPages();

    std::size_t GetCount() const;

    //! Set the specified window to be the active notebook page.
    void SetActivePage(wxWindow* page);

    //! Set the specified index to be the active notebook page.
    void SetActivePage(size_t n);

    //! @}

    //! @{
    //! \name Tab control settings

    //! Set the flags that will be used when creating the notebook
    //! control.
    //!
    //! The following flags are set by default: wxAUI_NB_TAB_MOVE |
    //! wxAUI_NB_TAB_SPLIT | wxAUI_NB_TAB_EXTERNAL_MOVE |
    //! wxAUI_NB_SCROLL_BUTTONS | wxAUI_NB_WINDOWLIST_BUTTON
    void SetNotebookFlags(int n) { notebookFlags = n; }

    //! Tells the control whether to draw the border or not.
    //!
    //! It tells the control to draw a border around the entire control
    //! starting from above the tab pages. The default is to draw the border.
    void SetBorder(bool b);

    //! Tells the control whether to draw the lower border or not.
    //!
    //! It tells the control to draw a border around the entire control
    //! starting from below the tab pages. The default is to not draw the border.
    void SetLowerBorderOnly(bool b);

    //! Set sizer flags.
    void SetSizerFlags(int f);

    //! Set the border size.
    void SetSizerBorder(int b);

    //! Set the notebook tab height.
    void SetTabHeight(int h);

    //! @}

    //! Return the notebook control.
    //!
    //! \see Component::GetControl
    virtual wxWindow* GetControl();

protected:

    //! @{
    //! \name IControl Public Virtual functions

    virtual bool OnInitContent() override;

    //! @}

private:

    //! Tab control where all Component subclasses are displayed.
    wxAuiNotebook* notebook;

    //! Flags to be used in creating the wxAuiNotebook control.
    int notebookFlags;

    //! Sizer flags.
    //!
    //! The flags are applied when the control is added to the
    //! sizer control. The default border size is wxEXPAND | wxALL.
    int sizerFlags;

    //! Sizer border size in pixels.
    //!
    //! The border size is applied when the control is added to the
    //! sizer control. The default border size is wxBORDER_DEFAULT.
    int sizerBorder;

    //! Registered Component subclasses.
    Components components;
#if 0
    //! All Component subclasses.
    //!
    //! A static list of all Component subclasses used by the
    //! application and registered using PaneBase::AddComponent.
    static MapComponents allComponents;
#endif
    //! Height of the tab control.
    int tabControlHeight;

    //! Border flag whether the control will draw its border or not.
    //! It tells the control to draw a border around the entire control
    //! starting from above the tab pages.
    //!
    //! \see SetBorder
    bool drawBorder;

    //! Border flag whether the control draws a border around the lower
    //! part of the control starting below the tab pages.
    //!
    //! \see drawBorder
    //! \see SetDrawLowerBorderOnly
    bool drawLowerBorderOnly;
};

} //_ namespace manager
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_MANAGER_AUINOTEBOOK_H
