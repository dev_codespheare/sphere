/*******************************************************************************
 * File:    negerns/gui/manager/pane.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 02 Oct 2013 9:53 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_MANAGER_PANE_H
#define NEGERNS_GUI_MANAGER_PANE_H

#include <wx/vector.h>
#include <negerns/gui/auicomponent.h>
#include <negerns/gui/pane.h>
#include <negerns/gui/icontrol.h>

namespace negerns {
namespace gui {
namespace manager {

//! Pane Manager.
//!
//! Classes can implement pane management by inheriting this class.
//! The \c FrameManager class inherits this class to manage the main application
//! Panes. This class can also be inherited by a \c Component class to
//! implement its own Panes within the \c Component.
class NEGERNS_DECL Pane : protected negerns::gui::IControl, public AuiComponent
{
public:
    Pane();
    virtual ~Pane();

    //! @{
    //! \name IControl Public Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    virtual void PostCreateControl() override;

    virtual void DestroyControl() override;

    //! \copydoc IControl::RefreshControl
    virtual void RefreshControl() override { }

    //! \copydoc IControl::ClearContent
    virtual void ClearContent() override { }

    //! \copydoc IControl::RefreshContent
    virtual void RefreshContent() override { }

    //! \copydoc IControl::ApplyConfiguration
    //! Apply configuration settings to the frame window and all panes.
    virtual void ApplyConfiguration() override;

    //! \copydoc IControl::ApplyTheme
    virtual void ApplyTheme() override { }

    //! \copydoc IControl::HasFocus
    virtual bool HasFocus() const override { return false; }

    //! copydoc IControl::SetFocus
    virtual bool SetFocus(const std::string &) override { return false; }

    //! @}

    //! @{
    //! \name Pane Management Functions

    //! Add a Pane object to the Pane collection.
    //! Return the index of the newly added Pane object.
    std::size_t AddPane(negerns::gui::Pane *);

    //! Return a reference to the Pane object at the specified index.
    negerns::gui::Pane* GetPane(std::size_t);

    //! Return the number of registered Pane objects.
    std::size_t GetPaneCount() const;

    //! @}

    //! Add a page to the specified Pane object.
    static void AddPage(negerns::gui::Pane *,
        negerns::gui::Component *,
        bool activate = true);

    void SetParent(wxWindow* parent);

#if EXCLUDE
    //! \brief Return the pane component specified by the argument string.
    Component* GetPaneComponent(const wxString&);
#endif

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    //! @}

protected:

    wxVector<negerns::gui::Pane *> panes;

    wxWindow* parent;
};

} //_ namespace manager
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_MANAGER_PANE_H
