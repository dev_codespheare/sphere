/*******************************************************************************
 * File:    negerns/gui/bar/panequickbar.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 10 Apr 2013 1:38 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_BAR_PANEQUICKBAR_H
#define NEGERNS_GUI_BAR_PANEQUICKBAR_H

#include <negerns/gui/bar/quickbar.h>

namespace negerns {
namespace gui {
namespace bar {

//! %Base class of all pane quickbars.
class NEGERNS_DECL PaneQuickBar : public QuickBar
{
public:
    PaneQuickBar() : QuickBar() { }
    virtual ~PaneQuickBar() { }
};

} //_ namespace bar
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_BAR_PANEQUICKBAR_H
