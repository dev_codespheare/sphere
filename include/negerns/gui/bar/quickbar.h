/*******************************************************************************
 * File:    negerns/gui/bar/quickbar.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 10 Apr 2013 1:37 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_BAR_QUICKBAR_H
#define NEGERNS_GUI_BAR_QUICKBAR_H

#include <negerns/gui/bar/barbase.h>

namespace negerns {
namespace gui {
namespace bar {

//! %Base class of all quickbars.
class NEGERNS_DECL QuickBar : public Bar
{
public:
    QuickBar() : BarBase() { }
    virtual ~QuickBar() { }
};

} //_ namespace bar
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_BAR_QUICKBAR_H
