/*******************************************************************************
 * File:    negerns/gui/bar/crudtoolbar.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 03 Oct 2013 5:24 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_GUI_BAR_CRUDTOOLBAR_H
#define NEGERNS_GUI_BAR_CRUDTOOLBAR_H

#include <negerns/core/declspec.h>
#include <negerns/gui/bar/toolbar.h>

namespace negerns {
namespace gui {
namespace bar {

class NEGERNS_DECL CrudToolBar : public ToolBar
{
public:
    CrudToolBar();
    ~CrudToolBar();

    //! @{
    //! \name IControl Virtual Functions

    //! \copydoc ToolBarBase::CreateControl
    virtual void CreateControl() override;

    virtual void DestroyControl() override;

    //! @}

};

} //_ namespace bar
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_BAR_CRUDTOOLBAR_H
