/*******************************************************************************
 * File:    negerns/gui/bar/toolbar.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 10 Apr 2013 1:37 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_BAR_TOOLBAR_H
#define NEGERNS_GUI_BAR_TOOLBAR_H

#include <negerns/gui/bar/bar.h>

namespace negerns {
namespace gui {
namespace bar {

//! \brief Base class of all toolbars.
class NEGERNS_DECL ToolBar : public Bar
{
public:
    ToolBar() : Bar() { }
    virtual ~ToolBar() { }
};

} //_ namespace bar
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_BAR_TOOLBAR_H
