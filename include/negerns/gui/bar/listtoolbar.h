/*******************************************************************************
 * File:    negerns/gui/bar/listtoolbar.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 03 Oct 2013 5:24 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_GUI_BAR_LISTTOOLBAR_H
#define NEGERNS_GUI_BAR_LISTTOOLBAR_H

#include <negerns/core/declspec.h>
#include <negerns/core/icrud.h>
#include <negerns/gui/bar/toolbar.h>

namespace negerns {
namespace gui {
namespace bar {

class NEGERNS_DECL ListToolBar : public ToolBar
{
public:
    ListToolBar();
    ~ListToolBar();

    //! @{
    //! \name IControl Virtual Functions

    //! \copydoc ToolBarBase::CreateControl
    virtual void CreateControl();

    //! @}

};

} //_ namespace bar
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_BAR_LISTTOOLBAR_H
