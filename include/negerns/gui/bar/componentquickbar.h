/*******************************************************************************
 * File:    negerns/gui/bar/componentquickbar.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 10 Apr 2013 1:38 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_BAR_COMPONENTQUICKBAR_H
#define NEGERNS_GUI_BAR_COMPONENTQUICKBAR_H

#include <negerns/gui/bar/quickbar.h>

namespace negerns {
namespace gui {
namespace bar {

//! %Base class of all component quickbars.
class NEGERNS_DECL ComponentQuickBar : public QuickBar
{
public:
    PaneComponentQuickBar() : QuickBar() { }
    virtual ~PaneComponentQuickBar() { }
};

} //_ namespace bar
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_BAR_COMPONENTQUICKBAR_H
