/*******************************************************************************
 * File:    negerns/gui/bar/statusbar.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 21 Apr 2014 4:19 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_BAR_STATUSBAR_H
#define NEGERNS_GUI_BAR_STATUSBAR_H

#include <negerns/gui/bar/bar.h>

namespace negerns {
namespace gui {
namespace bar {

//! Status bar.
class NEGERNS_DECL StatusBar : public Bar
{
public:
    StatusBar() : Bar() { }
    virtual ~StatusBar() { }
};

} //_ namespace bar
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_BAR_STATUSBAR_H
