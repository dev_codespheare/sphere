/*******************************************************************************
 * File:    negerns/gui/bar/bar.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 10 Apr 2013 1:37 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_GUI_BAR_BAR_H
#define NEGERNS_GUI_BAR_BAR_H

#include <map>
#include <negerns/core/defs.h>
#include <negerns/core/string.h>
#include <negerns/gui/icontrol.h>
#include <negerns/gui/auitoolbar.h>

namespace negerns {
namespace gui {
namespace bar {

//! %Base class of all toolbars or quickbars.
class NEGERNS_DECL Bar : public negerns::gui::IControl
{
public:
    //! Bar Border enumeration.
    enum Border { None, Top, Left, Bottom, Right, All };

    Bar();
    virtual ~Bar();

    //! @{
    //! \name IControl Public Virtual Functions

    //! Create the toolbar.
    //! This is called from Component::CreateControl.
    //! The parent must have been set prior to this call.
    //!
    //! \see Component::CreateControl
    //! \see Bar::SetParent
    virtual void CreateControl() override;

    virtual void PostCreateControl() override;

    virtual void DestroyControl() override;

    //! \copydoc IControl::RefreshControl
    virtual void RefreshControl() override;

    //! \copydoc IControl::ClearContent
    virtual void ClearContent() override;

    //! \copydoc IControl::RefreshContent
    virtual void RefreshContent() override;

    //! \copydoc IControl::ApplyConfiguration
    virtual void ApplyConfiguration() override;

    //! \copydoc IControl::ApplyTheme
    virtual void ApplyTheme() override;

    //! \copydoc IControl::HasFocus
    virtual bool HasFocus() const override { return false; }

    //! copydoc IControl::SetFocus
    virtual bool SetFocus(const std::string&) override { return false; }

    //! @}

    virtual bool Show(bool status = true) final;
    virtual void Hide(void) final;
    virtual bool IsVisible(void) const final;

    virtual wxAuiToolBarItem* AddTool(int toolId,
        const std::string& label,
        const std::string& icon,
        const std::string& shortHelpString = std::string(),
        wxItemKind kind = wxITEM_NORMAL) final;

    virtual wxAuiToolBarItem* AddLabel(int toolId,
        const std::string& label = std::string(),
        const int width = -1) final;

    virtual wxAuiToolBarItem* AddControl(wxControl* control,
        const std::string& label = std::string()) final;

    virtual wxAuiToolBarItem* AddSeparator() final;

    virtual wxAuiToolBarItem* AddSpacer(int pixels) final;

    virtual wxAuiToolBarItem* AddStretchSpacer(int proportion = Proportion::Dynamic) final;

    virtual void EnableTool(int toolId, bool state = true) final;

    virtual void SetDisabledToolShortHelp(int toolId, const std::string &s) final;

    virtual bool Realize() final;

    template <typename EventTag, typename Class, typename EventArg, typename EventHandler>
    void Bind(const EventTag &eventType,
        void (Class::*method)(EventArg &),
        EventHandler *handler,
        int winid = wxID_ANY)
    {
        wxASSERT_MSG(toolbar, "Toolbar has not been created.");
        toolbar->Bind(eventType, method, handler, winid);
    }

    template <typename Class, typename EventArg, typename EventHandler>
    void Bind(void (Class::*method)(EventArg &),
        EventHandler *handler,
        int winid = wxID_ANY)
    {
        wxASSERT_MSG(toolbar, "Toolbar has not been created.");
        toolbar->Bind(wxEVT_TOOL, method, handler, winid);
    }

    //! Set the parent object or container.
    //! The parent is set in Component::CreateControl.
    virtual void SetParent(wxWindow*) final;

    //! Return the reference to the parent object.
    virtual wxWindow* GetParent() const final;

    virtual void SetBorder(Bar::Border b) final;

    virtual void SetStyle(long s) final;

    //! Return the toolbar control.
    virtual AuiToolBar* GetControl() final;

protected:

    //! @{
    //! \name IControl Public Virtual functions

    virtual bool OnInitContent() override;

    //! @}

private:

    //! Parent control.
    //!
    //! This is not set or initialized in this class.
    //! It is intended for subclasses to do this. Component::CreateControl
    //! automatically initializes this reference.
    //!
    //! \see Component::CreateControl
    wxWindow* parent;

    //! ToolBar control.
    AuiToolBar* toolbar;

    Bar::Border border;

    long style;

    std::map<int, std::string> enabledToolHelpString;
    std::map<int, std::string> disabledToolHelpString;
};



inline void Bar::SetParent(wxWindow* window)
{
    parent = window;
}

inline wxWindow* Bar::GetParent() const
{
    return parent;
}

inline void Bar::SetBorder(Bar::Border b)
{
    border = b;
}

inline void Bar::SetStyle(long s)
{
    style = s;
}

inline AuiToolBar* Bar::GetControl()
{
    return toolbar;
}

} //_ namespace bar
} //_ namespace gui
} //_ namespace negerns

#endif //_ NEGERNS_GUI_BAR_BAR_H
