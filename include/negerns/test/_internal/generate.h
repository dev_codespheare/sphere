//  Copyright (c) 2001-2010 Hartmut Kaiser
//
//  Distributed under the Boost Software License, Version 1.0. (See accompanying
//  file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

//  http://www.boost.org/doc/libs/1_52_0/libs/spirit/optimization/karma/int_generator.cpp

#include <climits>
#include <cstdlib>

#include <iostream>
#include <sstream>
#include <boost/format.hpp>

//#include "../high_resolution_timer.hpp"

//  This value specifies, how to unroll the integer string generation loop in
//  Karma.
//      Set this to some integer in between 0 (no unrolling) and max expected
//      integer string len (complete unrolling). If not specified, this value
//      defaults to 6.
#define BOOST_KARMA_NUMERICS_LOOP_UNROLL 6

#include <boost/spirit/include/karma.hpp>

using namespace std;
using namespace boost::spirit;

#define MAX_ITERATION 10000000

struct random_fill
{
    int operator()() const
    {
        int scale = std::rand() / 100 + 1;
        return (std::rand() * std::rand()) / scale;
    }
};

// Usage: Randomly fill the vector
//
// std::srand(0);
// std::vector<int> v (MAX_ITERATION);
// std::generate(v.begin(), v.end(), random_fill());
