#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/timer.h>
#include <negerns/core/defs.h>
#include <negerns/core/string.h>

#include <negerns/negerns.h>

#include <boost/test/unit_test.hpp>
#include <boost/test/detail/unit_test_parameters.hpp>

using namespace boost::unit_test;

//! Exits the Boost test case function if the test application is currently
//! running within a debugger. This can be used to disable running other test
//! cases not intended to be debugged.
//!
//! Intentionally without a semicolon after the return statement. This is so
//! the macro is called in the client code with a semicolon after it.
#define LEAVE_TEST_IF_DEBUGGING()   if (::n::is_debugging()) return

#define EXCLUDE_IN_DEBUG_BUILD(b)   if ((!!(b)) && ::n::is_debug()) return

#define MESSAGE(M)                                      \
    if (runtime_config::log_level() <= log_messages) {  \
        BOOST_MESSAGE("          " << M);               \
    }

#define WARN(P)             BOOST_WARN(P)
#define CHECK(P)            BOOST_CHECK(P)
#define REQUIRE(P)          BOOST_REQUIRE(P)

#define WARN_MSG(P, M)      BOOST_WARN_MESSAGE(P, M)
#define CHECK_MSG(P, M)     BOOST_CHECK_MESSAGE(P, M)
#define REQUIRE_MSG(P, M)   BOOST_REQUIRE_MESSAGE(P, M)

#define WARN_EQ(L, R)       BOOST_WARN_EQUAL(L, R)
#define CHECK_EQ(L, R)      BOOST_CHECK_EQUAL(L, R)
#define REQUIRE_EQ(L, R)    BOOST_REQUIRE_EQUAL(L, R)

#define WARN_NEQ(L, R)      BOOST_WARN_NE(L, R)
#define CHECK_NEQ(L, R)     BOOST_CHECK_NE(L, R)
#define REQUIRE_NEQ(L, R)   BOOST_REQUIRE_NE(L, R)

#define WARN_LT(L, R)       BOOST_WARN_LT(L, R)
#define CHECK_LT(L, R)      BOOST_CHECK_LT(L, R)
#define REQUIRE_LT(L, R)    BOOST_REQUIRE_LT(L, R)

#define WARN_LE(L, R)       BOOST_WARN_LE(L, R)
#define CHECK_LE(L, R)      BOOST_CHECK_LE(L, R)
#define REQUIRE_LE(L, R)    BOOST_REQUIRE_LE(L, R)

#define WARN_GT(L, R)       BOOST_WARN_GT(L, R)
#define CHECK_GT(L, R)      BOOST_CHECK_GT(L, R)
#define REQUIRE_GT(L, R)    BOOST_REQUIRE_GT(L, R)

#define WARN_GE(L, R)       BOOST_WARN_GE(L, R)
#define CHECK_GE(L, R)      BOOST_CHECK_GE(L, R)
#define REQUIRE_GE(L, R)    BOOST_REQUIRE_GE(L, R)

//! Do not execute the rest of the test code.
//! This is much better than wrapping test cases within an #if/#endif.
#define SKIP_TEST()                 \
    do {                            \
        MESSAGE("------------");    \
        MESSAGE("Test skipped");    \
        MESSAGE("------------");    \
        return;                     \
    } while (false)

void set_assert_setting(
    n::assert::Status = n::assert::Status::Enable,
    n::assert::Output = n::assert::Output::Console);
void set_log_setting(
    n::log::Status = n::log::Status::Disable,
    n::log::tracing::Status = n::log::tracing::Status::Disable);
