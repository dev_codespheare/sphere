#ifndef NEGERNS_TEST_DB_BACKEND_ODBC_SQL_H
#define NEGERNS_TEST_DB_BACKEND_ODBC_SQL_H

#include <string>
#include <negerns/db/connection/pool.h>
#include <negerns/db/backend/odbc/sql.h>
#include <negerns/db/backend/odbc/backend.h>

namespace negerns {
namespace test {
namespace db {
namespace backend {
namespace odbc {

class Sql : public negerns::db::backend::odbc::Sql
{
public:
    Sql(negerns::db::connection::Pool *,
        std::size_t,
        negerns::db::backend::IBackend *);
    virtual ~Sql();

    virtual std::string transform(const std::string &) override;
};

} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace test
} //_ namespace negerns

#endif //_ NEGERNS_TEST_DB_BACKEND_ODBC_SQL_H
