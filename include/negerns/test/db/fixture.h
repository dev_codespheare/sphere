#include <string>
#include <negerns/db/connection/parameters.h>

namespace negerns {
namespace test {
namespace db {

class Fixture {
public:
    static negerns::db::connection::Parameters get_connection_parameters(bool = true);
    static std::string get_create_table();
    static std::string get_drop_table();
    static std::string get_insert_row();
    //! Experimental use only.
    //! The following functions are used only during test case development.
    //! After the test cases are finalized, these functions must be replaced
    //! by the actual test functions above.
    static std::string get_create_table_x();
    static std::string get_drop_table_x();
}; //_ class Fixture

} //_ namespace db
} //_ namespace test
} //_ namespace negerns

negerns::db::connection::Parameters get_connection_parameters(bool = true);
