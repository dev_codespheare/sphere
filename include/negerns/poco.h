/*******************************************************************************
 * File:    negerns/poco.h
 * Created: 27 Aug 2013 9:33 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment: Declare new names for the Poco namespaces
 ******************************************************************************/

// Declare the POCO library namespaces so we can
// create aliases for them below.
namespace Poco {
    namespace Dynamic { }
    namespace JSON { }
    namespace Net { }
    namespace Util { }
    namespace XML { }
    namespace Data {
        namespace Keywords { }
        namespace SQLite { }
        namespace ODBC { }
    }
}

// Aliases
namespace poco = ::Poco;
namespace Poco {
    namespace dyn = Dynamic;
    namespace json = JSON;
    namespace net = Net;
    namespace util = Util;
    namespace xml = XML;
    namespace data = Data;
    namespace Data {
        namespace keywords = Keywords;
        namespace sqlite = SQLite;
        namespace odbc = ODBC;
    }
}
