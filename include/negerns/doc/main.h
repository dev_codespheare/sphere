/*******************************************************************************
 * File:    doc/system_overview.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 23 Apr 2013 4:17 AM
 *
 * Comment: Negerns Library Overview
 ******************************************************************************/

/*!

\mainpage Negerns C++ Library Documentation

The is the Negerns C++ Library for developing native cross-platform applications.

<b>Version</b>      0.1.0 \n
<b>Author</b>       Ricky Maicle \n
<b>Date</b>         2013-2014 \n
<b>Copyright</b>    2013-2014 Ricky Maicle. All rights reserved. \n

\section user_manual User Manual

\li \subpage page_introduction
\li \subpage page_programming_guide
\li \subpage page_categories


\section reference_manual Reference Manual

\li \subpage page_overview
\li \subpage page_configuration
\li \subpage page_logging
\li \subpage page_data_access
\li \subpage page_user_interface

*/ // end of documentation
