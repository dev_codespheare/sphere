/*******************************************************************************
 * File:    negerns/db/backend/isql.h
 * Created: 22 Mar 2015 1:52 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_BACKEND_ISQL_H
#define NEGERNS_DB_BACKEND_ISQL_H

#include <string>
#include <memory>

#include <negerns/db/types.h>
#include <negerns/db/columns.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {

namespace connection {
class Pool;
} //_ namespace connection

namespace backend {

class IBackend;

//! Base class of all concrete Sql classes.
class NEGERNS_DECL ISql
{
public:
    ISql();
    ISql(connection::Pool *, std::size_t, backend::IBackend *);
    virtual ~ISql();

    //! Rewrite any named parameters into database/driver-specifics.
    //! Example: To ODBC numbers (:param -> $1)
    virtual std::string transform(const std::string &) = 0;

    //! Prepare the SQL statement and an optional flag whether to call the describe
    //! function. The default is to call the describe function. Returns true if the
    //! operation was successful.
    virtual bool prepare(const std::string &, bool = true) = 0;

    virtual bool describe() = 0;

    virtual ExecuteStatus execute(unsigned int) = 0;

    //! Fetch the next set of rows.
    virtual std::size_t fetch() = 0;

    virtual std::string get_native_sql() const = 0;

    std::string get_sql() const;
    void set_sql(const std::string &);

    virtual const Columns get_columns() const final;

    virtual std::size_t get_column_count() const final;

    virtual column::Properties get_column(std::size_t) const final;

    virtual column::Properties get_column(const std::string &) const final;

    virtual std::pair<bool, std::size_t> get_index(const std::string &) const final;

    virtual std::size_t get_affected_rows() const final;

    virtual std::size_t get_fetched_rows() const final;

protected:
    virtual void set_affected_rows(std::size_t n = 0) final;

    virtual void set_fetched_rows(std::size_t n) final;

protected:
    Columns columns;

private:
    std::string sql;
    std::size_t affected_rows = { 0 };
    std::size_t fetched_rows = { 0 };

}; //_ class ISql



inline
std::string ISql::get_sql() const
{
    return sql;
}

inline
void ISql::set_sql(const std::string &s)
{
    sql = s;
}

inline
std::size_t ISql::get_affected_rows() const
{
    return affected_rows;
}

inline
std::size_t ISql::get_fetched_rows() const
{
    return fetched_rows;
}

inline
const Columns ISql::get_columns() const
{
    return columns;
}

inline
std::size_t ISql::get_column_count() const
{
    return columns.count();
}

inline
column::Properties ISql::get_column(std::size_t i) const
{
    return columns(i);
}

inline
column::Properties ISql::get_column(const std::string &s) const
{
    return columns(s);
}

inline
std::pair<bool, std::size_t> ISql::get_index(const std::string &s) const
{
    return columns.get_index(s);
}

inline
void ISql::set_affected_rows(std::size_t n) {
    affected_rows = n;
}

inline
void ISql::set_fetched_rows(std::size_t n) {
    fetched_rows = n;
}

} //_ namespace backend
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_BACKEND_ISQL_H
