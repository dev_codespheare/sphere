/*******************************************************************************
 * File:    negerns/db/backend/ibackend.h
 * Created: 26 Feb 2014 12:59 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_BACKEND_IBACKEND_H
#define NEGERNS_DB_BACKEND_IBACKEND_H

#include <memory>
#include <negerns/db/types.h>
#include <negerns/db/connection/source.h>
#include <negerns/db/connection/parameters.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace backend {

enum class CloseAction
{
    None,
    Commit,
    Rollback
};

//! Interface for database backend operations.
//!
//! \todo
//! Move unrelated operations to BackendBase class.
//! This class should be an interface-only class which should be
//! inherited also by the Connection class to share common interface
//! with other backend specific classes.
class NEGERNS_DECL IBackend
{
public:
    IBackend();
    IBackend(const IBackend &);
    IBackend(IBackend &&);
    virtual ~IBackend();

    IBackend & operator = (const IBackend &);
    IBackend & operator = (IBackend &&);

    virtual negerns::db::Error open(const connection::Parameters &) = 0;

    //! Close the database connection.
    //!
    //! The argument provides mechanism on what to do with incomplete transactions.
    virtual bool close(CloseAction = CloseAction::None) = 0;
    virtual negerns::db::Error reconnect() = 0;
    virtual bool is_connected() const = 0;

    virtual void begin() = 0;
    virtual void commit() = 0;
    virtual void rollback() = 0;

#if 0
    // Functions for dealing with sequence/auto-increment values.

    // If true is returned, value is filled with the next value from the given
    // sequence. Otherwise either the sequence is invalid (doesn't exist) or
    // the current backend doesn't support sequences. If you use sequences for
    // automatically generating primary key values, you should use
    // get_last_insert_id() after the insertion in this case.
    bool get_next_sequence_value(std::string const & sequence, long & value);

    // If true is returned, value is filled with the last auto-generated value
    // for this table (although some backends ignore the table argument and
    // return the last value auto-generated in this session).
    bool get_last_insert_id(std::string const & table, long & value);
#endif

    //! Acquire the backend Source type.
    virtual connection::Source get_source() const final;
    virtual connection::Parameters get_parameters() const final;

    //! Acquire the backend Source type.
    //! If the Source is ODBC then this will return the actual database
    //! backend Source type. Otherwise, it returns what get_source returns.
    virtual connection::Source get_product() const;

protected:
    IBackend(connection::Source);
    virtual void set_parameters(const connection::Parameters &) final;

    //! Set close routine flag.
    void set_closed(bool b) { closed = b; }

    //! Returns true if the close routine has been processed.
    bool is_closed() const { return closed; }

private:
    connection::Source source = { db::connection::Source::None };
    connection::Parameters parameters = { connection::Parameters() };

private:
    //! Whether the close routine has already been processed.
    //!
    //! To support automatic cleanup without calling the close() function from the client
    //! code and to support C++ smart pointers, this must be checked in the destructor.
    //! If the value is false (not closed yet) then the destructor needs to call the
    //! cleanup routine.
    //!
    //! \see set_closed()
    //! \see is_closed()
    bool closed = { false };

}; //_ class IBackend



inline
connection::Source IBackend::get_source() const
{
    return source;
}

inline
connection::Parameters IBackend::get_parameters() const
{
    return parameters;
}

inline
connection::Source IBackend::get_product() const
{
    return source;
}

inline
void IBackend::set_parameters(const connection::Parameters &params)
{
    parameters = params;
}

} //_ namespace backend
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_BACKEND_IBACKEND_H
