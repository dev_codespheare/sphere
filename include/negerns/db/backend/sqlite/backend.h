/*******************************************************************************
 * File:    negerns/db/backend/sqlite/backend.h
 * Created: 23 Feb 2014 8:33 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_BACKEND_SQLITE_BACKEND_H
#define NEGERNS_DB_BACKEND_SQLITE_BACKEND_H

#include <negerns/db/backend/ibackend.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace backend {
namespace sqlite {

class NEGERNS_DECL Backend : public negerns::db::backend::IBackend
{
public:
    Backend();
    Backend(const Backend &) = default;
    Backend(Backend &&);
    virtual ~Backend();

    Backend & operator = (Backend &&);

    virtual negerns::db::Error open(const connection::Parameters &) override;
    virtual bool close(CloseAction = CloseAction::None) override;
    virtual negerns::db::Error reconnect() override;
    virtual bool is_connected() const override;

    virtual void begin() override;
    virtual void commit() override;
    virtual void rollback() override;

private:
    Backend & swap(Backend &rhs);
};



inline
Backend & Backend::operator = (Backend &&rhs)
{
    rhs.swap(*this);
    Backend().swap(rhs);
    return *this;
}

inline
Backend & Backend::swap(Backend &)
{
    //std::swap(parameters, rhs.parameters);
    return *this;
}

} //_ namespace sqlite
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_BACKEND_SQLITE_BACKEND_H
