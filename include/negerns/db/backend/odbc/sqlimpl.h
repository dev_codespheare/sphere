/*******************************************************************************
 * File:    negerns/db/backend/odbc/sql.h
 * Created: 08 Feb 2015 12:53 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_BACKEND_ODBC_SQLIMPL_H
#define NEGERNS_DB_BACKEND_ODBC_SQLIMPL_H

#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/db/row.h>
#include <negerns/db/connection/pool.h>
#include <negerns/db/backend/odbc/buffer.h>
#include <negerns/db/backend/odbc/sql.h>
#include <negerns/db/backend/odbc/handles.h>
#include <negerns/db/backend/odbc/environment.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {

//! ODBC SQL class implementation.
//!
//! The implementation is separated from the actual Sql class to avoid
//! internal header dependencies creeping into the client code.
//!
//! \todo some drivers don't support 64-bit numbers; use strings
class NEGERNS_DECL Sql::Implementation : public Statement
{
public:
    //! Constructor.
    Implementation(Sql *, connection::Pool *, std::size_t, backend::IBackend *);
    virtual ~Implementation();

    //! Rewrite any named parameters into ODBC numbers (:param -> $1)
    std::string transform(const std::string &);

    bool prepare(const std::string &, bool = true);

    //! Returns true if the SQL statement returns at least 1 result set column.
    //!
    //! Calling describe() on DDL statements will return false because DDL
    //! Statements will not have any result set column.
    bool describe();

    ExecuteStatus execute(unsigned int);

    //! Bind columns to buffers.
    void bind();

    std::size_t fetch();
    void post_fetch();

    negerns::Vars get_row_values() const { return data; }

    std::string get_native_sql() const;

    SQLHSTMT get_statement_handle();

private:
    bool is_select_statement() const;
    //! Returns true if a result set available after a call to execute().
    bool is_resultset_available(SQLHSTMT) const;
    //! Set the number of affected rows after a call to execute().
    void set_affected_rows(SQLHSTMT);

private:
    //! Pointer to the Sql instance.
    //! Allows access to the Sql class specific functionality.
    //! Note that this requires a friend declaration in the Sql class.
    //!
    //! NOTE: Just a reference. Not destroyed here.
    Sql *sql = { nullptr };

    //! Pointer to the connection \c Pool instance.
    //! Used when setting a connection \c Pool element status flag.
    //!
    //! NOTE: Just a reference. Not destroyed here.
    connection::Pool *pool { nullptr };

    //! Index of the backend instance in the connection \c Pool.
    //!
    //! Used when setting a connection \c Pool element status flag.
    //!
    //! \SEE Pool::free()
    std::size_t poolIndex = 0;

    //! Pointer to a backend instance in the connection \c Pool
    //!
    //! NOTE: Just a reference. Not destroyed here.
    Backend *backend = { nullptr };

    //. TODO: Named bindings
    //. Maintain a list of names for name binding.
    std::vector<std::string> names;

    Columns columns;

    //! Temporary data buffer.
    Buffer buffer;

    Vars data;

}; //_ class Sql::Implementation

} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_BACKEND_ODBC_SQLIMPL_H
