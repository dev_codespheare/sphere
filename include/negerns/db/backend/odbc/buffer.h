/*******************************************************************************
 * File:    negerns/db/backend/odbc/buffer.h
 * Created:
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_BACKEND_ODBC_SQL_BUFFER_H
#define NEGERNS_DB_BACKEND_ODBC_SQL_BUFFER_H

#include <vector>
#include <sql.h>
#include <sqlext.h>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {

//! Temporary buffer structure where data is stored on SQLFetch.
struct NEGERNS_DECL BufferData final
{
    union {
        char*               char_;
        SQL_NUMERIC_STRUCT* numeric_;
        int32_t             int_;
        uint32_t            uint_;
        double              double_;
    };

    //! Buffer data types used to determine how to free the resource.
    enum class Type {
        String,
        Numeric,
        Int,
        UInt,
        Double,
        Others
    } type = { Type::Others }; //_ enum class Type
    SQLLEN length = 0;

    // Constructors initialize the internal data.
    BufferData(char *v)                 : char_(v),     type(Type::String) { }
    BufferData(SQL_NUMERIC_STRUCT *v)   : numeric_(v),  type(Type::Numeric) { }
    BufferData(int v)                   : int_(v),      type(Type::Int) { }
    BufferData(unsigned int v)          : uint_(v),     type(Type::UInt) { }
    BufferData(double v)                : double_(v),   type(Type::Double) { }

    virtual ~BufferData();

}; //_ struct BufferData



//! Encapsulates temporary buffer management.
class NEGERNS_DECL Buffer
{
public:
    Buffer();
    Buffer(size_t);
    ~Buffer();

    void set_statement_handle(SQLHSTMT);

    //! Initialize the buffer with the specified size.
    void init(std::size_t);

    //! Clear the contents of the buffer.
    void clear();

    SQLRETURN add(SQLSMALLINT, SQLLEN);

    char*               get_char(size_t i) const    { return data[i].char_; }
    SQL_NUMERIC_STRUCT* get_numeric(size_t i) const { return data[i].numeric_; }
    int32_t             get_int(size_t i) const     { return data[i].int_; }
    uint32_t            get_uint(size_t i) const    { return data[i].uint_; }
    double              get_double(size_t i) const  { return data[i].double_; }

    std::size_t size() const;

    //! Buffer contents.
    std::vector<BufferData> data;

    //! Pointers to a Buffer content.
    std::vector<SQLPOINTER> ptrs;

private:
    //! ODBC statement handle.
    //!
    //! NOTE: Just a reference. Not destroyed here.
    SQLHSTMT stmt = { nullptr };

    //! For row descriptor when binding temporary buffer to SQL_NUMERIC_STRUCT.
    SQLHDESC hdesc = nullptr;

}; //_ class Buffer



inline
void Buffer::set_statement_handle(SQLHSTMT s)
{
    stmt = s;
}

inline
std::size_t Buffer::size() const
{
    return data.size();
}

} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_BACKEND_ODBC_SQL_BUFFER_H
