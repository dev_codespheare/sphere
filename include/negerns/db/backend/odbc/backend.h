/*******************************************************************************
 * File:    negerns/db/backend/odbc/backend.h
 * Created: 23 Feb 2014 8:33 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_BACKEND_ODBC_BACKEND_H
#define NEGERNS_DB_BACKEND_ODBC_BACKEND_H

#include <string>
#include <memory>
#include <sqlext.h>

#include <negerns/db/connection/parameters.h>
#include <negerns/db/backend/ibackend.h>
#include <negerns/db/backend/odbc/environment.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {

struct DriverInfo
{
#if 0
    SQL_ACTIVE_ENVIRONMENTS
    SQL_ASYNC_DBC_FUNCTIONS
    SQL_ASYNC_MODE
    SQL_ASYNC_NOTIFICATION
    SQL_BATCH_ROW_COUNT
    SQL_BATCH_SUPPORT
    SQL_DATA_SOURCE_NAME
    SQL_DRIVER_AWARE_POOLING_SUPPORTED
    SQL_DRIVER_HDBC
    SQL_DRIVER_HDESC
    SQL_DRIVER_HENV
    SQL_DRIVER_HLIB
    SQL_DRIVER_HSTMT
    SQL_DRIVER_NAME
    SQL_DRIVER_ODBC_VER
    SQL_DRIVER_VER
    SQL_DYNAMIC_CURSOR_ATTRIBUTES1
    SQL_DYNAMIC_CURSOR_ATTRIBUTES2
    SQL_FILE_USAGE
    SQL_FORWARD_ONLY_CURSOR_ATTRIBUTES1
    SQL_FORWARD_ONLY_CURSOR_ATTRIBUTES2
    SQL_GETDATA_EXTENSIONS
    SQL_INFO_SCHEMA_VIEWS
    SQL_KEYSET_CURSOR_ATTRIBUTES1
    SQL_KEYSET_CURSOR_ATTRIBUTES2
    SQL_MAX_ASYNC_CONCURRENT_STATEMENTS
    SQL_MAX_CONCURRENT_ACTIVITIES
    SQL_MAX_DRIVER_CONNECTIONS
    SQL_ODBC_INTERFACE_CONFORMANCE
    SQL_ODBC_STANDARD_CLI_CONFORMANCE
    SQL_ODBC_VER
    SQL_PARAM_ARRAY_ROW_COUNTS
    SQL_PARAM_ARRAY_SELECTS
    SQL_ROW_UPDATES
    SQL_SEARCH_PATTERN_ESCAPE
    SQL_SERVER_NAME
    SQL_STATIC_CURSOR_ATTRIBUTES1
    SQL_STATIC_CURSOR_ATTRIBUTES2
#endif
};

struct DataSourceInfo
{
#if 0
    SQL_ACCESSIBLE_PROCEDURES
    SQL_ACCESSIBLE_TABLES
    SQL_BOOKMARK_PERSISTENCE
    SQL_CATALOG_TERM
    SQL_COLLATION_SEQ
    SQL_CONCAT_NULL_BEHAVIOR
    SQL_CURSOR_COMMIT_BEHAVIOR
    SQL_CURSOR_ROLLBACK_BEHAVIOR
    SQL_CURSOR_SENSITIVITY
    SQL_DATA_SOURCE_READ_ONLY
    SQL_DEFAULT_TXN_ISOLATION
    SQL_MULTIPLE_ACTIVE_TXN
    SQL_MULT_RESULT_SETS
    SQL_NEED_LONG_DATA_LEN
    SQL_NULL_COLLATION
    SQL_PROCEDURE_TERM
    SQL_SCHEMA_TERM
    SQL_SCROLL_OPTIONS
    SQL_TABLE_TERM
    SQL_TXN_CAPABLE
    SQL_TXN_ISOLATION_OPTION
    SQL_USER_NAME
#endif
};

struct SupportedSQL
{
#if 0
    SQL_AGGREGATE_FUNCTIONS
    SQL_ALTER_DOMAIN
    SQL_ALTER_SCHEMA
    SQL_ALTER_TABLE
    SQL_ANSI_SQL_DATETIME_LITERALS
    SQL_CATALOG_LOCATION
    SQL_CATALOG_NAME
    SQL_CATALOG_NAME_SEPARATOR
    SQL_CATALOG_USAGE
    SQL_COLUMN_ALIAS
    SQL_CORRELATION_NAME
    SQL_CREATE_ASSERTION
    SQL_CREATE_CHARACTER_SET
    SQL_CREATE_COLLATION
    SQL_CREATE_DOMAIN
    SQL_CREATE_SCHEMA
    SQL_CREATE_TABLE
    SQL_CREATE_TRANSLATION
    SQL_DDL_INDEX
    SQL_DROP_ASSERTION
    SQL_DROP_CHARACTER_SET
    SQL_DROP_COLLATION
    SQL_DROP_DOMAIN
    SQL_DROP_SCHEMA
    SQL_DROP_TABLE
    SQL_DROP_TRANSLATION
    SQL_DROP_VIEW
    SQL_EXPRESSIONS_IN_ORDERBY
    SQL_GROUP_BY
    SQL_IDENTIFIER_CASE
    SQL_IDENTIFIER_QUOTE_CHAR
    SQL_INDEX_KEYWORDS
    SQL_INSERT_STATEMENT
    SQL_INTEGRITY
    SQL_KEYWORDS
    SQL_LIKE_ESCAPE_CLAUSE
    SQL_NON_NULLABLE_COLUMNS
    SQL_OJ_CAPABILITIES
    SQL_ORDER_BY_COLUMNS_IN_SELECT
    SQL_OUTER_JOINS
    SQL_PROCEDURES
    SQL_QUOTED_IDENTIFIER_CASE
    SQL_SCHEMA_USAGE
    SQL_SPECIAL_CHARACTERS
    SQL_SQL_CONFORMANCE
    SQL_SUBQUERIES
    SQL_UNION
#endif
};

struct SQLLimits {
#if 0
    SQL_MAX_BINARY_LITERAL_LEN
    SQL_MAX_CATALOG_NAME_LEN
    SQL_MAX_CHAR_LITERAL_LEN
    SQL_MAX_COLUMNS_IN_GROUP_BY
    SQL_MAX_COLUMNS_IN_INDEX
    SQL_MAX_COLUMNS_IN_ORDER_BY
    SQL_MAX_COLUMNS_IN_SELECT
    SQL_MAX_COLUMNS_IN_TABLE
    SQL_MAX_COLUMN_NAME_LEN
    SQL_MAX_CURSOR_NAME_LEN
    SQL_MAX_IDENTIFIER_LEN
    SQL_MAX_INDEX_SIZE
    SQL_MAX_PROCEDURE_NAME_LEN
    SQL_MAX_ROW_SIZE
    SQL_MAX_ROW_SIZE_INCLUDES_LONG
    SQL_MAX_SCHEMA_NAME_LEN
    SQL_MAX_STATEMENT_LEN
    SQL_MAX_TABLES_IN_SELECT
    SQL_MAX_TABLE_NAME_LEN
    SQL_MAX_USER_NAME_LEN
#endif
};

//! ODBC Backend class.
//!
//! Calls from the abstract Backend class is forwarded to this class.
class NEGERNS_DECL Backend :
    public Connection,
    public db::backend::IBackend
{
public:
    Backend();
    Backend(const Backend &);
    Backend(Backend &&);
    virtual ~Backend();

    Backend & operator = (const Backend &);
    Backend & operator = (Backend &&);

    //! Open a connection to a datasource.
    virtual db::Error open(const connection::Parameters &) override;

    //! Close the database connection.
    //!
    //! The argument provides mechanism on what to do with incomplete
    //! transactions.
    //!
    //! Frees all statements and descriptors that have been explicitly
    //! allocated on the connection.
    virtual bool close(CloseAction = CloseAction::None) override;
    virtual db::Error reconnect() override;
    virtual bool is_connected() const override;

    virtual void begin() override;
    virtual void commit() override;
    virtual void rollback() override;

    virtual db::connection::Source get_product() const override;

    std::string get_database_name() const;
    std::string get_database_version() const;
    std::string get_connection_string() const;

    //! Return the maximum number of active connections supported by the driver.
    std::size_t get_max_active_connections();

private:
    Backend & swap(Backend &rhs);

    db::Error open();
    void reset_transaction();

    //! Get general information about the driver and data source.
    std::string get_info(SQLUSMALLINT) const;

private:
    std::string connectionString;

    std::string databaseName;
    //! Type of backend connected to.
    connection::Source dbmsName = connection::Source::None;
    std::string dbmsVersion;

}; //_ class Backend



inline
Backend & Backend::operator = (Backend &&rhs)
{
    rhs.swap(*this);
    Backend().swap(rhs);
    return *this;
}

inline
db::connection::Source Backend::get_product() const
{
    return dbmsName;
}

inline
std::string Backend::get_database_name() const
{
    return databaseName;
}

inline
std::string Backend::get_database_version() const
{
    return dbmsVersion;
}

inline
std::string Backend::get_connection_string() const
{
    return connectionString;
}

inline
Backend & Backend::swap(Backend &rhs)
{
    std::swap(connectionString, rhs.connectionString);
    std::swap(databaseName, rhs.databaseName);
    std::swap(dbmsName, rhs.dbmsName);
    std::swap(dbmsVersion, rhs.dbmsVersion);
    return *this;
}

} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_BACKEND_ODBC_BACKEND_H
