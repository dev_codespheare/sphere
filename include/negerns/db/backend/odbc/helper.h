/*******************************************************************************
 * File:    negerns/db/backend/odbc/helper.h
 * Created: 04 Mar 2015 12:24 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_BACKEND_ODBC_HELPER_H
#define NEGERNS_DB_BACKEND_ODBC_HELPER_H

#include <utility>
#include <sql.h>

#include <negerns/db/column/properties.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {
namespace helper {

//! Determine the equivalent data type from the specified SQL type.
NEGERNS_DECL
column::Type to_column_type(const SQLSMALLINT t, bool isUnsigned);

NEGERNS_DECL
SQLSMALLINT to_sql_type(const column::Type);

NEGERNS_DECL
SQLSMALLINT to_c_type(const column::Type);

NEGERNS_DECL
bool is_identity(SQLHSTMT, const SQLUSMALLINT);

NEGERNS_DECL
bool is_unsigned(SQLHSTMT, const SQLUSMALLINT);

NEGERNS_DECL
column::Properties describe_column(SQLHSTMT,
    const SQLUSMALLINT,
    column::Properties);

NEGERNS_DECL
double to_double(const SQL_NUMERIC_STRUCT &);

} //_ namespace helper
} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_BACKEND_ODBC_HELPER_H