/*******************************************************************************
 * File:    negerns/db/backend/odbc/sql.h
 * Created: 08 Feb 2015 12:53 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_BACKEND_ODBC_SQL_H
#define NEGERNS_DB_BACKEND_ODBC_SQL_H

#include <memory>
#include <negerns/db/row.h>
#include <negerns/db/backend/odbc/handles.h>
#include <negerns/db/backend/isql.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {

namespace connection {
class Pool;
} //_ namespace connection

namespace backend {
namespace odbc {

const bool NoDescribe = false;

class IBackend;

class NEGERNS_DECL Sql : public ISql
{
public:
    Sql(negerns::db::connection::Pool *, std::size_t, backend::IBackend *);
    Sql(const Sql &) = delete;
    Sql(Sql &&);
    virtual ~Sql();

    Sql & operator = (const Sql &) = delete;
    Sql & operator = (Sql &&);

    virtual bool prepare(const std::string &, bool = true) override;

    virtual bool describe() override;

    virtual negerns::db::ExecuteStatus execute(unsigned int) override;

    virtual std::size_t fetch() override;

    virtual std::string get_native_sql() const override;

    SQLHSTMT get_statement_handle();

    negerns::Vars get_row_values() const;

protected:
    //! Rewrite any named parameters into ODBC numbers (:param -> $1)
    virtual std::string transform(const std::string &) override;

private:
    Sql & swap(Sql &);


private:
    class Implementation;
    typedef std::unique_ptr<Implementation> ImplementationPtr;
    ImplementationPtr pimpl;

}; //_ class Sql



// -----------------------------------------------------------------------------



//! Return the corresponding size of the specified type.
NEGERNS_DECL
std::size_t get_data_size(column::Type);

} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_BACKEND_ODBC_SQL_H
