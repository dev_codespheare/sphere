/*******************************************************************************
 * File:    negerns/db/backend/odbc/environment.h
 * Created: 13 Feb 2015 4:11 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_BACKEND_ODBC_ENVIRONMENT_H
#define NEGERNS_DB_BACKEND_ODBC_ENVIRONMENT_H

#include <string>
#include <memory>
#include <sql.h>
#include <sqlext.h>

#include <negerns/db/backend/odbc/handles.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {
namespace environment {

enum class Version {
    ODBCUnknown,
    ODBC380 = SQL_OV_ODBC3_80,
    ODBC300 = SQL_OV_ODBC3,
    ODBC200 = SQL_OV_ODBC2,
};

enum class ConnectionPooling {
    Off = SQL_CP_OFF,
    OnePerDriver = SQL_CP_ONE_PER_DRIVER,
    OnePerEnvironment = SQL_CP_ONE_PER_HENV,
    DriverAware = SQL_CP_DRIVER_AWARE
};

enum class ConnectionPoolingMatch {
    Strict = SQL_CP_STRICT_MATCH,
    Relaxed = SQL_CP_RELAXED_MATCH
};

NEGERNS_DECL
std::string get_odbc_version_string(Version);

NEGERNS_DECL
std::string connection_pooling_name(ConnectionPooling);

NEGERNS_DECL
std::string connection_pooling_match_name(ConnectionPoolingMatch);

//! Check if the singleton implementation instance is already created.
//! Create it if not.
NEGERNS_DECL
SQLHENV init();

//! Destroy the singleton implementation instance.
NEGERNS_DECL
void shutdown();

//! Returns true if the environment handle has been initialized.
NEGERNS_DECL
bool is_valid();

NEGERNS_DECL
Version get_odbc_version();

NEGERNS_DECL
ConnectionPooling get_connection_pooling();

NEGERNS_DECL
ConnectionPoolingMatch get_connection_pooling_match();

//! Return true if null-terminated string is supported.
NEGERNS_DECL
bool get_nts();

//! Return the environment handle.
NEGERNS_DECL
SQLHENV get();

NEGERNS_DECL
Environment * implementation(EnvironmentPtr = nullptr, bool = false);

} //_ namespace environment
} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_BACKEND_ODBC_ENVIRONMENT_H
