/*******************************************************************************
 * File:    negerns/db/backend/odbc/handles.h
 * Created: 30 Mar 2015 3:00 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_BACKEND_ODBC_HANDLES_H
#define NEGERNS_DB_BACKEND_ODBC_HANDLES_H

#include <string>
#include <memory>
#include <sql.h>
#include <sqlext.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {

//! Encapsulated ODBC Handle base class.
class NEGERNS_DECL Handle
{
public:
    Handle() { }
    Handle(SQLSMALLINT t) : type(t) { }
    Handle(SQLSMALLINT t, SQLHANDLE h) : handle(h), type(t) { }
    virtual ~Handle() { handle = NULL; }

    Handle(const Handle &) = default;
    //! Move constructor.
    Handle::Handle(Handle &&o) {
        handle = o.handle;
        type = o.type;
        o.handle = NULL;
    }

    Handle & operator = (const Handle &) = default;

    //! Move assignment.
    Handle & Handle::operator = (Handle &&rhs) {
        if (this != &rhs) {
            handle = rhs.handle;
            type = rhs.type;
            rhs.handle = NULL;
        }
        return *this;
    }

    //! Get the internal handle.
    virtual SQLHANDLE get() const final { return handle; }

    //! Get the type of handle.
    virtual SQLSMALLINT get_handle_type_id() const final { return type; }

protected:
    //! Set the value of the internal handle.
    virtual void set(SQLHANDLE h) final { handle = h; }

    //! Set internal handle to NULL.
    virtual void set_to_null() final { handle = NULL; }

    //! Returns true if the handle is not null.
    virtual bool is_valid() const final { return handle != NULL; }

private:
    //! ODBC handle.
    SQLHANDLE handle { NULL };
    //! ODBC handle type identifier.
    //!
    //! ODBC 3.0 defines these identifiers as follows:
    //!   SQL_HANDLE_ENV  1
    //!   SQL_HANDLE_DBC  2
    //!   SQL_HANDLE_STMT 3
    //!   SQL_HANDLE_DESC 4
    SQLSMALLINT type = SQL_NULL_HANDLE;

}; //_ class Handle



//! Environment Handle.
class NEGERNS_DECL Environment final : public Handle
{
public:
    Environment();
    virtual ~Environment();

}; //_ class Environment

//! DBC Handle.
class NEGERNS_DECL Connection : public Handle
{
public:
    Connection();
    Connection(SQLHDBC h);
    virtual ~Connection();
}; //_ class Connection

//! Statement Handle.
class NEGERNS_DECL Statement : public Handle
{
public:
    Statement();
    virtual ~Statement();
}; //_ class Statement



//! Environment smart pointer type.
typedef std::unique_ptr<Environment> EnvironmentPtr;

} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_BACKEND_ODBC_HANDLES_H
