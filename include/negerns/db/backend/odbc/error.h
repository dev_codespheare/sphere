/*******************************************************************************
 * File:    negerns/db/backend/odbc/error.h
 * Created: 06 Oct 2014 12:45 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_BACKEND_ODBC_ERROR_H
#define NEGERNS_DB_BACKEND_ODBC_ERROR_H

#include <string>

#include <boost/type_index.hpp>

#include <negerns/core/string/format.h>
#include <negerns/db/backend/odbc/handles.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {

inline
bool is_error(SQLRETURN rc) { return !SQL_SUCCEEDED(rc); }

inline
bool is_success(SQLRETURN rc) { return SQL_SUCCEEDED(rc); }

inline
bool is_success_with_info(SQLRETURN rc) { return rc == SQL_SUCCESS_WITH_INFO; }

inline
bool is_no_data(SQLRETURN rc) {
#if (ODBCVER >= 0x0300)
    return rc == SQL_NO_DATA;
#else
    return false;
#endif
}

template <class T>
std::string get_type_name(T) {
    return boost::typeindex::type_id<T>().pretty_name();
}

NEGERNS_DECL
void log_sqlreturn_impl(const char *file, int line, const char *func,
    SQLRETURN rc,
    const std::string &,
    const std::vector<::negerns::Var> &);

NEGERNS_DECL
void log_sqlreturn_impl(const char *file, int line, const char *func,
    SQLRETURN rc,
    const Handle &handle,
    const std::vector<::negerns::Var> &);

template <typename... A>
void log_sqlreturn(const char *file, int line, const char *func,
    SQLRETURN rc, const std::string &s, const A&...args)
{
    auto count = sizeof...(args);
    if (count == 0) {
        const std::vector<::negerns::Var> v;
        log_sqlreturn_impl(file, line, func, rc, s, v);
    } else {
        log_sqlreturn_impl(file, line, func, rc, s, {args...});
    }
}

template <typename... A>
void log_sqlreturn(const char *file, int line, const char *func,
    SQLRETURN rc, const Handle &handle, const A&...args)
{
    auto count = sizeof...(args);
    if (count == 0) {
        const std::vector<::negerns::Var> v;
        log_sqlreturn_impl(file, line, func, rc, handle, v);
    } else {
        log_sqlreturn_impl(file, line, func, rc, handle, {args...});
    }
}

#define LOG_SQLRETURN(rc, h_or_s, ...)              \
    do {                                            \
        using namespace ::negerns::db::backend;     \
        odbc::log_sqlreturn(__FILE__,               \
                __LINE__,                           \
                __FUNCTION__,                       \
                rc,                                 \
                h_or_s,                             \
                __VA_ARGS__);                       \
    } while (false)

} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_BACKEND_ODBC_ERROR_H
