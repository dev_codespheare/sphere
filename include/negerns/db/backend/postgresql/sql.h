/*******************************************************************************
 * File:    negerns/db/backend/postgresql/sql.h
 * Created: 22 Feb 2015 9:56 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_BACKEND_POSTGRESQL_SQL_H
#define NEGERNS_DB_BACKEND_POSTGRESQL_SQL_H

#include <negerns/db/backend/isql.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace backend {
namespace postgresql {

class NEGERNS_DECL Sql : public ISql
{
public:
    Sql();
    virtual ~Sql();
    //static void execute(const std::string &s);
}; //_ class Sql

} //_ namespace postgresql
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_BACKEND_POSTGRESQL_SQL_H
