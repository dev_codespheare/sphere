/*******************************************************************************
 * File:    negerns/db/pool.h
 * Created: 08 Oct 2014 11:26 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_POOL_H
#define NEGERNS_DB_POOL_H

#include <string>
#include <vector>
#include <memory>
#include <negerns/db/connection/pool.h>

#include <negerns/core/declspec.h>

//' NOTE: The get() functions exposes the class to be used in client code.
//' NOTE: I cannot find a good reason how to use the Connection class that the get()
//' functions return.
//'
//' The Pool class should just declare Sql and Connection classes as 'friends' and
//' make the get() functions return the Backend instance that the Pool maintains.
//' This makes sense since the Sql and Connection classes just forwards/delegates
//' the real work to the Backend instance.
//'
//' This allows the following usage(s):
//'
//' Sql sql;            // acquires a Backend instance
//' sql.execute(...);   // delegates to the Backend instance
//'
//' Connection conn;    // acquires a Backend instance
//' conn.whatever(...); // delegates to the Backend instance
//'
//' Sql::Sql() : backend(nullptr)
//' {
//'     auto backend_pool = Pool::get();
//'     this->backend = backend_pool->get();
//' }
//'
//' The same with the Connection object. One thing that comes to mind on what to do with
//' a Connection object is to manage the connection. Set/query options, status and other
//' information. Not anything related to SQL statements would be a possible candidate for
//' this class.
//'
//' Connection::get_vendor();
//' Connection::is_connected();
//' Connection::max_connections();
//' Connection::set_timeout();
//' ...
//'
//' NOTE: Multiple database connections are maintained in the Pool. Setting options
//' (/etc) only affects the current connection. Or there could be some that is global
//' (ODBC Environment).

// Do something with backend pointer...

namespace negerns {
namespace db {

//! Container of connection pools.
//!
//! \code
//! bool success = negerns::db::Pool::add(parameters, 1);
//! if (!success) {
//!     // Error checking
//! }
//! auto connPool = pool->get(0);
//! if (connPool == nullptr) {
//!     // Error checking
//! }
//! auto conn = connPool->get();
//! if (conn == nullptr) {
//!     // Error checking
//! }
//!
//! // Do something with the connection
//!
//! negerns::db::Pool::close();
//! \endcode
class NEGERNS_DECL Pool final
{
public:
    Pool() = delete;
    ~Pool() = delete;

    //! Add a connection pool object into the container.
    //! The connection pool object is created with the specified pool size.
    //!
    //! It does not check whether the specified connection parameters were already used
    //! to create a connection pool object. So, using the same connection parameters
    //! twice will create two unique connection pool objects.
    static bool add(const connection::Parameters &, std::size_t);

    //! Returns a shared connection pool object in the specified index.
    //! The index to the container must exist otherwise, it returns nullptr.
    static connection::PoolPtr get(std::size_t n);

    //! Return a shared connection pool object from the container at the index
    //! specified by the default index.
    static connection::PoolPtr get();

    //! Close all connections and clears the container.
    static void close();

    //! Return true if the container of connection pools is empty.
    static bool empty();

    //! Return the number of items in the container of connection pools.
    static std::size_t count();

    //! Set the default index.
    //!
    //! The default index is initially zero.
    static void set_default_index(std::size_t);

private:
    static std::vector<connection::PoolPtr> pool;
    static std::size_t defaultIndex;

}; //_ class Pool

} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_POOL_H
