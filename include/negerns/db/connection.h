/*******************************************************************************
 * File:    negerns/db/connection.h
 * Created: 23 Feb 2014 1:30 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_CONNECTION_H
#define NEGERNS_DB_CONNECTION_H

#include <memory>
#include <string>
#include <negerns/core/debug.h>
#include <negerns/db/types.h>
#include <negerns/db/connection/parameters.h>
#include <negerns/db/connection/odbc/driver.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {

namespace backend {
class IBackend;
}

class Sql;

//! Database connection.
class NEGERNS_DECL Connection final
{
public:
    //! Constructor.
    Connection();
    Connection(const Connection &);
    Connection(Connection &&);

    virtual ~Connection();

    Connection & operator = (const Connection &rhs);
    Connection & operator = (Connection &&);

    db::Error reconnect();

    //! Return true if currently connected to the database.
    bool is_connected() const;

    //! Return the pool index that the Connection object is in.
    std::size_t get_pool_index() const;

    //! Acquire the backend Source type.
    connection::Source get_source() const;

    //! Acquire connection parameters.
    connection::Parameters get_parameters() const;

    //! Acquire the backend Source type or the backend Source type that ODBC
    //! is connected to.
    //!
    //! If the Source is ODBC then this will return the actual database
    //! backend Source type. Otherwise, it returns what get_source returns.
    connection::Source get_product() const;

private:
    Connection & swap(Connection &rhs);

private:
    class Impl;
    std::unique_ptr<Impl> pimpl = { nullptr };

}; //_ class Connection

} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_CONNECTION_H
