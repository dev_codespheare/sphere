/*******************************************************************************
 * File:    negerns/db/sql.h
 * Created: 18 Mar 2015 9:46 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_SQL_H
#define NEGERNS_DB_SQL_H

#include <string>
#include <memory>
#include <negerns/db/backend/isql.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {

class NEGERNS_DECL Sql final
{
public:
    Sql();
    Sql(const std::string &);

    bool prepare(bool = true);

    bool describe();

    ExecuteStatus execute();

    const Columns get_columns() const;
    std::size_t get_column_count() const;
    column::Properties get_column(std::size_t) const;
    column::Properties get_column(const std::string &) const;
    std::pair<bool, std::size_t> get_index(const std::string &) const;
    std::size_t get_affected_rows() const;
    std::size_t get_fetched_rows() const;
    std::string get_sql() const;
    std::string get_native_sql() const;

private:
    //! Pointer to the concrete Sql instance.
    std::unique_ptr<backend::ISql> isql { nullptr };

}; //_ class Sql



typedef std::unique_ptr<Sql> SqlPtr;



// ----------------------------------------------------------------------------------------



inline
bool Sql::prepare(bool flag)
{
    WARNING(isql != nullptr);
    auto str(isql->get_sql());
    return isql->prepare(str, flag);
}

inline
bool Sql::describe()
{
    WARNING(isql != nullptr);
    return isql->describe();
}

inline
ExecuteStatus Sql::execute()
{
    WARNING(isql != nullptr);
    const std::size_t fetchFirstRow = 1;
    return isql->execute(fetchFirstRow);
}

inline
const Columns Sql::get_columns() const
{
    WARNING(isql != nullptr);
    return isql->get_columns();
}

inline
std::size_t Sql::get_column_count() const
{
    WARNING(isql != nullptr);
    return isql->get_column_count();
}

inline
column::Properties Sql::get_column(std::size_t i) const
{
    WARNING(isql != nullptr);
    return isql->get_column(i);
}

inline
column::Properties Sql::get_column(const std::string &s) const
{
    WARNING(isql != nullptr);
    return isql->get_column(s);
}

inline
std::pair<bool, std::size_t> Sql::get_index(const std::string &s) const
{
    WARNING(isql != nullptr);
    return isql->get_index(s);
}

inline
std::size_t Sql::get_affected_rows() const
{
    WARNING(isql != nullptr);
    return isql->get_affected_rows();
}

inline
std::size_t Sql::get_fetched_rows() const
{
    WARNING(isql != nullptr);
    return isql->get_fetched_rows();
}

inline
std::string Sql::get_sql() const
{
    WARNING(isql != nullptr);
    return isql->get_sql();
}

inline
std::string Sql::get_native_sql() const
{
    WARNING(isql != nullptr);
    return isql->get_native_sql();
}

} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_SQL_H
