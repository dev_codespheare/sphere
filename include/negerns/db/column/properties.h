/*******************************************************************************
 * File:    negerns/db/column/properties.h
 * Created: 11 Jan 2013 12:23 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_COLUMN_PROPERTIES_H
#define NEGERNS_DB_COLUMN_PROPERTIES_H

#include <string>
#include <map>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace column {

//! Data type enumeration.
enum class Type {
    Boolean,
    Int8,
    UInt8,
    Int16,
    UInt16,
    Int32,
    UInt32,
    Int64,
    UInt64,
    Float,
    Double,
    Real,
    Numeric,
    Decimal,
    Char,
    WChar,
    String,
    WString,
    Clob,
    WClob,
    Blob,
    Date,
    Time,
    Timestamp,
    TimeNTZ,
    TimeTZ,
    TimestampNTZ,
    TimestampTZ,
    Unknown
};

NEGERNS_DECL std::string type_name(Type);
NEGERNS_DECL std::string sql_type_name(Type);



//! Table column metadata.
class NEGERNS_DECL Properties
{
public:
    Properties();
    Properties(const Properties &);
    Properties(Properties &&);
    Properties(bool b) : valid(b) { }
    virtual ~Properties();

    Properties & operator = (const Properties &);
    Properties & operator = (Properties &&);

    void clear();
    bool is_valid() const;

    std::size_t position;
    std::string name;

    //. TODO: Keep local and SQL data types in sync.
    Type type;

    // SQL Data Types
    // https://msdn.microsoft.com/en-us/library/ms710150%28v=vs.85%29.aspx
    short sqlType;

    // The following union is for name aliasing purposes only.
    // The column length can either be referred to as length or precision
    // depending on the data type in reference.

    //! Total count of significant digits in both sides of the decimal point.
    //!
    //! Client may use length for string type(s) and precision for numeric types.
    union {
        std::size_t precision;
        std::size_t length;
    };
    //! Count of decimal digits in the fractional part, to the right of the decimal point.
    std::size_t scale;
    bool identity;
    bool nullable;
    bool is_unsigned;

private:
    //. TODO: Determine if valid state is needed.
    bool valid = false;

}; //_ class Properties

} //_ namespace column
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_COLUMN_PROPERTIES_H
