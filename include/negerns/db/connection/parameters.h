/*******************************************************************************
 * File:    negerns/db/connection/parameters.h
 * Created: 23 Feb 2014 12:12 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_CONNECTION_PARAMETERS_H
#define NEGERNS_DB_CONNECTION_PARAMETERS_H

#include <string>
#include <map>
#include <memory>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/db/connection/source.h>
#include <negerns/db/connection/options.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace connection {

//! Connection parameters.
class NEGERNS_DECL Parameters
{
public:
    Parameters();
    Parameters(const Parameters &);
    Parameters(Parameters &&);
    Parameters(Source, std::unique_ptr<Options>);
    virtual ~Parameters();

    Parameters & operator = (const Parameters &);
    Parameters & operator = (Parameters &&);

    void set_options(std::unique_ptr<Options>);
    Source get_source() const;

    //! Return the value of the specified string key.
    //!
    //! Made public only for testing.
    std::string get(const std::string &) const;

    bool exists(const std::string &) const;
    std::size_t count() const;
    bool empty() const;
    void clear();

    bool is_required_set() const;
    std::string get_connection_string();

private:
    Parameters & swap(Parameters &rhs);

private:
    Source source;
    std::unique_ptr<Options> options;

}; //_ class Parameters



inline
Parameters & Parameters::operator = (const Parameters &rhs)
{
    if (this != &rhs) {
        source = rhs.source;
        options = rhs.options->clone();
    }
    return *this;
}

inline
Parameters & Parameters::operator = (Parameters &&rhs)
{
    if (this != &rhs) {
        rhs.swap(*this);
        Parameters().swap(rhs);
    }
    return *this;
}

inline
void Parameters::set_options(std::unique_ptr<Options> o)
{
    options = std::move(o);
}

inline
Source Parameters::get_source() const
{
    return source;
}

inline
std::string Parameters::get(const std::string &key) const
{
    ASSERT(options != nullptr);
    return options->get(key);
}

inline
bool Parameters::exists(const std::string &key) const
{
    ASSERT(options != nullptr);
    return options->exists(key);
}

inline
std::size_t Parameters::count() const
{
    ASSERT(options != nullptr);
    return options->count();
}

inline
bool Parameters::empty() const
{
    ASSERT(options != nullptr);
    return options->empty();
}

inline
void Parameters::clear()
{
    if (options) {
        options->clear();
    }
}

inline
std::string Parameters::get_connection_string()
{
    ASSERT(options != nullptr);
    return options->get_connection_string();
}

inline
Parameters & Parameters::swap(Parameters &rhs)
{
    std::swap(source, rhs.source);
    std::swap(options, rhs.options);
    return *this;
}

} //_ namespace connection
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_CONNECTION_PARAMETERS_H
