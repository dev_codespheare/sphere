/*******************************************************************************
 * File:    negerns/db/connection/options.h
 * Created: 5 Mar 2015 3:25 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_CONNECTION_OPTIONS_H
#define NEGERNS_DB_CONNECTION_OPTIONS_H

#include <string>
#include <map>
#include <memory>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace connection {

class NEGERNS_DECL Options
{
public:
    Options();
    Options(const Options &);
    Options(Options &&);
    Options(const std::map<std::string, std::string> &);
    virtual ~Options();

    Options & operator = (const Options &);
    Options & operator = (Options &&);

    //! Make a clone.
    //!
    //! Subclasses must override this.
    virtual std::unique_ptr<Options> clone() const;

    //! Return the composed connection string.
    //!
    //! Subclasses must override this.
    virtual std::string get_connection_string();

    virtual void set(const std::string &, const std::string &) final;
    //! Return the value of the specified string key.
    //!
    //! Made public only for testing.
    virtual std::string get(const std::string &) const final;
    virtual bool exists(const std::string &) const final;
    virtual std::size_t count() const final;
    virtual bool empty() const;
    virtual void clear() final;

protected:

    //! Return the key/valu map.
    virtual std::map<std::string, std::string> get() const final;

private:
    Options & swap(Options &rhs);

private:
    //! Key and value.
    std::map<std::string, std::string> options;

}; //_ class Options



inline
void Options::set(const std::string &key, const std::string &value)
{
    if (!value.empty()) {
        options.emplace(key, value);
    }
}

inline
std::string Options::get(const std::string &key) const
{
    auto pos = options.find(key);
    return pos != options.end() ? pos->second : std::string();
}

inline
bool Options::exists(const std::string &key) const
{
    auto pos = options.find(key);
    return pos != options.end();
}

inline
std::size_t Options::count() const
{
    return options.size();
}

inline
bool Options::empty() const
{
    return options.empty();
}

inline
void Options::clear()
{
    options.clear();
}

inline
std::map<std::string, std::string> Options::get() const
{
    return options;
}

} //_ namespace connection
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_CONNECTION_OPTIONS_H
