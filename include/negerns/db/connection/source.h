/*******************************************************************************
 * File:    negerns/db/connection/source.h
 * Created: 23 Feb 2014 12:19 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_CONNECTION_SOURCE_H
#define NEGERNS_DB_CONNECTION_SOURCE_H

#include <string>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace connection {

//! Data source types.
enum class Source
{
    None = 0,
    ODBC,

    Firebird,
    MicrosoftSQLServer,
    MySQL,
    MariaDB,
    Oracle,
    PostgreSQL,
    SQLite,
    SybaseSQLAnwhere
};

//! Return the Source type string representation.
NEGERNS_DECL
std::string source_name(Source);

//! Return equivalent Source type.
NEGERNS_DECL
Source source_type(const std::string &);

NEGERNS_DECL
bool is_odbc(Source);

} //_ namespace connection
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_CONNECTION_SOURCE_H
