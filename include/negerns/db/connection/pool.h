/*******************************************************************************
 * File:    negerns/db/connection/pool.h
 * Created: 23 Feb 2014 2:44 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_CONNECTION_POOL_H
#define NEGERNS_DB_CONNECTION_POOL_H

#include <string>
#include <vector>
#include <memory>

#include <negerns/db/sql.h>
#include <negerns/db/connection/parameters.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace backend {
class IBackend;
} //_ namespace backend
namespace connection {

//. TODO: Set max number of items.
//. This is to let the client/developer know that a certain threshold is being reached.
//. This may need to be addressed if there are limitations to resources or just some
//. intentional application limitation.

//. TODO: Pooling types
//. http://www.enterprisedb.com/resources-community/tutorials-quickstarts/all-platforms/how-setup-pgbouncer-connection-pooling-postg

namespace pooling {
    enum class Type {
        None,
        ODBCPooling,
        LibraryPooling
    };
} //_namespace pooling

//! Connection Pool for a single data source.
class NEGERNS_DECL Pool final
{
public:
    typedef std::pair<backend::IBackend *, std::size_t> ItemRef;

    //! Default pool count is used to specify the number of pool items to be created
    //! if the client did not specify any.
    //!
    //! Pool count greater than 1 also means that the client requests connection
    //! pooling.
    //!
    //! A large application may use a large connection pool if connection pooling is
    //! not supported by the backend. By default we choose a large number to support
    //! the situation.
    static const std::size_t defaultPoolCount = 10;

    //! Return true if the specified connection parameters can successfully connect to
    //! the specified database.
    static bool test(const Parameters &);

    Pool() = delete;
    Pool(const Parameters &, std::size_t count = defaultPoolCount);
    virtual ~Pool();

#if 0
    //! Destroy and create the backend connections.
    //!
    //! If no argument is passed or if the argument is zero, the maximum number of pool
    //! items will remain as when the pool items were last initialized. Otherwise, the
    //! backend connections will be created with a new maximum number of pool items.
    void recreate(std::size_t = 0);
#endif

    //! Get a connection instance from the pool.
    //!
    //! Returns nullptr if there is no free item available in the pool item.
    Pool::ItemRef get();

    //! Mark the Connection Pool item as available for use.
    //!
    //! Caller: Connection::~Connection()
    void free(std::size_t);

    //! Close all connections.
    void close();

    //! Returns true if there are no pool items initialized.
    //!
    //! This is equivalent to the expression count() == 0.
    bool empty() const;

    //! Returns the total number of all pool items.
    std::size_t count() const;

    //! Return the total number of pool items that are available for the client to lease.
    std::size_t count_free() const;

    pooling::Type get_pooling_type() const;

private:
    //! Allow get() and free(size_t) to be called from the Connection class.
    //friend Connection;

private:
    class Impl;
    std::unique_ptr<Impl> pimpl;

}; //_ class Pool



typedef std::shared_ptr<Pool> PoolPtr;



//! Base class for users of connection pool.
//!
//! This supports the implementation of Session, Transaction, and Statement pooling.
class NEGERNS_DECL PoolClient
{
public:
private:
};

} //_ namespace connection
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_CONNECTION_POOL_H
