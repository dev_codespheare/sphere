/*******************************************************************************
 * File:    negerns/db/connection/odbc/driver.h
 * Created: 5 Mar 2015 2:59 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_CONNECTION_ODBC_DRIVER_H
#define NEGERNS_DB_CONNECTION_ODBC_DRIVER_H

#include <string>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace connection {
namespace odbc {

//! ODBC Drivers
enum class Driver {
    None,
    PostgreSQL_ANSI_32Bit,
    PostgreSQL_Unicode_32Bit,
    PostgreSQL_ANSI_64Bit,
    PostgreSQL_Unicode_64Bit,
}; //_ enum class Driver

//! Return the driver string of the specified Driver type.
NEGERNS_DECL
std::string get_driver_string(Driver);

//! Return the default port of the sepcified Driver type.
NEGERNS_DECL
std::string get_default_port(Driver);

} //_ namespace odbc
} //_ namespace connection
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_CONNECTION_ODBC_DRIVER_H