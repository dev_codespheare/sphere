/*******************************************************************************
 * File:    negerns/db/connection/odbc/postgresql/options.h
 * Created: 23 Feb 2014 8:14 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_BACKEND_ODBC_POSTGRESQL_OPTIONS_H
#define NEGERNS_DB_BACKEND_ODBC_POSTGRESQL_OPTIONS_H

#include <string>
#include <map>
#include <memory>
#include <negerns/db/connection/options.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {
namespace connection {
namespace odbc {
namespace postgresql {

//! PostgreSQL ODBC ConnectionString options.
//!
//! Some options were intentionally removed due to inapplicability or non-usage
//! of the options with modern versions of PostgreSQL. The reference PostgreSQL
//! version is at least is 9.3.0.
//!
//! See the link below for the other option information:
//! http://www.postgresql.org/message-id/attachment/15329/config.html
//!
//! http://www.connectionstrings.com/postgresql/#p51
enum class Option
{
    None,

    Driver,
    Server,
    Port,
    Database,
    User,
    Password,

    ConnectionName,
    ConnectionTimeOut,
    SSLMode,
    ReadOnly,
    //! Fake OID Index
    //! This option fakes a unique index on OID. This is useful when there is
    //! not a real unique index on OID and for apps which can't ask what the
    //! unique identifier should be (i.e, Access 2.0).
    FakeOIDIndex,
    //! Show OID Column
    //! Includes the OID in SQLColumns. This is good for using as a unique
    //! identifier to update records if no good key exists OR if the key has
    //!  many parts, which blows up the backend.
    ShowOIDColumn,
    //! Row Versioning
    //! Allows applications to detect whether data has been modified by other
    //! users while you are attempting to update a row. It also speeds the
    //! update process since every single column does not need to be specified
    //! in the where clause to update a row. The driver uses the "xmin" system
    //! field of PostgreSQL to allow for row versioning. Microsoft products
    //! seem to use this option well. See the faq for details on what you need
    //! to do to your database to allow for the row versioning feature to be
    //! used.
    RowVersioning,
    //! Show System Tables
    //! The driver will treat system tables as regular tables in SQLTables.
    //! This is good for Access so you can see system tables.
    ShowSystemTables,
    Fetch,
    //! Max Varchar
    //! The maximum precision of the Varchar and BPChar(char[x]) types. The
    //! default is 254 which actually means 255 because of the null terminator.
    //! Note, if you set this value higher than 254, Access will not let you
    //! index on varchar columns!
    MaxVarcharSize,
    //! Max LongVarChar
    //! The maximum precision of the LongVarChar type. The default is 4094
    //! which actually means 4095 with the null terminator. You can even
    //! specify (-4) for this size, which is the odbc SQL_NO_TOTAL value.
    MaxLongVarcharSize,
    //! Data Type Options: Text as LongVarChar
    //! PostgreSQL TEXT type is mapped to SQLLongVarchar, otherwise SQLVarchar.
    TextAsLongVarchar,
    //! Data Type Options: Unknowns as LongVarChar
    //! Unknown types (arrays, etc) are mapped to SQLLongVarChar, otherwise
    //! SQLVarchar
    UnknownsAsLongVarchar,
    //! Data Type Options: Bools as Char
    //! Bools are mapped to SQL_CHAR, otherwise to SQL_BIT.
    BoolsAsChar,
    //! Bytea as Large Objects
    //! Allow the use of bytea columns for Large Objects.
    ByteaAsLongVarBinary,
    Debug,
    CommLog,
    Optimizer,
    //! Use Declare/Fetch
    //! If true, the driver automatically uses declare cursor/fetch to handle
    //! SELECT statements and keeps 100 rows in a cache. This is mostly a great
    //! advantage, especially if you are only interested in reading and not
    //! updating. It results in the driver not sucking down lots of memory to
    //! buffer the entire result set. If set to false, cursors will not be used
    //! and the driver will retrieve the entire result set. For very large
    //! tables, this is very inefficient and may use up all the Windows memory/
    //! resources. However, it may handle updates better since the tables are
    //! not kept open, as they are when using cursors. This was the style of
    //! the old podbc32 driver. However, the behavior of the memory allocation
    //! is much improved so even when not using cursors, performance should at
    //! least be better than the old podbc32.
    UseDeclareFetch,
    //! Parse Statements:
    //! Tell the driver how to gather the information about result columns of
    //! queries. See also Disallow Premature and ServerSide Prepare options.
    //!
    //! The driver checks this option first. If disabled then it checks the
    //! Server Side Prepare option. If disabled also it checks the Disallow
    //! Premature option. If neither of them is specified the driver would
    //! execute the prepared statement prematurely when the application
    //! inquires the result columns' info.
    //!
    //! If this option is enabled, the driver will parse an SQL query statement
    //! to identify the columns and tables and gather statistics about them
    //! such as precision, nullability, aliases, etc. It then reports this
    //! information in SQLDescribeCol, SQLColAttributes, and SQLNumResultCols.
    //! Prior to PostgreSQL 6.4, this was the only accurate way of getting
    //! information on precision from a query result.
    //!
    //! If the parser can not deal with a column (because it is a function or
    //! expression, etc.), it will fallback to executing the statement which is
    //! the old style of getting the info. The parser is fairly sophisticated
    //! and can handle many things such as column and table aliases, quoted
    //! identifiers, literals, joins, cross-products, etc. It can correctly
    //! identify a function or expression column, regardless of the complexity,
    //! but it does not attempt to determine the data type or precision of
    //! these columns.
    Parse,
    //! SysTable Prefixes
    //! The additional prefixes of table names to regard as System Tables.
    //! The driver already treats names that begin with "pg_" as system tables.
    //! Here you can add additional ones, such as data dictionary tables (dd_).
    //! Separate each prefix with a semicolon (;)
    ExtraSysTablePrefixes,
    //! LF <-> CR/LF conversion
    //! Convert Unix style line endings to DOS style.
    LFConversion,
    //! Updateable Cursors
    //! Enable updateable cursor emulation in the driver.
    UpdatableCursors,
    //! Server side prepare:
    //! Applicable for 7.3+ servers and recommended for 7.4+.
    //!   - (7.4+) Tell the driver how to gather the information about result
    //!     columns. See also Parse Statement and Disallow Premature options.
    //!     By using extended query protocol the driver replies to the inquiry
    //!     correctly and effectively.
    //!   - (7.4+) By using extended query protocol the driver replies to the
    //!     inquiry for the information of parameters.
    //!   - (7.3+) When using prepared statements, prepare them on the server
    //!     rather than in the driver. This can give a slight performance
    //!     advantage as the server doesn't need to re-parse the statement
    //!     each time it is used.
    UseServerSidePrepare,
    LowerCaseIdentifier,
    GssAuthUseGSS,
    //! Extra Options
    //! combination of the following bits.
    //!   - 0x1: Force the output of short-length formatted connection string.
    //!     Check this bit when you use MFC CDatabase class.
    //!   - 0x2: Fake MS SQL Server so that MS Access recognizes PostgreSQL's
    //!     serial type as AutoNumber type.
    //!   - 0x4: Reply ANSI (not Unicode) char types for the inquiries from
    //!     applications. Try to check this bit when your applications don't
    //!     seem to be good at handling Unicode data.
    XaOpt,
#if 0
    Socket,
    //! Unknown Sizes:
    //! This controls what SQLDescribeCol and SQLColAttributes will return as
    //! to precision for character data types (varchar, text, and unknown) in
    //! a result set when the precision is unknown. This was more of a
    //! workaround for pre-6.4 versions of PostgreSQL not being able to return
    //! the defined column width of the varchar data type.
    //!
    //!   - Maximum: Always return the maximum precision of the data type.
    //!   - Dont Know: Return "Don't Know" value and let application decide.
    //!   - Longest: Return the longest string length of the column of any row.
    //!     Beware of this setting when using cursors because the cache size
    //!     may not be a good representation of the longest column in the cache.
    //!
    //!   - MS Access: Seems to handle Maximum setting ok, as well as all the
    //!     others.
    //!   - Borland: If sizes are large and lots of columns, Borland may crash
    //!     badly (it doesn't seem to handle memory allocation well) if using
    //!     Maximum size.
    UnknownSizes,
    CancelAsFreeStmt,
    BI,
    //! KSQO (Keyset Query Optimization):
    //! Deprecated for 7.1+ servers. Check this option when connecting 7.0-
    //! servers and the application seems to be suffering from the following
    //! kind of queries:
    //! select...where (a = 1 AND b = 1 AND c = 1) OR (a=1 AND b=1 AND c = 2)...
    KSQO,
    //! Protocol
    //! Note that when using SSL connections this setting is ignored.
    //!   - 6.2: Forces driver to use PostgreSQL 6.2(V0) protocol, which had
    //!     different byte ordering, protocol, and other semantics.
    //!   - 6.3: Use the 6.3(V1) protocol. This is compatible with both V1(6.3)
    //!     and V2(6.4 to 7.3) backends.
    //!   - 6.4+: Use the 6.4(V2) protocol. This is only compatible with 6.4
    //!     and higher backends.
    //!   - 7.4+: Use the 7.4(V3) protocol. This is only compatible with 7.4
    //!     and higher backends.
    Protocol,
    DisallowPremature,
    //! True is -1
    //! Represent TRUE as -1 for compatibility with some applications.
    TrueIsMinus1,
#endif
}; //_ enum class Option



//! Stores key/value database connection options.
//!
//! An instance is 'moved' into a Parameter instance.
class NEGERNS_DECL Options : public db::connection::Options
{
private:
    //! Return the Option that matches the specified string.
    static Option get_option(const std::string &);

public:
    //! Return the Option string representation.
    static std::string get_string(Option);

    Options() = default;
    using negerns::db::connection::Options::Options;

    //! Instantiate with key/value map.
    //!
    //! This is primarily used when creating a clone.
    //! \note Made public due to make_unique.
    Options(const std::map<std::string, std::string> &);

    virtual ~Options();

    Options & operator = (const Options &);
    Options & operator = (Options &&);

    //! Make a clone.
    virtual
    std::unique_ptr<db::connection::Options> clone() const override;

    //! Set the value for the specified Option.
    void set(Option, const std::string &);

    //! Return the value of the specified Option.
    //!
    //! If the specified Option is not found in the key/value map then it
    //! returns an empty string.
    std::string get(Option) const;

    //! Returns true if the specified Option is in the key/value map.
    bool exists(Option) const;

    //! Return the composed connection string.
    std::string get_connection_string();

    void log();

private:
    //! Option and string representation mapping.
    static std::map<Option, std::string> names;

}; //_ class Options



inline
void Options::set(Option key, const std::string &value)
{
    if (key != Option::None) {
        db::connection::Options::set(Options::get_string(key), value);
    }
}

inline
std::string Options::get(Option key) const
{
    std::string keystr(Options::get_string(key));
    if (db::connection::Options::exists(keystr)) {
        return db::connection::Options::get(keystr);
    } else {
        return std::string();
    }
}

inline
bool Options::exists(Option key) const
{
    std::string keystr(Options::get_string(key));
    return db::connection::Options::exists(keystr);
}

} //_ namespace postgresql
} //_ namespace odbc
} //_ namespace connection
} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_BACKEND_ODBC_POSTGRESQL_OPTIONS_H
