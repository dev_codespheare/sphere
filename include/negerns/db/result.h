/*******************************************************************************
 * File:    negerns/db/result.h
 * Created: 7 Feb 2015 5:28 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_RESULT_H
#define NEGERNS_DB_RESULT_H

#include <negerns/core/declspec.h>

#if 0
namespace negerns {
namespace db {

template <typename T>
class NEGERNS_DECL Result final
{
public:
    Result() { }
    Result(const Result &o) : valid(o.valid), value(o.value) { }
    Result(Result &&) : valid(o.valid), value(o.value) { }
    Result(T t) : valid(true), value(t) { }
    //Result(const T& t) : valid(false), value(t) { }
    Result(bool b) : valid(b) { }
    virtual ~Result() { }

    Result & operator = (const Result &o) {
        if (this != &o) {
            valid = o.valid;
            value = o.value;
        }
        return *this;
    }
    Result & operator = (Result &&o) {
        valid = o.valid;
        value = o.value;
        return *this;
    }

    T operator () () const { return value; }
    bool is_valid() const { return valid; }

private:
    bool valid = false;
    T value;
};



typedef Result<std::size_t> IndexResult;

} //_ namespace db
} //_ namespace negerns
#endif

#endif //_ NEGERNS_DB_RESULT_H
