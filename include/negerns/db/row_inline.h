/*******************************************************************************
 * File:    negerns/db/row_inline.h
 * Created:
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#include <negerns/core/log.h>
#include <negerns/core/debug.h>

namespace negerns {
namespace db {

inline
bool Row::Iterator::operator != (const Row::Iterator &other) const
{
    return pos != other.pos;
}

inline
const Var Row::Iterator::operator * () const
{
    return ref->is_linked() ? (*ref)(pos) : ref->values[pos];
}

inline
const Row::Iterator & Row::Iterator::operator ++ ()
{
    ++pos;
    // Although not strictly necessary for a range-based for loop
    // following the normal convention of returning a value from
    // operator++ is a good idea.
    return *this;
}



// -----------------------------------------------------------------------------



inline
Var Row::operator () (const std::size_t i)
{
    ASSERT(values.size() > 0, "Row has no column values.");
    ASSERT(i < values.size(), "Specified index is out of range.");
    return is_linked() ? (*ref)(index)(i) : values[i];

}

inline
const Var Row::operator () (const std::size_t i) const
{
    ASSERT(values.size() > 0, "Row has no column values.");
    ASSERT(i < values.size(), "Specified index is out of range.");
    return is_linked() ? (*ref)(index)(i) : values[i];
}

inline
Var Row::operator () (const std::string &s)
{
    auto ir = get_index(s);
    return ir.first ? Row::operator()(ir.second) : Var();
}

inline
const Var Row::operator () (const std::string &s) const
{
    auto ir = get_index(s);
    return ir.first ? Row::operator()(ir.second) : Var();
}

inline
Row::Iterator Row::begin()
{
    return Row::Iterator(this, 0);
}

inline
Row::Iterator Row::end()
{
    return Row::Iterator(this, get_column_count());
}

#if 0
inline
void Row::add(negerns::Var &&v)
{
    values.emplace_back(v);
}
#endif

inline
void Row::set(std::size_t i, const Var &v)
{
    ASSERT(values.size() > 0, "Row has no column values.");
    ASSERT(i < values.size(), "Specified index is out of range.");
    values[i] = v;
    TRACE("Row::set(): %1", values[i].as_string());
}

inline
void Row::clear()
{
    if (!is_linked()) {
        columns.clear();
        values.clear();
    }
}

inline
const Columns Row::get_columns() const
{
    if (is_linked()) {
        return ref->get_columns();
    } else {
        return columns;
    }
}

inline
column::Properties Row::get_column(std::size_t i)
{
    if (is_linked()) {
        return ref->get_column(i);
    } else {
        return columns(i);
    }
}

inline
column::Properties Row::get_column(const std::string &s)
{
    if (is_linked()) {
        return ref->get_column(s);
    } else {
        return columns(s);
    }
}

inline
std::size_t Row::get_column_count() const
{
    return is_linked() ? ref->get_column_count() : columns.count();
}

inline
bool Row::is_linked() const
{
    return ref != nullptr;
}

inline
void Row::unlink() { }

inline
std::pair<bool, std::size_t> Row::get_index(const std::string &s) const
{
    return is_linked() ? ref->get_index(s) : columns.get_index(s);
}

} //_ namespace db
} //_ namespace negerns
