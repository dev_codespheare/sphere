/*******************************************************************************
 * File:    negerns/db/columns.h
 * Created: 11 Feb 2014 5:01 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_COLUMNS_H
#define NEGERNS_DB_COLUMNS_H

#include <string>
#include <map>
#include <vector>
#include <negerns/core/debug.h>
#include <negerns/db/result.h>
#include <negerns/db/column/properties.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {

//! Collection of Column property objects.
//!
//! \internal
//! Making it usable with range-based for loop see the ff links
//! \link http://www.cprogramming.com/c++11/c++11-ranged-for-loop.html
//! \link http://mbevin.wordpress.com/2012/11/14/range-based-for/
//! \endinternal
class NEGERNS_DECL Columns
{
public:

    class NEGERNS_DECL Iterator
    {
    public:
        bool operator != (const Iterator &o) const {
            return pos != o.pos;
        }
        const column::Properties & operator * () const {
            return ref->properties[pos];
        }
        const Iterator & operator ++ () {
            ++pos;
            // Although not strictly necessary for a range-based for loop
            // following the normal convention of returning a value from
            // operator++ is a good idea.
            return *this;
        }

    private:
        friend Columns;
        //! Called by begin(), end() only.
        Iterator(Columns *r, std::size_t n) : ref(r), pos(n) { }

    private:
        Columns *ref;
        std::size_t pos;

    }; //_ class Iterator

    //! @{
    //! \name Iterator interface for range-based for loop

    Columns::Iterator begin() {
        return Iterator(this, 0);
    }

    Columns::Iterator end() {
        return Iterator(this, properties.size());
    }

    //! @}



    Columns() { }
    Columns(const Columns &o);
    Columns(Columns &&o);
    virtual ~Columns();

    Columns & operator = (const Columns &o);
    Columns & operator = (Columns &&o);

    //! Add a \c Column property.
    void add(const column::Properties &);

    //! @{
    //! \name Operator access

    //! Return column \c Property object at specified index.
    column::Properties operator () (const std::size_t i);

    //! Return column \c Property object at specified index.
    const column::Properties operator () (const std::size_t i) const;

    //! Return column \c Property object at specified name index.
    column::Properties operator () (const std::string &);

    //! Return column \c Property object at specified name index.
    const column::Properties operator () (const std::string &) const;

    //! @}

    //! @{
    //! \name Property query

    std::string  get_name(std::size_t) const;
    column::Type get_type(std::size_t) const;
    short        get_sqltype(std::size_t) const;
    std::size_t  get_precision(std::size_t) const;
    std::size_t  get_length(std::size_t) const;
    std::size_t  get_scale(std::size_t) const;
    bool         is_identity(std::size_t) const;
    bool         is_nullable(std::size_t) const;

    //! @}

    std::pair<bool, std::size_t> get_index(const std::string &) const;

    //! Remove all properties.
    void clear();

    //! Return number of properties.
    std::size_t count() const;

private:
#if 0
    //! Returns true if the specified string is found in the string to index
    //! mapping container.
    bool is_found(const std::string &s) const;
#endif

private:
    //! Column properties.
    std::vector<column::Properties> properties;

    //! String to index mapping.
    std::map<std::string, std::size_t> string_map;

}; //_ class Columns



// -----------------------------------------------------------------------------



inline
column::Properties Columns::operator () (const std::size_t i)
{
    ASSERT(i < properties.size(), "Column index is out of range.");
    if (i < properties.size()) {
        return properties[i];
    } else {
        return column::Properties();
    }
}

inline
const column::Properties Columns::operator () (const std::size_t i) const
{
    ASSERT(i < properties.size(), "Column index is out of range.");
    if (i < properties.size()) {
        return properties[i];
    } else {
        return column::Properties();
    }
}

inline
std::string Columns::get_name(std::size_t i) const
{
    ASSERT(!properties.empty(), "Column is empty.");
    ASSERT(i < properties.size(), "Column index is out of range.");
    return properties[i].name;
}

inline
column::Type Columns::get_type(std::size_t i) const
{
    ASSERT(!properties.empty(), "Column is empty.");
    ASSERT(i < properties.size(), "Column index is out of range.");
    return properties[i].type;
}

inline
short Columns::get_sqltype(std::size_t i) const
{
    ASSERT(!properties.empty(), "Column is empty.");
    ASSERT(i < properties.size(), "Column index is out of range.");
    return properties[i].sqlType;
}

inline
std::size_t Columns::get_precision(std::size_t i) const
{
    ASSERT(!properties.empty(), "Column is empty.");
    ASSERT(i < properties.size(), "Column index is out of range.");
    return properties[i].precision;
}

inline
std::size_t Columns::get_length(std::size_t i) const
{
    ASSERT(!properties.empty(), "Column is empty.");
    ASSERT(i < properties.size(), "Column index is out of range.");
    return properties[i].length;
}

inline
std::size_t Columns::get_scale(std::size_t i) const
{
    ASSERT(!properties.empty(), "Column is empty.");
    ASSERT(i < properties.size(), "Column index is out of range.");
    return properties[i].scale;
}

inline
bool Columns::is_identity(std::size_t i) const
{
    ASSERT(!properties.empty(), "Column is empty.");
    ASSERT(i < properties.size(), "Column index is out of range.");
    return properties[i].identity;
}

inline
bool Columns::is_nullable(std::size_t i) const
{
    ASSERT(!properties.empty(), "Column is empty.");
    ASSERT(i < properties.size(), "Column index is out of range.");
    return properties[i].nullable;
}

inline
void Columns::clear()
{
    properties.clear();
    string_map.clear();
}

inline
std::size_t Columns::count() const
{
    return properties.size();
}

} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_COLUMNS_H
