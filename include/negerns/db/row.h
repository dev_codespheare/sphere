/*******************************************************************************
 * File:    negerns/db/row.h
 * Created: 28 Feb 2014 7:34 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_ROW_H
#define NEGERNS_DB_ROW_H

#include <memory>
#include <vector>

#include <negerns/core/var.h>
#include <negerns/db/columns.h>
#include <negerns/db/sql.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {

class Rows;

//! Collection of column values.
class NEGERNS_DECL Row final
{
public:

    class NEGERNS_DECL Iterator final
    {
    public:
        bool operator != (const Iterator &) const;
        const negerns::Var operator * () const;
        const Iterator & operator ++ ();

    private:
        friend Row;
        //! Called by begin(), end() only.
        Iterator(Row *r, std::size_t n) : ref(r), pos(n) { }

    private:
        Row *ref;
        std::size_t pos;
    }; // class Iterator

    Row() = default;
    Row(const Columns &);
    Row(const Row &) = default;
    virtual ~Row() { }

    Row & operator = (const Row &) = default;

    //! @{
    //! \name Operator access

    //! Return column value at specified index.
    negerns::Var operator () (const std::size_t);

    //! Return column value at specified index.
    const negerns::Var operator () (const std::size_t) const;

    //! Return column value at specified name index.
    negerns::Var operator () (const std::string &s);

    //! Return column value at specified name index.
    const negerns::Var operator () (const std::string &s) const;

    //! @}

    //! @{
    //! \name Iterator interface for range-based for loop

    Row::Iterator begin();

    Row::Iterator end();

    //! @}

#if 0
    //! Add a value.
    void add(negerns::Var &&);
#endif

    //! Set the value of the column specified by the index.
    void set(std::size_t, const negerns::Var &);

    //! Remove all column values.
    void clear();

    //! Set the column information.
    //! Values are initialized to their defaults.
    void set_columns(const Columns &);
    const Columns get_columns() const;

    column::Properties get_column(std::size_t);

    column::Properties get_column(const std::string &);

    //! Return number of column values.
    std::size_t get_column_count() const;

    //! Whether the Row is linked to Rows.
    bool is_linked() const;

    void unlink();

private:
    //! Helper function returning the index of the specified column name.
    std::pair<bool, std::size_t> get_index(const std::string &) const;

    //! Initialize values to default.
    void set_default_values();

private:
    //! Row index in the referenced Rows instance.
    std::size_t index = 0;

    //! Rows instance reference.
    Rows *ref = { nullptr };

    //! Contains column properties if there is no reference to a Rows instance.
    Columns columns;

    //! Contains column values if there is no reference to a Rows instance.
    Vars values;

}; //_ class Row



// ----------------------------------------------------------------------------------------



//! Collection of Row objects.
class NEGERNS_DECL Rows final
{
public:
    Rows() = default;
    Rows(std::unique_ptr<Sql>);
#if 0
    Rows(const Columns &);
#endif
    virtual ~Rows();

    //! @{
    //! \name Operator access

    // Functions returning objects must return it as const ref.
    // Can we return ref in case the returned value is to be modified?

    //! Return the Row object in the specified row index.
    Row & operator () (std::size_t);

    const Row & operator () (std::size_t) const;

    //! Return the value in the specified row and column index.
    negerns::Var operator () (std::size_t, std::size_t);

    //! Return the value in the specified row index and column name.
    negerns::Var operator () (std::size_t, const std::string &);

    //! @}

    //! @{
    //! \name Null checking and handling

    //! Return true if the value in the specified row and column index is null.
    bool is_null(std::size_t, std::size_t);

    //! Return true if the value in the specified row index and column name
    //! is null.
    bool is_null(std::size_t, const std::string &);

    //! Return the value in the specified row and column index.
    //! If the value in the row and column is null then the specified default
    //! value is returned instead.
    negerns::Var if_null(std::size_t, std::size_t, const negerns::Var &);

    //! Return the value in the specified row index and column name.
    //! If the value in the row and column is null then the specified default
    //! value is returned instead.
    negerns::Var if_null(std::size_t, const std::string &, const negerns::Var &);

    //! @}

    const Columns get_columns() const;

    std::size_t get_column_count() const;

    column::Properties get_column(std::size_t);

    column::Properties get_column(const std::string &);

    std::pair<bool, std::size_t> get_index(const std::string &) const;

    std::size_t get_affected_rows() const;

    //! Return the number of rows.
    std::size_t get_row_count() const;

    bool is_linked() const;
    void unlink();

private:
    friend Row;

    //! Contains column properties if there is no reference to an Sql instance.
    Columns columns;

    std::unique_ptr<Sql> sql = { nullptr };
    std::vector<Row> rows;

}; //_ class Rows

} //_ namespace db
} //_ namespace negerns

#include <negerns/db/row_inline.h>
#include <negerns/db/rows_inline.h>

#endif //_ NEGERNS_DB_ROW_H
