/*******************************************************************************
 * File:    negerns/db/row_inline.h
 * Created:
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#include <negerns/core/log.h>
#include <negerns/core/debug.h>

namespace negerns {
namespace db {

inline
Row & Rows::operator () (std::size_t i)
{
    ASSERT(i < rows.size(), "Index out of range.");
    return rows[i];
}

inline
const Row & Rows::operator () (std::size_t i) const
{
    ASSERT(i < rows.size(), "Index out of range.");
    return rows[i];
}

inline
Var Rows::operator () (std::size_t r, std::size_t c)
{
    ASSERT(r < rows.size(), "Row out of range.");
    return rows[r](c);
}

inline
Var Rows::operator () (std::size_t r, const std::string &s)
{
    ASSERT(r < rows.size(), "Row out of range.");
    return rows[r](s);
}



inline
bool Rows::is_null(std::size_t r, std::size_t c)
{
    ASSERT(r < rows.size(), "Row out of range.");
    return rows[r](c).empty();
}

inline
bool Rows::is_null(std::size_t r, const std::string &c)
{
    return rows[r](c).empty();
}

inline
Var Rows::if_null(std::size_t r, std::size_t c, const Var &v)
{
    auto value = rows[r](c);
    return value.empty() ? Var(v) : value;
}

inline
Var Rows::if_null(std::size_t r, const std::string &c, const Var &v)
{
    auto value = rows[r](c);
    return value.empty() ? Var(v) : value;
}



inline
const Columns Rows::get_columns() const
{
    if (is_linked()) {
        return sql->get_columns();
    } else {
        return columns;
    }
}

inline
std::size_t Rows::get_column_count() const
{
    if (is_linked()) {
        return sql->get_column_count();
    } else {
        return columns.count();
    }
}

inline
column::Properties Rows::get_column(std::size_t i)
{
    if (is_linked()) {
        return sql->get_column(i);
    } else {
        return columns(i);
    }
}

inline
column::Properties Rows::get_column(const std::string &s)
{
    if (is_linked()) {
        return sql->get_column(s);
    } else {
        return columns(s);
    }
}

inline
std::pair<bool, std::size_t> Rows::get_index(const std::string &s) const
{
    if (is_linked()) {
        return sql->get_index(s);
    } else {
        return columns.get_index(s);
    }
}



inline
std::size_t Rows::get_affected_rows() const
{
    ASSERT(sql != nullptr, "Reference to Sql instance is null.");
    return sql->get_affected_rows();
}

inline
std::size_t Rows::get_row_count() const
{
    return rows.size();
}

inline
bool Rows::is_linked() const
{
    return sql != nullptr;
}

inline
void Rows::unlink() { }

} //_ namespace db
} //_ namespace negerns
