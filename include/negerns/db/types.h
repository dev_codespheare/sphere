/*******************************************************************************
 * File:    negerns/db/types.h
 * Created: 28 Oct 2014 2:27 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DB_TYPES_H
#define NEGERNS_DB_TYPES_H

#include <string>

#include <negerns/core/declspec.h>

namespace negerns {
namespace db {

// Type of statement (used for optimizing statement preparation)
#if 0
enum class QueryType {
    OneTime,
    Repeatable
};
#endif



// ----------------------------------------------------------------------------------------



//! Status result after a database SQL command execution.
enum class ExecuteStatus {
    Error,
    Success,
    NoData
};

NEGERNS_DECL
bool is_execute_error(ExecuteStatus);



// ----------------------------------------------------------------------------------------



//! Error types
//!
//! \todo
//! Make error types generic across all databases/data sources as much as
//! possible. This means mapping specific error types to generic ones.
//! Each database/data source will have to maintain it's own specific errors
//! and mapped to this generic type.
enum class Error
{
    None = 0,
    GenericError,
    ODBCEnvironmentError,
    ODBCDBCError,
    ODBCDriverConnectError,
    ODBCDataSourceNameNotFound
}; // enum class Error

//! Return the Error type string representation.
NEGERNS_DECL
std::string get_error_string(Error);

} //_ namespace db
} //_ namespace negerns

#endif //_ NEGERNS_DB_TYPES_H
