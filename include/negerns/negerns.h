/*******************************************************************************
 * File:    negerns/negerns.h
 * Created: 31 Oct 2013 10:55 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment: Declare alias(es) for the negerns namespaces
 *          This is intended only for code using the negerns library.
 *          The negerns library should not use this header.
 ******************************************************************************/

// Declare the negerns library namespaces so we can
// create aliases for them below.
namespace negerns {
    namespace data { }
    namespace gui { }
}

// Aliases
namespace n = negerns;
namespace negerns {
    namespace d = data;
    namespace g = gui;
}
