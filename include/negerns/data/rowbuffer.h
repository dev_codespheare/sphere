/*******************************************************************************
 * File:    negerns/data/rowbuffer.h
 * Created: 11 Feb 2014 1:34 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DATA_ROWBUFFER_H
#define NEGERNS_DATA_ROWBUFFER_H

#include <string>
#include <negerns/core/declspec.h>
#include <negerns/core/variant.h>
#include <negerns/data/irows.h>

#include <negerns/poco.h>

namespace negerns {
namespace data {

class DataStore;

class NEGERNS_DECL RowBuffer : public IRows
{
public:

    RowBuffer();
    virtual ~RowBuffer();

    /// @{
    /// \name Read Operations

    /// Return the Row object in the specified row index.
    Row & operator()(std::size_t i);

    /// Return the Row object in the specified row index.
    Row & GetRow(const std::size_t i);

    /// Return the value in the specified row and column index.
    virtual Variant & operator()(
        std::size_t r,
        std::size_t c) override;

    /// Return the value in the specified row index and column name.
    virtual Variant & operator()(
        std::size_t r,
        const std::string &s) override;

    /// Return the value in the specified row and column index.
    virtual Variant & Get(
        std::size_t r,
        std::size_t c) override;

    /// Return the value in the specified row index and column name.
    virtual Variant & Get(
        std::size_t r,
        const std::string &s) override;

    /// Return the value in the specified row and column index.
    /// If the value in the row and column is null then the specified default
    /// value is used instead.
    virtual Variant & IfNull(
        std::size_t r,
        std::size_t c,
        const Variant &v) override;

    /// Return the value in the specified row index and column name.
    /// If the value in the row and column is null then the specified default
    /// value is used instead.
    virtual Variant & IfNull(
        std::size_t r,
        const std::string &s,
        const Variant &v) override;

    /// @}

    /// @{
    /// \name Write Operations

    /// Set the value in the specified row and column index.
    virtual void operator()(
        std::size_t r,
        std::size_t c,
        const Variant &v) override;

    /// Set the value in the specified row index and column name.
    virtual void operator()(
        std::size_t r,
        const std::string &s,
        const Variant &v) override;

    /// @}

    /// Set the internal buffer with the specified row container.
    std::size_t SetRows(const std::map<std::size_t, Row> &);

    /// Return \c true if the value in the \c row and \c column is null.
    virtual bool IsNull(std::size_t r, std::size_t c) override;

    /// Return \c true if the value in the \c row and \c column is null.
    virtual bool IsNull(std::size_t r, const std::string &s) override;

    /// Return the number of rows in the internal buffer.
    virtual std::size_t RowCount() override;

    virtual std::size_t Find(std::size_t c, const Variant &) override;

protected:

    /// Add the specified Row to the container. If the Row exists then
    /// the Row is set.
    virtual void AddRow(const std::size_t, const Row &);

    /// Returns true if the specified row index exists in the internal buffer.
    /// Otherwise, returns false.
    virtual bool IsRowFound(const std::size_t) final;

    /// Delete all rows from the buffer.
    void DeleteAllRows();

    /// Delete the specified row from the buffer.
    void DeleteRow(std::size_t row);

};



class NEGERNS_DECL InsertRowBuffer : public RowBuffer
{
public:
    friend class ::negerns::data::DataStore;

    InsertRowBuffer();
    virtual ~InsertRowBuffer();

    void SetRow(const std::size_t i, const Row &row);

    /// Add n number of the specified Row.
    void AddRows(const std::size_t, const Row &);
};



class NEGERNS_DECL ModifyRowBuffer : public RowBuffer
{
public:
    friend class ::negerns::data::DataStore;

    ModifyRowBuffer();
    virtual ~ModifyRowBuffer();

    void SetRow(const std::size_t i, const Row &row);

    /// Revert the values of the specified row to its original value.
    void RevertRow(std::size_t);

private:

    /// Set the reference to the \c RecordSet object of the \c Datastore object.
    void SetRecordSet(poco::data::RecordSet *);
#if 0
private:

    /// Reference to the \c RecordSet object of the \c Datastore object.
    ///
    /// \see InternalRowIFace::SetRecordSet
    poco::data::RecordSet * recordset;
#endif
};



class NEGERNS_DECL DeleteRowBuffer : public RowBuffer
{
public:
    friend class ::negerns::data::DataStore;

    DeleteRowBuffer();
    virtual ~DeleteRowBuffer();
};

} // namespace data
} // namespace negerns

#endif // NEGERNS_DATA_ROWBUFFER_H
