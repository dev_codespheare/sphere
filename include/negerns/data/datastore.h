/*******************************************************************************
 * File:    negerns/data/datastore.h
 * Created: 14 Feb 2014 8:56 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DATA_DATASTORE_H
#define NEGERNS_DATA_DATASTORE_H

#include <vector>
#include <memory>
#include <Poco/Data/RecordSet.h>
#include <negerns/core/declspec.h>
#include <negerns/core/member.h>
#include <negerns/data/bufferstore.h>
#include <negerns/data/row.h>
#include <negerns/data/recordset.h>
#include <negerns/data/rowbuffer.h>
#include <negerns/data/transaction.h>

#include <negerns/poco.h>

namespace negerns {
namespace data {

/// Database row manipulation.
///
/// A class that encapsulates database transactions focusing on data
/// retrieval and manipulation. It is intended primarily to be used as
/// a container for database row retrievals. It provides row operations
/// like insertion, updates and deletion.
///
/// It also provides capabilities that aid in the data manipulation. The
/// class maintains buffered rows for insert, modified, deleted rows. It
/// can be also be used to present the rows to the client for further
/// operations prior to sending the updates to the database.
///
/// \internal
/// DataStore shares almost the same class information as the \c BufferStore.
/// The \c DataStore class could be subclassed from the \c BufferStore class.
/// \endinternal
///
/// \todo Set primary columns for the delete operation.
/// \todo Sort rows.
/// \todo Filter rows.
/// \todo Export contents to a file.
class NEGERNS_DECL DataStore : public NameIndexContainer
{
public:
    friend class Transaction;
    friend class InternalRowsIFace;

private:

    /// \c SqlStatement class encapsulates an SQL statement string that is used in
    /// database transactions. The statement is not checked for validity. The client
    /// must ensure that it is well formed.
    typedef member::String SqlStatement;

public:

    /// Row buffer types.
    enum class BufferType { Unknown, Original, Inserted, Modified, Deleted, Detached };

    /// Compares the rows and returns true if they are equal.
    /// The comparison considers the number of columns, column type and the
    /// column values. If a column value is null then it is converted to an
    /// empty string.
    static bool IsEqual(Row &row1, Row &row2);

    /// Default constructor.
    DataStore();

    /// Constructor accepting a Transaction object.
    DataStore(Transaction *);

    /// Destructor.
    virtual ~DataStore();

    /// Set the Transaction object.
    void SetTransaction(Transaction *);

    /// Return a pointer to the Poco Recordset object.
    virtual poco::data::RecordSet * GetRecordSet() final;

    /// Copy the contents of the \c detached buffer into a \c BufferStore
    /// object and returns the BufferStore object.
    BufferStore * Detach();

    /// Copy the contents of the \c BufferStore object into the \c modified
    /// buffer.
    void Attach(const BufferStore *);

    /// Set the primary key indices. The indices will be sorted. The primary
    /// key indices is used in the update and delete database transactions.
    ///
    /// ~~~~~~~~~~{.cpp}
    /// SetPrimaryKeys({0, 1});
    /// ~~~~~~~~~~
    ///
    /// \todo Overload to accept string indices.
    void SetPrimaryKeys(const std::vector<std::size_t> &);

#ifndef NDEBUG
    const std::vector<std::size_t> & GetPrimaryKeys() const;
#endif

    /// @{
    /// \name Internal Row Buffer Operations
    ///
    /// Perform internal buffer row operations. This operations do not perform
    /// any database transactions.

    /// Set the default row.
    ///
    /// The default row is a row structure only. It is initialized after a
    /// retrieve operation. Usually the retrieve operation only acquires the
    /// row structure and is therefore not necessary to actually retrieve an
    /// existing row from the database.
    ///
    /// \example Retrieve a non-existent row from \table_1.
    ///
    /// ~~~~~~~~~~{.cpp}
    /// auto *ds = datastore->Retrieve("select * from table_1 where id = 0");
    /// auto count = ds->Retrieve();
    /// if (count == 0) {
    ///     ds->SetDefaults();
    /// } else {
    ///     FAIL("Not expecting a row after a database retrieve operation.");
    /// }
    /// ~~~~~~~~~~
    ///
    /// \internal
    /// Setting the default row is in a separate function because we do not need
    /// to keep setting it during a retrieve operation which will wipe the
    /// previously set column default values and the client would need to reset
    /// it again. So putting it here allows the client to call this function
    /// once only during the initial retrieve operation.
    /// \endinternal
    void SetDefaults();

    /// Add the row from the \c original buffer specified by the parameter
    /// \c index into the \c inserted buffer.
    ///
    /// \internal
    /// The blank row gets its structure from the \c RecordSet after a retrieve
    /// operation. So, the initial retrieve, even if it does not return a row,
    /// acquires the row structure.
    /// \endinternal
    ///
    /// \todo Set colum default values.
    void InsertRow(std::size_t index = 0);

    /// Add the specified number of rows into the \c inserted buffer. By default
    /// it adds a single row. If the \c index parameter is specified then it
    /// copies rows from the \c original buffer specified by the indices into
    /// the \c inserted buffer. The number of rows to be added must be equal to
    /// the number of indices in the vector.
    ///
    /// \internal
    /// Blank row(s) get their structure from the \c RecordSet after a retrieve
    /// operation. So, the initial retrieve, even if it does not return a row,
    /// acquires the row structure.
    /// \endinternal
    void InsertRows(std::size_t rows, const std::vector<std::size_t> &index = { });

    /// Add rows from the \c original buffer into the \c inserted buffer. The
    /// rows from the \c original buffer starting at \c start index up to the
    /// row before the \c end index are copied into the \c inserted buffer. The
    /// number of rows added is equal to the \c end index less the \c start
    /// index.
    void InsertRows(std::size_t start, std::size_t end);

    /// Copy a row from the \c original buffer into the \c modified buffer.
    /// The row to be copied is specified by its \c original buffer index.
    ///
    /// \see DataStore::IsModified
    void ModifyRow(std::size_t);

    /// Add the specified number of rows into the \c modified buffer. By default
    /// it adds a single row. If the \c index parameter is specified then it
    /// copies rows from the \c original buffer specified by the indices into
    /// the \c modified buffer. The number of rows to be added must be equal to
    /// the number of indices in the vector.
    void ModifyRows(std::size_t rows,  const std::vector<std::size_t> &index = { });

    /// Add rows from the \c original buffer into the \c modified buffer. The
    /// rows from the \c original buffer starting at \c start index up to the
    /// row before the \c end index are copied into the \c modified buffer. The
    /// number of rows added is equal to the \c end index less the \c start
    /// index.
    void ModifyRows(std::size_t start, std::size_t end);

    /// Copy all the rows from the \c original buffer into the \c deleted
    /// buffer.
    ///
    /// \see DataStore::IsDeleted
    void DeleteRow(std::size_t);

    /// Copy the rows from the \c original buffer into the \c deleted buffer.
    /// buffer. The rows to be copied are specified by their \c original buffer
    /// indices. If the rows are not specified, then it will copy all the rows
    /// from the \c original buffer.
    ///
    /// \todo
    void DeleteRows(const std::vector<std::size_t> &rows);

    /// Copy the row from the \c original buffer into the \c detached buffer.
    /// The row to be copied is specified by its \c original buffer index.
    void DetachRow(std::size_t);

    /// Discards all changes without updating the database. Clears all rows
    /// in the \c inserted, \c modified, \c deleted buffers.
    void DiscardChanges();

    /// Compares the specified row from the first buffer with the row from
    /// the second buffer. Returns true if the column values are equal.
    bool IsEqual(BufferType, BufferType, std::size_t);

    /// @}

    /// @{
    /// \name Row Status Queries

    /// Returns true if the specified row index is in the \c inserted buffer.
    /// Otherwise, returns false.
    bool IsInserted(std::size_t);

    /// Returns true if the specified row index is in the \c modified buffer.
    /// Otherwise, returns false.
    bool IsModified(std::size_t);

    /// Returns true if the specified row index is int he \c deleted buffer.
    /// Otherwise, returns false.
    bool IsDeleted(std::size_t);

    /// @}

    /// @{
    /// \name Database Transactions
    ///
    /// Perform database transactions.

    /// Retrieve rows from the database. Returns the number of rows retrieved.
    ///
    /// This database transaction affects the internal state of the object.
    ///
    /// \todo Check number of arguments.
    std::size_t Retrieve();

    /// Retrieve rows from the database passing retrieve SQL statement arguments.
    /// Returns the number of rows retrieved.
    ///
    /// This database transaction affects the internal state of the object.
    ///
    /// \todo Check number of arguments.
    std::size_t Retrieve(const Parameters &);

    std::size_t Insert(bool discard = true);

    std::size_t Insert(Rows &, bool discard = true);

    std::size_t Update(bool discard = true);

    std::size_t Update(const Rows &, bool discard = true);

    std::size_t Delete();

    std::size_t Delete(Rows &);

    /// Update database with changes.
    /// Performs delete, update and insert transactions; in that order.
    ///
    /// This database transaction affects the internal state of the object.
    ///
    /// \todo Add more functionality to query failed operations.
    std::size_t UpdateAll();

    /// @}

    /// Return the value in the specified row and column index in the
    /// specified \c BufferType. If the \c BufferType argument is not specified
    /// then the \c original buffer is queried for the row and column
    /// value.
    Variant & GetValue(std::size_t row,
        std::size_t col,
        BufferType t = BufferType::Unknown);

    /// copydoc DataStore::GetValue
    Variant & GetValue(std::size_t row,
        const std::string &col,
        BufferType t = BufferType::Unknown);

    /// Returns the number of rows of the specified \c BufferType. The default
    /// returns the number of rows in the \c original buffer after the last
    /// retrieve operation.
    ///
    /// \see DataStore::BufferType
    std::size_t RowCount(const BufferType t = BufferType::Original);

    /// @{
    /// \name Generic Database Transactions
    ///
    /// Allows client code to perform generic database transactions using
    /// the associated transaction object. The transaction, however, does
    /// not affect the current state of the class. This is useful for
    /// performing database-related support operations.

    /// Execute the specified SQL statement and returns the number of rows
    /// affected. The function does not check the validity of the SQL
    /// statement. It is up to the client to ensure a well formed SQL
    /// statement.
    ///
    /// The database transaction does not affect the state of the object.
    std::size_t Execute(const std::string &);

    /// Execute the specified SQL statement and SQL statement arguments.
    /// Returns the number of rows affected. The function does not check
    /// the validity of the SQL statement. It is up to the client to ensure
    /// a well formed SQL statement.
    ///
    /// The database transaction does not affect the state of the object.
    ///
    /// \todo Check number of arguments.
    std::size_t Execute(const std::string &, Parameters &);

    /// @}

private:

    Row GetPrimaryKeyColumns(Row &);
    
public:

    /// Select statement
    SqlStatement sqlSelect;

    SqlStatement sqlInsert;

    SqlStatement sqlUpdate;

    SqlStatement sqlDelete;

    /// Column properties.
    Columns column;

    /// Column default values.
    Row default;

    /// @{
    /// \name Row Buffers.
    ///
    /// Buffer rows are temporary containers.

    /// Original rows from the database.
    /// The buffer is indexed from zero (0).
    RecordSet original;

    /// Inserted rows buffer.
    /// The buffer is indexed from zero (0).
    InsertRowBuffer inserted;

    /// Modified rows buffer.
    ///
    /// \c Detached rows buffer are indexed according to the \c original rows
    /// index.
    ModifyRowBuffer modified;

    /// Deleted rows buffer.
    ///
    /// \c Detached rows buffer are indexed according to the \c original rows
    /// index.
    DeleteRowBuffer deleted;

    /// Detached rows buffer.
    ///
    /// \c Detached rows buffer are indexed according to the \c original rows
    /// index.
    ///
    /// \see Detach The detached buffer is copied into a BufferStore on a call
    /// to \c Detach.
    ModifyRowBuffer detached;

    /// Returned row buffer.
    ///
    /// Contains values returned after a call to Update function. The row buffer
    /// is populated after an insert operation wherein a row primary key from
    /// the database is returned.
    ///
    /// The buffer is indexed from zero (0).
    InsertRowBuffer returned;

    /// @}

protected:

    /// Database transaction instance.
    Transaction *transaction;

    /// Holds database query result.
    std::unique_ptr<poco::data::RecordSet> recordset;

    /// Primary key indices.
    std::vector<std::size_t> pkIndex;
};

} // namespace data
} // namespace negerns

#endif // NEGERNS_DATA_DATASTORE_H
