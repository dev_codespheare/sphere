/*******************************************************************************
 * File:    negerns/data/columns.h
 * Created: 11 Feb 2014 5:01 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DATA_COLUMNS_H
#define NEGERNS_DATA_COLUMNS_H

#include <string>
#include <map>
#include <vector>
#include <negerns/core/declspec.h>
#include <negerns/core/nameindex.h>
#include <negerns/data/column.h>

namespace negerns {
namespace data {

class Columns;

class NEGERNS_DECL ColumnsIterator
{
public:
    ColumnsIterator(const Columns *, std::size_t pos);

    bool operator!=(const ColumnsIterator &other) const;
 
    const Column& operator*() const;
 
    const ColumnsIterator& operator++();

private:

    std::size_t pos;
    const Columns *columns;
};

/// Set of Column objects.
///
/// \internal
/// Making it usable with range-based for loop see the ff link
/// \link http://www.cprogramming.com/c++11/c++11-ranged-for-loop.html
/// \link http://mbevin.wordpress.com/2012/11/14/range-based-for/
/// \endinternal
class NEGERNS_DECL Columns : public NameIndexIFace<Column>
{
public:
    Columns();
    virtual ~Columns();

    ColumnsIterator begin() const;
 
    ColumnsIterator end() const;

    /// Add a name index value.
    /// The index value is the current size of the string to index mapping
    /// container less one.
    void Add(const Column &);

    /// Get \c Column object at the specified index.
    Column& Get(const std::size_t);

    const Column& Get(const std::size_t) const;

    /// Get \c Column object at the specified string index.
    Column& Get(const std::string &s);
    
    const Column& Get(const std::string &s) const;

    /// Remove all elements.
    void Clear();

    std::size_t Count() const;

private:

    Column & GetColumnInternal(const std::size_t i);

private:

    /// Column properties.
    /// Populated when Row is detached.
    std::vector<Column> items;
};

} // namespace data
} // namespace negerns

#endif // NEGERNS_DATA_COLUMNS_H
