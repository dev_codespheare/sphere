/*******************************************************************************
 * File:    negerns/data/transaction.h
 * Created: 11 Jan 2013 12:13 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DATA_TRANSACTION_H
#define NEGERNS_DATA_TRANSACTION_H

#include <string>
#include <memory>
#include <negerns/core/defs.h>
#include <negerns/core/declspec.h>
#include <negerns/core/memory.h>
#include <negerns/core/variant.h>
#include <negerns/data/recordset.h>
#include <negerns/data/row.h>
#include <negerns/poco.h>

namespace negerns {
namespace data {

    namespace transaction {

        /// Transaction group intended to be extended in client.
        /// Transaction group is a C-style enumeration allowing new groups to
        /// be defined. User-defined groups are Group::NoGroup + 1. To define
        /// new groups the enumeration must be enclosed in \c negerns::data::transaction
        /// namespace.
        enum BaseGroup : std::size_t
        {
            //NoGroup
        };

        /// Transaction types
        ///
        /// Transaction type is a C-style enumeration allowing new types to be
        /// defined. User-defined types are Type::Max + 1. To define new types
        /// the enumeration must be enclosed in \c negerns::data::transaction
        /// namespace.
        enum Type : std::size_t
        {
            NoType,

            GetStructure,

            Insert,
            Read,
            Update,
            Delete,

            Truncate,
            Procedure,
            Function,
            TypeMax
        };

    } // namespace transaction

typedef Variants Parameters;
#if 0
    namespace internal {
        class DataStore;
    }
#endif
class RecordSet;

/// Database transaction.
class NEGERNS_DECL Transaction
{
public:

    Transaction(std::size_t sessionPool);
    Transaction(Transaction &&);
    virtual ~Transaction();

    Transaction & operator=(Transaction &&);

    void SetSessionPoolId(std::size_t sp) { sessionPoolId = sp; }

    poco::data::Statement* CreateStatement(const std::string &);
    std::size_t Bind(poco::data::Statement *, const Parameters &);
    std::size_t Bind(poco::data::Statement *, Row &);
    std::size_t Execute(poco::data::Statement *);

    /// Execute an SQL statement.
    /// The parameter Row holds the arguments to the SQL statement.
    std::size_t Execute(const std::string &, const Parameters &);

    /// Execute an SQL statement.
    /// The parameter Row holds the arguments to the SQL statement.
    ///
    /// \internal The parameter Row is intended not to be \c const.
    std::size_t Execute(const std::string &sql, Row &);

    std::size_t Execute(const std::string &sql, const Rows &rows);

private:

    friend class DataStore;

    /// Used by DataStore::Update for inserting new rows.
    poco::data::Statement* Delete(const std::string &, Row &);

    /// Session pool ID to use for acquiring a session pool object.
    std::size_t sessionPoolId;
};

} // namespace data
} // namespace negerns

#endif // NEGERNS_DATA_TRANSACTION_H
