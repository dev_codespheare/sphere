/*******************************************************************************
 * File:    negerns/data/datasource.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 9 Feb 2014 6:43 AM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DATA_DATASOURCE_H
#define NEGERNS_DATA_DATASOURCE_H

#include <string>
#include <negerns/core/declspec.h>

namespace negerns {
namespace data {

/// Properties used to connect to a database.
class NEGERNS_DECL DataSource
{
public:

    enum class Connector {
        ODBC,
        SQLite
    };

    DataSource();
    DataSource(const std::string& dsn,
        const std::string& uid = "",
        const std::string& pwd = "");
    virtual ~DataSource();

    static std::string GetConnector(Connector);

    virtual void SetConnector(const std::string& c) final;
    virtual void SetDataSourceName(const std::string& dsn) final;
    virtual void SetUser(const std::string& uid) final;
    virtual void SetPassword(const std::string& pwd) final;
    virtual void SetConnectionName(const std::string& cn) final;
    
    virtual const std::string & GetConnector() const final;
    virtual const std::string & GetDataSourceName() const final;
    virtual const std::string & GetUser() const final;
    virtual const std::string & GetPassword() const final;
    virtual const std::string & GetConnectionName() const final;
    
    /// Return the connection string
    ///
    /// Format: DSN=<dsn>;UID=<uid>;PWD=<pwd>
    /// Format: DSN=<dsn>;UID=<uid>;PWD=<pwd>;CONNECTIONNAME=<connection name>
    virtual const std::string Get() const final;

protected:

    /// POCO connector string.
    ///
    /// poco::data::odbc::Connector::KEY
    /// poco::data::sqlite::Connector::KEY
    std::string connector;
    std::string dataSourceName;
    std::string userId;
    std::string userPassword;
    std::string connectionName;
};

} // data
} // negerns

#endif // NEGERNS_DATA_DATASOURCE_H
