/*******************************************************************************
 * File:    negerns/data/sqlstatement.h
 * Created: 14 Jul 2014 3:44 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DATA_SQLSTATEMENT_H
#define NEGERNS_DATA_SQLSTATEMENT_H

#include <string>
#include <negerns/core/vector.h>
#include <negerns/core/nameindex.h>
#include <negerns/core/member.h>
#include <negerns/core/declspec.h>

namespace negerns {
namespace data {

class NEGERNS_DECL SqlStatement
{
public:
    SqlStatement() = default;
    virtual ~SqlStatement() = default;

    virtual void Compose() { }

public:

    negerns::member::String sql;

    NameIndexContainer key;
};

/// \c SqlStatement class encapsulates an SQL statement string that is used in
/// database transactions. The statement is not checked for validity. The client
/// must ensure that it is well formed.
class NEGERNS_DECL Query : public SqlStatement
{
public:
    using SqlStatement::SqlStatement;

    virtual void Compose() override;

    void SaveColumns();
    void RestoreColumns();

public:

    negerns::Vector<std::string> select;
    negerns::Vector<std::string> from;
    negerns::Vector<std::string> where;
    negerns::Vector<std::string> group;
    negerns::Vector<std::string> having;
    negerns::Vector<std::string> order;

private:

    /// Buffer for saving and restoring columns in the SQL select clause.
    negerns::Vector<std::string> selectBuffer;
};

class NEGERNS_DECL Insert : public SqlStatement
{
public:
    using SqlStatement::SqlStatement;

    virtual void Compose() override { }

public:

    negerns::member::String table;
    negerns::Vector<std::string> column;
    negerns::Vector<std::string> value;
};

class NEGERNS_DECL Update : public SqlStatement
{
public:
    using SqlStatement::SqlStatement;

    virtual void Compose() override { }

public:

    negerns::member::String table;
    negerns::Vector<std::string> column;
    negerns::Vector<std::string> value;
};

class NEGERNS_DECL Delete : public SqlStatement
{
public:
    using SqlStatement::SqlStatement;

    virtual void Compose() override { }

public:

    negerns::member::String table;
    negerns::Vector<std::string> where;
};

class NEGERNS_DECL Procedure : public SqlStatement
{
public:
    using SqlStatement::SqlStatement;

    virtual void Compose() override { }

public:

    negerns::member::String procedure;
    negerns::Vector<std::string> column;
};

} // namespace data
} // namespace negerns

#endif // NEGERNS_DATA_SQLSTATEMENT_H
