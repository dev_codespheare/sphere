/*******************************************************************************
 * File:    negerns/data/dataaccess.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 30 Sep 2013 10:18 AM
 *
 * Comment: Data Access base class
 ******************************************************************************/

#ifndef NEGERNS_DATA_DATAACCESS_H
#define NEGERNS_DATA_DATAACCESS_H

#include <memory>
#include <Poco/Data/SessionPool.h>
#include <negerns/core/declspec.h>
#include <negerns/data/datasource.h>
#include <negerns/poco.h>

class poco::data::SessionPool;

namespace negerns {
namespace data {

class Transaction;

/// Ancestor of all data access objects. This class provides the framework
/// for all CRUD operations on one or more data sources.
///
/// ~~~~~~~~~~{.cpp}
/// DataSource ds;
/// ds.SetDataSourceName("sphere-ais");
/// ds.SetUser("user");
/// ds.SetPassword("password");
/// ds.SetConnector(poco::data::odbc::Connector::KEY);
/// ds.SetConnectionName("sphere ais");
/// 
/// DataAccess da;
/// if (DataAccess::Test(ds)) {
///     DataAccess::Add(ds, true);
/// }
///
/// DataAccess::Init();
///
/// if (DataAccess::IsInitialized()) {
///     auto trans = DataAccess::GetTransaction();
///     if (trans->IsConnected()) {
///         ...
///     }
///     auto ds = new DataStore(DataAccess::GetTransaction(0));
/// }
///
/// DataAccess::UnInit();
/// ~~~~~~~~~~
class NEGERNS_DECL DataAccess
{
public:
    friend class Transaction;

    DataAccess();
    virtual ~DataAccess();

    /// Initialize data sources.
    /// Data sources must have been added via DataAccess::DataSource::Add.
    ///
    /// \param min Minimum number of sessions to be maintained in the pool.
    /// \param max Maximum number of sessions to be created in the pool.
    /// \param idleTime Number of seconds before a session is automatically
    ///                 destroyed if there are more than the number of minimum
    ///                 number of sessions.
    ///
    /// \note This function must be called during application startup.
    ///
    /// \note To set a database feature, override this function. In the
    ///       override function, call this function first and acquire the
    ///       desired session and set the feature by calling
    ///       SessionPool::setFeature();
    ///
    /// \see DataAccess::DataSource::Add
    /// \see DataAccess::UnInit
    /// \see DataAccess::IsInitialize
    /// \see DataAccess::initialized
    static void Init(const int min = 1, const int max = 32, const int idleTime = 10);

    /// Uninitialize the session pool object.
    ///
    /// \note This function must be called during application exit.
    ///       After uninitialization, any call to DataAccess::IsInitialized
    ///       will return false.
    ///
    /// \see DataAccess::Init
    /// \see DataAccess::IsInitialized
    /// \see DataAccess::initialized
    static void UnInit();

    /// Returns the value of the initialization flag.
    ///
    /// This will be called by subclasses to check whether any data source
    /// operation is good to go. Note that this is different from checking
    /// the database connection which happens only after the data source has
    /// been checked.
    ///
    /// \see DataAccess::Init
    /// \see DataAccess::UnInit
    /// \see DataAccess::initialized
    static bool IsInitialized();

    /// Set the default session pool index to be used when acquiring a
    /// Transaction object via a call to DataAccess::GetTransaction().
    ///
    /// The default session pool index may also be set when adding a data
    /// source via \c DataAccess::Add.
    ///
    /// \see DataAccess::Add
    /// \see DataAccess::GetTransaction
    static void SetDefault(std::size_t n);

    /// Get a Transaction object from the session pool container using the
    /// default session pool index.
    ///
    /// To get a Transaction object not using the default session pool index,
    /// call DataAccess::GetTransaction and passing a session pool index
    /// argument.
    ///
    /// \see DataAccess::SetDefault.
    static Transaction* GetTransaction();

    /// Get a Transaction object from the session pool container using the
    /// specified session pool index.
    ///
    /// To get a Transaction object using the default session pool index,
    /// call DataAccess::GetTransaction without an argument.
    static Transaction* GetTransaction(std::size_t);

    /// Returns true if the specified session pool index is connected to the
    /// database. Otherwise, returns false.
    static bool IsConnected(std::size_t);

    /// Returns true if the session pool container is empty. Otherwise, false.
    static bool IsEmpty();

    /// Add a data source.
    /// Specify whether this data source will be the default session pool
    /// index when acquiring a Transaction object.
    ///
    /// \see DataAccess::datasources
    /// \see DataAccess::SetDefault.
    /// \see DataAccess::GetTranaction
    static void Add(const DataSource &, bool isDefault = false);

    /// Test a connection to the data source.
    /// Returns true on successful connection. Otherwise, returns false.
    static bool Test(const DataSource &);

    /// Get result of \c Test.
    static std::string GetTestResult();


private:

    /// Get a reference to a SessionPool specified by an index.
    static poco::data::SessionPool * GetSessionPool(std::size_t);

    /// Get a Session object from the default Session pool.
    static std::unique_ptr<poco::data::Session> GetSession();

    /// Get a Session object from the Session pool specified by an index.
    static std::unique_ptr<poco::data::Session> GetSession(std::size_t);

private:

    class DataAccessImp;

    static DataAccessImp* Implementation(DataAccessImp * = nullptr);

};

} // data
} // negerns

#endif // NEGERNS_DATA_DATAACCESS_H
