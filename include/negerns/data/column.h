/*******************************************************************************
 * File:    negerns/data/column.h
 * Created: 11 Jan 2013 12:23 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DATA_COLUMN_H
#define NEGERNS_DATA_COLUMN_H

#include <string>
#include <Poco/Data/MetaColumn.h>
#include <negerns/core/variant.h>
#include <negerns/core/declspec.h>
#include <negerns/poco.h>

namespace negerns {
namespace data {

/// Column data type enumeration.
/// This is a one to one mapping of Poco::Data::MetaColumn::ColumnDataType.
enum class NEGERNS_DECL DataType {
    Bool        = poco::data::MetaColumn::ColumnDataType::FDT_BOOL,
    Int8        = poco::data::MetaColumn::ColumnDataType::FDT_INT8,
    UInt8       = poco::data::MetaColumn::ColumnDataType::FDT_UINT8,
    Int16       = poco::data::MetaColumn::ColumnDataType::FDT_INT16,
    UInt16      = poco::data::MetaColumn::ColumnDataType::FDT_UINT16,
    Int32       = poco::data::MetaColumn::ColumnDataType::FDT_INT32,
    UInt32      = poco::data::MetaColumn::ColumnDataType::FDT_UINT32,
    Int64       = poco::data::MetaColumn::ColumnDataType::FDT_INT64,
    UInt64      = poco::data::MetaColumn::ColumnDataType::FDT_UINT64,
    Float       = poco::data::MetaColumn::ColumnDataType::FDT_FLOAT,
    Double      = poco::data::MetaColumn::ColumnDataType::FDT_DOUBLE,
    String      = poco::data::MetaColumn::ColumnDataType::FDT_STRING,
    WString     = poco::data::MetaColumn::ColumnDataType::FDT_WSTRING,
    Blob        = poco::data::MetaColumn::ColumnDataType::FDT_BLOB,
    Clob        = poco::data::MetaColumn::ColumnDataType::FDT_CLOB,
    Date        = poco::data::MetaColumn::ColumnDataType::FDT_DATE,
    Time        = poco::data::MetaColumn::ColumnDataType::FDT_TIME,
    Timestamp   = poco::data::MetaColumn::ColumnDataType::FDT_TIMESTAMP,
    Unknown     = poco::data::MetaColumn::ColumnDataType::FDT_UNKNOWN,
};



/// Column metadata.
///
///
/// \internal
/// Is this the best solution?
/// \li Inherit from poco::data::MetaColumn override poco::data::MetaColumn::type()
///     to return DataType
/// \endinternal
class NEGERNS_DECL Column
{
public:
    static DataType GetType(poco::data::MetaColumn::ColumnDataType type);
    static std::string GetTypeName(poco::data::MetaColumn::ColumnDataType type);
    static std::string GetTypeName(DataType type);
    static Variant GetDefault(const Column &p);

    Column();
    Column(const Column &);
    Column(Column &&);
    Column(const poco::data::MetaColumn &mc);
    virtual ~Column();

    Column& operator=(const Column &);
    Column& operator=(Column &&);

    virtual void Set(const poco::data::MetaColumn &mc) final;

    std::string GetTypeName();

    std::size_t     position;
    std::string     name;
    DataType        type;
    std::size_t     length;
    std::size_t     precision;
    bool            nullable;
};

} // namespace data
} // namespace negerns

#endif // NEGERNS_DATA_COLUMN_H
