/*******************************************************************************
 * File:    negerns/core/bufferstore.h
 * Created: 14 Feb 2014 6:26 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DATA_INTERNAL_BUFFERSTORE_H
#define NEGERNS_DATA_INTERNAL_BUFFERSTORE_H

#include <negerns/core/declspec.h>
#include <negerns/data/rowbuffer.h>
#include <negerns/data/columns.h>

namespace negerns {
namespace data {

/// BufferStore class contains row buffers. Row buffers are rows copied from a
/// \c DataStore object using \c DataStore::Detach. For now, existing rows can
/// only be modified.
///
/// \internal
/// Should inserted/deleted rows in a \c DataStore object be copied to the
/// \c BufferStore? Including the inserted/deleted rows from a \c DataStore
/// allows client code to exploit such feature like displaying inserted/deleted
/// rows for review/confirmation of the possible next operation.
/// \endinternal
class NEGERNS_DECL BufferStore : public NameIndexContainer
{
public:
    friend class DataStore;

    BufferStore();
    virtual ~BufferStore();

    bool IsModified() const;

    /// Column properties.
    data::Columns column;

    /// Modified rows buffer.
    ModifyRowBuffer original;

    /// Inserted rows buffer.
    //internal::InsertRowBuffer inserted;

    /// Modified rows buffer.
    ModifyRowBuffer modified;

    /// Deleted rows buffer.
    //internal::DeleteRowBuffer deleted;
};

} // namespace data
} // namespace negerns

#endif // NEGERNS_DATA_INTERNAL_BUFFERSTORE_H
