/*******************************************************************************
 * File:    negerns/data/irows.h
 * Created: 14 Feb 2014 6:26 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DATA_IROWS_H
#define NEGERNS_DATA_IROWS_H

#include <string>
#include <Poco/Data/RecordSet.h>
#include <negerns/core/variant.h>
#include <negerns/core/nameindex.h>
#include <negerns/core/declspec.h>
#include <negerns/data/row.h>

namespace negerns {
namespace data {

/// Interface for row column value.
/// A column value may be queried or modified using column name subscript.
class NEGERNS_DECL IRows : public NameIndexIFace<Variant>
{
public:

    IRows();
    IRows(poco::data::RecordSet *);
    virtual ~IRows();

    /// @{
    /// \name Read Operations

    /// Return the value in the specified row and column index.
    ///
    /// This function must be overridden by subclasses.
    virtual Variant & operator()(
        std::size_t r,
        std::size_t c);

    /// Return the value in the specified row index and column name.
    virtual Variant & operator()(
        std::size_t r,
        const std::string &s);

    /// Return the value in the specified row and column index.
    virtual Variant & Get(
        std::size_t r,
        std::size_t c);

    /// Return the value in the specified row index and column name.
    virtual Variant & Get(
        std::size_t r,
        const std::string &s);

    /// Return the value in the specified row and column index.
    /// If the value in the row and column is null then the specified default
    /// value is used instead.
    ///
    /// This function must be overridden by subclasses.
    virtual Variant & IfNull(
        std::size_t r,
        std::size_t c,
        const Variant &v);

    /// Return the value in the specified row index and column name.
    /// If the value in the row and column is null then the specified default
    /// value is used instead.
    virtual Variant & IfNull(
        std::size_t r,
        const std::string &s,
        const Variant &v);

    /// @}

    /// @{
    /// \name Write Operations

    /// Set the value in the specified row and column index.
    ///
    /// This function must be overridden by subclasses.
    virtual void operator()(
        std::size_t r,
        std::size_t c,
        const Variant &v);

    /// Set the value in the specified row index and column name.
    virtual void operator()(
        std::size_t r,
        const std::string &s,
        const Variant &v);

    /// @}

    /// Return \c true if the value in the \c row and \c column is null.
    virtual bool IsNull(std::size_t r, std::size_t c);

    /// Return \c true if the value in the \c row and \c column is null.
    virtual bool IsNull(std::size_t r, const std::string &s);

    /// Return the number of rows.
    virtual std::size_t RowCount();

    virtual std::size_t Find(std::size_t c, const Variant &);

#ifdef _DEBUG
    void LogRowValues(std::size_t);
#endif

protected:

    /// Internal row container object.
    ///
    /// \internal Reference to the \c RecordSet object of the \c DataStore class.
    ///
    /// \see InternalRowIFace::SetRecordSet
    poco::data::RecordSet * recordset;

    /// Internal buffer for rows.
    Rows rows;
};

} // namespace data
} // namespace negerns

#endif // NEGERNS_DATA_IROWS_H
