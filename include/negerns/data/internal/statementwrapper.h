/*******************************************************************************
 * File:    negerns/data/internal/statementwrapper.h
 * Created: 27 Jan 2014 3:48 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DATA_INTERNAL_STATEMENTWRAPPER_H
#define NEGERNS_DATA_INTERNAL_STATEMENTWRAPPER_H

#include <Poco/Data/Statement.h>
#include <negerns/core/declspec.h>
#include <negerns/poco.h>

namespace negerns {
namespace data {
namespace internal {

/// Wrapper for Poco::Data::Statement so protected functions can be exposed
/// publicly through this class.
///
/// Primarily used to expose column metadata.
class NEGERNS_DECL StatementWrapper : public poco::data::Statement
{
public:
    StatementWrapper(const poco::data::Statement &s);
    virtual ~StatementWrapper();

    /// Get column metadata.
    poco::data::MetaColumn GetMetaColumn(std::size_t pos);

    /// Get column metadata.
    poco::data::MetaColumn GetMetaColumn(const std::string &name);

    bool IsNull(std::size_t col, std::size_t row) const;

};

inline bool StatementWrapper::IsNull(std::size_t col, std::size_t row) const
{
	return poco::data::Statement::isNull(col, row);
}

} // namespace internal
} // namespace data
} // namespace negerns

#endif // NEGERNS_DATA_INTERNAL_STATEMENTWRAPPER_H
