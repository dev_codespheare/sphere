/*******************************************************************************
 * File:    negerns/data/row.h
 * Created: 24 May 2014 1:56 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DATA_ROW_H
#define NEGERNS_DATA_ROW_H

#include <map>
#include <Poco/Data/Row.h>
#include <negerns/core/declspec.h>

#include <negerns/poco.h>

namespace negerns {
namespace data {

typedef poco::data::Row Row;

typedef std::map<std::size_t, Row> Rows;

} // namespace data
} // namespace negerns

#endif // NEGERNS_DATA_ROW_H
