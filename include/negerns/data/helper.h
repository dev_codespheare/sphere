/*******************************************************************************
 * File:    negerns/data/helper.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 30 May 2014 4:18 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_DATA_HELPER_H
#define NEGERNS_DATA_HELPER_H

#include <negerns/negerns.h>
#include <negerns/core/system.h>
#include <negerns/core/declspec.h>
#include <negerns/data/row.h>
#include <negerns/data/recordset.h>
#include <negerns/data/datastore.h>

namespace negerns {
namespace data {

class NEGERNS_DECL Helper
{
public:
    Helper();
    virtual ~Helper();
#if 0
    static negerns::data::Row GetRow(const std::string &sql);

    static std::unique_ptr<negerns::data::RecordSet> GetRecordSet(const std::string &sql);

    static negerns::data::DataStore * CreateDataStore();
#endif
};

} // namespace data
} // namespace negerns

#endif // NEGERNS_DATA_HELPER_H
