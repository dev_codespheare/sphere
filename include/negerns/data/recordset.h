/*******************************************************************************
 * File:    negerns/data/recordset.h
 * Created: 14 Feb 2014 8:56 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_DATA_RECORDSET_H
#define NEGERNS_DATA_RECORDSET_H

#include <string>
#include <Poco/Data/RecordSet.h>
#include <negerns/data/row.h>
#include <negerns/core/variant.h>
#include <negerns/core/declspec.h>
#include <negerns/data/irows.h>

#include <negerns/poco.h>

namespace negerns {
namespace data {

/// RecordSet
class NEGERNS_DECL RecordSet : public IRows
{
public:

    RecordSet();
    RecordSet(poco::data::RecordSet *);
    virtual ~RecordSet();

    /// Set the reference to the \c DataStore::recordset object.
    void SetRecordSet(poco::data::RecordSet *rs);

    /// Return the Row object in the specified row index.
    Row & operator()(std::size_t i);

    /// Return the Row object in the specified row index.
    Row & GetRow(const std::size_t i);

    /// Return the value in the specified row and column index.
    virtual Variant & operator()(
        std::size_t r,
        std::size_t c) override;

    /// Return the value in the specified row index and column name.
    virtual Variant & operator()(
        std::size_t r,
        const std::string &s) override;

    /// Return the value in the specified row and column index.
    virtual Variant & Get(
        std::size_t r,
        std::size_t c) override;

    /// Return the value in the specified row index and column name.
    virtual Variant & Get(
        std::size_t r,
        const std::string &s) override;

    /// Return the value in the specified row and column index.
    /// If the value in the row and column is null then the specified default
    /// value is used instead.
    virtual Variant & IfNull(
        std::size_t r,
        std::size_t c,
        const Variant &v) override;

    /// Return the value in the specified row index and column name.
    /// If the value in the row and column is null then the specified default
    /// value is used instead.
    virtual Variant & IfNull(
        std::size_t r,
        const std::string &s,
        const Variant &v) override;

    virtual bool IsNull(std::size_t r, std::size_t c) override;
    virtual bool IsNull(std::size_t r, const std::string &s) override;

    /// Return the number of rows in the internal buffer.
    virtual std::size_t RowCount() override;

    virtual std::size_t Find(std::size_t c, const Variant &) override;

    /// Return a \c Row with blank column values. The \c Row columns are
    /// acquired from the internal storage. The internal storage must have been
    /// 'initialized' by a database retrieve operation.
    ///
    /// \internal
    /// Internal storage is Poco RecordSet. The Poco RecordSet is initialized by
    /// an empty database retrieve operation; a retrieve operation returning no
    /// rows.
    /// \endinternal
    Row GetRow();

private:

    /// Set the value in the specified row and column index.
    ///
    /// \note This function is overridden and making it private because this
    /// class does not implement any data modification.
    ///
    /// We override the function and make it private because row/column values
    /// are readonly.
    virtual void operator()(
        std::size_t r,
        std::size_t c,
        const Variant &v) override { }

    /// Set the value in the specified row index and column name.
    ///
    /// \note This function is overridden and making it private because this
    /// class does not implement any data modification.
    ///
    /// We override the function and make it private because row/column values
    /// are readonly.
    virtual void operator()(
        std::size_t r,
        const std::string &s,
        const Variant &v) override { }

};

} // namespace data
} // namespace negerns

#endif // NEGERNS_DATA_RECORDSET_H
