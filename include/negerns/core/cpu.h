/*******************************************************************************
 * File:    negerns/core/cpu.h
 * Created: 16 Mar 2013 3:29 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment: CPU information
 ******************************************************************************/

#ifndef NEGERNS_CORE_CPU_H
#define NEGERNS_CORE_CPU_H

#include <wx/string.h>
#include <negerns/core/declspec.h>

#ifdef __WXMSW__
#include <windows.h>
#include <intrin.h>
#pragma comment(lib, "user32.lib")
#endif

namespace negerns {

class NEGERNS_DECL CPUInfo
{
public:
    CPUInfo();
    wxString GetBrandString() const { return CPUBrandString; }
#if EXCLUDE_TEST
    wxString GetArchitecture() const {
        if (architecture == 9) {
            return "64-bit";
        } else if (architecture == 6) {
            return "Intel Itanium";
        } else if (architecture == 0) {
            return "32-bit";
        } else {
            return "Unknown";
        }
    }
#endif
    unsigned long GetLogicalProcessorCount() const { return processorCount; }
private:
    wxString CPUBrandString;
    unsigned long processorCount;
#if EXCLUDE_TEST
    unsigned short architecture;
#endif
    unsigned short level;

#ifdef __WXMSW__
    void GetWindowsCPUInfo();
#endif
};

} //_ namespace negerns

#endif //_ NEGERNS_CORE_CPU_H
