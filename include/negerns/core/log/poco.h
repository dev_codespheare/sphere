/*******************************************************************************
 * File:    negerns/core/log/poco.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 30 Jul 2014 3:27 AM
 *
 * Comment: POCO Logger
 ******************************************************************************/
#if 0

#ifndef NEGERNS_CORE_LOG_POCO_H
#define	NEGERNS_CORE_LOG_POCO_H
#include <Poco/Logger.h>
#include <negerns/core/log/ilogger.h>
#include <negerns/core/declspec.h>

#include <negerns/poco.h>

namespace negerns {
namespace log {

class NEGERNS_DECL Poco : public ILogger
{
public:
    Poco();
    virtual ~Poco();

    //! \copydoc ILogger::Init()
    //!
    //! \see ILogger::Init
    virtual void Init() override;

    //! \copydoc ILogger::Shutdown()
    //!
    //! \see ILogger::Shutdown
    virtual void Shutdown() override;

    virtual void Log(Priority p, const std::string &s) override;
    virtual void Log(Priority p, const std::string &s,
        const char* file,
        int line) override;

private:

    static poco::Logger *logger;

    // The following variables are used log message formatting.

    //! Filenames starts at column 120.
    static const int fileColumn = 120;

    //! Length of log message.
    //! Message will be truncated up to the maximum length.
    static const int messageLength = 400;

    //! Multiple line log messages ensures that the next line is aligned
    //! with the first line text.
    static const int indent = 34;
};

} //_ namespace log
} //_ namespace negerns
#endif //_ NEGERNS_CORE_LOG_POCO_H

#endif
