/*******************************************************************************
 * File:    negerns/core/log/ilogger.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 30 Jul 2014 3:29 AM
 *
 * Comment: Logger base class
 ******************************************************************************/

#ifndef NEGERNS_CORE_LOG_ILOGGER_H
#define	NEGERNS_CORE_LOG_ILOGGER_H

#include <string>

#include <negerns/core/declspec.h>

namespace negerns {
namespace log {

enum Priority
{
	Trace = 0,          // A tracing message. This is the lowest priority.
	Debug,              // A debugging message.
	Information,        // An informational message, usually denoting the
                        // successful completion of an operation.
    Info = Information,
	Notice,             // A notice, which is an information with just a higher
                        // priority.
	Warning,            // A warning. An operation completed with an unexpected
                        // result.
	Error,              // An error. An operation did not complete successfully,
                        // but the application as a whole is not affected.
	Critical,           // A critical error. The application might not be able
                        // to continue running successfully.
	Fatal               // A fatal error. The application will most likely
                        // terminate. This is the highest priority.
};

NEGERNS_DECL
std::string priority_abbr(Priority);

NEGERNS_DECL
std::string priority_name(Priority);

//! Minimal log message.
//!
//! \todo Expand and make it compatible with other logging libraries.
struct Message {
    std::string     source;
    log::Priority   priority;
    std::string     message;
    const char*     file;
	int             line;
    Message(const std::string &src,
        log::Priority p,
        const std::string &msg) :
        source(src), priority(p), message(msg), file(""), line(0)
    { }
    Message(const std::string &src,
        log::Priority p,
        const std::string &msg,
        const char *f,
        int n) :
        source(src), priority(p), message(msg), file(f), line(n)
    { }
};

//! Logger interface for the internal logging mechanism.
class NEGERNS_DECL ILogger
{
public:
    static std::string format_message(const std::string &, int = 0);

    ILogger() = default;
    virtual ~ILogger() = default;

    //! Initialize the logging component.
    //!
    //! \internal
    //! The initialization is intentionally separated from the creation of the
    //! \c Logger object. This is to allow client code to configure and setup
    //! the \c Logger object before the actual initialization.
    //! \endinternal
    virtual void Init() { }

    //! Shutdown the logging component.
    virtual void Shutdown() { }

    virtual void Log(Priority, const std::string &) { }
    virtual void Log(Priority, const std::string &, const char*, int) { }

    //! Set the path and file name.
    //! The filename is automatically appended with the default log file
    //! extension (.log).
    void set_file(const std::string &, const std::string &);

    std::string get_path() const { return path; }
    void set_path(const std::string &s) { path = s; }
    std::string get_filename() const { return filename; }
    void set_filename(const std::string &s) { filename = s; }
    std::string get_full_path() const { return path + filename; }

private:

    //! Absolute path for log output.
    //!
    //! If this is empty then the log outputs are sent to the path
    //! of the executable file.
    //!
    //! \see SetPath, GetPath
    std::string path;

    //! Log output filename.
    //! File extension is automatically added using the default log file
    //! extension (.log).
    std::string filename;
};

} //_ namespace log
} //_ namespace negerns

#endif //_ NEGERNS_CORE_LOG_ILOGGER_H
