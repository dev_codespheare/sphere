/*******************************************************************************
 * File:    negerns/core/log/boost.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 30 Jul 2014 3:23 AM
 *
 * Comment: Boost Logger
 ******************************************************************************/

#ifndef NEGERNS_CORE_LOG_BOOST_H
#define	NEGERNS_CORE_LOG_BOOST_H

#include <boost/log/trivial.hpp>
#include <boost/log/sources/severity_logger.hpp>

#include <negerns/core/log/ilogger.h>
#include <negerns/core/declspec.h>

//. TODO: Allow more information to be sent to the log.

#if 0
// User-defined severity
enum severity_level
{
	trace = negerns::log::Trace,
    debug = negerns::log::Debug,
    info = negerns::log::Information,
    notice = negerns::log::Notice,
    warning = negerns::log::Warning,
    error = negerns::log::Error,
    critical = negerns::log::Critical,
    fatal = negerns::log::Fatal
};
#endif

using namespace boost::log;

namespace negerns {
namespace log {

class NEGERNS_DECL Boost : public ILogger
{
public:
    Boost();
    virtual ~Boost();

    virtual void Init() override;
    virtual void Shutdown() override;

    virtual void Log(Priority p, const std::string &s) override;
    virtual void Log(Priority p, const std::string &s,
        const char* file,
        int line) override;

private:
    static sources::severity_logger<trivial::severity_level> *logger;
};

} //_ namespace log
} //_ namespace negerns

#endif //_ NEGERNS_CORE_LOG_BOOST_H
