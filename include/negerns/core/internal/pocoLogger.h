/*******************************************************************************
 * File:    negerns/core/internal/pocologger.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 28 Feb 2014 9:48 AM
 *
 * Comment: Logging Facility
 ******************************************************************************/

#ifndef NEGERNS_CORE_INTERNAL_POCOLOGGER_H
#define	NEGERNS_CORE_INTERNAL_POCOLOGGER_H

#include <Poco/Logger.h>
#include <negerns/core/declspec.h>
#include <negerns/poco.h>

namespace negerns {
namespace core {
namespace internal {

class NEGERNS_DECL PocoLogger
{
public:
    PocoLogger();
    virtual ~PocoLogger();
public:
    poco::Logger *logger = nullptr;
};

} //_ namespace internal
} //_ namespace core
} //_ namespace negerns

#endif //_ NEGERNS_CORE_INTERNAL_POCOLOGGER_H
