/*******************************************************************************
 * File:    negerns/core/internal/wxlogger.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 28 Feb 2014 9:49 AM
 *
 * Comment: Logging Facility
 ******************************************************************************/

#ifndef NEGERNS_CORE_INTERNAL_WXLOGGER_H
#define	NEGERNS_CORE_INTERNAL_WXLOGGER_H

#include <negerns/core/declspec.h>

namespace negerns {
namespace core {
namespace internal {
#if 0
class NEGERNS_DECL wxLogger
{
public:
    wxLogger();
    virtual ~wxLogger();
};
#endif
} //_ namespace internal
} //_ namespace core
} //_ namespace negerns

#endif //_ NEGERNS_CORE_INTERNAL_WXLOGGER_H
