/*******************************************************************************
 * File:    negerns/core/outputsink.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 6 Mar 2014 4:29 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_OUTPUT_SINK_H
#define NEGERNS_CORE_OUTPUT_SINK_H

#include <string>
#include <vector>
#include <negerns/core/declspec.h>

namespace negerns {

class NEGERNS_DECL OutputSink
{
public:
    OutputSink();
    virtual ~OutputSink();

    virtual void Init() { }
    virtual void Shutdown() { }
    virtual void Send(const std::string &) { }
    virtual void Clear() { }
};

} //_ namespace negerns

#endif //_ NEGERNS_CORE_OUTPUT_SINK_H
