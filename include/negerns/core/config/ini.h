/*******************************************************************************
 * File:    negerns/core/config/ini.h
 * Created: 30 Aug 2014 12:05 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_CONFIG_INI_H
#define NEGERNS_CORE_CONFIG_INI_H

#include <fstream>
#include <negerns/core/config/base.h>

namespace negerns {
namespace config {

class NEGERNS_DECL Ini : public Base
{
public:
    Ini();
    Ini(const std::string &filename);
    Ini(const std::string &filename, bool is_updateable);
    virtual ~Ini();

    //! \copydoc Base::read
    virtual void read() override;

    //! \copydoc Base::save
    virtual void save() override;

private:

    std::string section;
};

} //_ namespace config
} //_ namespace negerns

#endif //_ NEGERNS_CORE_CONFIG_INI_H
