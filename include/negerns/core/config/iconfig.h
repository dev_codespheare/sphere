/*******************************************************************************
 * File:    negerns/core/config/iconfig.h
 * Created: 30 Aug 2014 8:10 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_CONFIG_ICONFIG_H
#define NEGERNS_CORE_CONFIG_ICONFIG_H

#include <string>
#include <negerns/core/declspec.h>

namespace negerns {
namespace config {

class NEGERNS_DECL IConfig
{
    //! Returns true if the specified key exists.
    virtual bool has_key(const std::string &key) = 0;

    //! Get the value of the key as a \c boolean.
    //! If the value cannot be converted to a boolean then the \c default value
    //! is returned. The following rules apply for the conversion to boolean
    //! type:
	//!   - non zero values is converted to true and zero to false
	//!   - strings true, yes, on are converted to true and strings false, no,
    //!     off to false
    virtual bool get_bool(const std::string &key, const bool def) = 0;

    //! Get the value of the key as a \c std::string.
    //! If the value cannot be converted to a boolean then the \c default value
    //! is returned.
    virtual std::string get_string(const std::string &key, const std::string &def) = 0;

    //! Get the value of the key as an \c integer.
    //! If the value cannot be converted to a boolean then the \c default value
    //! is returned.
	virtual int get_int(const std::string &key, const int def) = 0;

    //! Get the value of the key as an \c unsigned \c integer.
    //! If the value cannot be converted to a boolean then the \c default value
    //! is returned.
	virtual unsigned int get_uint(const std::string &key, const unsigned int def) = 0;

    //! Get the value of the key as a \c double.
    //! If the value cannot be converted to a boolean then the \c default value
    //! is returned.
	virtual double get_double(const std::string &key, const double def) = 0;

    //! Set new key value.
    virtual bool set(const std::string &key, bool value) = 0;

    //! Set new key value.
    virtual bool set(const std::string &key, const std::string &value) = 0;

    //! Set new key value.
	virtual bool set(const std::string &key, int value) = 0;

    //! Set new key value.
	virtual bool set(const std::string &key, unsigned int value) = 0;

    //! Set new key value.
	virtual bool set(const std::string &key, double value) = 0;

    //! Read key/value pairs from file.
    virtual void read() = 0;

    //! Save key/value pairs to file.
    //! Does nothing if the key/value pairs cannot be written to file.
    //!
    //! \see SetUpdateable
    virtual void save() = 0;
};

} //_ namespace config
} //_ namespace negerns

#endif //_ NEGERNS_CORE_CONFIG_ICONFIG_H