/*******************************************************************************
 * File:    negerns/core/config/base.h
 * Created: 30 Aug 2014 12:06 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_CONFIG_BASE_H
#define NEGERNS_CORE_CONFIG_BASE_H

#include <string>
#include <map>
#include <negerns/core/string.h>
#include <negerns/core/config/iconfig.h>
#include <negerns/core/declspec.h>

namespace negerns {
namespace config {

typedef std::map<std::string, std::string> KeyValues;
typedef KeyValues::iterator KeyValuesIter;

class NEGERNS_DECL Base : public IConfig
{
public:
    Base();
    Base(const std::string &filename);
    Base(const std::string &filename, bool is_updateable);
    virtual ~Base();

    virtual bool find(const std::string &key, KeyValues::iterator &pos) final;

    virtual bool get_bool(KeyValues::iterator pos, bool def) final;
    virtual std::string get_string(KeyValues::iterator pos) final;
    virtual int get_int(KeyValues::iterator pos, int def) final;
    virtual unsigned int get_uint(KeyValues::iterator pos, unsigned int def) final;
    virtual double get_double(KeyValues::iterator pos, double def) final;

    virtual void set(KeyValues::iterator pos, bool value) final;
    virtual void set(KeyValues::iterator pos, const std::string &) final;
    virtual void set(KeyValues::iterator pos, int value) final;
    virtual void set(KeyValues::iterator pos, unsigned int value) final;
    virtual void set(KeyValues::iterator pos, double value) final;

    //! \copydoc IConfig::has_key
    virtual bool has_key(const std::string &key) override;

    //! \copydoc IConfig::get_bool
    virtual bool get_bool(const std::string &key, const bool def) override;

    //! \copydoc IConfig::get_string
    virtual std::string get_string(const std::string &key, const std::string &def) override;

    //! \copydoc IConfig::get_int
    virtual int get_int(const std::string &key, const int def) override;

    //! \copydoc IConfig::get_uint
    virtual unsigned int get_uint(const std::string &key, const unsigned int def) override;

    //! \copydoc IConfig::get_double
    virtual double get_double(const std::string &key, const double def) override;

    //! \copydoc IConfig::set
    virtual bool set(const std::string &key, bool value) override;

    //! \copydoc IConfig::set
    virtual bool set(const std::string &key, const std::string& value) override;

    //! \copydoc IConfig::set
    virtual bool set(const std::string &key, int value) override;

    //! \copydoc IConfig::set
    virtual bool set(const std::string &key, unsigned int value) override;

    //! \copydoc IConfig::set
    virtual bool set(const std::string &key, double value) override;

    //! \copydoc IConfig::read
    virtual void read() override { }

    //! \copydoc IConfig::save
    virtual void save() override { }

    virtual void set_filename(const std::string &f) final;

    virtual std::string get_filename() const final;

    //! Set to true if the key/value pairs is written to file.
    //!
    //! \see is_updateable
    virtual void set_updateable(bool b) final;

    //! Returns the status whether the key/value pairs could be written to file.
    //!
    //! \see set_updateable
    virtual bool is_updateable() const final;

#ifdef _DEBUG

    void set_key_values(KeyValues kv) {
        keyvalues = kv;
    }

    KeyValues get_key_values() {
        return keyvalues;
    }

#endif

protected:

    KeyValues keyvalues;

private:

    std::string filename;

    bool updateable;
};



struct NEGERNS_DECL ConfigItem
{
    ConfigItem(Base *ptr, KeyValues::iterator p) :
        ptr(ptr), pos(p)
    { }
    Base *ptr;
    KeyValues::iterator pos;
};



// -------------------------------------------------------------------------



inline
bool Base::get_bool(KeyValues::iterator pos, bool def)
{
    unsigned long n;
    return negerns::string::stoul(std::string(pos->second), n) ? n > 0 : def;
}

inline
std::string Base::get_string(KeyValues::iterator pos)
{
    return pos->second;
}

inline
int Base::get_int(KeyValues::iterator pos, int def)
{
    int n = 0;
    return negerns::string::stoi(std::string(pos->second), n) ? n : def;
}

inline
unsigned int Base::get_uint(KeyValues::iterator pos, unsigned int def)
{
    unsigned int n = 0;
    return negerns::string::stoui(std::string(pos->second), n) ? n : def;
}

inline
double Base::get_double(KeyValues::iterator pos, double def)
{
    double n = 0.0;
    try {
        n = std::stod(std::string(pos->second));
    } catch (const std::invalid_argument &e) {
        return def;
    }
    return n;
}

inline
void Base::set(KeyValues::iterator pos, bool value)
{
    pos->second = value;
}

inline
void Base::set(KeyValues::iterator pos, const std::string &value)
{
    pos->second = value;
}

inline
void Base::set(KeyValues::iterator pos, int value)
{
    pos->second = value;
}

inline
void Base::set(KeyValues::iterator pos, unsigned int value)
{
    pos->second = value;
}

inline
void Base::set(KeyValues::iterator pos, double value)
{
    pos->second = value;
}

inline
void Base::set_filename(const std::string &f)
{
    filename = f;
}

inline
std::string Base::get_filename() const
{
    return filename;
}

inline
void Base::set_updateable(bool b)
{
    updateable = b;
}

inline
bool Base::is_updateable() const
{
    return updateable;
}

} //_ namespace config
} //_ namespace negerns

#endif //_ NEGERNS_CORE_CONFIG_BASE_H
