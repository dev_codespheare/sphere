/*******************************************************************************
 * File:    negerns/core/config/property.h
 * Created: 30 Aug 2014 4:49 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_CONFIG_PROPERTY_H
#define NEGERNS_CORE_CONFIG_PROPERTY_H

#include <fstream>
#include <negerns/core/config/base.h>

namespace negerns {
namespace config {

//! Java-style properties file.
class NEGERNS_DECL Property : public Base
{
public:
    Property();
    Property(const std::string &filename);
    Property(const std::string &filename, bool is_updateable);
    virtual ~Property();

    //! \copydoc Base::read
    virtual void read() override;

    //! \copydoc Base::save
    virtual void save() override;

private:

    int read_char(std::istream &);
};

} //_ namespace config
} //_ namespace negerns

#endif //_ NEGERNS_CORE_CONFIG_PROPERTY_H
