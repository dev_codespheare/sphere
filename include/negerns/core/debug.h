/*******************************************************************************
 * File:    negerns/core/debug.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 10 Jul 2014 9:31 PM
 *
 * Comment:
 * To Do:   Add static assertions for release build.
 ******************************************************************************/

#ifndef NEGERNS_CORE_DEBUG_H
#define NEGERNS_CORE_DEBUG_H

#include <iostream>
#include <vector>
#include <boost/logic/tribool.hpp>

#include <negerns/core/noop.h>
#include <negerns/core/string.h>
#include <negerns/core/var.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace assert {
namespace {

//! Assertion state.
//!
//! The function holds a flag used by the assertion macros (ASSERT and FAIL).
//! The flag is queried to determine whether the assertion is to proceed or
//! not.
//!
//! \see enable
//! \see is_enabled
bool enable_state(boost::logic::tribool b = boost::logic::indeterminate);

//! Assertion GUI interface prompting state.
//!
//! The function holds a flag used by the assertion macros (ASSERT and FAIL).
//! The flag is queried to determine whether the graphical user interface will
//! be used to prompt the user about the assertion or not. If the state is not
//! to show/use the graphical user interface then the assertion is sent to the
//! standard output.
//!
//. TODO: Should we set where to send the assertion message (stdout stderr)?
bool use_gui_state(boost::logic::tribool b = boost::logic::indeterminate);

} //_ anonymous namespace
} //_ namespace assert
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace assert {

enum class Status {
    Enable,
    Disable
};

enum class Output {
    GUI,
    Console
};

//! Enable/Disable assertions.
NEGERNS_DECL
void enable(bool b);

//! Enable/Disable assertions.
NEGERNS_DECL
void status(Status);

//! Returns whether assertions are enabled or disabled.
NEGERNS_DECL
bool is_enabled();

//! Set whether assertions use the graphical user interface or console to
//! display assertion information.
NEGERNS_DECL
void use_gui(bool b);

//! Set whether assertions use the graphical user interface or console to
//! display assertion information.
NEGERNS_DECL
void output(Output);

//! Returns whether the graphical user interace is used to display the
//! assertion information.
NEGERNS_DECL
bool is_gui();

//! Returns whether the console is used to display the
//! assertion information.
NEGERNS_DECL
bool is_console();

enum class AssertLevel
{
    Test = 0,
    Warning,
    Debug,
    Error,
    Fatal
};

NEGERNS_DECL
void process(
    AssertLevel,
    const char *file,
    int line,
    const char *func,
    const char *cond,
    std::vector<::negerns::Var> &&);

} //_ namespace assert
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {

//! Queries debug or release build configuration state.
//!
//! It is meant to be used in conditional expressions for convenience instead of wrapping
//! statement(s) between conditional compilation blocks.
//!
//! ~~~~~~~~~~{.cpp}
//! if (negerns::debug()) foo();
//! // instead of
//! #ifdef DEBUG_MODE
//! foo();
//! #endif
//! ~~~~~~~~~~
NEGERNS_DECL
bool is_debug();

//! Returns true if the application is currently running within a debugger.
NEGERNS_DECL
bool is_debugging();

template <typename... A>
void assert_(
    ::negerns::assert::AssertLevel level,
    const char *file,
    int line,
    const char *func,
    const char *cond,
    A&&... args)
{
    auto count = sizeof...(args);
    if (count == 0) {
        std::vector<::negerns::Var> v;
        ::negerns::assert::process(level, file, line, func, cond, std::move(v));
    } else {
#if 0
        std::vector<::negerns::Var> v;
        ::negerns::string::format::expand_args_f(v, std::forward<A>(args)...);
        ::negerns::assert::process(level, file, line, func, cond, std::move(v));
#endif
        ::negerns::assert::process(level, file, line, func, cond, {args...});
    }
}

} //_ namespace negerns



// -----------------------------------------------------------------------------



//! Warning macro for debug builds only.
//!
//! Generates a warning when the \c condition evaluates to \c false. This macro
//! is excluded in release builds.
//!
//! The variable number of arguments after the \c condition argument is for
//! message formatting. Message formatting accepts a \c format string and a
//! variable number of \c format string arguments.
//!
//! Example:
//! ~~~~~~~~~~{.cpp}
//! WARN(i == 0);
//! WARN(i > 0, "Index must be greater than zero.");
//! WARN(i < n, "Index (%1) must be less than n (%2).", i, n);
//! ~~~~~~~~~~
#ifdef _DEBUG
#define WARNING(cond, ...)                                              \
    ((cond) ? static_cast<void>(0) : ::negerns::assert_(                \
        ::negerns::assert::AssertLevel::Warning,                        \
        __FILE__, __LINE__, __FUNCSIG__, #cond, __VA_ARGS__))
#else
#define WARNING(cond, ...) NOOP(cond)
#endif

//! Debug macro for debug builds only.
//!
//! Generates a debug message when the \c condition evaluates to \c false.
//! This macro is excluded in release builds.
//!
//! The variable number of arguments after the \c condition argument is for
//! message formatting. Message formatting accepts a \c format string and a
//! variable number of \c format string arguments.
//!
//! Example:
//! ~~~~~~~~~~{.cpp}
//! DEBUG(i == 0);
//! DEBUG(i > 0, "Index must be greater than zero.");
//! DEBUG(i < n, "Index (%1) must be less than n (%2).", i, n);
//! ~~~~~~~~~~
#ifdef _DEBUG
#define DEBUG(cond, ...)                                                \
    ((cond) ? static_cast<void>(0) : ::negerns::assert_(                \
        ::negerns::assert::AssertLevel::Debug,                          \
        __FILE__, __LINE__, __FUNCSIG__, #cond, __VA_ARGS__))
#else
#define DEBUG(cond, ...) NOOP(cond)
#endif

//! Assert macro.
//!
//! Generates an assertion when the \c condition evaluates to \c false. This
//! macro is always evaluated even in release builds.
//!
//! The variable number of arguments after the \c condition argument is for
//! message formatting. Message formatting accepts a \c format string and
//! a variable number of \c format string arguments.
//!
//! Example:
//! ~~~~~~~~~~{.cpp}
//! ASSERT(i == 0);
//! ASSERT(i > 0, "Index must be greater than zero.");
//! ASSERT(i < n, "Index (%1) must be less than n (%2).", i, n);
//! ~~~~~~~~~~
#define ASSERT(cond, ...)                                               \
    ((cond) ? static_cast<void>(0) : ::negerns::assert_(                \
        ::negerns::assert::AssertLevel::Error,                          \
        __FILE__, __LINE__, __FUNCSIG__, #cond, __VA_ARGS__))

//! Fatal macro.
//!
//! Generates a fatal assertion when the \c condition evaluates to \c false.
//! This macro is always evaluated even in release builds.
//!
//! The variable number of arguments after the \c condition argument is for
//! message formatting. Message formatting accepts a \c format string and
//! a variable number of \c format string arguments.
//!
//! Example:
//! ~~~~~~~~~~{.cpp}
//! FATAL(i == 0);
//! FATAL(i > 0, "Index must be greater than zero.");
//! FATAL(i < n, "Index (%1) must be less than n (%2).", i, n);
//! ~~~~~~~~~~
#define FATAL(cond, ...)                                                \
    ((cond) ? static_cast<void>(0) : ::negerns::assert_(                \
        ::negerns::assert::AssertLevel::Fatal,                          \
        __FILE__, __LINE__, __FUNCSIG__, #cond, __VA_ARGS__))

//! Fail macro.
//!
//! Always generates an assertion. This macro is always executed even in
//! release builds.
//!
//! The variable number of arguments is for message formatting. Message
//! formatting accepts a \c format string and a variable number of \c format
//! string arguments.
//!
//! Example:
//! ~~~~~~~~~~{.cpp}
//! FAIL();
//! FAIL("Should not reach this execution path.");
//! FAIL("Should not reach this execution path."
//!      "Index (%1) must be less than n (%2)", i, n);
//! ~~~~~~~~~~
#define FAIL(...)                                                       \
    ::negerns::assert_(::negerns::assert::AssertLevel::Fatal,           \
        __FILE__, __LINE__, __FUNCSIG__, "", __VA_ARGS__)

//! No switch default
//!
//! https://msdn.microsoft.com/en-us/library/1b3fsfxw%28v=vs.100%29.aspx
//!
//! Example:
//! ~~~~~~~~~~{.cpp}
//! switch(p) {
//!     case 1:
//!         func1(1);
//!         break;
//!     case 2:
//!         func1(-1);
//!         break;
//!     default:
//!         NO_SWITCH_DEFAULT;
//! }
//! ~~~~~~~~~~
#ifdef DEBUG
# define NO_SWITCH_DEFAULT  FAIL()
#else
# if defined(__GNUC__)
# define NO_SWITCH_DEFAULT  __builtin_unreachable()
# elif defined(__clang__)
# define NO_SWITCH_DEFAULT  __builtin_unreachable()
# elif defined(_MSC_VER)
# define NO_SWITCH_DEFAULT  __assume(0)
# endif
#endif



#endif //_ NEGERNS_CORE_DEBUG_H
