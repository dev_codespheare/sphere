/*******************************************************************************
 * File:    negerns/core/identifier.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 11 Jul 2013 2:11 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_IDENTIFIER_H
#define NEGERNS_CORE_IDENTIFIER_H

#include <string.h>
#ifdef _DEBUG
#include <map>
#endif
#include <negerns/core/string.h>
#include <negerns/core/var.h>

#include <negerns/core/declspec.h>

namespace negerns {

//! Unique identifier class.
//!
//! It implements a unique identifier as a combination of a base and a suffix.
//! It supports multiple instantiaions of a a class. The class shares the same
//! base identifier but the instances provides the suffix to uniquely identify
//! earch object.
//!
//! Format: base_suffix
//!         base_suffix1_suffix2 ...
//!
class NEGERNS_DECL Identifier
{
public:

    Identifier();
    Identifier(const Identifier&);
    Identifier(const std::string &base, const std::string &suffix = "",
        bool unique = true);
    virtual ~Identifier();

    //! Set the unique identifier string.
    virtual void set(const std::string &base, const std::string &suffix = "",
        bool unique = true) final;

    //! Return the unique identifier string.
    //! If a suffix is specified, then the unique identifier returned is
    //! suffixed with the specified string.
    virtual std::string get(const std::string &suffix = "") const final;

    //! Return the unique identifier string.
    //! The unique identifier returned is suffixed with the number, converted
    //! to string.
    virtual std::string get(std::size_t) const final;

    //! Return the unique identifier string.
    //! The unique identifier returned is suffixed with the number, converted
    //! to string.
    virtual std::string get(const negerns::Var &) const final;

    //! Return the unique identifier string.
    //! The unique identifier returned is suffixed with the concatenation of
    //! the contents of the \c Variant argument converted to string.
    virtual std::string get(const negerns::Vars &) const final;

    //! Returns true if the unique identifier string is empty.
    virtual bool empty() const final;

    //! Set the base identifier string.
    virtual void set_base(const std::string &) final;

    //! Return the base identifier string.
    virtual std::string get_base() const final;

    //! Set the specified string as the suffix of the base identifier.
    virtual void set_suffix(const std::string &) final;

    //! Set the specified number, converted to string, as the suffix of the
    //! base identifier.
    virtual void set_suffix(std::size_t) final;

    //! Set the specified number, converted to string, as the suffix of the
    //! base identifier.
    virtual void set_suffix(const negerns::Vars &) final;

    //! Return the suffix string.
    virtual std::string get_suffix() const final;

    //! Tag the identifier to be unique.
    virtual void set_unique(bool) final;

    //! Returns true if the identifier is tagged to be unique.
    virtual bool is_unique() const final;

    //! Returns true if the specified string is equal to the unique identifier.
    virtual bool is(const std::string &) const final;

private:

    std::string base;
    std::string suffix;
    bool unique;

#ifdef _DEBUG

public:
    static bool is_present(const std::string &);
    static bool is_unique(const std::string &);
private:
    static std::map<std::string, bool> identifiers;

#endif

};



inline std::string Identifier::get(const std::string &s) const
{
    return negerns::Format("%1_%2", base, s.empty() ? suffix : s);
}

inline std::string Identifier::get(std::size_t n) const
{
    return negerns::Format("%1_%2", base, n);
}

inline bool Identifier::empty() const
{
    return base.empty() && suffix.empty();
}

inline void Identifier::set_base(const std::string &s)
{
    base = s;
}

inline std::string Identifier::get_base() const
{
    return base;
}

inline void Identifier::set_suffix(const std::string &s)
{
    suffix = s;
}

inline void Identifier::set_suffix(std::size_t n)
{
    suffix = string::to_string(n);
}

inline std::string Identifier::get_suffix() const
{
    return suffix;
}

inline void Identifier::set_unique(bool u)
{
    unique = u;
}

inline bool Identifier::is_unique() const
{
    return unique;
}

inline bool Identifier::is(const std::string &s) const
{
    return string::Format("%1_%2", base, suffix) == s;
}

} //_ namespace negerns

#endif //_ NEGERNS_CORE_IDENTIFIER_H
