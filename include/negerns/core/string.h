/*******************************************************************************
 * File:    negerns/core/string.h
 * Created: 28 Feb 2014 5:26 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_STRING_H
#define NEGERNS_CORE_STRING_H

#include <string>
#include <negerns/core/string/const.h>
#include <negerns/core/string/convert.h>
#include <negerns/core/string/format.h>

#include <negerns/core/declspec.h>

namespace negerns {

template <typename... A>
std::string Format(const char *fmt, A... args) {
    return string::Format(fmt, std::forward<A>(args)...);
}

template <typename... A>
std::string Format(const std::string &fmt, A... args) {
    return string::Format(fmt.c_str(), std::forward<A>(args)...);
}

NEGERNS_DECL
std::string Format(const char *, const std::vector<Var> &);

NEGERNS_DECL
std::string Format(const std::string &, const std::vector<Var> &);

} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace string {

//. TODO: The following enumeration should not be here.
//. For now we declare it here. Only used to test the newline().
enum class OSType {
    Windows,
    Macintosh,
    Linux,
    Unix,
#ifdef _WIN32
    Default = Windows
#elif defined macintosh // OS 9
    Default = Macintosh
#else
    // Mac OS X uses \n
    Default = Linux
#endif
};

NEGERNS_DECL
const char * newline(OSType t = OSType::Default);

//! Searches the string backward and returns the index of the first character not
//! equal to the specified character starting from the specified index. Otherwise,
//! it returns string::npos.
NEGERNS_DECL
std::size_t rfind_first_not_of(const std::string &,
    char c, std::size_t idx = 0);

NEGERNS_DECL
std::string clean(const std::string &);

//! Replace all occurrences of a string in a string (in-place).
//. TODO: Test
NEGERNS_DECL
void replace_inplace(std::string &, const std::string &, const std::string &);

//! Replace all occurrences of a string in a string and returns the result.
//. TODO: Test
NEGERNS_DECL
std::string replace(const std::string &, const std::string &,
    const std::string & = std::string());

//! Replaces all occurrences of a string in a string.
//!
//! Scans the source string for the occurrence of the specified string and replaces it
//! with the replacement string.
//!
//! Arguments:
//!   source string
//!   find string
//!   replacement string
//!
//! \code
//! replace("..hello..", "..", "") == "hello"
//! replace("....hello..", "..", "") == "hello"
//! replace(".....hello...", "...", ".") == "...hello."
//! \endcode
//. TODO: Create a function on how to address the last scenario.
NEGERNS_DECL
char* replace(const char *, const char *, const char * = "");

NEGERNS_DECL
void reverse(char*, char*);

//! Returns a copy of the string with all leading whitespace removed.
NEGERNS_DECL
std::string trim_left(const std::string &);

//! Returns a copy of the string with all trailing whitespace removed.
NEGERNS_DECL
std::string trim_right(const std::string &);

//! Returns a copy of the string with all leading and trailing whitespace
//! removed.
NEGERNS_DECL
std::string trim(const std::string &);

NEGERNS_DECL
void trim_right_if(std::string &, char, std::size_t idx = 0);

NEGERNS_DECL
std::string concat_i(int, char, int, ...);

//! Concatenate strings.
template <int repeat = 0, char delim = ' ', typename... A>
std::string concat(const A&... args)
{
    return concat_i(repeat, delim, sizeof...(args), (const std::string &)args...);
}

//! Split a string into a vector of strings.
//!
//! By default, it ignores repeated delimiters, unless the repeat argument
//! is true. It ignores the last character if it is the same as the
//! delimiter character.
NEGERNS_DECL
std::vector<std::string> split(const std::string &,
    char delim = '\n',
    bool repeat = false,
    const std::string &prefix = std::string(),
    const std::string &suffix = std::string());

//. TODO: Add split variation accepting multiple delimiters.
//. std::vector<std::string> split(const std::string &,
//.     const std::string &delims = '\n ,;',
//.     bool repeat = false,
//.     const std::string prefix = "",
//.     const std::string suffix = "");

//. TODO: Also with multiple string delimiters.
//. std::vector<std::string> split(const std::string &,
//.     const std::vector<std::string> &delims = {",", "and"},
//.     bool repeat = false,
//.     const std::string prefix = "",
//.     const std::string suffix = "");

//! Concatenate a vector of strings and return it.
//!
//! By default, the new line character is the delimiter.
//! The delimiter is appended after every item in the vector except the
//! last. If repeat argument specifies a number greater than 1 then the
//! delimiter is replicated n times before appending to each vector item
//! (except the last one).
NEGERNS_DECL
std::string join(const std::vector<std::string> &,
    int repeat = 1,
    char delim = '\n');

} //_ namespace string
} //_ namespace negerns

#endif //_ NEGERNS_CORE_STRING_H
