/*******************************************************************************
 * File:    negerns/core/output.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 30 Jul 2013 4:35 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_OUTPUT_H
#define NEGERNS_CORE_OUTPUT_H

#include <string>
#include <vector>
#include <negerns/core/string.h>
#include <negerns/core/declspec.h>
#include <negerns/core/outputsink.h>

namespace negerns {

//! Output interface
class NEGERNS_DECL Output
{
public:

    Output();
    virtual ~Output() = default;

    //! Initialize the output facility.
    static void Init(OutputSink *ptr);

    //! Shutdown the output facility.
    static void Shutdown();

    template <typename... A>
    static void Send(const char *fmt, A... args) {
        SendThis(string::Format(fmt, args...));
    }

    template <typename... A>
    static void Send(const std::string &fmt, A... args) {
        SendThis(string::Format(fmt.c_str(), args...));
    }

    template <typename... A>
    static void Debug(const char *fmt, A... args) {
#ifdef _DEBUG
        SendThis(string::Format(fmt, args...));
#endif
    }

    template <typename... A>
    static void Debug(const std::string &fmt, A... args) {
#ifdef _DEBUG
        SendThis(string::Format(fmt.c_str(), args...));
#endif
    }

    //! Send an empty string.
    static void Send();

    static void Clear();

private:

    class OutputImp
    {
    public:
        OutputImp() = default;
        virtual ~OutputImp() = default;

        //! Message buffer that holds messages sent to the output sink when
        //! the sink is not yet initialized. The contents of the buffer are
        //! sent to the output sink it is initialized. After that, the
        //! contents of the message buffer are discarded.
        std::vector<std::string> buffer;

        OutputSink *sink = nullptr;
    };

    static void SendThis(const std::string &s);

    static OutputImp* Implementation(OutputImp* = nullptr);

    static bool initialized;
};

} //_ namespace negerns

#endif //_ NEGERNS_CORE_OUTPUT_H
