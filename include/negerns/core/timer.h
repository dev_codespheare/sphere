/*****************************************************************************
 * File:    negerns/core/timer.h
 * Created: 26 Mar 2014 12:09 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Purpose: Timer
 *****************************************************************************/

#ifndef NEGERNS_CORE_TIMER_H
#define NEGERNS_CORE_TIMER_H

#include <thread>   // for sleep()
#include <chrono>   // for sleep()
#include <boost/timer/timer.hpp>
#include <boost/cstdint.hpp>

#include <negerns/core/declspec.h>

namespace negerns {
namespace timer {

typedef boost::int_least64_t Seconds;
typedef boost::timer::cpu_times CPUTimeNanoseconds;

struct CPUTimeSeconds
{
    CPUTimeSeconds(CPUTimeNanoseconds ns) :
        wall(ns.wall / 1000000000),
        user(ns.user / 1000000000),
        system(ns.system / 1000000000)
    { }
    Seconds wall;
    Seconds user;
    Seconds system;

    void clear() { wall = user = system = 0LL; }
};

//! Sleep until number of specified seconds has elapsed.
inline
void sleep(uint64_t seconds)
{
    std::this_thread::sleep_for(std::chrono::seconds(seconds));
}

} //_ namespace timer

//! Timer timer for timing portions of code.
//!
//! Format specifiers for returning a formatted string:
//!   %w  wall
//!   %u  user
//!   %s  system
//!   %t  user + system
//!   %p  The percentage of times.wall represented by times.user + times.system
class NEGERNS_DECL Timer : private boost::timer::cpu_timer
{
public:
    Timer() BOOST_NOEXCEPT;

    //! Constructor.
    //! Start the timer according to the passed flag.
    Timer(bool) BOOST_NOEXCEPT;
    virtual ~Timer();

    //! Starts (or restarts) the stopwatch.
    void start() BOOST_NOEXCEPT;

    //! Stops or pauses the stopwatch.
    void stop() BOOST_NOEXCEPT;

    void resume() BOOST_NOEXCEPT;

    //! Stop and start the stopwatch.
    void restart() BOOST_NOEXCEPT;

    std::string format(
        const timer::CPUTimeNanoseconds &,
        short places = boost::timer::default_places) const;

    std::string format(
        const std::string &,
        const timer::CPUTimeNanoseconds &,
        short places = boost::timer::default_places) const;

    std::string format(short places = boost::timer::default_places) const;

    std::string format(
        const std::string &,
        short places = boost::timer::default_places) const;

    void set_format_string(const std::string &);

    bool is_stopped() const BOOST_NOEXCEPT;

    //! Returns the elapsed time in nanoseconds since the stopwatch started.
    negerns::timer::CPUTimeNanoseconds elapsed() const BOOST_NOEXCEPT;

    //! Returns the number of seconds elapsed since the stopwatch started.
    negerns::timer::CPUTimeSeconds elapsed_seconds() const BOOST_NOEXCEPT;

private:
    std::string outputFormat;
};

} //_ namespace negerns

#endif //_ NEGERNS_CORE_TIMER_H
