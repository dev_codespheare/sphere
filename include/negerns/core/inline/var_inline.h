
inline
Var & Var::operator = (const Var &rhs)
{
    if (this != &rhs) {
        type = rhs.type;
        if (is_type_object()) {
            content = rhs.content->clone();
        } else {
            double_ = rhs.double_;
        }
    }
    return *this;
}

inline
Var & Var::operator = (Var &&rhs)
{
    if (this != &rhs) {
        rhs.swap(*this);
        rhs.type = variant::Type::Empty;
        rhs.content = nullptr;
    }
    return *this;
}

// Type functions
// -----------------------------------------------------------------------------

inline
bool Var::is_type_signed() const
{
    return type == variant::Type::SChar
        || type == variant::Type::Short
        || type == variant::Type::Integer
        || type == variant::Type::Long
        || type == variant::Type::LLong
        || type == variant::Type::Float
        || type == variant::Type::Double
        || type == variant::Type::LDouble
        || type == variant::Type::Fixed2;
}

inline
bool Var::is_type_unsigned() const
{
    return type == variant::Type::UChar
        || type == variant::Type::UShort
        || type == variant::Type::UInteger
        || type == variant::Type::ULong
        || type == variant::Type::ULLong;
}

inline
bool Var::is_type_arithmetic() const
{
    return type > variant::Type::Empty && type < variant::Type::Fixed2;
}

inline
bool Var::is_type_char() const
{
    return type > variant::Type::Empty && type < variant::Type::Bool;
}

inline
bool Var::is_type_integral() const
{
    return type > variant::Type::Empty && type < variant::Type::Float;
}

inline
bool Var::is_type_float() const
{
    return type > variant::Type::ULLong && type < variant::Type::Fixed2;
}

inline
bool Var::is_type_fixed() const
{
    return type > variant::Type::LDouble && type < variant::Type::String;
}

inline
bool Var::is_type_string() const
{
    return type == variant::Type::ShortString || type == variant::Type::String;
}

inline
bool Var::is_type_object() const
{
    return type > variant::Type::ShortString && type < variant::Type::Unknown;
}

// Get functions
// -----------------------------------------------------------------------------

inline
long double Var::get_ldouble() const
{
    if (is_ldouble()) {
        return static_cast<LDoubleHolder_t *>(content)->get();
    } else {
        throw std::logic_error({"Type is not ", TypeName(type)});
    }
}

inline
dec2_t Var::get_fixed2() const
{
    if (is_fixed2()) {
        return static_cast<Dec2Holder_t *>(content)->get();
    } else {
        throw std::logic_error({"Type is not ", TypeName(type)});
    }
}

inline
std::string Var::get_string() const
{
    if (is_shortstring()) {
        return &shortstr_[0];
    } else if (is_string()) {
        return std::string(static_cast<CharPtrHolder_t*>(content)->get());
    } else {
        throw std::logic_error({"Type is not ", TypeName(type)});
    }
}

inline
const char * Var::get_charptr() const
{
    if (is_shortstring()) {
        return &shortstr_[0];
    } else if (is_string()) {
        return static_cast<CharPtrHolder_t*>(content)->get();
    } else {
        throw std::logic_error({"Type is not ", TypeName(type)});
    }
}

inline
const char * Var::get_charptr(std::size_t &n) const
{
    if (is_shortstring()) {
        n = std::strlen(shortstr_);
        return &shortstr_[0];
    } else if (is_string()) {
        return static_cast<CharPtrHolder_t*>(content)->get(n);
    } else {
        throw std::logic_error({"Type is not ", TypeName(type)});
    }
}

inline
std::tm Var::get_tm() const
{
    if (is_tm()) {
        return static_cast<TmHolder_t *>(content)->get();
    } else {
        throw std::logic_error({"Type is not ", TypeName(type)});
    }
}

// As
// -----------------------------------------------------------------------------

inline
double Var::as_double() const
{
    if (is_float()) {
        return static_cast<double>(float_);
    } else if (is_double()) {
        return double_;
    } else {
        throw std::logic_error({"Invalid type ", variant::TypeName(type)});
    }
}

inline
long double Var::as_ldouble() const
{
    if (is_float()) {
        return static_cast<long double>(float_);
    } else if (is_double()) {
        return static_cast<long double>(double_);
    } else if (is_ldouble()) {
        return static_cast<LDoubleHolder_t *>(content)->get();
    } else {
        throw std::logic_error({"Invalid type ", variant::TypeName(type)});
    }
}

inline
std::string Var::as_string(uint8_t prec) const
{
    return variant::get_as_string(this, prec);
}

// Equality
// -----------------------------------------------------------------------------

//. TODO: Fix comparison. Exceptions may be thrown or should it?
//. This kind of equality checks for values only and not types.
//. The internal value is converted to the type equivalent to the argument.

inline
bool Var::operator == (const char rhs) const {
    return !is_char() ? false : get_char() == rhs;
}

inline
bool Var::operator == (const signed char rhs) const {
    return !is_schar() ? false : get_schar() == rhs;
}

inline
bool Var::operator == (const unsigned char rhs) const {
    return !is_uchar() ? false : get_uchar() == rhs;
}

inline
bool Var::operator == (bool rhs) const {
    return !is_bool() ? false : get_bool() == rhs;
}

inline
bool Var::operator == (const short &rhs) const {
    return !is_short() ? false : get_short() == rhs;
}

inline
bool Var::operator == (const unsigned short &rhs) const {
    return !is_ushort() ? false : get_ushort() == rhs;
}

inline
bool Var::operator == (const int &rhs) const {
    return !is_int() ? false : get_int() == rhs;
}

inline
bool Var::operator == (const unsigned int rhs) const {
    return !is_uint() ? false : get_uint() == rhs;
}

inline
bool Var::operator == (const long rhs) const {
    return !is_long() ? false : get_long() == rhs;
}

inline
bool Var::operator == (const unsigned long rhs) const {
    return !is_ulong() ? false : get_ulong() == rhs;
}

inline
bool Var::operator == (const long long rhs) const {
    return !is_llong() ? false : get_llong() == rhs;
}

inline
bool Var::operator == (const unsigned long long rhs) const {
    return !is_ullong() ? false : get_ullong() == rhs;
}

inline
bool Var::operator == (const float rhs) const {
    return !is_float() ? false : get_float() == rhs;
}

inline
bool Var::operator == (const double rhs) const {
    return !is_double() ? false : get_double() == rhs;
}

inline
bool Var::operator == (const long double rhs) const {
    return !is_ldouble() ? false : get_ldouble() == rhs;
}

inline
bool Var::operator == (const dec2_t &rhs) const {
    return !is_fixed2() ? false : get_fixed2() == rhs;
}

inline
bool Var::operator == (const char *rhs) const {
    if (empty() && rhs == nullptr) {
        return true;
    } else if ((empty() && rhs != nullptr) || (!empty() && rhs == nullptr)) {
        return false;
    } else {
        return !is_type_string() ? false : get_string() == std::string(rhs);
    }
}

inline
bool Var::operator == (const std::string &rhs) const {
    return !is_type_string() ? false : get_string() == rhs;
}

inline
bool Var::operator == (const std::tm &rhs) const {
    if (!is_tm()) {
        return false;
    } else {
        auto left = date::to_time(get_tm());
        auto right = date::to_time(rhs);
        auto diff = std::difftime(left, right);
        return diff == 0.00;
    }
}

// Inequality
// -----------------------------------------------------------------------------

inline
bool Var::operator != (const char rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const signed char rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const unsigned char rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const bool rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const short &rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const unsigned short &rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const int &rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const unsigned int rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const long rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const unsigned long rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const long long rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const unsigned long long rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const float rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const double rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const long double rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const dec2_t rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const char *rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const std::string &rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const std::tm &rhs) const {
    return !this->operator==(rhs);
}

inline
bool Var::operator != (const Var &rhs) const {
    return !this->operator==(rhs);
}

// Private functions
// -----------------------------------------------------------------------------

inline
void Var::clear()
{
    if (is_type_object()) {
        delete content;
        content = nullptr;
    }
    type = variant::Type::Empty;
}

inline
Var & Var::swap(Var &rhs) {
    std::swap(type, rhs.type);
    if (rhs.is_type_object()) {
        std::swap(content, rhs.content);
    } else {
        std::swap(double_, rhs.double_);
    }
    return *this;
}
