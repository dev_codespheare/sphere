/*******************************************************************************
 * File:    negerns/core/datetime.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 30 Oct 2013 11:45
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_DATETIME_H
#define NEGERNS_CORE_DATETIME_H

#include <ctime>
#include <vector>
#include <wx/datetime.h>
#include <Poco/DateTime.h>
#include <Poco/Timestamp.h>
#include <Poco/Data/Date.h>
#include <negerns/poco.h>

#include <negerns/core/declspec.h>

//! Global operator == overload for std::tm.
NEGERNS_DECL bool operator == (const std::tm &lhs, const std::tm &rhs);
NEGERNS_DECL bool operator != (const std::tm &lhs, const std::tm &rhs);

namespace negerns {

//! Date/time format string:
//!
//!   %a  The abbreviated weekday name according to the current locale.
//!   %A  The full weekday name according to the current locale.
//!   %b  The abbreviated month name according to the current locale.
//!   %B  The full month name according to the current locale.
//!   %c  The preferred date and time representation for the current locale.
//!   %C  The century number (year/100) as a 2-digit integer. (SU)
//!   %d  The day of the month as a decimal number (range 01 to 31).
//!   %D  Equivalent to %m/%d/%y.  (Yecch--for Americans only.
//!       Americans should note that in other countries %d/%m/%y is
//!       rather common.  This means that in international context this
//!       format is ambiguous and should not be used.) (SU)
//!   %e  Like %d, the day of the month as a decimal number, but a
//!       leading zero is replaced by a space. (SU)
//!   %E  Modifier: use alternative format, see below. (SU)
//!   %F  Equivalent to %Y-%m-%d (the ISO 8601 date format). (C99)
//!   %G  The ISO 8601 week-based year (see NOTES) with century as a
//!       decimal number.  The 4-digit year corresponding to the ISO
//!       week number (see %V).  This has the same format and value as
//!   %Y, except that if the ISO week number belongs to the previous
//!       or next year, that year is used instead. (TZ)
//!   %g  Like %G, but without century, that is, with a 2-digit year (00-99). (TZ)
//!   %h  Equivalent to %b.  (SU)
//!   %H  The hour as a decimal number using a 24-hour clock (range 00 to 23).
//!   %I  The hour as a decimal number using a 12-hour clock (range 01 to 12).
//!   %j  The day of the year as a decimal number (range 001 to 366).
//!   %k  The hour (24-hour clock) as a decimal number (range 0 to 23); single
//!       digits are preceded by a blank.  (See also %H.)  (TZ)
//!   %l  The hour (12-hour clock) as a decimal number (range 1 to 12); single
//!       digits are preceded by a blank.  (See also %I.)  (TZ)
//!   %m  The month as a decimal number (range 01 to 12).
//!   %M  The minute as a decimal number (range 00 to 59).
//!   %n  A newline character. (SU)
//!   %O  Modifier: use alternative format, see below. (SU)
//!   %p  Either "AM" or "PM" according to the given time value, or the
//!       corresponding strings for the current locale.  Noon is treated as
//!       "PM" and midnight as "AM".
//!   %P  Like %p but in lowercase: "am" or "pm" or a corresponding string for
//!       the current locale. (GNU)
//!   %r  The time in a.m. or p.m. notation.  In the POSIX locale this is
//!       equivalent to %I:%M:%S %p. (SU)
//!   %R  The time in 24-hour notation (%H:%M). (SU) For a version including
//!       the seconds, see %T below.
//!   %s  The number of seconds since the Epoch, 1970-01-01 00:00:00+0000 (UTC). (TZ)
//!   %S  The second as a decimal number (range 00 to 60). (The range is up to
//!       60 to allow for occasional leap seconds.)
//!   %t  A tab character. (SU)
//!   %T  The time in 24-hour notation (%H:%M:%S). (SU)
//!   %u  The day of the week as a decimal, range 1 to 7, Monday being 1.
//!       See also %w. (SU)
//!   %U  The week number of the current year as a decimal number, range 00 to
//!       53, starting with the first Sunday as the first day of week 01.
//!       See also %V and %W.
//!   %V  The ISO 8601 week number (see NOTES) of the current year as a decimal
//!       number, range 01 to 53, where week 1 is the first week that has at
//!       least 4 days in the new year.  See also %U and %W. (SU)
//!   %w  The day of the week as a decimal, range 0 to 6, Sunday being 0.
//!       See also %u.
//!   %W  The week number of the current year as a decimal number, range 00 to
//!       53, starting with the first Monday as the first day of week 01.
//!   %x  The preferred date representation for the current locale without the time.
//!   %X  The preferred time representation for the current locale without the date.
//!   %y  The year as a decimal number without a century (range 00 to 99).
//!   %Y  The year as a decimal number including the century.
//!   %z  The +hhmm or -hhmm numeric timezone (that is, the hour and minute
//!       offset from UTC). (SU)
//!   %Z  The timezone name or abbreviation.
//!   %+  The date and time in date(1) format. (TZ) (Not supported in glibc2.)
//!   %%  A literal '%' character.
//!
//! http://www.cplusplus.com/reference/clibrary/ctime/strftime.html
//!
//! For C++11 format string see: http://en.cppreference.com/w/cpp/chrono/c/strftime

#define ISO_8601_DATE       "%Y-%m-%d"
#define ISO_8601_TIME       "%H:%M:%S"
#define ISO_8601_DATETIME   "%Y-%m-%dT%H:%M:%S"
#define ISO_8601_DATETIME_NO_T "%Y-%m-%d %H:%M:%S"



namespace date {

enum class MonthName {
    Full = wxDateTime::NameFlags::Name_Full,
    Short = wxDateTime::NameFlags::Name_Abbr
};

enum class Month {
    Invalid,
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December
};

NEGERNS_DECL std::tm now();

NEGERNS_DECL
std::vector<std::string> get_month_names(MonthName = MonthName::Full);

NEGERNS_DECL
bool to_tm(const std::string &, std::tm &, const char* = ISO_8601_DATETIME_NO_T);

NEGERNS_DECL
std::tm to_tm(const std::string &, const char* = ISO_8601_DATETIME_NO_T);

NEGERNS_DECL std::tm to_tm(const std::time_t &);

NEGERNS_DECL std::tm to_tm(const wxDateTime &);

NEGERNS_DECL std::time_t to_time(const std::tm &);

NEGERNS_DECL wxDateTime to_wxdatetime(const std::tm &);

NEGERNS_DECL
std::string to_string(const std::tm &, const char* = ISO_8601_DATETIME_NO_T);



// ----------------------------------------------------------------------------------------



class NEGERNS_DECL Date
{
public:

    Date() = default;
    //! Copy constructor
    Date(const Date &other) : dt(other.dt) { }
    //! Move constructor
    Date(Date &&other) : dt(other.dt) { }
    Date(const std::tm &);
    Date(const std::time_t &);
    virtual ~Date();

    Date & operator = (const std::tm &rhs) {
        dt = rhs;
        return *this;
    }

    // Copy assignment
    Date & operator = (const Date &rhs) {
        Date(rhs).swap(*this);
        return *this;
    }

    //! Move assignment
    Date & operator = (Date &&rhs) {
        rhs.swap(*this);
        Date().swap(rhs);
        return *this;
    }

    bool operator == (const std::tm &);
    bool operator == (const std::time_t &);
    bool operator == (const Date &);
    bool operator < (const std::tm &);
    bool operator < (const std::time_t &);
    bool operator < (const Date &);
    bool operator > (const std::tm &);
    bool operator > (const std::time_t &);
    bool operator > (const Date &);

    //! Get the current calendar date/time.
    Date & now();
    Date & second(int n) { dt.tm_sec = n; return *this; }
    Date & minute(int n) { dt.tm_min = n; return *this; }
    Date & hour(int n)   { dt.tm_hour = n; return *this; }
    Date & day(int n)    { dt.tm_mday = n; return *this; }
    Date & month(int n)  { dt.tm_mon = n; return *this; }
    Date & year(int n)   { dt.tm_year = n; return *this; }

    void set_now();
    void set_second(int n) { dt.tm_sec = n; }
    void set_minute(int n) { dt.tm_min = n; }
    void set_hour(int n)   { dt.tm_hour = n; }
    void set_day(int n)    { dt.tm_mday = n; }
    void set_month(int n)  { dt.tm_mon = n; }
    void set_year(int n)   { dt.tm_year = n; }

    int get_second() { return dt.tm_sec; }
    int get_minute() { return dt.tm_min; }
    int get_hour()   { return dt.tm_hour; }
    int get_day()    { return dt.tm_mday; }
    int get_month()  { return dt.tm_mon; }
    int get_year()   { return dt.tm_year; }

    inline std::tm get_tm() const { return dt; }
    inline std::time_t get_time() const { return to_time(dt); }

private:

    Date & swap(Date &rhs) {
        std::swap(dt, rhs.dt);
        return *this;
    }

private:
    std::tm dt;
};

} //_ namespace date



// ----------------------------------------------------------------------------------------



#define NEGERNS_DATE_FORMAT "%Y-%m-%d"
#define NEGERNS_TIME_FORMAT "%H:%M:%S"
#define NEGERNS_DATETIME_FORMAT "%Y-%m-%d %H:%M:%S"

enum class Month {
    Invalid,
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December
};



class NEGERNS_DECL DateTime
{
public:

    enum class Name {
        Full = wxDateTime::NameFlags::Name_Full,
        Abbrev = wxDateTime::NameFlags::Name_Abbr
    };



    //! \name Set to default
    //! @{

    //! Set Poco::Timestamp to std::time_t = 0 (1970/01/01 00:00:00).
    static void Default(poco::Timestamp &);

    //! Set Poco::DateTime to std::time_t = 0 (1970/01/01 00:00:00).
    static void Default(poco::DateTime &);

    //! Set wxDatetime to std::time_t = 0 (1970/01/01 00:00:00).
    static void Default(wxDateTime &);

    //! @}

    //! \name Convert Poco::DateTime or Poco::Timestamp
    //! @{

    //! Convert Poco::Timestamp to wxDateTime.
    static bool Convert(const poco::Timestamp &pts, wxDateTime &wxdt);

    //! Convert Poco DateTime to wxDateTime.
    static bool Convert(const poco::DateTime &, wxDateTime &);

    //! @}

    //! \name Convert wxDateTime
    //! @{

    //! Convert wxDateTime to Poco::Timestamp.
    static bool Convert(const wxDateTime &wxdt, poco::Timestamp &pts);

    //! Convert wxDateTime to Poco::DateTime.
    static bool Convert(const wxDateTime &, poco::DateTime &);

    //! Convert wxDateTime to Poco::Data::Date.
    static bool Convert(const wxDateTime &, poco::Data::Date &);

    //! Convert wxDateTime to Poco::DateTime using the specified format.
    static bool Convert(const wxDateTime &, poco::DateTime &, const std::string &fmt);

    //! @}

    //! \name Convert from/to std::string
    //!

    //!
    //! @{

    //! Convert std::string to Poco::Timestamp.
    static bool Convert(const std::string &source, poco::Timestamp &ts, const std::string &fmt = NEGERNS_DATETIME_FORMAT);

    //! Convert std::string to Poco::DateTime.
    static bool Convert(const std::string &source, poco::DateTime &dt, const std::string &fmt = NEGERNS_DATETIME_FORMAT);

    //! Convert std::string to wxDateTime.
    static bool Convert(const std::string &source, wxDateTime &dt, const std::string &fmt = NEGERNS_DATETIME_FORMAT);

    //! Convert Poco::Timestamp to std::string.
    static std::string ToString(const poco::Timestamp &ts, const std::string &fmt = NEGERNS_DATETIME_FORMAT);

    //! Convert Poco::DateTime to std::string.
    static std::string ToString(const poco::DateTime &dt, const std::string &fmt = NEGERNS_DATETIME_FORMAT);

    //! Convert Poco::Data::Date to std::string.
    static std::string ToString(const poco::Data::Date &dt, const std::string &fmt = NEGERNS_DATE_FORMAT);

    //! Convert wxDateTime to std::string.
    static std::string ToString(const wxDateTime &dt, const std::string &fmt = NEGERNS_DATETIME_FORMAT);

    static std::string ToISODate(const wxDateTime &dt);

    static std::string ToISODate(const poco::Timestamp &ts);

    static std::string ToISODate(const poco::DateTime &dt);

    static std::string ToISOTime(const wxDateTime &dt);

    static std::string ToISOTime(const poco::Timestamp &ts);

    static std::string ToISOTime(const poco::DateTime &dt);

    // @}

#if 0
    //! Converts date from Poco::DateTime to wxDateTime using the specified
    //! format string.
    static bool ConvertDate(const poco::DateTime&, wxDateTime&, const std::string& fmt);

    //! Converts date from wxDateTime to Poco::DateTime using the specified
    //! format string.
    static bool ConvertDate(const wxDateTime&, poco::DateTime&, const std::string& fmt);
#endif

    static std::vector<std::string> GetMonthNames(Name n = Name::Full);
};

} //_ namespace negerns

#endif //_ NEGERNS_CORE_DATETIME_H
