/*******************************************************************************
 * File:    negerns/core/vector.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 24 Jul 2014 4:56 AM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_VECTOR_H
#define NEGERNS_CORE_VECTOR_H

#include <vector>
#include <algorithm>
#include <negerns/core/debug.h>
#include <negerns/core/string.h>

namespace negerns {

template <typename T>
class NEGERNS_DECL Vector
{
public:

    class NEGERNS_DECL Iterator
    {
    public:
        Iterator(Vector<T> *ni, std::size_t n) :
            ref(ni),
            pos(n)
        { }

        bool operator!=(const Iterator &it) const {
            return pos != it.pos;
        }

        const std::string& operator*() const {
            return ref->Get(pos);
        }

        const Iterator& operator++() {
            ++pos;
            // Although not strictly necessary for a range-based for loop
            // following the normal convention of returning a value from
            // operator++ is a good idea.
            return *this;
        }

    private:
        Vector<T> *ref;
        std::size_t pos;

    }; // class Iterator

    Vector() = default;
    Vector(const Vector&) = default;
    virtual ~Vector() { content.clear(); }

    Iterator begin() { return Iterator(this, 0); }

    Iterator end() { return Iterator(this, content.size()); }

    //! Add an element.
    virtual void Add(const T& t) { content.push_back(t); }

    //! Return element at specified index.
    virtual T& Get(std::size_t n) {
        ASSERT(content.size() > 0, "Vector is empty. Cannot access any element.");
        ASSERT(n < content.size(), "Index %1 is out of range %2..", n, content.size());
        return content.at(n);
    }

    //! Returns true if the specified element exists.
    virtual bool IsFound(const T& t) final {
        return content.size() > 0 ?
            std::find(content.begin(), content.end(), t) != content.end()
            : false;
    }

    //! Return the index of the specified element.
    virtual std::size_t Index(const T& t) {
        auto pos = std::find(content.begin(), content.end(), t);
        if (pos != content.end()) {
            return std::distance(content.begin(), pos);
        } else {
            // TODO: This is wrong!
            return 0;
        }
    }

    //! Return true if there is no element.
    bool Empty() const { return content.size() == 0; }

    //! Return true if there is no element.
    bool IsEmpty() const { return content.size() == 0; }

    //! Return the number of elements.
    std::size_t Count() const { return content.size(); }

    //! Remove all elements.
    virtual void Clear() { content.clear(); }

private:

    std::vector<T> content;
}; // class Vector

} //_ namespace negerns

#endif //_ NEGERNS_CORE_VECTOR_H
