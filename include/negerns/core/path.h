/*****************************************************************************
 * File:    negerns/core/path.h
 * Created: 3 Apr 2013 8:47 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Purpose: Path utility
 *****************************************************************************/

#ifndef NEGERNS_CORE_PATH_H
#define NEGERNS_CORE_PATH_H

#include <wx/string.h>
#include <negerns/core/declspec.h>

//. TODO:  Merge with namespace filesystem

namespace negerns {

//! File path utility class.
//!
//! \todo Rename to ExecutablePath
class NEGERNS_DECL Path
{
public:
    Path();
    ~Path() { }
    //! Set the application path.
    //!
    //! \see executablePath
    void SetExecutablePath(const wxString& p);

    //! Returns the full absolute path of the file relative to the executable
    //! path.
    //!
    //! The parameter may contain a parent directory symbol/shortcut '..'
    //! and the function returns the full absolute path such that
    //! <code>c:\\project\\build\\..\\bin\\file.txt</code> is converted to
    //! <code>c:\\project\\bin\\file.txt</code> assuming that the executable
    //! path is <code>c:\\project\\build</code> and the argument value is
    //! <code>..\\bin\\file.txt</code>.
    wxString BuildFilename(const wxString& file);

    //! Return the absolute file path of the specified file path.
    wxString MakeAbsolute(const wxString& file);
private:
    //! Holds the application path.
    //!
    //! NOTE:
    //! This is a workaround to wxStandardPaths::GetExecutablePath.
    //! Using the function in a DLL causes an assertion being that
    //! a reference to the application instance is NULL. See the
    //! implementation of wxStandardPaths::Get in stdpbase.cpp.
    wxString executablePath;
};

} //_ namespace negerns

#endif //_ NEGERNS_CORE_PATH_H
