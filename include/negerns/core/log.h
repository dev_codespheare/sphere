/*******************************************************************************
 * File:    negerns/core/log.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 10 Sep 2011 7:41 AM
 *
 * Comment: Logging Facility
 ******************************************************************************/

#ifndef NEGERNS_CORE_LOG_H
#define	NEGERNS_CORE_LOG_H

#include <vector>
#include <boost/logic/tribool.hpp>

#include <negerns/core/string.h>
#include <negerns/core/utility.h>
#include <negerns/core/log/ilogger.h>

#ifndef _DEBUG
#include <negerns/core/noop.h>
#endif

#include <negerns/core/declspec.h>

namespace negerns {
namespace log {
namespace internal {

//! Set/Get logging state.
//!
//! Initial state is \c true, logging is enabled.
//!
//! \see enable
//! \see is_enabled
bool enable_state(boost::logic::tribool = boost::logic::indeterminate);

} //_ namespace internal
} //_ namespace log
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace log {
namespace tracing {
namespace internal {

//! Set/Get trace logging state.
//! The state determines whether trace logging is to proceed or not.
//!
//! Initial setting is \c true, trace logging is enabled.
//!
//! \see enable
//! \see is_enabled
bool trace_state(boost::logic::tribool = boost::logic::indeterminate);

} //_ namespace internal
} //_ namespace tracing
} //_ namespace log
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace log {
namespace stdoutput {
namespace internal {

//! Set/Get state of log messages being sent to the standard output.
//!
//! Initial setting is \c false, messages are not sent to the standard output.
//!
//! \see enable
//! \see is_enabled
bool stdoutput_state(boost::logic::tribool = boost::logic::indeterminate);

//! Set/Get the state of log messages being sent exclusively to the standard
//! output.
//!
//! Initial setting is \c false, messages are not exclusivley sent to the
//! standard output.
//!
//! \see only
//! \see is_only
bool only_state(boost::logic::tribool = boost::logic::indeterminate);

} //_ namespace internal
} //_ namespace stdoutput
} //_ namespace log
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace log {

enum class Status {
    Enable,
    Disable
};

//! Enable/Disable logging.
NEGERNS_DECL
void enable(bool);

//! Enable/Disable logging.
NEGERNS_DECL
void status(Status);

//! Returns whether logging is enabled or disabled.
NEGERNS_DECL
bool is_enabled();

} //_ namespace log
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace log {
namespace tracing {

enum class Status {
    Enable,
    Disable
};

//! Enable/Disable trace logging.
NEGERNS_DECL
void enable(bool);

//! Enable/Disable trace logging.
NEGERNS_DECL
void status(Status);

//! Returns whether trace logging is enabled or disabled.
NEGERNS_DECL
bool is_enabled();

} //_ namespace tracing
} //_ namespace log
} //_ namespace negerns


namespace negerns {
namespace log {
namespace tracing {
namespace utility {

struct Info
{
    Info(const char *fn, const char *f, std::size_t n) :
        function(fn),
        file(f),
        line(n)
    { }
    std::string function;
    std::string file;
    std::size_t line;

    std::string get_line_as_string() const {
        return string::to_string(line);
    }
}; //_ struct Info

} //_ namespace utility
} //_ namespace tracing
} //_ namespace log
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace log {
namespace stdoutput {

enum class Status {
    Enable,
    Disable
};

//! Enable/Disable sending log messages to standard output.
NEGERNS_DECL
void enable(bool b);

//! Enable/Disable sending log messages to standard output.
NEGERNS_DECL
void status(Status);

//! Returns whether log messages are sent to standard output.
NEGERNS_DECL
bool is_enabled();

//! Enable/Disable sending log messages to the standard output only.
NEGERNS_DECL
void only(bool b);

//! Returns whether log messages are sent to the stanrd output only.
NEGERNS_DECL
bool is_only();

} //_ namespace stdout
} //_ namespace log
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace log {

} //_ namespace log
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {

//! Logging component interface
//!
//! \internal
//! See Apache Common Log Format
//! http://httpd.apache.org/docs/2.4/logs.html#common
//! \endinternal
class NEGERNS_DECL Log final
{
public:

    enum class Type { Poco, Boost };

    //! Constructor.
    //!
    //! On first creation, the logging component is initialized. Log messages
    //! can be sent after this but they will be sent to the log message buffer
    //! until the log sink is initialized.
    //!
    //! \see Init
    Log();
    virtual ~Log();

    //! Initialize the log sink (destination).
    //!
    //! All log messages in the log message buffer will be sent to the log sink
    //! and cleared.
    //!
    //! \see Shutdown
    static void Init(Type t = Type::Poco);

    //! Shutdown the logging component.
    //!
    //! After this, no more log messages will processed.
    //!
    //! \see Init
    static void Shutdown();

    //! Set the path and file name.
    //! The filename is automatically appended with the default log file
    //! extension (.log).
    void SetFile(const std::string &p, const std::string &f);

    //std::string GetPath() const;
    //std::string GetFilename() const;
    std::string GetFullPath();

    template <typename... A>
    static void log(const log::Priority priority, const char *fmt, A&&... args)
    {
        if (!log::is_enabled()) {
            return;
        }
        if (!log::tracing::is_enabled() && priority == log::Priority::Trace) {
            return;
        }
        std::string s {
            sizeof...(args) == 0 ? fmt : ::negerns::Format(fmt, {args...})
        };
        if (log::stdoutput::is_enabled()) {
            if (!s.empty()) send_to_stdout(priority, s);
            if (log::stdoutput::is_only()) return;
        }
        send_to_logger(priority, s);
    }

    template <typename... A>
    static void log(const log::Priority priority,
        const log::tracing::utility::Info &trinfo,
        const char *fmt, A&&...args) {
        if (!log::is_enabled()) {
            return;
        }
        if (!log::tracing::is_enabled() && priority == log::Priority::Trace) {
            return;
        }
        std::string s {
            sizeof...(args) == 0 ? fmt : ::negerns::Format(fmt, {args...})
        };
        if (log::stdoutput::is_enabled()) {
            send_to_stdout(priority, trinfo, s);
            if (log::stdoutput::is_only()) return;
        }
        send_to_logger(priority, trinfo, s);
    }

private:

    //' NOTE: Define implementation here, now.
    //'
    //' Because the function templates below references the implementation
    //' member variables.

    struct LogImp
    {
        LogImp() = default;
        virtual ~LogImp() = default;

        //! Temporary storage for log messages.
        //!
        //! Log messages are sent to the buffer when the internal logging
        //! mechanism is not yet initialized. When the internal logging
        //! mechanism has been initialized, all the contents of the buffer
        //! is processed and sent to the actual logging destination. After
        //! that buffer is emptied and no longer used.
        //!
        //! \see to Log::Init.
        std::vector<log::Message> buffer;

        //! Log output filename.
        //!
        //! At startup, when the ILogger instance is not yet created, this
        //! serves as temporary container for the Log output filename. This
        //! temporary is necessary since the ILogger instance cannot be used
        //! until it is created in the Init funciton.
        std::string file;

        //! Log output path.
        //!
        //! Same as \c file.
        std::string path;

        //! Initializatoin flag for the internal logging mechanism.
        //!
        //! \internal
        //! This flag is necessary to allow the Logging component to accept
        //! log messages during the initialization of the internal logging
        //! mechanism. It is set to true when the internal logging mechanism
        //! is finished with its initialization process.
        //! \endinternal
        bool initDone = false;

        log::ILogger *logger = nullptr;

    }; //_ struct LogImp

    static void send_to_stdout(log::Priority, const std::string &);
    static void send_to_stdout(log::Priority,
        const log::tracing::utility::Info &,
        const std::string &);

    static void send_to_logger(log::Priority, const std::string &);
    static void send_to_logger(log::Priority,
        const log::tracing::utility::Info &,
        const std::string &);

    static std::string get_status(bool cond) {
        return string::to_string(cond, string::boolean::success_failed());
    }

private:

    static LogImp* Implementation(LogImp * = nullptr);

}; //_ class Log

} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace log {

//! Trace logging.
//!
//! If the tracing state is disabled then it does nothing.
template <typename... A>
void trace(
    const tracing::utility::Info &info,
    const char *fmt = "",
    A&&... args)
{
    ::negerns::Log::log(log::Priority::Trace, info, fmt, args...);
}

//! Conditional trace logging.
//!
//! Allows trace logging of program states even if tracing
//! state is disabled provided that the value of the priority argument is
//! other than Priority::Trace.
template <typename... A>
void trace(
    const log::Priority priority,
    const tracing::utility::Info &info,
    const char *fmt = "",
    A&&... args)
{
    ::negerns::Log::log(priority, info, fmt, args...);
}



// -----------------------------------------------------------------------------



template <typename... A>
void debug(const char *fmt, A... args) {
#ifdef _DEBUG
    ::negerns::Log::log(log::Priority::Debug, fmt, args...);
#endif
}

template <typename... A>
void info(const char *fmt, A... args) {
    ::negerns::Log::log(log::Priority::Information, fmt, args...);
}

template <typename... A>
void notice(const char *fmt, A... args) {
    ::negerns::Log::log(log::Priority::Notice, fmt, args...);
}

template <typename... A>
void warning(const char *fmt, A... args) {
    ::negerns::Log::log(log::Priority::Warning, fmt, args...);
}

template <typename... A>
void error(const char *fmt, A... args) {
    ::negerns::Log::log(log::Priority::Error, fmt, args...);
}

template <typename... A>
void critical(const char *fmt, A... args) {
    ::negerns::Log::log(log::Priority::Critical, fmt, args...);
}

template <typename... A>
void fatal(const char *fmt, A... args) {
    ::negerns::Log::log(log::Priority::Fatal, fmt, args...);
}



// -----------------------------------------------------------------------------



template <typename... A>
void debug(const std::string &fmt, A... args) {
#ifdef _DEBUG
    ::negerns::Log::log(log::Priority::Debug, fmt.c_str(), args...);
#endif
}

template <typename... A>
void info(const std::string &fmt, A... args) {
    ::negerns::Log::log(log::Priority::Information, fmt.c_str(), args...);
}

template <typename... A>
void notice(const std::string &fmt, A... args) {
    ::negerns::Log::log(log::Priority::Notice, fmt.c_str(), args...);
}

template <typename... A>
void warning(const std::string &fmt, A... args) {
    ::negerns::Log::log(log::Priority::Warning, fmt.c_str(), args...);
}

template <typename... A>
void error(const std::string &fmt, A... args) {
    ::negerns::Log::log(log::Priority::Error, fmt.c_str(), args...);
}

template <typename... A>
void critical(const std::string &fmt, A... args) {
    ::negerns::Log::log(log::Priority::Critical, fmt.c_str(), args...);
}

template <typename... A>
void fatal(const std::string &fmt, A... args) {
    ::negerns::Log::log(log::Priority::Fatal, fmt.c_str(), args...);
}



// -----------------------------------------------------------------------------



template <typename... A>
void debug_if(bool cond, const char *fmt, A... args) {
#ifdef _DEBUG
    if (cond) ::negerns::Log::log(log::Priority::Debug, fmt, args...);
#endif
}

template <typename... A>
void info_if(bool cond, const char *fmt, A... args) {
    if (cond) ::negerns::Log::log(log::Priority::Information, fmt, args...);
}

template <typename... A>
void notice_if(bool cond, const char *fmt, A... args) {
    if (cond) ::negerns::Log::log(log::Priority::Notice, fmt, args...);
}

template <typename... A>
void warning_if(bool cond, const char *fmt, A... args) {
    if (cond) ::negerns::Log::log(log::Priority::Warning, fmt, args...);
}

template <typename... A>
void error_if(bool cond, const char *fmt, A... args) {
    if (cond) ::negerns::Log::log(log::Priority::Error, fmt, args...);
}

template <typename... A>
void critical_if(bool cond, const char *fmt, A... args) {
    if (cond) ::negerns::Log::log(log::Priority::Critical, fmt, args...);
}

template <typename... A>
void fatal_if(bool cond, const char *fmt, A... args) {
    if (cond) ::negerns::Log::log(log::Priority::Fatal, fmt, args...);
}



// -----------------------------------------------------------------------------



template <typename... A>
void debug_if(bool cond, const std::string &s, A... args) {
#ifdef _DEBUG
    if (cond) ::negerns::Log::log(log::Priority::Debug, s.c_str(), args...);
#endif
}

template <typename... A>
void info_if(bool cond, const std::string &s, A... args) {
    if (cond) ::negerns::Log::log(log::Priority::Information, s.c_str(), args...);
}

template <typename... A>
void notice_if(bool cond, const std::string &s, A... args) {
    if (cond) ::negerns::Log::log(log::Priority::Notice, s.c_str(), args...);
}

template <typename... A>
void warning_if(bool cond, const std::string &s, A... args) {
    if (cond) ::negerns::Log::log(log::Priority::Warning, s.c_str(), args...);
}

template <typename... A>
void error_if(bool cond, const std::string &s, A... args) {
    if (cond) ::negerns::Log::log(log::Priority::Error, s.c_str(), args...);
}

template <typename... A>
void critical_if(bool cond, const std::string &s, A... args) {
    if (cond) ::negerns::Log::log(log::Priority::Critical, s.c_str(), args...);
}

template <typename... A>
void fatal_if(bool cond, const std::string &s, A... args) {
    if (cond) ::negerns::Log::log(log::Priority::Fatal, s.c_str(), args...);
}

} //_ namespace log
} //_ namespace negerns



#ifdef _DEBUG
#define LOG_FUNCTION(...)                                               \
    ::negerns::log::trace(                                              \
        ::negerns::log::tracing::utility::Info(__FUNCTION__, __FILE__,  \
            __LINE__),                                                  \
        __VA_ARGS__)

#define TRACE(...)                                                      \
    ::negerns::log::trace(                                              \
        ::negerns::log::tracing::utility::Info(__FUNCTION__, __FILE__,  \
            __LINE__),                                                  \
        __VA_ARGS__)
#else
#define LOG_FUNCTION(...) NOOP(0)
#define TRACE(...)        NOOP(0)
#endif


#endif //_ NEGERNS_CORE_LOG_H
