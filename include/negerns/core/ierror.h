/*******************************************************************************
 * File:    negerns/core/ierror.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 19 May 2013 6:23 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_IERROR_H
#define NEGERNS_CORE_IERROR_H

// Undefine GetMessage in winuser.h
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <string>
#include <negerns/core/declspec.h>

namespace negerns {

class NEGERNS_DECL IError
{
public:
    IError();
    IError(const std::string &c,
        const std::string &m,
        const std::string &d = std::string()) :
        code(c),
        message(m),
        detail(d)
    { }
    virtual ~IError() = default;

    std::string get_code() const { return code; }
    std::string get_message() const { return message; }
    std::string get_detail() const { return detail; }

    void set_code(const std::string &s) { code = s; }
    void set_message(const std::string &s) { message = s; }
    void set_detail(const std::string &s) { detail = s; }

    void set_format(const std::string &s);

    //! Return a formatted error message.
    //!
    //! A format string may be passed to the method or use the default format
    //! string. The default format string is
    //! <code>Code: %s\nError: %s\n\n%s</code>.
    virtual std::string get_string();

private:

    std::string code;
    std::string message;
    std::string detail;

    std::string msgFormat;
};



inline
void negerns::IError::set_format(const std::string &s)
{
    ASSERT(!s.empty(), "Format string cannot be empty.");
    msgFormat = s;
}

} //_ namespace negerns

#endif //_ NEGERNS_CORE_IERROR_H
