/*******************************************************************************
 * File:    negerns/core/member.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 20 Jul 2014 1:50 AM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_MEMBER_H
#define NEGERNS_CORE_MEMBER_H

#include <string>
#include <negerns/core/declspec.h>

namespace negerns {
namespace member {

template <typename T>
class MemberBase
{
public:
    MemberBase() = default;
    MemberBase(T t) : member(t) { }
    MemberBase(const T &t) : member(t) { }
    virtual ~MemberBase() = default;

    void Set(T t) { member = t; }
    T Get() const { return member; }
    bool Equal(T t) { return member == t; }

    MemberBase<T> & operator=(T t) {
        member = t;
        return *this;
    }
    MemberBase<T> & operator=(const T &t) {
        member = t;
        return *this;
    }
private:
    T member;
};



class NEGERNS_DECL String : public MemberBase<std::string>
{
public:
    using MemberBase::MemberBase;
    bool Empty() const;
    bool IsEmpty() const;
};

} //_ namespace member
} //_ namespace negerns

#endif //_ NEGERNS_CORE_MEMBER_H
