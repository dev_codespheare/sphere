/*******************************************************************************
 * File:    negerns/core/platform.h
 * Created: 16 Mar 2013 12:36 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment: Platform information
 ******************************************************************************/

#ifndef NEGERNS_CORE_PLATFORM_H
#define NEGERNS_CORE_PLATFORM_H

#include <string>
#include <negerns/core/declspec.h>

#ifdef __WXMSW__
#include <windows.h>
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "user32.lib")
#endif

namespace negerns {

class NEGERNS_DECL PlatformInfo
{
public:
    PlatformInfo();

    std::string get_os_name() const { return osName; }
    std::string get_architecture() const { return architecture; }
    unsigned int get_major_version() const { return versionMajor; }
    unsigned int get_minor_version() const { return versionMinor; }
    unsigned int get_revision() const { return versionRevision; }
    std::string get_service_pack() const { return servicePack; }

private:
    std::string osName;
    std::string architecture;
    unsigned long versionMajor;
    unsigned long versionMinor;
    unsigned long versionRevision;
    std::string servicePack;

#ifdef __WXMSW__
    void GetWindowsPlatformInfo(const OSVERSIONINFOEX&, const SYSTEM_INFO&);
#endif
};

#ifdef __WINDOWS__
typedef void (WINAPI *PGNSI)(LPSYSTEM_INFO);
typedef BOOL (WINAPI *PGPI)(DWORD, DWORD, DWORD, DWORD, PDWORD);
#endif

} //_ namespace negerns

#endif //_ NEGERNS_CORE_PLATFORM_H
