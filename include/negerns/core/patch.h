/*******************************************************************************
 * File:    negerns/core/patch.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 07 Dec 2013 6:34 PM
 *
 * Comment: This file only defines macro with labels so we could refer to them.
 ******************************************************************************/

#ifndef NEGERNS_CORE_PATCH_H
#define NEGERNS_CORE_PATCH_H

// wxAUI_TB_PLAIN_BACKGROUND does not work in two-step creation of
// wxAuiToolBar when not passing the style argument
// http://trac.wxwidgets.org/ticket/15751
#define UNPATCHED_15751 1


#endif //_ NEGERNS_CORE_PATCH_H