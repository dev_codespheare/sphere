/*******************************************************************************
 * File:    negerns/core/declspec.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 10 Apr 2013 6:33 AM
 *
 * Comment: Import/Export macro definition for the Negerns library.
 ******************************************************************************/

#ifndef NEGERNS_CORE_DECLSPEC_H
#define NEGERNS_CORE_DECLSPEC_H

// Preprocessor Macros
//
// NEGERNS_DLL
//   The library will be built as a dynamic library (.dll). If the macro is
//   not defined, then the library will be built as a static library (.lib).
//
// USING_NEGERNS_DLL
//   The application will be linked with the dynamic library version (.dll).
//   If the macro is not defined, then the application will be linked with
//   the static library version (.lib).



// NOTE:
//
// Define the storage class specifier extern when using exported class
// templates in an executable or another dynamic library module. In the
// dynamic library where the class template is defined the storage class
// specifier must be null (empty).
// Ref: http://support.microsoft.com/kb/168958

#if defined(_WIN32) || defined(__CYGWIN__)
#   ifdef _WINDLL
#       ifdef NEGERNS_DLL
#           ifdef __GNUC__
#               define HAIRPOLL_EXPORT __attribute__ ((dllexport))
#           else
#               define NEGERNS_DECL __declspec(dllexport)
#           endif
#           define NEGERNS_EXPIMP_TEMPLATE
#       else
#           ifdef __GNUC__
#               define HAIRPOLL_EXPORT __attribute__ ((dllimport))
#           else
#               define NEGERNS_DECL __declspec(dllimport)
#           endif
#           define NEGERNS_EXPIMP_TEMPLATE extern
#       endif
#   else
// NOTE:
//
// The following applies when building the application since external
// definitions are checked during linkage.
//
// When the application is being built using dynamic libraries (.dll), the
// application needs to import the definitions from the dynamic libraries.
// To let the linker know of this, the preprocessor macro NEGERNS_DLL must
// be defined.
//
// When the application is being built using static libraries (.lib), there
// is no need to tell the linker where to find the definitions since the
// linker will be merging everything into the executable file. In the case,
// the preprocessor macro NEGERNS_DLL must not be defined in the application
// project.
#       ifdef NEGERNS_DLL
#           ifdef __GNUC__
#               define HAIRPOLL_EXPORT __attribute__ ((dllimport))
#           else
#               define NEGERNS_DECL __declspec(dllimport)
#           endif
#           define NEGERNS_EXPIMP_TEMPLATE extern
#       else
#           define NEGERNS_DECL
#           define NEGERNS_EXPIMP_TEMPLATE
#       endif
#   endif
#else // windows
#   if __GNUC__ >= 4
#       define HAIRPOLL_EXPORT __attribute__ ((visibility ("default")))
#   else
#       define NEGERNS_DECL     // Define empty value for linux OS
#   endif
#   define NEGERNS_EXPIMP_TEMPLATE
#endif

#if 0
#ifndef NEGERNS_DECL
#define NEGERNS_DECL
#endif

#ifndef NEGERNS_EXPIMP_TEMPLATE
#define NEGERNS_EXPIMP_TEMPLATE
#endif
#endif

#endif //_ NEGERNS_CORE_DECLSPEC_H
