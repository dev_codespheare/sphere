/*******************************************************************************
 * File:    negerns/core/icrud.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 14 Oct 2013 8:57 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_ICRUD_H
#define NEGERNS_CORE_ICRUD_H

#include <string>
#include <wx/event.h>
#include <negerns/core/declspec.h>

namespace negerns {

//! CRUD operations interface
//!
//! A very lightweight CRUD framework. This class is intended to be inherited
//! and implemented in subclasses.
class NEGERNS_DECL ICrud
{
public:

    enum class Existence {
        New,
        Old
    };

    enum class State {
        Unmodified,
        Modified
    };

    ICrud();
    ICrud(const ICrud &);
    virtual ~ICrud();

    //! @{
    //! \name Entry point
    //! Intentionally called via menu or toolbar or from another class.
    //! Internally calls corresponding prexxx, onxxx and postxxx functions.
    virtual void Create() final;
    virtual void Read() final;
    virtual void Update() final;
    virtual void Delete() final;
    virtual void Refresh() final;

    virtual void Edit() final;
    //! @}

    virtual bool IsNew() final { return (existence == Existence::New); }

    //! Returns true if the current \c state is \c State::Modified.
    virtual bool IsModified() final { return (state == State::Modified); }

protected:

    virtual void Create(wxCommandEvent&) final;
    virtual void Read(wxCommandEvent&) final;
    virtual void Update(wxCommandEvent&) final;
    virtual void Delete(wxCommandEvent&) final;
    virtual void Refresh(wxCommandEvent&) final;

    virtual void Edit(wxCommandEvent&) final;
    virtual void Undo(wxCommandEvent&) final;
    virtual void Redo(wxCommandEvent&) final;

    //! Internally called by ICrud::Create(wxCommandEvent &).
    //!
    //! Subclasses should implement this for specific functionality.
    virtual bool OnCreate(wxCommandEvent&);

    //! Internally called by ICrud::Read(wxCommandEvent &).
    //!
    //! Subclasses should implement this for specific functionality.
    virtual bool OnRead(wxCommandEvent&);

    //! Internally called by ICrud::Update(wxCommandEvent &).
    //!
    //! Subclasses should implement this for specific functionality.
    virtual bool OnUpdate(wxCommandEvent&);

    //! Internally called by ICrud::Delete(wxCommandEvent &).
    //!
    //! Subclasses should implement this for specific functionality.
    virtual bool OnDelete(wxCommandEvent&);

    //! Internally called by ICrud::Refresh(wxCommandEvent &).
    //!
    //! Subclasses should implement this for specific functionality.
    virtual bool OnRefresh(wxCommandEvent&);

    //! Called internally before ICrud::Create.
    //!
    //! If ICrud::PreCreate returns false, ICrud::Create is not called.
    virtual bool PreCreate(wxCommandEvent&);

    //! Called internally before ICrud::Read.
    //!
    //! If ICrud::PreRead returns false, ICrud::Read is not called.
    virtual bool PreRead(wxCommandEvent&);

    //! Called internally before ICrud::Update.
    //!
    //! If ICrud::PreUpdate returns false, ICrud::Update is not called.
    virtual bool PreUpdate(wxCommandEvent&);

    //! Called internally before ICrud::Delete.
    //!
    //! If ICrud::PreDelete returns false, ICrud::Delete is not called.
    virtual bool PreDelete(wxCommandEvent&);

    //! Called internally before ICrud::Refresh.
    //!
    //! If ICrud::PreRefresh returns false, ICrud::Refresh is not called.
    virtual bool PreRefresh(wxCommandEvent&);

    //! Called internally after ICrud::Create.
    //!
    //! If ICrud::Create returns false, this method is not called.
    virtual bool PostCreate(wxCommandEvent&);

    //! Called internally after ICrud::Read.
    //!
    //! If ICrud::Read returns false, this method is not called.
    virtual bool PostRead(wxCommandEvent&);

    //! Called internally after ICrud::Update.
    //!
    //! If ICrud::Update returns false, this method is not called.
    virtual bool PostUpdate(wxCommandEvent&);

    //! Called internally after ICrud::Delete.
    //!
    //! If ICrud::Delete returns false, this method is not called.
    virtual bool PostDelete(wxCommandEvent&);

    //! Called internally after ICrud::Refresh.
    //!
    //! If ICrud::Refresh returns false, this method is not called.
    virtual bool PostRefresh(wxCommandEvent&);

    virtual void OnEdit(wxCommandEvent&);

    virtual void OnUndo(wxCommandEvent&);

    virtual void OnRedo(wxCommandEvent&);

    virtual void SetState(State) final;

    virtual void SetModified(bool b = true) final;

    virtual void SetUnmodified(bool b = true) final;

private:

    Existence existence;

    //! Status of the data.
    //!
    //! \see GetStatus()
    State state;
};

} //_ namespace negerns

#endif //_ NEGERNS_CORE_ICRUD_H
