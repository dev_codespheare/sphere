/*******************************************************************************
 * File:    negerns/core/message.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 22 Oct 2013 3:39 PM
 *
 * Comment: Message Dialog
 *          This is a GUI class but moved to the framework project to avoid
 *          cyclical dependency between the gui and framework projects.
 ******************************************************************************/

#ifndef NEGERNS_CORE_MESSAGE_H
#define NEGERNS_CORE_MESSAGE_H

#include <string>
#include <wx/string.h>
#include <wx/msgdlg.h>
#include <wx/notifmsg.h>
#include <negerns/core/declspec.h>
#include <negerns/core/string.h>

namespace negerns {

class NEGERNS_DECL Message
{
public:

    enum Button {
        Close = wxOK,
        YesNo = wxYES_NO,
        YesNoCancel = wxYES_NO | wxCANCEL,
        Apply = wxAPPLY
    };

    enum Reponse {
        Yes = wxID_YES,
        No = wxID_NO,
        Cancel = wxID_CANCEL,
        Ok = wxID_OK
    };

    static const long none = wxICON_NONE;
    static const long information = wxICON_INFORMATION;
    static const long exclamation = wxICON_EXCLAMATION;
    static const long warning = wxICON_EXCLAMATION;
    static const long question = wxICON_QUESTION;
    static const long error = wxICON_ERROR;

    static void Notify(const std::string &title, const std::string &msg);
    static int Assert(const std::string &msg, const std::string &emsg = std::string(),
        long button = Button::YesNoCancel, long icon = Message::exclamation);
    static int Info(const std::string &msg, const std::string &emsg = std::string(),
        long button = Button::Close, long icon = Message::information);
    static int Warning(const std::string &msg, const std::string &emsg = std::string(),
        long button = Button::YesNoCancel | Button::Apply, long icon = Message::exclamation);
    static int Error(const std::string &msg, const std::string &emsg = std::string(),
        long button = Button::YesNoCancel, long icon = Message::error);
    static int Question(const std::string &msg, const std::string &emsg = std::string(),
        long button = Button::YesNoCancel, long icon = Message::question);

    static void NotifyIf(bool, const std::string &title, const std::string &msg);
    static int InfoIf(bool, const std::string &msg, const std::string &emsg = std::string());
    static int WarningIf(bool, const std::string &msg, const std::string &emsg = std::string());
    static int ErrorIf(bool, const std::string &msg, const std::string &emsg = std::string());
    static int QuestionIf(bool, const std::string &msg, const std::string &emsg = std::string(),
        long button = Button::YesNoCancel, long icon = Message::question);

    Message();

    Message(const std::string& msg, const std::string& emsg = "",
        long button = Button::YesNoCancel, long icon = Message::question);

    virtual ~Message();

    //! Set the parent window.
    static void SetParent(wxWindow* p);
    static void SetTitle(const std::string& s);

    void SetMessage(const std::string &);
    void SetExtMessage(const std::string &);
    void SetIcon(long n);
    void SetButtons(long n);
    int Show();
    int ShowIf(bool);
protected:
    static wxNotificationMessage note;
    //! Parent window which must be set before using this class.
    //!
    //! Must be called during application startup.
    static wxWindow* parent;

    //! Dialog window title
    static std::string title;

    //! Message
    std::string msg;

    //! Extended message
    std::string emsg;

    long iconStyle;
    long buttonsStyle;
};

//! Prompts a message.
#define MESSAGE_FUNC() negerns::Message::Info("Function called.", \
    negerns::Format("Function: %1\nLine: %2\nFilename: %3", \
    __FUNCSIG__, __LINE__, __FILE__))

} //_ namespace negerns

#endif //_ NEGERNS_CORE_MESSAGE_H
