/*******************************************************************************
 * File:    negerns/core/nameindex.h
 * Created: 14 Feb 2014 6:26 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_NAMEINDEX_H
#define NEGERNS_CORE_NAMEINDEX_H

#include <string>
#include <map>
#include <functional>
#include <negerns/core/vector.h>

#include <negerns/core/declspec.h>

namespace negerns {

//! Encapsulates a string to index mapping container.
//!
//! \see NameIndexIFace
class NEGERNS_DECL NameIndexContainer : public Vector<std::string>
{
public:
    //! Constructor.
    NameIndexContainer() = default;

    //! Destructor.
    virtual ~NameIndexContainer();

    //! Add an element.
    //!
    //! The index value is the current size of the string to index mapping
    //! container less one.
    virtual void Add(const std::string &s) override;

    //! Return the string in the specified index.
    virtual std::string& GetName(std::size_t) final;

    //! Remove all elements.
    virtual void Clear() override;

protected:
    //! String to index mapping container.
    std::map<std::string, std::size_t> name;

}; //- class NameIndexContainer



// ----------------------------------------------------------------------------------------



//! String and numeric subscript operator interface to access a data source.
//!
//! The template parameter is the type of data source element to be returned
//! by the subscript operator(s).
//!
//! \see NameIndexContainer
//! \see NameIndexDataSourceAccessIFace
template <typename T>
class NEGERNS_DECL NameIndexIFace
{
public:

    class NEGERNS_DECL Iterator
    {
    public:
        Iterator(NameIndexIFace<T> *ni, std::size_t n) :
            ref(ni),
            pos(n)
        { }

        bool operator!=(const Iterator &it) const {
            return pos != it.pos
        }

        const std::string& operator*() const {
            return ref[pos];
        }

        const Iterator& operator++() {
            ++pos;
            // Although not strictly necessary for a range-based for loop
            // following the normal convention of returning a value from
            // operator++ is a good idea.
            return *this;
        }
    private:
        NameIndexIFace<T> *ref;
        std::size_t pos;
    };

    //! Type of function to be called by the subscript access operators
    //! to acquire the data from a data source.
    //!
    //! \see NameIndexIface::SetGetDataFunction
    //! \see NameIndexIface::operator[](std::size_t)
    typedef std::function< T& (std::size_t)> GetDataFunction;

    //! Constructor.
    NameIndexIFace() : ref(nullptr) { }

    //! Constructor.
    //!
    //! Set the reference to the class that inherited \c NameIndexContainer
    //! and the reference to the data source object.
    //!
    //! \see NameIndexIFace::Set
    //! \see NameIndexIFace::ref
    //! \see NameIndexIFace::datasource
    NameIndexIFace(NameIndexContainer *src) : ref(src) { }

    //! Set the reference to the class that inherited \c NameIndexContainer
    //! and the reference to the data source object.
    //!
    //! \see NameIndexIFace::datasource
    void Set(NameIndexContainer *src, GetDataFunction f) {
        ref = src;
        GetDataFunc = std::forward<GetDataFunction>(f);
    }

    //! Set the reference to the class that inherited \c NameIndexContainer.
    //!
    //! \see NameIndexIFace::ref
    void SetNameIndex(NameIndexContainer *src) { ref = src; }

    //! Set the function to be called to acquire data from a data source
    //! which will be returned by the subscript access operators.
    //!
    //! ~~~~~~~~~~{.cpp}
    //! auto f = std::bind(&Class::func, this,
    //!             std::placeholders::_1,
    //!             std::placeholders::_2);
    //! SetGetDataFunction(f);
    //! ~~~~~~~~~~
    //!
    //! \see NameIndexIFace::GetDataFunction
    void SetGetDataFunction(GetDataFunction f) {
        GetDataFunc = std::forward<GetDataFunction>(f);
    }

    //! Get the reference to the \c NameIndexContainer.
    //!
    //! \see NameIndexIFace::ref
    NameIndexContainer *GetNameIndexContainer() { return ref; }

    Iterator begin() { return Iterator(this, 0); }

    Iterator end() { return Iterator(this, ref->Count()); }

    //! Numeric subscript access to an element of the referenced data source.
    T& operator[](const std::size_t i) {
        return GetDataFunc(i);
    }

    //! Numeric subscript access to an element of the referenced data source.
    const T& operator[](const std::size_t i) const {
        return GetDataFunc(i);
    }

    //! String subscript access to an element of the referenced data source.
    //!
    //! \see NameIndexContainer::Index
    T& operator[](const std::string &s) {
        auto i = ref->Index(s);
        return GetDataFunc(i);
    }

    //! String subscript access to an element of the referenced data source.
    //!
    //! \see NameIndexContainer::Index
    const T& operator[](const std::string &s) const {
        auto i = ref->Index(s);
        return GetDataFunc(i);
    }

    //! Get name in the specified index.
    std::string GetName(std::size_t i) { return ref->GetName(i); }

    virtual bool Empty() const final { return ref->Empty(); }

    virtual bool IsEmpty() const final { return ref->IsEmpty(); }

private:

    //! Reference to the class that inherited the string to index mapping
    //! container class.
    NameIndexContainer *ref;

    //! Function pointer to be called by the subscript access operators
    //! to acquire data from a data source.
    GetDataFunction GetDataFunc;

}; //- class NameIndexIFace

} //_ namespace negerns

#endif //_ NEGERNS_CORE_NAMEINDEX_H
