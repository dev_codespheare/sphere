/*******************************************************************************
 * File:    negerns/core/system.h
 * Created: 27 Feb 2014 2:55 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_CORE_SYSTEM_H
#define NEGERNS_CORE_SYSTEM_H

#include <string>
#include <boost/logic/tribool.hpp>
#include <negerns/core/declspec.h>
#include <negerns/gui/pane.h>

#include <negerns/poco.h>

namespace negerns {

class Configuration;
class EventManager;

namespace gui {
class Application;
}

class NEGERNS_DECL System
{
public:

    //! Set or get the Application subclass instance.
    static gui::Application* Application(gui::Application *app = nullptr);

    //! Get or set the configuration object.
    static negerns::Configuration* Configuration(negerns::Configuration *cfg = nullptr);

    //! Set or get the event manager object.
    static negerns::EventManager* EventManager(negerns::EventManager *em = nullptr);

    //! Get the Pane object at the specified index.
    //! If the specified index does not exist, it returns \c nullptr.
    static gui::Pane* Pane(std::size_t);

    //! @{
    //! \name Application States
    //!
    //! The application states are application specific. The states are set
    //! programmatically within the application logic.
    //!
    //! %Application Loading State - state of the application when it is
    //! in the process of loading itself, loading resources, initialization,
    //! etc. This state is intended to be set only during application startup.
    //! Setting this state after application startup is not recommended or
    //! advisable.
    //!
    //! %Application Exit State - state of the application when it is in
    //! the process of exiting like closing the application, unloading of all
    //! resources, etc. This state is intended to be set only during application
    //! shutdown. Setting this state other than during the application shutdown
    //! is not recommended or advisable.

    //! Set the application loading state.
    static void set_loading_state(boost::logic::tribool b = true);

    //! Return the application loading state.
    //!
    //! \returns \c true if the application is still loading. Otherwise,
    //! returns \c false.
    //!
    //! \internal
    //! Because using the built-in type \c bool logically sets the application
    //! loading state, there is no way to determine if we are setting or just
    //! checking it. Using the \c boost::tribool helps in this situation. Also,
    //! notice that the default argument value is neither \c true or \c false.
    //! Thus, calling the method without an argument serves its purpose.
    //! \endinternal
    static bool is_loading(boost::logic::tribool b = boost::logic::indeterminate);

    //! Set the application exit state.
    static void set_exit_state(boost::logic::tribool b = true);

    //! Return the application exit state.
    //!
    //! \returns \c true if the application is still loading. Otherwise,
    //! returns \c false.
    //!
    //! \internal
    //! Because using the built-in type \c bool logically sets the application
    //! loading state, there is no way to determine if we are setting or just
    //! checking it. Using the \c boost::tribool helps in this situation. Also,
    //! notice that the default argument value is neither \c true or \c false.
    //! Thus, calling the method without an argument serves its purpose.
    //! \endinternal
    static bool is_exiting(boost::logic::tribool b = boost::logic::indeterminate);

    //! @}
};

} //_ namespace negerns

#endif //_ NEGERNS_CORE_SYSTEM_H
