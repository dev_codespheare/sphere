/*******************************************************************************
 * File:    negerns/core/concurrent/queue.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 21 Aug 2014 10:51 PM
 *
 * Comment: Logging Facility
 ******************************************************************************/

#ifndef NEGERNS_CORE_CONCURRENT_QUEUE_H
#define	NEGERNS_CORE_CONCURRENT_QUEUE_H

#include <memory>
#include <condition_variable>
#include <queue>

namespace negerns {
namespace concurrent {

//! Thread-Safe Queue
//!
//! Implementation is taken from C++ Concurrency in Action, Manning, 2012.
template<typename T>
class queue
{
private:
    // Locking a mutex is a mutating operation. The mutable keyword allows the
    // mutex to be locked in the copy constructor and empty().
    mutable std::mutex mut;
    std::queue<T> data;
    std::condition_variable condition;

public:

    //! Default constructor.
    queue() {}

    //! Copy constructor.
    queue(const queue& other)
    {
        std::lock_guard<std::mutex> lk(other.mut);
        data = other.data;
    }

    //! No copy assignment.
    queue& operator=(const queue&) = delete;

    //! Add an item to the end of the queue and notifies a waiting thread.
    void push(T new_value)
    {
        std::lock_guard<std::mutex> lk(mut);
        data.push(new_value);
        condition.notify_one();
    }

    //! Waits for an available item in the queue. The waiting thread is
    //! notified when an item is available in the queue and passes the item
    //! at the front of the queue to the reference argument.
    void wait_and_pop(T& value)
    {
        std::unique_lock<std::mutex> lk(mut);
        // Wait until the queue is not empty.
        //
        // The condition is evaluated again on notify_one() or notify_all() or
        // due to a *spurious wake*.
        //
        // > The number and frequency of any such spurious wakes are
        // > indeterminate. Therefore, it is advisable to use a function
        // > without side effects for the condition check.
        //
        // (Re)acquires the lock on the mutex and checks the condition. If
        // the condition evaluates to false, the thread unlocks the mutex and
        // resumes waiting. Otherwise, the thread unlocks the mutex before
        // the function returns.
        condition.wait(lk, [this] { return !data.empty(); });

        // Source is copied to a buffer first before deleting the source.
        //
        // Strong exception guarantee:
        // Operations can fail, but failed operations are guaranteed to have
        // no side effects so all data retain original values.
        // http://en.wikipedia.org/wiki/Exception_safety
        value = data.front();
        data.pop();
    }

    //! Waits for an available item in the queue. The waiting thread is
    //! notified when an item is available in the queue and returns the item
    //! at the front of the queue.
    //!
    //! Client can check whether the returned value contains a valid pointer
    //! via operator bool() or calling get and compared to nullptr.
    std::shared_ptr<T> wait_and_pop()
    {
        std::unique_lock<std::mutex> lk(mut);
        condition.wait(lk, [this] { return !data.empty(); });
        std::shared_ptr<T> res(std::make_shared<T>(data.front()));
        data.pop();
        return res;
    }

    //! Try to get an item from the front of the queue and returns immediately.
    //! If the queue is not empty then the function returns true and the
    //! item at the front of the queue is passed to the reference argument.
    //!
    //! ~~~~~~~~~~{.cpp}
    //! Data d;
    //! if (try_pop(d)) {
    //!     // do something with the data
    //! }
    //! ~~~~~~~~~~
    bool try_pop(T& value)
    {
        std::lock_guard<std::mutex> lk(mut);
        if (data.empty()) {
            return false;
        }
        value = data.front();
        data.pop();
        return true;
    }

    //! Try to get an item from the front of the queue and returns immediately.
    //! If the queue is not empty then the function returns the item at the
    //! front of the queue as a shared_ptr.
    //!
    //! Client can check whether the returned value contains a valid pointer
    //! via operator bool() or calling get and compared to nullptr.
    //!
    //! ~~~~~~~~~~{.cpp}
    //! auto d = try_pop();
    //! if (d.get() != nullptr) {
    //!     // do something with the data
    //! }
    //! ~~~~~~~~~~
    std::shared_ptr<T> try_pop()
    {
        std::lock_guard<std::mutex> lk(mut);
        if (data.empty()) {
            return std::shared_ptr<T>();
        }
        std::shared_ptr<T> res(std::make_shared<T>(data.front()));
        data.pop();
        return res;
    }

    //! Returns true if the queue is empty.
    bool empty() const
    {
        std::lock_guard<std::mutex> lk(mut);
        return data.empty();
    }
};

} //_ namespace concurrent
} //_ namespace negerns

#endif //_ NEGERNS_CORE_CONCURRENT_QUEUE_H
