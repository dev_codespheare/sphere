/*******************************************************************************
 * File:    negerns/core/defs.h
 * Created: 18 Dec 2013 11:44 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment: Macros and other declarations
 ******************************************************************************/

#include <wx/defs.h>

// For use with #if to exclude code blocks from compilation
// This makes it easier to categorize excluded code blocks
// which may be used for testing, buggy code, etc.
#define EXCLUDE 0
#define EXCLUDE_BUG 0
#define EXCLUDE_TEST 0
#define EXCLUDE_EXPERIMENT 0
#define EXCLUDE_FOR_DELETION 0

#define INCLUDE 1
#define INCLUDE_TEST 1
#define INCLUDE_EXPERIMENT 1

#ifndef NEGERNS_CORE_DEFS_H
#define NEGERNS_CORE_DEFS_H

#if defined(_WIN32)
#define UNUSED(P)   UNREFERENCED_PARAMETER(P)
#endif

namespace negerns {

enum Style : int { None = 0 };

enum class Operation {
    Yes = wxID_YES,
    No = wxID_NO,
    Cancel = wxID_CANCEL
};

enum Proportion {
    NotChangeable = 0,
    Static = 0,
    Fixed = 0,
    Changeable = 1,
    Dynamic = 1,
    Relative = 1
};

enum Alignment {
    Invalid = wxALIGN_INVALID,
    Not = wxALIGN_NOT,
    CenterHorizontal = wxALIGN_CENTER_HORIZONTAL,
    CentreHorizontal = wxALIGN_CENTER_HORIZONTAL,
    Left = wxALIGN_LEFT,
    Top = wxALIGN_TOP,
    Right = wxALIGN_RIGHT,
    Bottom = wxALIGN_BOTTOM,
    CenterVertical = wxALIGN_CENTER_VERTICAL,
    CentreVertical = wxALIGN_CENTER_VERTICAL,
    Center = wxALIGN_CENTER,
    Centre = wxALIGN_CENTER,
    Mask = wxALIGN_MASK
};

enum Orientation {
    Horizontal = wxHORIZONTAL,
    Vertical = wxVERTICAL,
    Both = wxHORIZONTAL | wxVERTICAL
};

} //_ namespace negerns

#endif //_ NEGERNS_CORE_DEFS_H
