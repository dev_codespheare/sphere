/*****************************************************************************
 * File:    negerns/core/version.h
 * Created: 20 Jun 2013 4:36 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Purpose: Framework version constants
 *****************************************************************************/

// NOTE: VS2008 spits out an error when using a header guard.

#include <string>
#include <boost/preprocessor/stringize.hpp>
#include <negerns/core/declspec.h>

#define VER_VERSION_MAJOR       0
#define VER_VERSION_MINOR       1
#define VER_VERSION_REVISION    0
#define VER_VERSION_INTERNAL    0

// This string should be present only if VS_FF_PRIVATEBUILD is specified
// in the fileflags parameter of the root block.
#ifdef _DEBUG
#define VER_PRODUCT_PRIVATEBUILD_STR "Development build\0"
#else
#define VER_PRODUCT_PRIVATEBUILD_STR ""
#endif

#define VER_PRODUCT_SPECIALBUILD_STR ""

#define VER_INTERNALNAME_STR "negerns\0"
#define VER_ORIGINALFILENAME_STR "negerns.dll\0"
#define VER_FILEDESCRIPTION_STR "Negerns library\0"

#define VER_COMPANYNAME_STR "\0"
#define VER_PRODUCT_AUTHOR "Ricky Maicle"
#define VER_PRODUCT_DATE_YEAR "2013"
#define VER_PRODUCTNAME_STR "Negerns Library\0"
#define VER_LEGALCOPYRIGHT_STR "Copyright " VER_PRODUCT_DATE_YEAR " " VER_PRODUCT_AUTHOR "\nAll rights reserved.\0"

#ifdef _WINDLL
#define VER_FILETYPE VFT_DLL
#else
#define VER_FILETYPE VFT_STATIC_LIB
#endif

#ifdef _DEBUG
#define VER_FILEFLAGS VS_FF_DEBUG | VS_FF_PRERELEASE
#else
#define VER_FILEFLAGS 0x0L
#endif

// NOTE:
// The following macro definitions are derivatives of the above definitions.
// Editing of the following macro definitions are for internal use only.
// -------------------------------------------------------------------------

#ifdef _DEBUG
#define VER_FILEVERSION VER_VERSION_MAJOR,VER_VERSION_MINOR,VER_VERSION_REVISION,VER_VERSION_INTERNAL
#else
#define VER_FILEVERSION VER_VERSION_MAJOR,VER_VERSION_MINOR,VER_VERSION_REVISION
#endif

#ifdef _DEBUG
#define VER_VERSION_STR \
    BOOST_PP_STRINGIZE(VER_VERSION_MAJOR) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_MINOR) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_REVISION) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_INTERNAL)
#else
#define VER_VERSION_STR \
    BOOST_PP_STRINGIZE(VER_VERSION_MAJOR) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_MINOR) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_REVISION)
#endif

#define VER_FILEVERSION_STR VER_VERSION_STR "\0"

#if 0
// A few helpful macro for version checking
// ----------------------------------------
//! \brief Check if the specified version is at least major.minor.release
//!
//! Logic from wxWidgets wxCHECK_VERSION (version.h)
#define FW_CHECK_VERSION(major,minor,revision) \
    (VER_VERSION_MAJOR > (major) || \
    (VER_VERSION_MAJOR == (major) && VER_VERSION_MINOR > (minor)) || \
    (VER_VERSION_MAJOR == (major) && VER_VERSION_MINOR == (minor) && VER_VERSION_REVISION >= (revision)))
#endif

#define NEGERNS_MAJOR_MULTIPLIER    1000000
#define NEGERNS_MINOR_MULTIPLIER      10000
#define NEGERNS_REVISION_MULTIPLIER     100
#define NEGERNS_INTERNAL_MULTIPLIER       1

#define NEGERNS_MAKE_VERSION(major, minor, revision) \
((major) * NEGERNS_MAJOR_MULTIPLIER + (minor) * NEGERNS_MINOR_MULTIPLIER + (revision) * NEGERNS_REVISION_MULTIPLIER)

#define NEGERNS_MAKE_VERSION_INTERNAL(major, minor, revision, internal) \
((major) * NEGERNS_MAJOR_MULTIPLIER + (minor) * NEGERNS_MINOR_MULTIPLIER + (revision) * NEGERNS_REVISION_MULTIPLIER + (internal * NEGERNS_INTERNAL_MULTIPLIER))

#define NEGERNS_VERSION \
NEGERNS_MAKE_VERSION(VER_VERSION_MAJOR, VER_VERSION_MINOR, VER_VERSION_REVISION)

#define NEGERNS_VERSION_INTERNAL \
NEGERNS_MAKE_VERSION_INTERNAL(VER_VERSION_MAJOR, VER_VERSION_MINOR, VER_VERSION_REVISION, VER_VERSION_INTERNAL)



namespace negerns {

//! Release numbering.
//!
//! It follows the \c major, \c minor, \c revision numbering scheme with an
//! additional internal numbering after the \c revision number. It allows
//! release numbering up to 99.99.99.99.
//!
//! The version number can be converted to an \c integer value that can be used
//! in conditional statements. The following formula is used:
//!
//! ~~~~~~~~~~
//! major    * 1,000,000
//! minor    *    10,000
//! patch    *       100
//! internal *         1
//! ~~~~~~~~~~
//!
//! The maximum \c integer value is 99,999,999.
//!
//! The version number can also be converted to a \c string in the following
//! format: <code>major.minor.revision.internal</code>.
class NEGERNS_DECL Version final
{
private:
    Version() = delete;
    virtual ~Version() = delete;
public:

    //! Returns true if the version number is at least the specified value.
    static bool at_least(const int major,
        const int minor = 0,
        const int revision = 0);

    //! Returns true if the version number is equal to the specified value.
    static bool is(const int major,
        const int minor = 0,
        const int revision = 0);

    //! %Convert the specified version numbers to an \c integer.
    static int to_int(const int major,
        const int minor = 0,
        const int revision = 0,
        const int internal = 0);

    //! %Convert the version number to an \c integer.
    static const int to_int();

    //! %Convert the version number to a \c string.
    static std::string to_string();

    static const int major = VER_VERSION_MAJOR;
    static const int minor = VER_VERSION_MINOR;
    static const int revision = VER_VERSION_REVISION;
    static const int internal = VER_VERSION_INTERNAL;
};

} //_ namespace negerns
