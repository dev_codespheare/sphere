/*******************************************************************************
 * File:    negerns/core/convert.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 7 Jan 2014 4:35 PM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERN_CORE_CONVERT_H
#define NEGERN_CORE_CONVERT_H

#include <string>
#include <wx/string.h>
#include <negerns/core/declspec.h>

namespace negerns {

class NEGERNS_DECL Convert
{
public:
};

} // negerns

#endif //_ NEGERN_CORE_CONVERT_H
