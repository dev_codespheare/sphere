/*******************************************************************************
 * File:    negerns/core/types.h
 * Created:
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment: Type alias
 ******************************************************************************/

using uchar = unsigned char;
using wchar = wchar_t;
using ushort = unsigned short;
using uint = unsigned int;
using ulong = unsigned long;
using llong = long long;
using ullong = unsigned long long;
