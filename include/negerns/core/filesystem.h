/*******************************************************************************
 * File:    negerns/core/filesystem.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 8 Sep 2014 3:11 PM
 *
 * Comment: Wrapper for VC++ implementation of TR2 filesystem header.
 *          When this becomes C++ standard maybe in C++14 then this
 *          header will no longer be needed.
 ******************************************************************************/

#ifndef NEGERNS_CORE_FILESYSTEM_H
#define NEGERNS_CORE_FILESYSTEM_H

#include <string>
#include <negerns/core/declspec.h>

namespace negerns {
namespace filesystem {

NEGERNS_DECL bool exists(const std::string &);

NEGERNS_DECL std::string current_path();

NEGERNS_DECL bool create_directory(const std::string &s);

NEGERNS_DECL std::string parent_path();

NEGERNS_DECL std::string parent_path(const std::string &s);

} //_ namespace filesystem
} //_ namespace negerns

#endif //_ NEGERNS_CORE_FILESYSTEM_H
