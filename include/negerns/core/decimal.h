/*******************************************************************************
 * File:    negerns/core/decimal.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 13 Mar 2015 8:00 AM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_DECIMAL_H
#define NEGERNS_CORE_DECIMAL_H

#include <string>
#include <negerns/core/internal/decimal.h>

#include <negerns/core/declspec.h>

//. TODO: Use MPFR C++ library (an interface for the MPFR library)
//. TODO: Or wait for C++ standardization or Boost implementation.
//.
//. The MPFR library is a C library for multiple-precision floating-point computations
//. with correct rounding.
//.
//. The main goal of MPFR is to provide a library for multiple-precision floating-point
//. computation which is both efficient and has a well-defined semantics. It copies the
//. good ideas from the ANSI/IEEE-754 standard for double-precision floating-point
//. arithmetic (53-bit significand).
//.
//. MPFR C++ is under GPL. Using it commercially requires purchasing the library from
//. the author (Pavel Holoborodko). See link below for author info.
//.
//. [MPFR Library] http://www.mpfr.org/
//. [MPFR C++] http://www.holoborodko.com/pavel/mpfr/#intro

namespace negerns {

typedef ::dec::decimal<2> dec2_t;
typedef ::dec::decimal<4> dec4_t;
typedef ::dec::decimal<6> dec6_t;
typedef ::dec::decimal<8> dec8_t;

typedef ::dec::decimal<2> currency_t;
typedef ::dec::decimal<4> foreign_exchange_rate_t;

typedef currency_t currency;
typedef foreign_exchange_rate_t forex_rate;

namespace decimal {

// Maximim of int64_t is 9223372036854775807

// Maximum of 64-bit signed integer type with 2 decimal digits is 92,233,720,368,547,758
const int64_t prec2_max = (((std::numeric_limits<int64_t>::max)()) / 100);

// Maximum of 64-bit signed integer type with 4 decimal digits is 922,337,203,685,477
const int64_t prec4_max = (((std::numeric_limits<int64_t>::max)()) / 10000);

//! Convert a string to decimal type without rounding.
//!
//! \code
//! auto d = to_dec2("1.7777");     // 1.77
//! auto d = to_dec4("1.7777");     // 1.7777
//! auto d = to_dec4("1.7777778");  // 1.777777
//! \endcode

NEGERNS_DECL
dec2_t to_dec2(const std::string &arg);

NEGERNS_DECL
dec4_t to_dec4(const std::string &arg);

NEGERNS_DECL
dec6_t to_dec6(const std::string &arg);

//! Convert a strng to decimal type with rounding.
//!
//! \code
//! auto d = to_dec2r("1.7777");   // 1.78
//! auto d = to_dec4r("1.7777");   // 1.7778
//! \endcode

NEGERNS_DECL
dec2_t to_dec2r(const std::string &arg);

NEGERNS_DECL
dec4_t to_dec4r(const std::string &arg);

NEGERNS_DECL
dec6_t to_dec6r(const std::string &arg);


//! Convert decimal to string.

NEGERNS_DECL
std::string to_string(dec2_t);

NEGERNS_DECL
std::string to_string(dec4_t);

NEGERNS_DECL
std::string to_string(dec6_t);

} //_ namespace decimal
} //_ namespace negerns

#endif //_ NEGERNS_CORE_DECIMAL_H
