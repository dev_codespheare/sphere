/*******************************************************************************
 * File:    negerns/core/utility.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 13 Mar 2015 10:56 AM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_UTILITY_H
#define NEGERNS_CORE_UTILITY_H

#include <negerns/core/string.h>

#include <negerns/core/declspec.h>

//! Does nothing except allow debugging to step through it.
//! This is a convenience macro that allows the debugger to step through this
//! line without executing anything. It is useful when debugging a block of
//! code where the use wants to see the value of a variable or an expression
//! before the instruction leaves the enclosing block.
#define EMPTY_LINE      do { } while (false);

namespace negerns {

//! Returns the first argument if it is less than or equal to the specified limit.
//! Otherwise, the limit is returned.
//!
//! T and U should be numeric and comparable.
//!
//! This is useful in situations when ensuring a specified range of values.
//! Usually it is enforced using an if statement. That same logic is encapsulated into
//! this template function.
//!
//! void func(int n) {
//!   if (n > 100) n = 100;
//!   ...
//! }
//!
//! Could be written like:
//!
//! void func(int n) {
//!   n = limit_to<int>(n, 100);
//!   ...
//! }
//. TODO: Constrain to numeric values ?
template <typename T>
T limit_to(T t, T max)
{
    return t <= max ? t : max;
}

//! Return the second argument only if the condition is true. Otherwise, it returns the
//! third argument.
//!
//! Two possible reasons against this template function are:
//!   - client code have to specify the return type
//!   - C++ have the ternary operator (which this function uses)
//! One benefit of using this function is that it is searchable.
//!
//! void func(int n) {
//!   std::string s("Normal");
//!   if (n > 100) {
//!     some.assign("Greater than 100");
//!   }
//!   ...
//! }
//!
//! Could be written like:
//!
//! void func(int n) {
//!   string s(iff<string>(n > 100, "Normal", "Greater than 100"));
//!   ...
//! }
template <typename T>
T iff(bool cond, T t, T f)
{
    return (!!!(condition)) ? t : f;
}

template <typename T>
bool in(T t, const std::vector<T> &v)
{
    for (auto &i : v) {
        if (i == t) {
            return true;
        }
    }
    return false;
}

} //_ namespace negerns

#endif //_ NEGERNS_CORE_UTILITY_H
