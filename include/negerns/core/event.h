/*******************************************************************************
 * File:    negerns/core/event.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 4 Nov 2013 10:29 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef NEGERNS_CORE_EVENT_H
#define NEGERNS_CORE_EVENT_H

#include <functional>
#include <vector>
#include <wx/event.h>
#include <negerns/core/defs.h>
#include <negerns/core/memory.h>
#include <negerns/data/row.h>
#include <negerns/core/declspec.h>
#include <negerns/negerns.h>
#include <negerns/poco.h>

// Reference(s):
//
// C++11 Variadic Templates and Functor with a variable number of member variables?
// http://www.cplusplus.com/forum/general/63061/

using namespace std::placeholders;

namespace negerns {
namespace event {

    typedef size_t ContextId;

    //! Event Types
    //!
    //! Event types are C-style enumerations. This allows new types to be
    //! defined. User-specified types are Type::Max + 1. To define new
    //! types in this level, the enumeration must be in the namespace
    //! \c negerns::event.
    enum Type : size_t
    {
        None = 0,
        Output,
        CreateControl,
        InitContent,

        DataCreate,
        DataRead,
        DataReadRows,
        DataUpdate,
        DataDelete,
        DataReceive,    // Pass data to receivers

        TriggerCreate,
        TriggerRefresh,
        TriggerUpdate,
        TriggerDelete,

        Max
    };

} //_ namespace event

//! Convenience macro for passing an event handler to EventManager::Add
#define EFUNC(pmf, instance) std::bind(pmf, instance, std::placeholders::_1)

struct NEGERNS_DECL EventData
{
    EventData();

    EventData(negerns::data::Row);

    virtual ~EventData();

    //! Set a pointer to the Row object.
    void SetRow(std::size_t, negerns::data::Row);

    //! Get row index.
    std::size_t GetIndex() const;

    //! Returns a pointer to the Row object.
    negerns::data::Row & GetRow();

private:

    //! Row index in the source rows container.
    std::size_t rowIndex;

    negerns::data::Row row;
};



typedef std::function<void(EventData*)> EventFunction;
typedef std::vector<size_t> EventPositions;



//! Structure holding event information
struct NEGERNS_DECL Event
{
    Event(event::ContextId, event::Type);
    Event(event::ContextId, event::Type, EventFunction f);
    virtual ~Event();

    //! Trigger the event handler as function call.
    void operator()(EventData *data);

    //! Context ID.
    event::ContextId context;

    //! Event type
    event::Type type;

    //! Pointer to event handler.
    EventFunction func;
};



//! Event manager.
//!
//! \todo Add asynchronous event dispatching.
//! \todo Add event handlers without arguments.
//! \todo Add helper functions for debugging.
class NEGERNS_DECL EventManager
{
public:
    EventManager();

    virtual ~EventManager();
#if INCLUDE_EXPERIMENT
    //! A debugging aid to check whether the supplied context id is available
    //! for use. This check must be performed only prior to adding events and
    //! event handlers.
    bool IsContextValid(event::ContextId);

    //! A debugging aid to check whether the supplied context id already exists.
    //! Adding an event handler must use this to check that the context id
    //! was previously set.
    bool ContextExists(event::ContextId);
#endif
    //! Set current context and event type.
    //! Usefull if there are multiple \c EventManager::Add calls.
    void Set(event::ContextId, event::Type);

    //! Add an event without an event handler.
    size_t Add(event::ContextId, event::Type);

    //! Add event handler.
    //!
    //! Example:
    //! ~~~~~~~~~~{.cpp}
    //! Add(1000, n::event::Type::DataReceive, EFUNC(&Class::OnReceive, this));
    //! ~~~~~~~~~~
    size_t Add(event::ContextId, event::Type, EventFunction f);

    //! Add event handler for the current context and type.
    size_t Add(EventFunction f);

    //! Remove event handlers.
    void Remove(event::ContextId);

    //! Remove event handlers.
    void Remove(event::ContextId, event::Type);

    //! Remove event handlers whose indices are in the container.
    virtual void Remove(EventPositions &positions) final;

    //! Remove all event handlers.
    void Clear();

    //! Trigger event handlers.
    void Call(event::ContextId, event::Type, EventData *data);
#if 0
    EventData* Call(int c, int t);
#endif

    //! Convenience function for passing event data to registered
    //! event handlers.
    void Pass(event::ContextId, EventData *);

    //! Return total event handlers.
    size_t Count();

    //! Return total event handlers registered with the specified context.
    size_t Count(event::ContextId);

    //! Return total event handlers registered with the specified context
    //! and event type.
    size_t Count(event::ContextId, event::Type);

    event::ContextId GetNextContextID();

    void Log();

protected:

    event::ContextId context;
    event::Type type;

    event::ContextId highestContext = 1;
    
    std::vector<Event*> events;
};

} //_ namespace negerns

#endif //_ NEGERNS_CORE_EVENT_H
