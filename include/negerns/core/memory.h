/*******************************************************************************
 * File:    negerns/core/memory.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 23 Jan 2014 11:33 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_MEMORY_H
#define NEGERNS_CORE_MEMORY_H

#include <memory>

namespace negerns {
#if 0
//! Wrapper around std::unique_ptr.
//! So the client code does not have to specify the template argument.
template<typename T>
std::unique_ptr<T> unique_ptr(T * ptr = nullptr)
{
    return std::unique_ptr<T>{ptr};
}

//! Wrapper around std::shared_ptr.
//! So the client code does not have to specify the template argument.
template<typename T>
std::shared_ptr<T> shared_ptr(T * ptr)
{
    return std::make_shared<T>{ptr};
}
#endif
//! Wrapper around std::make_unique.
//! So the client code does not have to specify the template argument.
template<typename T>
std::unique_ptr<T> make_unique(T * ptr = nullptr)
{
    return std::unique_ptr<T>{ptr};
}

//! Wrapper around std::make_shared.
//! So the client code does not have to specify the template argument.
template<typename T>
std::shared_ptr<T> make_shared(T * ptr)
{
    return std::make_shared<T>{ptr};
}

//! Deletes a pointer if it is not equal to \c nullptr and sets it to nullptr.
//!
//! It checks if the pointer is equal to a nullptr first which is divergent
//! from the following reasoning:
//! The C++ language guarantees that delete p will do nothing if p is equal
//! to NULL.
//! \link http://www.parashift.com/c++-faq/delete-handles-null.html
//!
//! Not checking for equality with nullptr, as advised above, makes the
//! assignment to nullptr redundant if the pointer is already equal to
//! nullptr.
template<typename T>
void reset(T *&ptr)
{
    if (ptr != nullptr) {
        delete ptr;
        ptr = nullptr;
    }
}

} //_ namespace negerns

#endif //_ NEGERNS_CORE_MEMORY_H
