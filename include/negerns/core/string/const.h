/*******************************************************************************
 * File:    negerns/core/string/const.h
 * Created:
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_STRING_CONST_H
#define NEGERNS_CORE_STRING_CONST_H

namespace negerns {
namespace string {

static const char eol = '\n';
static const char null_char = '\0';
static const char eos = '\0';
static const std::size_t null_size = 1;

} //_ namespace string
} //_ namespace negerns

#endif //_ NEGERNS_CORE_STRING_CONST_H
