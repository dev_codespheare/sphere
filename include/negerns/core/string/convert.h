/*******************************************************************************
 * File:    negerns/core/string/convert.h
 * Created: 28 Jul 2014 6:12 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_STRING_CONV_H
#define NEGERNS_CORE_STRING_CONV_H

#include <cstdint>
#include <ctime>
#include <string>
#include <utility>
#include <boost/logic/tribool.hpp>

#include <negerns/core/declspec.h>

namespace negerns {
namespace string {

//! Enumeration for string case conversion.
enum class Case {
    None,
    Lower,
    Upper
};

//! Returns the specified string converted to lower case.
NEGERNS_DECL
std::string to_lower(const std::string &);

//! Returns the specified string converted to lower case.
NEGERNS_DECL
std::string to_upper(const std::string &);

//! Returns the specified string converted to the specified Case.
NEGERNS_DECL
std::string to_case(const std::string &, Case);

NEGERNS_DECL bool stos(const std::string &, short &);
NEGERNS_DECL bool stous(const std::string &, unsigned short &);
NEGERNS_DECL bool stoi(const std::string &, int &);
NEGERNS_DECL bool stoui(const std::string &, unsigned int &);
NEGERNS_DECL bool stol(const std::string &, long &);
NEGERNS_DECL bool stoul(const std::string &, unsigned long &);
NEGERNS_DECL bool stoll(const std::string &, long long &);
NEGERNS_DECL bool stoull(const std::string &, unsigned long long &);

NEGERNS_DECL short              stos(const std::string &);
NEGERNS_DECL unsigned short     stous(const std::string &);
NEGERNS_DECL int                stoi(const std::string &);
NEGERNS_DECL unsigned int       stoui(const std::string &);
NEGERNS_DECL long               stol(const std::string &);
NEGERNS_DECL unsigned long      stoul(const std::string &);
NEGERNS_DECL long long          stoll(const std::string &);
NEGERNS_DECL unsigned long long stoull(const std::string &);

NEGERNS_DECL std::string to_string(bool);
NEGERNS_DECL std::string to_string(bool, std::pair<std::string, std::string>);

NEGERNS_DECL std::string to_string(int32_t);
NEGERNS_DECL std::string to_string(uint32_t);
NEGERNS_DECL std::string to_string(int64_t);
NEGERNS_DECL std::string to_string(uint64_t);
NEGERNS_DECL std::string to_string(float, int prec = 6);
NEGERNS_DECL std::string to_string(double, int prec = 6);
NEGERNS_DECL std::string to_string(long double, int prec = 6);

NEGERNS_DECL
std::string utos(
    uint64_t,
    boost::logic::tribool = boost::logic::indeterminate);
NEGERNS_DECL
std::string ftos(long double, uint8_t = 6);

NEGERNS_DECL
uint32_t count_digits(uint64_t);

//! Returns the hexadecimal string of the specified numeric value.
//!
//! Symbol:
//!   x - 0x47de
//!   X - 0X47DE
//!   # - #47DE
//!   h - 0h47DE
//!   \ - \x47DE
NEGERNS_DECL
std::string to_hex(uint64_t, char = 'X');

NEGERNS_DECL
std::string to_bin(uint64_t);

NEGERNS_DECL
std::string to_octal(uint64_t);

} //_ namespace string
} //_ namespace negerns



namespace negerns {
namespace string {
namespace boolean {

typedef std::pair<std::string, std::string> StringPair;

//! Returns '1' for true and '0' for false.
NEGERNS_DECL
StringPair no_bool_alpha();

//! Returns 'true' for true and 'false' for false.
//! Intended for fast conversion.
NEGERNS_DECL
StringPair bool_alpha_default();

//! Returns 'true' for true and 'false' for false using the specified Case.
NEGERNS_DECL
StringPair bool_alpha(Case = Case::Lower);

//! Returns 'yes' for true and 'no' for false using the specified Case.
NEGERNS_DECL
StringPair yes_no(Case = Case::Lower);

//! Returns 'success' for true and 'failed' for false using the specified Case.
NEGERNS_DECL
StringPair success_failed(Case = Case::Lower);

//! Returns the first string for true and the second string for false using the
//! specified Case.
NEGERNS_DECL
StringPair custom(const std::string &, const std::string &, Case = Case::Lower);

} //_ namespace boolean
} //_ namespace string
} //_ namespace negerns

#endif //_ NEGERNS_CORE_STRING_CONV_H
