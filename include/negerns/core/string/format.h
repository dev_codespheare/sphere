/*******************************************************************************
 * File:    negerns/core/string/format.h
 * Created: 28 Feb 2014 5:26 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_STRING_FORMAT_H
#define NEGERNS_CORE_STRING_FORMAT_H

#include <string>
#include <vector>
#include <negerns/core/var.h>

#include <negerns/core/declspec.h>

namespace negerns {
namespace string {
namespace format {

#if 0
template <typename T>
void expand_args_f(std::vector<Var> &vars, T &&arg)
{
    vars.emplace_back(std::forward<T>(arg));
}

template <typename T, typename... A>
void expand_args_f(std::vector<Var> &vars, T &&arg, A&&...a)
{
    vars.emplace_back(std::forward<T>(arg));
    expand_args_f(vars, std::forward<A>(a)...);
}

template <typename... A>
void expand_args_f(std::vector<Var> &vars, A&&...a)
{
    expand_args_f(vars, std::forward<A>(a)...);

    //? WARNING:
    //: Recursive on all paths, function will cause runtime stack overflow.
    //:
    //: If templates are deduced and expanded during compile time then why
    //: does VS2013U4 reports this?
}
#endif

NEGERNS_DECL
std::string formatx(const char *, const std::vector<Var> &);

} //_ namespace format
} //_ namespace string
} //_ namespace negerns



namespace negerns {
namespace string {

//! Format a string.
//!
//! Format:
//!
//!   %[argpos][alignment][sign][$width][.precision][conversion char]%
//!
//! argpos ::= integer
//!   positional argument index
//!
//! alignment ::= '<' | '>'
//!   '<' Align to left (default for non-numeric values)
//!   '>' Align to right (default for numeric values)
//!
//! sign ::= '-' | '+' | ' '
//!   '+' A positive sign for positive values and a negative sign
//!       for negative values
//!   '-' A negative sign for negative values only (default)
//!   '=' An equal sign means space for positive values and a minus sign
//!       for negative values.
//!
//! width ::= [$integer]
//!   '$' Separator for the argpos and the width when alignment and sign are
//!       not specified.
//!   Minimum number of characters to be printed. If the value to be printed
//!   is shorter than this number, the result is padded with blank spaces.
//!   The value is not truncated if it is larger.
//!
//! precision [.0-9]
//!   '.' Separator for the argpos and the precision when alignment, sign
//!       and width are not specified or when width is not specified.
//!   For integers and strings, this is ignored.
//!
//!   For floating point types, this is the maximum number of digits to be
//!   be represented after the decimal point (by default, this is 6). If the
//!   fractional part is longer than the specified precision then it is
//!   truncated and not rounded off.
//!
//!   If the period is specified without an explicit value for precision, the
//!   default value is assumed.
//!
//! conversion char ::= 'b' | 'x' | 'X' | 'o' | 't' | 'T' | 'y' | 'Y'
//!   'b' integer value in binary format
//!   'x' integer value in hexadecimal format (lowercase)
//!   'X' integer value in hexadecimal format (uppercase)
//!   'o' integer value in octal format (lowercase)
//!   't' boolean value as 'true/false' literal string (lowercase)
//!   'T' boolean value as 'TRUE/FALSE' literal string (uppercase)
//!   'y' boolean value as 'yes/no' literal string (lowercase)
//!   'Y' boolean value as 'YES/NO' literal string (uppercase)
//!
//! \link http://www.gkrueger.info/java/printf.html
//! \link http://www.pixelbeat.org/programming/gcc/format_specs.html
//!
//! \todo Add grouping symbol.
template <typename... A>
std::string Format(const char *fmtstr, A&&... a)
{
    return ::negerns::string::format::formatx(fmtstr, {a...});
}

template <typename... A>
std::string Format(const std::string &fmtstr, A&&... a)
{
    return ::negerns::string::format::formatx(fmtstr.c_str(), {a...});
}

} //_ namespace string
} //_ namespace negerns

#endif //_ NEGERNS_CORE_STRING_FORMAT_H
