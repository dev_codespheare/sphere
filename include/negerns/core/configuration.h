/*******************************************************************************
 * File:    negerns/core/configuration.h
 * Created: 30 Aug 2014 5:07 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef NEGERNS_CORE_CONFIGURATION_H
#define NEGERNS_CORE_CONFIGURATION_H

#include <list>
#include <negerns/core/config/base.h>

namespace negerns {

// Priorities:
// Configurations are scanned starting from the configuration that have the
// highest priority.
//
// Append higher priorities at the end of the list.
// If the configuration being added has a lower priority then it is inserted
// before the higher priority.
// If the configuration being added has an equal priority then it is inserted
// after the last equal priority. This allows keys to be 'overriden'
class NEGERNS_DECL Configuration : public config::IConfig
{
public:
    Configuration();
    virtual ~Configuration();

    //! \copydoc IConfig::has_key
    virtual bool has_key(const std::string &key) override;

    //! \copydoc IConfig::get_bool
    virtual bool get_bool(const std::string &key, const bool def) override;

    //! \copydoc IConfig::get_string
    virtual std::string get_string(const std::string &key, const std::string &def) override;

    //! \copydoc IConfig::get_int
	virtual int get_int(const std::string &key, const int def) override;

    //! \copydoc IConfig::get_uint
	virtual unsigned int get_uint(const std::string &key, const unsigned int def) override;

    //! \copydoc IConfig::get_double
	virtual double get_double(const std::string &key, const double def) override;

    //! \copydoc IConfig::set
    virtual bool set(const std::string &key, bool value) override;

    //! \copydoc IConfig::set
    virtual bool set(const std::string &key, const std::string& value) override;

    //! \copydoc IConfig::set
	virtual bool set(const std::string &key, int value) override;

    //! \copydoc IConfig::set
	virtual bool set(const std::string &key, unsigned int value) override;

    //! \copydoc IConfig::set
	virtual bool set(const std::string &key, double value) override;

    //! \copydoc IConfig::read
    virtual void read() override;

    //! \copydoc IConfig::save
    virtual void save() override;

    virtual void read(const std::string &filename) final;

    virtual void save(const std::string &filename) final;

    void add(config::Base *conf, unsigned int priority = 0);

private:

    negerns::config::ConfigItem find(const std::string &key);

private:

    struct ConfigurationInput
	{
        ConfigurationInput(const std::string &fn, config::Base *ptr,
            unsigned int p) :
            filename(fn), ptr(ptr), priority(p)
        { }
        std::string filename;
		config::Base *ptr;
		unsigned int priority;
	};

    std::list<ConfigurationInput> configs;
};

} //_ namespace negerns

#endif //_ NEGERNS_CORE_CONFIGURATION_H
