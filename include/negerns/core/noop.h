/*******************************************************************************
 * File:    negerns/core/noop.h
 * Created: 21 Mar 2015 11:44 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment: No operation macro
 ******************************************************************************/

#ifdef _MSC_VER
#define NOOP(...)   __noop(__VA_ARGS__)
#else
#define NOOP(x)     ((void)(0))
#endif
