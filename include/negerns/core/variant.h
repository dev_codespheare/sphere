#ifndef NEGERNS_CORE_VARIANT_H
#define NEGERNS_CORE_VARIANT_H

#include <Poco/Dynamic/Var.h>

namespace negerns {
    typedef ::Poco::Dynamic::Var Variant;
    typedef std::vector<::Poco::Dynamic::Var> Variants;
} //_ namespace negerns

#endif //_ NEGERNS_CORE_VARIANT_H
