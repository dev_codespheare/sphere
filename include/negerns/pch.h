/*******************************************************************************
 * File:    negerns/pch.h
 * Created: 23 Apr 2014 9:09 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment: Precompiled Header
 ******************************************************************************/

#ifndef NEGERNS_PCH_H
#define NEGERNS_PCH_H

#pragma message("Compiling negerns precompiled headers.\n")

#include <cctype>
#include <cstdint>
#include <vector>
#include <string>
#include <memory>
#include <map>
#include <list>
#include <functional>
#include <algorithm>
#include <fstream>

#ifdef _MSC_VER
#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>
#endif

#include <boost/any.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <boost/spirit/include/qi_parse.hpp>
#include <boost/spirit/include/qi_numeric.hpp>
#include <boost/filesystem.hpp>

#include <wx/wx.h>
#include <wx/aui/aui.h>
#if 0
#include <Poco/DateTime.h>
#include <Poco/Timestamp.h>
#include <Poco/Dynamic/Var.h>
#include <Poco/DateTimeFormat.h>
#include <Poco/DateTimeParser.h>
#include <Poco/DateTimeFormatter.h>
#include <Poco/PatternFormatter.h>
#include <Poco/Format.h>
#include <Poco/FormattingChannel.h>
#include <Poco/AsyncChannel.h>
#include <Poco/Path.h>
#include <Poco/Logger.h>
#include <Poco/FileChannel.h>
#include <Poco/Stopwatch.h>
#include <Poco/Exception.h>

#include <Poco/Data/MetaColumn.h>
#include <Poco/Data/SessionPool.h>
#include <Poco/Data/Row.h>
#include <Poco/Data/RecordSet.h>
#include <Poco/Data/Statement.h>
#include <Poco/Data/ODBC/Connector.h>
#endif
#endif //_ NEGERNS_PCH_H