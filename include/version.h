/*****************************************************************************
 * File:    version.h
 * Created: 25 Jun 2013 3:08 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Purpose: System version information
 *****************************************************************************/

#ifndef SPHERE_VERSION_H
#define SPHERE_VERSION_H

#include <boost/preprocessor/stringize.hpp>

//! Version information

#define VER_SPHERE_NAME                "Sphere"
// I'm thinking about doing a Personal/Enterprise Edition
#define VER_SPHERE_EDITION             ""
#define VER_SPHERE_NAME_STR VER_SPHERE_NAME " " VER_SPHERE_EDITION "\0"

// NOTE:
// The following definitions are used in the generation of the documentation

#define VER_SPHERE_AUTHOR              "Ricky Maicle"
#define VER_SPHERE_DATE                "April 2013"
#define VER_SPHERE_DATE_YEAR           "2013"

#define VER_SPHERE_VERSION_MAJOR       0
#define VER_SPHERE_VERSION_MINOR       1
#define VER_SPHERE_VERSION_REVISION    0
#define VER_SPHERE_VERSION_INTERNAL    0

// This string should be present only if VS_FF_PRIVATEBUILD is specified
// in the fileflags parameter of the root block.
#ifdef _DEBUG
#define VER_SPHERE_PRIVATEBUILD_STR "Development build\0"
#else
#define VER_SPHERE_PRIVATEBUILD_STR ""
#endif

#define VER_SPHERE_SPECIALBUILD_STR "Alpha"

#define VER_SPHERE_VERSION VER_SPHERE_VERSION_MAJOR,VER_SPHERE_VERSION_MINOR,VER_SPHERE_VERSION_REVISION,VER_SPHERE_VERSION_INTERNAL

#define VER_SPHERE_VERSION_STR \
    BOOST_PP_STRINGIZE(VER_SPHERE_VERSION_MAJOR) "." \
    BOOST_PP_STRINGIZE(VER_SPHERE_VERSION_MINOR) "." \
    BOOST_PP_STRINGIZE(VER_SPHERE_VERSION_REVISION) "." \
    BOOST_PP_STRINGIZE(VER_SPHERE_VERSION_INTERNAL)

#define VER_COMPANY_NAME_STR "\0"

#define VER_SPHERE_WEBSITE             "http://www.maicle.com/sphere"

#define VER_LEGAL_COPYRIGHT_STR "Copyright " VER_SPHERE_DATE_YEAR " " VER_SPHERE_AUTHOR "\nAll rights reserved.\0"
#define VER_LEGAL_TRADEMARKS1_STR "\0"
#define VER_LEGAL_TRADEMARKS2_STR "\0"

#endif // SPHERE_VERSION_H
