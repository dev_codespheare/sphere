/*****************************************************************************
 * File:    app/ais/version.h
 * Created: 20 Jun 2013 5:24 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Purpose: AIS version constants
 *****************************************************************************/

// NOTE: VS2008 spits out an error when using a header guard.

#include <boost/preprocessor/stringize.hpp>

#define VER_NAME_SHORT          "ais"
#define VER_NAME_LONG           "Sphere Accounting Information System"
#define VER_EDITION             ""
//#define VER_EDITION             "Cooperative Edition"

#define VER_VERSION_MAJOR       0
#define VER_VERSION_MINOR       1
#define VER_VERSION_REVISION    0
#define VER_VERSION_INTERNAL    0

// TODO: Rename the project and application name

#define VER_INTERNALNAME_STR "ais\0"
#define VER_ORIGINALFILENAME_STR "ais.exe\0"

#define VER_FILEDESCRIPTION_STR VER_NAME_LONG "\0"

#define VER_FILETYPE VFT_APP

// NOTE:
// The following macro definitions are derivatives of the above definitions.
// Editing of the following macro definitions are for internal use only.
// -------------------------------------------------------------------------

#ifdef _DEBUG
#define VER_FILEVERSION VER_VERSION_MAJOR,VER_VERSION_MINOR,VER_VERSION_REVISION,VER_VERSION_INTERNAL
#else
#define VER_FILEVERSION VER_VERSION_MAJOR,VER_VERSION_MINOR,VER_VERSION_REVISION
#endif

#ifdef _DEBUG
#define VER_VERSION_STR \
    BOOST_PP_STRINGIZE(VER_VERSION_MAJOR) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_MINOR) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_REVISION) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_INTERNAL)
#else
#define VER_VERSION_STR \
    BOOST_PP_STRINGIZE(VER_VERSION_MAJOR) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_MINOR) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_REVISION)
#endif

// TODO: Is this really necessary?
#define VER_APPVERSION_STRING VER_NAME_SHORT " " VER_VERSION_STR

#define VER_FILEVERSION_STR VER_VERSION_STR "\0"

#ifdef _DEBUG
#define VER_FILEFLAGS VS_FF_DEBUG | VS_FF_PRERELEASE | VS_FF_PRIVATEBUILD | VS_FF_SPECIALBUILD
#else
#define VER_FILEFLAGS VS_FF_PRIVATEBUILD | VS_FF_SPECIALBUILD
#endif



// A few helpful macro for version checking

//! \brief Check if the specified version is at least major.minor.release
//!
//! Logic from wxWidgets wxCHECK_VERSION (version.h)
#define AIS_CHECK_VERSION(major,minor,revision) \
    (VER_VERSION_MAJOR > (major) || \
    (VER_VERSION_MAJOR == (major) && VER_VERSION_MINOR > (minor)) || \
    (VER_VERSION_MAJOR == (major) && VER_VERSION_MINOR == (minor) && VER_VERSION_REVISION >= (revision)))
