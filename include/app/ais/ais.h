/*******************************************************************************
 * File:    app/ais/ais.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 13 Feb 2013 4:41 AM
 *
 * Comment: AIS application entry point
 ******************************************************************************/

#ifndef APP_AIS_AIS_H
#define APP_AIS_AIS_H

#include <sphere/cmn/app.h>

namespace sphere {
namespace ais {

class Ais : public sphere::common::Application
{
public:

    Ais();

    //! Called by the framework to initialize the application.
    //!
    //! Specific initialization routine. Must return true if the application
    //! is to proceed. Otherwise, returns false to terminate.
    //!
    //! \code{.cpp}
    //! // Allow command line processing provided by wxWidgets
    //! if (!Application::OnInit()) {
    //!     return false;
    //! }
    //! // Application specific initialization routine...
    //! return true;
    //! \endcode
    virtual bool OnInit(void) override;

    //! Called by the framework on application termination.
    virtual int OnExit(void) override;

    //! Called by the framework when a fatal exception occurs.
    virtual void OnFatalException();

protected:

    //! Called by the framework to initialize application configuration.
    virtual void InitConfiguration() override;

private:

    enum Priority {
        Configuration = 5,
        Trace,
    };

    static const char* configFile;
#ifdef _DEBUG
    static const char* traceFile;
#endif
};

DECLARE_APP(Ais)

} // namespace ais
} // namespace sphere

#endif // APP_AIS_AIS_H
