/*******************************************************************************
 * File:    sphere/cmn/app.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 30 Jul 2013 4:52 PM
 *
 * Comment: Core application
 ******************************************************************************/

#ifndef SPHERE_COMMON_APP_H
#define SPHERE_COMMON_APP_H

#include <negerns/negerns.h>
#include <negerns/gui/app.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {

class Mediator;

class SPHERE_COMMON_DECL Application : public n::gui::Application
{
public:
    Application();

    //! \brief This is called by wxWidgets to initialize the application.
    //!
    //! \details %Application specific initialization routine is executed here
    //! and must return true if the application is to proceed. Otherwise,
    //! return false to terminate.
    //!
    //! \code{.cpp}
    //! // Allow command line processing provided by wxWidgets
    //! if (!Application::OnInit()) {
    //!     return false;
    //! }
    //! // Application specific initialization routine...
    //! return true;
    //! \endcode
    virtual bool OnInit(void) override;

    //! \copydoc Application::OnExit
    virtual int OnExit(void) override;

    //! \copydoc Application::OnFatalException
    virtual void OnFatalException();

    //! \copydoc Application::OnInitCmdLine
    virtual void OnInitCmdLine(wxCmdLineParser&);

    //! \copydoc Application::OnAbout
    virtual void OnAbout(wxCommandEvent&);

    //! \brief Set the MediatorCore-derived object instance
    void SetMediator(Mediator* m) { mediator = m; }

protected:

    //! \copydoc Application::InitConfiguration
    virtual void InitConfiguration() override;

protected:

    Mediator* mediator;

private:

    enum Priority {
        Configuration = 3,
        Trace,
    };

    static const char* configFile;
#ifdef _DEBUG
    static const char* traceFile;
#endif
};

} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_APP_H
