/*******************************************************************************
 * File:    sphere/cmn/toolbar/toolbar.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 30 Jul 2013 4:35 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef SPHERE_COMMON_TOOLBAR_TOOLBAR_H
#define SPHERE_COMMON_TOOLBAR_TOOLBAR_H

#include <negerns/negerns.h>
#include <negerns/gui/bar/toolbar.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {
namespace toolbar {

class SPHERE_COMMON_DECL ToolBar : public negerns::gui::bar::ToolBar
{
public:
    ToolBar();
    ~ToolBar();

    //! @{
    //! \name IControl Virtual Functions

    //! \copydoc ToolBarBase::CreateControl
    virtual void CreateControl();

    //! @}
};

} // namespace toolbar
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_TOOLBAR_TOOLBAR_H
