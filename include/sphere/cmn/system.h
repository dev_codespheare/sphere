/*******************************************************************************
 * File:    sphere/cmn/system.h
 * Created: 13 May 2014 2:01 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_SYSTEM_H
#define SPHERE_COMMON_SYSTEM_H

#include <negerns/core/system.h>
#include <negerns/gui/component.h>
#include <sphere/cmn/pane/navigation.h>
#include <sphere/cmn/pane/main.h>
#include <sphere/cmn/pane/results.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {

class SPHERE_COMMON_DECL System : public negerns::System
{
public:

    class SPHERE_COMMON_DECL Pane {
    public:
        static std::size_t Navigation;
        static std::size_t Results;
        static std::size_t Main;

        //! Add a page to the specified Pane object.
        static void AddPage(std::size_t, n::gui::Component *,
            bool activate = true);

        static void SetPage(std::size_t, std::size_t);

        //! Return a reference to the Navigation Pane.
        static sphere::common::pane::Navigation* GetNavigation();

        //! Return a reference to the Main Pane.
        static sphere::common::pane::Main* GetMain();

        //! Return a reference to the Results Pane
        static sphere::common::pane::Results* GetResults();
    };
};

} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_SYSTEM_H
