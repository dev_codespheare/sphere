/*******************************************************************************
 * File:    sphere/cmn/datetime.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 22 Jun 2014 8:58 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef SPHERE_COMMON_DATETIME_H
#define SPHERE_COMMON_DATETIME_H

#include <vector>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {

class SPHERE_COMMON_DECL DateTime
{
public:
    DateTime();
    virtual ~DateTime();

    //! Return the current year.
    static std::size_t DateTime::GetYear();

    //! Return the month names.
    static std::vector<std::string> GetMonthNames();
};

} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_DATETIME_H
