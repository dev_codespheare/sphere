/*******************************************************************************
 * File:    sphere/cmn/artprovider.h
 * Created: 30 Jul 2013 4:46 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef SPHERE_COMMON_ARTPROVIDER_H
#define	SPHERE_COMMON_ARTPROVIDER_H

#include <wx/artprov.h>
#include <wx/bitmap.h>
#include <wx/icon.h>
#include <negerns/negerns.h>
#include <negerns/gui/shellfileinfo.h>
#include <sphere/cmn/declspec.h>

#define ART_ICON_APP                wxART_MAKE_ART_ID(ART_ICON_APP)
#define ART_ICON_APP_BIG            wxART_MAKE_ART_ID(ART_ICON_APP_BIG)
#define ART_ICON_FOLDER             wxART_MAKE_ART_ID(ART_ICON_FOLDER)

#define ART_ICON_NEW                wxART_MAKE_ART_ID(ART_ICON_NEW)
#define ART_ICON_EDIT               wxART_MAKE_ART_ID(ART_ICON_EDIT)
#define ART_ICON_REFRESH            wxART_MAKE_ART_ID(ART_ICON_REFRESH)

namespace sphere {
namespace common {

//! \brief Centralized art provider for look and feel customization.
class SPHERE_COMMON_DECL ArtProvider : public wxArtProvider
{
public:
    ArtProvider();
    ~ArtProvider();
protected:
    virtual wxBitmap CreateBitmap(const wxArtID& id, const wxArtClient& client, const wxSize& size);
private:
#ifdef __WXMSW__
    n::gui::ShellFileInfo fileInfo;
#endif
};

} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_ARTPROVIDER_H
