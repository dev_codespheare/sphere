/*******************************************************************************
 * File:    sphere/cmn/framemanager.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 30 Jul 2013 4:45 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef SPHERE_COMMON_FRAMEMANAGER_H
#define SPHERE_COMMON_FRAMEMANAGER_H

#include <negerns/negerns.h>
#include <negerns/gui/framemanager.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {

class SPHERE_COMMON_DECL FrameManager : public negerns::gui::FrameManager
{
public:
    FrameManager();
    virtual ~FrameManager();

    //! @{
    //! \name IControl Virtual Methods

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    virtual void DestroyControl() override;

    //! \copydoc IControl::ApplyConfiguration
    virtual void ApplyConfiguration() override;

    //! @}

    virtual void OnAbout(wxCommandEvent&) override;

    virtual void OnViewHelp(wxCommandEvent&) { }

    void OnToggleNavigationPane(wxCommandEvent &);
    void OnToggleResultsPane(wxCommandEvent &);
};

} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_FRAMEMANAGER_H
