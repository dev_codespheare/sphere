/*****************************************************************************
 * File:    sphere/cmn/constants.h
 * Created: 13 Feb 2013 6:47 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Purpose: Core Constants
 *****************************************************************************/

#ifndef SPHERE_COMMON_CONSTANTS_H
#define SPHERE_COMMON_CONSTANTS_H

#define CONST_RESOURCE_FILE_COMMON  "cmnres.bin"

#define CONST_DATABASE_MAIN         "Main"
#define CONST_DATABASE_CACHE        "Cache"

namespace sphere {
namespace common {

//! \brief Menu item identifier constants
class Menu
{
public:
    enum {
        ID_START = wxID_HIGHEST + 1,
        ID_TOGGLE_FULLSCREEN,
        // Can be used as the reference to start another group constant
        // like the wxWidgets identifier constant wxID_HIGHEST
        ID_MAX
    };
};

} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_CONSTANTS_H
