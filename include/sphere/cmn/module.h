/*******************************************************************************
 * File:    sphere/cmn/module.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 11 Jun 2014 6:04 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_MODULE_H
#define SPHERE_COMMON_MODULE_H

#include <vector>
#include <map>
#include <string>
#include <wx/xrc/xmlres.h>
#include <negerns/negerns.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {

class ModuleNode;

typedef std::vector<ModuleNode *> Modules;

class ModuleNode
{
public:
    ModuleNode(ModuleNode *parent,
        unsigned int id,
        const std::string &title) :
        parentNode(parent), id(id), title(title), enabled(true) { }

    virtual ~ModuleNode() {
        std::size_t count = children.size();
        for (std::size_t i = 0; i < count; i++) {
            ModuleNode *child = children[i];
            wxDELETE(child);
        }
    }

    bool IsRoot() const { return parentNode == nullptr; }
    bool IsContainer() const { return children.size() > 0; }
    bool IsEnabled() const { return enabled; }
    void Enable() { enabled = true; }
    void Disable() { enabled = false; }

    ModuleNode* GetParent() { return parentNode; }
    Modules& Get() { return children; }
    //ModuleNode* GetNthChild(unsigned int n) { return children.Item( n ); }
    //void Insert(ModuleNode *child, unsigned int n) { children.Insert( child, n); }
    void Add(ModuleNode *child) { children.push_back(child); }
    std::size_t GetCount() const { return children.size(); }

public:

    unsigned int id;
    std::string title;
    bool enabled;

private:

    ModuleNode *parentNode;
    Modules children;
};



class SPHERE_COMMON_DECL Module
{
public:

    enum {
        IdStart = 1000
    };

    struct HumanResource {
        enum {
            Group = IdStart,
            Employee,
            HolidayCalendar,
            WorkCalendar,
            DailyTimeRecord,
            Last
        };
    };

    struct Accounting {
        enum {
            Group = HumanResource::Last,
            ChartOfAccounts,
            SubsidiaryAccounts,
            GeneralJournal,
            SpecialJournals,
            GeneralLedger,
            SubsidiaryLedgers,
            Payroll,
            Last
        };
    };

    struct System {
        enum {
            Group = Accounting::Last,
            Configuration,
            Last
        };
    };

    enum {
        IdEnd = System::Last
    };

    //! Constructor.
    //!
    //! Populate the module item c\ status and flag each as enabled (true).
    //! It then calls \c ReadExcludedNodes.
    //!
    //! \see ReadExcludedNodes
    Module();
    ~Module();

    //! Initialize the module item strings for display.
    //! Returns the root node of the tree which is then used as the data
    //! model for the contents of the \c Modules component.
    ModuleNode * InitContent();

    //! Returns the specified module item state.
    //!
    //! \see status
    bool IsEnabled(std::size_t);

private:

    //! Build the tree nodes.
    //! All disabled module items are excluded from the tree. This is called
    //! by \c InitContent.
    //!
    //! \see ReadExcludedNodes
    void BuildNodes(ModuleNode *parent,
        ModuleNode *current,
        const std::vector<std::string> &items,
        const std::size_t id_start);

    //! Read module items from resource file to be disabled.
    //! The module items are defined in system.xrc with the following sample
    //! contents:
    //!
    //! \code
    //! <?xml version="1.0" encoding="utf-8"?>
    //! <resource xmlns="http://www.wxwidgets.org/wxxrc" version="2.5.3.0">
    //!   <object name="module_count">6</object>
    //!   <object name="module_1">1006</object>
    //!   <object name="module_2">1007</object>
    //!   <object name="module_3">1008</object>
    //!   <object name="module_4">1009</object>
    //!   <object name="module_5">1010</object>
    //!   <object name="module_6">1011</object>
    //! </resource>
    //! \endcode
    //!
    //! The module item identifiers found in the file are flagged as disabled
    //! (false) in the module item \c status member variable. Note that the
    //! module item c\ status has been previously populated and each flagged
    //! by default as enabled (true).
    //!
    //! \internal Must be called before \c InitContent.
    void ReadExcludedNodes();

    //! Set the state of the specified module item to false.
    //!
    //! \see status
    void Disable(std::size_t);

private:

    //! Enabled/disabled state of each module item.
    //! True for enabled and false for disabled.
    std::map<std::size_t, bool> status;

    wxXmlResource *resource = nullptr;
};

} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_MODULE_H
