/*******************************************************************************
 * File:    sphere/cmn/pane/main.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 1 Nov 2013 3:36 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_PANE_MAIN_H
#define SPHERE_COMMON_PANE_MAIN_H

#include <sphere/cmn/pane.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {
namespace pane {

class SPHERE_COMMON_DECL Main : public sphere::common::Pane
{
public:
    Main();
    virtual ~Main();

    //! @{
    //! \name IControl Virtual Methods

    //! \copydoc IControl::ApplyConfiguration
    virtual void ApplyConfiguration() override;

    //! @}
};

} // namespace pane
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_PANE_MAIN_H
