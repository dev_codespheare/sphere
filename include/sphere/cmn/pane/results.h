/*******************************************************************************
 * File:    sphere/cmn/pane/results.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 21 Apr 2013 7:41 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_PANE_RESULTS_H
#define SPHERE_COMMON_PANE_RESULTS_H

#include <sphere/cmn/pane.h>
#include <sphere/cmn/declspec.h>
#include <sphere/cmn/component/output.h>

namespace sphere {
namespace common {
namespace pane {

class SPHERE_COMMON_DECL Results : public sphere::common::Pane
{
public:
    Results();
    virtual ~Results();

    //! @{
    //! \name IControl Virtual Functions

    //! \copydoc IControl::ApplyConfiguration
    virtual void ApplyConfiguration() override;

    //! @}
};

} // namespace pane
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_PANE_RESULTS_H
