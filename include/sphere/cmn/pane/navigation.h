/*******************************************************************************
 * File:    sphere/cmn/pane/navigation.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 1 Nov 2013 3:36 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_PANE_NAVIGATION_H
#define SPHERE_COMMON_PANE_NAVIGATION_H

#include <sphere/cmn/component/modules.h>
#include <sphere/cmn/pane.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {
namespace pane {

class SPHERE_COMMON_DECL Navigation : public sphere::common::Pane
{
public:
    Navigation();
    virtual ~Navigation();

    //! @{
    //! \name IControl Virtual Methods

    virtual void CreateControl() override;

    virtual void PostCreateControl() override;

    virtual void DestroyControl() override;

    //! \copydoc IControl::ApplyConfiguration
    virtual void ApplyConfiguration() override;

    //! @}

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    //! @}

protected:

    sphere::common::component::Modules *modules;
};

} // namespace pane
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_PANE_NAVIGATION_H
