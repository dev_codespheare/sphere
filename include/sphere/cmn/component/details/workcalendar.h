/*******************************************************************************
 * File:    sphere/cmn/component/details/workcalendar.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 18 Jun 2014 4:08 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_COMPONENT_DETAILS_WORKCALENDAR_H
#define SPHERE_COMMON_COMPONENT_DETAILS_WORKCALENDAR_H

#include <negerns/gui/data/input/import.h>
#include <negerns/gui/sizer/flexgrid.h>
#include <negerns/gui/listview.h>
#include <negerns/gui/renderer/choice.h>

#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {
namespace component {
namespace details {

class SPHERE_COMMON_DECL WorkCalendar : public n::gui::data::input::Import
{
public:
    WorkCalendar();
    virtual ~WorkCalendar();

    //! @{
    //! \name IControl Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    //! @}

    //! \copydoc DataEntry::SetIdentifier
    void SetIdentifier(const std::string &);

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    virtual void PostInitContent() override;

    //! @}

    //! @{
    //! \name ICRUD Virtual Functions

    virtual bool OnCreate(wxCommandEvent &) override;

    virtual bool OnRead(wxCommandEvent &) override;
    virtual bool OnRefresh(wxCommandEvent &) override;

    virtual bool PreUpdate(wxCommandEvent &) override;
    virtual bool OnUpdate(wxCommandEvent &) override;
    virtual bool PostUpdate(wxCommandEvent &) override;

    //! @}

private:

    //! Custom row display.
    bool OnRowDisplay(std::size_t, std::size_t, wxVariant &);

    void OnSelectionActivated(wxDataViewEvent &);
    
#ifdef _DEBUG
    virtual void SetTestValues() override;
#endif
    
private:

    n::gui::sizer::FlexGrid *flexgrid = nullptr;

    //! Stores the user input
    //n::data::DataStore *listDataStore = nullptr;

    //! User input copied here
    //n::data::DataStore *inputDataStore = nullptr;

    class Input {
    public:
        n::gui::SpinCtrl *year = nullptr;
        //n::gui::DatePickerCtrl *date = nullptr;
        //n::gui::ChoiceCtrl *type = nullptr;
        n::gui::ListView *list = nullptr;

        wxDataViewColumn *monthColumn = nullptr;
    } input;
};

} // namespace details
} // namespace component
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_COMPONENT_DETAILS_WORKCALENDAR_H
