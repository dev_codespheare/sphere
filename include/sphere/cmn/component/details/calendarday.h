/*******************************************************************************
 * File:    sphere/cmn/component/details/calendarday.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 04 Jun 2014 4:38 PM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_COMPONENT_DETAILS_CALENDARDAY_H
#define SPHERE_COMMON_COMPONENT_DETAILS_CALENDARDAY_H

#include <negerns/gui/sizer/flexgrid.h>
#include <negerns/gui/data/input/single.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {
namespace component {
namespace details {

class SPHERE_COMMON_DECL CalendarDay : public n::gui::data::input::Single
{
public:
    CalendarDay();
    virtual ~CalendarDay();

    //! @{
    //! \name IControl Virtual Functions

    virtual void CreateControl() override;

    //virtual void InitContent() override;

    //! @}

    void SetIdentifier(const std::string &);

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    virtual void PostInitContent() override;

    //! @}

private:

    n::gui::sizer::FlexGrid *flexgrid = nullptr;

    n::gui::DatePickerCtrl *dateCtrl = nullptr;
    n::gui::ChoiceCtrl *typeCtrl = nullptr;
};

} // namespace details
} // namespace component
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_COMPONENT_DETAILS_CALENDARDAY_H
