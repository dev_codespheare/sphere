/*******************************************************************************
 * File:    sphere/cmn/component/details/employee.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 04 Oct 2013 4:41 PM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_COMPONENT_DETAILS_EMPLOYEE_H
#define SPHERE_COMMON_COMPONENT_DETAILS_EMPLOYEE_H

#include <negerns/gui/sizer/flexgrid.h>
#include <negerns/gui/data/input/single.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {
namespace component {
namespace details {

class SPHERE_COMMON_DECL Employee : public n::gui::data::input::Single
{
public:
    Employee();
    virtual ~Employee();

    //! @{
    //! \name IControl Public Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    //! @}

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    virtual void PostInitContent() override;

    //! @}

#ifdef _DEBUG
    //! @{
    //! \name ICRUD Virtual Functions

    virtual bool OnRead(wxCommandEvent &) override;
    virtual bool OnRefresh(wxCommandEvent &) override;
    virtual bool OnUpdate(wxCommandEvent &) override;

    //! @}
#endif

private:

    n::gui::sizer::FlexGrid *flexgrid = nullptr;

    class Input {
    public:
#ifdef _DEBUG
        n::gui::SpinCtrl *code = nullptr;
#endif
        n::gui::TextCtrl *firstname = nullptr;
        n::gui::TextCtrl *middlename = nullptr;
        n::gui::TextCtrl *lastname = nullptr;
        n::gui::ChoiceCtrl *sex = nullptr;
        n::gui::DatePickerCtrl *dob = nullptr;
        n::gui::TextCtrl *pob = nullptr;
        n::gui::ChoiceCtrl *civil_status = nullptr;

        n::gui::TextCtrl *driver_license = nullptr;
        n::gui::TextCtrl *sss_id = nullptr;
        n::gui::TextCtrl *pagibig_id = nullptr;
        n::gui::TextCtrl *philhealth_id = nullptr;
        n::gui::TextCtrl *tax_id = nullptr;
        n::gui::TextCtrl *passport_id = nullptr;

        n::gui::DatePickerCtrl *employment_date = nullptr;
        n::gui::TextCtrl *employee_number = nullptr;
        n::gui::ChoiceCtrl *status = nullptr;

    } input;
};

} // namespace details
} // namespace component
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_COMPONENT_DETAILS_EMPLOYEE_H
