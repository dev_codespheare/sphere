/*******************************************************************************
 * File:    sphere/cmn/component/list/employee.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 16 May 2014 3:35 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_COMPONENT_LIST_EMPLOYEE_H
#define SPHERE_COMMON_COMPONENT_LIST_EMPLOYEE_H

#include <negerns/negerns.h>
#include <negerns/gui/data/list.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {
namespace component {
namespace list {

class SPHERE_COMMON_DECL Employee : public n::gui::data::List
{
public:
    Employee();
    virtual ~Employee();

    //! @{
    //! \name IControl Public Virtual Functions

    virtual void CreateControl() override;

    virtual void DestroyControl() override;
    
    virtual void ApplyConfiguration() override;

    //! @}

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    //! @}

    //! @{
    //! \name ICRUD Virtual Functions

    virtual bool OnCreate(wxCommandEvent &) override;
    virtual void OnEdit(wxCommandEvent &) override;

    //! @}

    //! Customize list display.
    //!
    //! \see ListViewComponent::OnDisplayRow
    virtual bool OnRowDisplay(std::size_t, std::size_t, wxVariant &) override;
};

} // namespace list
} // namespace component
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_COMPONENT_LIST_EMPLOYEE_H
