/*******************************************************************************
 * File:    sphere/cmn/component/list/holidaycalendar.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 18 Jun 2014 2:44 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_COMPONENT_LIST_HOLIDAYCALENDAR_H
#define SPHERE_COMMON_COMPONENT_LIST_HOLIDAYCALENDAR_H

#include <negerns/gui/data/list.h>
#include <sphere/cmn/declspec.h>

#include <negerns/negerns.h>

namespace sphere {
namespace common {
namespace component {
namespace list {

class SPHERE_COMMON_DECL HolidayCalendar : public n::gui::data::List
{
public:
    HolidayCalendar();
    virtual ~HolidayCalendar();

    //! @{
    //! \name IControl Public Virtual Functions

    virtual void CreateControl() override;

    virtual void DestroyControl() override;
    
    virtual void ApplyConfiguration() override;

    //! @}

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    //! @}

    //! @{
    //! \name ICRUD Virtual Functions
    //! Implementation of ICRUD functions

    virtual bool OnCreate(wxCommandEvent &) override;
    virtual void OnEdit(wxCommandEvent &) override;

    //! @}

    //! Customize list display.
    //!
    //! \see ListViewComponent::OnDisplayRow
    virtual bool OnRowDisplay(std::size_t, std::size_t, wxVariant &) override;
};

} // namespace list
} // namespace component
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_COMPONENT_LIST_HOLIDAYCALENDAR_H
