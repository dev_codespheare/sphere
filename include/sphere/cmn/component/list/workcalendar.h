/*******************************************************************************
 * File:    sphere/cmn/component/list/workcalendar.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 4 Jun 2014 3:03 PM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_COMPONENT_LIST_WORKCALENDAR_H
#define SPHERE_COMMON_COMPONENT_LIST_WORKCALENDAR_H

#include <negerns/negerns.h>
#include <negerns/gui/data/list.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {
namespace component {
namespace list {

class SPHERE_COMMON_DECL WorkCalendar : public n::gui::data::List
{
public:
    WorkCalendar();
    virtual ~WorkCalendar();

    //! @{
    //! \name IControl Public Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    virtual void DestroyControl() override;
    
    virtual void ApplyConfiguration() override;

    //! @}

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    //! @}

    //! @{
    //! \name ICRUD Virtual Functions
    //! Implementation of ICRUD functions

    virtual bool OnCreate(wxCommandEvent &) override;
    virtual void OnEdit(wxCommandEvent &) override;

    //! @}

    //! Customize list display.
    //!
    //! \see ListViewComponent::OnDisplayRow
    virtual bool OnRowDisplay(std::size_t, std::size_t, wxVariant &) override;
};

} // namespace list
} // namespace component
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_COMPONENT_LIST_WORKCALENDAR_H
