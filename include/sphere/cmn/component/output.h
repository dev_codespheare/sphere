/*******************************************************************************
 * File:    sphere/cmn/component/output.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 03 Oct 2013 11:20 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_COMPONENT_OUTPUT_H
#define SPHERE_COMMON_COMPONENT_OUTPUT_H

#include <sphere/cmn/component.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {
namespace component {

class SPHERE_COMMON_DECL Output : public sphere::common::Component
{
public:
    Output();
    virtual ~Output();

    //! @{
    //! \name IControl Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    //! @}

    void OnClearContents(wxCommandEvent &);
};

} // namespace component
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_COMPONENT_OUTPUT_H
