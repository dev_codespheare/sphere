/*******************************************************************************
 * File:    sphere/cmn/component/home.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 14 May 2014 4:21 PM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_COMPONENT_HOME_H
#define SPHERE_COMMON_COMPONENT_HOME_H

#include <negerns/negerns.h>
#include <sphere/cmn/component.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {
namespace component {

class SPHERE_COMMON_DECL Home : public sphere::common::Component
{
public:
    Home();
    virtual ~Home();

    //! @{
    //! \name IControl Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    //! @}
};

} // namespace component
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_COMPONENT_HOME_H
