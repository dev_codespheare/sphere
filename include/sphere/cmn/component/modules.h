/*******************************************************************************
 * File:    sphere/cmn/component/modules.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 20 May 2014 4:14 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_COMPONENT_MODULES_H
#define SPHERE_COMMON_COMPONENT_MODULES_H

#include <wx/dataview.h>
#include <sphere/cmn/module.h>
#include <sphere/cmn/component.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {
namespace component {

class SPHERE_COMMON_DECL ModuleModel : public wxDataViewModel
{
public:

    ModuleModel();
    virtual ~ModuleModel();

    //! Returns the ID of the specified item.
    static unsigned int GetID(const wxDataViewItem &item);

    //! Returns the titem of the specified item.
    static std::string GetTitle(const wxDataViewItem &item);

    //! @{
    //! \name wxDataViewModel Virtual Functions
    //! Implementation of base class virtuals to define model

    //! Sort branches ascendingly.
    virtual int Compare(const wxDataViewItem &item1,
        const wxDataViewItem &item2,
        unsigned int column,
        bool ascending) const override;

    //! Return the number of columns.
    virtual unsigned int GetColumnCount() const override { return 1; }

    //! Return the data type of the specified column as a string.
    //! The built-in types are:
    //!     "bool"
    //!     "char"
    //!     "datetime"
    //!     "double"
    //!     "list"
    //!     "long"
    //!     "longlong"
    //!     "string"
    //!     "ulonglong"
    //!     "arrstring"
    //!     "void*"
    //!
    //! \internal The data type is as reported by \c wxVariant.
    virtual wxString GetColumnType(unsigned int col) const override {
        return wxT("string");
    }

    //! Get the specified item column value as \c wxVariant.
    virtual void GetValue(wxVariant &variant,
        const wxDataViewItem &item,
        unsigned int col) const override;

    //! Set the specified item column value.
    virtual bool SetValue(const wxVariant &variant,
        const wxDataViewItem &item,
        unsigned int col) override;

    //! Return the parent item of the specified item. If the root item is the
    //! parent item then it returns an invalid item. An invalid item is an item
    //! wherein IsOk() returns false.
    virtual wxDataViewItem GetParent(const wxDataViewItem &item) const override;

    //! Returns true if the specified item has child items.
    virtual bool IsContainer(const wxDataViewItem &item) const override;

    //! Get the child items of the specified item. Returns the number of child
    //! items.
    //!
    //! If specified item is root then it returns 1 and the child items
    //! contain the root item. If the specified item does not have child items,
    //! then it returns 0 and the child items are empty.
    virtual unsigned int GetChildren(const wxDataViewItem &parent,
        wxDataViewItemArray &array) const override;

    virtual bool IsEnabled(const wxDataViewItem &item,
        unsigned int col) const override;

    //! @}

    //! Returns the root item.
    wxDataViewItem GetRoot() const { return wxDataViewItem(root); }

    //! Return the \c Module instance.
    Module * GetModule() const { return module; }

protected:

    //! Populate the model.
    void Populate();

    //! Called by \c Populate before any routine in \c Populate is processed.
    //! This allows subclasses to enable/disable model data items.
    //!
    //! \code
    //! GetModule()->Disable(Module::Accounting::ChartOfAccounts);
    //! ...
    //! \endcode
    virtual void PrePopulate();

private:

    Module *module = nullptr;

    ModuleNode *root = nullptr;

    // pointers to some "special" nodes of the tree:
    ModuleNode *system = nullptr;
    ModuleNode *humanResource = nullptr;
    ModuleNode *accounting = nullptr;
#if 0
    ModuleNode *sales;
    ModuleNode *purchasing;
    ModuleNode *inventory;
    ModuleNode *fixedAssetsTangible;
    ModuleNode *fixedAssetsIntangible;
#endif
    
};



class SPHERE_COMMON_DECL Modules : public sphere::common::Component
{
public:
    Modules();
    virtual ~Modules();

    //! @{
    //! \name IControl Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    //! \copydoc IControl::RefreshContent
    virtual void RefreshContent() override;

    //! @}

private:

    void ExpandItems(wxCommandEvent &);
    void CollapseItems(wxCommandEvent &);

    void OnSize(wxSizeEvent &);

    void OnMouse(wxMouseEvent &);
    void OnSelectionExpanded(wxDataViewEvent &);
    void OnSelectionCollapsed(wxDataViewEvent &);
    void OnSelectionChanged(wxDataViewEvent &);
    void OnSelectionActivated(wxDataViewEvent &);

private:

    wxDataViewCtrl* control;

    wxObjectDataPtr<ModuleModel> model;

    wxDataViewColumn *column;
};

} // namespace component
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_COMPONENT_MODULES_H
