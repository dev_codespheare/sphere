/*******************************************************************************
 * File:    sphere/cmn/pane.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 13 May 2014 3:51 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_PANE_H
#define SPHERE_COMMON_PANE_H

#include <negerns/negerns.h>
#include <negerns/gui/pane.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {

class SPHERE_COMMON_DECL Pane : public n::gui::Pane
{
public:
    Pane();
    virtual ~Pane();
};

} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_PANE_H
