/*******************************************************************************
 * File:    sphere/cmn/declspec.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 16 Jul 2013 6:56 AM
 *
 * Comment: Import/Export macro definition for the Sphere Common library.
 ******************************************************************************/

#ifndef SPHERE_COMMON_DECLSPEC_H
#define SPHERE_COMMON_DECLSPEC_H

// Define the storage class specifier extern when using an exported class
// templates in an executable or another dynamic library module. In the
// dynamic library where the class template is defined the storage class
// specifier must be null (empty).
// Ref: http://support.microsoft.com/kb/168958

#ifdef _MSC_VER
#   ifdef _WINDLL
#       ifdef SPHERE_COMMON_DLL
#           define SPHERE_COMMON_DECL __declspec(dllexport)
#           define SPHERE_COMMON_EXPIMP_TEMPLATE
#       else
#           define SPHERE_COMMON_DECL __declspec(dllimport)
#           define SPHERE_COMMON_EXPIMP_TEMPLATE extern
#       endif
#   else
#       define SPHERE_COMMON_DECL
#       define SPHERE_COMMON_EXPIMP_TEMPLATE
#   endif
#else
#   define SPHERE_COMMON_DECL     // Define empty value for linux OS
#   define SPHERE_COMMON_EXPIMP_TEMPLATE
#endif

#if 0
#ifndef SPHERE_COMMON_DECL
#define SPHERE_COMMON_DECL
#endif

#ifndef SPHERE_COMMON_EXPIMP_TEMPLATE
#define SPHERE_COMMON_EXPIMP_TEMPLATE
#endif
#endif

//
// Automatically library linking
//
#ifdef _MSC_VER
	#ifdef _WINDLL
		#pragma comment(lib, "sphere_da.lib")
	#endif
#endif

#endif // SPHERE_COMMON_DECLSPEC_H
