/*******************************************************************************
 * File:    sphere/cmn/component.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 03 Nov 2013 18:56
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_COMMON_COMPONENT_H
#define SPHERE_COMMON_COMPONENT_H

#include <negerns/negerns.h>
#include <negerns/gui/component.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {

class SPHERE_COMMON_DECL Component : public n::gui::Component
{
public:
    Component();
    ~Component();
};

} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_COMPONENT_H
