/*******************************************************************************
 * File:    sphere/cmn/mediator.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 30 Jul 2013 4:35 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef SPHERE_COMMON_MEDIATOR_H
#define SPHERE_COMMON_MEDIATOR_H

#include <negerns/negerns.h>
#include <negerns/gui/mediator.h>
#include <sphere/cmn/framemanager.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {

class SPHERE_COMMON_DECL Mediator : public n::gui::Mediator
{
public:
    Mediator(FrameManager* fm);
    virtual ~Mediator();

    //! @{
    //! \name IControl Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    //! @}

#ifdef _DEBUG
    virtual bool AutoLogIn() override;
#endif

protected:

    virtual void OnLogIn(wxCommandEvent&) override;

    virtual void ProcessToolBarClick(wxCommandEvent &) override;

    void OnSavePage(wxCommandEvent &);

    void OnClosePage(wxCommandEvent &);

    void OnCloseAllPages(wxCommandEvent &);

    void OnRefreshPage(wxCommandEvent &);

    virtual std::string GetDataSourceName() final;

};

} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_MEDIATOR_H
