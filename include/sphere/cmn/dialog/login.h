/*******************************************************************************
 * File:    sphere/cmn/dialog/login.h
 * Created: 17 Apr 2013 12:46 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef SPHERE_COMMON_DIALOG_LOGIN_H
#define SPHERE_COMMON_DIALOG_LOGIN_H

#include <negerns/negerns.h>
#include <negerns/gui/dialog/logindialogbase.h>
#include <sphere/cmn/declspec.h>

class wxTextCtrl;

namespace sphere {
namespace common {
namespace dialog {

//! Log In window for the default local database.
class SPHERE_COMMON_DECL LogIn : public n::gui::LogInDialogBase
{
public:
    LogIn(wxWindow* parent,
        wxWindowID id,
        const wxString& title,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxDEFAULT_DIALOG_STYLE);
    virtual ~LogIn();

    // ------------------------------------------------------------------------
    //! @{
    //! \name IControl Virtual Methods

    //! \copydoc IControl::CreateControl
    virtual void CreateControl();

    //! @}
};

} // namespace dialog
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_DIALOG_LOGIN_H
