/*******************************************************************************
 * File:    sphere/cmn/dialog/dialog.h
 * Created: 17 Apr 2013 12:46 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef SPHERE_COMMON_DIALOG_DIALOG_H
#define SPHERE_COMMON_DIALOG_DIALOG_H

#include <negerns/negerns.h>
#include <negerns/gui/dialog/dialogbase.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {
namespace dialog {

//! Log In window for the default local database.
class SPHERE_COMMON_DECL Dialog : public n::gui::DialogBase
{
public:
    Dialog();
    Dialog(wxWindow* parent,
        wxWindowID id,
        const wxString& title,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxDEFAULT_DIALOG_STYLE | wxWS_EX_VALIDATE_RECURSIVELY);
    virtual ~Dialog();
};

} // namespace dialog
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_DIALOG_DIALOG_H
