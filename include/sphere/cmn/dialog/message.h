/*******************************************************************************
 * File:    sphere/cmn/dialog/message.h
 * Created: 14 Aug 2013 3:55 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef SPHERE_COMMON_DIALOG_MESAGE_H
#define SPHERE_COMMON_DIALOG_MESAGE_H

#include <wx/listctrl.h>
#include <negerns/negerns.h>
#include <negerns/gui/dialog/dialogbase.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {
namespace dialog {

//! Message dialog.
class SPHERE_COMMON_DECL Message : public n::gui::DialogBase
{
public:
    Message();
    virtual ~Message();

    // ------------------------------------------------------------------------
    //! @{
    //! \name IControl Virtual Methods

    //! \copydoc IControl::CreateControl
    virtual void CreateControl();

    //! @}

    void OnKey(wxListEvent&);
};

} // namespace dialog
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_DIALOG_MESAGE_H
