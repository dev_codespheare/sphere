/*******************************************************************************
 * File:    sphere/cmn/about.h
 * Created: 13 Aug 2013 9:30 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef SPHERE_COMMON_DIALOG_ABOUT_H
#define SPHERE_COMMON_DIALOG_ABOUT_H

#include <wx/listctrl.h>
#include <negerns/negerns.h>
#include <sphere/cmn/dialog/dialog.h>
#include <sphere/cmn/declspec.h>

namespace sphere {
namespace common {
namespace dialog {

//! About dialog.
class SPHERE_COMMON_DECL About : public Dialog
{
public:
    About();
    virtual ~About();

    // ------------------------------------------------------------------------
    //! @{
    //! \name IControl Virtual Methods

    //! \copydoc IControl::CreateControl
    virtual void CreateControl();

    //! @}

    void OnKey(wxListEvent&);
};

} // namespace dialog
} // namespace common
} // namespace sphere

#endif // SPHERE_COMMON_DIALOG_ABOUT_H
