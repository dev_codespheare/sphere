/*******************************************************************************
 * File:    sphere/data/transactions.h
 * Created: 15 Oct 2013 2:47 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef SPHERE_DATA_TRANSACTIONS_H
#define SPHERE_DATA_TRANSACTIONS_H

#include <negerns/data/transaction.h>
#include <sphere/data/declspec.h>
#include <negerns/negerns.h>

namespace negerns {
namespace data {
namespace transaction {

    //! Transaction groups
    enum Group : unsigned int
    {
        NoGroup = 0,

        TestStatus,
        TestTest,
        TestOther,
        TestBlank,

        CivilStatusSet,
        BirStatusSet,
        EmployeeSet,
        Employee,
        Max
    };

} // namespace transaction
} // namespace data
} // namespace negerns


namespace sphere {
namespace data {

class SPHERE_DATA_DECL Transactions
{
public:

    Transactions();

    virtual ~Transactions();

    void Log();
};

} // namespace data
} // namespace sphere

#endif // SPHERE_DATA_TRANSACTIONS_H
