/*******************************************************************************
 * File:    sphere/data/declspec.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 14 Mar 2013 8:50 AM
 *
 * Comment: Import/Export macro definition for the Sphere Data Access library.
 ******************************************************************************/

#ifndef SPHERE_DATA_DECLSPEC_H
#define SPHERE_DATA_DECLSPEC_H

// This include avoids an error in wxThread::Yield declaration
//#include <wx/wx.h>

// NOTE:
// Define the storage class specifier extern when using an exported class
// templates in an executable or another dynamic library module. In the
// dynamic library where the class template is defined the storage class
// specifier must be null (empty).
// Ref: http://support.microsoft.com/kb/168958

#ifdef _MSC_VER
#   ifdef _WINDLL
#       ifdef SPHERE_DATA_DLL
#           define SPHERE_DATA_DECL __declspec(dllexport)
#           define ERP_SPHERE_DATA_EXPIMP_TEMPLATE
#       else
#           define SPHERE_DATA_DECL __declspec(dllimport)
#           define ERP_SPHERE_DATA_EXPIMP_TEMPLATE extern
#       endif
#   else
#       define SPHERE_DATA_DECL
#       define ERP_SPHERE_DATA_EXPIMP_TEMPLATE
#   endif
#else
    // Define empty value for linux OS
#   define SPHERE_DATA_DECL
#   define ERP_SPHERE_DATA_EXPIMP_TEMPLATE
#endif

#if 0
#ifndef SPHERE_DATA_DECL
#define SPHERE_DATA_DECL
#endif

#ifndef SPHERE_DATA_EXPIMP_TEMPLATE
#define SPHERE_DATA_EXPIMP_TEMPLATE
#endif
#endif

#endif // SPHERE_DATA_DECLSPEC_H
