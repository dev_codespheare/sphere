/*******************************************************************************
 * File:    sphere/ais/mediator.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 14 Feb 2013 11:10 AM
 *
 * Comment:
 ******************************************************************************/

#ifndef SPHERE_AIS_MEDIATOR_H
#define SPHERE_AIS_MEDIATOR_H

#include <sphere/cmn/mediator.h>
#include <sphere/ais/declspec.h>

namespace sphere {
namespace ais {

class SPHERE_AIS_DECL Mediator : public common::Mediator
{
public:
    Mediator();
    virtual ~Mediator();

    //! @{
    //! \name IControl Virtual Functions

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    //! @}
};

} // namespace ais
} // namespace sphere

#endif // SPHERE_AIS_MEDIATOR_H
