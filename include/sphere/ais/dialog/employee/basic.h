/*******************************************************************************
 * File:    sphere/ais/dialog/employee/basic.h
 * Created: 17 Apr 2013 12:46 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef SPHERE_AIS_DIALOG_EMPLOYEE_BASIC_H
#define SPHERE_AIS_DIALOG_EMPLOYEE_BASIC_H

#include <memory>
#include <negerns/negerns.h>
#include <negerns/core/event.h>
#include <negerns/core/icrud.h>
#include <negerns/gui/sizer/flexgrid.h>
#include <sphere/cmn/component.h>
#include <sphere/ais/declspec.h>

class wxTextCtrl;
class wxDatePickerCtrl;
class wxChoice;

namespace sphere {
namespace ais {
namespace dialog {
namespace employee {

//! Employee dialog
class SPHERE_AIS_DECL Basic :
    public sphere::common::Component,
    public negerns::ICrud
{
public:
    Basic();
    virtual ~Basic();

    //! @{
    //! \name IControl Virtual Methods

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    virtual void DestroyControl() override;

    //! @}

    void OnReceive(negerns::EventData *);

    virtual bool OnCreate(wxCommandEvent &) override;
    virtual bool OnRead(wxCommandEvent &) override;
    virtual bool OnRefresh(wxCommandEvent &) override;
    virtual bool OnUpdate(wxCommandEvent &) override;

    bool ChangesPending();

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    //! @}

protected:

    n::gui::sizer::FlexGrid *flexgrid = nullptr;

    n::gui::TextCtrl *codeCtrl = nullptr;
    n::gui::TextCtrl *firstnameCtrl = nullptr;
    n::gui::TextCtrl *middlenameCtrl = nullptr;
    n::gui::TextCtrl *lastnameCtrl = nullptr;
    n::gui::ChoiceCtrl *sexCtrl = nullptr;
    n::gui::DatePickerCtrl *dobCtrl = nullptr;
    n::gui::TextCtrl *pobCtrl = nullptr;
    n::gui::ChoiceCtrl *civilStatusCtrl = nullptr;

    //std::unique_ptr<n::data::Row> item;

    negerns::EventPositions events;
};

} // namespace employee
} // namespace dialog
} // namespace ais
} // namespace sphere

#endif // SPHERE_AIS_DIALOG_EMPLOYEE_BASIC_H
