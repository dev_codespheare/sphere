/*******************************************************************************
 * File:    sphere/ais/dialog/employee/employee.h
 * Created: 17 Apr 2013 12:46 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment:
 ******************************************************************************/

#ifndef SPHERE_AIS_DIALOG_EMPLOYEE_EMPLOYEE_H
#define SPHERE_AIS_DIALOG_EMPLOYEE_EMPLOYEE_H

#include <negerns/negerns.h>
#include <negerns/core/event.h>
#include <negerns/core/icrud.h>
#include <negerns/gui/dialog/propertydialog.h>
#include <sphere/cmn/dialog/dialog.h>
#include <sphere/ais/declspec.h>
#include <sphere/ais/dialog/employee/basic.h>

class wxTextCtrl;
class wxDatePickerCtrl;
class wxChoice;

namespace sphere {
namespace ais {
namespace dialog {
namespace employee {

class SPHERE_AIS_DECL Employee :
    public n::gui::PropertyDialog,
    public negerns::ICrud
{
public:
    Employee();
    virtual ~Employee();

    //! @{
    //! \name IControl Virtual Methods

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    virtual void DestroyControl() override;
    //! @}

    virtual bool OnCreate(wxCommandEvent &) override;
    virtual bool OnRead(wxCommandEvent &) override;
    virtual bool OnRefresh(wxCommandEvent &) override;
    virtual bool OnUpdate(wxCommandEvent &) override;

    virtual void OnCancelClicked(wxCommandEvent &) override;

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    //! @}

protected:

    Basic* basic;
};

} // namespace employee
} // namespace dialog
} // namespace ais
} // namespace sphere

#endif // SPHERE_AIS_DIALOG_EMPLOYEE_EMPLOYEE_H
