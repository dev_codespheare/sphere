/*******************************************************************************
 * File:    sphere/ais/framemanager.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 15 Feb 2013 3:38 AM
 *
 * Comment:
 ******************************************************************************/

#ifndef SPHERE_AIS_FRAMEMANAGER_H
#define SPHERE_AIS_FRAMEMANAGER_H

#include <sphere/ais/declspec.h>
#include <sphere/cmn/framemanager.h>

namespace sphere {
namespace ais {

class SPHERE_AIS_DECL FrameManager : public common::FrameManager
{
public:
    FrameManager();
    virtual ~FrameManager();

    //! @{
    //! \name IControl Virtual Methods

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    virtual void ApplyConfiguration();

    //! @}

};

} // namespace ais
} // namespace sphere

#endif // SPHERE_AIS_FRAMEMANAGER_H
