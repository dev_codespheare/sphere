/*******************************************************************************
 * File:    sphere/ais/pane/navigation.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 20 Apr 2013 1:40 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_AIS_PANE_NAVIGATION_H
#define SPHERE_AIS_PANE_NAVIGATION_H

#include <negerns/negerns.h>
#include <sphere/cmn/pane/navigation.h>
#include <sphere/ais/declspec.h>

namespace sphere {
namespace ais {
namespace pane {

class SPHERE_AIS_DECL Navigation : public sphere::common::pane::Navigation
{
public:
    Navigation();
    virtual ~Navigation();

    //! @{
    //! \name IControl Virtual Methods

    virtual void PostCreateControl() override;

    //! @}
};

} // namespace pane
} // namespace ais
} // namespace sphere

#endif // SPHERE_AIS_PANE_NAVIGATION_H
