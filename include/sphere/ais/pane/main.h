/*******************************************************************************
 * File:    sphere/ais/pane/main.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 21 Apr 2013 7:46 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_AIS_PANE_MAIN_H
#define SPHERE_AIS_PANE_MAIN_H

#include <negerns/negerns.h>
#include <sphere/cmn/pane/main.h>
#include <sphere/cmn/component/home.h>
#include <sphere/ais/declspec.h>

namespace sphere {
namespace ais {
namespace pane {

class SPHERE_AIS_DECL Main : public sphere::common::pane::Main
{
public:
    Main();
    virtual ~Main();

    //! @{
    //! \name IControl Virtual Methods

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    virtual void DestroyControl() override;

    //! \copydoc IControl::ApplyConfiguration
    virtual void ApplyConfiguration() override;

    //! @}

protected:

    //! @{
    //! \name IControl Protected Virtual Functions

    virtual bool OnInitContent() override;

    //! @}

private:

    sphere::common::component::Home *home;
};

} // namespace pane
} // namespace ais
} // namespace sphere

#endif // SPHERE_AIS_PANE_MAIN_H
