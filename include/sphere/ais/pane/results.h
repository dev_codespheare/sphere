/*******************************************************************************
 * File:    sphere/ais/pane/results.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 21 Apr 2013 7:41 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef SPHERE_AIS_PANE_RESULTS_H
#define SPHERE_AIS_PANE_RESULTS_H

#include <negerns/negerns.h>
#include <sphere/cmn/pane/results.h>
#include <sphere/ais/declspec.h>

namespace sphere {
namespace ais {
namespace pane {

class SPHERE_AIS_DECL Results : public sphere::common::pane::Results
{
public:
    Results();
    virtual ~Results();

    //! @{
    //! \name IControl Virtual Methods

    //! \copydoc IControl::CreateControl
    virtual void CreateControl() override;

    virtual void DestroyControl() override;

    virtual void ApplyConfiguration() override;

    //! @}
};

} // namespace pane
} // namespace ais
} // namespace sphere

#endif // SPHERE_AIS_PANE_RESULTS_H
