/*******************************************************************************
 * File:    sphere/ais/declspec.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 10 Apr 2013 6:33 AM
 *
 * Comment: Import/Export macro definition for the Sphere Accounting library.
 ******************************************************************************/

#ifndef SPHERE_AIS_DECLSPEC_H
#define SPHERE_AIS_DECLSPEC_H

// NOTE:
// Define the storage class specifier extern when using an exported class
// templates in an executable or another dynamic library module. In the
// dynamic library where the class template is defined the storage class
// specifier must be null (empty).
// Ref: http://support.microsoft.com/kb/168958

#ifdef _MSC_VER
#   ifdef _WINDLL
#       ifdef SPHERE_AIS_DLL
#           define SPHERE_AIS_DECL __declspec(dllexport)
#           define SPHERE_AIS_EXPIMP_TEMPLATE
#       else
#           define SPHERE_AIS_DECL __declspec(dllimport)
#           define SPHERE_AIS_EXPIMP_TEMPLATE extern
#       endif
#   else
#       define SPHERE_AIS_DECL
#       define SPHERE_AIS_EXPIMP_TEMPLATE
#   endif
#else
#   define SPHERE_AIS_DECL     // Define empty value for linux OS
#   define SPHERE_AIS_EXPIMP_TEMPLATE
#endif

#if 0
#ifndef SPHERE_AIS_DECL
#define SPHERE_AIS_DECL
#endif

#ifndef SPHERE_AIS_EXPIMP_TEMPLATE
#define SPHERE_AIS_EXPIMP_TEMPLATE
#endif
#endif

//
// Automatically library linking
//
#ifdef _MSC_VER
	#ifdef _WINDLL
		#pragma comment(lib, "sphere_cmn.lib")
		#pragma comment(lib, "sphere_da.lib")
	#endif
#endif

#endif // SPHERE_AIS_DECLSPEC_H
