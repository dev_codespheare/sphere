/*******************************************************************************
 * File:    sphere/ais/transparent.h
 * Created: 15 Mar 2013 7:16 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment: Transparent background for static text, checkbox and radio button.
 *          The code is taken from http://trac.wxwidgets.org/ticket/14492.
 *          The 'enhancement' has been set for wxWidgets 3.0.
 ******************************************************************************/

#ifndef SPHERE_AIS_TRANSPARENT_H
#define SPHERE_AIS_TRANSPARENT_H

#include <wx/stattext.h>
#include <wx/checkbox.h>
#include <wx/radiobut.h>
#include <wx/bitmap.h>

//! Transparent background static text
//! @see http://www.romwnet.org/dasblogce/PermaLink,guid,2dd767f8-1952-45b6-b1d8-aaac89702e2e.aspx
class TransparentStaticText : public wxStaticText
{
    DECLARE_DYNAMIC_CLASS(TransparentStaticText)
public:
    TransparentStaticText();
    TransparentStaticText(
        wxWindow* parent,
        wxWindowID id,
        const wxString& label,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = 0,
        const wxString& name= wxStaticTextNameStr
    );

    bool Create(
        wxWindow* parent,
        wxWindowID id,
        const wxString& label,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = 0,
        const wxString& name= wxStaticTextNameStr
    );

    virtual bool HasTransparentBackground() { return true; };

    virtual void OnPaint(wxPaintEvent& event);

    DECLARE_EVENT_TABLE()
};

#endif // SPHERE_AIS_TRANSPARENT_H
