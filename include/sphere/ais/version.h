/*****************************************************************************
 * File:    sphere/ais/version.h
 * Created: 20 Jun 2013 4:36 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Purpose: AIS Core version constants
 *****************************************************************************/

// NOTE: VS2008 spits out an error when using a header guard.

#include <boost/preprocessor/stringize.hpp>

#define VER_VERSION_MAJOR       0
#define VER_VERSION_MINOR       1
#define VER_VERSION_REVISION    0
#define VER_VERSION_INTERNAL    0

#define VER_INTERNALNAME_STR "ais\0"
#define VER_ORIGINALFILENAME_STR "aiscore.dll\0"
#define VER_FILEDESCRIPTION_STR "AIS Core library\0"

#ifdef _WINDLL
#define VER_FILETYPE VFT_DLL
#else
#define VER_FILETYPE VFT_STATIC_LIB
#endif

#ifdef _DEBUG
#define VER_FILEFLAGS VS_FF_DEBUG | VS_FF_PRERELEASE
#else
#define VER_FILEFLAGS 0x0L
#endif

// NOTE:
// The following macro definitions are derivatives of the above definitions.
// Editing of the following macro definitions are for internal use only.
// -------------------------------------------------------------------------

#ifdef _DEBUG
#define VER_FILEVERSION VER_VERSION_MAJOR,VER_VERSION_MINOR,VER_VERSION_REVISION,VER_VERSION_INTERNAL
#else
#define VER_FILEVERSION VER_VERSION_MAJOR,VER_VERSION_MINOR,VER_VERSION_REVISION
#endif

#ifdef _DEBUG
#define VER_VERSION_STR \
    BOOST_PP_STRINGIZE(VER_VERSION_MAJOR) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_MINOR) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_REVISION) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_INTERNAL)
#else
#define VER_VERSION_STR \
    BOOST_PP_STRINGIZE(VER_VERSION_MAJOR) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_MINOR) "." \
    BOOST_PP_STRINGIZE(VER_VERSION_REVISION)
#endif

#define VER_FILEVERSION_STR VER_VERSION_STR "\0"

// A few helpful macro for version checking
// ----------------------------------------
//! \brief Check if the specified version is at least major.minor.release
//!
//! Logic from wxWidgets wxCHECK_VERSION (version.h)
#define SPHERE_AIS_CHECK_VERSION(major,minor,revision) \
    (VER_VERSION_MAJOR > (major) || \
    (VER_VERSION_MAJOR == (major) && VER_VERSION_MINOR > (minor)) || \
    (VER_VERSION_MAJOR == (major) && VER_VERSION_MINOR == (minor) && VER_VERSION_REVISION >= (revision)))
