/*****************************************************************************
 * File:    sphere/ais/constants.h
 * Created: 13 Feb 2013 6:47 AM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Purpose: AIS Constants
 *****************************************************************************/

#include <wx/defs.h>

#ifndef SPHERE_AIS_CONSTANTS_H
#define SPHERE_AIS_CONSTANTS_H

#define CONST_RESOURCE_XRC              "aisxrc"
#define CONST_RESOURCE_IMAGE            "aisimg"

#define CONST_RESOURCE_XRC_FILE_AIS     "../res/aisxrc.bin"
#define CONST_RESOURCE_IMAGE_FILE_AIS   "../res/aisimg.bin"

#define CONST_RESOURCE_SYSTEM           "system.xrc"
#define CONST_RESOURCE_ICON             "icon.xrc"
#define CONST_RESOURCE_MENU             "menu.xrc"
#define CONST_RESOURCE_MESSAGE          "message.xrc"
#define CONST_RESOURCE_LOGIN            "login.xrc"
#define CONST_RESOURCE_ABOUT            "about.xrc"
#define CONST_RESOURCE_PANEL_EMPLOYEE_DETAILS            "employeedetails.xrc"

#define CONST_CONFIG_AIS                "Config AIS"

#if 0
#define CONST_COMPONENT_PAYROLL         "payroll_component"
#define CONST_COMPONENT_DTR             "dtr_component"
#define CONST_COMPONENT_EMPLOYEE_LIST   "employee_list_component"
#define CONST_COMPONENT_EMPLOYEE_DETAIL "employee_details_component"
#endif

namespace sphere {
namespace ais {

class MenuID {
public:
    enum {
        employee_new = wxID_HIGHEST + 1,
        employee_edit,
        employee_refresh
    };
};

} // namespace ais
} // namespace sphere

#endif // SPHERE_AIS_CONSTANTS_H
