/*******************************************************************************
 * File:    core/ais/component/output.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 03 Oct 2013 11:20 AM
 *
 * Comment: 
 ******************************************************************************/

#ifndef AISCORE_OUTPUTCOMPONENT_H
#define AISCORE_OUTPUTCOMPONENT_H

#include <negerns/negerns.h>
#include <negerns/gui/outputctrl.h>
#include <negerns/gui/panecomponentbase.h>
#include <core/ais/internal.h>

namespace sphere {
namespace ais {

class n::gui::OutputCtrl;

class ERP_AISCORE_DECL OutputComponent : public n::gui::PaneComponentBase
{
public:
    OutputComponent();
    virtual ~OutputComponent();

    // ------------------------------------------------------------------------
    //! @{
    //! \name IControl Virtual Methods

    //! \copydoc IControl::CreateControl
    virtual void CreateControl();
    //! \copydoc IControl::RefreshControl
    virtual void RefreshControl() { }
    //! \copydoc IControl::ClearContent
    virtual void ClearContent() { }
    //! \copydoc IControl::RefreshContent
    virtual void RefreshContent() { }
    //! \copydoc IControl::ApplyConfiguration
    virtual void ApplyConfiguration() { }
    //! \copydoc IControl::ApplyTheme
    virtual void ApplyTheme() { }
    //! \copydoc IControl::HasFocus
    virtual bool HasFocus(void) const;
    //! \copydoc IControl::SetFocus
    virtual bool SetFocus(const wxString&);
    //! @}

protected:
    n::gui::OutputCtrl* control;
};

} // namespace ais
} // namespace sphere

#endif // AISCORE_OUTPUTCOMPONENT_H
