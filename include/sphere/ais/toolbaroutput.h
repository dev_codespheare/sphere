/*******************************************************************************
 * File:    core/ais/toolbaroutput.h
 * Author:  rmaicle (rmaicle@gmail.com)
 * Created: 03 Oct 2013 5:42 PM
 *
 * Comment:
 ******************************************************************************/

#ifndef AISCORE_TOOLBAROUTPUT_H
#define AISCORE_TOOLBAROUTPUT_H

#include <negerns/negerns.h>
#include <negerns/gui/bar/toolbarbase.h>
#include <core/ais/internal.h>

namespace sphere {
namespace ais {

class ERP_AISCORE_DECL ToolBarOutput : public n::gui::ToolBarBase
{
public:
    ToolBarOutput();
    ~ToolBarOutput();

    // ------------------------------------------------------------------------
    //! @{
    //! \name IControl Virtual Methods

    //! \copydoc ToolBarBase::CreateControl
    virtual void CreateControl();
    //! \copydoc ToolBarBase::RefreshControl
    //virtual void RefreshControl() { }
    //! \copydoc ToolBarBase::ClearContent
    //virtual void ClearContent() { }
    //! \copydoc ToolBarBase::RefreshContent
    //virtual void RefreshContent() { }
    //! \copydoc ToolBarBase::ApplyConfiguration
    //virtual void ApplyConfiguration() { }
    //! \copydoc ToolBarBase::ApplyTheme
    //virtual void ApplyTheme() { }
    //! \copydoc IControl::HasFocus
    virtual bool HasFocus(void) const { return HasFocus(); }
    //! \copydoc IControl::SetFocus
    virtual bool SetFocus(const wxString& s) { return SetFocus(s); }
    //! @}
};

} // namespace ais
} // namespace sphere

#endif // AISCORE_TOOLBAROUTPUT_H
