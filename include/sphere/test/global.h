#include <boost/test/unit_test.hpp>

#include <negerns/data/dataaccess.h>
#include <negerns/data/datasource.h>
#include <negerns/negerns.h>

using namespace boost::unit_test;

struct negerns_data {
    negerns_data();
    ~negerns_data();

    std::unique_ptr<negerns::data::DataAccess> da;
    static int is_init;
};

BOOST_GLOBAL_FIXTURE(negerns_data);
