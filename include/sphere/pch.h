/*******************************************************************************
 * File:    sphere/pch.h
 * Created: 23 Apr 2014 6:11 PM
 * Author:  rmaicle <rmaicle@gmail.com>
 *
 * Comment: Precompiled Header
 ******************************************************************************/

#ifndef SPHERE_PCH_H
#define SPHERE_PCH_H

#pragma message("Compiling sphere precompiled headers.\n")

#include <vector>
#include <string>
#include <memory>

#include <wx/wx.h>
#include <wx/aui/aui.h>

#endif // SPHERE_PCH_H
