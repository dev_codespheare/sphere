#ifndef NEGERNS_X_CORE_VAR_H
#define NEGERNS_X_CORE_VAR_H

#include <string>
#include <map>
#include <vector>
#include <cstdint>
#include <boost/logic/tribool.hpp>
#include <negerns/core/types.h>
#include <negerns/core/datetime.h>
#include <negerns/core/decimal.h>
// Do not include string.h to avoid cyclical dependency
#include <negerns/core/string/const.h>
#include <negerns/core/string/convert.h>

#include <negerns/negerns.h>

using namespace negerns;

namespace negernsx {
namespace variant {

enum class Type : int {
    Empty = 0,
    Char,
    SChar,
    UChar,
    WChar,
    Bool,
    Short,
    UShort,
    Integer,
    UInteger,
    Long,
    ULong,
    LLong,
    ULLong,
    Float,
    Double,
    ShortString,
    LDouble,
    Fixed2,
    String,
    Tm,
    Unknown
};

static std::map<Type, boost::logic::tribool> IsTypeSigned {
    { Type::Empty,       boost::logic::indeterminate },
    { Type::Char,        boost::logic::indeterminate },
    { Type::SChar,       true  },
    { Type::UChar,       false },
    { Type::WChar,       boost::logic::indeterminate },
    { Type::Bool,        boost::logic::indeterminate },
    { Type::Short,       true  },
    { Type::UShort,      false },
    { Type::Integer,     true  },
    { Type::UInteger,    false },
    { Type::Long,        true  },
    { Type::ULong,       false },
    { Type::LLong,       true  },
    { Type::ULLong,      false },
    { Type::Float,       true  },
    { Type::Double,      true  },
    { Type::ShortString, boost::logic::indeterminate },
    { Type::LDouble,     true  },
    { Type::Fixed2,      true  },
    { Type::String,      boost::logic::indeterminate },
    { Type::Tm,          boost::logic::indeterminate }
};

static const char * TypeName(Type t) {
    static std::map<Type, const char *> names {
        { Type::Empty,          "empty" },
        { Type::Char,           "char" },
        { Type::SChar,          "signed char" },
        { Type::UChar,          "unsigned char" },
        { Type::WChar,          "wchar_t" },
        { Type::Bool,           "bool" },
        { Type::Short,          "short" },
        { Type::UShort,         "unsigned short" },
        { Type::Integer,        "int" },
        { Type::UInteger,       "unsigned int" },
        { Type::Long,           "long" },
        { Type::ULong,          "unsigned long" },
        { Type::LLong,          "long long" },
        { Type::ULLong,         "unsigned long long" },
        { Type::Float,          "float" },
        { Type::Double,         "double" },
        { Type::ShortString,    "char[]" },
        { Type::LDouble,        "long double" },
        { Type::Fixed2,         "fixed(2)" },
        { Type::String,         "std::string" },
        { Type::Tm,             "std::tm" },
        { Type::Unknown,        "void*" }
    };
    return names[t];
}

} //_ namespace variant
} //_ namespace negernsx



namespace negernsx {
namespace {

class ContentHolderBase {
public:
    virtual ~ContentHolderBase() { }
    virtual ContentHolderBase * clone() const = 0;
};



template <typename T>
class ContentHolder : public ContentHolderBase {
public:
    ContentHolder(const T &t) : value(t) { }
    //ContentHolder(T &&t) : value(static_cast<T &&>(t)) { }
    ContentHolder(T &&t) : value(std::move(t)) { }
    virtual ~ContentHolder() { }
    T get() const { return value; }
    void set(const T &v) { value = v; }
private:
    virtual ContentHolder * clone() const override {
        return new ContentHolder<T>(value);
    }
public:
    T value;
};



template <>
class ContentHolder<char> : public ContentHolderBase {
public:
    ContentHolder(const char *v) :
        len(std::strlen(v)),
        value(new char[len + string::null_size])
    {
        //value = new char[len + string::null_size];
        std::memcpy(value, v, len);
        value[len] = string::null_char;
    }
    ContentHolder(const char *v, std::size_t n) :
        len(n),
        value(new char[len + string::null_size])
    {
        //value = new char[len + string::null_size];
        std::memcpy(value, v, len);
        value[len] = string::null_char;
    }
    //ContentHolder(const char *&&t) : value(std::move(t)){ }
    virtual ~ContentHolder() {
        delete [] value;
    }
    const char * get() const {
        return &value[0];
    }
    std::size_t length() const { return len; }
private:
    virtual ContentHolder * clone() const override {
        return new ContentHolder<char>(&value[0], len);
    }
public:
    std::size_t len;
    char *value;
};




typedef ContentHolder<long double>      LDoubleHolder_t;
typedef ContentHolder<negerns::dec2_t>  Dec2Holder_t;
typedef ContentHolder<char>             CharPtrHolder_t;
typedef ContentHolder<std::tm>          TmHolder_t;

} //_ anonymous namespace
} //_ namespace negerns



namespace negernsx {

class Var;

namespace variant {

const bool positive = true;
const bool negative = false;

std::string unsigned_to_string(uint64_t, boost::logic::tribool = boost::logic::indeterminate);
std::string float_to_string(double, uint8_t = 6, boost::logic::tribool = boost::logic::indeterminate);

char        get_as_char(const negernsx::Var *v);
int8_t      get_as_int8(const negernsx::Var *v);
uint8_t     get_as_uint8(const negernsx::Var *v);
int16_t     get_as_int16(const negernsx::Var *v);
uint16_t    get_as_uint16(const negernsx::Var *v);
int32_t     get_as_int32(const negernsx::Var *v);
uint32_t    get_as_uint32(const negernsx::Var *v);
int64_t     get_as_int64(const negernsx::Var *v);
uint64_t    get_as_uint64(const negernsx::Var *v);
uint64_t    get_as_abs(const negernsx::Var *v);
std::string get_as_string(const negernsx::Var *v, uint8_t prec);
std::string get_as_fstring(const negernsx::Var *v, uint8_t prec);

} //_ namespace variant
} //_ namespace negernsx



namespace negernsx {

//! Var class for fundamental types implemented as a tagged union.
//! Aims to be space efficient with as little performance overhead as possible.
//!
//! Supports simple type promotion for some types.
//!
//! \todo It may be modified to hold other types like Boost::Any.
class Var {

    friend char         variant::get_as_char(const Var *);
    friend int8_t       variant::get_as_int8(const Var *);
    friend uint8_t      variant::get_as_uint8(const Var *);
    friend int16_t      variant::get_as_int16(const Var *);
    friend uint16_t     variant::get_as_uint16(const Var *);
    friend int32_t      variant::get_as_int32(const Var *);
    friend uint32_t     variant::get_as_uint32(const Var *);
    friend int64_t      variant::get_as_int64(const Var *);
    friend uint64_t     variant::get_as_uint64(const Var *);
    friend uint64_t     variant::get_as_abs(const Var *);
    friend std::string  variant::get_as_string(const Var *, uint8_t = 6);
    friend std::string  variant::get_as_fstring(const Var *, uint8_t = 6);

    union {
        double              double_;
        float               float_;
        unsigned long long  ullong_;
        long long           llong_;
        unsigned long       ulong_;
        long                long_;
        unsigned int        uint_;
        int                 int_;
        unsigned short      ushort_;
        short               short_;
        unsigned char       uchar_;
        signed char         schar_;
        char                char_;
        char                shortstr_[8];
        ContentHolderBase*  content;
    };
    variant::Type type = variant::Type::Empty;

public:

    Var() { }
    Var(char v) :           type(variant::Type::Char), char_(v)           { }
    Var(signed char v) :    type(variant::Type::SChar), schar_(v)         { }
    Var(unsigned char v) :  type(variant::Type::UChar), uchar_(v)         { }
    Var(bool v) :           type(variant::Type::Bool), uint_(v ? 1U : 0U) { }
    Var(short v) :          type(variant::Type::Short), short_(v)         { }
    Var(unsigned short v) : type(variant::Type::UShort), ushort_(v)       { }
    Var(int v) :            type(variant::Type::Integer), long_(v)        { }
    Var(unsigned int v) :   type(variant::Type::UInteger), ulong_(v)      { }
    Var(long v) :           type(variant::Type::Long), long_(v)           { }
    Var(unsigned long v) :  type(variant::Type::ULong), ulong_(v)         { }
    Var(long long v) :      type(variant::Type::LLong), llong_(v)         { }
    Var(ullong v) :         type(variant::Type::ULLong), ullong_(v)       { }
    Var(float v) :          type(variant::Type::Float), float_(v)         { }
    Var(double v) :         type(variant::Type::Double), double_(v)       { }

    Var(long double v) :
        type(variant::Type::LDouble),
        content(new LDoubleHolder_t(v))
    { }
    Var(const negerns::dec2_t &v) :
        type(variant::Type::Fixed2),
        content(new Dec2Holder_t(v))
    { }
    Var(const char *v);
    Var(const std::string &v);
    Var(const std::tm &v) :
        type(variant::Type::Tm),
        content(new TmHolder_t(v))
    { }

    virtual ~Var() { clear(); }

    //! Copy constructor
    Var(const Var &o);

    //! Copy assignment
    Var & operator = (const Var &rhs);

    //! Move constructor
    Var(Var &&o);

    //! Move assignment
    Var & operator = (Var &&rhs);

    template <typename T>
    Var & operator = (T &&rhs) {
        return set(rhs);
    }

    bool empty() const { return type == variant::Type::Empty; }
    bool is_null() const { return empty(); }
    void clear();

    // Type functions
    // -------------------------------------------------------------------------

    bool is_type_signed() const;
    bool is_type_unsigned() const;
    bool is_type_arithmetic() const;
    bool is_type_char() const;
    bool is_type_integral() const;
    bool is_type_float() const;
    bool is_type_fixed() const;
    bool is_type_string() const;
    bool is_type_object() const;

    bool is_char() const        { return type == variant::Type::Char; }
    bool is_schar() const       { return type == variant::Type::SChar; }
    bool is_uchar() const       { return type == variant::Type::UChar; }
    bool is_wchar() const       { return type == variant::Type::WChar; }
    bool is_bool() const        { return type == variant::Type::Bool; }
    bool is_short() const       { return type == variant::Type::Short; }
    bool is_ushort() const      { return type == variant::Type::UShort; }
    bool is_int() const         { return type == variant::Type::Integer; }
    bool is_uint() const        { return type == variant::Type::UInteger; }
    bool is_long() const        { return type == variant::Type::Long; }
    bool is_ulong() const       { return type == variant::Type::ULong; }
    bool is_llong() const       { return type == variant::Type::LLong; }
    bool is_ullong() const      { return type == variant::Type::ULLong; }
    bool is_float() const       { return type == variant::Type::Float; }
    bool is_double() const      { return type == variant::Type::Double; }
    bool is_shortstring() const { return type == variant::Type::ShortString; }
    bool is_ldouble() const     { return type == variant::Type::LDouble; }
    bool is_fixed2() const      { return type == variant::Type::Fixed2; }
    bool is_string() const      { return type == variant::Type::String; }
    bool is_tm() const          { return type == variant::Type::Tm; }

    variant::Type get_type() const { return type; }
    const char * get_type_string() { return variant::TypeName(type); }

    char            get_char()      const { return char_; }
    signed char     get_schar()     const { return schar_; }
    unsigned char   get_uchar()     const { return uchar_; }
    bool            get_bool()      const { return uint_ != 0; }
    short           get_short()     const { return short_; }
    unsigned short  get_ushort()    const { return ushort_; }
    int             get_int()       const { return int_; }
    unsigned int    get_uint()      const { return uint_; }
    long            get_long()      const { return long_; }
    unsigned long   get_ulong()     const { return ulong_; }
    long long       get_llong()     const { return llong_; }
    ullong          get_ullong()    const { return ullong_; }
    float           get_float()     const { return float_; }
    double          get_double()    const { return double_; }
    long double     get_ldouble()   const;
    dec2_t          get_fixed2()    const;
    std::string     get_string()    const;
    const char *    get_charptr()   const;
    std::tm         get_tm()        const;

    // Promotion functions
    //
    // Follows integral promotion.
    //
    // An exception is thrown if:
    // - a negative value is being accessed via a function returning an
    //   unsigned integral value (-1 accessed by as_uint()).
    // - the conversion is from a wider type (long to int)
    // -------------------------------------------------------------------------

    char          as_char()    const { return variant::get_as_char(this); }
    int           as_int()     const { return variant::get_as_int32(this); }
    unsigned int  as_uint()    const { return variant::get_as_uint32(this); }
    // How do we know that a long is 32-bits long?
    // How do we conditionally determine if long is 32-bit or 64-bit long?
    // This goes for the other types that needs this kind of special attention.
    // Note that there is char16_t and char32_t.
    long          as_long()    const { return variant::get_as_int32(this); }
    unsigned long as_ulong()   const { return variant::get_as_uint32(this); }
    int64_t       as_llong()   const { return variant::get_as_int64(this); }
    uint64_t      as_ullong()  const { return variant::get_as_uint64(this); }

    int8_t        as_int8()    const { return variant::get_as_int8(this); }
    uint8_t       as_uint8()   const { return variant::get_as_uint8(this); }
    int16_t       as_int16()   const { return variant::get_as_int16(this); }
    uint16_t      as_uint16()  const { return variant::get_as_uint16(this); }
    int32_t       as_int32()   const { return variant::get_as_int32(this); }
    uint32_t      as_uint32()  const { return variant::get_as_uint32(this); }
    int64_t       as_int64()   const { return variant::get_as_int64(this); }
    uint64_t      as_uint64()  const { return variant::get_as_uint64(this); }

    uint64_t      as_abs()     const; //{ return variant::get_as_abs(this); }

    double        as_double()  const;
    long double   as_ldouble() const;
    std::string   as_string(uint8_t prec = 6) const;
    std::string   as_fstring(uint8_t prec = 6) const;

    // Set function
    // -------------------------------------------------------------------------

    template <typename T>
    Var & set(T v) {
        clear();
        Var(v).swap(*this);
        return *this;
    }
#if 0
    Var & set(bool v) {
        clear();
        Var(v).swap(*this);
        return *this;
    }
#endif
    Var & set(const std::string &v) {
        clear();
        Var(v).swap(*this);
        return *this;
    }

    // Equality
    // -------------------------------------------------------------------------

    //. TODO: Fix comparison. Exceptions may be thrown or should it?
    //. This kind of equality checks for values only and not types.
    //. The internal value is converted to the type equivalent to the argument.

    bool operator == (const char rhs) const;
    bool operator == (const signed char rhs) const;
    bool operator == (const unsigned char rhs) const;
    bool operator == (bool rhs) const;
    bool operator == (const short &rhs) const;
    bool operator == (const unsigned short &rhs) const;
    bool operator == (const int &rhs) const;
    bool operator == (const unsigned int rhs) const;
    bool operator == (const long rhs) const;
    bool operator == (const unsigned long rhs) const;
    bool operator == (const long long rhs) const;
    bool operator == (const unsigned long long rhs) const;
    bool operator == (const float rhs) const;
    bool operator == (const double rhs) const;
    bool operator == (const long double rhs) const;
    bool operator == (const dec2_t &rhs) const;
    bool operator == (const char *rhs) const;
    bool operator == (const std::string &rhs) const;
    bool operator == (const std::tm &rhs) const;
    bool operator == (const Var &) const;

    // Inequality
    // -------------------------------------------------------------------------

    bool operator != (const char rhs) const;
    bool operator != (const signed char rhs) const;
    bool operator != (const unsigned char rhs) const;
    bool operator != (const bool rhs) const;
    bool operator != (const short &rhs) const;
    bool operator != (const unsigned short &rhs) const;
    bool operator != (const int &rhs) const;
    bool operator != (const unsigned int rhs) const;
    bool operator != (const long rhs) const;
    bool operator != (const unsigned long rhs) const;
    bool operator != (const long long rhs) const;
    bool operator != (const unsigned long long rhs) const;
    bool operator != (const float rhs) const;
    bool operator != (const double rhs) const;
    bool operator != (const long double rhs) const;
    bool operator != (const dec2_t rhs) const;
    bool operator != (const char *rhs) const;
    bool operator != (const std::string &rhs) const;
    bool operator != (const std::tm &rhs) const;
    bool operator != (const Var &rhs) const;

private:
    Var & swap(Var &rhs);

}; //_ class Var



#include <negernsx/core/inline/var_inline.h>

} //_ namespace negernsx

#endif //_ NEGERNS_X_CORE_VAR_H
