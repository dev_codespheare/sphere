#ifndef NEGERNS_X_CORE_STRING_FORMAT_MAIN_H
#define NEGERNS_X_CORE_STRING_FORMAT_MAIN_H

namespace negernsx {
namespace string {

void format_main();

} //_ namespace string
} //_ namespace negernsx

#endif //_ NEGERNS_X_CORE_STRING_FORMAT_MAIN_H
