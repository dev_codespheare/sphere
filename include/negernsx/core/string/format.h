#ifndef NEGERNS_X_CORE_STRING_FORMAT_H
#define NEGERNS_X_CORE_STRING_FORMAT_H

#include <string>
#include <vector>
#include <boost/container/static_vector.hpp>
#include <negerns/core/var.h>

#include <negerns/negerns.h>

namespace negernsx {
namespace string {
namespace format {

const bool positive = true;
const bool negative = false;

enum class Alignment : uint8_t {
    Default,
    Left = Default,
    Right
};

enum class Sign : uint8_t {
    Default,
    Negative = Default,
    Positive,
    Space
};

enum class Precision : uint8_t  {
    None = 0,
    Default = 6,
    Max = 9
};

enum class Conversion : uint8_t {
    None,
    Binary,
    HexLowerCase,
    HexUpperCase,
    Octal,
    TrueFalseLowerCase,
    TrueFalseUpperCase,
    YesNoLowerCase,
    YesNoUpperCase
};

struct FormatSpec
{
    uint8_t     index = 0;
    Alignment   alignment = Alignment::Default;
    Sign        sign= Sign::Default;
    uint8_t     width = 0;
    uint8_t     precision = 0;
    Conversion  conversion = Conversion::None;
};

// Temporary buffer for argument conversion.
// The minimum must not be less than the maximum number of bits of the
// largest integer type plus 3 (binary prefix symbol '0b' and null char).
// Note that larger string arguments (> 67 chars) will cause the vector
// to expand.
//? According to ...
//typedef boost::container::static_vector<char, std::numeric_limits<uint64_t>::digits + 3> Buffer;
typedef boost::container::static_vector<char, 100> Buffer;

std::size_t parse_unsigned(Buffer &, uint64_t, bool, Sign = Sign::Default);
std::size_t parse_float(Buffer &, long double, uint8_t, Sign = Sign::Default);
//std::size_t parse_string(Buffer)

std::size_t get_as_fstring(Buffer &, const n::Var *, uint8_t, FormatSpec = FormatSpec());
std::string formatx(const char *, const std::vector<n::Var> &);

} //_ namespace format
} //_ namespace string
} //_ namespace negernsx



namespace negernsx {
namespace string {

//std::string formatx(const char *, const std::vector<n::Var> &);

template <typename... A>
std::string Format(const char *fmtstr, A&&... a)
{
    return format::formatx(fmtstr, {a...});
}

} //_ namespace string
} //_ namespace negernsx

#endif //_ NEGERNS_X_CORE_STRING_FORMAT_H
