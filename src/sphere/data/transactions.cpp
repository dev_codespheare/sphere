#include <sphere/data/transactions.h>

using namespace sphere::data;

Transactions::Transactions()
{
#if 0
    using namespace n::data;

    n::data::TransactionTypes t;

    t.clear();
    t.emplace(transaction::Type::Read, "select id, label, abbrev, description from civil_status");
    n::data::Transaction::groups.emplace(transaction::Group::CivilStatusSet, t);

    t.clear();
    t.emplace(transaction::Type::Read, "select code, label, description from bir_status");
    n::data::Transaction::groups.emplace(transaction::Group::BirStatusSet, t);

    t.clear();
    t.emplace(transaction::Type::GetStructure, "select * from employee where employee_id = 0");
    t.emplace(transaction::Type::Read, "select * from employee");
    n::data::Transaction::groups.emplace(transaction::Group::EmployeeSet, t);

    t.clear();
    t.emplace(transaction::Type::Insert, "select employee_insert((?::integer, ?::integer, ?, ?, ?, ?, ?, ?, ?))");
    t.emplace(transaction::Type::Update, "select employee_update((?, ?, ?, ?, ?, ?, ?, ?, ?))");
    t.emplace(transaction::Type::Read, "select * from employee where employee_id = ?");
    n::data::Transaction::groups.emplace(transaction::Group::Employee, t);
#endif
}

Transactions::~Transactions() { }

void Transactions::Log()
{
#if 0
    auto count = n::data::transaction::Group::Max;
    for (std::size_t i = 0; i < count; ++i) {
        auto v = n::data::Transaction::groups.at(i);
        auto count2 = v.size();
        for (std::size_t k = 0; k < count2; ++k) {
            LOG_TRACE("SQL: %s", LOG_STR(v.at(i)));
        }
    }
#endif
}
