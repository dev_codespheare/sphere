#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <wx/aui/framemanager.h>
#include <negerns/core/log.h>
#include <negerns/core/system.h>
#include <sphere/cmn/def.h>
#include <sphere/ais/constants.h>
#include <sphere/ais/pane/results.h>

using namespace sphere::ais::pane;

Results::Results() :
    sphere::common::pane::Results()
{
    LOG_FUNCTION();
    // Override the system default minimum pane size
    // TODO: Determine where to keep the width when the application terminates
    auto config = n::System::Configuration();
    int height = config->GetInt("dynamic.pane.results.height", 0);
    if (height > 0) {
        paneInfo->MinSize(wxSize(0, height));
    }
}

Results::~Results()
{
    LOG_FUNCTION();
    // Save the current window size so that we can use
    // it to restore the window to the same size on show.
    wxSize size = paneInfo->window->GetClientSize();
#if 0
    ConfigBase* config = (GetConfig())->Get(CONST_CONFIG_AIS);
    config->Set(AisConfig::dynamic_pane_results_height, size.GetHeight());
#endif
}

void Results::CreateControl()
{
    LOG_FUNCTION();
    sphere::common::pane::Results::CreateControl();
}

void Results::DestroyControl()
{
    LOG_FUNCTION();
    sphere::common::pane::Results::DestroyControl();
}

void Results::ApplyConfiguration()
{
    LOG_FUNCTION();
    sphere::common::pane::Results::ApplyConfiguration();
}
