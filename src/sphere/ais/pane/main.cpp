#include <negerns/core/log.h>
#include <sphere/ais/pane/main.h>

using namespace sphere::ais::pane;

Main::Main() :
    sphere::common::pane::Main()
{
    LOG_FUNCTION();
    home = new sphere::common::component::Home();
    sphere::common::pane::Main::AddComponent(home);
}

Main::~Main() { }

void Main::CreateControl()
{
    LOG_FUNCTION();
    sphere::common::pane::Main::CreateControl();
}

void Main::DestroyControl()
{
    LOG_FUNCTION();
    sphere::common::pane::Main::DestroyControl();
}

bool Main::OnInitContent()
{
    LOG_FUNCTION();
    return sphere::common::pane::Main::OnInitContent();
}

void Main::ApplyConfiguration()
{
    LOG_FUNCTION();
    sphere::common::pane::Main::ApplyConfiguration();
}

