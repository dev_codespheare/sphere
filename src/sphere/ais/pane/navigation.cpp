#include <negerns/core/log.h>
#include <negerns/core/system.h>
#include <sphere/cmn/def.h>
#include <sphere/ais/constants.h>
#include <sphere/ais/pane/navigation.h>

using namespace sphere::ais::pane;

Navigation::Navigation() :
    sphere::common::pane::Navigation()
{
    LOG_FUNCTION();
    // Override the system default minimum pane size
    // TODO: Determine where to keep the width when the application terminates
    auto config = n::System::Configuration();
    int width = config->GetInt("dynamic.pane.navigation.width", 0);
    if (width > 0) {
        paneInfo->MinSize(wxSize(width, 120));
    }
}

Navigation::~Navigation()
{
    LOG_FUNCTION();
    // Save the current window size so that we can use
    // it to restore the window to the same size on show.
    wxSize size = paneInfo->window->GetClientSize();
#if 0
    ConfigBase* config = (GetConfig())->Get(CONST_CONFIG_AIS);
    config->Set(AisConfig::dynamic_pane_navigation_width, size.GetWidth());
#endif
}

void Navigation::PostCreateControl()
{
    LOG_FUNCTION();
    sphere::common::pane::Navigation::PostCreateControl();
    // TODO: Enable/disable modules
}
