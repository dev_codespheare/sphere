#include <negerns/core/log.h>
#include <negerns/core/message.h>
#include <negerns/gui/bar/crudtoolbar.h>
#include <sphere/ais/dialog/employee/employee.h>

using namespace sphere::ais::dialog::employee;

Employee::Employee() :
    n::gui::PropertyDialog(nullptr, wxID_ANY, "Employee Edit")
{
    basic = new Basic();
    n::gui::PropertyDialog::AddComponent(basic);
}

Employee::~Employee() { }

void Employee::CreateControl()
{
    LOG_FUNCTION();
    auto tb = new n::gui::bar::CrudToolBar();
    tb->SetBorder(n::gui::bar::Bar::Border::Bottom);
    SetToolBar(tb);

    n::gui::PropertyDialog::CreateControl();

    tb->Bind(wxEVT_TOOL, &Employee::ICrud::Create, this, wxID_NEW);
    tb->Bind(wxEVT_TOOL, &Employee::ICrud::Update, this, wxID_SAVE);
    tb->Bind(wxEVT_TOOL, &Employee::ICrud::Refresh, this, wxID_REFRESH);
}

bool Employee::OnInitContent()
{
    return n::gui::PropertyDialog::OnInitContent();
}

void Employee::DestroyControl()
{
    LOG_FUNCTION();
    // NOTE:
    // Do not call DestroyControl here. Otherwise, an exception is thrown.
}

bool Employee::OnCreate(wxCommandEvent &e)
{
    if (basic->ChangesPending()) {
        n::Message msg("Create Employee Information", "The are modifications in the employee information.\nWould you like to save the changes first?");
        msg.SetIcon(wxICON_QUESTION);
        msg.SetButtons(wxYES_NO | wxCANCEL);
        auto ans = msg.Show();
        if (ans == wxID_YES) {
            basic->Update();
            basic->Create();
        } else if (ans == wxID_NO) {
            basic->Create();
        } else if (ans == wxID_CANCEL) {
            
        }
    } else {
        basic->Create();
    }
    return true;
}

bool Employee::OnRead(wxCommandEvent &)
{
    basic->Read();
    return true;
}

bool Employee::OnRefresh(wxCommandEvent &)
{
    n::Message msg("Refresh Employee Information", "There are modifications in the employee information.\nWould you like to discard the changes made?");
    msg.SetIcon(wxICON_QUESTION);
    msg.SetButtons(wxYES_NO | wxCANCEL);
    auto ans = msg.Show();
    if (ans == wxID_NO || ans == wxID_CANCEL) {
        return false;
    } else if (ans == wxID_YES) {
        basic->Refresh();
        return true;
    } else {
        return false;
    }
}

bool Employee::OnUpdate(wxCommandEvent &e)
{
    basic->Update();
    return true;
}

void Employee::OnCancelClicked(wxCommandEvent &e)
{
    if (basic->ChangesPending()) {
        n::Message msg("Close Employee Information", "The are modifications in the employee information.\nWould you like to save the changes made?");
        msg.SetIcon(wxICON_QUESTION);
        msg.SetButtons(wxYES_NO | wxCANCEL);
        auto ans = msg.Show();
        if (ans == wxID_YES) {
            basic->Update();
            e.Skip();
        } else if (ans == wxID_NO) {
            e.Skip();
        } else if (ans == wxID_CANCEL) {
            e.Skip(false);
        }
    } else {
        e.Skip();
    }
}
