#include <windows.h>
// Include winundef.h to avoid the warning C4002: too many actual parameters for macro 'Yield'
// because windows.h defines Yield.
#include <wx/msw/winundef.h>

#include <wx/button.h>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/message.h>
#include <negerns/core/event.h>
#include <negerns/core/system.h>
#include <negerns/gui/panel.h>
#include <negerns/gui/validator/dataxfer.h>
#include <negerns/gui/bar/crudtoolbar.h>
#include <negerns/gui/artprovider.h>
#include <sphere/data/transactions.h>
#include <sphere/ais/dialog/employee/basic.h>
#include <sphere/cmn/def.h>

using namespace sphere::ais::dialog::employee;

Basic::Basic() :
    Component()
{
    LOG_FUNCTION();
    //Component::IIdentifier::Set(CONST_COMPONENT_EMPLOYEE_DETAIL);
    Component::SetName("General");
}

Basic::~Basic()
{
    wxDELETE(flexgrid);

    n::System::EventManager()->Remove(events);
}

void Basic::CreateControl()
{
    LOG_FUNCTION();
    Component::CreateControl();

    wxPanel* p = (wxPanel*) Component::GetPanel();
    ASSERT(p != nullptr, "Parent should not be null.");

    auto panelBase = new n::gui::Panel(p);

    const int columns = 2;
    const wxSize gap(3, 3);
    const size_t columnIndex = 1;
    const wxSize shortSize(100, wxDefaultCoord);
    const wxSize normalSize(200, wxDefaultCoord);

    flexgrid = new n::gui::sizer::FlexGrid(columns, gap, panelBase);
    flexgrid->AddGrowableCol(columnIndex);

    codeCtrl =        flexgrid->AddText       ("Code:", shortSize, wxTE_READONLY);
    firstnameCtrl =   flexgrid->AddText       ("Firstname:", normalSize);
    middlenameCtrl =  flexgrid->AddText       ("Middlename:", normalSize);
    lastnameCtrl =    flexgrid->AddText       ("Lastname:", normalSize);
    sexCtrl =         flexgrid->AddChoice     ("Sex:", shortSize);
    dobCtrl =         flexgrid->AddDatePicker ("Birthday:", shortSize);
    pobCtrl =         flexgrid->AddText       ("Birthplace:", normalSize);
    civilStatusCtrl = flexgrid->AddChoice     ("Civil Status:", shortSize);

    panelBase->AddInStaticBox(flexgrid, n::Orientation::Horizontal,
        n::Proportion::Dynamic, wxEXPAND);
    Component::Add(panelBase, n::Proportion::Dynamic, wxEXPAND | wxLEFT | wxRIGHT | wxBOTTOM, 8);
    Component::FitContent(p);
    Component::Layout();
}

void Basic::DestroyControl()
{
}

bool Basic::OnInitContent()
{
    auto n = n::System::EventManager()->Add(1000, n::event::Type::DataReceive, EFUNC(&Basic::OnReceive, this));
    events.push_back(n);

    // Populate gender control

    ASSERT(sexCtrl != nullptr, "Control is null.");
    sexCtrl->Append("M");
    sexCtrl->Append("F");

    // Populate the civil status control

#if 0
    using namespace negerns::data::transaction;
    auto rs = Component::ExecTrans(Group::CivilStatus, Type::ReadRows);
    wxASSERT_MSG(civilStatusCtrl, "Control is null.");
#endif
#if 0
    using namespace negerns::data::transaction;
    auto rs = n::make_unique(new n::data::Rowset(Component::GetDataAccess()));
    rs->transactions.Add(Group::CivilStatusSet, Type::Read);
    auto count = rs->Retrieve();
    civilStatusCtrl->SetValues(rs.get(), "abbrev");
    rs.reset();
#endif
    // Set employee data exchange

#if 0

    auto trans = Component::GetDataAccess()->GetTransaction();
    item = trans->GetColumnMetadata(Group::Employee, Type::GetStructure);

    codeCtrl->SetValidator(&(item->current["employee_id"]), wxFILTER_EMPTY);
    firstnameCtrl->SetValidator(&(item->current["firstname"]), wxFILTER_EMPTY);
    middlenameCtrl->SetValidator(&(item->current["middlename"]), wxFILTER_EMPTY);
    lastnameCtrl->SetValidator(&(item->current["lastname"]), wxFILTER_EMPTY);
    sexCtrl->SetValidator(&(item->current["sex"]), wxFILTER_EMPTY);
    dobCtrl->SetValidator(&(item->current["birthday"]), wxFILTER_EMPTY);
    pobCtrl->SetValidator(&(item->current["birthplace"]), wxFILTER_EMPTY);
    civilStatusCtrl->SetValidator(&(item->current["civil_status_abbrev"]), wxFILTER_EMPTY);
#endif

    XferDataToWindow(Component::GetPanel());

    return true;
}

void Basic::OnReceive(negerns::EventData *data)
{
    LOG_FUNCTION();
    //item->Set(data->GetRow());
    //item->Log();
    XferDataToWindow(Component::GetPanel());
}

bool Basic::OnCreate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    //item->Set();
    XferDataToWindow(Component::GetPanel());
    firstnameCtrl->SetFocus();
    return true;
}

bool Basic::OnRead(wxCommandEvent &e)
{
    LOG_FUNCTION();
    XferDataToWindow(Component::GetPanel());
    firstnameCtrl->SetFocus();
    return true;
}

bool Basic::OnRefresh(wxCommandEvent &e)
{
    LOG_FUNCTION();
    //item->CopyOriginal();
    XferDataToWindow(Component::GetPanel());
    firstnameCtrl->SetFocus();
    return true;
}

bool Basic::OnUpdate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    XferDataFromWindow(Component::GetPanel());

    // Add one since the selection is zero-based and the
    // database key for civil status starts at one.
    //item->current["civil_status_id"] = civilStatusCtrl->GetSelection() + 1;
    //item->Log();

    using namespace negerns::data::transaction;
    if (ICrud::IsNew()) {
        //Component::ExecTrans(Group::Employee, Type::Insert, item.get());
    } else {
        //Component::ExecTrans(Group::Employee, Type::Update, item.get());
    }
    //item->CopyCurrent();
    return true;
}

bool Basic::ChangesPending()
{
    XferDataFromWindow(Component::GetPanel());
    //return item->IsModified();
    return true;
}
