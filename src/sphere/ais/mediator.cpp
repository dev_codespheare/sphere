#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <sphere/cmn/def.h>
#include <sphere/ais/mediator.h>
#include <sphere/ais/framemanager.h>

using namespace sphere::ais;

Mediator::Mediator() : common::Mediator(new FrameManager()) { }

Mediator::~Mediator()
{
    LOG_FUNCTION();
}

void Mediator::CreateControl()
{
    LOG_FUNCTION();
    common::Mediator::CreateControl();

    wxFrame* frame = frameManager->GetFrame();
    ASSERT(frame != nullptr, "Application frame window should not be null.");
    BindMenu(frame, &Mediator::OnLogIn, this, "connect_menu_item");
    BindMenu(frame, &Mediator::OnLogOut, this, "disconnect_menu_item");
    
    frameManager->Show();
}
