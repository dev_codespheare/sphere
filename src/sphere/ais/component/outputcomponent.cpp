#include <negerns/gui/outputctrl.h>
#include <core/ais/component/results/outputcomponent.h>

using namespace sphere;
using namespace sphere::ais;

OutputComponent::OutputComponent() :
    PaneComponentBase(),
    control(0)
{
    LOG_FUNCTION();
    PaneComponentBase::SetName("Output");
}

OutputComponent::~OutputComponent()
{
    LOG_FUNCTION();
}

void OutputComponent::CreateControl()
{
    LOG_FUNCTION();
    PaneComponentBase::CreateControl();
    wxWindow* window = (wxWindow*) PaneComponentBase::GetPanel();
    // TODO: Should create the output control here and pass the
    //       reference to the ComponentBase.
    ComponentBase::CreateOutputControl(window);
    control = ComponentBase::GetOutputControl();
    wxBoxSizer* sizer(PaneComponentBase::GetSizer());
    const int changeable = 1;
    sizer->Add((wxWindow*)control->get(), changeable, wxGROW | wxALL, wxBORDER_DEFAULT);

    PaneComponentBase::AddControl((wxWindow*)control->get());
}

bool OutputComponent::HasFocus(void) const
{
    return false;
}

bool OutputComponent::SetFocus(const wxString&)
{
    return true;
}
