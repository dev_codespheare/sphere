#include <negerns/core/log.h>
#include <sphere/cmn/system.h>
#include <sphere/ais/framemanager.h>
#include <sphere/ais/pane/main.h>
#include <sphere/ais/pane/navigation.h>
#include <sphere/ais/pane/results.h>

using namespace sphere::ais;

FrameManager::FrameManager() :
    common::FrameManager()
{ }

FrameManager::~FrameManager()
{
    LOG_FUNCTION();
}

void FrameManager::CreateControl()
{
    LOG_FUNCTION();
    std::size_t index = 0;
    index = common::FrameManager::AddPane(new sphere::ais::pane::Navigation());
    common::System::Pane::Navigation = index;
    index = common::FrameManager::AddPane(new sphere::ais::pane::Results());
    common::System::Pane::Results = index;
    index = common::FrameManager::AddPane(new sphere::ais::pane::Main());
    common::System::Pane::Main = index;

    common::FrameManager::CreateControl();
}

void FrameManager::ApplyConfiguration()
{
    LOG_FUNCTION();
    common::FrameManager::ApplyConfiguration();

    auto config = n::System::Configuration();
    bool status = config->GetBool("ui.window.statusbar", false);
    if (status) {

    }
}
