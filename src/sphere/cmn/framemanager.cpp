#include <string>
#include <wx/frame.h>
#include <wx/aui/aui.h>
#include <wx/xrc/xmlres.h>
#include <negerns/core/log.h>
#include <negerns/gui/resource.h>
#include <sphere/cmn/def.h>
#include <sphere/cmn/system.h>
#include <sphere/cmn/dialog/about.h>
#include <sphere/cmn/framemanager.h>
#include <sphere/cmn/artprovider.h>

#include <negerns/gui/artprovider.h>

using namespace sphere::common;

FrameManager::FrameManager() :
    negerns::gui::FrameManager()
{
    SetToolBar(new n::gui::bar::ToolBar(), false);
    SetStatusBar(new n::gui::bar::StatusBar(), false);
}

FrameManager::~FrameManager() { }

void FrameManager::CreateControl()
{
    LOG_FUNCTION();
    wxArtProvider::Push(new ArtProvider());
    negerns::gui::FrameManager::CreateControl();
    // Set the minimum size. The minimum size could be acquired from
    // a configuration setting if necessary or required.
    frame->SetMinSize(frame->GetSize());

#ifdef __WXMSW__
    frame->SetIcon(wxIcon("app_icon"));
#endif

    frame->SetMenuBar(wxXmlResource::Get()->LoadMenuBar("main_menu_bar"));

    // Set up frame toolbar
    if (IsToolBarSet()) {
        auto tb = GetToolBar();
        tb->AddTool(wxID_CLOSE, "Close", ART_ICON_NEGERNS_FILE_CLOSE, "Close");
        tb->AddTool(wxID_CLOSE_ALL, "Close All", ART_ICON_NEGERNS_FILE_CLOSE_ALL, "Close all");
        // TODO: Add dropdown list of modules
        tb->AddStretchSpacer();
        // TODO: Add username if logged in. Disconnected icon otherwise.
        //       When the user clicks the disconnected icon, user is prompted
        //       for log in credentials.
        //tb->AddTool(wxID_REFRESH, "Refresh", ART_ICON_NEGERNS_REFRESH, "Refresh employee list");
        tb->Realize();

        tb->Bind(wxEVT_TOOL, &FrameManager::ProcessToolBarClick, this, wxID_CLOSE);
        tb->Bind(wxEVT_TOOL, &FrameManager::ProcessToolBarClick, this, wxID_CLOSE_ALL);
    }

    if (IsStatusBarSet()) {
        auto sb = Component::GetStatusBar();
        sb->AddLabel(wxID_ANY, "Ready");
        sb->Realize();
    }

    BindMenu(frame, &FrameManager::OnQuit, this, "exit_menu_item");
    
    BindMenu(frame, &FrameManager::OnToggleToolBar, this, "toolbar_menu_item");
    BindMenu(frame, &FrameManager::OnToggleStatusBar, this, "statusbar_menu_item");
    BindMenu(frame, &FrameManager::OnToggleStayOnTop, this, "stayontop_menu_item");
    BindMenu(frame, &FrameManager::OnToggleFullscreen, this, "fullscreen_menu_item");
    BindMenu(frame, &FrameManager::OnToggleNavigationPane, this, "navigationpane_menu_item");
    BindMenu(frame, &FrameManager::OnToggleResultsPane, this, "resultspane_menu_item");

    BindMenu(frame, &FrameManager::OnViewHelp, this, "view_help_menu_item");
    BindMenu(frame, &FrameManager::OnAbout, this, "about_menu_item");
}

void FrameManager::DestroyControl()
{
    LOG_FUNCTION();
    negerns::gui::FrameManager::DestroyControl();
    wxArtProvider::Pop();
}

void FrameManager::ApplyConfiguration()
{
    auto dockArt = n::gui::FrameManager::AuiComponent::auiManager->GetArtProvider();
    // TODO: Make the caption size a constant
    dockArt->SetMetric(wxAUI_DOCKART_CAPTION_SIZE, 20);
    dockArt->SetColour(wxAUI_DOCKART_ACTIVE_CAPTION_COLOUR, wxColour(wxSYS_COLOUR_ACTIVECAPTION));
    dockArt->SetColour(wxAUI_DOCKART_ACTIVE_CAPTION_GRADIENT_COLOUR, wxColour(wxSYS_COLOUR_GRADIENTACTIVECAPTION));
    // Set a default font depending on the platform
#if 0
    int major = 0;
    int minor = 0;
    wxOperatingSystemId osId = wxGetOsVersion(&major, &minor);
    if (osId == wxOS_WINDOWS_NT && major >= 5) {
        int pointSize = 8;
        wxFont font(pointSize,
            wxFONTFAMILY_MODERN,
            wxFONTSTYLE_NORMAL,
            wxFONTWEIGHT_NORMAL,
            false,
            "Segoe UI");
        dockArt->SetFont(wxAUI_DOCKART_CAPTION_FONT, font);
    }
#endif
    wxFont font = wxSystemSettings::GetFont(wxSYS_DEFAULT_GUI_FONT);
    dockArt->SetFont(wxAUI_DOCKART_CAPTION_FONT, font);
    n::gui::FrameManager::AuiComponent::auiManager->Update();
}

void FrameManager::OnAbout(wxCommandEvent&)
{
    common::dialog::About dlg;
    dlg.CreateControl();
    int result = dlg.ShowModal();
}

void FrameManager::OnToggleNavigationPane(wxCommandEvent &)
{
    auto *pane = n::gui::manager::Pane::GetPane(System::Pane::Navigation);
    pane->ToggleDisplay();
}

void FrameManager::OnToggleResultsPane(wxCommandEvent &)
{
    auto *pane = n::gui::manager::Pane::GetPane(System::Pane::Results);
    pane->ToggleDisplay();
}
