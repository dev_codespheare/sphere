#include <negerns/core/log.h>
#include <negerns/core/system.h>
#include <sphere/cmn/pane/main.h>

using namespace sphere::common::pane;

Main::Main() :
    Pane()
{
    LOG_FUNCTION();
    Component::SetName("Main");
    Identifier::Set("main_pane");
    paneInfo->Name("Main Pane");
    paneInfo->Layer(1);
    paneInfo->CenterPane();
    // Setting this as a center pane also sets the border. To hide the border,
    // it must be set after.
    paneInfo->PaneBorder(false);
    paneInfo->FloatingSize(wxSize(200, 400));
}

Main::~Main() { }

void Main::ApplyConfiguration()
{
    LOG_FUNCTION();
    Pane::ApplyConfiguration();

    auto config = n::System::Configuration();
    bool showStatus = config->GetBool("ui.pane.main.caption", false);
    ShowPaneCaption(showStatus);
}
