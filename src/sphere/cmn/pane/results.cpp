#include <negerns/core/log.h>
#include <negerns/core/system.h>
#include <sphere/cmn/pane/results.h>

using namespace sphere::common::pane;

Results::Results() :
    Pane()
{
    LOG_FUNCTION();
    Component::SetName("Results");
    Identifier::Set("results_pane");
    paneInfo->Name("Results Pane");
    paneInfo->Layer(0);
    paneInfo->CaptionVisible(false);
    paneInfo->Bottom();
    paneInfo->FloatingSize(wxSize(400, 200));

    Pane::AddComponent(new component::Output());
}

Results::~Results() { }

void Results::ApplyConfiguration()
{
    LOG_FUNCTION();
    Pane::ApplyConfiguration();

    auto config = n::System::Configuration();
    bool showStatus = config->GetBool("ui.pane.results.caption", true);
    ShowPaneCaption(showStatus);
}
