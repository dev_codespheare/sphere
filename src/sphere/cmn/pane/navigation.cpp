#include <negerns/core/log.h>
#include <negerns/core/system.h>
#include <sphere/cmn/pane/navigation.h>

using namespace sphere::common::pane;

Navigation::Navigation() :
    Pane()
{
    LOG_FUNCTION();
    Component::SetName("Navigation");
    Identifier::Set("navigation_pane");
    paneInfo->Name("Navigation Pane");
    paneInfo->Layer(1);
    paneInfo->CaptionVisible(false);
    paneInfo->Left();
    paneInfo->FloatingSize(wxSize(200, 400));

    modules = new sphere::common::component::Modules();
    sphere::common::Pane::AddComponent(modules);
}

Navigation::~Navigation() { }

void Navigation::CreateControl()
{
    LOG_FUNCTION();
    sphere::common::Pane::CreateControl();
}

void Navigation::PostCreateControl()
{
    sphere::common::Pane::PostCreateControl();
}

void Navigation::DestroyControl()
{
    LOG_FUNCTION();
    sphere::common::Pane::DestroyControl();
}

bool Navigation::OnInitContent()
{
    LOG_FUNCTION();
    return sphere::common::Pane::OnInitContent();
}

void Navigation::ApplyConfiguration()
{
    LOG_FUNCTION();
    auto config = n::System::Configuration();
    bool showStatus = config->GetBool("ui.pane.navigation.caption", true);
    ShowPaneCaption(showStatus);

    sphere::common::Pane::ApplyConfiguration();
}
