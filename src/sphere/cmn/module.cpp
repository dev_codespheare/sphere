#include <wx/debug.h>
#include <wx/xml/xml.h>
#include <negerns/core/debug.h>
#include <negerns/core/output.h>
#include <negerns/core/string.h>
#include <sphere/cmn/module.h>

using namespace sphere::common;

Module::Module()
{
    for (std::size_t i = IdStart; i < IdEnd; ++i) {
        status.emplace(i, true);
    }

    if (!resource) {
        resource = wxXmlResource::Get();
    }
    ReadExcludedNodes();
}

Module::~Module()
{
    status.clear();
}

ModuleNode * Module::InitContent()
{
    std::vector<std::string> items {
        "Human Resource",
        "-Employee",
        "-Holiday Calendar",
        "-Work Calendar",
        "-Daily Time Record",
        "Accounting",
        "-Chart of Accounts",
        "-Subsidiary Accounts",
        "-General Journal",
        "-Special Journals",
        "-General Ledger",
        "-Subsidiary Ledgers",
        "-Payroll",
        "System",
        "-Configuration"
    };

    auto *root = new ModuleNode(nullptr, 0, "Modules");
    BuildNodes(root, root, items, Module::IdStart);
    return root;
}

bool Module::IsEnabled(std::size_t id)
{
    ASSERT(status.find(id) != status.end(), "Module ID (%1) not found.", id);
    return status.at(id);
}

void Module::Disable(std::size_t id)
{
    status[id] = false;
}

void Module::BuildNodes(ModuleNode *parent,
    ModuleNode *current,
    const std::vector<std::string> &items,
    const std::size_t id)
{
    int level = 0;
    for (std::size_t i = 0; i < items.size(); ) {
        if (i == items.size()) return;
        if (IsEnabled(id + i)) {
            std::string s = items[i];
            int new_level = s.find_first_not_of("-");
            s = s.substr(new_level);

            if (new_level == level) {
                current = new ModuleNode(parent, id + i, s);
                parent->Add(current);
                i++;
            } else if (new_level > level) {
                parent = current;
                current = new ModuleNode(parent, id + i, s);
                parent->Add(current);
                level = new_level;
                i++;
            } else {
                parent = parent->GetParent();
                level--;
            }
        } else {
            i++;
        }
    }
}

void Module::ReadExcludedNodes()
{
    const wxXmlNode *node = resource->GetResourceNode("module_count");
    if (node) {
        std::string s = node->GetNodeContent().ToStdString();
        std::size_t count = n::string::stol(s);

        // Check number of items after the module_count node.
        std::size_t n = 0;
        while ((node = node->GetNext()) != nullptr) {
            n++;
        }
    
        for (std::size_t i = 1; i <= count; ++i) {
            s = n::Format("module_%1", i);
            const wxXmlNode *n = resource->GetResourceNode(s);
            if (n) {
                s = n->GetNodeContent().ToStdString();
                std::size_t id = n::string::stol(s);
                Disable(id);
            } else {
                negerns::Output::Send("Module item %1 could not be found in file.", s);
            }
        }
    }
}
