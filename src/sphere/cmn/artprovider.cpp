#include <windows.h>
// Include winundef.h to avoid the warning C4002: too many actual parameters for macro 'Yield'
// because windows.h defines Yield.
#include <wx/msw/winundef.h>

#include <wx/xrc/xmlres.h>
#include <wx/debug.h>
#include <negerns/core/log.h>
#include <negerns/gui/resource.h>
#include <sphere/cmn/artprovider.h>

using namespace sphere::common;

ArtProvider::ArtProvider() : wxArtProvider() { }

ArtProvider::~ArtProvider() { }

wxBitmap ArtProvider::CreateBitmap(const wxArtID& id, const wxArtClient& client, const wxSize& size)
{
    // Note:
    // wxAuiNotebook tab pages can display wxBitmap derived from icon files
    // but could not display wxBitmap derived from bitmap files.

    LOG_FUNCTION();
    wxBitmap bmp(wxNullBitmap);
    bool status = false;
    if (id == ART_ICON_APP) {
        status = bmp.CopyFromIcon(wxXmlResource::Get()->LoadIcon("icon_app"));
    } else if (id == ART_ICON_APP_BIG) {
        status = bmp.CopyFromIcon(wxXmlResource::Get()->LoadIcon("icon_app_big"));
    } else if (id == ART_ICON_FOLDER) {
        status = bmp.CopyFromIcon(wxXmlResource::Get()->LoadIcon("icon_folder"));
    } else if (id == ART_ICON_NEW) {
        status = bmp.CopyFromIcon(wxXmlResource::Get()->LoadIcon("icon_new"));
    } else if (id == ART_ICON_EDIT) {
        status = bmp.CopyFromIcon(wxXmlResource::Get()->LoadIcon("icon_edit"));
    } else if (id == ART_ICON_REFRESH) {
        status = bmp.CopyFromIcon(wxXmlResource::Get()->LoadIcon("icon_refresh"));
    }
    return (bmp.IsOk()) ? bmp : wxNullBitmap;
}
