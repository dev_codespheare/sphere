#include <negerns/core/log.h>
#include <negerns/core/message.h>
#include <negerns/core/debug.h>
#include <negerns/core/configuration.h>
#include <negerns/gui/data/base.h>
#include <sphere/cmn/def.h>
#include <sphere/cmn/constants.h>
#include <sphere/cmn/system.h>
#include <sphere/cmn/mediator.h>
#include <sphere/cmn/dialog/login.h>

using namespace sphere::common;

Mediator::Mediator(FrameManager* fm) :
    n::gui::Mediator(fm)
{ }

Mediator::~Mediator() { }

void Mediator::CreateControl()
{
    LOG_FUNCTION();
    n::gui::Mediator::CreateControl();

    auto *frame = frameManager->GetFrame();
    //BindMenu(frame, &Mediator::OnOpenPage, this, "file_open_menu_item");
    BindMenu(frame, &Mediator::OnSavePage, this, "file_save_menu_item");
    BindMenu(frame, &Mediator::OnClosePage, this, "file_close_menu_item");
    BindMenu(frame, &Mediator::OnCloseAllPages, this, "file_close_all_menu_item");
    BindMenu(frame, &Mediator::OnRefreshPage, this, "refresh_menu_item");
}


#ifdef _DEBUG

bool Mediator::AutoLogIn()
{
    LOG_FUNCTION();
    n::data::DataSource ds;
    auto connector = n::data::DataSource::Connector::ODBC;
    ds.SetConnector(n::data::DataSource::GetConnector(connector));
    ds.SetDataSourceName(GetDataSourceName());
    ds.SetUser("sphere");
    ds.SetPassword("sphere");
    ds.SetConnectionName("sphere ais");

    return n::gui::Mediator::LogIn(ds);
}
#endif

void Mediator::OnLogIn(wxCommandEvent&)
{
    LOG_FUNCTION();
    n::data::DataSource ds;
    sphere::common::dialog::LogIn dlg(NULL, wxID_ANY, "Log In");
    dlg.CreateControl();
    int result = dlg.ShowModal();
    if (result == wxID_OK) {
        wxString u = dlg.GetUser();
        wxString p = dlg.GetPassword();
        ds.SetUser(u.ToStdString());
        ds.SetPassword(p.ToStdString());
        auto connector = n::data::DataSource::Connector::ODBC;
        ds.SetConnector(n::data::DataSource::GetConnector(connector));
        ds.SetDataSourceName(GetDataSourceName());
        ds.SetConnectionName("sphere ais");
        n::gui::Mediator::LogIn(ds);
    }
}

void Mediator::ProcessToolBarClick(wxCommandEvent &e)
{
    switch (e.GetId()) {
        case wxID_CLOSE:
            OnClosePage(e);
            break;
        case wxID_CLOSE_ALL:
            OnCloseAllPages(e);
            break;
        default:
            n::Message::Error(n::Format("Unknown toolbar item %1", e.GetId()));
    }
}

void Mediator::OnSavePage(wxCommandEvent &)
{
    auto *pane = System::Pane::GetMain();
    auto n = pane->GetComponentIndex();
    // Do not save 'home' tabpage
    if (n > 1) {
        auto *component = static_cast<n::gui::data::Base*>(pane->GetComponent());
        component->Update();
    }
}

void Mediator::OnClosePage(wxCommandEvent &)
{
    auto *pane = System::Pane::GetMain();
    // Do not delete 'home' tabpage
    if (pane->GetCount() > 1) {
        auto *component = static_cast<n::gui::data::Base*>(pane->GetComponent());
        if (component->CanClose()) {
            pane->ClosePage();
        }
    }
}

void Mediator::OnCloseAllPages(wxCommandEvent &)
{
    auto *pane = System::Pane::GetMain();
    // Do not delete 'home' tabpage
    std::size_t count = pane->GetCount();
    if (count > 1) {
        // TODO: Freeze
        pane->Freeze();
        for (std::size_t i = 1; i < count; ++i) {
            pane->ClosePage(1);
        }
        pane->Thaw();
    }
}

void Mediator::OnRefreshPage(wxCommandEvent &)
{
    auto *pane = System::Pane::GetMain();
    auto *component = static_cast<n::gui::data::Base*>(pane->GetComponent());
    component->Refresh();
}

std::string Mediator::GetDataSourceName()
{
    std::string dsn;
    auto config = n::System::Configuration();
#ifdef __WINDOWS__
#  ifdef _MSC_VER
    // Must check for 64-bit defines first because wxWidgets
    // also defines 32-bit symbols even when building 64-bit
#   if defined(__WIN64__) || defined(_WIN64)
    dsn = config->get_string("database.odbc.dsn.64", "");
#   else
    dsn = config->get_string("database.odbc.dsn.32", "");
#   endif
#  endif
#endif
    ASSERT(dsn.length() > 0, "Datasource key not found in configuration file.");
    return dsn;
}
