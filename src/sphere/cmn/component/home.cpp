#include <wx/statbmp.h>
#include <negerns/core/debug.h>
#include <negerns/gui/app.h>
#include <negerns/gui/sizer/box.h>
#include <negerns/gui/sizer/flexgrid.h>
#include <negerns/core/log.h>
#include <negerns/gui/artprovider.h>
#include <sphere/cmn/system.h>
#include <sphere/cmn/artprovider.h>
#include <sphere/cmn/component/home.h>

using namespace sphere::common::component;

Home::Home() :
    Component()
{
    LOG_FUNCTION();
    Component::SetName("Home");
    Component::SetBitmap(wxArtProvider::GetBitmap(ART_ICON_NEGERNS_HOME));

    auto *tb = new n::gui::bar::ToolBar();
    SetToolBar(tb);
    tb->SetBorder(n::gui::bar::Bar::Border::Bottom);
}

Home::~Home()
{
    LOG_FUNCTION();
}

void Home::CreateControl()
{
    LOG_FUNCTION();
    Component::CreateControl();

    auto *tb = GetToolBar();
    tb->AddTool(wxID_ANY, "Save", ART_ICON_NEGERNS_SAVE, "Save changes");
    tb->AddSeparator();
    tb->AddTool(wxID_REFRESH, "Revert", ART_ICON_NEGERNS_REFRESH, "Revert to original information");
    tb->Realize();

    if (IsToolBarSet()) {
        auto tb = GetToolBar();
        //tb->AddTool(wxID_NEW, "Test", ART_ICON_NEGERNS_NEW, "Test");
        tb->Realize();
    }

    wxPanel* p = (wxPanel*) Component::GetPanel();
    ASSERT(p != nullptr, "Parent should not be null.");
    auto color = wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW);
    p->SetBackgroundColour(color);

    auto info = sphere::common::System::Application()->GetAboutInfo();

    auto panelBase = new n::gui::Panel(p);

    auto *boxSizer = new n::gui::sizer::Box(nullptr, n::Orientation::Horizontal);

    auto *image = new wxStaticBitmap((wxWindow *) panelBase, wxID_ANY, wxNullBitmap);
    image->SetIcon(wxArtProvider::GetIcon(ART_ICON_APP_BIG, wxART_OTHER, wxSize(48, 48)));

    const int columns = 1;
    const wxSize gap(0, 0);
    const size_t growColumn = 0;

    auto flexgrid = new n::gui::sizer::FlexGrid(columns, gap, panelBase);
    flexgrid->AddGrowableCol(growColumn);
    flexgrid->SetFlexibleDirection(wxHORIZONTAL);

    flexgrid->AddStaticText(info.GetName());
    if (info.HasEdition()) {
        flexgrid->AddStaticText(info.GetEdition());
    }
    flexgrid->AddStaticText(info.GetLongVersion());
    flexgrid->AddSpacer(10);
    if (info.IsEvaluation()) {
        flexgrid->AddStaticText(n::Format("Evaluation copy for %1", info.GetLicensee()));
    } else {
        flexgrid->AddStaticText(n::Format("Licensed to %1", info.GetLicensee()));
    }
    flexgrid->Fit(panelBase);

    boxSizer->Add(image, n::Proportion::Static, wxTOP | wxRIGHT, 1);
    boxSizer->Add(flexgrid, n::Proportion::Dynamic, wxEXPAND | wxLEFT, 10);

    auto *boxSizer2 = new n::gui::sizer::Box(nullptr, n::Orientation::Horizontal);

    panelBase->AddInStaticBox(boxSizer, n::Orientation::Horizontal, n::Proportion::Static, wxEXPAND | wxLEFT | wxRIGHT, 8);
    panelBase->AddInStaticBox(boxSizer2, n::Orientation::Vertical, n::Proportion::Dynamic, wxEXPAND | wxALL, 8);
    Component::Add(panelBase, n::Proportion::Dynamic, wxEXPAND | wxLEFT | wxRIGHT | wxBOTTOM, 8);
}
