#include <sphere/cmn/system.h>
#include <sphere/cmn/component/list/employee.h>
#include <sphere/cmn/component/details/employee.h>

using namespace sphere::common::component::list;

Employee::Employee() :
    n::gui::data::List()
{
    LOG_FUNCTION();
    Component::SetName("Employee");
    Identifier::Set("employee_list");
}

Employee::~Employee() { }

void Employee::CreateControl()
{
    LOG_FUNCTION();
    auto *list = GetListCtrl();
    list->SetSingleSelection();
    list->AppendTextColumn(1, "", 30, n::Alignment::Center,
        n::gui::ListView::CellMode::Inert,
        n::gui::ListView::ColumnFlags::None);
    list->AppendTextColumn(2, "Firstname", 180, n::Alignment::Left,
        n::gui::ListView::CellMode::Inert,
        n::gui::ListView::ColumnFlags::Resizable);
    list->AppendTextColumn(5, "Sex", 35, n::Alignment::Center,
        n::gui::ListView::CellMode::Inert,
        n::gui::ListView::ColumnFlags::None);
    list->AppendDateColumn(6, "Birthday");
    list->AppendTextColumn(7, "Birthplace");
    list->AppendTextColumn(9, "Civil Status", 70, n::Alignment::Center);

    n::gui::data::List::CreateControl();
}

void Employee::DestroyControl()
{
    LOG_FUNCTION();
    n::gui::data::List::DestroyControl();
}

bool Employee::OnInitContent()
{
    LOG_FUNCTION();
    n::gui::data::List::OnInitContent();
    auto *ds = GetDataStore();
    ds->sqlSelect.Set("select \
        person_id, \
        employee_id, \
        firstname, \
        middlename, \
        lastname, \
        sex, \
        birthday, \
        birthplace, \
        civil_status_id, \
        civil_status_abbrev from employee");
    return true;
}

void Employee::ApplyConfiguration()
{
    LOG_FUNCTION();
    n::gui::data::List::ApplyConfiguration();
}

bool Employee::OnCreate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    n::gui::data::List::OnCreate(e);

    auto *component = new details::Employee();
    System::Pane::AddPage(System::Pane::Main, component);
    component->Create({0});
    return true;
}

void Employee::OnEdit(wxCommandEvent &e)
{
    LOG_FUNCTION();
    n::gui::data::List::OnEdit(e);

    if (current.IsOk()) {
        auto *list = GetListCtrl();
        auto row = list->GetRow(current);

        auto *component = new details::Employee();
        std::string id = component->Identifier::Get(row["employee_id"]);

        if (!GetPane()->IsComponentPresent(id)) {
            System::Pane::AddPage(System::Pane::Main, component);
            negerns::data::Parameters params;
            params.push_back(row["employee_id"].convert<long>());
            component->Read(params);
        } else {
            wxDELETE(component);
            std::size_t index = GetPane()->GetComponentIndex(id);
            System::Pane::SetPage(System::Pane::Main, index);
        }
    }
}

bool Employee::OnRowDisplay(std::size_t row, std::size_t column, wxVariant &variant)
{
    auto *ds = GetDataStore();
    auto name = ds->column[column].name;
    if (name == "firstname") {
        auto firstname = ds->original(row, "firstname").convert<std::string>();
        auto middlename = ds->original.IfNull(row, "middlename", "").convert<std::string>();
        auto lastname = ds->original.IfNull(row, "lastname", "").convert<std::string>();
        variant = n::Format("%1, %2 %3", lastname, firstname, middlename);
        return false;
    }
    return true;
}
