#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <sphere/cmn/system.h>
#include <sphere/cmn/component/list/workcalendar.h>

#include <sphere/cmn/component/details/calendarday.h>
#include <sphere/cmn/component/details/workcalendar.h>

using namespace sphere::common::component::list;

WorkCalendar::WorkCalendar() :
    n::gui::data::List()
{
    LOG_FUNCTION();
    Component::SetName("Work Calendar");
    Identifier::Set("work_calendar");
}

WorkCalendar::~WorkCalendar() { }

void WorkCalendar::CreateControl()
{
    LOG_FUNCTION();
    auto *list = GetListCtrl();
    list->SetSingleSelection();
    list->AppendDateColumn(1, "Date");
    list->AppendTextColumn(3, "Code", 50, n::Alignment::Center);
    list->AppendTextColumn(4, "Type Name", 200);

    n::gui::data::List::CreateControl();
}

void WorkCalendar::DestroyControl()
{
    LOG_FUNCTION();
    n::gui::data::List::DestroyControl();
}

bool WorkCalendar::OnInitContent()
{
    LOG_FUNCTION();
    n::gui::data::List::OnInitContent();
    auto *ds = GetDataStore();
    ds->sqlSelect.Set("select * from work_calendar");
    return true;
}

void WorkCalendar::ApplyConfiguration()
{
    LOG_FUNCTION();
    n::gui::data::List::ApplyConfiguration();
}

bool WorkCalendar::OnCreate(wxCommandEvent &)
{
    LOG_FUNCTION();
    auto *component = new details::WorkCalendar();
    System::Pane::AddPage(System::Pane::Main, component);
    component->Create({0});
    return true;
}

void WorkCalendar::OnEdit(wxCommandEvent &e)
{
    LOG_FUNCTION();
    n::gui::data::List::OnEdit(e);
    if (current.IsOk()) {
        auto *list = GetListCtrl();
        auto row = list->GetRow(current);

        auto *component = new details::CalendarDay();
        std::string id = component->Identifier::Get(row["id"]);

        if (!GetPane()->IsComponentPresent(id)) {
            System::Pane::AddPage(System::Pane::Main, component);

            negerns::data::Parameters params;
            params.push_back(row["id"].convert<long>());
            component->Read(params);
        } else {
            wxDELETE(component);
            std::size_t index = GetPane()->GetComponentIndex(id);
            System::Pane::SetPage(System::Pane::Main, index);
        }
    }
}

bool WorkCalendar::OnRowDisplay(std::size_t row, std::size_t column, wxVariant &variant)
{
    return true;
}
