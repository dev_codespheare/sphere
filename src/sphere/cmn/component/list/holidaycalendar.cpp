#include <sphere/cmn/system.h>
#include <sphere/cmn/component/list/holidaycalendar.h>

using namespace sphere::common::component::list;

HolidayCalendar::HolidayCalendar() :
    n::gui::data::List()
{
    LOG_FUNCTION();
    Component::SetName("Holiday Calendar");
    Identifier::Set("holiday_calendar");
}

HolidayCalendar::~HolidayCalendar() { }

void HolidayCalendar::CreateControl()
{
    LOG_FUNCTION();
    auto *list = GetListCtrl();
    list->SetSingleSelection();
    list->AppendTextColumn(2, "Month", 70);
    list->AppendTextColumn(3, "Day", 40, n::Alignment::Center);
    list->AppendTextColumn(4, "Name", 140);
    list->AppendTextColumn(5, "Day Type", 160);

    n::gui::data::List::CreateControl();
}

void HolidayCalendar::DestroyControl()
{
    LOG_FUNCTION();
    n::gui::data::List::DestroyControl();
}

bool HolidayCalendar::OnInitContent()
{
    LOG_FUNCTION();
    n::gui::data::List::OnInitContent();
    auto *ds = GetDataStore();
    ds->sqlSelect.Set("select id, month, month_name, day, name, day_type_name from holiday_calendar");
    return true;
}

void HolidayCalendar::ApplyConfiguration()
{
    LOG_FUNCTION();
    n::gui::data::List::ApplyConfiguration();
}

bool HolidayCalendar::OnCreate(wxCommandEvent &)
{
    LOG_FUNCTION();
#if 0
    auto *component = new details::HolidayCalendar();
    System::Pane::AddPage(System::Pane::Main, component);
    component->Create({0});
#endif
    return true;
}

void HolidayCalendar::OnEdit(wxCommandEvent &e)
{
    LOG_FUNCTION();
#if 0
    DataList::OnEdit(e);
    if (current.IsOk()) {
        auto *list = GetListCtrl();
        auto row = list->GetRow(current);

        auto *component = new details::WorkCalendar();
        std::string id = component->Identifier::Get(row["id"]);

        if (!GetPane()->IsComponentPresent(id)) {
            System::Pane::AddPage(System::Pane::Main, component);

            negerns::data::Parameters params;
            params.push_back(row["id"].convert<long>());
            component->Read(params);
        } else {
            wxDELETE(component);
            std::size_t index = GetPane()->GetComponentIndex(id);
            System::Pane::SetPage(System::Pane::Main, index);
        }
    }
#endif
}

bool HolidayCalendar::OnRowDisplay(std::size_t row, std::size_t column, wxVariant &variant)
{
    return true;
}
