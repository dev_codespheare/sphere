#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/output.h>
#include <negerns/gui/outputctrl.h>
#include <negerns/gui/bar/toolbar.h>
#include <negerns/gui/artprovider.h>
#include <sphere/cmn/component/output.h>

#include <negerns/negerns.h>

using namespace sphere::common::component;

sphere::common::component::Output::Output() :
    Component()
{
    LOG_FUNCTION();
    Component::SetName("Output");

    auto tb = SetToolBar(new n::gui::bar::ToolBar());
    tb->SetBorder(n::gui::bar::Bar::Border::Bottom);
}

sphere::common::component::Output::~Output()
{
    LOG_FUNCTION();
}

void sphere::common::component::Output::CreateControl()
{
    LOG_FUNCTION();
    Component::CreateControl();
    
    auto *tb = GetToolBar();
    auto *tool = tb->AddTool(wxID_ANY, "Clear", ART_ICON_NEGERNS_CLEAR, "Clear all output contents");
    tb->AddSeparator();
    tb->Realize();

    tb->Bind(wxEVT_TOOL, &Output::OnClearContents, this, tool->GetId());

    wxWindow *parent = (wxWindow*) Component::GetPanel();
    ASSERT(parent != nullptr, "Parent should not be null.");

    auto *control = new n::gui::OutputCtrl(parent);
    n::Output::Init(control);

    auto *window = control->GetControl();
    Component::Add(window, n::Proportion::Changeable, wxGROW | wxALL);
}

void sphere::common::component::Output::OnClearContents(wxCommandEvent &)
{
    n::Output::Clear();
}
