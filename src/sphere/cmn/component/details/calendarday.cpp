#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/debug.h>
#include <sphere/ais/constants.h>
#include <sphere/cmn/component/details/calendarday.h>

using namespace sphere::common::component::details;

CalendarDay::CalendarDay() :
    n::gui::data::input::Single()
{
    LOG_FUNCTION();
    Component::SetName("Work Calendar Detail");
    Identifier::set("work_calendar_detail");

    subject.Set("Work Calendar");

    SetParameters({0});
}

CalendarDay::~CalendarDay()
{
    LOG_FUNCTION();
    wxDELETE(flexgrid);
}

void CalendarDay::CreateControl()
{
    LOG_FUNCTION();
    n::gui::data::input::Single::CreateControl();

    wxPanel* p = (wxPanel*) Component::GetPanel();
    ASSERT(p != nullptr, "Parent should not be null.");

    auto panelBase = new n::gui::Panel(p);

    const int columns = 2;
    const wxSize gap(5, 3);
    const size_t columnIndex = 1;
    const wxSize shortSize(100, wxDefaultCoord);
    const wxSize normalSize(200, wxDefaultCoord);

    flexgrid = new n::gui::sizer::FlexGrid(columns, gap, panelBase);
    flexgrid->AddGrowableCol(columnIndex);

    dateCtrl = flexgrid->AddDatePicker("Date:", shortSize);
    typeCtrl = flexgrid->AddChoice    ("Type:", normalSize);

    SetFirstFocus(dateCtrl);

    panelBase->AddInStaticBox(flexgrid, n::Orientation::Horizontal, n::Proportion::Dynamic, wxEXPAND);
    Component::Add(panelBase, n::Proportion::Dynamic, wxEXPAND | wxLEFT | wxRIGHT | wxBOTTOM, 8);
}

bool CalendarDay::OnInitContent()
{
    LOG_FUNCTION();
    n::gui::data::input::Single::OnInitContent();
    auto *ds = GetDataStore();
    ds->sqlSelect.Set("select * from work_calendar where id = ?");
    ds->sqlInsert.Set("select work_calendar_insert((?, ?, ?))");
    ds->sqlUpdate.Set("select work_calendar_update((?, ?, ?))");
    return true;
}

void CalendarDay::PostInitContent()
{
    LOG_FUNCTION();
    n::gui::data::input::Single::PostInitContent();
    {   
        auto *ds = CreateDataStore();
        ds->sqlSelect.Set("select id, name, abbrev from day_type");
        ds->Retrieve();
        typeCtrl->SetValues(ds, "name", "id");
    }

    dateCtrl->SetValidator(current["when"], wxFILTER_EMPTY);
    typeCtrl->SetValidator(current["day_type_id"], wxFILTER_EMPTY);

    auto *ds = GetDataStore();
    ds->default["day_type_id"] = typeCtrl->GetValue(0);
}
