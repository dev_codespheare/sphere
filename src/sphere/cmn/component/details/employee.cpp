#include <negerns/core/debug.h>
#include <negerns/core/output.h>
#include <negerns/core/defs.h>
#include <negerns/core/datetime.h>
#include <negerns/gui/sizer/vstaticbox.h>
#include <sphere/cmn/component/details/employee.h>

using namespace sphere::common::component::details;

Employee::Employee() :
    n::gui::data::input::Single()
{
    LOG_FUNCTION();
    Component::SetName("Employee Detail");
    Identifier::set("employee_detail");

    subject.Set("Employee");

    SetParameters({0});
}

Employee::~Employee()
{
    LOG_FUNCTION();
    wxDELETE(flexgrid);
}

void Employee::CreateControl()
{
    LOG_FUNCTION();
    n::gui::data::input::Single::CreateControl();

    auto *panel = (wxPanel*) Component::GetPanel();
    ASSERT(panel != nullptr, "Parent should not be null.");

    auto panelBase = new n::gui::Panel(panel);

    const int columns = 2;
    const wxSize gap(8, 3);
    const size_t columnIndex = 1;
    const wxSize shortSize(100, wxDefaultCoord);
    const wxSize normalSize(200, wxDefaultCoord);

    flexgrid = new n::gui::sizer::FlexGrid(columns, wxSize(8, 8), panelBase);
    flexgrid->AddGrowableCol(columnIndex);

    auto *genInfo = new n::gui::sizer::VStaticBox(panelBase, "General");
    auto *fg1 = new n::gui::sizer::FlexGrid(columns, gap, (wxWindow*) genInfo->GetStaticBox());
    fg1->AddGrowableCol(columnIndex);
    
#ifdef _DEBUG
    input.code =         fg1->AddSpin       ("Code:", shortSize);
#endif
    input.firstname =    fg1->AddText       ("Firstname:", normalSize);
    input.middlename =   fg1->AddText       ("Middlename:", normalSize);
    input.lastname =     fg1->AddText       ("Lastname:", normalSize);
    input.sex =          fg1->AddChoice     ("Sex:", shortSize);
    input.dob =          fg1->AddDatePicker ("Birthday:", shortSize);
    input.pob =          fg1->AddText       ("Birthplace:", normalSize);
    input.civil_status = fg1->AddChoice     ("Civil Status:", shortSize);

    fg1->AddSpacer(20);
    fg1->AddSpacer(20);

    input.driver_license = fg1->AddText("Driver's License:", normalSize);
    input.sss_id =         fg1->AddText("SSS ID:", normalSize);
    input.pagibig_id =     fg1->AddText("PAG-IBIG ID:", normalSize);
    input.philhealth_id =  fg1->AddText("PhilHealth ID:", normalSize);
    input.tax_id =         fg1->AddText("TIN:", normalSize);
    input.passport_id =    fg1->AddText("Passport ID:", normalSize);

    auto *empInfo = new n::gui::sizer::VStaticBox(panelBase, "Employment");
    auto *fg2 = new n::gui::sizer::FlexGrid(columns, gap, (wxWindow*) empInfo->GetStaticBox());
    fg2->AddGrowableCol(columnIndex);

    input.employment_date = fg2->AddDatePicker ("Date:", shortSize);
    input.employee_number = fg2->AddText       ("Number:", normalSize);
    input.status =          fg2->AddChoice     ("Status:", shortSize);

#ifdef _DEBUG
    input.code->Disable();
#endif
    SetFirstFocus(input.firstname);

    genInfo->Add(fg1, n::Proportion::Dynamic, wxEXPAND | wxLEFT | wxRIGHT | wxBOTTOM, 6);
    empInfo->Add(fg2, n::Proportion::Dynamic, wxEXPAND | wxLEFT | wxRIGHT | wxBOTTOM, 6);

    flexgrid->AddSizer(genInfo);
    flexgrid->AddSizer(empInfo);

    panelBase->Add(flexgrid, n::Proportion::Dynamic, wxEXPAND | wxALL, 8);

    {   // Do not put border so the scrollbar sticks to the edge.
        const int border = 0;
        Component::Add(panelBase, n::Proportion::Dynamic, wxEXPAND, border);
    }
}

bool Employee::OnInitContent()
{
    LOG_FUNCTION();
    n::gui::data::input::Single::OnInitContent();
    auto *ds = GetDataStore();
    ds->sqlSelect.Set("select \
        person_id, \
        employee_id, \
        firstname, \
        middlename, \
        lastname, \
        sex, \
        birthday, \
        birthplace, \
        civil_status_id, \
        driver_license, \
        sss_id, \
        pagibig_id, \
        philhealth_id, \
        tax_id, \
        passport_id, \
        employment_date, \
        employee_number, \
        status \
        from employee where employee_id = ?");
    ds->sqlInsert.Set("select employee_insert((?::integer, ?::integer, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?))");
    ds->sqlUpdate.Set("select employee_update((?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?))");
    return true;
}

void Employee::PostInitContent()
{
    LOG_FUNCTION();
    n::gui::data::input::Single::PostInitContent();

    // Populate gender control

    input.sex->Append("Male", new wxStringClientData("M"));
    input.sex->Append("Female", new wxStringClientData("F"));

    // Populate the civil status control

    {   
        auto ds = CreateDataStore();
        ds->sqlSelect.Set("select id, label, abbrev from civil_status");
        ds->Retrieve();
        input.civil_status->SetValues(ds, "label", "id");
    }

    input.status->Append("Active", new wxStringClientData("1"));
    input.status->Append("Inactive", new wxStringClientData("0"));

    // Set data exchange

#ifdef _DEBUG
    input.code->SetValidator(current["employee_id"]);
#endif
    input.firstname->SetValidator(current["firstname"]);
    input.middlename->SetValidator(current["middlename"]);
    input.lastname->SetValidator(current["lastname"]);
    input.sex->SetValidator(current["sex"]);
    input.dob->SetValidator(current["birthday"]);
    input.pob->SetValidator(current["birthplace"]);
    input.civil_status->SetValidator(current["civil_status_id"]);

    input.driver_license->SetValidator(current["driver_license"]);
    input.sss_id->SetValidator(current["sss_id"]);
    input.pagibig_id->SetValidator(current["pagibig_id"]);
    input.philhealth_id->SetValidator(current["philhealth_id"]);
    input.tax_id->SetValidator(current["tax_id"]);
    input.passport_id->SetValidator(current["passport_id"]);

    input.employment_date->SetValidator(current["employment_date"]);
    input.employee_number->SetValidator(current["employee_number"]);
    input.status->SetValidator(current["status"]);

#ifdef _DEBUG
    auto a = input.sex->GetValue(0);
    auto b = input.civil_status->GetValue(0);
#endif

    // Set default values for columns

    auto *ds = GetDataStore();
    poco::data::Date pdt;
    n::DateTime::Convert(wxDateTime::Now(), pdt);

    ds->default["sex"] = input.sex->GetValue(0);
    ds->default["birthday"] = pdt;
    ds->default["civil_status_id"] = input.civil_status->GetValue(0);
    ds->default["employment_date"] = pdt;
    ds->default["status"] = input.status->GetValue(0);

#ifdef _DEBUG
    auto *dsdebug = GetDataStore();
    for (auto &column : dsdebug->column) {
        n::Output::Send("%1: %2", column.name,
            n::data::Column::GetTypeName(column.type));
    }
    n::Output::Send();
#endif
}

#ifdef _DEBUG

bool Employee::OnRead(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = n::gui::data::input::Single::OnRead(e);

    auto *dsdebug = GetDataStore();
    for (auto &column : dsdebug->column) {
#if 0
        n::Log::Debug("%1 %2: %3", column.name,
            n::data::Column::GetTypeName(column.type),
            (*dsdebug).original.IfNull(0, column.name, "").convert<std::string>());
#endif
        n::Output::Send("%1: %2: %3", column.name,
            n::data::Column::GetTypeName(column.type),
            (*dsdebug).original.IfNull(0, column.name, "[null]").convert<std::string>());
    }
    n::Output::Send();
    return status;
}

bool Employee::OnRefresh(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = false;
    if (operation == Operation::Edit) {
        auto *ds = GetDataStore();
        ds->modified.SetRow(0, current);
        const type_info &v1 = ds->modified(0, "civil_status_id").type();
        const type_info &v2 = ds->original(0, "civil_status_id").type();
        status = true;
    }
    return n::gui::data::input::Single::OnRefresh(e);
}

bool Employee::OnUpdate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = false;
    if (operation == Operation::Edit) {
        auto *ds = GetDataStore();
        ds->modified.SetRow(0, current);
        const type_info &v1 = ds->modified(0, "civil_status_id").type();
        const type_info &v2 = ds->original(0, "civil_status_id").type();
        status = true;
    }
    return n::gui::data::input::Single::OnUpdate(e);
}
#endif
