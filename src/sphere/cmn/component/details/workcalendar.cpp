#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/debug.h>
#include <negerns/core/message.h>
#include <negerns/core/datetime.h>
#include <negerns/core/configuration.h>
#include <negerns/gui/sizer/box.h>
#include <sphere/cmn/datetime.h>
#include <sphere/cmn/component/details/workcalendar.h>

using namespace sphere::common::component::details;

WorkCalendar::WorkCalendar() :
    n::gui::data::input::Import()
{
    LOG_FUNCTION();
    Component::SetName("Work Calendar Holidays");
    Identifier::set("work_calendar_holidays");

    subject.Set("Holiday Calendar");

    SetParameters({0});
}

WorkCalendar::~WorkCalendar()
{
    LOG_FUNCTION();
    wxDELETE(flexgrid);
    wxDELETE(input.list);
}

void WorkCalendar::CreateControl()
{
    LOG_FUNCTION();
    n::gui::data::input::Import::CreateControl();

    auto *panel = Component::GetPanel();
    ASSERT(panel != nullptr, "Parent should not be null.");

    auto panelBase = new n::gui::Panel(panel);

    auto *boxSizer = new n::gui::sizer::Box(panelBase, n::Orientation::Vertical);

    auto *message = new n::gui::StaticText(panelBase, "Create a new work calendar.");

    const int columns = 1;
    const wxSize gap(5, 3);
    const std::size_t growableColumn = 0;
    const wxSize shortSize(100, wxDefaultCoord);

    flexgrid = new n::gui::sizer::FlexGrid(columns, gap, panelBase);
    flexgrid->AddGrowableCol(growableColumn);

    flexgrid->AddSpacer(10);
    flexgrid->AddStaticText("The work calendar will be generated for the given year.");
    input.year = flexgrid->AddSpin("", shortSize);
    input.year->SetRange(2014, DateTime::GetYear() + 1);
    flexgrid->AddSpacer(5);
    flexgrid->AddStaticText("The following holidays will be included in the "
        "work calendar.")->Wrap(470);

    input.list = new n::gui::ListView();
    input.list->SetSize(wxSize(450, 300));
    input.list->SetSingleSelection();
    input.list->SetRowDisplayEventFunction(LV_ROWDISPLAY_EVENT_FUNC(&WorkCalendar::OnRowDisplay, this));
    input.monthColumn = input.list->AppendChoiceIndexColumn(1, "Month", 70, n::Alignment::Left, n::gui::ListView::CellMode::Editable);
    input.monthColumn->GetRenderer()->EnableEllipsize(wxELLIPSIZE_NONE);
    input.list->AppendTextColumn(2, "Day", 40, n::Alignment::Center);
    input.list->AppendTextColumn(3, "Name", 140);
    input.list->AppendTextColumn(5, "Type", 160);
    
    input.list->SetParent(flexgrid->GetParent());
    input.list->CreateControl();

    // NOTE:
    // Bind event handlers only after the model has been set.
    // Otherwise it crashes the application and has caused headaches.

    input.list->Bind(wxEVT_DATAVIEW_ITEM_ACTIVATED, &WorkCalendar::OnSelectionActivated, this);

    flexgrid->AddComponent(input.list);
    SetFirstFocus(input.year);

    boxSizer->Add(message, n::Proportion::Static);
    boxSizer->Add(flexgrid, n::Proportion::Dynamic, wxEXPAND | wxLEFT);

    panelBase->AddInStaticBox(boxSizer, n::Orientation::Horizontal, n::Proportion::Dynamic, wxEXPAND);
    Component::Add(panelBase, n::Proportion::Dynamic, wxEXPAND | wxLEFT | wxRIGHT | wxBOTTOM, 8);
}

bool WorkCalendar::OnInitContent()
{
    LOG_FUNCTION();
    n::gui::data::input::Import::OnInitContent();
    auto *ds = GetDataStore();
    ds->sqlSelect.Set("select id, month, day, name, type, day_type_name from holiday_calendar");
    ds->sqlInsert.Set("select work_calendar_holiday_insert((?, ?, ?, ?, ?))");
    return true;
}

void WorkCalendar::PostInitContent()
{
    LOG_FUNCTION();
    n::gui::data::input::Import::PostInitContent();

    auto *renderer = static_cast<n::gui::renderer::Choice *>(input.monthColumn->GetRenderer());
    auto names = DateTime::GetMonthNames();
    renderer->SetValues(names);

    // TODO: Year default = Query the last year used in the database and add 1.
}

bool WorkCalendar::OnCreate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = n::gui::data::input::Import::OnCreate(e);
    if (status) {
        auto *ds = GetDataStore();
        input.list->SetModelData(ds, n::gui::ListView::DataSource::Inserted);
    }
    return status;
}

bool WorkCalendar::OnRead(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = n::gui::data::input::Import::OnRead(e);
    if (status) {
        auto *ds = GetDataStore();
        input.list->SetModelData(ds, n::gui::ListView::DataSource::Modified);
    }
    return status;
}

bool WorkCalendar::OnRefresh(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = n::gui::data::input::Import::OnRefresh(e);
    if (status) {
        auto *ds = GetDataStore();
        input.list->SetModelData(ds, n::gui::ListView::DataSource::Inserted);
    }
    return status;
}

bool WorkCalendar::PreUpdate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = n::gui::data::input::Import::PreUpdate(e);
    if (status) {

        // Copy all rows to a temporary DataStore that reflects the columns to
        // be updated. Replace the original rows with rows from the temporary
        // DatatStore.

        int year = input.year->GetValue();

        auto *ds = GetDataStore();
        auto count = ds->RowCount();

        auto *inputds = CreateDataStore();
        inputds->sqlSelect.Set("select year, id, month, day, type from input.holiday_calendar");
        inputds->sqlInsert.Set("select work_calendar_holiday_insert((?, ?, ?, ?, ?))");
        inputds->Retrieve();
        inputds->SetDefaults();
        inputds->InsertRows(count);
        count = inputds->inserted.RowCount();

        for (std::size_t i = 0; i < count; ++i) {
            inputds->inserted(i, "year") = year;
            inputds->inserted(i, "id") = ds->inserted(i, "id");
            inputds->inserted(i, "month") = ds->inserted(i, "month");
            inputds->inserted(i, "day") = ds->inserted(i, "day");
            inputds->inserted(i, "type") = ds->inserted(i, "type");
        }

        ds->DiscardChanges();
        ds->inserted = inputds->inserted;

#ifdef _DEBUG
        auto config = n::System::Configuration();
        if (config->get_bool("component.details.workcalendar.data.show", false)) {
            n::Log::Debug("Input DataStore inserted rows:");
            for (std::size_t i = 0; i < count; ++i) {
                inputds->inserted.LogRowValues(i);
            }
            n::Log::Debug("Default DataStore inserted rows:");
            for (std::size_t i = 0; i < count; ++i) {
                ds->inserted.LogRowValues(i);
            }
        }
#endif
    }
    return status;
}

bool WorkCalendar::OnUpdate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = n::gui::data::input::Import::OnUpdate(e);
    if (status) {
        if (operation == Operation::Create) {
            auto *ds = GetDataStore();
            auto count = ds->Insert(false);
            input.list->SetModelData(nullptr);

            ds->sqlSelect.Set("select id, extract(month from wc.when)::smallint, extract(day from wc.when)::smallint, holiday_name, day_type_id, day_type_name from public.work_calendar wc where extract(year from wc.when) = ?");
            ds->sqlUpdate.Set("select work_calendar_holiday_update((?, ?, ?, ?, ?))");

            n::Variant variant = input.year->GetValue();
            SetParameters({variant});

            n::Message::ErrorIf(count == 0, "There was an error creating new item.");
            status = count > 0;
            operation = Operation::Edit;
        } else {
            n::Message::Error("Unknown mode.");
            status = false;
        }
    }
    return status;
}

bool WorkCalendar::PostUpdate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = n::gui::data::input::Import::PostUpdate(e);
    if (status) {
        ICrud::Read();
    }
    return status;
}

bool WorkCalendar::OnRowDisplay(std::size_t row, std::size_t column, wxVariant &variant)
{
    return true;
}

void WorkCalendar::OnSelectionActivated(wxDataViewEvent &e)
{
    LOG_FUNCTION();
    auto item = e.GetItem();
    if (item.IsOk()) {
        wxDataViewColumn *column = e.GetDataViewColumn();
        input.list->EditItem(item, column);
    }
    e.Skip();
}

#ifdef _DEBUG
void WorkCalendar::SetTestValues()
{
    auto *config = n::System::Configuration();
    if (config->get_bool("component.details.workcalendar.data.test", false)) {
        std::vector<unsigned int> rows = { 1, 2, 3, 4, 11, 12 };
        std::vector<n::Month> months = {
            n::Month::February,
            n::Month::February,
            n::Month::February,
            n::Month::February,
            n::Month::November,
            n::Month::December
        };
        std::vector<unsigned int> days = { 1, 2, 3, 4, 11, 12 };
        auto *ds = GetDataStore();
        for(std::size_t i = 0; i < months.size(); ++i) {
            ds->inserted(rows[i], "month") = static_cast<int>(months[i]);
            ds->inserted(rows[i], "day") = days[i];
        }
    }
}
#endif
