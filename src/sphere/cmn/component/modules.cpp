#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/debug.h>
#include <negerns/core/message.h>
#include <negerns/gui/artprovider.h>
#include <sphere/cmn/artprovider.h>
#include <sphere/cmn/system.h>
#include <sphere/cmn/component/modules.h>

#include <sphere/cmn/component/list/employee.h>
#include <sphere/cmn/component/list/workcalendar.h>
#include <sphere/cmn/component/list/holidaycalendar.h>

using namespace sphere::common::component;

ModuleModel::ModuleModel() :
    module(new Module())
{
    Populate();
}

ModuleModel::~ModuleModel()
{
    wxDELETE(root);
    wxDELETE(system);
    wxDELETE(humanResource);
    wxDELETE(accounting);
}

unsigned int ModuleModel::GetID(const wxDataViewItem &item)
{
    unsigned int id = wxNOT_FOUND;
    if (item.IsOk()) {
        ModuleNode *node = (ModuleNode*) item.GetID();
        if (node) {
            id = node->id;
        }
    }
    return id;
}

std::string ModuleModel::GetTitle(const wxDataViewItem &item)
{
    std::string title;
    if (item.IsOk()) {
        ModuleNode *node = (ModuleNode*) item.GetID();
        if (node) {
            title = node->title;
        }
    }
    return title;
}

int ModuleModel::Compare(const wxDataViewItem &item1,
    const wxDataViewItem &item2,
    unsigned int column,
    bool ascending ) const
{
    FAIL(item1.IsOk() && item2.IsOk());
    // should never happen

    if (IsContainer(item1) && IsContainer(item2))
    {
        wxVariant value1, value2;
        GetValue(value1, item1, 0);
        GetValue(value2, item2, 0);

        wxString str1 = value1.GetString();
        wxString str2 = value2.GetString();
        int res = str1.Cmp(str2);
        if (res) return res;

        // items must be different
        wxUIntPtr litem1 = (wxUIntPtr) item1.GetID();
        wxUIntPtr litem2 = (wxUIntPtr) item2.GetID();

        return litem1-litem2;
    }

    return wxDataViewModel::Compare(item1, item2, column, ascending);
}

void ModuleModel::GetValue(wxVariant &variant,
    const wxDataViewItem &item,
    unsigned int col) const
{
    ASSERT(item.IsOk());
    ModuleNode *node = (ModuleNode*) item.GetID();
    switch (col) {
    case 0:
        variant = node->title;
        break;
    case 1:
        variant = (long) node->id;
        break;
    default:
        negerns::Log::Debug("ModuleModel::GetValue: wrong column %1", col);
    }
}

bool ModuleModel::SetValue(const wxVariant &variant,
    const wxDataViewItem &item,
    unsigned int col)
{
    ASSERT(item.IsOk());
    ModuleNode *node = (ModuleNode*) item.GetID();
    switch (col) {
        case 0:
            node->title = variant.GetString();
            return true;
        case 1:
            node->id = variant.GetLong();
            return true;
        default:
            negerns::Log::Debug("ModuleModel::SetValue: wrong column %1", col);
    }
    return false;
}

bool ModuleModel::IsEnabled(const wxDataViewItem &item, unsigned int col) const
{
    ASSERT(item.IsOk());
    auto *node = (ModuleNode*) item.GetID();
    return node->IsEnabled();
}

wxDataViewItem ModuleModel::GetParent( const wxDataViewItem &item ) const
{
    // the invisible root node has no parent
    if (!item.IsOk()) {
        return wxDataViewItem(0);
    }

    ModuleNode *node = (ModuleNode*) item.GetID();
    if (node == root) {
        return wxDataViewItem(0);
    }
    return wxDataViewItem((void*) node->GetParent());
}

bool ModuleModel::IsContainer(const wxDataViewItem &item) const
{
    // the invisble root node can have children
    // (in our model always "MyMusic")
    if (!item.IsOk()) {
        return true;
    }

    ModuleNode *node = (ModuleNode*) item.GetID();
    return node->IsContainer();
}

unsigned int ModuleModel::GetChildren(const wxDataViewItem &parent,
    wxDataViewItemArray &array) const
{
    unsigned int count = 0;

    ModuleNode *node = (ModuleNode*) parent.GetID();
    if (!node) {
        array.Add(wxDataViewItem((void*) root));
        count = 1;
    } else {
        if (node->GetCount() == 0) {
            array.clear();
            count = 0;
        } else {
            sphere::common::Modules modules = node->Get();
            count = modules.size();
            ModuleNode *child = nullptr;
            for (auto &m : modules) {
                array.Add(wxDataViewItem((void*) m));
            }
        }
    }
    return count;
}

void ModuleModel::Populate()
{
    PrePopulate();
    root = module->InitContent();
}

void ModuleModel::PrePopulate() { }



Modules::Modules() :
    Component(),
    control(nullptr)
{
    LOG_FUNCTION();
    Component::SetName("Modules");

    auto *tb = new n::gui::bar::ToolBar();
    tb->SetBorder(n::gui::bar::Bar::Border::Bottom);
    SetToolBar(tb);
}

Modules::~Modules() { }

void Modules::CreateControl()
{
    LOG_FUNCTION();
    Component::CreateControl();

    auto tb = GetToolBar();
    tb->AddTool(wxID_OPEN, "Expand", ART_ICON_NEGERNS_EXPAND, "Expand all subitems");
    tb->AddTool(wxID_CLOSE, "Collapse", ART_ICON_NEGERNS_COLLAPSE, "Collapse all subitems");
    tb->Realize();

    tb->Bind(wxEVT_TOOL, &Modules::ExpandItems, this, wxID_OPEN);
    tb->Bind(wxEVT_TOOL, &Modules::CollapseItems, this, wxID_CLOSE);

    const int icon_width = 16;
    const int icon_height = 16;

    wxImageList* images = new wxImageList(icon_width, icon_height);
    images->Add(wxArtProvider::GetIcon(ART_ICON_FOLDER));

    const int style = wxDV_SINGLE | wxDV_NO_HEADER | wxNO_BORDER;
    const int child_item_indention = 18;
    wxWindow* window = (wxWindow*) Component::GetPanel();
    control = new wxDataViewCtrl(window, wxID_ANY, wxDefaultPosition,
        wxDefaultSize, style);
    control->SetIndent(child_item_indention);
    control->SetRowHeight(18);
    column = control->AppendTextColumn("Module", 0, wxDATAVIEW_CELL_INERT, 200,
        wxALIGN_LEFT, wxCOL_RESIZABLE);
#if 0
    control->AppendTextColumn("ID", 1, wxDATAVIEW_CELL_INERT, 50,
        wxALIGN_LEFT, wxCOL_RESIZABLE);
#endif
    model = new ModuleModel();
    control->AssociateModel(model.get());

    control->Bind(wxEVT_SIZE, &Modules::OnSize, this);
    // Mouse events are captured in the wxDataViewCtrl client area
    auto *client= control->GetMainWindow();
    client->Bind(wxEVT_LEFT_UP, &Modules::OnMouse, this);
    control->Bind(wxEVT_DATAVIEW_ITEM_EXPANDED, &Modules::OnSelectionExpanded, this);
    control->Bind(wxEVT_DATAVIEW_ITEM_COLLAPSED, &Modules::OnSelectionCollapsed, this);
    control->Bind(wxEVT_DATAVIEW_SELECTION_CHANGED, &Modules::OnSelectionChanged, this);
    control->Bind(wxEVT_DATAVIEW_ITEM_ACTIVATED, &Modules::OnSelectionActivated, this);

    Component::Add(control, n::Proportion::Changeable, wxGROW | wxALL);
    Component::AddControl(control);

    control->Select(model->GetRoot());

    RefreshContent();
}

void Modules::RefreshContent()
{
}

void Modules::ExpandItems(wxCommandEvent &e)
{
    auto item = control->GetCurrentItem();
    if (item.IsOk() && model->IsContainer(item)) {
        control->Expand(item);
    }
}

void Modules::CollapseItems(wxCommandEvent &e)
{
    auto item = control->GetCurrentItem();
    if (item.IsOk() && model->IsContainer(item)) {
        control->Collapse(item);
    }
}

void Modules::OnSize(wxSizeEvent &e)
{
    column->SetWidth(e.GetSize().GetWidth());
    e.Skip();
}

void Modules::OnMouse(wxMouseEvent &e)
{
#if 0
    wxDataViewItem item;
    wxDataViewColumn *col;
    control->HitTest(e.GetPosition(), item, col);
    bool state = item.IsOk();
    auto *tb = GetToolBar();
    tb->EnableTool(wxID_OPEN, state);
    tb->EnableTool(wxID_CLOSE, state);
#endif
    e.Skip();
}

void Modules::OnSelectionExpanded(wxDataViewEvent &e)
{
    auto item = e.GetItem();
    auto *tb = GetToolBar();
    tb->EnableTool(wxID_OPEN, false);
    tb->EnableTool(wxID_CLOSE, true);
}

void Modules::OnSelectionCollapsed(wxDataViewEvent &e)
{
    auto item = e.GetItem();
    auto *tb = GetToolBar();
    tb->EnableTool(wxID_OPEN, true);
    tb->EnableTool(wxID_CLOSE, false);
}

void Modules::OnSelectionChanged(wxDataViewEvent &e)
{
    auto item = e.GetItem();
    auto *tb = GetToolBar();
    if (item.IsOk() && model->IsContainer(item)) {
        bool state = control->IsExpanded(item);
        tb->EnableTool(wxID_OPEN, !state);
        tb->EnableTool(wxID_CLOSE, state);
    } else {
        tb->EnableTool(wxID_OPEN, false);
        tb->EnableTool(wxID_CLOSE, false);
    }
}

void Modules::OnSelectionActivated(wxDataViewEvent &e)
{
    auto item = e.GetItem();
    if (item.IsOk()) {
        if (model->IsContainer(item)) {
            if (control->IsExpanded(item)) {
                control->Collapse(item);
            } else {
                control->Expand(item);
            }
            auto *tb = GetToolBar();
            bool state = control->IsExpanded(item);
            tb->EnableTool(wxID_OPEN, !state);
            tb->EnableTool(wxID_CLOSE, state);
            //e.Skip();
        } else {
            wxVariant var;
            unsigned int id = model->GetID(item);
            n::gui::data::List *list = nullptr;
            switch (id) {
                case Module::HumanResource::Employee:
                    list = new list::Employee();
                    break;
                case Module::HumanResource::WorkCalendar:
                    list = new list::WorkCalendar();
                    break;
                case Module::HumanResource::HolidayCalendar:
                    list = new list::HolidayCalendar();
                    break;
                default:
                    n::Message::Error(n::Format("Item activated is invalid with ID value %1", id));
            }
            if (list) {
                System::Pane::AddPage(System::Pane::Main, list);
            }
        }
    }
}
