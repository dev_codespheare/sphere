#include <negerns/gui/manager/pane.h>
#include <sphere/cmn/system.h>

using namespace sphere::common;

SPHERE_COMMON_DECL std::size_t sphere::common::System::Pane::Navigation = 0;
SPHERE_COMMON_DECL std::size_t sphere::common::System::Pane::Results = 0;
SPHERE_COMMON_DECL std::size_t sphere::common::System::Pane::Main = 0;

void sphere::common::System::Pane::AddPage(std::size_t index, n::gui::Component *c,
    bool activate)
{
    auto *genericPane = negerns::System::Pane(index);
    if (genericPane != nullptr) {
        if (index == System::Pane::Navigation) {
            auto *specificPane = static_cast<pane::Navigation *>(genericPane);
            c->SetPane(specificPane);
            negerns::gui::manager::Pane::AddPage(specificPane, c, activate);
        } else if (index == System::Pane::Results) {
            auto *specificPane = static_cast<pane::Results *>(genericPane);
            c->SetPane(specificPane);
            negerns::gui::manager::Pane::AddPage(specificPane, c, activate);
        } else if (index == System::Pane::Main) {
            auto *specificPane = static_cast<pane::Main *>(genericPane);
            c->SetPane(specificPane);
            negerns::gui::manager::Pane::AddPage(specificPane, c, activate);
        }
    }
}

void sphere::common::System::Pane::SetPage(std::size_t paneIndex, std::size_t paneTabIndex)
{
    auto *genericPane = negerns::System::Pane(paneIndex);
    if (genericPane != nullptr) {
        if (paneIndex == System::Pane::Navigation) {
            auto *specificPane = static_cast<pane::Navigation *>(genericPane);
            specificPane->SetActivePage(paneTabIndex);
        } else if (paneIndex == System::Pane::Results) {
            auto *specificPane = static_cast<pane::Results *>(genericPane);
            specificPane->SetActivePage(paneTabIndex);
        } else if (paneIndex == System::Pane::Main) {
            auto *specificPane = static_cast<pane::Main *>(genericPane);
            specificPane->SetActivePage(paneTabIndex);
        }
    }
}

sphere::common::pane::Navigation* sphere::common::System::Pane::GetNavigation()
{
    auto id = System::Pane::Navigation;
    return static_cast<pane::Navigation *>(negerns::System::Pane(id));
}

sphere::common::pane::Main* sphere::common::System::Pane::GetMain()
{
    auto id = System::Pane::Main;
    return static_cast<pane::Main *>(negerns::System::Pane(id));
}

sphere::common::pane::Results* sphere::common::System::Pane::GetResults()
{
    auto id = System::Pane::Results;
    return static_cast<pane::Results *>(negerns::System::Pane(id));
}
