#include <string>
#include <wx/xrc/xmlres.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/button.h>
#include <wx/msgdlg.h>
#include <wx/dialog.h>
#include <negerns/core/log.h>
#include <negerns/core/message.h>
#include <negerns/core/system.h>
#include <negerns/gui/aboutinfo.h>
#include <negerns/gui/app.h>
#include <negerns/gui/resource.h>
#include <sphere/cmn/dialog/about.h>
#include <sphere/cmn/def.h>

using namespace sphere::common::dialog;

About::About() :
    Dialog()
{
    bool status = wxXmlResource::Get()->LoadDialog(this, NULL, "AboutDialog");
    if (!status) {
        n::Message::Error("Failed to load the About dialog.");
    }
}

About::~About() { }

void About::CreateControl()
{
    LOG_FUNCTION();
    wxStaticText* product = XRCCTRL(*this, "product", wxStaticText);
    wxStaticText* edition = XRCCTRL(*this, "edition", wxStaticText);
    wxStaticText* copyright = XRCCTRL(*this, "copyright", wxStaticText);
    wxStaticText* website = XRCCTRL(*this, "website", wxStaticText);
    wxStaticText* license = XRCCTRL(*this, "license", wxStaticText);
    wxStaticText* licensee = XRCCTRL(*this, "registeredTo", wxStaticText);
    wxListCtrl* modules = XRCCTRL(*this, "modules", wxListCtrl);
    wxButton* okButton = XRCCTRL(*this, "wxID_OK", wxButton);

    modules->Bind(wxEVT_LIST_KEY_DOWN, &About::OnKey, this);

    auto app = n::System::Application();
    n::gui::AboutInfo about = app->GetAboutInfo();
    product->SetLabel(about.GetName());
    edition->SetLabel(about.GetBuildInfo());
    copyright->SetLabel(about.GetCopyright());
    website->SetLabel(about.GetWebSiteURL());
    licensee->SetLabel(about.GetLicensee());

    // Resize the window so the contents fit well
    if (wxDialog::GetSizer()) {
        wxDialog::GetSizer()->Fit(this);
    }
}

void About::OnKey(wxListEvent& e)
{
    if (e.GetKeyCode() == WXK_RETURN) {
        this->Close();
    }
}
