#include <wx/xrc/xmlres.h>
#include <negerns/gui/resource.h>
#include <sphere/cmn/dialog/dialog.h>

using namespace sphere::common::dialog;

Dialog::Dialog() :
    n::gui::DialogBase()
{ }

Dialog::Dialog(wxWindow* parent,
    wxWindowID id,
    const wxString& title,
    const wxPoint& pos,
    const wxSize& size,
    long style) :
    n::gui::DialogBase(parent, id, title, pos, size, style)
{ }

Dialog::~Dialog() { }