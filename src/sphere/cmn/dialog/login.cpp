#include <string>
#include <wx/xrc/xmlres.h>
#include <wx/validate.h>
#include <wx/valtext.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/msgdlg.h>
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <negerns/core/defs.h>
#include <negerns/core/log.h>
#include <negerns/gui/resource.h>
#include <sphere/cmn/dialog/login.h>
#include <sphere/cmn/def.h>

using namespace sphere::common::dialog;

LogIn::LogIn(wxWindow* parent,
    wxWindowID id,
    const wxString& title,
    const wxPoint& pos,
    const wxSize& size,
    long style) :
    LogInDialogBase(parent, id, title, pos, size, style)
{ }

LogIn::~LogIn() { }

void LogIn::CreateControl()
{
    LOG_FUNCTION();
    LogInDialogBase::CreateControl();

    wxBoxSizer* boxSizer = new wxBoxSizer(wxVERTICAL);
    wxStaticBoxSizer* topStaticBox = new wxStaticBoxSizer(wxHORIZONTAL, LogInDialogBase::GetPanel());
    wxStaticBoxSizer* bottomStaticBox = new wxStaticBoxSizer(wxHORIZONTAL, LogInDialogBase::GetPanel());
    wxBoxSizer* topInner = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* bottomInner = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* buttonSizer = new wxBoxSizer(wxHORIZONTAL);

    wxStaticText* appname = new wxStaticText(topStaticBox->GetStaticBox(), wxID_ANY, "Accounting Information System");
    wxStaticText* version = new wxStaticText(topStaticBox->GetStaticBox(), wxID_ANY, "Version 0.1.0");
    wxStaticText* copyright = new wxStaticText(topStaticBox->GetStaticBox(), wxID_ANY, "Copyright 2013 Ricky Maicle");
    wxStaticText* note = new wxStaticText(bottomStaticBox->GetStaticBox(), wxID_ANY, "Please provide your security credentials to log into the system. If you are unable to log into the system, please consult your system administrator.");
    note->Wrap(320);
    wxStaticText* username = new wxStaticText(bottomStaticBox->GetStaticBox(), wxID_ANY, "Username:");
    int usernameInputStyle = 0; // wxTE_PROCESS_ENTER
    wxTextCtrl* usernameInput = new wxTextCtrl(bottomStaticBox->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, usernameInputStyle, wxTextValidator(wxFILTER_EMPTY, GetUserPtr()));
    usernameInput->SetToolTip("Username identifies you to the system.\nUsername cannot be empty.");
    wxStaticText* password = new wxStaticText(bottomStaticBox->GetStaticBox(), wxID_ANY, "Password:");
    int passwordInputStyle = wxTE_PASSWORD;
    wxTextCtrl* passwordInput = new wxTextCtrl(bottomStaticBox->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, passwordInputStyle, wxTextValidator(wxFILTER_EMPTY, GetPasswordPtr()));
    passwordInput->SetToolTip("Username is authenticated by a password.\nPassword cannot be empty.");
    wxButton* okButton = new wxButton(LogInDialogBase::GetPanel(), wxID_OK, "L&og In");
    wxButton* cancelButton = new wxButton(LogInDialogBase::GetPanel(), wxID_CANCEL, "Cancel");

    SetAffirmativeId(okButton->GetId());
    SetEscapeId(cancelButton->GetId());

    const int notchangeable = 0;
    const int changeable = 1;

    topInner->Add(appname, notchangeable, wxGROW | wxALL);
    topInner->Add(version, notchangeable, wxGROW | wxALL);
    topInner->Add(copyright, notchangeable, wxGROW | wxALL);

    bottomInner->Add(note, notchangeable, wxGROW | wxLEFT, 0);
    bottomInner->AddSpacer(15);
    bottomInner->Add(username, notchangeable, wxGROW | wxLEFT, 40);
    bottomInner->AddSpacer(2);
    bottomInner->Add(usernameInput, notchangeable, wxGROW | wxLEFT | wxRIGHT, 40);
    bottomInner->AddSpacer(5);
    bottomInner->Add(password, notchangeable, wxGROW | wxLEFT, 40);
    bottomInner->AddSpacer(2);
    bottomInner->Add(passwordInput, notchangeable, wxGROW | wxLEFT | wxRIGHT, 40);
    bottomInner->AddSpacer(15);

    buttonSizer->Add(okButton, notchangeable, wxGROW | wxTOP | wxLEFT, 5);
    buttonSizer->Add(cancelButton, notchangeable, wxGROW | wxTOP | wxLEFT, 5);

    topStaticBox->Add(topInner, notchangeable, wxALIGN_TOP | wxGROW | wxALL, 5);
    bottomStaticBox->Add(bottomInner, notchangeable, wxGROW | wxALL, 5);

    boxSizer->Add(topStaticBox, notchangeable, wxGROW | wxLEFT | wxRIGHT, 5);
    boxSizer->Add(bottomStaticBox, notchangeable, wxGROW | wxLEFT | wxRIGHT, 5);
    boxSizer->Add(buttonSizer, notchangeable, wxALL | wxALIGN_RIGHT, 5);

    Component::Add(boxSizer, n::Proportion::Changeable, wxGROW | wxALL, 5);
    Component::FitContent(Dialog::GetThis());
    Component::Layout();
    Centre();

    bool b = wxDialog::TransferDataToWindow();
}
