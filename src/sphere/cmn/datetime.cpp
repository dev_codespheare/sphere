#include <negerns/core/datetime.h>
#include <sphere/cmn/datetime.h>

#include <negerns/negerns.h>

using namespace sphere::common;

sphere::common::DateTime::DateTime() { }

sphere::common::DateTime::~DateTime() { }

std::size_t sphere::common::DateTime::GetYear()
{
    wxDateTime dt = wxDateTime::Now();
    return dt.GetYear();
}

std::vector<std::string> sphere::common::DateTime::GetMonthNames()
{
    std::vector<std::string> names { std::string() };

    auto months = n::DateTime::GetMonthNames(n::DateTime::Name::Full);
    for (auto &m : months) {
        names.push_back(m);
    }
    return names;
}
