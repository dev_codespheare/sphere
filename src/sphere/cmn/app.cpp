#include <wx/app.h>
#include <wx/cmdline.h>
#include <wx/bitmap.h>
#include <wx/xrc/xmlres.h>
#include <negerns/core/log.h>
#include <negerns/core/system.h>
#include <negerns/core/configuration.h>
#include <negerns/core/config/property.h>
#include <sphere/cmn/constants.h>
#include <sphere/cmn/version.h>
#include <sphere/cmn/app.h>
#include <sphere/cmn/mediator.h>

using namespace sphere::common;

const char * Application::configFile = "sphere.conf";
#ifdef _DEBUG
const char * Application::traceFile = "sphere_trace.conf";
#endif

static const wxCmdLineEntryDesc cmdLineDesc[] =
{
    { wxCMD_LINE_SWITCH, "h", "help", "show this help message", wxCMD_LINE_VAL_NONE, wxCMD_LINE_OPTION_HELP },
    { wxCMD_LINE_SWITCH, "v", "version", "show version information", wxCMD_LINE_VAL_NONE, wxCMD_LINE_PARAM_OPTIONAL },
    { wxCMD_LINE_NONE }
};

Application::Application() :
    n::gui::Application(),
    mediator(nullptr)
{ }

bool Application::OnInit(void)
{
    // Allow command line processing provided by wxWidgets
    if (!n::gui::Application::OnInit()) {
        return false;
    }
    return true;
}

int Application::OnExit(void)
{
    LOG_FUNCTION();
    auto *config = n::System::Configuration();
    config->save(Application::configFile);
    return n::gui::Application::OnExit();
}

void Application::OnFatalException()
{
    LOG_FUNCTION();
    auto *config = n::System::Configuration();
    config->save(Application::configFile);
    n::gui::Application::OnFatalException();
}

void Application::OnAbout(wxCommandEvent& e)
{
    n::gui::Application::OnAbout(e);
}

void Application::OnInitCmdLine(wxCmdLineParser& parser)
{
    n::gui::Application::OnInitCmdLine(parser);
    parser.SetDesc(cmdLineDesc);
}

void Application::InitConfiguration()
{
    LOG_FUNCTION();
    negerns::Log::Info("Sphere configuration initialization...");
    auto *config = n::System::Configuration();
    config->add(new negerns::config::Property(Application::configFile, true),
        Priority::Configuration);
    config->read(Application::configFile);
#ifdef _DEBUG
    negerns::Log::Debug("Sphere trace configuration initialization...");
    config->add(new negerns::config::Property(Application::traceFile), Priority::Trace);
    config->read(Application::traceFile);
#endif
    n::gui::Application::InitConfiguration();
}
