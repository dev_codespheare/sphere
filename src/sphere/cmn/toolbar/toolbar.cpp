#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/log.h>
#include <sphere/cmn/artprovider.h>
#include <sphere/cmn/toolbar/toolbar.h>

using namespace sphere::common::toolbar;

ToolBar::ToolBar() : negerns::gui::bar::ToolBar() { }

ToolBar::~ToolBar() { }

void ToolBar::CreateControl()
{
    LOG_FUNCTION();
    negerns::gui::bar::ToolBar::CreateControl();
    // TODO: Allow name to be set from concrete classes
    auto tb = ToolBar::GetControl();
#if 0
    tb->AddTool(wxID_ANY, "Groups", wxArtProvider::GetBitmap(ART_ICON_APP), "Test icon only");
    tb->AddSeparator();
#endif
    tb->Realize();
}
