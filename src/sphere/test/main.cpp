// Application Arguments
// ---------------------
// --build_info=value           Print build information
//                              value = [yes | no]
// --detect_memory_leaks = 0    Do not detect memory leaks
// --log_level=message          Display messages
//                              message = [all | success | test_suite | message |
//                                        warning | error | cpp_exception | system_error |
//                                        fatal_error | nothing]
// --log_sink=value             Where to send the log output
//                              value = [default | stderr | fileame]
// --run_test                   Filter which test units to execute
//
// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=message
// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=Suite_RowsetBase
// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=Suite_RowsetBase

// Compilation flags
// -----------------
// BOOST_TEST_DYN_LINK 	            Define this flag to build/use dynamic library.
// BOOST_TEST_NO_LIB 	            Define this flag to prevent auto-linking.
// BOOST_TEST_NO_MAIN 	            Define this flag to prevent function main()
//                                  implementation generation.
// BOOST_TEST_MAIN 	                Define this flag to generate an empty test module
//                                  initialization function and in case of dynamic
//                                  library variant default function main() implementation
//                                  as well.
// BOOST_TEST_MODULE 	            Define this flag to generate the test module initialization
//                                  function, which uses the defined value to name the master
//                                  test suite. In case of dynamic library variant default
//                                  function main() implementation is generated as well
// BOOST_TEST_ALTERNATIVE_INIT_API 	Define this flag to generate the switch to the alternative
//                                  test module initialization API. 

#define BOOST_TEST_MODULE Sphere Master Test Suite

#include <boost/test/unit_test.hpp>
#include <sphere/test/global.h>

using namespace boost::unit_test;
