#include <string>
#include <vector>
#include <map>

#include <Poco/Data/Date.h>
#include <negerns/data/dataaccess.h>
#include <negerns/data/datastore.h>
#include <negerns/core/datetime.h>

#include <negerns/negerns.h>
#include <negerns/poco.h>

#include <boost/test/unit_test.hpp>
#include <sphere/test/global.h>

using namespace boost::unit_test;

// sphere_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=Suite_DataAccess

BOOST_AUTO_TEST_SUITE(Suite_DataAccess)
BOOST_FIXTURE_TEST_SUITE(work_calendar, negerns_data)

BOOST_AUTO_TEST_CASE(select_date)
{
#if 0
    n::data::DataStore ds(da->GetTransaction());
    ds.SetSqlSelect("select id, 'when', day_type_id from public.work_calendar wc where wc.when = ?");

    const int year = 2014;
    wxDateTime date = wxDateTime::Now();
    date.ResetTime();
    date.SetYear(year);
    date.SetMonth(wxDateTime::Month(static_cast<int>(n::Month::February) - 1));
    date.SetDay(1);

    std::string tmp = date.FormatISODate().ToStdString();
    n::data::Parameters param { tmp };

    std::size_t rowcount = ds.Retrieve(param);
    BOOST_CHECK(rowcount > 0);
#endif
}

BOOST_AUTO_TEST_CASE(work_calendar_holiday_insert)
{
    n::data::DataStore ds(da->GetTransaction());
    ds.sqlSelect.Set("select id, month, day, type from public.holiday_calendar");
    ds.sqlInsert.Set("select work_calendar_holiday_insert((?, ?, ?, ?, ?))");
    std::size_t rowcount = ds.Retrieve();
    ds.SetDefaults();
    ds.InsertRows(0, rowcount);

    std::vector<int> rowindex { 1, 2, 3, 4, 11, 12 };

    std::vector<n::Month> months {
        n::Month::February,
        n::Month::February,
        n::Month::February,
        n::Month::February,
        n::Month::November,
        n::Month::December
    };
    std::vector<int> days { 1, 2, 3, 4, 11, 12 };
    const int year = 2014;

#if 0
    BOOST_MESSAGE("Inputs:");
    for (std::size_t i = 0; i < months.size(); ++i) {
        BOOST_MESSAGE("Month: " << n::to_string(static_cast<int>(months[i])));
        BOOST_MESSAGE("Day: " << n::to_string(days[i]));
    }
#endif

    BOOST_MESSAGE("Put into DataStore:");
    for (std::size_t i = 0; i < months.size(); ++i) {
        ds.inserted(rowindex[i], "month") = static_cast<int>(months[i]);
        ds.inserted(rowindex[i], "day") = days[i];

        BOOST_MESSAGE("Month: " << ds.inserted(rowindex[i], "month").convert<std::string>());
        BOOST_MESSAGE("Day: " << ds.inserted(rowindex[i], "day").convert<std::string>());
    }

#if 0
    BOOST_MESSAGE("Insert Rows contents:");
    for (std::size_t i = 0; i < rowcount; ++i) {
        BOOST_MESSAGE("Month: " << ds.inserted(i, "month").convert<std::string>());
        BOOST_MESSAGE("Day: " << ds.inserted(i, "day").convert<std::string>());
    }
#endif

    {
        n::data::DataStore input(da->GetTransaction());
        input.sqlSelect.Set("select year, id, month, day, type from input.holiday_calendar");
        input.sqlInsert.Set("select public.work_calendar_holiday_insert((?, ?, ?, ?, ?))");
        input.Retrieve();
        input.SetDefaults();
        input.InsertRows(rowcount);
        rowcount = input.inserted.RowCount();

        BOOST_MESSAGE("Move input to buffer");
        for (std::size_t i = 0; i < rowcount; ++i) {
            input.inserted(i, "year") = year;
            input.inserted(i, "id") = ds.inserted(i, "id");
            input.inserted(i, "month") = ds.inserted(i, "month");
            input.inserted(i, "day") = ds.inserted(i, "day");
            input.inserted(i, "type") = ds.inserted(i, "type");

            BOOST_MESSAGE("Month: " << input.inserted(i, "month").convert<std::string>());
            BOOST_MESSAGE("Day: " << input.inserted(i, "day").convert<std::string>());
        }

        ds.DiscardChanges();
        // Needs to set the SQL statement and issue a retrieve operation so the
        // columns will be the same with the input.
        ds.sqlSelect.Set("select year, id, month, day, type from input.holiday_calendar where id = 0");
        ds.Retrieve();
        ds.SetDefaults();
        ds.inserted = input.inserted;

#if 0
        BOOST_MESSAGE("New contents:");
        for (std::size_t i = 0; i < rowcount; ++i) {
            BOOST_MESSAGE("Month: " << ds.inserted(i, "month").convert<std::string>());
            BOOST_MESSAGE("Day: " << ds.inserted(i, "day").convert<std::string>());
        }
#endif

#if 0
        for (std::size_t i = 0; i < rowcount; ++i) {
            BOOST_MESSAGE(ds.inserted(i, "month").convert<std::string>());
            BOOST_MESSAGE(n::to_string(static_cast<int>(months[i])));
            BOOST_MESSAGE(ds.inserted(i, "day").convert<std::string>());
            BOOST_MESSAGE(n::to_string(days[i]));

            BOOST_CHECK(ds.inserted(i, "year") == year);
            BOOST_CHECK(ds.inserted(i, "id") == input.inserted(i, "id"));
            BOOST_CHECK(ds.inserted(i, "month") == static_cast<int>(months[i]));
            BOOST_CHECK(ds.inserted(i, "day") == days[i]);
            BOOST_CHECK(ds.inserted(i, "type") == input.inserted(i, "type"));
        }
#endif

        rowcount = ds.Insert();
        BOOST_CHECK(rowcount == input.inserted.RowCount());
    }

#if 0
    ds.SetSqlSelect("select id, 'when', day_type_id from public.work_calendar wc where wc.when = ?");

    wxDateTime date = wxDateTime::Now();
    date.ResetTime();
    date.SetYear(year);
    std::string tmp;
    for (std::size_t i = 0; i < months.size(); ++i) {
        date.SetDay(days[i]);
        date.SetMonth(wxDateTime::Month(static_cast<int>(months[i]) - 1));

        tmp = date.FormatISODate().ToStdString();
        n::data::Parameters param { tmp };

        rowcount = ds.Retrieve(param);
        BOOST_CHECK(rowcount == 1);
    }
#endif
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
