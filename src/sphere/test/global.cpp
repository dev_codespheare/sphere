#include <negerns/test/global.h>
#include <negerns/core/memory.h>

using namespace boost::unit_test;

int negerns_data::is_init = 0;

negerns_data::negerns_data()
{
#if 0
    // Initialize only once.
    // This is because this is being executed 3 times by the boost test framework.
    if (negerns_data::is_init > 0) return;
#endif

    negerns_data::is_init = 1;
#if 0
    BOOST_TEST_MESSAGE("negerns::data setup");
#endif

//#ifdef __WINDOWS__
//# ifdef _MSC_VER
    // Must check for 64-bit defines first because wxWidgets
    // also defines 32-bit symbols even when building 64-bit
#   if defined(__WIN64__) || defined(_WIN64)
    std::string dsn = "sphere-client-64U";
#   else
    std::string dsn = "sphere-client-32U";
#   endif
//# endif
//#endif

    n::data::DataSource ds;
    ds.SetConnector("ODBC");
    ds.SetDataSourceName(dsn);
    ds.SetUser("sphere");
    ds.SetPassword("sphere");
    ds.SetConnectionName("sphere ais");

#if 0
    BOOST_TEST_MESSAGE("Database connection information:");
    BOOST_TEST_MESSAGE("Connector: " << ds.GetConnector());
    BOOST_TEST_MESSAGE("DSN: " << ds.GetDataSourceName());
    BOOST_TEST_MESSAGE("User: " << ds.GetUser());
    BOOST_TEST_MESSAGE("Connection name: " << ds.GetConnectionName());
    BOOST_TEST_MESSAGE("Connectionstring: " << ds.Get());
#endif

    da = n::make_unique(new n::data::DataAccess());
    bool status = da->Test(ds);

    if (status) {
        da->Add(ds, true);
        da->Init();
    }
}

// Teardown
negerns_data::~negerns_data()
{
#if 0
    if (negerns_data::is_init == 0) return;
#endif
#if 0
    BOOST_TEST_MESSAGE("negerns::data teardown");
#endif
    da->UnInit();
    negerns_data::is_init = 0;
}
