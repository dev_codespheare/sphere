#include <wx/aboutdlg.h>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/system.h>
#include <negerns/core/message.h>
#include <negerns/gui/aboutinfo.h>
#include <negerns/gui/dialog/dialog.h>
#include <negerns/gui/framemanager.h>
#include <negerns/gui/mediator.h>
#include <negerns/gui/app.h>

namespace negerns {
namespace gui {

FrameManager::FrameManager() :
    Component(),
    manager::Pane(),
    frame(nullptr)
{ }

FrameManager::~FrameManager()
{
    wxDELETE(frame);
}

void FrameManager::CreateControl()
{
    LOG_FUNCTION();
    wxString appName(negerns::System::Application()->GetAppDisplayName());
    frame = new wxFrame(NULL, wxID_ANY, appName,
        wxDefaultPosition, wxSize(800, 600), wxDEFAULT_FRAME_STYLE);
    frame->SetSizer(new wxBoxSizer(wxVERTICAL));
    frame->CenterOnScreen();

    Component::SetParent(frame);
    Component::CreateControl();

    // Set the message dialog parent
    Message::SetParent(frame);

    // Set the dialog base parent
    Dialog::SetParent(frame);

    // The frame has its own sizer. We add the toolbar to the frame's sizer
    // instead of how the Component class does it.
    auto *sizer = frame->GetSizer();

    if (IsToolBarSet()) {
        auto *tb = Component::GetToolBar();
        tb->SetParent(frame);
        tb->CreateControl();
        sizer->Add(tb->GetControl(), Proportion::NotChangeable,
            wxGROW | wxALL, wxBORDER_DEFAULT);
    }

    auto panel = Component::GetPanel();
    sizer->Add(panel, Proportion::Changeable, wxGROW | wxALL, wxBORDER_DEFAULT);

    if (IsStatusBarSet()) {
        auto *sb = Component::GetStatusBar();
        sb->SetParent(frame);
        sb->CreateControl();
        sizer->Add(sb->GetControl(), Proportion::NotChangeable,
            wxGROW | wxALL, wxBORDER_DEFAULT);
    }

    manager::Pane::AuiComponent::SetManagedWindow(panel);
    manager::Pane::SetParent((wxWindow *) panel);
    manager::Pane::CreateControl();

    frame->Bind(wxEVT_CLOSE_WINDOW, &FrameManager::OnClose, this);
}

void FrameManager::PostCreateControl()
{
    LOG_FUNCTION();
    manager::Pane::PostCreateControl();
}

void FrameManager::DestroyControl()
{
    LOG_FUNCTION();
    manager::Pane::DestroyControl();
}

bool FrameManager::OnInitContent()
{
    LOG_FUNCTION();
    return manager::Pane::OnInitContent();
}

void FrameManager::ApplyConfiguration()
{
    LOG_FUNCTION();
    manager::Pane::ApplyConfiguration();
}

bool FrameManager::Show()
{
    ASSERT(this->frame, "Frame instance is null.");
    return frame->Show();
}

void FrameManager::OnQuit(wxCommandEvent& e)
{
    LOG_FUNCTION();
    frame->Close();
    // IMPORTANT:
    //
    // Do not call Skip otherwise, the application hangs.
#if 0
    e.Skip();
#endif
}

void FrameManager::OnClose(wxCloseEvent& e)
{
    LOG_FUNCTION();
    Component::GetMediatorInstance()->LogOut();
    DestroyControl();
    manager::Pane::AuiComponent::UnInit();
    // Proceed closing the frame
    e.Skip();
}

void FrameManager::OnAbout(wxCommandEvent&)
{
    AboutInfo aboutInfo;
    aboutInfo.SetName("Application");
    aboutInfo.SetVersion("Version 0.1.0");
    aboutInfo.SetDescription("Application description");
    aboutInfo.SetCopyright("(C) 2014");
    aboutInfo.SetWebSite("http://application.org");
    aboutInfo.AddDeveloper("Developer");
    //wxAboutBox(aboutInfo);
}

void FrameManager::OnToggleToolBar(wxCommandEvent &)
{
    if (IsToolBarSet()) {
        auto *tb = Component::GetToolBar();
        tb->Show(!tb->IsVisible());
        frame->GetSizer()->Layout();
    }
}

void FrameManager::OnToggleStatusBar(wxCommandEvent &)
{
    if (IsStatusBarSet()) {
        auto *sb = Component::GetStatusBar();
        sb->Show(!sb->IsVisible());
        frame->GetSizer()->Layout();
    }
}

void FrameManager::OnToggleStayOnTop(wxCommandEvent &e)
{
    bool state = e.IsChecked();
    frame->ToggleWindowStyle(wxSTAY_ON_TOP);
    //config->Set(Config::window_stay_on_top, state);
    //CheckMenuItem(Constants::ID_TOGGLE_STAY_ON_TOP, state);
}

void FrameManager::OnToggleFullscreen(wxCommandEvent&)
{
    LOG_FUNCTION();
    bool isFullScreen = !frame->IsFullScreen();
    frame->ShowFullScreen(isFullScreen,
        wxFULLSCREEN_NOTOOLBAR | wxFULLSCREEN_NOSTATUSBAR |
        wxFULLSCREEN_NOBORDER | wxFULLSCREEN_NOCAPTION);
#if 0
    if (isFullScreen) {
        // Uncheck status bar menu item when in fullscreen mode
        // because it will always hide the status bar.
        oldStatusBarState = frame->GetStatusBar()->IsShown();
    } else {
        // Make sure we restore the status bar to its original setting
        // Because fullscreen mode hides it and may have changed state
        // during fullscreen mode.
        CheckMenuItem(Constants::ID_TOGGLE_STATUS_BAR, oldStatusBarState);
    }
#endif
}

void FrameManager::ProcessToolBarClick(wxCommandEvent &e)
{
    Component::GetMediatorInstance()->ProcessToolBarClick(e);
}

wxFrame* FrameManager::GetFrame()
{
    return frame;
}

} //_ namespace gui
} //_ namespace negerns
