#include <negerns/core/log.h>
#include <negerns/core/message.h>
#include <negerns/gui/icontrol.h>

namespace negerns {
namespace gui {

wxVector<wxWindow*> IControl::controls;

void IControl::InitContent()
{
    if (PreInitContent()) {
        if (OnInitContent()) {
            PostInitContent();
        }
    }
}

bool IControl::PreInitContent()
{
    return true;
}

bool IControl::OnInitContent()
{
    return true;
}

void IControl::PostInitContent() { }

wxWindow* IControl::GetControl()
{
    Message::Info("Component::GetControl will return a NULL value.\n"
        "Implement this method in descendant class.");
    log::warning("Implement this method in descendant class.");
    return NULL;
}

void IControl::AddControl(wxWindow* w)
{
    if (w != NULL) {
        controls.push_back(w);
    }
}

wxWindow* IControl::FindControl(const std::string &name)
{
    wxWindow* window = NULL;
    if (controls.empty()) {
        return window;
    }
    int count = controls.size();
    for (int i = 0; i < count; ++i) {
        window = controls.at(i);
        if (window->GetName() == name) {
            break;
        }
    }
    return window;
}

} //_ namespace gui
} //_ namespace negerns
