#include <windows.h>
// Include winundef.h to avoid the warning C4002: too many actual parameters for macro 'Yield'
// because windows.h defines Yield.
#include <wx/msw/winundef.h>

#include <negerns/gui//shellfileinfo.h>

namespace negerns {
namespace gui {

HICON ShellFileInfo::GetFolderIconHandle(bool smallIcon)
{
    SHFILEINFO sfi;
    unsigned int flags = SHGFI_ICON | SHGFI_USEFILEATTRIBUTES | SHGFI_SMALLICON;

    SHGetFileInfo((WCHAR*)"Doesn\'t matter\0", FILE_ATTRIBUTE_DIRECTORY, &sfi, sizeof(SHFILEINFO), flags);
    return sfi.hIcon;
}

HICON ShellFileInfo::GetFileIconHandle(LPCTSTR filename)
{
    SHFILEINFO sfi;
    unsigned int flags = SHGFI_ICON | SHGFI_USEFILEATTRIBUTES | SHGFI_SMALLICON;
    SHGetFileInfo(filename, FILE_ATTRIBUTE_NORMAL, &sfi, sizeof(SHFILEINFO), flags);
    return sfi.hIcon;
}

wxString ShellFileInfo::GetFileType(const char* filename)
{
    SHFILEINFO sfi;
    SHGetFileInfo((WCHAR*)filename,
        FILE_ATTRIBUTE_NORMAL,
        &sfi,
        sizeof(SHFILEINFO),
        SHGFI_TYPENAME | SHGFI_USEFILEATTRIBUTES);

    return wxString(sfi.szTypeName);
}

} //_ namespace gui
} //_ namespace negerns
