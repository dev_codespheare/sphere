#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <wx/xrc/xmlres.h>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/gui/resource.h>
#include <negerns/gui/artprovider.h>

namespace negerns {
namespace gui {

ArtProvider::ArtProvider() :
    wxArtProvider(),
    resource(nullptr)
{ }

ArtProvider::~ArtProvider() { }

wxBitmap ArtProvider::CreateBitmap(const wxArtID& id, const wxArtClient& client, const wxSize& size)
{
    // Note:
    // wxAuiNotebook tab pages can display wxBitmap derived from icon files
    // but could not display wxBitmap derived from bitmap files.

    if (!resource) {
        resource = wxXmlResource::Get();
    }

    LOG_FUNCTION();
    wxBitmap bmp(wxNullBitmap);
    bool status = false;
    if (id == ART_ICON_NEGERNS) {
        status = bmp.CopyFromIcon(resource->LoadIcon("negerns_icon_negerns"));
    } else if (id == ART_ICON_NEGERNS_NONE) {
        ASSERT(false, "Should not execute this part.\nThis serves as a template only for acquring an\nicon resource linked with the application.");
        status = bmp.CopyFromIcon(wxIcon("negerns_icon_blank", wxBITMAP_TYPE_ICO_RESOURCE, 16, 16));
    } else if (id == ART_ICON_NEGERNS_HOME) {
        status = bmp.CopyFromIcon(resource->LoadIcon("negerns_icon_home"));
    } else if (id == ART_ICON_NEGERNS_NEW) {
        status = bmp.CopyFromIcon(resource->LoadIcon("negerns_icon_new"));
    } else if (id == ART_ICON_NEGERNS_FILE_CLOSE) {
        status = bmp.CopyFromIcon(resource->LoadIcon("negerns_icon_file_close"));
    } else if (id == ART_ICON_NEGERNS_FILE_CLOSE_ALL) {
        status = bmp.CopyFromIcon(resource->LoadIcon("negerns_icon_file_close_all"));
    } else if (id == ART_ICON_NEGERNS_EDIT) {
        status = bmp.CopyFromIcon(resource->LoadIcon("negerns_icon_edit"));
    } else if (id == ART_ICON_NEGERNS_UNDO) {
        status = bmp.CopyFromIcon(resource->LoadIcon("negerns_icon_undo"));
    } else if (id == ART_ICON_NEGERNS_REDO) {
        status = bmp.CopyFromIcon(resource->LoadIcon("negerns_icon_redo"));
    } else if (id == ART_ICON_NEGERNS_SAVE) {
        status = bmp.CopyFromIcon(resource->LoadIcon("negerns_icon_save"));
    } else if (id == ART_ICON_NEGERNS_REFRESH) {
        status = bmp.CopyFromIcon(resource->LoadIcon("negerns_icon_refresh"));
    } else if (id == ART_ICON_NEGERNS_CLEAR) {
        status = bmp.CopyFromIcon(resource->LoadIcon("negerns_icon_clear"));
    } else if (id == ART_ICON_NEGERNS_EXPAND) {
        status = bmp.CopyFromIcon(resource->LoadIcon("negerns_icon_expand"));
    } else if (id == ART_ICON_NEGERNS_COLLAPSE) {
        status = bmp.CopyFromIcon(resource->LoadIcon("negerns_icon_collapse"));
    }
    return (bmp.IsOk()) ? bmp : wxNullBitmap;
}

} //_ namespace gui
} //_ namespace negerns
