#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/gui/timepickerctrl.h>

using namespace negerns::gui;

TimePickerCtrl::TimePickerCtrl() : wxTimePickerCtrl() { }

TimePickerCtrl::TimePickerCtrl(wxWindow *parent,
    wxWindowID id,
    const wxDateTime &value,
    const wxPoint &pos,
    const wxSize &size,
    long style,
    const wxValidator &validator,
    const wxString &name) :
    wxTimePickerCtrl(parent, id, value, pos, size, style, validator, name)
{ }

TimePickerCtrl::TimePickerCtrl(wxWindow *parent,
    const wxSize &size,
    long style) :
    wxTimePickerCtrl(parent, wxID_ANY, wxDefaultDateTime, wxDefaultPosition, size, style | wxDP_DROPDOWN)
{ }

TimePickerCtrl::~TimePickerCtrl() { }

void TimePickerCtrl::SetValidator(negerns::Var *val, long style)
{
    wxTimePickerCtrl::SetValidator(DataXfer(val, DataXfer::Control::Date, style));
}

void TimePickerCtrl::SetValidator(negerns::Var &val, long style)
{
    wxTimePickerCtrl::SetValidator(DataXfer(&val, DataXfer::Control::Date, style));
}

void TimePickerCtrl::Create(wxWindow *parent,
    wxWindowID id,
    const wxDateTime &value,
    const wxPoint &pos,
    const wxSize &size,
    long style,
    const wxValidator &validator,
    const wxString &name)
{
    wxTimePickerCtrl::Create(parent, id, value, pos, size, style, validator, name);
}
