#include <wx/dcclient.h>
#include <negerns/gui/auitoolbar.h>

namespace negerns {
namespace gui {

AuiToolBar::AuiToolBar() :
    wxAuiToolBar()
{ }

AuiToolBar::AuiToolBar(wxWindow* parent,
             wxWindowID id,
             const wxPoint& position,
             const wxSize& size,
             long style)
: wxAuiToolBar(parent, id, position, size, style) { }

void AuiToolBar::Create(wxWindow* parent,
    wxWindowID id,
    const wxPoint& position,
    const wxSize& size,
    long style)
{
#ifdef PATCH_15751
    if (style == -1) {
        style = GetWindowStyleFlag();
    }
#endif
    wxAuiToolBar::Create(parent, id, position, size, style);
}

void AuiToolBar::UpdateTool(int toolId)
{
    wxClientDC dc(this);
    wxRect cli_rect(wxPoint(0,0), GetClientSize());

    wxAuiToolBarItem toolItem;

    bool found = false;
    // The following code is taken from FindTool.
    size_t i, count;
    for (i = 0, count = m_items.GetCount(); i < count; ++i)
    {
        wxAuiToolBarItem& item = m_items.Item(i);
        int id = item.GetId();
        if (id == toolId) {
            toolItem = item;
            found = true;
        }
    }
    if (!found) return;

    // ----------------------------------------------
    // The following code has been taken from OnPaint.
    // ----------------------------------------------

    bool horizontal = m_orientation == wxHORIZONTAL;
    int dropdown_size = m_art->GetElementSize(wxAUI_TBART_OVERFLOW_SIZE);

    // calculated how far we can draw items
    int last_extent;
    if (horizontal)
        last_extent = cli_rect.width;
    else
        last_extent = cli_rect.height;
    if (m_overflowVisible)
        last_extent -= dropdown_size;

    if (!toolItem.GetSizerItem()) return;
    wxRect item_rect = toolItem.GetSizerItem()->GetRect();
    if ((horizontal  && item_rect.x + item_rect.width >= last_extent) ||
        (!horizontal && item_rect.y + item_rect.height >= last_extent))
    {
        return;
    }

    m_art->DrawBackground(dc, this, item_rect);
    int kind = toolItem.GetKind();
    bool dropdown = toolItem.HasDropDown();
    if (kind == wxITEM_LABEL) {
        //m_art->DrawBackground(dc, this, item_rect);
        // draw a text label only
        m_art->DrawLabel(dc, this, toolItem, item_rect);
    } else if (kind == wxITEM_CHECK) {
        // draw either a regular or dropdown toggle button
        if (!dropdown)
            m_art->DrawButton(dc, this, toolItem, item_rect);
        else
            m_art->DrawDropDownButton(dc, this, toolItem, item_rect);
    } else if (kind == wxITEM_NORMAL) {
        // draw a regular button or dropdown button
        if (!dropdown)
            m_art->DrawButton(dc, this, toolItem, item_rect);
        else
            m_art->DrawDropDownButton(dc, this, toolItem, item_rect);
    } else if (kind == wxITEM_CHECK) {
        // draw either a regular or dropdown toggle button
        if (!dropdown)
            m_art->DrawButton(dc, this, toolItem, item_rect);
        else
            m_art->DrawDropDownButton(dc, this, toolItem, item_rect);
    } else if (kind == wxITEM_RADIO) {
        // draw a toggle button
        m_art->DrawButton(dc, this, toolItem, item_rect);
    } else if (kind == wxITEM_CONTROL) {
        m_art->DrawBackground(dc, this, item_rect);
        // draw the control's label
        m_art->DrawControlLabel(dc, this, toolItem, item_rect);
    }

    // fire a signal to see if the item wants to be custom-rendered
    OnCustomRender(dc, toolItem, item_rect);
    // ------------------
    // end of copied code.
    // ------------------
}

void AuiToolBar::UpdateToolByIndex(int idx)
{
    wxClientDC dc(this);
    wxRect cli_rect(wxPoint(0,0), GetClientSize());

    wxAuiToolBarItem& item = m_items.Item(idx);

    // ----------------------------------------------
    // The following code has been taken from OnPaint.
    // ----------------------------------------------

    bool horizontal = m_orientation == wxHORIZONTAL;
    int dropdown_size = m_art->GetElementSize(wxAUI_TBART_OVERFLOW_SIZE);

    // calculated how far we can draw items
    int last_extent;
    if (horizontal)
        last_extent = cli_rect.width;
    else
        last_extent = cli_rect.height;
    if (m_overflowVisible)
        last_extent -= dropdown_size;


    if (!item.GetSizerItem()) return;
    wxRect item_rect = item.GetSizerItem()->GetRect();
    if ((horizontal  && item_rect.x + item_rect.width >= last_extent) ||
        (!horizontal && item_rect.y + item_rect.height >= last_extent))
    {
        return;
    }

    m_art->DrawBackground(dc, this, item_rect);
    int kind = item.GetKind();
    bool dropdown = item.HasDropDown();
    if (kind == wxITEM_LABEL)
    {
        //m_art->DrawBackground(dc, this, item_rect);
        // draw a text label only
        m_art->DrawLabel(dc, this, item, item_rect);
    } else if (kind == wxITEM_CHECK) {
        // draw either a regular or dropdown toggle button
        if (!dropdown)
            m_art->DrawButton(dc, this, item, item_rect);
        else
            m_art->DrawDropDownButton(dc, this, item, item_rect);
    } else if (kind == wxITEM_NORMAL) {
        // draw a regular button or dropdown button
        if (!dropdown)
            m_art->DrawButton(dc, this, item, item_rect);
        else
            m_art->DrawDropDownButton(dc, this, item, item_rect);
    } else if (kind == wxITEM_CHECK) {
        // draw either a regular or dropdown toggle button
        if (!dropdown)
            m_art->DrawButton(dc, this, item, item_rect);
        else
            m_art->DrawDropDownButton(dc, this, item, item_rect);
    } else if (kind == wxITEM_RADIO) {
        // draw a toggle button
        m_art->DrawButton(dc, this, item, item_rect);
    } else if (kind == wxITEM_CONTROL) {
        m_art->DrawBackground(dc, this, item_rect);
        // draw the control's label
        m_art->DrawControlLabel(dc, this, item, item_rect);
    }

    // fire a signal to see if the item wants to be custom-rendered
    OnCustomRender(dc, item, item_rect);
    // ------------------
    // end of copied code.
    // ------------------
}

bool AuiToolBar::Realize()
{
    return wxAuiToolBar::Realize();
}

} //_ namespace gui
} //_ namespace negerns
