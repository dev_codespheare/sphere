#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/gui/textctrl.h>

namespace negerns {
namespace gui {

TextCtrl::TextCtrl() : wxTextCtrl() { }

#if 0
TextCtrl::TextCtrl(wxWindow *parent,
    wxWindowID id,
    const wxString &value,
    const wxPoint pos,
    const wxSize& size,
    long style,
    const wxValidator& validator,
    const wxString& name) :
    wxTextCtrl(parent, id, value, pos, size, style, validator, name)
{ }

TextCtrl::TextCtrl(wxWindow *parent,
    const wxSize& size,
    long style,
    std::string *val) :
    wxTextCtrl(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, size, style)
{
    wxTextCtrl::SetValidator(negerns::gui::DataGateway(val, negerns::gui::DataGateway::TextCtrl, wxFILTER_EMPTY));
}

TextCtrl::TextCtrl(wxWindow *parent,
    const wxSize& size,
    long style,
    unsigned int *val) :
    wxTextCtrl(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, size, style)
{
    wxTextCtrl::SetValidator(negerns::gui::DataGateway(val, negerns::gui::DataGateway::TextCtrl, wxFILTER_EMPTY));
}
#endif



TextCtrl::TextCtrl(wxWindow *parent,
    const wxSize& size,
    long style) :
    wxTextCtrl(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, size, style)
{ }

TextCtrl::~TextCtrl() { }

void TextCtrl::SetValidator(negerns::Var *val, long style)
{
    wxTextCtrl::SetValidator(DataXfer(val, DataXfer::Control::Text, style));
}

void TextCtrl::SetValidator(negerns::Var &val, long style)
{
    wxTextCtrl::SetValidator(DataXfer(&val, DataXfer::Control::Text, style));
}



#if 0
void TextCtrl::Create(wxWindow *parent,
    wxWindowID id,
    const wxString &value,
    const wxPoint pos,
    const wxSize& size,
    long style,
    const wxValidator& validator,
    const wxString& name)
{
    wxTextCtrl::Create(parent, id, value, pos, size, style, validator, name);
}

// Create without id, position and choices
void TextCtrl::Create(wxWindow *parent,
    const wxSize& size,
    long style,
    std::string *val)
{
    wxTextCtrl::Create(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, size, style);
    wxTextCtrl::SetValidator(negerns::gui::DataGateway(val, negerns::gui::DataGateway::TextCtrl, wxFILTER_EMPTY));
}

// Create without id, position and choices
void TextCtrl::Create(wxWindow *parent,
    const wxSize& size,
    long style,
    unsigned int *val)
{
    wxTextCtrl::Create(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, size, style);
    wxTextCtrl::SetValidator(negerns::gui::DataGateway(val, negerns::gui::DataGateway::TextCtrl, wxFILTER_EMPTY));
}
#endif

} //_ namespace gui
} //_ namespace negerns
