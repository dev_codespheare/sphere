#include <thread>
// This include avoids an error in wxThread::Yield declaration
#include <wx/wx.h>

#include <wx/cmdline.h>
#include <wx/debugrpt.h>
#include <wx/artprov.h>
#include <wx/fs_zip.h>
#include <wx/xrc/xmlres.h>
#include <negerns/core/debug.h>
#include <negerns/core/system.h>
#include <negerns/core/log.h>
#include <negerns/core/output.h>
#include <negerns/core/message.h>
#include <negerns/core/filesystem.h>
#include <negerns/core/platform.h>
#include <negerns/core/event.h>
#include <negerns/core/string.h>
#include <negerns/core/config/property.h>
#include <negerns/core/configuration.h>
#include <negerns/data/dataaccess.h>
#include <negerns/gui/app.h>
#include <negerns/gui/resource.h>
#include <negerns/gui/artprovider.h>

namespace negerns {
namespace gui {

const char * Application::configFile = "system.conf";
#ifdef _DEBUG
const char * Application::traceFile = "system_trace.conf";
#endif

Application::Application() :
    wxApp()
{
    // Let Application::OnFatalException catch fatal exceptions.
    // See wxHandleFatalExceptions
#if defined wxUSE_ON_FATAL_EXCEPTION && wxUSE_ON_FATAL_EXCEPTION == 1
    wxHandleFatalExceptions(true);
#endif
#ifndef __WXMSW__
    SetUseBestVisual(true, true);
#endif
    SetExitOnFrameDelete(true);

    negerns::System::Application(this);

    {
        auto log = negerns::make_unique(new negerns::Log());
        auto output = negerns::make_unique(new negerns::Output());
    }

    log::info("System configuration initialization...");
    auto *config = new negerns::Configuration();
    negerns::System::Configuration(config);

    auto *pfile = new negerns::config::Property(Application::configFile, true);
    config->add(pfile, Priority::Configuration);
    config->read(Application::configFile);
#ifdef _DEBUG
    log::debug("System trace configuration initialization...");
    auto *tfile = new negerns::config::Property(Application::traceFile);
    config->add(tfile, Priority::Trace);
    config->read(Application::traceFile);
#endif

    {
        auto res = negerns::make_unique(new negerns::gui::Resource());
    }

    log::info("Event manager initialization...");
    negerns::System::EventManager(new negerns::EventManager());

    {
        auto da = negerns::make_unique(new negerns::data::DataAccess());
    }
}

bool Application::OnInit(void)
{
    // Allow command line processing provided by wxWidgets
    bool status = wxApp::OnInit();
    if (!status) return false;

    Message::SetTitle(GetAppDisplayName().ToStdString());

    {
        std::string path(negerns::filesystem::parent_path());
        path.append("\\log");
        auto log = negerns::make_unique(new negerns::Log());
        log->SetFile(path, this->GetAppName().ToStdString());
        log::debug("Log File: %1", log->GetFullPath());
        negerns::Log::Init();
    }

    PlatformInfo platformInfo;
    log::info("Operating System: %1\n"
        "Version: %2.%3.%4 %5\n"
        "Architecture: %6\n"
        "Application: %7\n"
        "Version: %8\n"
        "Architecture: %9 (%10)\n"
        "Application Path: %11\n"
        "Thread Count: %12",
        platformInfo.get_os_name(),
        platformInfo.get_major_version(),
        platformInfo.get_minor_version(),
        platformInfo.get_revision(),
        platformInfo.get_service_pack(),
        platformInfo.get_architecture(),
        aboutInfo.GetName(),
        aboutInfo.GetVersion(),
        aboutInfo.GetPlatform(),
        aboutInfo.GetConfiguration(),
        boost::filesystem::current_path().string(),
        std::thread::hardware_concurrency());

    // Required when loading the resources below
    wxFileSystem::AddHandler(new wxArchiveFSHandler());
    wxImage::AddHandler(new wxBMPHandler());
    wxImage::AddHandler(new wxPNGHandler());
    wxImage::AddHandler(new wxICOHandler());

    log::info("Loading system resources...");
    negerns::gui::Resource::Add("negerns", "../res/negerns.bin");
    std::string url = negerns::gui::Resource::BuildUrl("negerns", "icons.xrc");
    bool resLoaded = wxXmlResource::Get()->Load(url);
    log::info("Resource: %1", url);
    ASSERT(resLoaded, "Could not load resource: %s", url);
    log::info("Done loading system resources.");

    wxValidator::SuppressBellOnError(false);
    wxXmlResource::Get()->InitAllHandlers();

    wxArtProvider::Push(new ArtProvider());

    InitConfiguration();
    InitDatabase();

    return OnPostInit();
}

bool Application::OnPostInit()
{
    return true;
}

bool Application::OnCmdLineParsed(wxCmdLineParser& parser)
{
    return true;
}

bool Application::OnCmdLineHelp(wxCmdLineParser& parser)
{
    return wxApp::OnCmdLineHelp(parser);
}

bool Application::OnCmdLineError(wxCmdLineParser& parser)
{
    return wxApp::OnCmdLineError(parser);
}

void Application::OnAssertFailure(const wxChar* f,
    int line,
    const wxChar *func,
    const wxChar *cond,
    const wxChar *msg)
{
    std::string file = wxString(f).ToStdString();
    {
        auto pos = file.rfind("..\\");
        if (pos != std::string::npos) {
            file.erase(0, pos + 3);
        }
    }

    std::string function = wxString(func).ToStdString();
    std::string condition = wxString(cond).ToStdString();
    std::string message = wxString(msg).ToStdString();
    std::string assertinfo;

    if (condition.empty()) {
        if (message.empty()) {
            assertinfo = negerns::Format(
                "Assertion Failed without condition.\n"
                "  Function: %1\n"
                "  File: %2 line %3",
                function,
                file,
                line);
        } else {
            assertinfo = negerns::Format(
                "Assertion Failed without condition.\n"
                "  Message: %1\n"
                "  Function: %2\n"
                "  File: %3 line %4",
                message,
                function,
                file,
                line);
        }
    } else {
        if (message.empty()) {
            assertinfo = negerns::Format(
                "Assertion Failed in expression [%1]\n"
                "  Function: %2\n"
                "  File: %3 line %4",
                condition,
                function,
                file,
                line);
        } else {
            assertinfo = negerns::Format(
                "Assertion Failed in expression [%1]\n"
                "  Message: %2\n"
                "  Function: %3\n"
                "  File: %4 line %5",
                condition,
                message,
                function,
                file,
                line);
        }
    }
    log::error(assertinfo);
    assertinfo.insert(0, "*****\n");
    assertinfo.append("\n");
    negerns::Output::Send(assertinfo);
    wxApp::OnAssertFailure(wxString(file).c_str(), line, func, cond, msg);
}

void Application::OnFatalException()
{
    LOG_FUNCTION();
#if !defined wxUSE_ON_FATAL_EXCEPTION || wxUSE_ON_FATAL_EXCEPTION == 0
    LOG_FATAL("The system encountered a fatal exception.\n"
        "The application will now terminate.\n"
        "Any unsaved information will be lost.");
    wxLog::FlushActive();
    wxLog::EnableLogging(false);
#else
#if EXCLUDE
    wxDebugReport report;
    wxDebugReportPreviewStd preview;
    report.AddAll();
    if (preview.Show(report)) {
        report.Process();
    }
#endif
#endif

    if (negerns::data::DataAccess::IsInitialized()) {
        negerns::data::DataAccess::UnInit();
    }

    log::info("Uninitializing event manager...");
    auto *em = negerns::System::EventManager();
    log::debug("Event manager objects: %1", em->Count());
    em->Clear();
    log::info("Event manager uninitialized.");

    log::info("Unloading resources.");
    wxArtProvider::Pop();
    Resource::Shutdown();
    wxXmlResource::Get()->ClearHandlers();
    log::info("Resources unloaded.");

    // IMPORTANT:
    //
    // If a fatal exception happens while the configuration file
    // is being read then saving it might not write all the contents
    // of the configuration file since there could have been no
    // contents read when the fatal exception occured.
    // Having a 'safe_to_save' flag is one solution.
    //
    // TODO: Determine when to save and not save the configuration file.

    auto *config = negerns::System::Configuration();
    config->save();

    negerns::Output::Shutdown();

    Message::Error("Exiting application on fatal exception.");
    log::info("Exiting application on fatal exception.");
    Log::Shutdown();
}

int Application::OnExit(void)
{
    LOG_FUNCTION();

    if (negerns::data::DataAccess::IsInitialized()) {
        negerns::data::DataAccess::UnInit();
    }

    log::info("Uninitializing event manager...");
    auto *em = negerns::System::EventManager();
    log::debug("Event manager objects: %1", em->Count());
    em->Clear();
    log::info("Event manager uninitialized.");

    log::info("Unloading resources.");
    Resource::Shutdown();
    wxXmlResource::Get()->ClearHandlers();
    log::info("Resources unloaded.");

    auto *config = negerns::System::Configuration();
    ASSERT(config, "Configuration instance is null.");
    config->save();

    negerns::Output::Shutdown();

    log::info("Exiting application normally.");
    Log::Shutdown();
    return 0;
}

void Application::OnInitCmdLine(wxCmdLineParser& parser)
{
    parser.EnableLongOptions(true);
    parser.SetSwitchChars("-");
}

void Application::SetFrameManager(FrameManager *fm)
{
    frameManager = fm;
    wxApp::SetTopWindow(frameManager->GetFrame());
}

wxAppTraits* Application::CreateTraits()
{
    return new ApplicationTraits();
}

void Application::InitConfiguration()
{
    LOG_FUNCTION();
    //Component::SetConfigurationInstance(baseConfig);
    // TODO: Check reference count of configuration instance.
    //       Test whether we can safely delete the reference
    //       here. If not, maybe using one of the new C++
    //       constructs for pointer use like SharedPtr, etc.
}



// ----------------------------------------------------------------------------------------



bool ApplicationTraits::ShowAssertDialog(const wxString &msg)
{
#ifndef NDEBUG
    // GUI message box can only be shown in the main thread
    if (wxThread::IsMain()) {
        // Parse the message
        // The message string is in the following format:
        // <file>(<line>): assert "<expression>" failed in <function>: <assert_message>
        //
        // Items between angle brackets are the components of the message.
        //
        // Example:
        // ..\..\src\app\ais\ais.cpp(68): assert "i > 0" failed in Ais::OnInit(): Test assertion.
        int pos = 0;
        pos = msg.First('(');
        std::string filename = msg.Left(pos);
        std::string line = msg.SubString(pos + 1, msg.First(')') - 1);
        pos = msg.First('\"');
        std::string expr = msg.SubString(pos + 1, msg.find('"', pos + 1) - 1);
        pos = msg.find("failed in") + strlen("failed in") + 1;
        int pos2 = msg.find(')', pos);
        std::string func = msg.SubString(pos, pos2);
        std::string message;

        pos = msg.find(':', pos2);
        if (pos != std::string::npos) {
            message = msg.substr(pos + 2);
            // End sentence with a period.
            if (message.back() != '.') {
                message.append(".");
            }
        }

        std::string emsg;
        if (expr.empty()) {
            if (message.empty()) {
                emsg = negerns::Format("An assertion was encountered in function %1\n", func);
            } else {
                emsg = negerns::Format("%1\nAn assertion was encountered in function %2\n", message, func);
            }
        } else {
            if (message.empty()) {
                emsg = negerns::Format("The assertion was encountered in expression [%1]\n", expr);
            } else {
                emsg = negerns::Format("%1\nThe assertion was encountered in expression [%2]\n", message, expr);
            }
            emsg.append(negerns::Format("in function %1\n", func));
        }
        emsg.append(negerns::Format("at %1 line %2..\n\n", filename, line));

#if 0
#if wxUSE_STACKWALKER
        const wxString stackTrace = GetAssertStackTrace();
        if (!stackTrace.empty()) {
            emsg.append("\n\nCall stack:\n");
            emsg.append(stackTrace);
        }
#endif //_ wxUSE_STACKWALKER
#endif
#if 0
        if (auto count = negerns::AssertContext::Count()) {
            emsg.append("Context:\n");
            std::string s;
            for (size_t i = 0; i < count; ++i) {
                s = negerns::AssertContext::GetInfo(i);
                emsg.append(s).append("\n");
            }
            emsg.append("\n");
        }
#endif
#ifdef _DEBUG
        emsg.append(
            "You can stop the execution and use the debugger to trace into the"
            "       \n"
            "program source code or you can suppress further warnings.");

        int response = negerns::Message::Assert("Assertion Failed!", emsg);
        switch (response) {
            case Message::Reponse::Yes:
                // IMPORTANT:
                //
                // The following line of code was taken from common/appbase.cpp.
                // If the function CreateTraits is not overridden, the function
                // DoShowAssertDialog is called instead.
                //
                // If wxTrap() is called directly from here, the debugger will
                // be 'breaking' on the wxTrap() call. It will be confusing for
                // the programmer to land on a code away from the actual line
                // the assertion is triggered.
                //
                // So setting the flag here allows the assert macros to call
                // wxTrap() which ensures that the debugger will be 'breaking'
                // on the actual line code that triggered the assertion.
                wxTrapInAssert = true;
                break;
            case Message::Reponse::Cancel:
                // no more asserts
                return true;
            default:
                break;
        }
#else
        emsg.append("The application has encountered an assertion. The application will"
            "       \n"
            "try to save your work before terminating.");
        negerns::Message::Assert("Assertion Failed!", emsg,
            negerns::Message::Button::Close);
#endif //_ _DEBUG
        return false;
    } else {
        return true;
    }
#else
    return wxAppTraitsBase::ShowAssertDialog(msg);
#endif
}

} //_ namespace gui
} //_ namespace negerns
