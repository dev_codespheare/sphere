#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/debug.h>
#include <negerns/gui/splitter.h>

namespace negerns {
namespace gui {

Splitter::Splitter(wxWindow* parent) :
    wxSplitterWindow()
{
    wxSplitterWindow::Create(parent,
        wxID_ANY,
        wxDefaultPosition,
        wxDefaultSize,
        wxSP_BORDER | wxSP_LIVE_UPDATE);
    SetSashInvisible(false);
}

Splitter::~Splitter() { }

void Splitter::Split(wxWindow* control1, wxWindow* control2, int mode)
{
    ASSERT(control1 != NULL, "First Splitter control should not be NULL.");
    ASSERT(control2 != NULL, "Second Splitter control should not be NULL.");
    if (mode == wxSPLIT_VERTICAL) {
        wxSplitterWindow::SplitVertically(control1, control2);
    } else if (mode == wxSPLIT_HORIZONTAL) {
        wxSplitterWindow::SplitHorizontally(control1, control2);
    } else {
        ASSERT(false, "Split mode is not set correctly: %1.\n"
            "Please use wxSPLIT_VERTICAL or wxSPLIT_HORIZONTAL.", mode);
    }
    wxSplitterWindow::UpdateSize();
}

} //_ namespace gui
} //_ namespace negerns
