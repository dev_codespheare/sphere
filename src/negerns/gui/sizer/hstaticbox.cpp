#include <negerns/gui/sizer/hstaticbox.h>

namespace negerns {
namespace gui {
namespace sizer {

HStaticBox::HStaticBox(wxWindow *parent, const std::string &s) :
    wxStaticBoxSizer(wxHORIZONTAL, parent, s),
    parent(parent)
{ }

HStaticBox::~HStaticBox() { }

void HStaticBox::SetParent(wxWindow *p)
{
    parent = p;
}

} //_ namespace sizer
} //_ namespace gui
} //_ namespace negerns
