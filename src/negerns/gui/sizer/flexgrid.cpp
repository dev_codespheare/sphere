#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/debug.h>
#include <negerns/gui/sizer/flexgrid.h>

namespace negerns {
namespace gui {
namespace sizer {

bool negerns::gui::sizer::IsBitSet(int source, int flag)
{
    return (source & flag) != 0;
}



FlexGrid::FlexGrid(
    int columns,
    const wxSize &gap,
    wxWindow *p) :
    wxFlexGridSizer(columns, gap),
    parent(p)
{ }

FlexGrid::~FlexGrid() { }

void FlexGrid::SetParent(wxWindow *p)
{
    ASSERT(!parent, "Parent already set.");
    parent = p;
}

wxWindow* FlexGrid::GetParent()
{
    return parent;
}

void FlexGrid::SetSpacer(int size)
{
    spacer = size;
}

wxSizerItem* FlexGrid::AddSpacer(int size)
{
    if (size == -1) {
        return wxFlexGridSizer::AddSpacer(spacer);
    } else {
        return wxFlexGridSizer::AddSpacer(size);
    }
}

void FlexGrid::AddSizer(wxSizer *s)
{
    wxFlexGridSizer::Add(s);
}

negerns::gui::StaticText * FlexGrid::AddStaticText(
    const std::string &label,
    int style,
    negerns::Proportion proportion,
    int border,
    wxObject *userData)
{
#if 0
    int textstyle = 0;
    int sizerflag = 0;
    // Compare using equality because Alignment::Left value is zero.
    // Bit testing cannot be applied to zero values.
    if (style == Alignment::Left) {
        textstyle = Alignment::Left;
        sizerflag = Alignment::Left | Alignment::CenterVertical;
    }
    if (IsBitSet(style, Alignment::Right)) {
        textstyle = Alignment::Right;
        sizerflag = Alignment::Right | Alignment::CenterVertical;
    }
    if (IsBitSet(style, Alignment::Top)) {
        textstyle = Alignment::Top;
        sizerflag = Alignment::Top;
    }
    if (IsBitSet(style, Alignment::CenterHorizontal)) {
        textstyle = Alignment::CenterHorizontal;
        sizerflag = Alignment::CenterHorizontal | Alignment::CenterVertical;
    }
    auto *control = new negerns::gui::StaticText(parent, label, style);

    // Center vertically only when not aligned to top or bottom
    //sizerflag |= Alignment::CenterVertical;
#endif
    auto *control = new negerns::gui::StaticText(parent, label, style);
    wxFlexGridSizer::Add(control, proportion, style, border, userData);
    return control;
}

negerns::gui::SpinCtrl * FlexGrid::AddSpin(const std::string &label,
    const wxSize &size,
    int labelStyle,
    int controlStyle,
    wxObject *userData)
{
    ASSERT(parent, "Parent is null.");
    auto *control = new SpinCtrl(parent, size);
    return (negerns::gui::SpinCtrl *) AddControl(label, labelStyle,
        controlStyle, userData, control);
}

negerns::gui::TextCtrl * FlexGrid::AddText(const std::string &label,
    const wxSize &size,
    int labelStyle,
    int controlStyle,
    wxObject *userData)
{
    ASSERT(parent, "Parent is null.");
    auto *control = new TextCtrl(parent, size, Style::None);
    return (negerns::gui::TextCtrl *) AddControl(label, labelStyle,
        controlStyle, userData, control);
}

negerns::gui::ChoiceCtrl * FlexGrid::AddChoice(const std::string &label,
    const wxSize &size,
    int labelStyle,
    int controlStyle,
    wxObject *userData)
{
    ASSERT(parent, "Parent is null.");
    auto *control = new ChoiceCtrl(parent, size, Style::None);
    return (negerns::gui::ChoiceCtrl *) AddControl(label, labelStyle,
        controlStyle, userData, control);
}

negerns::gui::DatePickerCtrl* FlexGrid::AddDatePicker(const std::string &label,
    const wxSize &size,
    int labelStyle,
    int controlStyle,
    wxObject *userData)
{
    ASSERT(parent, "Parent is null.");
    auto *control = new DatePickerCtrl(parent, size, Style::None);
    return (negerns::gui::DatePickerCtrl *) AddControl(label, labelStyle,
        controlStyle, userData, control);
}

negerns::gui::Component* FlexGrid::AddComponent(negerns::gui::Component *component,
    int controlStyle)
{
    ASSERT(parent, "Parent is null.");
    wxFlexGridSizer::Add(component->GetPanel(), Proportion::Static, controlStyle,
        wxBORDER_NONE, NULL);
    return component;
}

wxControl* FlexGrid::AddControl(const std::string &label,
    int labelStyle,
    int controlStyle,
    wxObject *userData,
    wxControl *control)
{
    if (!label.empty()) {
        AddStaticText(label, labelStyle);
    }

    controlStyle |= Alignment::CenterVertical;

    int proportion = Proportion::Static;
    {
        // Determine if the next column where the control is to be placed is
        // growable (dynamic) and set the proportion appropriately.
        std::size_t count = wxFlexGridSizer::GetItemCount();
        int column = count % wxFlexGridSizer::GetCols();
        if (wxFlexGridSizer::IsColGrowable(column)) {
            proportion = Proportion::Dynamic;
        }
    }
    int border = wxBORDER_NONE;
    wxFlexGridSizer::Add(control, proportion, controlStyle, border, userData);
    return control;
}

#if 0
void FlexGrid::Clear()
{
    for (auto control : controls) {
        wxDELETE(control);
    }
    controls.clear();
}
#endif

} //_ namespace sizer
} //_ namespace gui
} //_ namespace negerns
