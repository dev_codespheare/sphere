#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/debug.h>
#include <negerns/gui/sizer/gridbag.h>

namespace negerns {
namespace gui {
namespace sizer {

GridBag::GridBag(
    int vgap,
    int hgap,
    wxWindow *p) :
    wxGridBagSizer(vgap, hgap),
    parent(p)
{ }

GridBag::~GridBag() { }

void GridBag::SetParent(wxWindow *p)
{
    ASSERT(!parent, "Parent already set.");
    parent = p;
}

} //_ namespace sizer
} //_ namespace gui
} //_ namespace negerns
