#include <negerns/gui/sizer/vstaticbox.h>

namespace negerns {
namespace gui {
namespace sizer {

VStaticBox::VStaticBox(wxWindow *parent, const std::string &s) :
    wxStaticBoxSizer(wxVERTICAL, parent, s),
    parent(parent)
{ }

VStaticBox::~VStaticBox() { }

void VStaticBox::SetParent(wxWindow *p)
{
    parent = p;
}

} //_ namespace sizer
} //_ namespace gui
} //_ namespace negerns
