#include <negerns/gui/sizer/box.h>

namespace negerns {
namespace gui {
namespace sizer {

Box::Box(wxWindow *p, n::Orientation o) :
    wxBoxSizer(o),
    parent(p)
{ }

Box::~Box() { }

void Box::SetParent(wxWindow *p)
{
    parent = p;
}
#if 0
negerns::gui::StaticText *Box::AddStaticText(
    const std::string &label,
    int style,
    int proportion,
    int flag,
    int border,
    wxObject *userData)
{
    auto *control = new negerns::gui::StaticText(parent, label, style);
    wxBoxSizer::Add(control, proportion, flag, border, userData);
    return control;
}
#endif

} //_ namespace sizer
} //_ namespace gui
} //_ namespace negerns
