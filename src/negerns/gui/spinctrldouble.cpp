#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/gui/spinctrldouble.h>

namespace negerns {
namespace gui {

SpinCtrlDouble::SpinCtrlDouble() : wxSpinCtrlDouble() { }

SpinCtrlDouble::SpinCtrlDouble(wxWindow *parent,
    const wxSize& size,
    long style) :
    wxSpinCtrlDouble(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, size, style)
{ }

SpinCtrlDouble::~SpinCtrlDouble() { }

void SpinCtrlDouble::SetValidator(negerns::Var *val, long style)
{
    wxSpinCtrlDouble::SetValidator(DataXfer(val, DataXfer::Control::Numeric, style));
}

void SpinCtrlDouble::SetValidator(negerns::Var &val, long style)
{
    wxSpinCtrlDouble::SetValidator(DataXfer(&val, DataXfer::Control::Numeric, style));
}

} //_ namespace gui
} //_ namespace negerns
