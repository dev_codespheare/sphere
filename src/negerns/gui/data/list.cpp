#include <negerns/core/debug.h>
//#include <negerns/gui/dataform.h>
#include <negerns/gui/data/list.h>

namespace negerns {
namespace gui {
namespace data {

List::List() :
    Base()
{
    LOG_FUNCTION();
    Component::SetName("Data List");
    Identifier::set("data_list");
    
    current = wxDataViewItem(nullptr);

    list = new n::gui::ListView();
    list->SetBorder(false);
}

List::~List()
{
    LOG_FUNCTION();
    wxDELETE(list);
}

void List::CreateControl()
{
    LOG_FUNCTION();
    Base::CreateControl();
    
    auto tb = GetToolBar();
    tb->AddTool(wxID_NEW, "New", ART_ICON_NEGERNS_NEW, "Create");
    tb->AddTool(wxID_EDIT, "Edit", ART_ICON_NEGERNS_EDIT, "Edit selected item");
    tb->AddSeparator();
    tb->AddTool(wxID_REFRESH, "Refresh", ART_ICON_NEGERNS_REFRESH, "Refresh list");
    tb->GetControl()->EnableTool(wxID_EDIT, false);
    tb->Realize();

    tb->Bind(&List::ICrud::Create, this, wxID_NEW);
    tb->Bind(&List::ICrud::Edit, this, wxID_EDIT);
    tb->Bind(&List::ICrud::Refresh, this, wxID_REFRESH);

    wxWindow* parent = (wxWindow*) Component::GetPanel();
    ASSERT(parent, "Parent should not be null.");

    list->SetParent(parent);
    list->CreateControl();
    
    Component::Add(list->GetPanel(), n::Proportion::Dynamic, wxGROW | wxALL);
    Component::Layout();

    list->SetRowDisplayEventFunction(LV_ROWDISPLAY_EVENT_FUNC(&List::OnRowDisplay, this));
    list->SetMouseEventFunction(LV_MOUSE_EVENT_FUNC(&List::OnMouse, this));
    list->Bind(wxEVT_DATAVIEW_SELECTION_CHANGED, &List::OnSelectionChanged, this);
    list->Bind(wxEVT_DATAVIEW_ITEM_ACTIVATED, &List::OnSelectionActivated, this);

    Component::AddControl(list->GetPanel());
}

void List::PostCreateControl()
{
    LOG_FUNCTION();
    Base::PostCreateControl();
}

void List::DestroyControl()
{
    LOG_FUNCTION();
    list->DestroyControl();
    Base::DestroyControl();
}

bool List::OnInitContent()
{
    LOG_FUNCTION();
    Base::OnInitContent();

    list->InitContent();
    return true;
}

void List::PostInitContent()
{
    LOG_FUNCTION();
    Base::PostInitContent();
    ICrud::Refresh();
}

void List::ApplyConfiguration()
{
    LOG_FUNCTION();
    Base::ApplyConfiguration();
    list->ApplyConfiguration();
}

n::gui::ListView * List::GetListCtrl()
{
    return list;
}

void List::OnEdit(wxCommandEvent &)
{
    LOG_FUNCTION();
    if (list->GetItemCount() == 0) {
        current = wxDataViewItem(nullptr);
    } else {
        current = list->GetCurrentItem();
    }
}

bool List::OnRefresh(wxCommandEvent &)
{
    LOG_FUNCTION();
    auto *ds = GetDataStore();
    auto count = ds->Retrieve();
    bool status = count > 0;
    list->SetModelData(ds, n::gui::ListView::DataSource::Original);
    GetToolBar()->EnableTool(wxID_EDIT, status);
    return status;
}

void List::OnSelectionChanged(wxDataViewEvent &e)
{
    LOG_FUNCTION();
    current = e.GetItem();
    if (current.IsOk()) {
        GetToolBar()->EnableTool(wxID_EDIT, true);
    }
    e.Skip();
}

void List::OnSelectionActivated(wxDataViewEvent &e)
{
    LOG_FUNCTION();
    ICrud::Edit();
    e.Skip();
}

void List::OnMouse(wxMouseEvent &e)
{
    LOG_FUNCTION();
    wxDataViewItem item;
    wxDataViewColumn *col;
    static_cast<wxDataViewListCtrl *>(list->GetControl())->HitTest(
        e.GetPosition(), item, col);
    if (!item.IsOk()) {
        GetToolBar()->EnableTool(wxID_EDIT, false);
    }
    current = item;
    e.Skip();
}

} //_ namespace data
} //_ namespace gui
} //_ namespace negerns
