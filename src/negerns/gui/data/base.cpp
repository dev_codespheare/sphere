#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/message.h>
#include <negerns/data/dataaccess.h>
#include <negerns/gui/data/base.h>

namespace negerns {
namespace gui {
namespace data {

negerns::data::DataStore * Base::CreateDataStore()
{
    //auto trans = negerns::data::DataAccess::GetTransaction();
    //ASSERT(trans != nullptr, "Transaction is null.");
    auto ds = new negerns::data::DataStore(negerns::data::DataAccess::GetTransaction());
    ASSERT(ds, "DataStore is null.");
    return ds;
}

Base::Base() :
    Component(),
    presentation(Presentation::Form),
    operation(Operation::Display),
    subject()
{
    auto *toolbar = new negerns::gui::bar::ToolBar();
    toolbar->SetBorder(negerns::gui::bar::Bar::Border::Bottom);
    SetToolBar(toolbar);

    ICrud::SetModified(false);
}

Base::~Base()
{
    LOG_FUNCTION();
    wxDELETE(datastore);
}

void Base::CreateControl()
{
    LOG_FUNCTION();
    Component::CreateControl();
}

void Base::PostCreateControl()
{
    LOG_FUNCTION();
    Component::PostCreateControl();
}

void Base::DestroyControl()
{
    LOG_FUNCTION();
    Component::DestroyControl();
}

bool Base::OnInitContent()
{
    LOG_FUNCTION();
    Component::OnInitContent();

    datastore = Base::CreateDataStore();

    return true;
}

void Base::PostInitContent()
{
    LOG_FUNCTION();
    Component::PostInitContent();
    XferDataToWindow();
}

void Base::ApplyConfiguration()
{
    LOG_FUNCTION();
    Component::ApplyConfiguration();
}

void Base::Create(const negerns::data::Parameters &p)
{
    LOG_FUNCTION();
    SetParameters(p);
    ICrud::Create();
}

void Base::Read(const negerns::data::Parameters &p)
{
    LOG_FUNCTION();
    SetParameters(p);
    ICrud::Read();
}

bool Base::PreCreate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    return ICrud::PreCreate(e);
}

bool Base::OnCreate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    ICrud::OnCreate(e);
    operation = Operation::Create;
    return true;
}

bool Base::PreRead(wxCommandEvent &e)
{
    LOG_FUNCTION();
    return ICrud::PreRead(e);
}

bool Base::OnRead(wxCommandEvent &e)
{
    LOG_FUNCTION();
    ICrud::OnRead(e);
    operation = Operation::Edit;
    SetIdentifier();
    return true;
}

bool Base::PreRefresh(wxCommandEvent &e)
{
    LOG_FUNCTION();
    ICrud::PreRefresh(e);
    XferDataFromWindow();
    return true;
}

bool Base::OnRefresh(wxCommandEvent &e)
{
    LOG_FUNCTION();
    ICrud::OnRefresh(e);
    return true;
}

bool Base::PreUpdate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    ICrud::PreUpdate(e);
    XferDataFromWindow();
    return true;
}

bool Base::OnUpdate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    ICrud::OnUpdate(e);
    return true;
}

bool Base::PostUpdate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    operation = Operation::Edit;
    return ICrud::PostUpdate(e);
}

void Base::SetParameters(const negerns::data::Parameters &p)
{
    parameters = p;
}

int Base::ShowErrorMessage(int error)
{
    ASSERT(error < 0, "Error code must be less than zero.");
    std::string sm;
    std::string em;
    switch (error) {
        case -1:
            sm = "Error creating %1.";
            em = "Unique identifier for already exists.";
            break;
        default:
            sm = "Database operation generic error.";
            em = "";
    }
    return n::Message::Error(FormatMessage(sm), FormatMessage(em));
}

n::data::DataStore * Base::GetDataStore()
{
    return datastore;
}

void Base::OnInitialRetrieve()
{
}

void Base::SetIdentifier(const std::string &s)
{
    if (s.empty()) {
        //Identifier::SetSuffix(parameters);
    } else {
        Identifier::set_suffix(s);
    }
}

std::string Base::FormatMessage(const std::string &msg)
{
    std::string s = subject.Get();
    return n::string::Format(msg, s);
}

} //_ namespace data
} //_ namespace gui
} //_ namespace negerns
