#include <negerns/core/debug.h>
#include <negerns/core/output.h>
#include <negerns/gui/data/input/single.h>

namespace negerns {
namespace gui {
namespace data {
namespace input {

Single::Single() :
    Base()
{
    LOG_FUNCTION();
    Component::SetName("Data Entry");
    Identifier::set("data_entry");

    operation = Operation::Create;
}

Single::~Single() { }

void Single::CreateControl()
{
    LOG_FUNCTION();
    Base::CreateControl();

    auto *tb = GetToolBar();
#if 0
    if (mode == Mode::Create) {
        tb->AddTool(wxID_NEW, "New", ART_ICON_NEGERNS_NEW, "Create new information");
    }
#endif
    tb->AddTool(wxID_SAVE, "Save", ART_ICON_NEGERNS_SAVE, "Save changes");
    tb->AddSeparator();
    tb->AddTool(wxID_REFRESH, "Revert", ART_ICON_NEGERNS_REFRESH, "Revert to original information");
    tb->Realize();
#if 0
    if (mode == Mode::Create) {
        tb->Bind(wxEVT_TOOL, &Single::ICrud::Create, this, wxID_NEW);
    }
#endif
    tb->Bind(&Single::ICrud::Update, this, wxID_SAVE);
    tb->Bind(&Single::ICrud::Refresh, this, wxID_REFRESH);
}

void Single::DestroyControl()
{
    LOG_FUNCTION();
    Base::DestroyControl();
}

void Single::PostInitContent()
{
    LOG_FUNCTION();
    Base::PostInitContent();

    // Get table structure by issuing a retrieve operation returning no row.

    auto *ds = GetDataStore();
    auto count = ds->Retrieve(parameters);
    ASSERT(count == 0, "DataStore::Retrieve should not return a row.\nSQL: %1", ds->sqlInsert.Get());
    ds->SetDefaults();
    original = ds->original.GetRow();
    current = original;
}

bool Single::OnCreate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = Base::OnCreate(e);
    if (status) {
        ASSERT(operation == Operation::Create,
            "OnCreate function must not be called.");
        auto *ds = GetDataStore();
        auto count = ds->Retrieve(parameters);
        if (count == 0) {
            ds->InsertRow();
            original = ds->inserted.GetRow(0);
            current = original;
            XferDataToWindow();
        } else {
            n::Message::Error("Transaction should not return a row.");
        }
        GotoFirstFocus();
    }
    return status;
}

bool Single::OnRead(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = Base::OnRead(e);
    if (status) {
        ASSERT(operation == Operation::Edit,
            "OnRead function must not be called.");
        auto *ds = GetDataStore();
        auto count = ds->Retrieve(parameters);
        if (count > 0) {
            ds->ModifyRow(0);
            original = ds->modified.GetRow(0);
            current = original;
            XferDataToWindow();
        } else {
            n::Message::Error("Transaction should return a row.");
        }
        GotoFirstFocus();
    }
    return status;
}

bool Single::OnRefresh(wxCommandEvent &e)
{
    LOG_FUNCTION();

#ifdef _DEBUG
    // Check types. This code is from Poco::Data::Row == operator overload.
    n::Output::Send("Check column types:");
    auto *ds = GetDataStore();
    auto count = ds->column.Count();
    for (int i = 0; i < count; ++i) {
        n::Output::Send(" Current %1: %2", i, current[i].type().name());
        n::Output::Send("Original %1: %2", i, original[i].type().name());
        if (current[i].type() != original[i].type()) {
            return false;
        }
    }
    n::Output::Send();
#endif

    bool status = Base::OnRefresh(e);
    if (status) {
        ICrud::SetModified(!n::data::DataStore::IsEqual(current, original));
        if (ICrud::IsModified()) {
            auto op = ShowUpdateMessage();
            if (op == n::Operation::Yes) {
                auto *ds = GetDataStore();
                // NOTE: Call DataStore Insert/Update instead of Single::Update
                //
                // This avoids duplicate calls to DataStore::IsEqual.
                if (operation == Operation::Create) {
                    ds->inserted.SetRow(0, current);
                    auto count = ds->Insert();
                    n::Message::ErrorIf(count == 0, "There was an error creating new item.");
                    status = count > 0;
                } else if (operation == Operation::Edit) {
                    ds->modified.SetRow(0, current);
                    auto count = ds->Update();
                    n::Message::ErrorIf(count == 0, "There was an error updating item.");
                    status = count > 0;
                } else {
                    n::Message::Error("Unknown mode.");
                    status = false;
                }
            } else if (op == n::Operation::No) {
                current = original;
                XferDataToWindow(Component::GetPanel());
                GotoFirstFocus();
                status = true;
            }
        }
    }
    return status;
}

bool Single::OnUpdate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = Base::OnUpdate(e);
    if (status) {
        ICrud::SetModified(!n::data::DataStore::IsEqual(current, original));
        if (ICrud::IsModified()) {
            auto *ds = GetDataStore();
            if (operation == Operation::Create) {
                ds->inserted.SetRow(0, current);
                auto count = ds->Insert();
                n::Message::ErrorIf(count == 0, "There was an error creating new item.");
                status = count > 0;
            } else if (operation == Operation::Edit) {
                ds->modified.SetRow(0, current);
                auto count = ds->Update();
                n::Message::ErrorIf(count == 0, "There was an error updating item.");
                status = count > 0;
            } else {
                n::Message::Error("Unknown mode.");
                status = false;
            }
        }
    }
    return status;
}

bool Single::PostUpdate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = Base::PostUpdate(e);
    if (status) {
        if (operation == Operation::Create) {
            operation = Operation::Edit;
            auto *ds = GetDataStore();
            auto count = ds->returned.RowCount();
            n::Message::ErrorIf(count == 0, "There was an error returning the "
                "unique identifier.");
            if (count > 0) {
                long eid = ds->returned(0, 0).convert<long>();
                if (eid < 0) {
                    ShowErrorMessage(eid);
                } else {
                    parameters.clear();
                    parameters.push_back(eid);
                    ICrud::Read();
                }
            }
            status = count > 0;
        } else if (operation == Operation::Edit) {
            ICrud::Read();
            status = true;
        } else {
            n::Message::Error("Unknown mode.");
            status = false;
        }
    }
    return status;
}

bool Single::CanClose()
{
    LOG_FUNCTION();
    bool status = true;
    XferDataFromWindow();
    ICrud::SetModified(!n::data::DataStore::IsEqual(current, original));
    if (ICrud::IsModified()) {
        auto op = ShowUpdateMessage();
        if (op == n::Operation::Yes) {
            Update();
        } else if (op == n::Operation::Cancel) {
            status = false;
        }
    }
    return status;
}

void Single::SetFirstFocus(wxWindow *w)
{
    firstFocus = w;
}

void Single::GotoFirstFocus()
{
    firstFocus->SetFocus();
}

n::Operation Single::ShowUpdateMessage()
{
    n::Operation op = n::Operation::Cancel;
    std::string sm;
    std::string em;
    bool empty;
    if (operation == Operation::Create) {
        empty = subject.IsEmpty();
        sm = empty ? DEFAULT_PROMPT_MESSAGE : ALT_PROMPT_MESSAGE;
        em = empty ? DEFAULT_PROMPT_CREATE_EMESSAGE : ALT_PROMPT_CREATE_EMESSAGE;
    } else if (operation == Operation::Edit) {
        empty = subject.IsEmpty();
        sm = empty ? DEFAULT_PROMPT_MESSAGE : ALT_PROMPT_MESSAGE;
        em = empty ? DEFAULT_PROMPT_EDIT_EMESSAGE : ALT_PROMPT_EDIT_EMESSAGE;
    } else {
        n::Message::Error("Unknown mode.");
        return op;
    }

    int ans = n::Message::Question(FormatMessage(sm), FormatMessage(em));
    return n::Operation(ans);
}

} //_ namespace input
} //_ namespace data
} //_ namespace gui
} //_ namespace negerns
