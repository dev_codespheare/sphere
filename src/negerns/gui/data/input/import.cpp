#include <negerns/core/debug.h>
#include <negerns/core/output.h>
#include <negerns/gui/data/input/import.h>

namespace negerns {
namespace gui {
namespace data {
namespace input {

Import::Import() :
    Base()
{
    LOG_FUNCTION();
    Component::SetName("Data Entry");
    Identifier::set("data_entry");

    operation = Operation::Create;
}

Import::~Import() { }

void Import::CreateControl()
{
    LOG_FUNCTION();
    Base::CreateControl();

    auto *tb = GetToolBar();
#if 0
    if (mode == Mode::Create) {
        tb->AddTool(wxID_NEW, "New", ART_ICON_NEGERNS_NEW, "Create new information");
    }
#endif
    tb->AddTool(wxID_SAVE, "Save", ART_ICON_NEGERNS_SAVE, "Save changes");
    tb->AddSeparator();
    tb->AddTool(wxID_REFRESH, "Revert", ART_ICON_NEGERNS_REFRESH, "Revert to original information");
    tb->Realize();
#if 0
    if (mode == Mode::Create) {
        tb->Bind(wxEVT_TOOL, &Import::ICrud::Create, this, wxID_NEW);
    }
#endif
    tb->Bind(&Import::ICrud::Update, this, wxID_SAVE);
    tb->Bind(&Import::ICrud::Refresh, this, wxID_REFRESH);
}

void Import::DestroyControl()
{
    LOG_FUNCTION();
    Base::DestroyControl();
}

void Import::PostInitContent()
{
    LOG_FUNCTION();
    Base::PostInitContent();
}

bool Import::OnCreate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = Base::OnCreate(e);
    if (status) {
        auto *ds = GetDataStore();
        auto count = ds->Retrieve();
        ds->InsertRows(0, count);
        ASSERT(ds->inserted.RowCount() == count, "Wrong number of rows inserted.");
#ifdef _DEBUG
        SetTestValues();
#endif
        GotoFirstFocus();
    }
    return status;
}

bool Import::OnRead(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = Base::OnRead(e);
    if (status) {
        ASSERT(operation == Operation::Edit, "Mode is create.\n"
            "OnRead function must not be called.");
        auto *ds = GetDataStore();
        auto count = ds->Retrieve(parameters);
        if (count > 0) {
            ds->ModifyRows(0, count);
            XferDataToWindow();
        } else {
            // TODO: Should be more than one row.
            n::Message::Error("Transaction should return a row.");
        }
        GotoFirstFocus();
    }
    return status;
}

bool Import::OnRefresh(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = Base::OnRefresh(e);
    if (status) {
        if (operation == Operation::Create) {
            auto *ds = GetDataStore();
            ds->DiscardChanges();
            auto count = ds->Retrieve();
            ds->InsertRows(0, count);
            ASSERT(ds->inserted.RowCount() == count, "Wrong number of rows inserted.");
#ifdef _DEBUG
            SetTestValues();
#endif
        }
    }
    return status;
}

bool Import::OnUpdate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = Base::OnUpdate(e);
    return status;
}

bool Import::PostUpdate(wxCommandEvent &e)
{
    LOG_FUNCTION();
    bool status = Base::PostUpdate(e);
    if (status) {
        ICrud::Read();
    }
    return status;
}

bool Import::CanClose()
{
    LOG_FUNCTION();
    bool status = true;
    XferDataFromWindow();
    ICrud::SetModified(!n::data::DataStore::IsEqual(current, original));
    if (ICrud::IsModified()) {
        auto op = ShowUpdateMessage();
        if (op == n::Operation::Yes) {
            Update();
        } else if (op == n::Operation::Cancel) {
            status = false;
        }
    }
    return status;
}

void Import::SetFirstFocus(wxWindow *w)
{
    firstFocus = w;
}

void Import::GotoFirstFocus()
{
    firstFocus->SetFocus();
}

n::Operation Import::ShowUpdateMessage()
{
    n::Operation op = n::Operation::Cancel;
    std::string sm;
    std::string em;
    bool empty;
    if (operation == Operation::Create) {
        empty = subject.IsEmpty();
        sm = empty ? DEFAULT_PROMPT_MESSAGE : ALT_PROMPT_MESSAGE;
        em = empty ? DEFAULT_PROMPT_CREATE_EMESSAGE : ALT_PROMPT_CREATE_EMESSAGE;
    } else if (operation == Operation::Edit) {
        empty = subject.IsEmpty();
        sm = empty ? DEFAULT_PROMPT_MESSAGE : ALT_PROMPT_MESSAGE;
        em = empty ? DEFAULT_PROMPT_EDIT_EMESSAGE : ALT_PROMPT_EDIT_EMESSAGE;
    } else {
        n::Message::Error("Unknown mode.");
        return op;
    }

    int ans = n::Message::Question(FormatMessage(sm), FormatMessage(em));
    return n::Operation(ans);
}

} //_ namespace input
} //_ namespace data
} //_ namespace gui
} //_ namespace negerns
