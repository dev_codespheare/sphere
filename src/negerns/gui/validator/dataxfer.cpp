#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/datetime.h>
#include <negerns/gui/validator/dataxfer.h>

#if wxUSE_VALIDATORS && (wxUSE_TEXTCTRL || wxUSE_COMBOBOX || wxUSE_DATEPICKCTRL)

#include <wx/valtext.h>

#ifndef WX_PRECOMP
#include <stdio.h>

#include <wx/checkbox.h>
#include <wx/timectrl.h>

#include <wx/datetime.h>
#include "wx/utils.h"
#include "wx/msgdlg.h"
#include "wx/intl.h"
#endif //_ WX_PRECOMP

#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#include <Poco/Data/Date.h>
#include <negerns/core/debug.h>
#include <negerns/core/datetime.h>
#include <negerns/gui/choicectrl.h>
#include <negerns/gui/datepickerctrl.h>
#include <negerns/gui/timepickerctrl.h>
#include <negerns/gui/textctrl.h>
#include <negerns/gui/spinctrl.h>
#include <negerns/gui/spinctrldouble.h>

#include <negerns/poco.h>

namespace negerns {
namespace gui {

DataXfer::DataXfer(negerns::Var *var, Control ctrl, long style) :
    variant(var),
    control(ctrl),
    m_validatorStyle(style)
{ }

DataXfer::DataXfer(const DataXfer& val)
    : wxValidator()
{
    wxValidator::Copy(val);

    control = val.control;
    m_validatorStyle = val.m_validatorStyle;
    variant = val.variant;
}

bool DataXfer::Validate(wxWindow *parent)
{
    // If window is disabled, simply return
    if (!m_validatorWindow->IsEnabled()) {
        return true;
    }

    bool isEmpty = false;
    if (control == Control::CheckBox) {

    } else if (control == Control::Text) {
        if (const auto *ctrl = GetWindowCtrl(TextCtrl)) {
            wxString val(ctrl->GetValue());
            isEmpty = val.IsEmpty();
        }
    } else if (control == Control::Numeric) {
        if (const auto *ctrl = GetWindowCtrl(SpinCtrl)) {
            // No checking because it will always have a value.
        }
    } else if (control == Control::Money) {
        if (const auto *ctrl = GetWindowCtrl(SpinCtrlDouble)) {
            // No checking because it will always have a value.
        }
    } else if (control == Control::Date) {
        if (const auto *ctrl = GetWindowCtrl(DatePickerCtrl)) {
            wxDateTime dt(ctrl->GetValue());
            isEmpty = !dt.IsValid();
        }
    } else if (control == Control::Time) {
        if (const auto *ctrl = GetWindowCtrl(TimePickerCtrl)) {
            wxDateTime dt = ctrl->GetValue();
            isEmpty = !dt.IsValid();
        }
    } else if (control == Control::Choice) {
        if (const auto *ctrl = GetWindowCtrl(ChoiceCtrl)) {
            int selection = ctrl->GetSelection();
            wxString val(ctrl->GetString(selection));
            isEmpty = val.IsEmpty();
        }
    }

    if (HasFlag(wxFILTER_EMPTY) && isEmpty) {
        m_validatorWindow->SetFocus();
        return false;
    }

    return true;
}

bool DataXfer::TransferToWindow()
{
    if (control == Control::CheckBox) {

    } else if (control == Control::Text) {
        if (auto *ctrl = GetWindowCtrl(TextCtrl)) {
            wxString value;
            if (!variant->empty()) {
                value = variant->get_string();
            }
            bool isEnabled = ctrl->IsEnabled();
            if (!isEnabled) {
                ctrl->Enable();
            }
            ctrl->SetValue(value);
            if (!isEnabled) {
                ctrl->Disable();
            }
        }
    } else if (control == Control::Numeric) {
        if (auto *ctrl = GetWindowCtrl(SpinCtrl)) {
            int value = 0;
            if (!variant->empty()) {
                value = variant->get_int();
            }
            bool isEnabled = ctrl->IsEnabled();
            if (!isEnabled) {
                ctrl->Enable();
            }
            ctrl->SetValue(value);
            if (!isEnabled) {
                ctrl->Disable();
            }
        }
    } else if (control == Control::Money) {
        if (auto *ctrl = GetWindowCtrl(SpinCtrlDouble)) {
            wxString value("0.00");
            if (!variant->empty()) {
                value = variant->get_string();
            }
            bool isEnabled = ctrl->IsEnabled();
            if (!isEnabled) {
                ctrl->Enable();
            }
            ctrl->SetValue(value);
            if (!isEnabled) {
                ctrl->Disable();
            }
        }
    } else if (control == Control::Date) {
        if (auto *ctrl = GetWindowCtrl(DatePickerCtrl)) {
            wxDateTime dt = wxDateTime::Now();
            if (!variant->empty()) {
                dt = date::to_wxdatetime(variant->get_tm());
            }
            bool isEnabled = ctrl->IsEnabled();
            if (!isEnabled) {
                ctrl->Enable();
            }
            // ==============================================================
            // BUG:
            // Setting the value like this ctrl->SetValue(dt) does not update
            // the value in the control.
            // wxWidgets 3.0.0
            // ==============================================================
            ctrl->wxDatePickerCtrl::SetValue(dt);
            if (!isEnabled) {
                ctrl->Disable();
            }
        }
    } else if (control == Control::Time) {
        if (auto *ctrl = GetWindowCtrl(TimePickerCtrl)) {
            wxDateTime dt = wxDateTime::Now();
            if (!variant->empty()) {
                dt = date::to_wxdatetime(variant->get_tm());
            }
            bool isEnabled = ctrl->IsEnabled();
            if (!isEnabled) {
                ctrl->Enable();
            }
            ctrl->SetValue(dt);
            if (!isEnabled) {
                ctrl->Disable();
            }
        }
    } else if (control == Control::Choice) {
        if (auto *ctrl = GetWindowCtrl(ChoiceCtrl)) {
            std::string value;
            if (!variant->empty()) {
                value = variant->get_string();
            }
            bool isEnabled = ctrl->IsEnabled();
            if (!isEnabled) {
                ctrl->Enable();
            }
            ctrl->SetValue(value);
            if (!isEnabled) {
                ctrl->Disable();
            }
        }
    }

    return true;
}

bool DataXfer::TransferFromWindow()
{
    if (control == Control::CheckBox) {

    } else if (control == Control::Text) {
        if (const auto *ctrl = GetWindowCtrl(TextCtrl)) {
            // Calling variant->empty() makes the variant type become
            // 'void' instead of 'string'. This affects the type
            // checking made in the data input class.
            ASSERT(variant, "Variant is null.");
            (*variant) = ctrl->GetValue().ToStdString();
        }
    } else if (control == Control::Numeric) {
        if (const auto *ctrl = GetWindowCtrl(SpinCtrl)) {
            ASSERT(variant, "Variant is null.");
            (*variant) = ctrl->GetValue();
        }
    } else if (control == Control::Money) {
        if (const auto *ctrl = GetWindowCtrl(SpinCtrlDouble)) {
            ASSERT(variant, "Variant is null.");
            (*variant) = ctrl->GetValue();
        }
    } else if (control == Control::Date) {
        if (const auto *ctrl = GetWindowCtrl(DatePickerCtrl)) {
            ASSERT(variant, "Variant is null.");
            // ==============================================================
            // BUG:
            // Getting the value like this ctrl->GetValue() causes an asserttion
            // wxLongLong to long conversion loss of precision in wxLongLongNative
            // ToLong function.
            // wxWidgets 3.0.0
            // ==============================================================
            wxDateTime dt(ctrl->DatePickerCtrl::GetValue());
            if (dt.IsValid()) {
                (*variant) = date::to_tm(dt);
            } else {
                (*variant) = date::now();
                return false;
            }
        }
    } else if (control == Control::Time) {
        if (const auto *ctrl = GetWindowCtrl(TimePickerCtrl)) {
            ASSERT(variant, "Variant is null.");
            wxDateTime dt(ctrl->GetValue());
            if (dt.IsValid()) {
                (*variant) = date::to_tm(dt);
            } else {
                (*variant) = date::now();
                return false;
            }
        }
    } else if (control == Control::Choice) {
        if (const auto *ctrl = GetWindowCtrl(ChoiceCtrl)) {
            ASSERT(variant, "Variant is null.");
            (*variant) = ctrl->GetValue();
        }
    }

    return true;
}

wxString DataXfer::IsValid(const wxString& val) const
{
    return wxEmptyString;
}

} //_ namespace gui
} //_ namespace negerns

#endif //_ wxUSE_VALIDATORS && (wxUSE_TEXTCTRL || wxUSE_COMBOBOX)
