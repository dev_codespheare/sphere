#include <negerns/core/log.h>
#include <negerns/gui/dialog/logindialogbase.h>

namespace negerns {
namespace gui {

LogInDialogBase::LogInDialogBase(wxWindow* parent,
                       wxWindowID id,
                       const wxString& title,
                       const wxPoint& pos,
                       const wxSize& size,
                       long style) :
    DialogBase(parent, id, title, pos, size, style)
{ }

void LogInDialogBase::OnLogin(wxCommandEvent&)
{
    LOG_FUNCTION();
    if (wxDialog::Validate() && wxDialog::TransferDataFromWindow()) {
        if (IsModal()) {
            EndModal(wxID_OK);
        } else {
            SetReturnCode(wxID_OK);
            Show(false);
        }
    }
}

void LogInDialogBase::OnClose(wxCloseEvent& e)
{
    LOG_FUNCTION();
    if (wxDialog::Validate() && wxDialog::TransferDataFromWindow()) {
        if (IsModal()) {
            EndModal(wxID_OK);
        } else {
            SetReturnCode(wxID_OK);
            Show(false);
        }
        e.Skip();
    }
}

} // namesapce gui
} // namesapce negerns
