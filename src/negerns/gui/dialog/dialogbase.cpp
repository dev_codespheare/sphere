#include <negerns/gui/dialog/dialogbase.h>

namespace negerns {
namespace gui {

DialogBase::DialogBase() : Dialog(), Component()
{
    wxDialog::SetExtraStyle(wxWS_EX_VALIDATE_RECURSIVELY);
    Component::SetParent(Dialog::GetThis());
}

DialogBase::DialogBase(wxWindow* parent,
    wxWindowID id,
    const wxString& title,
    const wxPoint& pos,
    const wxSize& size,
    long style) :
    Component()
{
    wxDialog::SetExtraStyle(wxWS_EX_VALIDATE_RECURSIVELY);
    Dialog::Create(parent, id, title, pos, size, style);
    Component::SetParent(Dialog::GetThis());
}

DialogBase::~DialogBase() { }

bool DialogBase::Create(wxWindow* parent,
                       wxWindowID id,
                       const wxString& title,
                       const wxPoint& pos,
                       const wxSize& size,
                       long style)
{
    return Dialog::Create(parent, id, title, pos, size, style);
}

} //_ namespace gui
} //_ namespace negerns
