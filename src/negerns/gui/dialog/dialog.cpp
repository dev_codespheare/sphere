#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/debug.h>
#include <negerns/gui/dialog/dialog.h>

namespace negerns {
namespace gui {

wxWindow* Dialog::parent = NULL;

Dialog::Dialog() : wxDialog()
{
    dialog = this;
}

Dialog::Dialog(wxWindow* parent,
               wxWindowID id,
               const wxString& title,
               const wxPoint& pos,
               const wxSize& size,
               long style) :
    wxDialog(parent, id, title, pos, size, style)
{
    dialog = this;
}

Dialog::~Dialog() { }

bool Dialog::Create(wxWindow* parent,
               wxWindowID id,
               const wxString& title,
               const wxPoint& pos,
               const wxSize& size,
               long style)
{
    return wxDialog::Create(parent, id, title, pos, size, style);
}

void Dialog::SetParent(wxWindow* p)
{
    ASSERT(p != NULL, "Message dialog parent should not be null.");
    parent = p;
}

} //_ namespace gui
} //_ namespace negerns
