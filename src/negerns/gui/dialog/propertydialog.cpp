#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <wx/button.h>
#include <negerns/core/defs.h>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/gui/dialog/propertydialog.h>

namespace negerns {
namespace gui {

PropertyDialog::PropertyDialog() :
    DialogBase(),
    buttonSizer(nullptr),
    notebook(nullptr)
{
    InitNotebook();
}

PropertyDialog::PropertyDialog(wxWindow* parent,
    wxWindowID id,
    const wxString& title,
    const wxPoint& pos,
    const wxSize& size,
    long style) :
    DialogBase(parent, id, title, pos, size, style),
    buttonSizer(nullptr),
    notebook(nullptr)
{
    InitNotebook();
}

PropertyDialog::~PropertyDialog() { }

void PropertyDialog::CreateControl()
{
    LOG_FUNCTION();
    Component::CreateControl();
    auto *p = Component::GetPanel();
    ASSERT(p, "Parent should not be null.");
    notebook->SetParent(p);
    notebook->CreateControl();

    auto *cancelButton = new wxButton(p, wxID_CANCEL, "Close");
    cancelButton->SetMaxSize(wxSize(90, -1));
    SetEscapeId(cancelButton->GetId());
    buttonSizer = new wxBoxSizer(wxHORIZONTAL);
    buttonSizer->Add(cancelButton, negerns::Proportion::Fixed, wxLEFT, 0);

    Component::Add(notebook->GetPanel(), negerns::Proportion::Dynamic, wxGROW | wxALL);
    Component::Add(buttonSizer, negerns::Proportion::Fixed,
        wxRIGHT | wxBOTTOM | wxALIGN_RIGHT, 8);

    Component::AddControl(notebook->GetControl());

    Centre();

    cancelButton->Bind(wxEVT_BUTTON, &PropertyDialog::OnCancelClicked, this);
}

bool PropertyDialog::OnInitContent()
{
    notebook->InitContent();
    return true;
}

void PropertyDialog::AddComponent(Component *c)
{
    notebook->AddComponent(c);
}

void PropertyDialog::InitNotebook()
{
    notebook = new manager::AuiNotebook();
    int notebookFlags = wxAUI_NB_TAB_SPLIT |
        wxAUI_NB_SCROLL_BUTTONS;
    notebook->SetNotebookFlags(notebookFlags);
    notebook->SetSizerBorder(8);
    notebook->SetBorder(false);
    notebook->SetLowerBorderOnly(true);
}

void PropertyDialog::OnCancelClicked(wxCommandEvent &event)
{
    event.Skip();
}

} //_ namespace gui
} //_ namespace negerns
