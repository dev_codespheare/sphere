#include <wx/settings.h>
#include <negerns/gui/auidockart.h>

namespace negerns {
namespace gui {


AuiDockArt::AuiDockArt() :
    wxAuiDefaultDockArt()
{
    wxAuiDefaultDockArt::SetColor(wxAUI_DOCKART_SASH_COLOUR, wxSystemSettings::GetColour(wxSYS_COLOUR_BTNFACE));
}

AuiDockArt::~AuiDockArt() { }

} //_ namespace gui
} //_ namespace negerns
