#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/message.h>
#include <negerns/core/log.h>
#include <negerns/core/system.h>
#include <negerns/core/output.h>
#include <negerns/data/dataaccess.h>
#include <negerns/gui/app.h>
#include <negerns/gui/mediator.h>

namespace negerns {
namespace gui {

Mediator::Mediator(FrameManager* fm) :
    Component(),
    frameManager(fm)
{
    LOG_FUNCTION();
    Component::SetMediatorInstance(this);
}

Mediator::~Mediator()
{
    LOG_FUNCTION();
    wxDELETE(frameManager);
}

void Mediator::CreateControl()
{
    LOG_FUNCTION();
    frameManager->CreateControl();
    negerns::System::Application()->SetFrameManager(frameManager);
}

void Mediator::PostCreateControl()
{
    LOG_FUNCTION();
    frameManager->PostCreateControl();
}

void Mediator::DestroyControl()
{
    LOG_FUNCTION();
    frameManager->DestroyControl();
}

bool Mediator::OnInitContent()
{
    LOG_FUNCTION();
    // NOTE:
    //
    // The following call should not be concerned with the internals of the
    // FrameManager class. It must be called like frameManager->InitContent().
    frameManager->Component::InitContent();
    return true;
}

void Mediator::ApplyConfiguration()
{
    LOG_FUNCTION();
    frameManager->ApplyConfiguration();
}

bool Mediator::LogIn(const negerns::data::DataSource &ds)
{
    bool status = false;
    if (negerns::data::DataAccess::IsInitialized()) {
        status = negerns::data::DataAccess::IsConnected(0);
    }
    if (status) {
        negerns::Message::Info("You are currently logged in.");
        return status;
    }
    status = negerns::data::DataAccess::Test(ds);
    if (status) {
        // Keep the data source only if log in is successful.
        // Do not record unsuccessful data source connections.
        log::info("Setting data sources...");
        negerns::data::DataAccess::Add(ds, true);
        negerns::data::DataAccess::Init();
#if 0
        n::Message::Info("Log in successful.");
#endif
        std::string user(ds.GetUser());
        log::info("Database Login: Successful (user %1).", user);
        negerns::Output::Send("Database Login: Successful (%1).\n", user);
        InitContent();
    } else {
#if 0
        n::Message::Warning("Log in failed!", da->GetTestResult());
#endif
        std::string err(negerns::Format("Database Login: Failed (%1).\n"
            "  Error: %2.",
            ds.GetUser(),
            negerns::data::DataAccess::GetTestResult()));
        log::info(err);
        negerns::Output::Send(err);
        negerns::Output::Send();
    }
    return status;
}

#ifdef _DEBUG
bool Mediator::AutoLogIn()
{
    return true;
}
#endif

bool Mediator::LogOut()
{
    LOG_FUNCTION();
    if (negerns::data::DataAccess::IsInitialized()) {
        negerns::data::DataAccess::UnInit();
    }
    return true;
}

void Mediator::OnLogOut(wxCommandEvent&)
{
    LOG_FUNCTION();
    if (negerns::data::DataAccess::IsConnected(0)) {
        LogOut();
        negerns::Message::Info("You are now logged out.");
    } else {
        negerns::Message::Warning("You are not logged in.");
    }
}

void Mediator::ProcessToolBarClick(wxCommandEvent &e)
{ }

} //_ namespace gui
} //_ namespace negerns
