#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <wx/sizer.h>
#include <wx/aui/framemanager.h>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/gui/pane.h>
#include <negerns/gui/component.h>
#include <negerns/gui/bar/toolbar.h>

namespace negerns {
namespace gui {

Pane::Pane() :
    AuiComponent(),
    manager::AuiNotebook(),
    paneInfo(new wxAuiPaneInfo())
{
    paneInfo->name = wxEmptyString;
    paneInfo->caption = wxEmptyString;
    paneInfo->CaptionVisible(true);
    paneInfo->CloseButton(true);
    paneInfo->MaximizeButton(true);
    paneInfo->PinButton(true);
    paneInfo->PaneBorder(false);
    paneInfo->Movable(true);
    paneInfo->Floatable(true);
    paneInfo->DockFixed(false);
    paneInfo->Dockable(true);
    paneInfo->RightDockable(true);
    paneInfo->LeftDockable(true);
    paneInfo->TopDockable(true);
    paneInfo->BottomDockable(true);

    // Set the system default minimum pane size horizontally and vertically
    paneInfo->MinSize(wxSize(120, 120));
}

Pane::~Pane() { }

void Pane::CreateControl()
{
    LOG_FUNCTION();
    manager::AuiNotebook::CreateControl();
}

void Pane::PostCreateControl()
{
    LOG_FUNCTION();
    manager::AuiNotebook::PostCreateControl();
}

void Pane::DestroyControl()
{
    LOG_FUNCTION();
    manager::AuiNotebook::DestroyControl();
}

bool Pane::OnInitContent()
{
    LOG_FUNCTION();
    return manager::AuiNotebook::OnInitContent();
}

void Pane::ApplyConfiguration()
{
    LOG_FUNCTION();
    manager::AuiNotebook::ApplyConfiguration();
}

wxWindow* Pane::GetControl()
{
    return manager::AuiNotebook::GetControl();
}

void Pane::SetName(const wxString& name)
{
    paneInfo->name = name;
    paneInfo->caption = name;
}

wxString Pane::GetName(void) const
{
    return paneInfo->name;
}

bool Pane::ToggleDisplay(void)
{
    bool isShown = IsVisible();
    isShown ? Hide() : Show();
    return (!isShown);
}

bool Pane::IsVisible(void)
{
    auto *auimgr = wxAuiManager::GetManager(Component::GetPanel());
    auto name = GetName();
    ASSERT(!name.empty(), "Pane name is empty.");
    auto &paneInfo = auimgr->GetPane(name);
    return paneInfo.IsShown();
}

void Pane::Show(void)
{
    auto *auimgr = wxAuiManager::GetManager(Component::GetPanel());
    auto name = GetName();
    ASSERT(!name.empty(), "Pane name is empty.");
    auto &paneInfo = auimgr->GetPane(name);
    if (!paneInfo.IsShown()) {
        // Make sure the pane will be visible in the frame.
        // Compare the frame size with the pane's best size.
        // The best size was saved before pane was hidden.
        auto *frame = auimgr->GetManagedWindow();
        wxSize frameSize = frame->GetSize();
        if (paneInfo.best_size.GetWidth() > frameSize.GetWidth() ||
            paneInfo.best_size.GetHeight() > frameSize.GetHeight()) {
                paneInfo.MinSize(wxSize(frameSize.GetWidth() / 3, frameSize.GetHeight() / 3));
        } else {
            paneInfo.MinSize(paneInfo.best_size);
        }
        paneInfo.Show();
        auimgr->Update();
        // Set minimum pane size allowed.
        //
        // Since autohide is implemented, the minimum allowed size of
        // the pane is affected. The requirement is that the user should
        // not be allowed to resize the pane as small as when it is
        // minimized. This is so that the user is not confused when a
        // pane is minimized or just resized to its smallest.
        //
        // The pane size when minimized is 30 pixels. That is either in the
        // y or x axis, depending on the orientation of the tabs.
        //
        // When the pane is resized but not minimized the smallest possible
        // should be greater than 30 pixels. This value is just a suggestion.
        // It could be acquired from a configuration setting if necessary.
        //
        // NOTE:
        // This might affect the calculation of the pane above.
        //paneInfo.MinSize(120, 120);
    }
    //UpdateSetting(true);
}

void Pane::Hide(void)
{
    auto *auimgr = wxAuiManager::GetManager(Component::GetPanel());
    auto name = GetName();
    ASSERT(!name.empty(), "Pane name is empty.");
    auto &paneInfo = auimgr->GetPane(name);
    if (paneInfo.IsShown()) {
        // Save the current window size so that we can use
        // it to restore the window to the same size on show.
        ASSERT(paneInfo.window, "PaneInfo window is null.");
        wxSize size = paneInfo.window->GetClientSize();
        paneInfo.BestSize(size);
        paneInfo.Hide();
        auimgr->Update();
    }
    //UpdateSetting(false);
}

void Pane::TogglePaneCaption(void)
{
    auto *auimgr = wxAuiManager::GetManager(Component::GetPanel());
    auto name = GetName();
    ASSERT(!name.empty(), "Pane name is empty.");
    auto &paneInfo = auimgr->GetPane(name);
    paneInfo.CaptionVisible(!paneInfo.HasCaption());
    auimgr->Update();
}

void Pane::ShowPaneCaption(bool status)
{
    auto *auimgr = wxAuiManager::GetManager(Component::GetPanel());
    auto name = GetName();
    ASSERT(!name.empty(), "Pane name is empty.");
    auto &paneInfo = auimgr->GetPane(name);
    paneInfo.CaptionVisible(status);
    auimgr->Update();
}

void Pane::HidePaneCaption(void)
{
    auto *auimgr = wxAuiManager::GetManager(Component::GetPanel());
    auto name = GetName();
    ASSERT(!name.empty(), "Pane name is empty.");
    auto &paneInfo = auimgr->GetPane(name);
    paneInfo.CaptionVisible(false);
    auimgr->Update();
}

bool Pane::IsPaneCaptionVisible()
{
    auto *auimgr = wxAuiManager::GetManager(Component::GetPanel());
    auto name = GetName();
    ASSERT(!name.empty(), "Pane name is empty.");
    auto &paneInfo = auimgr->GetPane(name);
    return paneInfo.HasCaption();
}

} //_ namespace gui
} //_ namespace negerns
