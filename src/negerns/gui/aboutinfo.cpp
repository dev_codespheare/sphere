#include <negerns/gui/aboutinfo.h>

namespace negerns {
namespace gui {

AboutInfo::AboutInfo()
{
    edition = std::string();
#ifdef __WINDOWS__
#  ifdef _MSC_VER
    // Must check for 64-bit defines first because wxWidgets
    // also defines 32-bit symbols even when building 64-bit
#   if defined(__WIN64__) || defined(_WIN64)
    platform = "64-bit";
#   else
    platform = "32-bit";
#   endif
#  endif
#endif

#ifdef _DEBUG
    configuration = "Debug";
    is_debug = true;
#else
    configuration = "Release";
    is_debug = false;
#endif
}

AboutInfo::~AboutInfo() { }

void AboutInfo::SetName(const std::string& name)
{
    about.SetName(name);
}

std::string AboutInfo::GetName() const
{
    return about.GetName().ToStdString();
}

void AboutInfo::SetVersion(const std::string& version,
                const std::string& longVersion)
{
    about.SetVersion(version, longVersion);
}

bool AboutInfo::HasVersion() const
{
    return about.HasVersion();
}

std::string AboutInfo::GetVersion() const
{
    return about.GetVersion().ToStdString();
}

std::string AboutInfo::GetLongVersion() const
{
    return about.GetLongVersion().ToStdString();
}

void AboutInfo::SetDescription(const std::string& desc)
{
    about.SetDescription(desc);
}

bool AboutInfo::HasDescription() const
{
    return about.HasDescription();
}

std::string AboutInfo::GetDescription() const
{
    return about.GetDescription().ToStdString();
}

void AboutInfo::SetCopyright(const std::string& copyright)
{
    about.SetCopyright(copyright);
}

bool AboutInfo::HasCopyright() const
{
    return about.HasCopyright();
}

std::string AboutInfo::GetCopyright() const
{
    return about.GetCopyright().ToStdString();
}

void AboutInfo::SetLicense(const std::string& licence)
{
    about.SetLicense(licence);
}

bool AboutInfo::HasLicense() const
{
    return about.HasLicence();
}

std::string AboutInfo::GetLicense() const
{
    return about.GetLicence().ToStdString();
}

void AboutInfo::SetIcon(const wxIcon& icon)
{
    about.SetIcon(icon);
}

bool AboutInfo::HasIcon() const
{
    return about.HasIcon();
}

wxIcon AboutInfo::GetIcon() const
{
    return about.GetIcon();
}

void AboutInfo::SetWebSite(const std::string& url, const std::string& desc)
{
    about.SetWebSite(url, desc);
}

bool AboutInfo::HasWebSite() const
{
    return about.HasWebSite();
}

std::string AboutInfo::GetWebSiteURL() const
{
    return about.GetWebSiteURL().ToStdString();
}

std::string AboutInfo::GetWebSiteDescription() const
{
    return about.GetWebSiteDescription().ToStdString();
}

void AboutInfo::SetDevelopers(const wxArrayString& developers)
{
    about.SetDevelopers(developers);
}

void AboutInfo::AddDeveloper(const std::string& developer)
{
    about.AddDeveloper(developer);
}

bool AboutInfo::HasDevelopers() const
{
    return about.HasDevelopers();
}

const wxArrayString& AboutInfo::GetDevelopers() const
{
    return about.GetDevelopers();
}

void AboutInfo::SetDocWriters(const wxArrayString& docwriters)
{
    about.SetDocWriters(docwriters);
}

void AboutInfo::AddDocWriter(const std::string& docwriter)
{
    about.AddDocWriter(docwriter);
}

bool AboutInfo::HasDocWriters() const
{
    return about.HasDocWriters();
}

const wxArrayString& AboutInfo::GetDocWriters() const
{
    return about.GetDocWriters();
}

void AboutInfo::SetArtists(const wxArrayString& artists)
{
    about.SetArtists(artists);
}

void AboutInfo::AddArtist(const std::string& artist)
{
    about.AddArtist(artist);
}

bool AboutInfo::HasArtists() const
{
    return about.HasArtists();
}

const wxArrayString& AboutInfo::GetArtists() const
{
    return about.GetArtists();
}

void AboutInfo::SetTranslators(const wxArrayString& translators)
{
    about.SetTranslators(translators);
}

void AboutInfo::AddTranslator(const std::string& translator)
{
    about.AddTranslator(translator);
}

bool AboutInfo::HasTranslators() const
{
    return about.HasTranslators();
}

const wxArrayString& AboutInfo::GetTranslators() const
{
    return about.GetTranslators();
}

bool AboutInfo::IsSimple() const
{
    return !about.HasWebSite() && !about.HasIcon() && !about.HasLicence();
}

std::string AboutInfo::GetDescriptionAndCredits() const
{
    return about.GetDescriptionAndCredits().ToStdString();
}

std::string AboutInfo::GetCopyrightToDisplay() const
{
    return about.GetCopyrightToDisplay().ToStdString();
}

} //_ namespace gui
} //_ namespace negerns
