#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/gui/datepickerctrl.h>

namespace negerns {
namespace gui {

DatePickerCtrl::DatePickerCtrl() : wxDatePickerCtrl() { }

DatePickerCtrl::DatePickerCtrl(wxWindow *parent,
    wxWindowID id,
    const wxDateTime &value,
    const wxPoint pos,
    const wxSize& size,
    long style,
    const wxValidator& validator,
    const wxString& name) :
    wxDatePickerCtrl(parent, id, value, pos, size, style, validator, name)
{ }

#if 0
DatePickerCtrl::DatePickerCtrl(wxWindow *parent,
    const wxSize& size,
    long style,
    wxDateTime *val)
{
    wxDatePickerCtrl::Create(parent, wxID_ANY, wxDefaultDateTime, wxDefaultPosition, size, style);
    wxDatePickerCtrl::SetValidator(negerns::gui::DataGateway(val, negerns::gui::DataGateway::DatePickerCtrl, wxFILTER_EMPTY));
}
#endif

DatePickerCtrl::DatePickerCtrl(wxWindow *parent,
    const wxSize& size,
    long style) :
    wxDatePickerCtrl(parent, wxID_ANY, wxDefaultDateTime, wxDefaultPosition, size, style | wxDP_DROPDOWN)
{ }

DatePickerCtrl::~DatePickerCtrl() { }

void DatePickerCtrl::SetValidator(negerns::Var *val, long style)
{
    wxDatePickerCtrl::SetValidator(DataXfer(val, DataXfer::Control::Date, style));
}

void DatePickerCtrl::SetValidator(negerns::Var &val, long style)
{
    wxDatePickerCtrl::SetValidator(DataXfer(&val, DataXfer::Control::Date, style));
}

void DatePickerCtrl::Create(wxWindow *parent,
    wxWindowID id,
    const wxDateTime &value,
    const wxPoint pos,
    const wxSize& size,
    long style,
    const wxValidator& validator,
    const wxString& name)
{
    wxDatePickerCtrl::Create(parent, id, value, pos, size, style, validator, name);
}

#if 0
// Create without id, position and choices
void DatePickerCtrl::Create(wxWindow *parent,
    const wxSize& size,
    long style,
    wxDateTime *val)
{
    wxDatePickerCtrl::Create(parent, wxID_ANY, wxDefaultDateTime, wxDefaultPosition, size, style);
    wxDatePickerCtrl::SetValidator(negerns::gui::DataGateway(val, negerns::gui::DataGateway::DatePickerCtrl, wxFILTER_EMPTY));
}
#endif

} //_ namespace gui
} //_ namespace negerns
