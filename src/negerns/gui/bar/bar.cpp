#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <wx/panel.h>
#include <wx/artprov.h>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/gui/auitoolbarart.h>
#include <negerns/gui/bar/bar.h>

namespace negerns {
namespace gui {
namespace bar {

Bar::Bar() :
    toolbar(nullptr)
{
    border = Bar::Border::None;

    // NOTE:
    // We use wxAUI_TB_PLAIN_BACKGROUND because we wanted to use the toolbar
    // inside an AUI tab page to match the color of the tab page background.
    // It is possible to use the gradient toolbar background but the AUI
    // toolbar is 'cut' on the last toolbar item which shows the background
    // color of the tab page. Until the AUI toolbar gradient background can be
    // displayed to occupy the whole width of its parent control then the
    // choice is to use wxAUI_TB_PLAIN_BACKGROUND.
    style =  wxAUI_TB_PLAIN_BACKGROUND | wxAUI_TB_HORZ_TEXT | wxAUI_TB_GRIPPER;
}

Bar::~Bar()
{
    wxDELETE(toolbar);
}

void Bar::CreateControl()
{
    LOG_FUNCTION();
    ASSERT(parent, "Parent should not be null.");
    auto *art = new AuiToolBarArt();
    art->SetBorder(border);
    toolbar = new AuiToolBar();
    toolbar->SetWindowStyleFlag(style);
    toolbar->SetArtProvider(art);
#ifdef UNPATCHED_15751
    toolbar->Create(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, style);
#else
    toolbar->Create(parent);
#endif
    toolbar->SetGripperVisible(false);
}

void Bar::PostCreateControl() { }

void Bar::DestroyControl()
{
    toolbar->Destroy();
}

void Bar::RefreshControl() { }

bool Bar::OnInitContent()
{
    return IControl::OnInitContent();
}

void Bar::ClearContent() { }

void Bar::RefreshContent() { }

void Bar::ApplyConfiguration() { }

void Bar::ApplyTheme() { }

bool Bar::Show(bool status)
{
    return toolbar->Show(status);
}

void Bar::Hide(void)
{
    toolbar->Show(false);
}

bool Bar::IsVisible(void) const
{
    return toolbar->IsShown();
}

wxAuiToolBarItem* Bar::AddTool(int toolId,
    const std::string& label,
    const std::string& icon,
    const std::string& shortHelpString,
    wxItemKind kind)
{
    enabledToolHelpString[toolId] = shortHelpString;
    return toolbar->AddTool(toolId, label, wxArtProvider::GetBitmap(icon), shortHelpString, kind);
}

wxAuiToolBarItem* Bar::AddLabel(int toolId, const std::string& label, const int width)
{
    return toolbar->AddLabel(toolId, label, width);
}

wxAuiToolBarItem* Bar::AddControl(wxControl* control, const std::string& label)
{
    return toolbar->AddControl(control, label);
}

wxAuiToolBarItem* Bar::AddSeparator()
{
    return toolbar->AddSeparator();
}

wxAuiToolBarItem* Bar::AddSpacer(int pixels)
{
    return toolbar->AddSpacer(pixels);
}

wxAuiToolBarItem* Bar::AddStretchSpacer(int proportion)
{
    return toolbar->AddStretchSpacer(proportion);
}

void Bar::EnableTool(int toolId, bool state)
{
    parent->Freeze();
    toolbar->EnableTool(toolId, state);
    if (state) {
        toolbar->SetToolShortHelp(toolId, enabledToolHelpString.at(toolId));
    } else {
        auto pos = disabledToolHelpString.find(toolId);
        if (pos != disabledToolHelpString.end()) {
            toolbar->SetToolShortHelp(toolId, disabledToolHelpString.at(toolId));
        }
    }
    Bar::Realize();
    // IMPORTANT:
    // Redraw the border. Without this call, the border is only redrawn up to
    // the last toolbar item.
    toolbar->GetParent()->GetSizer()->Layout();
    parent->Thaw();
}

void Bar::SetDisabledToolShortHelp(int toolId, const std::string &s)
{
    disabledToolHelpString[toolId] = s;
}

bool Bar::Realize()
{
    return toolbar->Realize();
}

} //_ namespace bar
} //_ namespace gui
} //_ namespace negerns
