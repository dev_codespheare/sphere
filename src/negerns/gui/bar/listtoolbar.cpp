#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/log.h>
#include <negerns/core/string.h>
#include <negerns/gui/artprovider.h>
#include <negerns/gui/bar/listtoolbar.h>

namespace negerns {
namespace gui {
namespace bar {

ListToolBar::ListToolBar() :
    ToolBar()
{ }

ListToolBar::~ListToolBar() { }

void ListToolBar::CreateControl()
{
    LOG_FUNCTION();
    ToolBar::CreateControl();
    // TODO: Allow name to be set from concrete classes
    AddTool(wxID_NEW, "New", ART_ICON_NEGERNS_NEW, "Create a new item");
    AddTool(wxID_EDIT, std::string(), ART_ICON_NEGERNS_EDIT, "Edit item");
    AddSeparator();
    AddTool(wxID_REFRESH, "Refresh", ART_ICON_NEGERNS_REFRESH, "Refresh display");
    Realize();
}

} //_ namespace bar
} //_ namespace gui
} //_ namespace negerns
