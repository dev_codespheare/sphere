#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/log.h>
#include <negerns/gui/artprovider.h>
#include <negerns/gui/bar/crudtoolbar.h>

namespace negerns {
namespace gui {
namespace bar {

CrudToolBar::CrudToolBar() :
    ToolBar()
{ }

CrudToolBar::~CrudToolBar() { }

void CrudToolBar::CreateControl()
{
    LOG_FUNCTION();
    ToolBar::CreateControl();
    // TODO: Allow name to be set from concrete classes
    auto *tb = ToolBar::GetControl();
    AddTool(wxID_NEW, "New", ART_ICON_NEGERNS_NEW, "Create a new item");
#if 0
    AddTool(wxID_EDIT, wxEmptyString, wxART_FILE_OPEN, wxART_MENU, "Edit Item");
#endif
    AddTool(wxID_UNDO, std::string(), ART_ICON_NEGERNS_UNDO, "Undo changes");
    AddTool(wxID_REDO, std::string(), ART_ICON_NEGERNS_REDO, "Redo changes");
    AddSeparator();
    AddTool(wxID_SAVE, "Save", ART_ICON_NEGERNS_SAVE, "Save");
    AddSeparator();
    AddTool(wxID_REFRESH, "Refresh", ART_ICON_NEGERNS_REFRESH, "Refresh display");
    Realize();

#if 0
    tb->Bind(wxEVT_TOOL, &CrudToolBar::Create, this, newtbi->GetId());
    tb->Bind(wxEVT_TOOL, &CrudToolBar::Undo, this, undotbi->GetId());
    tb->Bind(wxEVT_TOOL, &CrudToolBar::Redo, this, redotbi->GetId());
    tb->Bind(wxEVT_TOOL, &CrudToolBar::Update, this, savetbi->GetId());
    tb->Bind(wxEVT_TOOL, &CrudToolBar::Read, this, refreshtbi->GetId());
#endif
}

void CrudToolBar::DestroyControl()
{

}

} //_ namespace bar
} //_ namespace gui
} //_ namespace negerns
