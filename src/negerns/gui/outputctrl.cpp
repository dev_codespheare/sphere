#include <wx/stc/stc.h>
#include <negerns/gui//outputctrl.h>

namespace negerns {
namespace gui {

OutputCtrl::OutputCtrl(wxWindow* parent) :
    OutputSink(),
    control(nullptr)
{
    const unsigned int max_margin = 5;
    const unsigned int margin_width = 4;

    // NOTE:
    // We use wxNO_BORDER because the control has a border by default.
    // We wanted the look to be consistent.
    int style = wxNO_BORDER | wxVSCROLL;
    control = new wxStyledTextCtrl(parent, wxID_ANY, wxDefaultPosition,
        wxDefaultSize, style, "output_control");

    control->SetUndoCollection(false);
    control->SetUseHorizontalScrollBar(true);
    control->SetScrollWidthTracking(true);
    // Just settting the state of the scroll width tracking isn't enough.
    // Maybe it's a bug in Scintilla or not, I am not aware. But setting
    // the scroll width below correctly shows/hides the scrollbar and 
    // eliminates the scrollbar flickering when setting the scroll width
    // in a wxSizeEvent.
    control->SetScrollWidth(300);

    // Collapse all margins
    for (int i = 0; i < max_margin; ++i) {
        control->SetMarginWidth(i, 0);
    }

    // Drawing
#ifdef __WXMAC__
    // turning off these two greatly improves performance on Mac
    control->SetTwoPhaseDraw(false);
    control->SetBufferedDraw(false);
#elif defined (__WXGTK__)
    control->SetTwoPhaseDraw(true);
    control->SetBufferedDraw(false);
#elif defined(__WXMSW__)
    // Two phase draw must be true for modern indicators
    control->SetTwoPhaseDraw(true);
    control->SetBufferedDraw(true);
#endif
    control->SetMarginLeft(margin_width);
    control->SetReadOnly(true);
    control->UsePopUp(false);
    control->SetWrapVisualFlags(wxSTC_WRAPVISUALFLAG_END);
    control->SetWrapVisualFlagsLocation(wxSTC_WRAPVISUALFLAGLOC_END_BY_TEXT);
    control->SetWrapStartIndent(wxSTC_WRAPINDENT_SAME);

    control->SetCodePage(wxSTC_CP_UTF8);
    control->SetLexer(wxSTC_LEX_NULL);
    //setBulkWrite(false);
}

OutputCtrl::~OutputCtrl()
{
    wxDELETE(control);
}

void OutputCtrl::Send(const std::string &s)
{
    control->SetReadOnly(false);
    control->AppendText(s + '\n');
    control->SetReadOnly(true);

    control->ScrollToEnd();
    control->GotoLine(control->GetLineCount());
}

void OutputCtrl::Clear()
{
    control->SetReadOnly(false);
    control->ClearAll();
    control->SetReadOnly(true);
}

} //_ namespace gui
} //_ namespace negerns
