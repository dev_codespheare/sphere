#include <negerns/gui/statictext.h>

namespace negerns {
namespace gui {

StaticText::StaticText() : wxStaticText() { }

StaticText::StaticText(wxWindow *parent,
    wxWindowID id,
    const wxString &label,
    const wxPoint pos,
    const wxSize& size,
    long style,
    const wxString& name) :
    wxStaticText(parent, id, label, pos, size, style, name)
{ }

StaticText::StaticText(wxWindow *parent,
    const wxString &label,
    long style) :
    wxStaticText(parent, wxID_STATIC, label, wxDefaultPosition, wxDefaultSize, style)
{ }

StaticText::~StaticText() { }

} //_ namespace gui
} //_ namespace negerns
