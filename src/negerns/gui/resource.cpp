#include <negerns/core/memory.h>
#include <negerns/core/debug.h>
#include <negerns/gui/resource.h>

namespace negerns {
namespace gui {

class Resource::ResourceImp
{
public:
    ResourceImp() = default;
    virtual ~ResourceImp() = default;

    //! Resource entries of a string identifier and resource filename.
    typedef std::map<std::string, std::string> ResourceMap;

    //! Map of resource entries.
    ResourceMap resourceMap;

    //! Relative path of resource files.
    //!
    //! If path is empty then the resource files are searched in the directory
    //! of the executable file.
    std::string path;
};

Resource::ResourceImp* Resource::Implementation(Resource::ResourceImp *p)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static Resource::ResourceImp *instance = nullptr;

    if (p != nullptr && instance == nullptr) {
        // Assignment
        instance = p;
        return nullptr;
    } else if (p == nullptr && instance != nullptr) {
        // Query
        return instance;
    } else if (p != nullptr && instance != nullptr) {
        // Reassignment
        FAIL("Attempt to reassign a value to a single-instance object.");
        return nullptr;
    } else if (p == nullptr && instance == nullptr) {
        // Possible query or an attempt to initialize object with a null value.
        // But we do not allow initialization to null, so, this must be a query
        // returning the uninitialized value (nullptr).
        return nullptr;
    } else {
        throw std::runtime_error("Should not execute this code.");
        return nullptr;
    }
}

Resource::Resource()
{
    // Create implementation object on first instance of this class.
    // This allows logs to be buffered in the implementation instance.
    if (Resource::Implementation() == nullptr) {
        Resource::Implementation(new Resource::ResourceImp());
    }
}

Resource::~Resource() { }

void Resource::Add(const std::string& id, const std::string& value)
{
    auto *ptr = Resource::Implementation();
    ASSERT(ptr, "Resource implementation is null.\n"
        "Create a Resource object first to initialize the implementation object.");
    ResourceImp::ResourceMap::iterator it = ptr->resourceMap.find(id);
    if (it != ptr->resourceMap.end() && it->first == id) {
        FAIL("Resource id '%1' is already found in Resource Map.", id);
    }
    if (it != ptr->resourceMap.end() && it->second == value) {
        FAIL("Resource value '%1' is already found in Resource Map.", value);
    }
    ptr->resourceMap.insert(ResourceImp::ResourceMap::value_type(id, value));
}

void Resource::Remove(const std::string& id)
{
    auto *ptr = Resource::Implementation();
    ASSERT(ptr, "Resource implementation is null.\n"
        "Create a Resource object first to initialize the implementation object.");
    ASSERT(Resource::Find(id), "Resource id %1 not found in Resource Map.", id);
    ptr->resourceMap.erase(id);
}

void Resource::Clear()
{
    auto *ptr = Resource::Implementation();
    ASSERT(ptr, "Resource implementation is null.\n"
        "Create a Resource object first to initialize the implementation object.");
    ptr->resourceMap.clear();
}

bool Resource::Find(const std::string& id)
{
    auto *ptr = Resource::Implementation();
    ASSERT(ptr, "Resource implementation is null.\n"
        "Create a Resource object first to initialize the implementation object.");
    return (ptr->resourceMap.find(id) != ptr->resourceMap.end());
}

std::string Resource::Get(const std::string& id)
{
    auto *ptr = Resource::Implementation();
    ASSERT(ptr, "Resource implementation is null.\n"
        "Create a Resource object first to initialize the implementation object.");
    ResourceImp::ResourceMap::iterator it = ptr->resourceMap.find(id);
    ASSERT(it != ptr->resourceMap.end(), "Resource id %1 not found in Resource Map.", id);
    std::string result(it->second);
    return std::move(result);
}

void Resource::SetPath(const std::string& p)
{
    auto *ptr = Resource::Implementation();
    ASSERT(ptr, "Resource implementation is null.\n"
        "Create a Resource object first to initialize the implementation object.");
    ptr->path = p;
}

std::string Resource::GetPath()
{
    auto *ptr = Resource::Implementation();
    ASSERT(ptr, "Resource implementation is null.\n"
        "Create a Resource object first to initialize the implementation object.");
    return ptr->path;
}

std::string Resource::BuildUrl(const std::string &resbinfile, const std::string &resfile)
{
    auto *ptr = Resource::Implementation();
    ASSERT(ptr, "Resource implementation is null.\n"
        "Create a Resource object first to initialize the implementation object.");
    return negerns::Format("%1#zip:%2", Resource::Get(resbinfile), resfile);
}

void Resource::Shutdown()
{
    auto *ptr = Resource::Implementation();
    ASSERT(ptr, "Resource implementation is null.\n"
        "Create a Resource object first to initialize the implementation object.");
    if (ptr != nullptr) {
        ptr->resourceMap.clear();
        negerns::reset(ptr);
    }
}

} //_ namespace gui
} //_ namespace negerns
