#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <wx/clntdata.h>
#include <negerns/core/defs.h>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/message.h>
#include <negerns/core/string/convert.h>
#include <negerns/data/recordset.h>
#include <negerns/gui/choicectrl.h>

namespace negerns {
namespace gui {

ChoiceCtrl::ChoiceCtrl() :
    wxChoice()
{ }

#if 0
ChoiceCtrl::ChoiceCtrl(wxWindow *parent,
    wxWindowID id,
    const wxPoint pos,
    const wxSize& size,
    int n, const wxString choices[],
    long style,
    const wxValidator& validator,
    const wxString& name) :
    wxChoice(parent, id, pos, size, n, choices, style, validator, name)
{ }


ChoiceCtrl::ChoiceCtrl(wxWindow *parent,
    const wxSize& size,
    long style,
    std::string *val) :
    wxChoice(parent, wxID_ANY, wxDefaultPosition, size, 0, NULL, style)
{
    SetValidator(negerns::gui::DataGateway(val, negerns::gui::DataGateway::ChoiceCtrl, wxFILTER_EMPTY));
}
#endif

ChoiceCtrl::ChoiceCtrl(wxWindow *parent,
    const wxSize& size,
    long style) :
    wxChoice(parent, wxID_ANY, wxDefaultPosition, size, 0, NULL, style)
{ }

ChoiceCtrl::~ChoiceCtrl() { }

void ChoiceCtrl::SetValidator(negerns::Var *val, long style)
{
    wxChoice::SetValidator(DataXfer(val, DataXfer::Control::Choice, style));
}

void ChoiceCtrl::SetValidator(negerns::Var &val, long style)
{
    wxChoice::SetValidator(DataXfer(&val, DataXfer::Control::Choice, style));
}

#if 0
void ChoiceCtrl::Create(wxWindow *parent,
    wxWindowID id,
    const wxPoint pos,
    const wxSize& size,
    int n, const wxString choices[],
    long style,
    const wxValidator& validator,
    const wxString& name)
{
    wxChoice::Create(parent, id, pos, size, n, choices, style, validator, name);
}

void ChoiceCtrl::Create(wxWindow *parent,
    const wxSize& size,
    long style,
    std::string *val)
{
    wxChoice::Create(parent, wxID_ANY, wxDefaultPosition, size, 0, NULL, style);
    SetValidator(negerns::gui::DataGateway(val, negerns::gui::DataGateway::ChoiceCtrl, wxFILTER_EMPTY));
}
#endif

bool ChoiceCtrl::SetValue(const std::string &value)
{
    bool status = false;
    std::size_t count = wxChoice::GetCount();
    if (HasClientObjectData()) {
        wxStringClientData *data = nullptr;
        wxString datastr;
        for (std::size_t i = 0; i < count; ++i) {
            data = (wxStringClientData *) GetClientObject(i);
            datastr = data->GetData();
            if (datastr == value) {
                wxChoice::SetSelection(i);
                status = true;
                break;
            }
        }
    } else {
        int n = wxChoice::FindString(value);
        if (n != wxNOT_FOUND) {
            wxChoice::SetSelection(n);
            status = true;
        }
    }
        
    return status;
}

negerns::Var ChoiceCtrl::GetValue() const
{
    negerns::Var value;
    int current = wxChoice::GetSelection();
    if (current != wxNOT_FOUND) {
        std::string s;
        if (HasClientObjectData()) {
            auto *data = (wxStringClientData *) GetClientObject(current);
            s = data->GetData().ToStdString();
        } else {
            s = wxChoice::GetString(current).ToStdString();
        }
        switch (valueColumn.type) {
            case n::data::DataType::Int16:
                value = string::stos(s);
                break;
            case n::data::DataType::Int32:
                value = string::stoi(s);
                break;
            case n::data::DataType::Int64:
                value = string::stoll(s);
                break;
            case n::data::DataType::UInt16:
                value = string::stous(s);
                break;
            case n::data::DataType::UInt32:
                value = string::stoui(s);
                break;
            case n::data::DataType::UInt64:
                value = string::stoull(s);
                break;
            case n::data::DataType::String:
                value = s;
                break;
            default:
                value = s;
        }
    }
    return value;
}

negerns::Var ChoiceCtrl::GetValue(std::size_t index) const
{
    negerns::Var value;
    n::Message::ErrorIf(IsEmpty(), "Choice control have no items.");
    if (GetCount() > 0) {
        std::string s;
        if (HasClientObjectData()) {
            auto *data = (wxStringClientData *) GetClientObject(index);
            s = data->GetData().ToStdString();
        } else {
            s = wxChoice::GetString(index).ToStdString();
        }
        switch (valueColumn.type) {
            case n::data::DataType::Int16:
                value = string::stos(s);
                break;
            case n::data::DataType::Int32:
                value = string::stoi(s);
                break;
            case n::data::DataType::Int64:
                value = string::stoll(s);
                break;
            case n::data::DataType::UInt16:
                value = string::stous(s);
                break;
            case n::data::DataType::UInt32:
                value = string::stoui(s);
                break;
            case n::data::DataType::UInt64:
                value = string::stoull(s);
                break;
            case n::data::DataType::String:
                value = s;
                break;
            default:
                value = s;
        }
    }
    return value;
}

std::size_t ChoiceCtrl::SetValues(const std::vector<std::string> &items)
{
    std::size_t count = items.size();
    if (count) {
        for (auto &s : items) {
            wxChoice::Append(s);
        }
    }
    return count;
}

std::size_t ChoiceCtrl::SetValuesMap(const std::map<std::size_t, std::string> &items)
{
    std::size_t count = items.size();
    if (count) {
        wxString id;
        for (auto &item : items) {
            id = string::to_string(item.first);
            wxChoice::Append(item.second, new wxStringClientData(id));
        }
    }
    return count;
}

std::size_t ChoiceCtrl::SetValuesMap(const std::map<std::string, std::string> &items)
{
    std::size_t count = items.size();
    if (count) {
        for (auto &item : items) {
            wxChoice::Append(item.second, new wxStringClientData(item.first));
        }
    }
    return count;
}

std::size_t ChoiceCtrl::SetValues(negerns::data::DataStore *ds,
    const negerns::data::Column &label)
{
    ASSERT(ds, "DataStore object cannot be null.");
    labelColumn = label;
    valueColumn = label;
    auto count = ds->RowCount();
    if (count > 0) {
        std::string s;
        for (decltype(count) i = 0; i < count; ++i) {
            s = ds->original(i, labelColumn.position).convert<std::string>();
            Append(s);
        }
    }
    return count;
}

std::size_t ChoiceCtrl::SetValues(negerns::data::DataStore *ds,
    const std::string &label)
{
    ASSERT(ds, "DataStore object cannot be null.");
    labelColumn = ds->column.Get(label);
    valueColumn = labelColumn;
    auto count = ds->RowCount();
    if (count > 0) {
        std::string s;
        for (decltype(count) i = 0; i < count; ++i) {
            s = ds->original(i, labelColumn.position).convert<std::string>();
            Append(s);
        }
    }
    return count;
}

std::size_t ChoiceCtrl::SetValues(negerns::data::DataStore *ds,
    const negerns::data::Column &label,
    const negerns::data::Column &value)
{
    ASSERT(ds, "DataStore object cannot be null.");
    labelColumn = label;
    valueColumn = value;
    auto count = ds->RowCount();
    if (count > 0) {
        wxString s;
        wxString v;
        int n = 0;
        for (decltype(count) i = 0; i < count; ++i) {
            s = ds->original(i, labelColumn.position).convert<std::string>();
            v = ds->original(i, valueColumn.position).convert<std::string>();

            n = Append(s, new wxStringClientData(v));
            ASSERT(n != wxNOT_FOUND, "Selection item not appended.");
        }

#ifdef _DEBUG
        // Test
        negerns::Variant testv;
        negerns::Variant testo;
        for (decltype(count) i = 0; i < count; ++i) {
            testo = ds->original(i, valueColumn.position);
            auto *data = (wxStringClientData *) GetClientObject(i);
            testv = data->GetData().ToStdString();
        }
#endif
    }
    return count;
}

std::size_t ChoiceCtrl::SetValues(negerns::data::DataStore *ds,
    const std::string &label,
    const std::string &value)
{
    ASSERT(ds, "DataStore object cannot be null.");
    labelColumn = ds->column.Get(label);
    valueColumn = ds->column.Get(value);
    auto count = ds->RowCount();
    if (count > 0) {
        wxString s;
        wxString v;
        int n = 0;
        for (decltype(count) i = 0; i < count; ++i) {
            s = ds->original(i, labelColumn.position).convert<std::string>();
            v = ds->original(i, valueColumn.position).convert<std::string>();

            n = Append(s, new wxStringClientData(v));
            ASSERT(n != wxNOT_FOUND, "Selection item not appended.");
        }

#ifdef _DEBUG
        // Test
        negerns::Variant testv;
        negerns::Variant testo;
        for (decltype(count) i = 0; i < count; ++i) {
            testo = ds->original(i, valueColumn.position);
            auto *data = (wxStringClientData *) GetClientObject(i);
            testv = data->GetData().ToStdString();
        }
#endif
    }
    return count;
}

} //_ namespace gui
} //_ namespace negerns
