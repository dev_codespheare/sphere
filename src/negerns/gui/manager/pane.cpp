#include <wx/aui/aui.h>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/gui/manager/pane.h>

namespace negerns {
namespace gui {
namespace manager {

Pane::Pane() :
    AuiComponent()
{
    LOG_FUNCTION();
}

Pane::~Pane()
{
    LOG_FUNCTION();
#if EXCLUDE_TEST
    int count = panes.size();
    if (count > 0) {
        for (int i = 0; i < count; ++i) {
            wxDELETE(panes[i]);
        }
    } else {
        LOG_WARNING("There are no panes to process.");
    }
    panes.clear();
#endif
}

void Pane::CreateControl()
{
    LOG_FUNCTION();
    ASSERT(parent, "Parent should not be null.");
    for (auto pane : panes) {
        pane->SetParent(parent);
        pane->CreateControl();
        auiManager->AddPane(pane->GetPanel(), *(pane->GetPaneInfo()));
    }
}

void Pane::PostCreateControl()
{
    LOG_FUNCTION();
    // Position and display the panes
    // We must show the panes first. Configuration settings will be
    // applied on \c ApplyConfiguration.
    for (auto pane : panes) {
        auiManager->GetPane(pane->GetName()).Show();
        pane->PostCreateControl();
    }
    auiManager->Update();
}

void Pane::DestroyControl()
{
    LOG_FUNCTION();
    for (auto pane : panes) {
        auiManager->DetachPane(pane->GetPanel());
        pane->DestroyControl();
    }
}

bool Pane::OnInitContent()
{
    LOG_FUNCTION();
    IControl::OnInitContent();
    for (auto pane : panes) {
        pane->InitContent();
    }
    return true;
}

void Pane::ApplyConfiguration()
{
    LOG_FUNCTION();
    for (auto pane : panes) {
        pane->ApplyConfiguration();
    }
}

std::size_t Pane::AddPane(negerns::gui::Pane* panebase)
{
    panes.push_back(panebase);
    return panes.size() - 1;
}

negerns::gui::Pane* Pane::GetPane(std::size_t index)
{
    ASSERT(panes.size() > index, string::Format("Pane index %1 is out of range.", index));
    return panes.size() > index ? panes.at(index) : nullptr;
}

std::size_t Pane::GetPaneCount() const
{
    return panes.size();
}

void Pane::AddPage(negerns::gui::Pane *pane,
    negerns::gui::Component *component,
    bool activate)
{
    pane->AddPage(component, activate);
}

void Pane::SetParent(wxWindow* p)
{
    parent = p;
}


#if EXCLUDE
Component* PaneManagerBase::GetPaneComponent(const wxString&)
{
    auiManager->GetPane(panes[i]->GetName())
}
#endif

} //_ namespace manager
} //_ namespace gui
} //_ namespace negerns
