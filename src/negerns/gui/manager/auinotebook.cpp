#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <algorithm>
#include <wx/sizer.h>
#include <wx/aui/framemanager.h>
#include <negerns/core/defs.h>
#include <negerns/core/debug.h>
#include <negerns/gui/auitabart.h>
#include <negerns/gui/manager/auinotebook.h>

namespace negerns {
namespace gui {
namespace manager {

#if 0
MapComponents AuiNotebook::allComponents;
#endif
AuiNotebook::AuiNotebook() :
    Component(),
    notebook(nullptr)
{
    notebookFlags = wxAUI_NB_TAB_SPLIT |
        wxAUI_NB_TAB_MOVE |
        wxAUI_NB_TAB_EXTERNAL_MOVE |
        wxAUI_NB_SCROLL_BUTTONS |
#if 0
        wxAUI_NB_CLOSE_ON_ACTIVE_TAB |
#endif
        // This will make Gtk spit some warnings
        wxAUI_NB_WINDOWLIST_BUTTON;
    sizerFlags = wxEXPAND | wxALL;
    sizerBorder = wxBORDER_DEFAULT;
    drawBorder = true;
    drawLowerBorderOnly = false;
    tabControlHeight = 29;
}

AuiNotebook::~AuiNotebook()
{
    // TODO: Remove from notebook first before destroying the components.
    for(auto component : components) {
        wxDELETE(component);
    }
}

void AuiNotebook::CreateControl()
{
    Component::CreateControl();
    wxWindow* panel = (wxWindow*) Component::GetPanel();
    ASSERT(panel, "Panel should not be null.");

    notebook = new wxAuiNotebook(panel,
        wxID_ANY,
        wxDefaultPosition,
        wxDefaultSize,
        notebookFlags);

    auto *art = new AuiTabArt();
    art->SetBorder(drawBorder);
    art->SetLowerBorderOnly(drawLowerBorderOnly);

    notebook->SetArtProvider(art);
    notebook->SetTabCtrlHeight(tabControlHeight);

    notebook->Freeze();
    for (auto component : components) {
        component->SetParent(notebook);
        component->CreateControl();
        notebook->AddPage(component->GetPanel(), component->GetName(), false,
            component->GetBitmap());
    }
    notebook->Thaw();

    Component::Add(notebook, negerns::Proportion::Changeable,
        AuiNotebook::sizerFlags, AuiNotebook::sizerBorder);
}

void AuiNotebook::PostCreateControl()
{
    for (auto component : components) {
        component->PostCreateControl();
    }
}

void AuiNotebook::DestroyControl()
{
    for (auto component : components) {
        component->DestroyControl();
    }
}

bool AuiNotebook::OnInitContent()
{
    for (auto component : components) {
        component->InitContent();
    }
    return true;
}

void AuiNotebook::ApplyConfiguration()
{
    for (auto component : components) {
        component->ApplyConfiguration();
    }
}

void AuiNotebook::AddComponent(Component* component)
{
    components.push_back(component);
#if 0
    AuiNotebook::allComponents[component->GetName().ToStdString()] = component;
#endif
}

negerns::gui::Component* AuiNotebook::GetComponent(const std::string &name)
{
    negerns::gui::Component* component = nullptr;
    if (name.empty()) {
        if (notebook->GetPageCount() > 0) {
            int n = notebook->GetSelection();
            ASSERT(n < components.size(), "Notebook and component size not in sync.");
            component = components[n];
        }
    } else {
        for (auto *c : components) {
            if (c->Identifier::is(name)) {
                component = c;
                break;
            }
        }
    }
    return component;
}

std::size_t AuiNotebook::GetComponentIndex(const std::string &name)
{
    std::size_t index = 0;
    if (name.empty()) {
        if (notebook->GetPageCount() > 0) {
            index = notebook->GetSelection();
        }
    } else {
        for (auto *c : components) {
            if (c->Identifier::is(name)) {
                break;
            } else {
                index++;
            }
        }
    }
    return index;
}

bool AuiNotebook::IsComponentPresent(const std::string &name)
{
    bool status = false;
    for (auto *c : components) {
        if (c->Identifier::is(name)) {
            status = true;
            break;
        }
    }
    return status;
}

void AuiNotebook::AddPage(Component *component, bool freeze)
{
    AddComponent(component);
    if (freeze) {
        notebook->Freeze();
    }
    component->SetParent(notebook);
    component->CreateControl();
    component->PostCreateControl();
    component->InitContent();
    component->ApplyConfiguration();
    notebook->AddPage(component->GetPanel(), component->GetName(), true,
        component->GetBitmap());
    if (freeze) {
        notebook->Thaw();
    }
}

void AuiNotebook::ClosePage()
{
    auto index = notebook->GetSelection();
    notebook->DeletePage(index);
    auto pos = std::remove(components.begin(), components.end(), components.at(index));
    components.erase(pos);
}

void AuiNotebook::ClosePage(std::size_t index)
{
    auto count = notebook->GetPageCount();
    ASSERT(count > index, "Index is out of range. Count: %1, Index: %2", count, index);
    if (count > index) {
        notebook->DeletePage(index);
        auto pos = std::remove(components.begin(), components.end(), components.at(index));
        components.erase(pos);
    }
}

void AuiNotebook::CloseAllPages()
{
    notebook->DeleteAllPages();
    components.clear();
}

std::size_t AuiNotebook::GetCount() const
{
    return notebook->GetPageCount();
}

void AuiNotebook::SetActivePage(wxWindow* page)
{
    throw std::runtime_error("Not implemented.");
}

void AuiNotebook::SetActivePage(size_t n)
{
    notebook->SetSelection(n);
}

void AuiNotebook::SetBorder(bool b)
{
    drawBorder = b;
}

void AuiNotebook::SetLowerBorderOnly(bool b)
{
    // TODO: This should also set the drawBorder flag.
    //       Might need to modify/update the border drawing implementation
    //       if this will be changed.
    drawLowerBorderOnly = b;
}

void AuiNotebook::SetSizerFlags(int f)
{
    sizerFlags = f;
}

void AuiNotebook::SetSizerBorder(int b)
{
    sizerBorder = b;
}

void AuiNotebook::SetTabHeight(int h)
{
    tabControlHeight = h;
}

wxWindow* AuiNotebook::GetControl()
{
    return notebook;
}

} //_ namespace manager
} //_ namespace gui
} //_ namespace negerns
