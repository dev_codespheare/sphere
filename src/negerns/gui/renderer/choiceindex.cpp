#include <negerns/core/log.h>
#include <negerns/core/output.h>
#include <negerns/core/string/convert.h>
#include <negerns/gui/renderer/choiceindex.h>

namespace negerns {
namespace gui {
namespace renderer {

ChoiceIndex::ChoiceIndex(const std::vector<std::string> &items,
    n::gui::ListView::CellMode mode,
    int alignment) :
    wxDataViewCustomRenderer(wxT("string"), wxDataViewCellMode(mode), alignment)
{
    choices = items;
}

ChoiceIndex::~ChoiceIndex() { }

bool ChoiceIndex::SetValue(const wxVariant &v)
{
    std::string s = v.GetString().ToStdString();
    value = string::stoui(s);
    return true;
}

bool ChoiceIndex::GetValue(wxVariant &v) const
{
    v = string::to_string(value);
    return true;
}

wxSize ChoiceIndex::GetSize() const
{
    // The following code is adopted from wxDataViewChoiceRenderer::GetSize in
    // common/datavcmn.cpp.
    wxSize size = wxDefaultSize;

    for (auto &s : choices) {
        size.IncTo(GetTextExtent(s));
    }

    // Allow some space for the right-side button, which is approximately the
    // size of a scrollbar (and getting pixel-exact value would be complicated).
    // Also add some whitespace between the text and the button:
    size.x += wxSystemSettings::GetMetric(wxSYS_VSCROLL_X);
    size.x += GetTextExtent("M").x;

    return size;
}

bool ChoiceIndex::Render(wxRect cell, wxDC *dc, int state)
{
    display = value < choices.size() ? choices.at(value) : "";
#ifdef _DEBUG
    log::debug_if(value >= choices.size(), "Index is out of range (%1/%2).", value, choices.size());
#endif
    RenderText(display, 0, cell, dc, state);
    return true;
}

wxWindow* ChoiceIndex::CreateEditorCtrl(wxWindow *parent, wxRect labelRect, const wxVariant &v)
{
    control = new ChoiceCtrl(parent);
    control->SetValues(choices);
    control->SetStringSelection(v.GetString());

    control->Move(labelRect.GetLeftTop());
    return control;
}

bool ChoiceIndex::GetValueFromEditorCtrl(wxWindow* editor, wxVariant &v)
{
    auto *control = static_cast<n::gui::ChoiceCtrl *>(editor);
    v = wxVariant(control->GetSelection());
    return true;
}

bool ChoiceIndex::StartEditing(const wxDataViewItem &item, wxRect rect)
{
    wxDataViewCustomRenderer::StartEditing(item, rect);
    value = (std::size_t) item.GetID();
    return true;
}

bool ChoiceIndex::SetValue(unsigned int n)
{
    value = n;
    return true;
}

std::size_t ChoiceIndex::SetValues(const std::vector<std::string> &items)
{
    choices = items;
    return choices.size();
}

unsigned int ChoiceIndex::GetValue() const
{
    return value;
}

} //_ namespace renderer
} //_ namespace gui
} //_ namespace negerns
