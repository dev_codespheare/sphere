#include <negerns/core/log.h>
#include <negerns/core/output.h>
#include <negerns/gui/renderer/choice.h>

namespace negerns {
namespace gui {
namespace renderer {

Choice::Choice(const std::vector<std::string> &items,
    n::gui::ListView::CellMode mode,
    int alignment) :
    wxDataViewCustomRenderer(wxT("string"), wxDataViewCellMode(mode), alignment)
{
    choices = items;
}

Choice::~Choice() { }

bool Choice::SetValue(const wxVariant &v)
{
    value = v.GetString().ToStdString();
#ifdef _DEBUG
    log::debug("Value: %1", value);
#endif
    return true;
}

bool Choice::GetValue(wxVariant &v) const
{
    v = value;
    return true;
}

wxSize Choice::GetSize() const
{
    // The following code is adopted from wxDataViewChoiceRenderer::GetSize in
    // common/datavcmn.cpp.
    wxSize size = wxDefaultSize;

    for (auto &s : choices) {
        size.IncTo(GetTextExtent(s));
    }

    // Allow some space for the right-side button, which is approximately the
    // size of a scrollbar (and getting pixel-exact value would be complicated).
    // Also add some whitespace between the text and the button:
    size.x += wxSystemSettings::GetMetric(wxSYS_VSCROLL_X);
    size.x += GetTextExtent("M").x;

    return size;
}

bool Choice::Render(wxRect cell, wxDC *dc, int state)
{
    RenderText(value, 0, cell, dc, state);
    return true;
}

wxWindow* Choice::CreateEditorCtrl(wxWindow *parent, wxRect labelRect, const wxVariant &v)
{
    control = new ChoiceCtrl(parent);
    control->SetValues(choices);
    control->SetStringSelection(v.GetString());

    control->Move(labelRect.GetLeftTop());
    return control;
}

bool Choice::GetValueFromEditorCtrl(wxWindow* editor, wxVariant &v)
{
    auto *control = static_cast<n::gui::ChoiceCtrl *>(editor);
    v = control->GetStringSelection();
    return true;
}

bool Choice::SetValue(const std::string &v)
{
    value = v;
    return true;
}

std::size_t Choice::SetValues(const std::vector<std::string> &items)
{
    choices = items;
    return choices.size();
}

std::string Choice::GetValue() const
{
    return value;
}

} //_ namespace renderer
} //_ namespace gui
} //_ namespace negerns
