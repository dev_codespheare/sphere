#include <negerns/gui/renderer/date.h>

namespace negerns {
namespace gui {
namespace renderer {

Date::Date(const wxString &varianttype,
    n::gui::ListView::CellMode mode,
    int align)
    : wxDataViewCustomRenderer(varianttype, wxDataViewCellMode(mode), align)
{
    date = wxDateTime::Now();
    format = NEGERNS_DATE_FORMAT;
}

Date::~Date() { }

bool Date::SetValue(const wxVariant& value)
{
    date = value.GetDateTime();
    return true;
}

bool Date::GetValue(wxVariant& value) const
{
    value = date;
    return true;
}

bool Date::Render(wxRect cell, wxDC *dc, int state)
{
    wxString tmp(date.Format(format));
    RenderText(tmp, 0, cell, dc, state);
    return true;
}

wxSize Date::GetSize() const
{
    return GetTextExtent(date.Format(format));
}

void Date::SetFormat(const std::string &fmt)
{
    format = fmt;
}

} //_ namespace renderer
} //_ namespace gui
} //_ namespace negerns
