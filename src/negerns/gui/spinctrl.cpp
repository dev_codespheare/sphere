#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/gui/spinctrl.h>

namespace negerns {
namespace gui {

SpinCtrl::SpinCtrl() : wxSpinCtrl() { }

SpinCtrl::SpinCtrl(wxWindow *parent,
    const wxSize& size,
    long style) :
    wxSpinCtrl(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, size, style)
{ }

SpinCtrl::~SpinCtrl() { }

void SpinCtrl::SetValidator(negerns::Var *val, long style)
{
    wxSpinCtrl::SetValidator(DataXfer(val, DataXfer::Control::Numeric, style));
}

void SpinCtrl::SetValidator(negerns::Var &val, long style)
{
    wxSpinCtrl::SetValidator(DataXfer(&val, DataXfer::Control::Numeric, style));
}

} //_ namespace gui
} //_ namespace negerns
