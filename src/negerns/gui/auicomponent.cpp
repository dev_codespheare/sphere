#include <wx/aui/aui.h>
#include <negerns/gui/auidockart.h>
#include <negerns/gui/auicomponent.h>

namespace negerns {
namespace gui {

AuiComponent::AuiComponent()
    : auiManager(nullptr)
{
    unsigned int flags = wxAUI_MGR_ALLOW_FLOATING |
        wxAUI_MGR_ALLOW_ACTIVE_PANE |
        wxAUI_MGR_LIVE_RESIZE |
        wxAUI_MGR_TRANSPARENT_HINT;
    auiManager = new wxAuiManager(NULL, flags);
    auiManager->SetArtProvider(new AuiDockArt());
}

AuiComponent::~AuiComponent()
{
    wxDELETE(auiManager);
}

void AuiComponent::SetManagedWindow(wxWindow* w)
{
    auiManager->SetManagedWindow(w);
}

void AuiComponent::UnInit()
{
    auiManager->UnInit();
}

wxAuiManager* AuiComponent::GetAuiManager()
{
     return auiManager;
}

} //_ namespace gui
} //_ namespace negerns
