#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <wx/panel.h>
#include <wx/sizer.h>
#include <negerns/core/debug.h>
#include <negerns/core/defs.h>
#include <negerns/core/log.h>
#include <negerns/core/message.h>
#include <negerns/core/event.h>
#include <negerns/core/system.h>
#include <negerns/gui/pane.h>
#include <negerns/gui/component.h>
#include <negerns/gui/mediator.h>

namespace negerns {
namespace gui {

Component::Components Component::components;

void Component::SetMediatorInstance(Mediator* m)
{
    // Initialize using the argument
    (void) Component::GetMediatorInstance(m);
}

Mediator* Component::GetMediatorInstance(Mediator* m)
{
    // The use of this method follows the advice on static initialization
    // mentioned in C++ FAQ. This is to avoid any static initalization problem
    // in the future if it occurs.
    static Mediator* p = nullptr;

    // If the argument is NULL, it is assumed that the calling routine wanted
    // a reference to the MediatorBase-derived object. So, return a pointer to
    // the MediatorBase-derived instance.
    //
    // If the argument is not NULL, then the argument is used to set the
    // reference to the MediatorBase-derived pointer.
    if (m && !p) {
        p = m;
    }
    ASSERT(p, "Mediator instance not set.");
    return p;
}

void Component::AddComponent(Component* cb)
{
    if (cb) {
        ASSERT(cb->Identifier::empty(), "Component identifier is empty.\n"
            "Please set a unique identifier for this component.");
        Component::components.push_back(cb);
    }
}

Component* Component::FindComponent(const wxString& id)
{
    if (Component::components.empty()) {
        return nullptr;
    }
    Component* cb = nullptr;
    for (auto *comp : Component::components) {
        if (comp->Identifier::get() == id) {
            cb = comp;
        }
    }
    return cb;
}

Component::Component() :
    Base(),
    Identifier(),
    parent(nullptr),
    panel(nullptr),
    sizer(nullptr),
    toolbar(nullptr),
    autoCreateToolBar(true),
    statusbar(nullptr),
    autoCreateStatusBar(true),
    name(wxEmptyString),
    bitmap(wxNullBitmap),
    pane(nullptr)
{ }

Component::~Component() { }

void Component::CreateControl()
{
    ASSERT(parent, "Parent cannot be null.");
    // IMPORTANT: This is part of the solution to the diamond problem.
    //
    // We avoid recreating the panel object if it is already created.
    // If there are two subclasses that calls Component::CreateControl,
    // only the first call creates the panel object, the second call does
    // nothing.
    if (!panel) {
        auto color = wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW);
        panel = new wxPanel();
        panel->SetExtraStyle(wxWS_EX_VALIDATE_RECURSIVELY);
        panel->SetBackgroundColour(color);
        panel->Create(parent);
        sizer = new wxBoxSizer(wxVERTICAL);
        panel->SetSizer(sizer);
    }

    if (IsToolBarSet() && autoCreateToolBar) {
        toolbar->SetParent(panel);
        toolbar->CreateControl();
        ASSERT(sizer, "Sizer object cannot be null.");
        sizer->Add(toolbar->GetControl(), Proportion::Fixed, wxGROW | wxALL,
            wxBORDER_DEFAULT);
    }

    if (IsStatusBarSet() && autoCreateStatusBar) {
        statusbar->SetParent(panel);
        statusbar->CreateControl();
        ASSERT(sizer, "Sizer object cannot be null.");
        sizer->Add(statusbar->GetControl(), Proportion::Fixed, wxGROW | wxALL,
            wxBORDER_DEFAULT);
    }
}

void Component::PostCreateControl()
{
    Component::FitContent(panel);
    Component::Layout();
}

void Component::DestroyControl()
{
    if (IsToolBarSet()) {
        toolbar->DestroyControl();
    }
}

void Component::RefreshControl() { }

bool Component::OnInitContent()
{
    return IControl::OnInitContent();
}

void Component::ClearContent() { }

void Component::RefreshContent() { }

void Component::ApplyConfiguration() { }

void Component::ApplyTheme() { }


void Component::Add(wxWindow *window, int proportion, int flag, int border)
{
    if (IsStatusBarSet()) {
        int index = 0;
        auto count = sizer->GetItemCount();
        if (count == 0) {
            index = 0;
        } else {
            index = count - 1;
        }
        sizer->Insert(index, window, proportion, flag, border);
    } else {
        sizer->Add(window, proportion, flag, border);
    }
}

void Component::Add(wxSizer *szr, int proportion, int flag, int border)
{
    if (IsStatusBarSet()) {
        int index = 0;
        auto count = sizer->GetItemCount();
        if (count == 0) {
            index = 0;
        } else {
            index = count - 1;
        }
        sizer->Insert(index, szr, proportion, flag, border);
    } else {
        sizer->Add(szr, proportion, flag, border);
    }
}

void Component::Insert(int index, wxWindow *window, int proportion, int flag, int border)
{
    auto count = sizer->GetItemCount();
    if (count == 0) {
        index = 0;
    } else {
        index = count - 1;
    }
    sizer->Insert(index, window, proportion, flag, border);
}

void Component::Insert(int index, wxSizer *szr, int proportion, int flag, int border)
{
    auto count = sizer->GetItemCount();
    if (count == 0) {
        index = 0;
    } else {
        index = count - 1;
    }
    sizer->Insert(index, szr, proportion, flag, border);
}

void Component::Layout()
{
    sizer->Layout();
}

void Component::FitContent(wxWindow *w)
{
    sizer->SetSizeHints(w);
}

negerns::gui::bar::ToolBar* Component::SetToolBar(negerns::gui::bar::ToolBar* tb,
    bool flag)
{
    toolbar = tb;
    autoCreateToolBar = flag;
    return toolbar;
}

negerns::gui::bar::ToolBar* Component::GetToolBar() const
{
    return toolbar;
}

bool Component::IsToolBarSet() const
{
    return (toolbar != nullptr);
}

negerns::gui::bar::StatusBar* Component::SetStatusBar(negerns::gui::bar::StatusBar* sb,
    bool flag)
{
    statusbar = sb;
    autoCreateStatusBar = flag;
    return statusbar;
}

negerns::gui::bar::StatusBar* Component::GetStatusBar() const
{
    return statusbar;
}

bool Component::IsStatusBarSet() const
{
    return (statusbar != nullptr);
}

bool Component::CanClose()
{
    return true;
}

void Component::Freeze()
{
    if (!panel->IsFrozen()) {
        panel->Freeze();
    }
}

void Component::Thaw()
{
    if (panel->IsFrozen()) {
        panel->Thaw();
    }
}

void Component::SetName(const wxString& s)
{
    name = s;
#if EXCLUDE // Exclude for now. We don't need this checking yet.
    // Ensure that the name being used is unique
    if (PaneComponentBase::names.empty()) {
        PaneComponentBase::names.insert(s);
    } else {
        bool present = PaneComponentBase::names.find(s) != PaneComponentBase::names.end();
        if (!present) {
            PaneComponentBase::names.insert(s);
        } else {
            ASSERT(false, wxString::Format("Pane component name \"%s\" is already present.", s));
        }
    }
#endif
}

wxString Component::GetName(void) const
{
    return name;
}

void Component::SetBitmap(const wxBitmap& bmp)
{
    bitmap = bmp;
}

wxBitmap Component::GetBitmap() const
{
    return bitmap;
}

void Component::SetParent(wxWindow* window)
{
    parent = window;
}

wxWindow* Component::GetParent()
{
    return parent;
}
#if 0
void Component::SetPanel(wxPanel* p)
{
    panel = p;
}
#endif
wxPanel* Component::GetPanel()
{
    return panel;
}

#if 0
void Component::SetSizer(wxBoxSizer* s)
{
    sizer = s;
}
wxBoxSizer* Component::GetSizer()
{
    return sizer;
}
#endif

void Component::SetPane(Pane *p)
{
    pane = p;
}

Pane* Component::GetPane()
{
    return pane;
}

void Component::XferDataToWindow()
{
    XferDataToWindow(panel);
}

void Component::XferDataToWindow(wxWindow *window)
{
    // Using wxDatePickerCtrl, wxWidgets pops up a warning message.
    // Just hide the popup window by disabling wxLog during the
    // data transfer operation and re-enable it after.
    wxLog::EnableLogging(false);
    window->TransferDataToWindow();
    wxLog::EnableLogging(true);
}

void Component::XferDataFromWindow()
{
    XferDataFromWindow(panel);
}

void Component::XferDataFromWindow(wxWindow *window)
{
    // Suppress wxWidgets warning message dialog
    // Just hide the popup window by disabling wxLog during the
    // data transfer operation and re-enable it after.
    wxLog::EnableLogging(false);
    window->TransferDataFromWindow();
    wxLog::EnableLogging(true);
}

#ifdef _DEBUG
void Component::LogEventSummary()
{
    auto count = negerns::System::EventManager()->Count();
    log::debug("Event count: %1", count);
}
#endif

} //_ namespace gui
} //_ namespace negerns
