#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <string>
#include <utility>
#include <wx/sizer.h>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/string.h>
#include <negerns/core/datetime.h>
#include <negerns/data/column.h>
#include <negerns/gui/listview.h>
#include <negerns/gui/renderer/date.h>
#include <negerns/gui/renderer/choice.h>
#include <negerns/gui/renderer/choiceindex.h>

namespace negerns {
namespace gui {

ListView::ListView() :
    Component(),
    control(nullptr)
{
    LOG_FUNCTION();
    Component::SetName("List");
    drawBorder = true;
    rowHeight = 18;
    size = wxDefaultSize;
    multipleSelection = false;
    rowDisplayEventFunc = nullptr;
    mouseEventFunc = nullptr;
}

ListView::~ListView()
{
    LOG_FUNCTION();
}

void ListView::CreateControl()
{
    LOG_FUNCTION();
    auto *p = Component::GetParent();
    ASSERT(p, "Parent should not be null.");
    Component::CreateControl();
    auto *pnl = static_cast<wxWindow*>(Component::GetPanel());
    ASSERT(pnl, "Panel should not be null.");

    int style =  wxDV_VERT_RULES | wxDV_HORIZ_RULES; // | wxDV_ROW_LINES;
    style |= multipleSelection ? wxDV_MULTIPLE : wxDV_SINGLE;
    if (!drawBorder) {
        style |= wxNO_BORDER;
    }
    control = new wxDataViewListCtrl(pnl, wxID_ANY, wxDefaultPosition, size, style);
    control->SetRowHeight(rowHeight);
    for (auto column : columns) {
        control->AppendColumn(column);
    }
    const int scrollbarWidth = wxSystemSettings::GetMetric(wxSYS_VSCROLL_X);
    std::size_t width = GetTotalColumnWidth() + scrollbarWidth;

    // NOTE:
    // The documentation shows associating a model before appending columns.
    // This crashes the application and has caused headaches. Associate the
    // model only after the columns have been appended.
    model = new ListViewModel(this);
    control->AssociateModel(model.get());

    int proportion = size.IsFullySpecified() ? Proportion::Static : Proportion::Dynamic;
    int flags = size.IsFullySpecified() ? 0 : wxGROW | wxALL;
    Component::Add(control, proportion, flags);

#if 0
    control->Bind(wxEVT_DATAVIEW_SELECTION_CHANGED, &ListView::OnSelectionChanged, this);
    control->Bind(wxEVT_DATAVIEW_ITEM_ACTIVATED, &ListView::OnSelectionActivated, this);
    control->Bind(wxEVT_DATAVIEW_ITEM_CONTEXT_MENU, &ListView::OnContextMenu, this);
#endif

    // Mouse events are captured in the wxDataViewCtrl client area
    auto *client= control->GetMainWindow();
    client->Bind(wxEVT_LEFT_UP, &ListView::OnMouse, this);

    Component::AddControl(control);
}

wxWindow* ListView::GetControl()
{
    return control;
}

void ListView::SetBorder(bool b)
{
    drawBorder = b;
}

void ListView::SetRowHeight(int h)
{
    rowHeight = h;
}

wxDataViewColumn *ListView::AppendToggleColumn(
    unsigned int modelColumn,
    const wxString &label,
    int width,
    negerns::Alignment align,
    CellMode mode,
    int flags)
{
    dataColumns.push_back(modelColumn);
    auto *r = new wxDataViewToggleRenderer(wxT("bool"), wxDataViewCellMode(mode));
    auto *ret = new wxDataViewColumn(label, r, modelColumn, width,
        wxAlignment(align), flags);
    ret->SetMinWidth(width);
    columns.push_back(ret);
    return ret;
}

wxDataViewColumn *ListView::AppendTextColumn(
    unsigned int modelColumn,
    const wxString &label,
    int width,
    negerns::Alignment align,
    CellMode mode,
    int flags)
{
    dataColumns.push_back(modelColumn);
    auto *r = new wxDataViewTextRenderer(wxT("string"), wxDataViewCellMode(mode));
    r->EnableEllipsize(wxELLIPSIZE_END);
    auto *ret = new wxDataViewColumn(label, r, modelColumn, width,
        wxAlignment(align), flags);
    ret->SetMinWidth(width);
    columns.push_back(ret);
    return ret;
}

wxDataViewColumn *ListView::AppendIconTextColumn(
    unsigned int modelColumn,
    const wxString &label,
    int width,
    negerns::Alignment align,
    CellMode mode,
    int flags)
{
    dataColumns.push_back(modelColumn);
    auto *r = new wxDataViewIconTextRenderer(wxT("wxDataViewIconText"),
        wxDataViewCellMode(mode));
    r->EnableEllipsize(wxELLIPSIZE_END);
    auto *ret = new wxDataViewColumn(label, r, modelColumn, width,
        wxAlignment(align), flags);
    ret->SetMinWidth(width);
    columns.push_back(ret);
    return ret;
}

wxDataViewColumn *ListView::AppendDateColumn(
    unsigned int modelColumn,
    const wxString &label,
    int width,
    negerns::Alignment align,
    CellMode mode,
    int flags)
{
    dataColumns.push_back(modelColumn);
    auto *r = new renderer::Date(wxT("datetime"), mode);
    auto *ret = new wxDataViewColumn(label, r, modelColumn, width,
        wxAlignment(align), flags);
    ret->SetMinWidth(width);
    columns.push_back(ret);
    return ret;
}

wxDataViewColumn *ListView::AppendTimeColumn(
    unsigned int modelColumn,
    const wxString &label,
    int width,
    negerns::Alignment align,
    CellMode mode,
    int flags)
{
    dataColumns.push_back(modelColumn);
    auto *r = new wxDataViewDateRenderer(wxT("datetime"),
        wxDataViewCellMode(mode));
    auto *ret = new wxDataViewColumn(label, r, modelColumn, width,
        wxAlignment(align), flags);
    ret->SetMinWidth(width);
    columns.push_back(ret);
    return ret;
}

wxDataViewColumn *ListView::AppendProgressColumn(
    unsigned int modelColumn,
    const wxString &label,
    int width,
    negerns::Alignment align,
    CellMode mode,
    int flags)
{
    dataColumns.push_back(modelColumn);
    auto *r = new wxDataViewProgressRenderer(wxEmptyString, wxT("long"),
        wxDataViewCellMode(mode));
    auto *ret = new wxDataViewColumn(label, r, modelColumn, width,
        wxAlignment(align), flags);
    ret->SetMinWidth(width);
    columns.push_back(ret);
    return ret;
}

wxDataViewColumn *ListView::AppendChoiceColumn(
    unsigned int modelColumn,
    const wxString &label,
    int width,
    negerns::Alignment align,
    CellMode mode,
    int flags)
{
    dataColumns.push_back(modelColumn);
    auto *r = new renderer::Choice({}, mode, wxAlignment(align));
    r->EnableEllipsize(wxELLIPSIZE_END);
    auto *ret = new wxDataViewColumn(label, r, modelColumn, width,
        wxAlignment(align), flags);
    ret->SetMinWidth(width);
    columns.push_back(ret);
    return ret;
}

wxDataViewColumn *ListView::AppendChoiceIndexColumn(
    unsigned int modelColumn,
    const wxString &label,
    int width,
    negerns::Alignment align,
    CellMode mode,
    int flags)
{
    dataColumns.push_back(modelColumn);
    auto *r = new renderer::ChoiceIndex({}, mode, wxAlignment(align));
    r->EnableEllipsize(wxELLIPSIZE_END);
    auto *ret = new wxDataViewColumn(label, r, modelColumn, width,
        wxAlignment(align), flags);
    ret->SetMinWidth(width);
    columns.push_back(ret);
    return ret;
}

#if EXCLUDE
wxDataViewColumn *ListView::AppendColumn(
    unsigned int itemColumnIndex,
    const wxString &label,
    int width,
    wxAlignment align,
    wxDataViewCellMode mode,
    int flags)
{
    dataColumns.push_back(itemColumnIndex);
    //ListViewModel *m = static_cast<ListViewModel*>(control->GetModel());
    std::string cppType(static_cast<ListViewModel*>(control->GetModel())->GetColumnType(itemColumnIndex).ToStdString());
    auto type = static_cast<ListViewModel*>(control->GetModel())->GetColumnDataType(itemColumnIndex);

    wxDataViewColumn *column = nullptr;

    switch (type) {
        case column::DataType::Bool : {
            auto *renderer = new wxDataViewToggleRenderer(wxT("bool"), mode);
            column = new wxDataViewColumn(label, renderer, columns.size(), width, align, flags);
            }
            break;
        case column::DataType::Int8:
        case column::DataType::UInt8:
        case column::DataType::Int16:
        case column::DataType::UInt16:
        case column::DataType::Int32:
        case column::DataType::UInt32: {
            auto *renderer = new wxDataViewTextRenderer(wxT("string"), mode);
            column = new wxDataViewColumn(label, renderer, columns.size(), width, align, flags);
            }
            break;
        case column::DataType::Int64: {
            auto *renderer = new wxDataViewTextRenderer(wxT("string"), mode);
            column = new wxDataViewColumn(label, renderer, columns.size(), width, align, flags);
            }
            break;
        case column::DataType::UInt64: {
            auto *renderer = new wxDataViewTextRenderer(wxT("string"), mode);
            column = new wxDataViewColumn(label, renderer, columns.size(), width, align, flags);
            }
            break;
        case column::DataType::Float:
        case column::DataType::Double: {
            auto *renderer = new wxDataViewTextRenderer(wxT("string"), mode);
            column = new wxDataViewColumn(label, renderer, columns.size(), width, align, flags);
            }
            break;
        case column::DataType::String: {
            auto *renderer = new wxDataViewTextRenderer(wxT("string"), mode);
            column = new wxDataViewColumn(label, renderer, columns.size(), width, align, flags);
            }
            break;
        case column::DataType::Date:
        case column::DataType::Time:
        case column::DataType::Timestamp: {
            auto *renderer = new wxDataViewDateRenderer(wxT("string"), mode);
            column = new wxDataViewColumn(label, renderer, columns.size(), width, align, flags);
            }
            break;
        default: {
            auto *renderer = new wxDataViewTextRenderer(wxT("string"), mode);
            column = new wxDataViewColumn(label, renderer, columns.size(), width, align, flags);
            }
    }

    //auto r = new wxDataViewProgressRenderer(wxEmptyString, wxT("long"), mode);
    //auto ret = new wxDataViewColumn(label, renderer, columns.size(), width, align, flags);
    columns.push_back(column);
    return column;
}
#endif

size_t ListView::GetTotalColumnWidth()
{
    int width = 0;
    unsigned int columnCount = control->GetColumnCount();
    if (columnCount > 0) {
        const int separatorBar = 2;
        width = control->GetColumn(0)->GetWidth() + separatorBar;
        for (unsigned int i = 1; i < columnCount; ++i) {
            width += control->GetBestColumnWidth(i);
        }
    }
    
    return width;
}

void ListView::SetRowDisplayEventFunction(RowDisplayEventFunction f)
{
    rowDisplayEventFunc = std::forward<RowDisplayEventFunction>(f);
}

void ListView::SetMouseEventFunction(MouseEventFunction f)
{
    mouseEventFunc = std::forward<MouseEventFunction>(f);
}

#if 0
void ListView::SetModelData(std::unique_ptr<negerns::data::Rowset> rs)
{
    LOG_FUNCTION();
    static_cast<ListViewModel*>(control->GetModel())->Set(std::move(rs));
}
#endif

void ListView::SetModelData(negerns::data::DataStore *ds, DataSource source)
{
    LOG_FUNCTION();
    //ASSERT(ds, "DataStore is null.");
    ASSERT(dataColumns.size() > 0, "No data columns specified.\nPlease append columns to the list first.");
    if (ds) {
        auto columnCount = ds->column.Count();
        for (auto column : dataColumns) {
            ASSERT(column < columnCount, n::Format("Specified column (%1) is out of range.", column));
        }
        ASSERT(control, "Control should not be null.");
        static_cast<ListViewModel*>(control->GetModel())->Set(ds, source);
        auto count = ds->RowCount();
        if (count > 0) {
            Select(0);
            control->SetFocus();
        }
    } else {
        static_cast<ListViewModel*>(control->GetModel())->Set(ds, source);
    }
}

wxDataViewItem ListView::GetCurrentItem() const
{
    ASSERT(control, "Control should not be null.");
    return control->GetCurrentItem();
}

wxDataViewItem ListView::GetItem(std::size_t row) const
{
    ASSERT(control, "Control should not be null.");
    return static_cast<ListViewModel*>(control->GetModel())->GetItem(row);
}

// Return a blank Row.
negerns::data::Row ListView::GetBlankRow()
{
    ASSERT(control, "Control should not be null.");
    return static_cast<ListViewModel*>(control->GetModel())->Get();
}

// Return the Row of the currently selected item in the list.
negerns::data::Row ListView::GetCurrentRow()
{
    ASSERT(control, "Control should not be null.");
    auto item = control->GetCurrentItem();
    return GetRow(item);
}

// Return the Row of the specified wxDataViewItem object.
negerns::data::Row ListView::GetRow(const wxDataViewItem &item)
{
    ASSERT(control, "Control should not be null.");
    return static_cast<ListViewModel*>(control->GetModel())->Get(item);
}

// Return the row index of the currently selected item.
std::size_t ListView::GetCurrentIndex() const
{
    ASSERT(control, "Control should not be null.");
    auto item = control->GetCurrentItem();
    return GetIndex(item);
}

// Return the row index of the specified wxDataViewItem object.
std::size_t ListView::GetIndex(const wxDataViewItem &item) const
{
    ASSERT(control, "Control should not be null.");
    return static_cast<ListViewModel*>(control->GetModel())->GetIndex(item);
}

void ListView::Select(std::size_t row)
{
    ASSERT(control, "Control should not be null.");
    auto item = static_cast<ListViewModel*>(control->GetModel())->GetItem(row);
    if (item.IsOk()) {
        control->Select(item);
    }
}

unsigned int ListView::GetItemCount()
{
    ASSERT(control, "Control should not be null.");
    return static_cast<ListViewModel*>(control->GetModel())->GetCount();
}

void ListView::EditItem(const wxDataViewItem &item, const wxDataViewColumn *column)
{
    ASSERT(control, "Control should not be null.");
    control->EditItem(item, column);
}

#if 0
bool ListView::OnRowDisplay(std::size_t row, std::size_t column,
    wxVariant &variant)
{
    return true;
}
#endif

void ListView::OnMouse(wxMouseEvent &event)
{
    if (mouseEventFunc) {
        mouseEventFunc(event);
    }
}

void ListView::SetSize(const wxSize &s)
{
    size = s;
}

void ListView::SetSingleSelection(bool b)
{
    multipleSelection = (b != true);
}

void ListView::SetMultipleSelection(bool b)
{
    multipleSelection = b;
}



// ----------------------------------------------------------------------------------------



ListViewModel::ListViewModel(ListView* c) :
    wxDataViewVirtualListModel(0),
    component(c),
    items(nullptr)
{
    //results.reserve(INITIAL_NUMBER_OF_ITEMS);
    // Undocumented (2.9.1)
    wxDataViewVirtualListModel::Reset(0);
}

ListViewModel::~ListViewModel() { }

unsigned int ListViewModel::GetColumnCount() const
{
    return items->column.Count();
}

wxString ListViewModel::GetColumnType(unsigned int column) const
{
    if (!items) {
        return wxEmptyString;
    }

    negerns::data::DataType type = items->column[column].type;
    switch (type) {
        case negerns::data::DataType::Bool :
            return "bool";
            break;
        case negerns::data::DataType::Int8:
        case negerns::data::DataType::UInt8:
        case negerns::data::DataType::Int16:
        case negerns::data::DataType::UInt16:
        case negerns::data::DataType::Int32:
        case negerns::data::DataType::UInt32:
            return "long";
            break;
        case negerns::data::DataType::Int64:
            return "longlong";
            break;
        case negerns::data::DataType::UInt64:
            return "ulonglong";
            break;
        case negerns::data::DataType::Float:
        case negerns::data::DataType::Double:
            return "double";
            break;
        case negerns::data::DataType::String:
            return "string";
            break;
        case negerns::data::DataType::Date:
        case negerns::data::DataType::Time:
        case negerns::data::DataType::Timestamp:
            return "datetime";
            break;
        default:
            return "string";
    }
}

void ListViewModel::GetValue(wxVariant &variant, const wxDataViewItem &item,
    unsigned int column) const
{
    ASSERT(item.IsOk());
    auto row = GetIndex(item);
    GetValueByRow(variant, row, column);
}

void ListViewModel::GetValueByRow(wxVariant &variant, unsigned int row, unsigned int column) const
{
    if (!items && GetCount() == 0) {
        return;
    }

    if (component->rowDisplayEventFunc) {
        bool status = component->rowDisplayEventFunc(row, column, variant);
        if (status == false) {
            return;
        }
    }

    auto *rows = GetDataSourceRows();

    std::string value;
    negerns::data::DataType type = items->column[column].type;
    switch (type) {
        case negerns::data::DataType::Bool:
            if ((*rows).IsNull(row, column)) {
                variant = false;
            } else {
                variant = (*rows)(row, column).convert<bool>();
            }
            break;
        case negerns::data::DataType::Int8:
            if ((*rows).IsNull(row, column)) {
                variant = 0L;
            } else {
                // For now we use long. Change ASAP.
                variant = (*rows)(row, column).convert<long>();
            }
            break;
        case negerns::data::DataType::UInt8:
            if ((*rows).IsNull(row, column)) {
                variant = 0L;
            } else {
                // For now we use long. Change ASAP.
                variant = (*rows)(row, column).convert<long>();
            }
            break;
        case negerns::data::DataType::Int16:
            if ((*rows).IsNull(row, column)) {
                variant = 0L;
            } else {
                // For now we use long. Change ASAP.
                variant = (*rows)(row, column).convert<long>();
            }
            break;
        case negerns::data::DataType::UInt16:
            if ((*rows).IsNull(row, column)) {
                variant = 0L;
            } else {
                // For now we use long. Change ASAP.
                variant = (*rows)(row, column).convert<long>();
            }
            break;
        case negerns::data::DataType::Int32:
            if ((*rows).IsNull(row, column)) {
                variant = 0L;
            } else {
                // For now we use long. Change ASAP.
                variant = (*rows)(row, column).convert<long>();
            }
            break;
        case negerns::data::DataType::UInt32:
            if ((*rows).IsNull(row, column)) {
                variant = 0L;
            } else {
                // For now we use long. Change ASAP.
                variant = (*rows)(row, column).convert<long>();
            }
            break;
        case negerns::data::DataType::Int64:
            if ((*rows).IsNull(row, column)) {
                variant = 0L;
            } else {
                // For now we use long. Change ASAP.
                variant = (*rows)(row, column).convert<long>();
            }
            break;
        case negerns::data::DataType::UInt64:
            if ((*rows).IsNull(row, column)) {
                variant = 0L;
            } else {
                // For now we use long. Change ASAP.
                variant = (*rows)(row, column).convert<long>();
            }
            break;
        case negerns::data::DataType::Float:
        case negerns::data::DataType::Double:
            variant = (*rows).IfNull(row, column, 0.00f).convert<float>();
            break;
        case negerns::data::DataType::String:
            if ((*rows).IsNull(row, column)) {
                variant = std::string();
            } else {
                variant = (*rows)(row, column).convert<std::string>();
            }
            break;
        case negerns::data::DataType::Date:
            if ((*rows).IsNull(row, column)) {
                variant = "";
            } else {
                auto value = (*rows)(row, column).convert<poco::DateTime>();
                wxString dt(negerns::DateTime::ToISODate(value));
                variant = dt;
            }
            break;
        case negerns::data::DataType::Time:
        case negerns::data::DataType::Timestamp:
            variant = (*rows)(row, column).convert<std::string>();
            break;
        default:
            variant = (*rows)(row, column).convert<std::string>();
    }
}

bool ListViewModel::SetValueByRow(const wxVariant &variant,
    unsigned int row,
    unsigned int column)
{
    if (!items && GetCount() == 0) {
        return false;
    }

    std::string value = variant.GetString().ToStdString();

    auto *rows = GetDataSourceRows();
    
    negerns::data::DataType type = items->column[column].type;
    switch (type) {
        case negerns::data::DataType::Bool:
            (*rows)(row, column) = variant.GetBool();
            break;
        case negerns::data::DataType::Int8:
            (*rows)(row, column) = (char) variant.GetChar();
            break;
        case negerns::data::DataType::UInt8:
            (*rows)(row, column) = (unsigned char) variant.GetChar();
            break;
        case negerns::data::DataType::Int16:
            (*rows)(row, column) = string::stos(value);
            break;
        case negerns::data::DataType::UInt16:
            (*rows)(row, column) = string::stous(value);
            break;
        case negerns::data::DataType::Int32:
            (*rows)(row, column) = string::stoi(value);
            break;
        case negerns::data::DataType::UInt32:
            (*rows)(row, column) = string::stoui(value);
            break;
        case negerns::data::DataType::Int64:
            (*rows)(row, column) = string::stoll(value);
            break;
        case negerns::data::DataType::UInt64:
            (*rows)(row, column) = string::stoull(value);
            break;
        case negerns::data::DataType::Float:
            (*rows)(row, column) = std::stof(value);
            break;
        case negerns::data::DataType::Double:
            (*rows)(row, column) = std::stod(value);
            break;
        case negerns::data::DataType::String:
            (*rows)(row, column) = value;
            break;
        case negerns::data::DataType::Date:
            (*rows)(row, column) = n::DateTime::ToISODate(variant.GetDateTime());
            break;
        case negerns::data::DataType::Time:
        case negerns::data::DataType::Timestamp:
            (*rows)(row, column) = n::DateTime::ToISOTime(variant.GetDateTime());
            break;
        default:
            (*rows)(row, column) = value;
    }

    return true;
}

#if 0
void ListViewModel::Set(std::unique_ptr<negerns::data::Rowset> rs)
{
    LOG_FUNCTION();
    items = std::move(rs);
    auto count = items->GetRowCount();
    markers.resize(count, false);
    
    wxDataViewVirtualListModel::Reset(count);
}
#endif

void ListViewModel::Set(negerns::data::DataStore *ds, ListView::DataSource src)
{
    LOG_FUNCTION();
    source = src;
    items = ds;
    std::size_t count = 0;
    if (items) {
        count = items->RowCount();
    }
    markers.resize(count, false);
    wxDataViewVirtualListModel::Reset(count);
}

negerns::data::Row ListViewModel::Get()
{
    LOG_FUNCTION();
    return items ? items->original(0) : negerns::data::Row();
}

negerns::data::Row ListViewModel::Get(const wxDataViewItem& item) const
{
    LOG_FUNCTION();
    negerns::data::Row row;
    if (item.IsOk()) {
        return GetCount() ? items->original(GetRow(item)) : std::move(row);
    } else {
        return std::move(row);
    }
}

negerns::data::Row ListViewModel::Get(const unsigned int index) const
{
    LOG_FUNCTION();
    const auto count = GetCount();
    negerns::data::Row row;
    if (count > 0) {
        return (index < count) ? items->original(index) : std::move(row);
    } else {
        return std::move(row);
    }
}

std::size_t ListViewModel::GetIndex(const wxDataViewItem &item) const
{
    return items ? GetRow(item) : 0;
}

void ListViewModel::Clear()
{
    LOG_FUNCTION();
    markers.clear();
    wxDataViewVirtualListModel::Reset(0);
}

negerns::data::IRows * ListViewModel::GetDataSourceRows() const
{
    if (source == ListView::DataSource::Original) {
        return &(items->original);
    } else if (source == ListView::DataSource::Modified) {
        return (negerns::data::IRows *) &(items->modified);
    } else if (source == ListView::DataSource::Inserted) {
        return (negerns::data::IRows *) &(items->inserted);
    } else {
        return nullptr;
    }
}

} //_ namespace gui
} //_ namespace negerns
