#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <wx/dc.h>
#include <wx/bitmap.h>
#include <wx/aui/framemanager.h>
#include <negerns/gui/auitoolbarart.h>

namespace negerns {
namespace gui {
namespace bar {

// Code from auibar.cpp
static const unsigned char DISABLED_TEXT_GREY_HUE = wxColour::AlphaBlend(0, 255, 0.4);
const wxColour DISABLED_TEXT_COLOR(DISABLED_TEXT_GREY_HUE,
    DISABLED_TEXT_GREY_HUE,
    DISABLED_TEXT_GREY_HUE);

AuiToolBarArt::AuiToolBarArt() :
    wxAuiDefaultToolBarArt()
{
    border = Bar::Border::None;
}

AuiToolBarArt::~AuiToolBarArt() { }

wxAuiToolBarArt* AuiToolBarArt::Clone()
{
    return static_cast<wxAuiToolBarArt*>(new AuiToolBarArt());
}

void AuiToolBarArt::DrawBackground(
    wxDC& dc,
    wxWindow* wnd,
    const wxRect& rect)
{
    wxAuiDefaultToolBarArt::DrawBackground(dc, wnd, rect);
    DrawBorder(dc, rect);
}

void AuiToolBarArt::DrawPlainBackground(wxDC& dc,
    wxWindow* wnd,
    const wxRect& rect)
{
    wxAuiDefaultToolBarArt::DrawPlainBackground(dc, wnd, rect);
    DrawBorder(dc, rect);
}

void AuiToolBarArt::SetBorder(Bar::Border b)
{
    border = b;
}

void AuiToolBarArt::DrawBorder(wxDC &dc, const wxRect &rect)
{
    //wxColour color = wxSystemSettings::GetColour(wxSYS_COLOUR_3DFACE);
    wxColour color = wxSystemSettings::GetColour(wxSYS_COLOUR_3DSHADOW);
    dc.SetPen(wxPen(color));

    if (border == Bar::Border::All) {
        dc.DrawRectangle(rect);
    } else {
        if (border & Bar::Border::Bottom) {
            wxPoint left(rect.GetLeftBottom());
            wxPoint right(rect.GetBottomRight());
            right.x++;
            dc.DrawLine(left, right);
        }
    }
}

void AuiToolBarArt::DrawLabel(
    wxDC& dc,
    wxWindow* WXUNUSED(wnd),
    const wxAuiToolBarItem& item,
    const wxRect& rect)
{
    dc.SetFont(m_font);
    dc.SetTextForeground(*wxBLACK);

    // we only care about the text height here since the text
    // will get cropped based on the width of the item
    int textWidth = 0, textHeight = 0;
    dc.GetTextExtent(wxT("ABCDHgj"), &textWidth, &textHeight);

    // set the clipping region
    wxRect clipRect = rect;
    clipRect.width -= 1;
    dc.SetClippingRegion(clipRect);

    int textX, textY;
    textX = rect.x + 1;
    textY = rect.y + (rect.height - textHeight) / 2;
    dc.DrawText(item.GetLabel(), textX, textY);
    dc.DestroyClippingRegion();
}

void AuiToolBarArt::DrawButton(
    wxDC& dc,
    wxWindow* WXUNUSED(wnd),
    const wxAuiToolBarItem& item,
    const wxRect& rect)
{
    int textWidth = 0, textHeight = 0;

    if (m_flags & wxAUI_TB_TEXT) {
        dc.SetFont(m_font);

        int tx, ty;

        dc.GetTextExtent(wxT("ABCDHgj"), &tx, &textHeight);
        textWidth = 0;
        dc.GetTextExtent(item.GetLabel(), &textWidth, &ty);
    }

    int bmpX = 0, bmpY = 0;
    int textX = 0, textY = 0;

    if (m_textOrientation == wxAUI_TBTOOL_TEXT_BOTTOM) {
        bmpX = rect.x +
            (rect.width / 2) -
            (item.GetBitmap().GetWidth() / 2);

        bmpY = rect.y +
            ((rect.height - textHeight) / 2) -
            (item.GetBitmap().GetHeight() / 2);

        textX = rect.x + (rect.width / 2) - (textWidth / 2) + 1;
        textY = rect.y + rect.height - textHeight - 1;
    } else if (m_textOrientation == wxAUI_TBTOOL_TEXT_RIGHT) {
        bmpX = rect.x + 3;

        bmpY = rect.y +
            (rect.height / 2) -
            (item.GetBitmap().GetHeight() / 2);

        textX = bmpX + 3 + item.GetBitmap().GetWidth();
        // Raise higher because the text appears a bit low.
        textY = rect.y +
            (rect.height / 2) -
            (textHeight / 2) - 1;
    }


    if (!(item.GetState() & wxAUI_BUTTON_STATE_DISABLED)) {
        if (item.GetState() & wxAUI_BUTTON_STATE_PRESSED) {
            dc.SetPen(wxPen(m_highlightColour));
            dc.SetBrush(wxBrush(m_highlightColour.ChangeLightness(150)));
            dc.DrawRectangle(rect);
        } else if ((item.GetState() & wxAUI_BUTTON_STATE_HOVER) || item.IsSticky()) {
            dc.SetPen(wxPen(m_highlightColour));
            dc.SetBrush(wxBrush(m_highlightColour.ChangeLightness(170)));

            // draw an even lighter background for checked item hovers (since
            // the hover background is the same color as the check background)
            if (item.GetState() & wxAUI_BUTTON_STATE_CHECKED)
                dc.SetBrush(wxBrush(m_highlightColour.ChangeLightness(180)));

            dc.DrawRectangle(rect);
        } else if (item.GetState() & wxAUI_BUTTON_STATE_CHECKED) {
            // it's important to put this code in an else statement after the
            // hover, otherwise hovers won't draw properly for checked items
            dc.SetPen(wxPen(m_highlightColour));
            dc.SetBrush(wxBrush(m_highlightColour.ChangeLightness(170)));
            dc.DrawRectangle(rect);
        }
    }

    wxBitmap bmp;
    if (item.GetState() & wxAUI_BUTTON_STATE_DISABLED)
        bmp = item.GetDisabledBitmap();
    else
        bmp = item.GetBitmap();

    if (bmp.IsOk())
        dc.DrawBitmap(bmp, bmpX, bmpY, true);

    // set the item's text color based on if it is disabled
    dc.SetTextForeground(*wxBLACK);
    if (item.GetState() & wxAUI_BUTTON_STATE_DISABLED)
        dc.SetTextForeground(DISABLED_TEXT_COLOR);

    if ((m_flags & wxAUI_TB_TEXT) && !item.GetLabel().empty()) {
        dc.DrawText(item.GetLabel(), textX, textY);
    }
}

} //_ namespace bar
} //_ namespace gui
} //_ namespace negerns
