#include <negerns/gui/panel.h>

namespace negerns {
namespace gui {

Panel::Panel() :
    wxScrolledWindow()
{ }

Panel::Panel(wxWindow* parent,
    wxWindowID id,
    const wxPoint& pos,
    const wxSize& size,
    long style,
    const wxString &name) :
    wxScrolledWindow(parent, id, pos, size, style)
{
    sizer = new wxBoxSizer(orientation);
    wxScrolledWindow::SetSizer(sizer);
    wxScrolledWindow::SetScrollRate(5, 5);
    wxScrolledWindow::Bind(wxEVT_SIZE, &Panel::OnSize, this);
}

Panel::~Panel() { }

void Panel::Create(wxWindow* parent,
    wxWindowID id,
    const wxPoint& pos,
    const wxSize& size,
    long style,
    const wxString &name)
{
    sizer = new wxBoxSizer(orientation);
    wxScrolledWindow::SetSizer(sizer);
    wxScrolledWindow::SetScrollRate(5, 5);
}

void Panel::SetDirection(n::Orientation o)
{
    sizer->SetOrientation(o);
}

wxBoxSizer *Panel::GetSizer()
{
    return sizer;
}

void Panel::Add(wxSizer *s, const wxSizerFlags &flags)
{
    sizer->Add(s, flags);
}

void Panel::Add(wxSizer *s,
    negerns::Proportion proportion,
    int flag,
    int border,
    wxObject *userData)
{
    sizer->Add(s, proportion, flag, border, userData);
}

void Panel::AddInStaticBox(wxSizer *s,
    n::Orientation orientation,
    negerns::Proportion proportion,
    int flag,
    int border)
{
    AddInStaticBox(s, "", orientation, proportion, flag, border);
}

void Panel::AddInStaticBox(wxSizer *s,
    const std::string &label,
    n::Orientation orientation,
    negerns::Proportion proportion,
    int flag,
    int border)
{
    auto *sb = new wxStaticBoxSizer(orientation, this, label);
    sb->Add(s, proportion, flag | wxALL, border);
    sizer->Add(sb, proportion, flag);
}

void Panel::OnSize(wxSizeEvent&)
{
    wxScrolledWindow::FitInside();
}

} //_ namespace gui
} //_ namespace negerns
