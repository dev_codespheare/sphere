#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/data/irows.h>

namespace negerns {
namespace data {

IRows::IRows() :
    recordset(nullptr)
{ }

IRows::IRows(poco::data::RecordSet *rs) :
    recordset(rs)
{ }

IRows::~IRows() { }

negerns::Variant & IRows::operator()(std::size_t r, std::size_t c)
{
#ifdef _DEBUG
    throw std::runtime_error("Not implemented yet.");
#endif
    return std::move(negerns::Variant());
}

negerns::Variant & IRows::operator()(std::size_t r, const std::string &s)
{
    auto ci = NameIndexIFace::GetNameIndexContainer()->Index(s);
    return (*this)(r, ci);
}

negerns::Variant & IRows::Get(std::size_t r, std::size_t c)
{
#ifdef _DEBUG
    throw std::runtime_error("Not implemented yet.");
#endif
    return std::move(negerns::Variant());
}

negerns::Variant & IRows::Get(std::size_t r, const std::string &s)
{
#ifdef _DEBUG
    throw std::runtime_error("Not implemented yet.");
#endif
    return std::move(negerns::Variant());
}

negerns::Variant & IRows::IfNull(std::size_t r, std::size_t c,
    const negerns::Variant &v)
{
#ifdef _DEBUG
    throw std::runtime_error("Not implemented yet.");
#endif
    return std::move(negerns::Variant());
}

negerns::Variant & IRows::IfNull(std::size_t r,
    const std::string &s,
    const negerns::Variant &v)
{
    auto ci = NameIndexIFace::GetNameIndexContainer()->Index(s);
    auto &cv = (*this)(r, ci);
    if (cv.isEmpty()) {
        cv = v;
    }
    return cv;
}

void IRows::operator()(std::size_t r, std::size_t c, const negerns::Variant &v)
{
#ifdef _DEBUG
    throw std::runtime_error("Not implemented yet.");
#endif
}

void IRows::operator()(std::size_t r,
    const std::string &s,
    const negerns::Variant &v)
{
    auto ci = NameIndexIFace::GetNameIndexContainer()->Index(s);
    (*this)(r, ci, v);
}

bool IRows::IsNull(std::size_t r, std::size_t c)
{
#ifdef _DEBUG
    throw std::runtime_error("Not implemented yet.");
#endif
    return false;
}

bool IRows::IsNull(std::size_t r, const std::string &s)
{
    auto ci = NameIndexIFace::GetNameIndexContainer()->Index(s);
    auto &cv = (*this)(r, ci);
    return cv.isEmpty();
}

std::size_t IRows::RowCount()
{
#ifdef _DEBUG
    throw std::runtime_error("Not implemented yet.");
#endif
    return 0;
}

std::size_t IRows::Find(std::size_t c, const negerns::Variant &v)
{
#ifdef _DEBUG
    throw std::runtime_error("Not implemented yet.");
#endif
    return 0;
}

#ifdef _DEBUG
void IRows::LogRowValues(std::size_t r)
{
    ASSERT(rows.size() > 0, "Buffer has no row.");
    ASSERT(r < rows.size(), "Row is out of range.");
    auto count = rows[r].fieldCount();
    log::debug_if(count == 0, "Row has no column.");
    if (count > 0) {
        std::string name;
        std::string value;
        log::debug("Contents of row %1:", r);
        for (std::size_t i = 0; i < count; ++i) {
            name = IRows::GetName(i);
            value = rows[r][i].convert<std::string>();
            log::debug("  Column %1 %2 [%3]", i, name, value);
        }
    }
}
#endif

} //_ namespace data
} //_ namespace negerns
