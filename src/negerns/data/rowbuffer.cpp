#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/data/rowbuffer.h>

using namespace negerns::data;

RowBuffer::RowBuffer() { }

RowBuffer::~RowBuffer() { }

Row & RowBuffer::operator()(std::size_t i)
{
    ASSERT(rows.size() > 0, "Buffer has no row.");
    ASSERT(i < rows.size(), "Row is out of range.");
    auto count = rows[i].fieldCount();
    return rows[i];
}

Row & RowBuffer::GetRow(const std::size_t i)
{
    ASSERT(rows.size() > 0, "Buffer has no row.");
    ASSERT(i < rows.size(), "Row is out of range.");
    auto count = rows[i].fieldCount();
    return rows[i];
}

negerns::Variant & RowBuffer::operator()(std::size_t r, const std::size_t c)
{
    ASSERT(rows.size() > 0, "Buffer has no row.");
    ASSERT(rows.find(r) != rows.end(), "Row not found.");
    return rows[r][c];
}

negerns::Variant & RowBuffer::operator()(std::size_t r, const std::string &s)
{
    return IRows::operator()(r, s);
}

negerns::Variant & RowBuffer::Get(std::size_t r, std::size_t c)
{
    ASSERT(rows.size() > 0, "Buffer has no row.");
    ASSERT(rows.find(r) != rows.end(), "Row not found.");
    return rows[r][c];
}

negerns::Variant & RowBuffer::Get(std::size_t r, const std::string &s)
{
    return IRows::operator()(r, s);
}

negerns::Variant & RowBuffer::IfNull(std::size_t r,
    std::size_t c,
    const negerns::Variant &v)
{
    auto &cv = RowBuffer::operator()(r, c);
    if (cv.isEmpty()) {
        cv = v;
    }
    return cv;
}

negerns::Variant & RowBuffer::IfNull(std::size_t r,
    const std::string &s,
    const negerns::Variant &v)
{
    return IRows::IfNull(r, s, &v);
}

void RowBuffer::operator()(
    std::size_t r,
    const std::size_t c,
    const negerns::Variant &v)
{
    ASSERT(rows.size() > 0, "Buffer has no row.");
    ASSERT(rows.find(r) != rows.end(), "Row not found.");
    rows[r][c] = v;
}

void RowBuffer::operator()(
    std::size_t r,
    const std::string &s,
    const negerns::Variant &v)
{
    IRows::operator()(r, s, v);
}

std::size_t RowBuffer::SetRows(const std::map<std::size_t, Row> &source)
{
    rows = source;
    return source.size();
}

bool RowBuffer::IsNull(std::size_t r, std::size_t c)
{
    auto &cv = RowBuffer::operator()(r, c);
    return cv.isEmpty();
}

bool RowBuffer::IsNull(std::size_t r, const std::string &s)
{
    return IRows::IsNull(r, s);
}

void RowBuffer::AddRow(const std::size_t i, const Row &row)
{
#ifdef _DEBUG
    auto oldrowcount = rows.size();
#endif
    //ASSERT(i > rows.size(), "Wrong row index to insert to.");

    // COMPILER NOTE:
    // VC++ 2013 error C2784 when calling emplace and passing an initializer
    // list like rows.emplace({rowcount + i, row})
    rows.emplace(std::make_pair(i, row));
#ifdef _DEBUG
    auto newrowcount = rows.size();
    ASSERT(rows.size() == oldrowcount + 1, "No row inserted.");
#endif
}

bool RowBuffer::IsRowFound(std::size_t row)
{
    return rows.find(row) != rows.end();
}

void RowBuffer::DeleteAllRows()
{
    rows.clear();
}

void RowBuffer::DeleteRow(std::size_t row)
{
    ASSERT(rows.size() > 0, "Buffer has no row.");
    ASSERT(rows.find(row) != rows.end(), "Row not found.");
    rows.erase(row);
}

std::size_t RowBuffer::RowCount()
{
    return rows.size();
}

std::size_t RowBuffer::Find(std::size_t c, const negerns::Variant &value)
{
    size_t count = 0;
    std::string s1;
    auto s2 = value.convert<std::string>();
    for (auto &row : rows) {
        count++;
        s1 = row.second.get(c).convert<std::string>();
        if (s1 == s2) {
            break;
        }
    }
    return count - 1;
}



// -----------------------------------------------------------------------------



InsertRowBuffer::InsertRowBuffer() { }
InsertRowBuffer::~InsertRowBuffer() { }

void InsertRowBuffer::SetRow(const std::size_t i, const Row &row)
{
    ASSERT(rows.size() > 0, "Buffer has no row.");
    ASSERT(i < rows.size(), "Row is out of range.");
    rows[i] = row;
}

void InsertRowBuffer::AddRows(const std::size_t count, const Row &row)
{
    auto rowcount = rows.size();
    for (std::size_t i = 0; i < count; ++i) {
        // COMPILER NOTE:
        // VC++ 2013 error C2784 when calling emplace and passing an initializer
        // list like rows.emplace({rowcount + i, row})
        rows.emplace(std::make_pair(rowcount + i, row));
    }
#ifdef _DEBUG
    auto newrowcount = rows.size();
    ASSERT(rows.size() == rowcount + count, "No row inserted.");
#endif
}



// -----------------------------------------------------------------------------



ModifyRowBuffer::ModifyRowBuffer() :
    RowBuffer()
{ }
ModifyRowBuffer::~ModifyRowBuffer() { }

void ModifyRowBuffer::SetRow(const std::size_t i, const Row &row)
{
    ASSERT(rows.size() > 0, "Buffer has no row.");
    ASSERT(i < rows.size(), "Row is out of range.");
    rows[i] = row;
}

void ModifyRowBuffer::RevertRow(const std::size_t i)
{
    ASSERT(rows.size() > 0, "Buffer has no row.");
    ASSERT(rows.find(i) != rows.end(), "Row not found.");
    rows[i] = recordset->row(i);
}

void ModifyRowBuffer::SetRecordSet(poco::data::RecordSet *rs)
{
    recordset = rs;
}



// -----------------------------------------------------------------------------



DeleteRowBuffer::DeleteRowBuffer() { }

DeleteRowBuffer::~DeleteRowBuffer() { }
