#include <negerns/core/variant.h>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/data/column.h>

using namespace negerns::data;

DataType Column::GetType(poco::data::MetaColumn::ColumnDataType t)
{
    DataType type = DataType::Unknown;
    switch (t) {
        case poco::data::MetaColumn::FDT_BOOL:      type = DataType::Bool; break;
        case poco::data::MetaColumn::FDT_INT8:      type = DataType::Int8; break;
        case poco::data::MetaColumn::FDT_UINT8:     type = DataType::UInt8; break;
        case poco::data::MetaColumn::FDT_INT16:     type = DataType::Int16; break;
        case poco::data::MetaColumn::FDT_UINT16:    type = DataType::UInt16; break;
        case poco::data::MetaColumn::FDT_INT32:	    type = DataType::Int32; break;
        case poco::data::MetaColumn::FDT_UINT32:    type = DataType::UInt32; break;
        case poco::data::MetaColumn::FDT_INT64:     type = DataType::Int64; break;
        case poco::data::MetaColumn::FDT_UINT64:    type = DataType::UInt64; break;
        case poco::data::MetaColumn::FDT_FLOAT:     type = DataType::Float; break;
        case poco::data::MetaColumn::FDT_DOUBLE:    type = DataType::Double; break;
        case poco::data::MetaColumn::FDT_STRING:    type = DataType::String; break;
        case poco::data::MetaColumn::FDT_WSTRING:   type = DataType::WString; break;
        case poco::data::MetaColumn::FDT_BLOB:      type = DataType::Blob; break;
        case poco::data::MetaColumn::FDT_DATE:      type = DataType::Date; break;
        case poco::data::MetaColumn::FDT_TIME:      type = DataType::Time; break;
        case poco::data::MetaColumn::FDT_TIMESTAMP: type = DataType::Timestamp; break;
        default:
            FAIL(false, "Unknown data type.");
    }
    return type;
}

std::string Column::GetTypeName(poco::data::MetaColumn::ColumnDataType type)
{
    return Column::GetTypeName(Column::GetType(type));
}

std::string Column::GetTypeName(DataType type)
{
    // The following typdefs are defined in stdint.h
    //
    // typedef signed char        int8_t;
    // typedef short              int16_t;
    // typedef int                int32_t;
    // typedef long long          int64_t;
    // typedef unsigned char      uint8_t;
    // typedef unsigned short     uint16_t;
    // typedef unsigned int       uint32_t;
    // typedef unsigned long long uint64_t;

    switch (type) {
        case DataType::Bool:      return "bool";
        case DataType::Int8:      return "int8_t";
        case DataType::UInt8:     return "uint8_t";
        case DataType::Int16:     return "int16_t";
        case DataType::UInt16:    return "uint16_t";
        case DataType::Int32:	  return "int32_t";
        case DataType::UInt32:    return "uint32_t";
        case DataType::Int64:     return "int64_t";
        case DataType::UInt64:    return "uint64_t";
        case DataType::Float:     return "float";
        case DataType::Double:    return "double";
        case DataType::String:    return "string";
        case DataType::WString:    return "wstring";
#if 0
        case poco::data::MetaColumn::FDT_BLOB:      return "";
#endif
        case DataType::Date:
        case DataType::Time:
        case DataType::Timestamp: return "std::time_t";
        default:
            return "";
    }
}

negerns::Variant Column::GetDefault(const Column &column)
{
    auto defvar = Variant();
    switch (column.type) {
        case DataType::Bool:
            defvar = false;
            break;
        case DataType::Int8:
        case DataType::UInt8:
        case DataType::Int16:
        case DataType::UInt16:
        case DataType::Int32:
        case DataType::UInt32:
        case DataType::Int64:
        case DataType::UInt64:
            defvar = 0;
            break;
        case DataType::Float:
        case DataType::Double:
            defvar = 0.00;
            break;
        case DataType::String:
            // NOTE:
            // Poco::Dynamic::Var documentation.
            // String truncation is allowed � it is possible to convert
            // between string and character when string length is greater
            // than 1. An empty string gets converted to the char '\0',
            // a non-empty string is truncated to the first character.

            // Compare with zero (0) because the column meta data may not
            // have been initialized yet when this function is called.
            if (column.length == 0 || column.length > 1) {
                defvar = "";
            } else if (column.length == 1) {
                defvar = '\0';
            }
            break;
        case DataType::WString:
            // Same as DataType::String
            if (column.length == 0 || column.length > 1) {
                defvar = "";
            } else if (column.length == 1) {
                defvar = '\0';
            }
            break;
        case DataType::Date:
            defvar = poco::DateTime(poco::Timestamp::fromEpochTime(0));
            break;
        case DataType::Time:
        case DataType::Timestamp:
            //defvar = poco::Timestamp(poco::Timestamp::fromEpochTime(0));
            defvar = poco::DateTime(poco::Timestamp::fromEpochTime(0));
            break;
        default:
            defvar = "";
    }
    return defvar;
}

Column::Column() { }

Column::Column(const Column &p) :
    position(p.position),
    name(p.name),
    type(p.type),
    length(p.length),
    precision(p.precision),
    nullable(p.nullable)
{ }

Column::Column(Column &&p) :
    position(std::move(p.position)),
    name(std::move(p.name)),
    type(std::move(p.type)),
    length(std::move(p.length)),
    precision(std::move(p.precision)),
    nullable(std::move(p.nullable))
{ }

Column::Column(const poco::data::MetaColumn &mc) :
    position(mc.position()),
    name(mc.name()),
    type(Column::GetType(mc.type())),
    length(mc.length()),
    precision(mc.precision()),
    nullable(mc.isNullable())
{ }

Column::~Column() { }

Column& Column::operator=(const Column &p)
{
    if (this == &p) {
        return *this;
    }
    position = p.position;
    name = p.name;
    type = p.type;
    length = p.length;
    precision = p.precision;
    nullable = p.nullable;
    return *this;
}

Column& Column::operator=(Column &&p)
{
    position = std::move(p.position);
    name = std::move(p.name);
    type = std::move(p.type);
    length = std::move(p.length);
    precision = std::move(p.precision);
    nullable = std::move(p.nullable);
    return *this;
}

void Column::Set(const poco::data::MetaColumn &mc)
{
    position = mc.position();
    name = mc.name();
    type = Column::GetType(mc.type());
    length = mc.length();
    precision = mc.precision();
    nullable = mc.isNullable();
}

std::string Column::GetTypeName()
{
    return Column::GetTypeName(type);
}
