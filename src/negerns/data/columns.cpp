#include <wx/debug.h>
#include <negerns/data/columns.h>

using namespace negerns::data;

ColumnsIterator::ColumnsIterator(const Columns *c, std::size_t p) :
    pos(p),
    columns(c)
{ }

bool ColumnsIterator::operator!=(const ColumnsIterator &other) const
{
    return pos != other.pos;
}

const Column& ColumnsIterator::operator*() const
{
    return columns->Get(pos);
}

const ColumnsIterator& ColumnsIterator::operator++()
{
    ++pos;
    // Although not strictly necessary for a range-based for loop
    // following the normal convention of returning a value from
    // operator++ is a good idea.
    return *this;
}



Columns::Columns()
{
    SetGetDataFunction(std::bind(&Columns::GetColumnInternal, this,
        std::placeholders::_1));
}

Columns::~Columns() { }

ColumnsIterator Columns::begin() const
{
    return ColumnsIterator(this, 0);
}
 
ColumnsIterator Columns::end() const
{
    return ColumnsIterator(this, items.size());
}

void Columns::Add(const Column &c)
{
    items.push_back(c);
}

Column& Columns::Get(const std::size_t i)
{
    wxASSERT_MSG(i < items.size(), "Column index is out of range.");
    return items[i];
}

const Column& Columns::Get(const std::size_t i) const
{
    wxASSERT_MSG(i < items.size(), "Column index is out of range.");
    return items[i];
}

Column& Columns::Get(const std::string &s)
{
    return (*this)[s];
}

const Column& Columns::Get(const std::string &s) const
{
    return (*this)[s];
}

void Columns::Clear()
{
    items.clear();
}

std::size_t Columns::Count() const
{
    return items.size();
}

Column & Columns::GetColumnInternal(const std::size_t i)
{
    return items[i];
}
