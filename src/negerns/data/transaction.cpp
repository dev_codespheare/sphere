#include <stdint.h>
#include <algorithm>
#include <Poco/Data/RecordSet.h>
#include <negerns/negerns.h>
#include <negerns/core/log.h>
#include <negerns/core/message.h>
#include <negerns/data/transaction.h>
#include <negerns/data/dataaccess.h>
#include <negerns/data/recordset.h>

namespace negerns {
namespace data {

Transaction::Transaction(std::size_t sessionPool)
{
    sessionPoolId = sessionPool;
}

Transaction::Transaction(Transaction &&other) :
    sessionPoolId(std::move(other.sessionPoolId))
{
    throw std::runtime_error("Transaction move constructor called.");
}

Transaction::~Transaction() { }

Transaction & Transaction::operator=(Transaction &&rhs)
{
    sessionPoolId = rhs.sessionPoolId;
    return *this;
}

poco::data::Statement* Transaction::CreateStatement(const std::string &sql)
{
    LOG_FUNCTION();
    auto session = negerns::data::DataAccess::GetSession(sessionPoolId);

    // TODO:: Try usig statement->reset(*(session.get()))

    auto *statement = new poco::data::Statement(*(session.get()));
    (*statement) << sql;
    return statement;
}

std::size_t Transaction::Bind(poco::data::Statement *statement, const Parameters &param)
{
    for (auto &p : param) {
        statement->addBind(poco::data::keywords::useRef(p));
    }
    return param.size();
}

std::size_t Transaction::Bind(poco::data::Statement *statement, Row &param)
{
    std::size_t count = param.fieldCount();
    if (count) {
        for (decltype(count) i = 0; i < count; ++i) {
            statement->addBind(poco::data::keywords::useRef(param[i]));
        }
    }
    return count;
}

std::size_t Transaction::Execute(poco::data::Statement *statement)
{
    std::size_t rows = 0;
    try {
        rows = statement->execute();
    } catch (poco::data::BindingException& ex) {
        std::string info(ex.displayText());
        log::fatal(info);
        Message::Error(info);
    } catch (poco::data::DataException& ex) {
        std::string info(ex.displayText());
        log::fatal(info);
        Message::Error(info);
    } catch (poco::Exception& ex) {
        std::string info(ex.displayText());
        log::fatal(info);
        Message::Error(info);
    }

    return rows;
}

std::size_t Transaction::Execute(const std::string &sql, const Parameters &param)
{
    LOG_FUNCTION();
    auto statement = CreateStatement(sql);
    Bind(statement, param);
    std::size_t rows = Execute(statement);
    wxDELETE(statement);
    return rows;
}

std::size_t Transaction::Execute(const std::string &sql, Row &param)
{
    LOG_FUNCTION();
    auto statement = CreateStatement(sql);
    Bind(statement, param);
    std::size_t rows = Execute(statement);
    wxDELETE(statement);
    return rows;
}

std::size_t Transaction::Execute(const std::string &sql, const Rows &rows)
{
    LOG_FUNCTION();
    std::size_t count = 0;
    if (!sql.empty() && rows.size()) {
        auto *statement = Transaction::CreateStatement(sql);
        auto row = rows.begin();
        auto param = row->second;
        if (auto pcount = param.fieldCount()) {
            for (decltype(pcount) i = 0; i < pcount; ++i) {
                statement->addBind(poco::data::keywords::useRef(param[i]));
            }
        }
        for (auto pos = rows.begin(); pos != rows.end(); ++pos) {
            param = pos->second;
            count += Transaction::Execute(statement);
        }
        wxDELETE(statement);
    }
    return count;
}

poco::data::Statement* Transaction::Delete(const std::string &sql,
    Row &param)
{
    LOG_FUNCTION();
    auto statement = CreateStatement(sql);
    Bind(statement, param);
    std::size_t rows = Execute(statement);
    return statement;
}

} //_ namespae data
} //_ namespae negerns
