#include <negerns/core/debug.h>
#include <negerns/data/sqlstatement.h>

using namespace negerns::data;

void Query::Compose()
{
    ASSERT(from.Count() > 0, "No database table specified.");
    std::string tmp("select ");
    if (select.Count() == 0) {
        // Get the column names and column index from the database by doing
        // a retrieve operation.
    } else {
        for (auto &col : select) {
            tmp.append(col).append(", ");
        }
        tmp.pop_back();
        tmp.pop_back();
        tmp.append(" ");
    }

    tmp.append("from ");
    for (auto &tab : from) {
        tmp.append(tab).append(" ");
    }
    tmp.pop_back();
    sql.Set(tmp);
}

void Query::SaveColumns()
{
    selectBuffer = select;
}

void Query::RestoreColumns()
{
    ASSERT(!selectBuffer.IsEmpty(), "Column info not previously saved; nothing to restore.");
    select = selectBuffer;
}

#if 0
void Sql::ComposeProcedure()
{
    std::string tmp("select ");
    tmp.append(procedure);
    tmp.append("((");
    if (column.Count() == 0) {
        // Get the column names and column index from the database by doing
        // a retrieve operation.
    } else {
        for (auto &col : column) {
            tmp.append(col).append(", ");
        }
        tmp.pop_back();
        tmp.pop_back();
        tmp.append(" ");
    }
    tmp.append("))");
}
#endif
