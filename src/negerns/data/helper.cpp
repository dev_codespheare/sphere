#include <negerns/data/helper.h>

using namespace negerns::data;

Helper::Helper() { }

Helper::~Helper() { }
#if 0
n::data::Row Helper::GetRow(const std::string &sql)
{
    auto rs = Helper::GetRecordSet(sql);
    return rs->GetRow();
}

std::unique_ptr<n::data::RecordSet> Helper::GetRecordSet(const std::string &sql)
{
    auto trans = n::System::Transaction();
    return trans->GetRecordSet(sql);
}

n::data::DataStore * Helper::CreateDataStore()
{
    auto trans = n::System::Transaction();
    return new n::data::DataStore(std::move(trans));
}
#endif
