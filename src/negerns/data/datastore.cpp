#include <algorithm>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/message.h>
#include <negerns/data/internal/statementwrapper.h>
#include <negerns/data/datastore.h>

namespace negerns {
namespace data {

bool DataStore::IsEqual(negerns::data::Row &row1, negerns::data::Row &row2)
{
    auto count_1 = row1.fieldCount();
    auto count_2 = row2.fieldCount();
    if (count_1 != count_2) {
        return false;
    }
    for (std::size_t i = 0; i < count_1; ++i) {
        if (row1[i].type() != row2[i].type()) {
            return false;
        }
    }
    std::string column_1;
    std::string column_2;
    for (std::size_t i = 0; i < count_1; ++i) {
        column_1 = row1[i].isEmpty() ? "" : row1[i].convert<std::string>();
        column_2 = row2[i].isEmpty() ? "" : row2[i].convert<std::string>();
        if (column_1 != column_2)
            return false;
    }
    return true;
}

DataStore::DataStore() :
    NameIndexContainer(),
    recordset(nullptr)
{
    column.SetNameIndex(this);
    original.SetRecordSet(nullptr);
    modified.SetRecordSet(nullptr);
}

DataStore::DataStore(negerns::data::Transaction *t) :
    NameIndexContainer(),
    recordset(nullptr),
    transaction(t)
{
    ASSERT(t, "Transaction is null.");
    column.SetNameIndex(this);
    original.SetNameIndex(this);
    inserted.SetNameIndex(this);
    modified.SetNameIndex(this);
    deleted.SetNameIndex(this);

    original.SetRecordSet(nullptr);
    modified.SetRecordSet(nullptr);

    //SetSqlSelect("select 0, 'test'");
    //Retrieve();
    //SetSqlSelect("");
}

DataStore::~DataStore()
{
    recordset.release();
}

void DataStore::SetTransaction(negerns::data::Transaction *t)
{
    ASSERT(t, "Transaction is null.");
    transaction = t;
}

poco::data::RecordSet * DataStore::GetRecordSet()
{
    return recordset.get();
}

BufferStore * DataStore::Detach()
{
    ASSERT(column.Count() > 0, "No columns definition.");
    auto *drows = new BufferStore();
    drows->column = column;
    drows->original = detached;
    drows->modified = detached;
    return drows;
}

void DataStore::Attach(const BufferStore *drows)
{
    //inserted.SetRows(drows->inserted.rows);
    modified.SetRows(drows->modified.rows);
    //deleted.SetRows(drows->deleted.rows);
}

void DataStore::SetPrimaryKeys(const std::vector<std::size_t> &indices)
{
    pkIndex = indices;
    std::sort(pkIndex.begin(), pkIndex.end());
}

#ifndef NDEBUG
const std::vector<std::size_t> & DataStore::GetPrimaryKeys() const
{
    return pkIndex;
}
#endif

void DataStore::SetDefaults()
{
    default = original.GetRow();
}

void DataStore::InsertRow(std::size_t index)
{
    ASSERT(!sqlInsert.IsEmpty(), "Could not acquire row format from a data source.\nPlease specify an SQL statement for database SELECT or INSERT operation.");
    if (index == 0) {
        inserted.AddRows(1, default);
    } else {
        if (index < original.RowCount()) {
            inserted.AddRows(1, original(index));
        } else {
            std::string msg = negerns::Format("DataStore::InsertRow argument (%1) is out of range.\n"
                "Original buffer have %2 rows", index, original.RowCount());
            negerns::Message::Error("Row not found.", msg);
        }
    }
}

void DataStore::InsertRows(std::size_t rows, const std::vector<std::size_t> &index)
{
    ASSERT(rows > 0, "Wrong number of rows to insert.");
    if (index.size() > 0) {
        ASSERT(rows == index.size(), "Number of rows specified is not in sync with row indices.");
    }
    ASSERT(!sqlInsert.IsEmpty(), "Could not acquire row format from a data source.\nPlease specify an SQL statement for database SELECT or INSERT operation.");
    if (index.size() == 0) {
        inserted.AddRows(rows, default);
    } else {
        for (auto i : index) {
            if (i < original.RowCount()) {
                inserted.AddRows(1, original(i));
            } else {
                std::string msg = negerns::Format("DataStore::InsertRow argument (%1) is out of range.\n"
                    "Original buffer have %2 rows", i, original.RowCount());
                negerns::Message::Error("Row not found.", msg);
            }
        }
    }
}

void DataStore::InsertRows(std::size_t start, std::size_t end)
{
    std::size_t count = original.RowCount();
    ASSERT(start < count, "Start index is > number of rows.");
    ASSERT(end <= count, "Ending index is > number of rows.");
    ASSERT(start < end, "Starting index is <= ending index.");
    ASSERT(!sqlInsert.IsEmpty(), "Could not acquire row format from a data source.\nPlease specify an SQL statement for database SELECT or INSERT operation.");
    for (std::size_t i = start; i < end; ++i) {
        if (i < count) {
            inserted.AddRows(1, original(i));
        } else {
            std::string msg = negerns::Format("DataStore::InsertRow argument (%1) is out of range.\n"
                "Original buffer have %2 rows", i, count);
            negerns::Message::Error("Row not found.", msg);
        }
    }
}

void DataStore::ModifyRow(std::size_t i)
{
    ASSERT(!sqlUpdate.IsEmpty(), "Could not acquire row format from a data source.\nPlease specify an SQL statement for database UPDATE operation.");
    ASSERT(!modified.IsRowFound(i), "Row is already in the modified buffer.");
    modified.AddRow(i, original(i));
}

void DataStore::ModifyRows(std::size_t rows, const std::vector<std::size_t> &index)
{
    ASSERT(rows > 0, "Wrong number of rows to insert.");
    if (index.size() > 0) {
        ASSERT(rows == index.size(), "Number of rows specified is not in sync with row indices.");
    }
    ASSERT(!sqlUpdate.IsEmpty(), "Could not acquire row format from a data source.\nPlease specify an SQL statement for database SELECT or INSERT operation.");
    if (index.size() == 0) {
        modified.AddRow(rows, original(rows));
    } else {
        for (auto i : index) {
            if (i < original.RowCount()) {
                modified.AddRow(i, original(i));
            } else {
                std::string msg = negerns::Format("DataStore::InsertRow argument (%1) is out of range.\n"
                    "Original buffer have %2 rows", i, original.RowCount());
                negerns::Message::Error("Row not found.", msg);
            }
        }
    }
}

void DataStore::ModifyRows(std::size_t start, std::size_t end)
{
    std::size_t count = original.RowCount();
    ASSERT(start < count, "Start index is > number of rows.");
    ASSERT(end <= count, "Ending index is > number of rows.");
    ASSERT(start < end, "Starting index is <= ending index.");
    ASSERT(!sqlUpdate.IsEmpty(), "Could not acquire row format from a data source.\nPlease specify an SQL statement for database SELECT or INSERT operation.");
    for (std::size_t i = start; i < end; ++i) {
        if (i < count) {
            modified.AddRow(i, original(i));
        } else {
            std::string msg = negerns::Format("DataStore::InsertRow argument (%1) is out of range.\n"
                "Original buffer have %2 rows", i, count);
            negerns::Message::Error("Row not found.", msg);
        }
    }
}

void DataStore::DeleteRow(std::size_t i)
{
    ASSERT(!sqlDelete.IsEmpty(), "Could not acquire row format from a data source.\nPlease specify an SQL statement for database DELETE operation.");
    // NOTE: Row can be deleted even if it is in the modified buffer.
    ASSERT(!deleted.IsRowFound(i), "Row is already in the deleted buffer.");
    deleted.AddRow(i, original(i));
}

void DataStore::DetachRow(std::size_t i)
{
    // NOTE: Row can be deleted even if it is in the modified buffer.
    ASSERT(!detached.IsRowFound(i), "Row is already in the detached buffer.");
    detached.AddRow(i, original(i));
}

negerns::Variant & DataStore::GetValue(std::size_t row,
    std::size_t col,
    BufferType t)
{
    if (t == BufferType::Unknown) {
        if (IsModified(row)) {
            ASSERT(row < modified.RowCount(), "Row out of range.");
            return modified(row, col);
        } else {
            ASSERT(row < original.RowCount(), "Row out of range.");
            return original(row, col);
        }
    } else if (t == BufferType::Original) {
        ASSERT(row < original.RowCount(), "Row out of range.");
        return original(row, col);
    } else if (t == BufferType::Inserted) {
        ASSERT(row < inserted.RowCount(), "Row out of range.");
        return inserted(row, col);
    } else if (t == BufferType::Modified) {
        ASSERT(row < modified.RowCount(), "Row out of range.");
        return modified(row, col);
    }
    return std::move(negerns::Variant());
}

negerns::Variant & DataStore::GetValue(std::size_t row,
    const std::string &col,
    BufferType t)
{
    if (t == BufferType::Unknown) {
        if (IsModified(row)) {
            ASSERT(row < modified.RowCount(), "Row out of range.");
            return modified(row, col);
        } else {
            ASSERT(row < original.RowCount(), "Row out of range.");
            return original(row, col);
        }
    } else if (t == BufferType::Original) {
        ASSERT(row < original.RowCount(), "Row out of range.");
        return original(row, col);
    } else if (t == BufferType::Inserted) {
        ASSERT(row < inserted.RowCount(), "Row out of range.");
        return inserted(row, col);
    } else if (t == BufferType::Modified) {
        ASSERT(row < modified.RowCount(), "Row out of range.");
        return modified(row, col);
    }
    return std::move(negerns::Variant());
}

std::size_t DataStore::RowCount(const BufferType t)
{
    if (t == BufferType::Original) {
        return recordset->getTotalRowCount();
    } else if (t == BufferType::Inserted) {
        return inserted.RowCount();
    } else if (t == BufferType::Modified) {
        return modified.RowCount();
    } else if (t == BufferType::Deleted) {
        return deleted.RowCount();
    } else if (t == BufferType::Detached) {
        return detached.RowCount();
    } else {
        return 0;
    }
}

void DataStore::DiscardChanges()
{
    inserted.DeleteAllRows();
    modified.DeleteAllRows();
    deleted.DeleteAllRows();
}

bool DataStore::IsEqual(BufferType t1, BufferType t2, std::size_t row)
{
    Row row1;
    Row row2;

    if (t1 == BufferType::Original) {
        row1 = original(row);
    } else if (t1 == BufferType::Inserted) {
        row1 = inserted.GetRow(row);
    } else if (t1 == BufferType::Modified) {
        row1 = modified.GetRow(row);
    } else if (t1 == BufferType::Deleted) {
        row1 = deleted.GetRow(row);
    } else if (t1 == BufferType::Detached) {
        row1 = detached.GetRow(row);
    }

    if (t2 == BufferType::Original) {
        row2 = original(row);
    } else if (t2 == BufferType::Inserted) {
        row2 = inserted.GetRow(row);
    } else if (t2 == BufferType::Modified) {
        row2 = modified.GetRow(row);
    } else if (t2 == BufferType::Deleted) {
        row2 = deleted.GetRow(row);
    } else if (t2 == BufferType::Detached) {
        row2 = detached.GetRow(row);
    }

#if 0
    bool status = true;
    std::size_t count = column.Count();
    for (std::size_t i = 0; i < count; ++i) {
        if (first->Get(row, i) != second->Get(row, i)) {
            status = false;
            break;
        }
    }
    return status;
#endif
    return row1 == row2;
}

bool DataStore::IsInserted(std::size_t i)
{
    return inserted.IsRowFound(i);
}

bool DataStore::IsModified(std::size_t i)
{
    return modified.IsRowFound(i);
}

bool DataStore::IsDeleted(std::size_t i)
{
    return deleted.IsRowFound(i);
}

std::size_t DataStore::Retrieve()
{
    negerns::data::Parameters param;
    return Retrieve(param);
}

std::size_t DataStore::Retrieve(const negerns::data::Parameters &param)
{
    // TODO:: Warn user if there are rows in the other buffers.
    ASSERT(transaction, "Transaction object is null.");
    ASSERT(!sqlSelect.IsEmpty(), "No SQL Select statement specified.");

    auto *statement = transaction->CreateStatement(sqlSelect.Get());
    for (std::size_t i = 0; i < param.size(); ++i) {
        statement->addBind(poco::data::keywords::useRef(param[i]));
    }
    std::size_t rows = transaction->Execute(statement);
    if (statement != nullptr) {
        recordset.reset(new poco::data::RecordSet(*statement));
        original.SetRecordSet(recordset.get());
        internal::StatementWrapper sw((*statement));

        if (auto count = recordset->columnCount()) {
            //name.clear();
            NameIndexContainer::Clear();
            column.Clear();
            for (std::size_t i = 0; i < count; ++i) {
                auto p = Column(sw.GetMetaColumn(i));
                //name[p.name] = i;
                NameIndexContainer::Add(p.name);
                column.Add(p);
            }
        }

        original.SetRecordSet(recordset.get());
        //inserted.SetRecordSet(recordset.get());
        modified.SetRecordSet(recordset.get());
        //deleted.SetRecordSet(recordset.get());

        rows = recordset->getTotalRowCount();
    }
    wxDELETE(statement);
    return rows;
}

std::size_t DataStore::Insert(bool discard)
{
    std::size_t count = 0;
    if (!sqlInsert.IsEmpty() && inserted.rows.size()) {
        count = Insert(inserted.rows, discard);
    }
    return count;
}

std::size_t DataStore::Insert(negerns::data::Rows &params, bool discard)
{
    std::size_t count = 0;
    if (!sqlInsert.IsEmpty() && params.size()) {
        returned.DeleteAllRows();
        auto *statement = transaction->CreateStatement(sqlInsert.Get());
        auto param = params[0];
        if (auto pcount = param.fieldCount()) {
            for (decltype(pcount) i = 0; i < pcount; ++i) {
                statement->addBind(poco::data::keywords::useRef(param[i]));
            }
        }

        auto rcount = params.size();
        for (std::size_t i = 0; i < rcount; ++i) {
            param = params[i];
            count += transaction->Execute(statement);
            auto *poco_rs = new poco::data::RecordSet(*statement);
            if (poco_rs) {
                auto *rs = new negerns::data::RecordSet(poco_rs);
                if (rs->RowCount() > 0) {
                    returned.AddRows(1, rs->GetRow(0));
                }
                wxDELETE(rs);
            } else {
                log::fatal("Cannot create Poco::Data::RecordSet. Aborting operation.");
                break;
            }
        }
        wxDELETE(statement);
        if (discard) {
            inserted.DeleteAllRows();
        }
    }
    return count;
}

std::size_t DataStore::Update(bool discard)
{
    std::size_t count = 0;
    if (!sqlUpdate.IsEmpty() && modified.rows.size()) {
        count = Update(modified.rows, discard);
    }
    return count;
}

std::size_t DataStore::Update(const negerns::data::Rows &params, bool discard)
{
    std::size_t count = 0;
    if (!sqlUpdate.IsEmpty() && params.size()) {
        count = transaction->Execute(sqlUpdate.Get(), params);
        if (discard) {
            modified.DeleteAllRows();
        }
    }
    return count;
}

std::size_t DataStore::Delete()
{
    std::size_t count = 0;
    if (!sqlDelete.IsEmpty() && deleted.rows.size()) {
        count = Delete(deleted.rows);
    }
    return count;
}

std::size_t DataStore::Delete(negerns::data::Rows &params)
{
    std::size_t count = 0;
    if (!sqlDelete.IsEmpty() && params.size()) {

        auto *statement = transaction->CreateStatement(sqlDelete.Get());

        auto row = params.begin();
        auto param = GetPrimaryKeyColumns(row->second);
        if (auto pcount = param.fieldCount()) {
            for (decltype(pcount) i = 0; i < pcount; ++i) {
                statement->addBind(poco::data::keywords::useRef(param[i]));
            }
        }

        auto rcount = params.size();
        for (auto pos = params.begin(); pos != params.end(); ++pos) {
            param = GetPrimaryKeyColumns(pos->second);
            count += transaction->Execute(statement);
        }
        wxDELETE(statement);

        deleted.DeleteAllRows();
    }
    return count;
}

std::size_t DataStore::UpdateAll()
{
#if 0
    if (!sqlDelete.empty()) {
        for (auto &row : deleted.rows) {
            auto pkcols = GetPrimaryKeyColumns(row.second);
            deletedCount += transaction->Execute(sqlDelete, pkcols);
        }
    }
#endif
    std::size_t deletedCount = Delete(deleted.rows);
    std::size_t modifiedCount = Update(modified.rows);
    std::size_t insertedCount = Insert(inserted.rows);

    deleted.DeleteAllRows();
    modified.DeleteAllRows();
    inserted.DeleteAllRows();

    return deletedCount + modifiedCount + insertedCount;
}

std::size_t DataStore::Execute(const std::string &sql)
{
    negerns::data::Parameters param;
    return Execute(sql, param);
}

std::size_t DataStore::Execute(const std::string &sql,
    negerns::data::Parameters &params)
{
    ASSERT(transaction, "Transaction object is null.");
    return transaction->Execute(sql, params);
}

Row DataStore::GetPrimaryKeyColumns(Row &row)
{
    ASSERT(pkIndex.size() > 0, "Primary keys not set.");
    ASSERT(row.fieldCount() > 0, "Row is empty.");
    ASSERT(row.fieldCount() > pkIndex.back(), "Primary key index not found in row.");
    Row pkRow;
    for(auto i : pkIndex) {
        pkRow.append(row.names()->at(i), row[i]);
    }
    return pkRow;
}

} //_ namespace data
} //_ namespace negerns
