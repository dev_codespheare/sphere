#include <algorithm>
#include <negerns/core/debug.h>
#include <negerns/data/recordset.h>

using namespace negerns::data;

RecordSet::RecordSet() :
    IRows()
{ }

RecordSet::RecordSet(poco::data::RecordSet *rs) :
    IRows(rs)
{ }

RecordSet::~RecordSet() { }

Row & RecordSet::operator()(std::size_t i)
{
    ASSERT(recordset, "RecordSet cannot be null.");
    ASSERT(recordset->totalRowCount() > 0, "RecordSet has no row.");
    ASSERT(i < recordset->totalRowCount(), "Row is out of range.");
    return recordset->row(i);
}

Row & RecordSet::GetRow(const std::size_t i)
{
    ASSERT(recordset, "RecordSet cannot be null.");
    ASSERT(recordset->totalRowCount() > 0, "RecordSet has no row.");
    ASSERT(i < recordset->totalRowCount(), "Row is out of range.");
    return recordset->row(i);
}

negerns::Variant & RecordSet::operator()(
    std::size_t r,
    std::size_t c)
{
    ASSERT(recordset, "RecordSet cannot be null.");
    ASSERT(recordset->totalRowCount() > 0, "RecordSet has no row.");
    ASSERT(r < recordset->totalRowCount(), "Row is out of range.");
    //ASSERT(c < recordset->columnCount(), "Column is out of range.");
    // TODO: Find a way to directly return a reference to the row/column value.
    return recordset->row(r)[c];
}

negerns::Variant & RecordSet::operator()(std::size_t r, const std::string &s)
{
    ASSERT(recordset, "RecordSet cannot be null.");
    ASSERT(recordset->totalRowCount() > 0, "RecordSet has no row.");
    ASSERT(r < recordset->totalRowCount(), "Row is out of range.");
    return recordset->row(r)[s];
}

negerns::Variant & RecordSet::Get(std::size_t r, std::size_t c)
{
    ASSERT(recordset, "RecordSet cannot be null.");
    ASSERT(recordset->totalRowCount() > 0, "RecordSet has no row.");
    ASSERT(r < recordset->totalRowCount(), "Row is out of range.");
    ASSERT(c < recordset->columnCount(), "Column is out of range.");
    // TODO: Find a way to directly return a reference to the row/column value.
    return recordset->row(r)[c];
}

negerns::Variant & RecordSet::Get(std::size_t r, const std::string &s)
{
    ASSERT(recordset, "RecordSet cannot be null.");
    ASSERT(recordset->totalRowCount() > 0, "RecordSet has no row.");
    ASSERT(r < recordset->totalRowCount(), "Row is out of range.");
    return recordset->row(r)[s];
}

negerns::Variant & RecordSet::IfNull(std::size_t r,
        std::size_t c,
        const negerns::Variant &v)
{
    ASSERT(recordset, "RecordSet cannot be null.");
    ASSERT(recordset->totalRowCount() > 0, "RecordSet has no row.");
    ASSERT(r < recordset->totalRowCount(), "Row is out of range.");
    //ASSERT(c < recordset->columnCount(), "Column is out of range.");
    //auto &cv = RecordSet::operator()(r, c);
    auto &cv = recordset->row(r)[c];
    if (cv.isEmpty()) {
        cv = v;
    }
    return cv;
}

negerns::Variant & RecordSet::IfNull(std::size_t r,
    const std::string &s,
    const negerns::Variant &v)
{
    ASSERT(recordset, "RecordSet cannot be null.");
    ASSERT(recordset->totalRowCount() > 0, "RecordSet has no row.");
    ASSERT(r < recordset->totalRowCount(), "Row is out of range.");
    //return IRows::IfNull(r, s, v);
    auto &cv = recordset->row(r)[s];
    if (cv.isEmpty()) {
        cv = v;
    }
    return cv;
}

bool RecordSet::IsNull(std::size_t r, std::size_t c)
{
    ASSERT(recordset, "RecordSet cannot be null.");
    ASSERT(recordset->totalRowCount() > 0, "RecordSet has no row.");
    ASSERT(r < recordset->totalRowCount(), "Row is out of range.");
    //ASSERT(c < recordset->columnCount(), "Column is out of range.");
    auto &cv = recordset->row(r)[c];
    return cv.isEmpty();
}

bool RecordSet::IsNull(std::size_t r, const std::string &s)
{
    ASSERT(recordset, "RecordSet cannot be null.");
    ASSERT(recordset->totalRowCount() > 0, "RecordSet has no row.");
    ASSERT(r < recordset->totalRowCount(), "Row is out of range.");
    auto &cv = recordset->row(r)[s];
    return cv.isEmpty();
}

std::size_t RecordSet::RowCount()
{
    ASSERT(recordset, "RecordSet cannot be null.");
    return recordset->getTotalRowCount();
}

std::size_t RecordSet::Find(std::size_t c, const negerns::Variant &value)
{
    ASSERT(recordset, "RecordSet cannot be null.");
    ASSERT(c < recordset->columnCount(), "Column is out of range.");
    size_t count = 0;
    auto s2 = value.convert<std::string>();
    auto pos = std::find_if(recordset->begin(), recordset->end(),
        [c, &value, &s2, &count](poco::data::Row &r) {
            auto s1 = r.get(c).convert<std::string>();
            count++;
            return s1 == s2;
        });
    return count - 1;
}

Row RecordSet::GetRow()
{
    ASSERT(recordset, "RecordSet cannot be null.");
    ASSERT(recordset->columnCount() > 0, "RecordSet has no row structure.");
    //ASSERT(i > 0, "Wrong number of rows to insert.");

    // TODO: Execute a statement that retrieves row structure use it if there
    //       is no structure available yet.

    Row row;
    // Set the column values of a temporary row to a default.
    // This is necessary because Poco Row append function will throw an
    // exception when adding a column with an empty or null value.
    poco::dyn::Var v(0);
    auto count = recordset->columnCount();
    for (std::size_t c = 0; c < count; ++c) {
        auto s = recordset->columnName(c);
        row.append(s, v);
    }
    // Set column values to empty
    for (std::size_t c = 0; c < count; ++c) {
        row[c].empty();
    }
    ASSERT(count == row.fieldCount(), "Source and destination row field count mismatch.");
    return row;
}

void RecordSet::SetRecordSet(poco::data::RecordSet *rs)
{
    recordset = rs;
}
