#include <negerns/data/internal/statementwrapper.h>

using namespace negerns::data::internal;

StatementWrapper::~StatementWrapper() { }

StatementWrapper::StatementWrapper(const poco::data::Statement &s) :
    poco::data::Statement(s)
{ }

poco::data::MetaColumn StatementWrapper::GetMetaColumn(std::size_t pos)
{
    return poco::data::Statement::metaColumn(pos);
}

poco::data::MetaColumn StatementWrapper::GetMetaColumn(const std::string &name)
{
    return poco::data::Statement::metaColumn(name);
}
