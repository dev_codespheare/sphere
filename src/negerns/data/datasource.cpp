#include <Poco/Data/ODBC/Connector.h>
#include <negerns/core/log.h>
#include <negerns/core/message.h>
#include <negerns/core/string.h>
#include <negerns/data/datasource.h>

using namespace negerns::data;

DataSource::DataSource() :
    connector(DataSource::GetConnector(Connector::ODBC))
{ }

DataSource::DataSource(const std::string& dsn,
    const std::string& uid,
    const std::string& pwd) :
    connector(DataSource::GetConnector(Connector::ODBC)),
    dataSourceName(dsn),
    userId(uid),
    userPassword(pwd)
{ }

DataSource::~DataSource() { }

std::string DataSource::GetConnector(Connector c)
{
    std::string s;
    switch (c) {
        case Connector::ODBC:
            s = "odbc";
            break;
        case Connector::SQLite:
            s = "sqlite";
            break;
        default:
            throw std::runtime_error("Unknown DataSource::Connector.");
    }
    return s;
}

void DataSource::SetConnector(const std::string& c)
{
    connector = c;
}

void DataSource::SetDataSourceName(const std::string& dsn)
{
    dataSourceName = dsn;
}

void DataSource::SetUser(const std::string& uid)
{
    userId = uid;
}

void DataSource::SetPassword(const std::string& pwd)
{
    userPassword = pwd;
}

void DataSource::SetConnectionName(const std::string& cn)
{
    connectionName = cn;
}

const std::string & DataSource::GetConnector() const
{
    return connector;
}

const std::string & DataSource::GetDataSourceName() const
{
    return dataSourceName;
}

const std::string & DataSource::GetUser() const
{
    return userId;
}

const std::string & DataSource::GetPassword() const
{
    return userPassword;
}

const std::string & DataSource::GetConnectionName() const
{
    return connectionName;
}

const std::string DataSource::Get() const
{
    std::string cs(negerns::string::Format("DSN=%1;UID=%2;PWD=%3",
        dataSourceName, userId, userPassword));
    if (!connectionName.empty()) {
        cs.append(";CONNECTIONNAME=").append(connectionName);
    }
    return std::move(cs);
}
