#include <negerns/data/bufferstore.h>

using namespace negerns::data;

BufferStore::BufferStore() :
    NameIndexContainer()
{
    column.SetNameIndex(this);
    original.SetNameIndex(this);
    //inserted.SetNameIndex(this);
    modified.SetNameIndex(this);
    //deleted.SetNameIndex(this);
}

BufferStore::~BufferStore() { }

bool BufferStore::IsModified() const
{
#if 0
    bool status = false;
    std::size_t count = column.Count();
    for (std::size_t i = 0; i < column.Count(); ++i) {
        if (original(0, i) != modified(0, i)) {
            status = true;
            break;
        }
    }
    return status;
#endif
    return true;
}
