#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/message.h>
#include <negerns/core/memory.h>
#include <negerns/data/dataaccess.h>
#include <negerns/data/transaction.h>

using namespace negerns::data;

class DataAccess::DataAccessImp
{
public:
    DataAccessImp() = default;
    virtual ~DataAccessImp() = default;

    /// Container for data sources.
    ///
    /// \see DataAccess::Add
    std::vector<DataSource> datasources;

    /// Default session pool container index.
    /// Used when automatically acquiring a reference to a session pool
    /// instance from the session pool container.
    ///
    /// The default is the first session pool container index.
    ///
    /// The default may be set when adding a data source via \c DataAccess::Add
    /// or via a call to DataAccess::SetDefault.
    ///
    /// \see DataAccess::Add
    /// \see DataAccess::SetDefault
    std::size_t defaultSessionPoolIndex = 0;

    /// Session pools.
    std::vector<poco::data::SessionPool*> sessionPools;

    /// Data source initialization flag
    bool initialized = false;

    /// Error information.
    std::string result;
};



// -----------------------------------------------------------------------------



DataAccess::DataAccessImp* DataAccess::Implementation(DataAccess::DataAccessImp *p)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static DataAccess::DataAccessImp *instance = nullptr;

    if (p != nullptr && instance == nullptr) {
        // Assignment
        instance = p;
        return nullptr;
    } else if (p == nullptr && instance != nullptr) {
        // Query
        return instance;
    } else if (p != nullptr && instance != nullptr) {
        // Reassignment
        FAIL("Attempt to reassign a value to a single-instance object.");
        return nullptr;
    } else if (p == nullptr && instance == nullptr) {
        // Possible query or an attempt to initialize object with a null value.
        // But we do not allow initialization to null, so, this must be a query
        // returning the uninitialized value (nullptr).
        return nullptr;
    } else {
        throw std::runtime_error("Should not execute this code.");
        return nullptr;
    }
}

DataAccess::DataAccess()
{
    // Create implementation object on first instance of this class.
    // This allows log messages to be buffered.
    if (DataAccess::Implementation() == nullptr) {
        DataAccess::Implementation(new DataAccess::DataAccessImp());
    }
}

DataAccess::~DataAccess() { }

void DataAccess::Init(const int minSessions,
    const int maxSessions,
    const int idleTimeInSecs)
{
    LOG_FUNCTION();
    auto *ptr = DataAccess::Implementation();
    ASSERT(ptr, "DataAccess implementation is null.\n"
        "Create a DataAccess object first to initialize the implementation object.");
    ASSERT(!ptr->initialized, "DataAccess::Init must only be called once.");
    log::info("DataAccess initialization.");
    log::debug("  Session pool settings:\n" \
        "  Minimum: %1\n" \
        "  Maximum: %2\n" \
        "  Idle Time (sec): %3",
        minSessions, maxSessions, idleTimeInSecs);
    ASSERT(maxSessions <= 32, "Maximum allowed sessions out of range (Poco).");
    if (auto count = ptr->datasources.size()) {
        log::info("  Processing data sources: %1", count);
        for (auto ds : ptr->datasources) {
            auto connString(ds.Get());
            auto *sp = new poco::data::SessionPool(ds.GetConnector(),
                connString,
                minSessions,
                maxSessions,
                idleTimeInSecs);
            sp->setFeature("emptyStringIsNull", true);
            ASSERT(sp != nullptr, "  Database session pool could not be created",
                connString);
            ptr->sessionPools.push_back(sp);
            log::debug(connString);
        }
        log::info("  Session pools ready.");
    }
    ptr->initialized = true;
    log::info("DataAccess initialization complete.");
}

void DataAccess::UnInit()
{
    LOG_FUNCTION();
    auto *ptr = DataAccess::Implementation();
    ASSERT(ptr, "DataAccess implementation is null.\n"
        "Create a DataAccess object first to initialize the implementation object.");
    ASSERT(ptr->initialized, "DataAccess is uninitialized.");
    log::info("DataAccess uninitialization.\n"
              "  Shutting down session pools: %1", ptr->sessionPools.size());
    if (ptr->sessionPools.size() > 0) {
        for (auto *sp : ptr->sessionPools) {
            sp->shutdown();
            delete sp;
        }
    }
    ptr->sessionPools.clear();
    ptr->initialized = false;
    log::info("  Session pools uninitialized.");
    log::info("DataAccess uninitialization complete.");
}

bool DataAccess::IsInitialized()
{
    auto *ptr = DataAccess::Implementation();
    ASSERT(ptr, "DataAccess implementation is null.\n"
        "Create a DataAccess object first to initialize the implementation object.");
    return ptr->initialized;
}

void DataAccess::SetDefault(std::size_t n)
{
    auto *ptr = DataAccess::Implementation();
    ASSERT(ptr, "DataAccess implementation is null.\n"
        "Create a DataAccess object first to initialize the implementation object.");
    ASSERT(ptr->initialized, "DataAccess is uninitialized.");
    ASSERT(!ptr->sessionPools.empty(), "Session pool container is empty.");
    ASSERT(n < ptr->sessionPools.size(), "Session pool container index is out of range.");
    ptr->defaultSessionPoolIndex = n;
}

Transaction* DataAccess::GetTransaction()
{
    auto *ptr = DataAccess::Implementation();
    ASSERT(ptr, "DataAccess implementation is null.\n"
        "Create a DataAccess object first to initialize the implementation object.");
    ASSERT(ptr->initialized, "DataAccess is uninitialized.");
    return GetTransaction(ptr->defaultSessionPoolIndex);
}

Transaction* DataAccess::GetTransaction(std::size_t n)
{
    auto *ptr = DataAccess::Implementation();
    ASSERT(ptr, "DataAccess implementation is null.\n"
        "Create a DataAccess object first to initialize the implementation object.");
    ASSERT(ptr->initialized, "DataAccess is uninitialized.");
    ASSERT(!ptr->sessionPools.empty(), "Session pool container is empty.");
    ASSERT(n < ptr->sessionPools.size(), "Session pool container index is out of range.");
    return new Transaction(n);
}

bool DataAccess::IsConnected(std::size_t n)
{
    auto *ptr = DataAccess::Implementation();
    ASSERT(ptr, "DataAccess implementation is null.\n"
        "Create a DataAccess object first to initialize the implementation object.");
    ASSERT(ptr->initialized, "DataAccess is uninitialized.");
    ASSERT(!ptr->sessionPools.empty(), "SessionPools is empty.");
    ASSERT(n < ptr->sessionPools.size(), "SessionPools index is out of range.");
    bool status = false;
    try {
        auto *sp = ptr->sessionPools[n];
        poco::data::Session session(sp->get());
        return session.isConnected();
    } catch (poco::data::ConnectionFailedException& ex) {
        std::string info(ex.displayText());
        log::fatal(info);
        Message::Error(info);
    } catch (poco::Exception& ex) {
        std::string info(ex.displayText());
        log::fatal(info);
        Message::Error(info);
    }
    return status;
}

bool DataAccess::IsEmpty()
{
    auto *ptr = DataAccess::Implementation();
    ASSERT(ptr, "DataAccess implementation is null.\n"
        "Create a DataAccess object first to initialize the implementation object.");
    ASSERT(ptr->initialized, "DataAccess is uninitialized.");
    return ptr->sessionPools.empty();
}

void DataAccess::Add(const DataSource &ds, bool isDefault)
{
    auto *ptr = DataAccess::Implementation();
    ASSERT(ptr, "DataAccess implementation is null.\n"
        "Create a DataAccess object first to initialize the implementation object.");
    ASSERT(!ptr->initialized, "DataAccess is already initialized.");
    ptr->datasources.push_back(ds);
    if (isDefault) {
        ptr->defaultSessionPoolIndex = ptr->datasources.size() - 1;
    }
}

bool DataAccess::Test(const DataSource &ds)
{
    auto *ptr = DataAccess::Implementation();
    ASSERT(ptr, "DataAccess implementation is null.\n"
        "Create a DataAccess object first to initialize the implementation object.");
    bool status = false;
    ptr->result = "";
    try {
        poco::data::Session session(ds.GetConnector(), ds.Get());
        poco::data::Statement insert(session);
        poco::DateTime dt;
        dt.makeLocal(8);
        std::string dts = poco::DateTimeFormatter::format(dt.timestamp(),
            poco::DateTimeFormat::ISO8601_FORMAT);
#if EXCLUDE_TEST
        insert << "insert into test values(default, ?)", poco::data::keyword::use(dts);
        int n = insert.execute();
#endif
        return session.isConnected();
    } catch (poco::data::ConnectionFailedException& ex) {
        ptr->result = ex.displayText();
        log::fatal(ptr->result);
    } catch (poco::Exception& ex) {
        ptr->result = ex.displayText();
        log::fatal(ptr->result);
    }
    return status;
}

std::string DataAccess::GetTestResult()
{
    auto *ptr = DataAccess::Implementation();
    ASSERT(ptr, "DataAccess implementation is null.\n"
        "Create a DataAccess object first to initialize the implementation object.");
    return ptr->result;
}

poco::data::SessionPool * DataAccess::GetSessionPool(std::size_t n)
{
    auto *ptr = DataAccess::Implementation();
    ASSERT(ptr, "DataAccess implementation is null.\n"
        "Create a DataAccess object first to initialize the implementation object.");
    ASSERT(ptr->initialized, "DataAccess is uninitialized.");
    ASSERT(!ptr->sessionPools.empty(), "SessionPools is empty.");
    ASSERT(n < ptr->sessionPools.size(), "SessionPools index is out of range.");
    return ptr->sessionPools.at(n);
}

std::unique_ptr<poco::data::Session> DataAccess::GetSession()
{
    auto *ptr = DataAccess::Implementation();
    ASSERT(ptr, "DataAccess implementation is null.\n"
        "Create a DataAccess object first to initialize the implementation object.");
    ASSERT(ptr->initialized, "DataAccess is uninitialized.");
    return GetSession(ptr->defaultSessionPoolIndex);
}

std::unique_ptr<poco::data::Session> DataAccess::GetSession(std::size_t n)
{
    auto *ptr = DataAccess::Implementation();
    ASSERT(ptr, "DataAccess implementation is null.\n"
        "Create a DataAccess object first to initialize the implementation object.");
    ASSERT(ptr->initialized, "DataAccess is uninitialized.");
    auto *sp = ptr->sessionPools.at(n);
    return negerns::make_unique(new poco::data::Session(sp->get()));
}
