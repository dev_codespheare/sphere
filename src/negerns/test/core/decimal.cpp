#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <climits>
#include <negerns/core/string/convert.h>
#include <negerns/core/decimal.h>
#include <negerns/test/global.h>

// negerns_test --build_info=yes --show-progress=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_core_decimal
// negerns_test --log_level=test_suite --run_test=suite_core_decimal

BOOST_AUTO_TEST_SUITE(suite_core_decimal)

BOOST_AUTO_TEST_CASE(ctor)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    // Default ctor
    CHECK_EQ(n::decimal::to_string(n::dec2_t()), "0.00");

    // Unsigned int
    CHECK_EQ(n::decimal::to_string(n::dec2_t(1U)),      "1.00");
    CHECK_EQ(n::decimal::to_string(n::dec2_t(INT_MAX)), "2147483647.00");
    CHECK_EQ(n::decimal::to_string(n::dec2_t(_UI32_MAX)), "4294967295.00");

    // Int
    CHECK_EQ(n::decimal::to_string(n::dec2_t(1)),  "1.00");
    CHECK_EQ(n::decimal::to_string(n::dec2_t(-1)), "-1.00");

    // Int 64-bit
    CHECK_EQ(n::decimal::to_string(n::dec2_t(1LL)),  "1.00");
    CHECK_EQ(n::decimal::to_string(n::dec2_t(-1LL)),  "-1.00");

    // Float
    CHECK_EQ(n::decimal::to_string(n::dec2_t(1.0f)),  "1.00");
    CHECK_EQ(n::decimal::to_string(n::dec2_t(-1.0f)), "-1.00");

    // Double
    CHECK_EQ(n::decimal::to_string(n::dec2_t(1.0)),  "1.00");
    CHECK_EQ(n::decimal::to_string(n::dec2_t(-1.0)), "-1.00");
}

BOOST_AUTO_TEST_CASE(ctor_float_and_double)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(n::dec2_t(1.0f),  n::dec2_t(1.0));
    CHECK_EQ(n::dec2_t(-1.0f),  n::dec2_t(-1.0));
    CHECK_EQ(n::dec2_t(1.0),  n::dec2_t(1.0f));
    CHECK_EQ(n::dec2_t(-1.0),  n::dec2_t(-1.0f));
}

BOOST_AUTO_TEST_CASE(ctor_decimal)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        n::dec2_t d(1.0);
        CHECK_EQ(n::dec2_t(d), d);
    }
    {
        n::dec2_t d(-1.0);
        CHECK_EQ(n::dec2_t(d), d);
    }
    {
        n::dec2_t d(3.14159);
        CHECK_EQ(n::dec2_t(d), d);
    }
}

BOOST_AUTO_TEST_CASE(string_to_decimal)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(n::dec2_t(),     n::decimal::to_dec2("0.00"));
    CHECK_EQ(n::dec2_t(1.0),  n::decimal::to_dec2("1.00"));
    CHECK_EQ(n::dec2_t(-1.0), n::decimal::to_dec2("-1.00"));

    CHECK_EQ(n::dec4_t(),     n::decimal::to_dec4("0.0000"));
    CHECK_EQ(n::dec4_t(1.0),  n::decimal::to_dec4("1.0000"));
    CHECK_EQ(n::dec4_t(-1.0), n::decimal::to_dec4("-1.0000"));

    CHECK_EQ(n::dec6_t(),     n::decimal::to_dec6("0.000000"));
    CHECK_EQ(n::dec6_t(1.0),  n::decimal::to_dec6("1.000000"));
    CHECK_EQ(n::dec6_t(-1.0), n::decimal::to_dec6("-1.000000"));

    // Less zeroes in string

    CHECK_EQ(n::dec2_t(),     n::decimal::to_dec2("0.0"));
    CHECK_EQ(n::dec2_t(1.0),  n::decimal::to_dec2("1.0"));
    CHECK_EQ(n::dec2_t(-1.0), n::decimal::to_dec2("-1.0"));

    CHECK_EQ(n::dec4_t(),     n::decimal::to_dec4("0.0"));
    CHECK_EQ(n::dec4_t(1.0),  n::decimal::to_dec4("1.0"));
    CHECK_EQ(n::dec4_t(-1.0), n::decimal::to_dec4("-1.0"));

    CHECK_EQ(n::dec6_t(),     n::decimal::to_dec6("0.0"));
    CHECK_EQ(n::dec6_t(1.0),  n::decimal::to_dec6("1.0"));
    CHECK_EQ(n::dec6_t(-1.0), n::decimal::to_dec6("-1.0"));

    // More zeroes in string

    CHECK_EQ(n::dec2_t(),     n::decimal::to_dec2("0.000"));
    CHECK_EQ(n::dec2_t(1.0),  n::decimal::to_dec2("1.000"));
    CHECK_EQ(n::dec2_t(-1.0), n::decimal::to_dec2("-1.000"));

    CHECK_EQ(n::dec4_t(),     n::decimal::to_dec4("0.00000"));
    CHECK_EQ(n::dec4_t(1.0),  n::decimal::to_dec4("1.00000"));
    CHECK_EQ(n::dec4_t(-1.0), n::decimal::to_dec4("-1.00000"));

    CHECK_EQ(n::dec6_t(),     n::decimal::to_dec6("0.0000000"));
    CHECK_EQ(n::dec6_t(1.0),  n::decimal::to_dec6("1.0000000"));
    CHECK_EQ(n::dec6_t(-1.0), n::decimal::to_dec6("-1.0000000"));

    // No zeroes in string

    CHECK_EQ(n::dec2_t(),     n::decimal::to_dec2("0."));
    CHECK_EQ(n::dec2_t(1.0),  n::decimal::to_dec2("1."));
    CHECK_EQ(n::dec2_t(-1.0), n::decimal::to_dec2("-1."));

    CHECK_EQ(n::dec4_t(),     n::decimal::to_dec4("0."));
    CHECK_EQ(n::dec4_t(1.0),  n::decimal::to_dec4("1."));
    CHECK_EQ(n::dec4_t(-1.0), n::decimal::to_dec4("-1."));

    CHECK_EQ(n::dec6_t(),     n::decimal::to_dec6("0."));
    CHECK_EQ(n::dec6_t(1.0),  n::decimal::to_dec6("1."));
    CHECK_EQ(n::dec6_t(-1.0), n::decimal::to_dec6("-1."));

    // No decimal point in string

    CHECK_EQ(n::dec2_t(),     n::decimal::to_dec2("0"));
    CHECK_EQ(n::dec2_t(1.0),  n::decimal::to_dec2("1"));
    CHECK_EQ(n::dec2_t(-1.0), n::decimal::to_dec2("-1"));

    CHECK_EQ(n::dec4_t(),     n::decimal::to_dec4("0"));
    CHECK_EQ(n::dec4_t(1.0),  n::decimal::to_dec4("1"));
    CHECK_EQ(n::dec4_t(-1.0), n::decimal::to_dec4("-1"));

    CHECK_EQ(n::dec6_t(),     n::decimal::to_dec6("0"));
    CHECK_EQ(n::dec6_t(1.0),  n::decimal::to_dec6("1"));
    CHECK_EQ(n::dec6_t(-1.0), n::decimal::to_dec6("-1"));
}

BOOST_AUTO_TEST_CASE(to_decx)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(n::dec2_t(1.33), n::decimal::to_dec2("1.33"));
    CHECK_EQ(n::dec2_t(1.33), n::decimal::to_dec2("1.3399"));

    CHECK_EQ(n::dec2_t(1.55), n::decimal::to_dec2("1.55"));
    CHECK_EQ(n::dec2_t(1.55), n::decimal::to_dec2("1.5599"));

    CHECK_EQ(n::dec2_t(1.3333), n::decimal::to_dec2("1.33"));
    CHECK_EQ(n::dec2_t(1.3333), n::decimal::to_dec2("1.3399"));

    CHECK_EQ(n::dec2_t(1.5555), n::decimal::to_dec2("1.56"));
    CHECK_EQ(n::dec2_t(1.5555), n::decimal::to_dec2("1.5699"));

    // With rounding

    CHECK_EQ(n::dec2_t(1.3333), n::decimal::to_dec2r("1.33"));
    CHECK_EQ(n::dec2_t(1.3333), n::decimal::to_dec2r("1.3333"));
    CHECK_LT(n::dec2_t(1.3333), n::decimal::to_dec2r("1.3399"));

    CHECK_EQ(n::dec2_t(1.5555), n::decimal::to_dec2r("1.56"));
    CHECK_GT(n::dec2_t(1.5555), n::decimal::to_dec2r("1.5500"));
    CHECK_EQ(n::dec2_t(1.5555), n::decimal::to_dec2r("1.5555"));
}

BOOST_AUTO_TEST_CASE(add)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        n::dec2_t dec(1.3333);
        MESSAGE("Result: " << dec + n::dec2_t(1));
        MESSAGE("Result: " << dec + 1U);
        MESSAGE("Result: " << dec + 1);
        MESSAGE("Result: " << dec + 1ULL);
    }
}

BOOST_AUTO_TEST_CASE(subtract)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        n::dec2_t dec(1.3333);
        MESSAGE("Result: " << dec - n::dec2_t(1));
        MESSAGE("Result: " << dec - 1U);
        MESSAGE("Result: " << dec - 1);
        MESSAGE("Result: " << dec - 1ULL);
    }
}

BOOST_AUTO_TEST_CASE(multiply)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        n::dec2_t dec1(999);
        n::dec2_t dec2(9999999999);
        n::dec2_t dec(dec1 * dec2);

        MESSAGE("Result: " << dec);
        CHECK_EQ(n::decimal::to_string(dec), "9989999999001.00");
    }
}

BOOST_AUTO_TEST_CASE(limits)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    MESSAGE("INT64 (max):                  " << std::to_string(INT64_MAX));
    MESSAGE("int64_t (numeric_limits max): " << std::to_string(std::numeric_limits<int64_t>::max()));
    MESSAGE("INT64(max) / prec(2):         " << std::to_string(n::decimal::prec2_max));
    MESSAGE("INT64(max) / prec(4):         " << std::to_string(n::decimal::prec4_max));
    MESSAGE("---");
    {
        n::dec2_t dec(n::decimal::prec2_max);
        MESSAGE("Decimal: " << n::decimal::to_string(dec));
        MESSAGE("Float:   " << std::to_string(n::decimal::prec2_max * 1.0f));
        MESSAGE("Double:  " << std::to_string(n::decimal::prec2_max * 1.0));
        // 92,233,720,368,547,758.00
        CHECK_EQ(dec, n::decimal::to_dec2("92233720368547758.00"));
    }

    {
        n::dec2_t dec(n::decimal::prec4_max);
        MESSAGE("Decimal: " << dec);
        MESSAGE("Float:   " << std::to_string(n::decimal::prec4_max * 1.0f));
        MESSAGE("Double:  " << std::to_string(n::decimal::prec4_max * 1.0));
        // 922,337,203,685,477.00
        CHECK_EQ(dec, n::decimal::to_dec2("922337203685477.00"));
    }

    {
        n::dec2_t dec(n::decimal::prec2_max + 1);
        MESSAGE("Decimal: " << dec);
        BOOST_CHECK(dec.is_negative());
        BOOST_CHECK(!dec.is_zero());
    }
}

BOOST_AUTO_TEST_SUITE_END()
