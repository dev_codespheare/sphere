#include <negerns/test/global.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_core_log

BOOST_AUTO_TEST_SUITE(suite_core_log)

BOOST_AUTO_TEST_CASE(test_states)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    CHECK(!negerns::log::is_enabled());
    CHECK(!negerns::log::tracing::is_enabled());
    CHECK(negerns::log::stdoutput::is_enabled());
    CHECK(negerns::log::stdoutput::is_only());

    negerns::log::enable(true);
    negerns::log::tracing::enable(true);
    negerns::log::stdoutput::enable(false);
    negerns::log::stdoutput::only(false);
    CHECK(negerns::log::is_enabled());
    CHECK(negerns::log::tracing::is_enabled());
    CHECK(!negerns::log::stdoutput::is_enabled());
    CHECK(!negerns::log::stdoutput::is_only());

    negerns::log::status(negerns::log::Status::Enable);
    negerns::log::tracing::status(negerns::log::tracing::Status::Enable);
    negerns::log::stdoutput::status(negerns::log::stdoutput::Status::Enable);
    CHECK(negerns::log::is_enabled());
    CHECK(negerns::log::tracing::is_enabled());
    CHECK(negerns::log::stdoutput::is_enabled());

    negerns::log::status(negerns::log::Status::Disable);
    negerns::log::tracing::status(negerns::log::tracing::Status::Disable);
    negerns::log::stdoutput::status(negerns::log::stdoutput::Status::Disable);
    CHECK(!negerns::log::is_enabled());
    CHECK(!negerns::log::tracing::is_enabled());
    CHECK(!negerns::log::stdoutput::is_enabled());
}

BOOST_AUTO_TEST_CASE(test_priority_name_abbr)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    using namespace negerns::log;

    CHECK_EQ(priority_abbr(Priority::Trace),       "T");
    CHECK_EQ(priority_abbr(Priority::Debug),       "D");
    CHECK_EQ(priority_abbr(Priority::Information), "I");
    CHECK_EQ(priority_abbr(Priority::Notice),      "N");
    CHECK_EQ(priority_abbr(Priority::Warning),     "W");
    CHECK_EQ(priority_abbr(Priority::Error),       "E");
    CHECK_EQ(priority_abbr(Priority::Critical),    "C");
    CHECK_EQ(priority_abbr(Priority::Fatal),       "F");
}

BOOST_AUTO_TEST_CASE(test_priority_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    using namespace negerns::log;

    CHECK_EQ(priority_name(Priority::Trace),       "Trace"      );
    CHECK_EQ(priority_name(Priority::Debug),       "Debug"      );
    CHECK_EQ(priority_name(Priority::Information), "Information");
    CHECK_EQ(priority_name(Priority::Notice),      "Notice"     );
    CHECK_EQ(priority_name(Priority::Warning),     "Warning"    );
    CHECK_EQ(priority_name(Priority::Error),       "Error"      );
    CHECK_EQ(priority_name(Priority::Critical),    "Critical"   );
    CHECK_EQ(priority_name(Priority::Fatal),       "Fatal"      );
}

BOOST_AUTO_TEST_CASE(test_log_function)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting(n::log::Status::Enable, n::log::tracing::Status::Enable);

    LOG_FUNCTION("is_testing(): %1 ", true);
}

BOOST_AUTO_TEST_SUITE_END()
