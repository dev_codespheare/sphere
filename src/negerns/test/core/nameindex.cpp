#include <negerns/core/memory.h>
#include <negerns/core/variant.h>
#include <negerns/core/nameindex.h>
#include <negerns/test/global.h>

#include <negerns/poco.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_core_nameindex

//! Interface and container class.
class A :
    public n::NameIndexIFace<std::string>,
    public n::NameIndexContainer
{
public:
    A() : n::NameIndexIFace<std::string>(this) {
        auto f = std::bind(&A::Get, this,
            std::placeholders::_1);
        SetGetDataFunction(f);
    }
    virtual std::string& Get(std::size_t i) override {
        return columntypes[i];
    }
    // Data we wanted to query using a string subscript.
    std::vector<std::string> columntypes;
};

BOOST_AUTO_TEST_SUITE(suite_core_nameindex)

BOOST_AUTO_TEST_CASE(test_nameindex)
{
    LEAVE_TEST_IF_DEBUGGING();
    MESSAGE("NameIndexContainer");

    n::NameIndexContainer a;

    CHECK_EQ(a.Count(), 0);
    CHECK(a.Empty());
    CHECK(a.IsEmpty());
    CHECK(!a.IsFound("first"));

    a.Add("first");
    a.Add("second");
    CHECK_EQ(a.Count(), 2);
    CHECK(!a.Empty());
    CHECK(!a.IsEmpty());

    CHECK_EQ(a.Index("first"), 0);
    CHECK_EQ(a.Index("second"), 1);

    CHECK_EQ(a.GetName(0), "first");
    CHECK_EQ(a.GetName(1), "second");
}

BOOST_AUTO_TEST_CASE(test_iterator)
{
    LEAVE_TEST_IF_DEBUGGING();
    n::NameIndexContainer container;

    container.Add("first");
    container.Add("second");
    CHECK_EQ(container.Count(), 2);

    std::size_t n = 0;
    for (auto &c : container) {
        CHECK_EQ(c, container.GetName(n));
        n++;
    }
}

BOOST_AUTO_TEST_CASE(test_nameindex_local)
{
    LEAVE_TEST_IF_DEBUGGING();
    MESSAGE("NameIndex Framework Local");

    A a;

    a.Add("first");
    a.GetNameIndexContainer()->Add("second");
    CHECK_EQ(a.GetNameIndexContainer()->Count(), 2);

    a.columntypes.push_back("boolean");
    a.columntypes.push_back("string");

    CHECK_EQ(a[0], "boolean");
    CHECK_EQ(a[1], "string");

    CHECK_EQ(a["first"], "boolean");
    CHECK_EQ(a["second"], "string");

    CHECK_EQ(a[0], a["first"]);
    CHECK_EQ(a["second"], a[1]);
}

BOOST_AUTO_TEST_CASE(test_nameindex_detached)
{
    LEAVE_TEST_IF_DEBUGGING();
    MESSAGE("NameIndex Framework Detached");

    class A : public n::NameIndexContainer {
    public:
        A() : NameIndexContainer() { }
    };

    class B : public n::NameIndexIFace<std::string> {
    public:
        B(A *a) : NameIndexIFace(a) {
            auto f = std::bind(&B::Get, this, std::placeholders::_1);
            SetGetDataFunction(f);
        }
        std::string & Get(const std::size_t i) { return columntypes[i]; }
        // Data we wanted to query using a string subscript.
        std::vector<std::string> columntypes;
    };

    A *a = new A();
    B b(a);

    CHECK_EQ(b.Empty(), true);
    CHECK_EQ(b.IsEmpty(), true);

    b.GetNameIndexContainer()->Add("first");
    b.GetNameIndexContainer()->Add("second");

    CHECK_EQ(a->Count(), 2);
    CHECK(!b.Empty());
    CHECK(!b.IsEmpty());

    b.columntypes.push_back("boolean");
    b.columntypes.push_back("string");

    CHECK_EQ(b[0], "boolean");
    CHECK_EQ(b[1], "string");

    CHECK_EQ(b["first"], "boolean");
    CHECK_EQ(b["second"], "string");

    CHECK_EQ(b[0], b["first"]);
    CHECK_EQ(b["second"], b[1]);
}

BOOST_AUTO_TEST_CASE(test_nameindex_detached_2)
{
    LEAVE_TEST_IF_DEBUGGING();
    MESSAGE("NameIndex Framework Detached 2");

    class A : public n::NameIndexContainer {
    public:
        A() : NameIndexContainer() { }
    };

    A *a = new A();

    a->Add("first");
    a->Add("second");

    class C : public n::NameIndexIFace<std::string> {
    public:
        C() : NameIndexIFace() {
            auto f = std::bind(&C::Get, this, std::placeholders::_1);
            SetGetDataFunction(f);
        }
        std::string & Get(const std::size_t i) { return columntypes[i]; }
        // Data we wanted to query using a string subscript.
        std::vector<std::string> columntypes;
    };

    C c;
    c.SetNameIndex(a);

    c.columntypes.push_back("boolean");
    c.columntypes.push_back("string");
    CHECK_EQ(c.GetNameIndexContainer()->Count(), 2);

    CHECK_EQ(c[0], "boolean");
    CHECK_EQ(c[1], "string");

    CHECK_EQ(c["first"], c[0]);
    CHECK_EQ(c["second"], c[1]);

    delete a;
}

BOOST_AUTO_TEST_SUITE_END()
