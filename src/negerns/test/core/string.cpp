#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <wx/arrstr.h>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <negerns/core/string.h>
#include <negerns/test/global.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_core_string

BOOST_AUTO_TEST_SUITE(suite_core_string)

BOOST_AUTO_TEST_CASE(bool_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(to_string(true), "true");
    CHECK_EQ(to_string(false), "false");

    CHECK_EQ(to_string(true, boolean::no_bool_alpha()), "1");
    CHECK_EQ(to_string(false, boolean::no_bool_alpha()), "0");

    CHECK_EQ(to_string(true, boolean::bool_alpha()), "true");
    CHECK_EQ(to_string(false, boolean::bool_alpha()), "false");

    CHECK_EQ(to_string(true, boolean::bool_alpha(Case::Upper)), "TRUE");
    CHECK_EQ(to_string(false, boolean::bool_alpha(Case::Upper)), "FALSE");

    CHECK_EQ(to_string(true, boolean::yes_no()), "yes");
    CHECK_EQ(to_string(false, boolean::yes_no()), "no");

    CHECK_EQ(to_string(true, boolean::yes_no(Case::Upper)), "YES");
    CHECK_EQ(to_string(false, boolean::yes_no(Case::Upper)), "NO");

    CHECK_EQ(to_string(true, boolean::success_failed()), "success");
    CHECK_EQ(to_string(false, boolean::success_failed()), "failed");

    CHECK_EQ(to_string(true, boolean::success_failed(Case::Upper)), "SUCCESS");
    CHECK_EQ(to_string(false, boolean::success_failed(Case::Upper)), "FAILED");
}

BOOST_AUTO_TEST_CASE(test_to_hex)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(to_hex(10, 'x'),  "0xa");
    CHECK_EQ(to_hex(10, 'X'),  "0XA");
#if 0
    CHECK_EQ(to_hex(10, 'h'),  "0hA");
    CHECK_EQ(to_hex(10, '\\'), "\\xA");
    CHECK_EQ(to_hex(10, '#'),  "#A");
#endif
    CHECK_EQ(to_hex(57005, 'x'),  "0xdead");
    CHECK_EQ(to_hex(57005, 'X'),  "0XDEAD");
}

BOOST_AUTO_TEST_CASE(test_to_bin)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(to_bin(1), "0b1");
    CHECK_EQ(to_bin(2), "0b10");
}

BOOST_AUTO_TEST_CASE(test_to_octal)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(to_octal(1),  "o1");
    CHECK_EQ(to_octal(2),  "o2");
    CHECK_EQ(to_octal(16), "o20");
}

BOOST_AUTO_TEST_CASE(string_find)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    std::string s("  hello");
    //std::size_t len = s.length();
    std::size_t n = n::string::rfind_first_not_of("  hello  ", ' ');
    CHECK_EQ(n, 6);
}

BOOST_AUTO_TEST_CASE(string_replace)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        std::string s(n::string::replace("..hello..", ".."));
        CHECK_EQ(s, "hello");
        MESSAGE("Result: " << s);
    }

    {
        std::string s(n::string::replace("....hello..", ".."));
        CHECK_EQ(s, "hello");
        MESSAGE("Result: " << s);
    }
    {
        std::string s(n::string::replace(".....hello...", "...", "."));
        CHECK_EQ(s, "...hello.");
        MESSAGE("Result: " << s);
    }
}

BOOST_AUTO_TEST_CASE(string_reverse)
{
}

BOOST_AUTO_TEST_CASE(test_rfind_first_not_of)
{
}

BOOST_AUTO_TEST_CASE(trim_funcs)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(n::string::trim_left("hello"), "hello");
    CHECK_EQ(n::string::trim_left("  hello"), "hello");

    CHECK_EQ(n::string::trim_right("hello"), "hello");
    CHECK_EQ(n::string::trim_right("hello  "), "hello");

    CHECK_EQ(n::string::trim("hello"), "hello");
    CHECK_EQ(n::string::trim("  hello  "), "hello");

    {
        std::string s("  hello");
        std::string p(s);
        p.append("  ");
        n::string::trim_right_if(p, ' ');
        MESSAGE("Original string: [" << s << "]");
        MESSAGE("Modified string: [" << p << "]");
        CHECK_EQ(p, s);
    }
}

BOOST_AUTO_TEST_CASE(case_funcs)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(n::string::to_lower("HELLO"), "hello");
    CHECK_EQ(n::string::to_lower("hELLO"), "hello");
    CHECK_EQ(n::string::to_lower("HELLO WORLD"), "hello world");
    CHECK_EQ(n::string::to_lower("HELLO WORLD!"), "hello world!");
    CHECK_EQ(n::string::to_lower("HELLO234"), "hello234");

    CHECK_EQ(n::string::to_upper("hello"), "HELLO");
    CHECK_EQ(n::string::to_upper("Hello"), "HELLO");
    CHECK_EQ(n::string::to_upper("hello world"), "HELLO WORLD");
    CHECK_EQ(n::string::to_upper("hello world!"), "HELLO WORLD!");
    CHECK_EQ(n::string::to_upper("hello234"), "HELLO234");
}

BOOST_AUTO_TEST_CASE(concat_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        auto a = n::string::concat("Hello", "world", "!");
        CHECK_EQ(a, "Helloworld!");
    }

    {
        std::string s1("Hello");
        std::string s2("world!");
        std::string s3("The quick brown");
        std::string s4("fox jumped over");
        std::string s5("the lazy dog");
        std::string s6("near the river kwai.");
        auto a = n::string::concat(s1, s2, s3, s4, s5, s6);
        CHECK_EQ(a, "Helloworld!The quick brownfox jumped overthe lazy dognear the river kwai.");
    }
}

BOOST_AUTO_TEST_CASE(split_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        auto a = n::string::split("", '\n');
        auto size = a.size();
        CHECK_EQ(size, 0);
    }

    {
        auto a = n::string::split("hello\nworld", '\n');
        auto size = a.size();
        CHECK_EQ(size, 2);
        CHECK_EQ(a[0], "hello");
        CHECK_EQ(a[1], "world");
    }

    {
        auto a = n::string::split("hello\nworld", '\n', true);
        auto size = a.size();
        CHECK_EQ(size, 2);
        CHECK_EQ(a[0], "hello");
        CHECK_EQ(a[1], "world");
    }

    {
        auto a = n::string::split("hello\n\n\nworld", '\n');
        auto size = a.size();
        CHECK_EQ(size, 2);
        CHECK_EQ(a[0], "hello");
        CHECK_EQ(a[1], "world");
    }

    {
        auto a = n::string::split("hello\n\n\nworld\n\n", '\n', true);
        auto size = a.size();
        CHECK_EQ(size, 5);
        CHECK_EQ(a[0], "hello");
        CHECK_EQ(a[1], "");
        CHECK_EQ(a[2], "");
        CHECK_EQ(a[3], "world");
        CHECK_EQ(a[4], "");
    }

    {
        auto a = n::string::split("hello\nworld", '\n', false, "*");
        auto size = a.size();
        CHECK_EQ(size, 2);
        CHECK_EQ(a[0], "*hello");
        CHECK_EQ(a[1], "*world");
    }

    {
        auto a = n::string::split("hello\nworld", '\n', false, "*", "*");
        auto size = a.size();
        CHECK_EQ(size, 2);
        CHECK_EQ(a[0], "*hello*");
        CHECK_EQ(a[1], "*world*");
    }

    {
        auto a = n::string::split("hello\n\n\nworld\n\n", '\n', true, "*", "*");
        auto size = a.size();
        CHECK_EQ(size, 6);
        CHECK_EQ(a[0], "*hello*");
        CHECK_EQ(a[1], "**");
        CHECK_EQ(a[2], "**");
        CHECK_EQ(a[3], "*world*");
        CHECK_EQ(a[4], "**");
        CHECK_EQ(a[5], "**");
    }
}

BOOST_AUTO_TEST_CASE(join_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    std::string s("The quick\nbrown fox\njumped over\nthe lazy\ndog near\nthe bank\nof the\nriver kwai\nin thailand.");
    auto vec = n::string::split(s, '\n');
    //auto size = vec.size();

    auto a = n::string::join(vec, 1, '\n');
    CHECK_EQ(a, s);
}

BOOST_AUTO_TEST_CASE(string_to_hex_timing)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting(n::assert::Status::Disable);
    set_log_setting();

#ifdef _DEBUG
    const std::size_t count = 1;
#else
    const std::size_t count = 20000000;
#endif

    std::vector<std::string> label {
        "  to_hex2  - ",
        "  to_hex - ",
    };

    std::string res;

    MESSAGE("Iterations: " << count);

    {
        n::Timer sw;
        sw.start();
        for (int i = 0; i < count; ++i) {
            res = n::string::to_hex(i);
        }
        sw.stop();
        auto times = sw.format(label[1]);
        MESSAGE(times);
    }
}

BOOST_AUTO_TEST_CASE(trim_timing)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting(n::assert::Status::Disable);
    set_log_setting();

#ifdef _DEBUG
    const std::size_t count = 1;
#else
    const std::size_t count = 100000;
#endif

    std::vector<std::string> label {
        "  negerns::string::trim_right_if - ",
        "  boost::algorithm::trim_right_if - ",
    };

    std::string s("  abcdefghijklmnopqrstuvwxyz");
    std::string p(s);
    p.append(100, ' ');

    {
        n::Timer sw;
        sw.start();
        for (int i = 0; i < count; ++i) {
            std::string r(p);
            n::string::trim_right_if(r, ' ');
        }
        sw.stop();
        auto times = sw.format(label[0]);
        MESSAGE(times);
    }

    {
        n::Timer sw;
        sw.start();
        for (int i = 0; i < count; ++i) {
            std::string r(p);
            boost::trim_right_if(r, boost::is_any_of(" "));
        }
        sw.stop();
        auto times = sw.format(label[1]);
        MESSAGE(times);
    }
}

BOOST_AUTO_TEST_CASE(case_timing)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting(n::assert::Status::Disable);
    set_log_setting();

    std::string str("Helloworld!The quick brownfox jumped overthe lazy dognear the river kwai.Helloworld!The quick brownfox jumped overthe lazy dognear the river kwai.");
    std::string result;

#ifdef _DEBUG
    const std::size_t count = 1;
#else
    const std::size_t count = 100000;
#endif

    std::vector<std::string> label {
        "  negerns::string::to_lower       - ",
        "  negerns::string::to_case(lower) - ",
        "  boost::to_lower                 - ",
        "  negerns::string::to_upper       - ",
        "  negerns::string::to_case(upper) - ",
        "  boost::to_upper                 - ",
    };

    {
        n::Timer sw;
        sw.start();
        for (int i = 0; i < count; ++i) {
            result = negerns::string::to_lower(str);
        }
        sw.stop();
        auto times = sw.format(label[0]);
        MESSAGE(times);
    }

    {
        n::Timer sw;
        sw.start();
        for (int i = 0; i < count; ++i) {
            result = negerns::string::to_case(str, negerns::string::Case::Lower);
        }
        sw.stop();
        auto times = sw.format(label[1]);
        MESSAGE(times);
    }

    {
        n::Timer sw;
        sw.start();
        for (int i = 0; i < count; ++i) {
            result = boost::algorithm::to_lower_copy(str);
        }
        sw.stop();
        auto times = sw.format(label[2]);
        MESSAGE(times);
    }

    {
        n::Timer sw;
        sw.start();
        for (int i = 0; i < count; ++i) {
            result = negerns::string::to_upper(str);
        }
        sw.stop();
        auto times = sw.format(label[3]);
        MESSAGE(times);
    }

    {
        n::Timer sw;
        sw.start();
        for (int i = 0; i < count; ++i) {
            result = negerns::string::to_case(str, negerns::string::Case::Upper);
        }
        sw.stop();
        auto times = sw.format(label[4]);
        MESSAGE(times);
    }

    {
        n::Timer sw;
        sw.start();
        for (int i = 0; i < count; ++i) {
            result = boost::algorithm::to_upper_copy(str);
        }
        sw.stop();
        auto times = sw.format(label[5]);
        MESSAGE(times);
    }
}

BOOST_AUTO_TEST_CASE(concat_timing)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting(n::assert::Status::Disable);
    set_log_setting();

    std::string s1("Hello");
    std::string s2("world!");
    std::string s3("The quick brown");
    std::string s4("fox jumped over");
    std::string s5("the lazy dog");
    std::string s6("near the river kwai.");
    std::string result("Helloworld!The quick brownfox jumped overthe lazy dognear the river kwai.Helloworld!The quick brownfox jumped overthe lazy dognear the river kwai.");
    std::string a;

#ifdef _DEBUG
    const std::size_t count = 1;
#else
    const std::size_t count = 100000;
#endif

    std::vector<std::string> label {
        "  variadic template/function - ",
        "  negerns::String::Join      - ",
    };

    {
        n::Timer sw;
        sw.start();
        for (int i = 0; i < count; ++i) {
            a = negerns::string::concat(s1, s2, s3, s4, s5, s6, s1, s2, s3, s4, s5, s6);
        }
        sw.stop();
        auto times = sw.format(label[0]);
        MESSAGE(times);
        CHECK_EQ(a, result);
    }

    {
        n::Timer sw;
        sw.start();
        for (int i = 0; i < count; ++i) {
            a = n::string::join({s1, s2, s3, s4, s5, s6, s1, s2, s3, s4, s5, s6}, 0);
        }
        sw.stop();
        auto times = sw.format(label[1]);
        MESSAGE(times);
        CHECK_EQ(a, result);
    }
}

BOOST_AUTO_TEST_CASE(split_string_timing)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting(n::assert::Status::Disable);
    set_log_setting();

    std::string s("Hello\nworld!\nThe quick brown\nfox jumped over\nthe lazy dog\nnear the river kwai.");
    std::vector<std::string> result {"Hello", "world!", "The quick brown", "fox jumped over", "the lazy dog", "near the river kwai."};
    std::vector<std::string> a;

#ifdef _DEBUG
    const std::size_t count = 1;
#else
    const std::size_t count = 100000;
#endif

    std::vector<std::string> label {
        "  negerns::string::Split  - ",
        "  wxSplit                 - ",
        "  boost::algorithm::split - ",
    };

    {
        n::Timer sw;
        sw.start();
        for (int i = 0; i < count; ++i) {
            a = n::string::split(s, '\n');
        }
        sw.stop();
        auto times = sw.format(label[0]);
        MESSAGE(times);
        CHECK(a == result);
    }

    {
        n::Timer sw;
        sw.start();
        for (int i = 0; i < count; ++i) {
            auto lines = wxSplit(s, '\n');
        }
        sw.stop();
        auto times = sw.format(label[1]);
        MESSAGE(times);
    }

    {
        std::vector<std::string> vecstr;
        n::Timer sw;
        sw.start();
        for (int i = 0; i < count; ++i) {
            boost::algorithm::split(vecstr, s, boost::algorithm::is_any_of("\n"));
        }
        sw.stop();
        auto times = sw.format(label[2]);
        MESSAGE(times);
    }
}

BOOST_AUTO_TEST_CASE(join_string_timing)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting(n::assert::Status::Disable);
    set_log_setting();

    std::string result("The quick\nbrown fox\njumped over\nthe lazy\ndog near\nthe bank\nof the\nriver kwai\nin thailand.");
    std::string a;

#ifdef _DEBUG
    const std::size_t count = 1;
#else
    const std::size_t count = 100000;
#endif

    negerns::Timer timer;
    std::vector<std::string> label {
        "  negerns::string::Join  - ",
        "  wxJoin                 - ",
        "  boost::algorithm::join - ",
    };

    {
        auto vec = n::string::split(result, '\n');
        timer.restart();
        for (int i = 0; i < count; ++i) {
            a = n::string::join(vec, 1, '\n');
        }
        MESSAGE(timer.format(label[0]));
        CHECK_EQ(a, result);
    }

    {
        auto vec = wxSplit(result, '\n');
        timer.restart();
        for (int i = 0; i < count; ++i) {
            auto lines = wxJoin(vec, '\n');
        }
        MESSAGE(timer.format(label[1]));
    }

    {
        auto vec = n::string::split(result, '\n');
        timer.restart();
        for (int i = 0; i < count; ++i) {
            std::vector<std::string> strs;
            boost::algorithm::join(vec, " ");
        }
        MESSAGE(timer.format(label[2]));
    }
}

BOOST_AUTO_TEST_SUITE_END()
