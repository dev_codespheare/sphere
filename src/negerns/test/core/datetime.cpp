#include <wx/datetime.h>
#include <Poco/DateTimeParser.h>
#include <Poco/DateTimeFormatter.h>
#include <negerns/core/datetime.h>
#include <negerns/test/global.h>

#include <negerns/poco.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_core_datetime

BOOST_AUTO_TEST_SUITE(suite_core_datetime)

BOOST_AUTO_TEST_CASE(to_from_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    wxDateTime wxdt;
    poco::Timestamp pts;
    poco::DateTime pdt;
    std::string src("1971-10-17 00:00:00");
    std::string dest;

    MESSAGE("Convert string to wxDateTime");
    n::DateTime::Convert(src, wxdt);
    CHECK(wxdt.IsValid());
    dest = n::DateTime::ToString(wxdt);
    CHECK_EQ(src, dest);

    MESSAGE("Convert string to Poco::Timestamp");
    n::DateTime::Convert(src, pts);
    dest = n::DateTime::ToString(pts);
    CHECK_EQ(src, dest);

    MESSAGE("Convert string to Poco::DateTime");
    n::DateTime::Convert(src, pdt);
    dest = n::DateTime::ToString(pdt);
    CHECK_EQ(src, dest);
}

BOOST_AUTO_TEST_CASE(wxdatetime_to_poco)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    poco::Timestamp pts;
    poco::DateTime pdt;
    wxDateTime wxdt;
    std::string fmt("%Y-%m-%d %H:%M:%S");
    std::string src("1971-10-17 00:00:00");
    std::string dest;

    wxdt.ParseFormat(src, fmt);
    CHECK(wxdt.IsValid());

    MESSAGE("Convert wxDateTime to Poco::Timestamp");
    n::DateTime::Convert(wxdt, pts);
    // Convert to strings for comparison
    src = n::DateTime::ToString(wxdt);
    dest = n::DateTime::ToString(pts);
    REQUIRE_EQ(src, dest);

    MESSAGE("Convert wxDateTime to Poco::DateTime");
    n::DateTime::Convert(wxdt, pdt);
    // Convert to strings for comparison
    src = n::DateTime::ToString(wxdt);
    dest = n::DateTime::ToString(pdt);
    REQUIRE_EQ(src, dest);

    MESSAGE("Convert wxDateTime to Poco::Data::Date");
    poco::data::Date pddt;
    n::DateTime::Convert(wxdt, pddt);
    // Convert to strings for comparison
    src = n::DateTime::ToString(wxdt, "%Y-%m-%d");
    dest = n::DateTime::ToString(pddt);
    REQUIRE_EQ(src, dest);
}

BOOST_AUTO_TEST_CASE(poco_to_wxdatetime)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    poco::Timestamp pts;
    poco::DateTime pdt;
    wxDateTime wxdt;
    const std::string fmt("%Y-%m-%d %H:%M:%S");
    std::string src("1971-10-17 00:00:00");
    std::string dest;

    wxdt.ParseFormat(src, fmt);
    CHECK(wxdt.IsValid());

    n::DateTime::Convert(wxdt, pdt);
    n::DateTime::Convert(wxdt, pts);

    MESSAGE("Convert Poco::Timestamp to wxDateTime");
    n::DateTime::Convert(pdt, wxdt);
    dest = n::DateTime::ToString(wxdt);
    REQUIRE_EQ(src, dest);

    MESSAGE("Convert Poco::DateTime to wxDateTime");
    n::DateTime::Convert(pts, wxdt);
    dest = n::DateTime::ToString(wxdt);
    REQUIRE_EQ(src, dest);
}

BOOST_AUTO_TEST_SUITE_END()
