
#if 0

BOOST_AUTO_TEST_CASE(unsigned_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    n::string::format::variant v(1u);
    std::string s(v.c_str());
    MESSAGE("Result: [" << s << "]");
    MESSAGE("Content.str: [" << v.c_str() << "]");
    //MESSAGE("Content.whole_length: " << is.whole_length);
    CHECK_EQ(v.to_string(), "+1");
    CHECK_EQ(s, "+1");
    CHECK_EQ(std::strcmp(v.c_str(), "+1"), 0);
    //BOOST_CHECK(is.whole_length == 1);
}

BOOST_AUTO_TEST_CASE(signed_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    n::string::format::variant v(-1);
    std::string s(v.c_str());
    MESSAGE("Result: [" << s << "]");
    MESSAGE("Content.str: [" << v.c_str() << "]");
    //MESSAGE("Content.whole_length: " << is.whole_length);
    CHECK_EQ(v.to_string(), "-1");
    CHECK_EQ(s, "-1");
    CHECK_EQ(std::strcmp(v.c_str(), "-1"), 0);
    //BOOST_CHECK(is.whole_length == 2);
}

BOOST_AUTO_TEST_CASE(float_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    n::string::format::variant v(1.0);
    //MESSAGE("Content.max: " << fs.max);
    MESSAGE("Convert: 1.0f");
    std::string s(v.c_str());
    MESSAGE("Result: [" + s + "]");
    MESSAGE("Content.str: [" << v.c_str() << "]");
    //MESSAGE("Content.max: " << fs.max);
    //MESSAGE("Content.whole_length: " << fs.whole_length);
    //MESSAGE("Content.length: " << fs.length);
    CHECK_EQ(v.to_string(), "+1.000000000");
    CHECK_EQ(s, "+1.000000000");
    CHECK_EQ(std::strcmp(v.c_str(), "+1.000000000"), 0);
    //BOOST_CHECK(fs.whole_length == 1);
    //BOOST_CHECK(fs.length == 11);

#if 0
    // Insert prefix characters
    MESSAGE("Insert prefix character \'0\'.");
    int insert = 10;
    BOOST_CHECK(fs.prefix(insert, '0') == 8);
    MESSAGE("Content.str: [" << fs.c_str() << "]");
    //MESSAGE("Content.max: " << fs.max);
    MESSAGE("Content.whole_length: " << fs.whole_length);
    MESSAGE("Content.length: " << fs.length);
    BOOST_CHECK(std::strcmp(fs.c_str(), "000000001.000000000") == 0);
    BOOST_CHECK(fs.whole_length == 1);
    // Less the terminating null character
    BOOST_CHECK(fs.length == v5::FLOAT_BUFFER_SIZE - 1);
#endif

#if 0
    // Delete trailing characters
    MESSAGE("Delete trailing characters \'0\'.");
    BOOST_CHECK(fs.unsuffix('0') == 9);
    MESSAGE("Content.str: [" << fs.c_str() << "]");
    //MESSAGE("Content.max: " << fs.max);
    MESSAGE("Content.whole_length: " << fs.whole_length);
    MESSAGE("Content.length: " << fs.length);
    BOOST_CHECK(std::strcmp(fs.c_str(), "000000001.") == 0);
    BOOST_CHECK(fs.whole_length == 1);
    BOOST_CHECK(fs.length == 10);
#endif
}

BOOST_AUTO_TEST_CASE(string_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    {
        n::string::format::variant v("abc");
        std::string s(v.c_str());
        MESSAGE("Result: [" << s << "]");
        MESSAGE("Content.str: [" << v.c_str() << "]");
        CHECK_EQ(v.to_string(), "abc");
        CHECK_EQ(s, "abc");
        CHECK_EQ(std::strcmp(v.c_str(), "abc"), 0);
    }
    {
        std::string str("The quick brown fox jumped over the lazy dog near the bank of the river kwai.");
        n::string::format::variant v(str);
        std::string s(v.c_str());
        MESSAGE("Result: [" << s << "]");
        MESSAGE("Content.str: [" << v.c_str() << "]");
        CHECK_EQ(v.to_string(), str);
        CHECK_EQ(s, str);
        CHECK_EQ(std::strcmp(v.c_str(), str.c_str()), 0);
    }
}

BOOST_AUTO_TEST_CASE(bool_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    {
        n::string::format::variant v(true);
        std::string s(v.c_str());
        MESSAGE("Result: [" << s << "]");
        MESSAGE("Content.str: [" << v.c_str() << "]");
        CHECK_EQ(v.to_string(), "true");
        CHECK_EQ(s, "true");
        CHECK_EQ(std::strcmp(v.c_str(), "true"), 0);
    }

    {
        n::string::format::variant v(false);
        std::string s(v.c_str());
        MESSAGE("Result: [" << s << "]");
        MESSAGE("Content.str: [" << v.c_str() << "]");
        CHECK_EQ(v.to_string(), "false");
        CHECK_EQ(s, "false");
        CHECK_EQ(std::strcmp(v.c_str(), "false"), 0);
    }
}

BOOST_AUTO_TEST_CASE(tm_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    std::tm value = negerns::date::now();

    const unsigned char max_char = 20;
    char iso_dt[max_char];
    strftime(iso_dt, max_char, n::string::format::variant::datetime_format().c_str(), &value);
    std::string r_dt(iso_dt);

    n::string::format::variant v(value);
    std::string s(v.c_str());
    MESSAGE("Output: [" << r_dt << "]");
    MESSAGE("Result: [" << s << "]");
    MESSAGE("Content.str: [" << v.c_str() << "]");
    MESSAGE(v.to_string());
    CHECK_EQ(v.to_string(), r_dt);
    CHECK_EQ(s, r_dt);
    CHECK_EQ(std::strcmp(v.c_str(), r_dt.c_str()), 0);
}

#endif
