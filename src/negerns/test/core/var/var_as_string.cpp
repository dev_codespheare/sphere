#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <boost/any.hpp>
#include <negerns/core/var.h>
#include <boost/container/vector.hpp>

#include <negerns/test/global.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_core_var_as_string
// negerns_test --log_level=test_suite --run_test=suite_core_var_as_string

using variant = negerns::Var;

BOOST_AUTO_TEST_SUITE(suite_core_var_as_string)

BOOST_AUTO_TEST_CASE(unsigned_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    variant v(1u);
    CHECK_EQ(v.as_string(), "1");
}

BOOST_AUTO_TEST_CASE(signed_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    variant v(-1);
    CHECK_EQ(v.as_string(), "-1");
}

BOOST_AUTO_TEST_CASE(float_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        variant v(1.0f);
        CHECK_EQ(v.as_string(1), "1.0");
        CHECK_EQ(v.as_string(2), "1.00");
        CHECK_EQ(v.as_string(3), "1.000");
        CHECK_EQ(v.as_string(4), "1.0000");
        CHECK_EQ(v.as_string(5), "1.00000");
        CHECK_EQ(v.as_string(6), "1.000000");
        CHECK_EQ(v.as_string(7), "1.0000000");
        CHECK_EQ(v.as_string(8), "1.00000000");
        CHECK_EQ(v.as_string(9), "1.000000000");
        CHECK_EQ(v.as_string(),  "1.000000");
    }

    {
        variant v(-1.0f);
        CHECK_EQ(v.as_string(1), "-1.0");
        CHECK_EQ(v.as_string(2), "-1.00");
        CHECK_EQ(v.as_string(3), "-1.000");
        CHECK_EQ(v.as_string(4), "-1.0000");
        CHECK_EQ(v.as_string(5), "-1.00000");
        CHECK_EQ(v.as_string(6), "-1.000000");
        CHECK_EQ(v.as_string(7), "-1.0000000");
        CHECK_EQ(v.as_string(8), "-1.00000000");
        CHECK_EQ(v.as_string(9), "-1.000000000");
        CHECK_EQ(v.as_string(),  "-1.000000");
    }
}

BOOST_AUTO_TEST_CASE(double_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        variant v(1.0);
        CHECK_EQ(v.as_string(1), "1.0");
        CHECK_EQ(v.as_string(2), "1.00");
        CHECK_EQ(v.as_string(3), "1.000");
        CHECK_EQ(v.as_string(4), "1.0000");
        CHECK_EQ(v.as_string(5), "1.00000");
        CHECK_EQ(v.as_string(6), "1.000000");
        CHECK_EQ(v.as_string(7), "1.0000000");
        CHECK_EQ(v.as_string(8), "1.00000000");
        CHECK_EQ(v.as_string(9), "1.000000000");
        CHECK_EQ(v.as_string(),  "1.000000");
    }

    {
        variant v(-1.0);
        CHECK_EQ(v.as_string(1), "-1.0");
        CHECK_EQ(v.as_string(2), "-1.00");
        CHECK_EQ(v.as_string(3), "-1.000");
        CHECK_EQ(v.as_string(4), "-1.0000");
        CHECK_EQ(v.as_string(5), "-1.00000");
        CHECK_EQ(v.as_string(6), "-1.000000");
        CHECK_EQ(v.as_string(7), "-1.0000000");
        CHECK_EQ(v.as_string(8), "-1.00000000");
        CHECK_EQ(v.as_string(9), "-1.000000000");
        CHECK_EQ(v.as_string(),  "-1.000000");
    }
}

BOOST_AUTO_TEST_CASE(ldouble_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        variant v(1.0L);
        CHECK_EQ(v.as_string(1), "1.0");
        CHECK_EQ(v.as_string(2), "1.00");
        CHECK_EQ(v.as_string(3), "1.000");
        CHECK_EQ(v.as_string(4), "1.0000");
        CHECK_EQ(v.as_string(5), "1.00000");
        CHECK_EQ(v.as_string(6), "1.000000");
        CHECK_EQ(v.as_string(7), "1.0000000");
        CHECK_EQ(v.as_string(8), "1.00000000");
        CHECK_EQ(v.as_string(9), "1.000000000");
        CHECK_EQ(v.as_string(),  "1.000000");
    }

    {
        variant v(-1.0L);
        CHECK_EQ(v.as_string(1), "-1.0");
        CHECK_EQ(v.as_string(2), "-1.00");
        CHECK_EQ(v.as_string(3), "-1.000");
        CHECK_EQ(v.as_string(4), "-1.0000");
        CHECK_EQ(v.as_string(5), "-1.00000");
        CHECK_EQ(v.as_string(6), "-1.000000");
        CHECK_EQ(v.as_string(7), "-1.0000000");
        CHECK_EQ(v.as_string(8), "-1.00000000");
        CHECK_EQ(v.as_string(9), "-1.000000000");
        CHECK_EQ(v.as_string(),  "-1.000000");
    }
}

BOOST_AUTO_TEST_CASE(string_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        const char *s = "abc";
        variant v(s);
        auto vs = v.as_string();
        CHECK_EQ(vs.compare("abc"), 0);
    }
    {
        variant v("abc");
        auto vs = v.as_string();
        CHECK_EQ(vs.compare("abc"), 0);
    }
    {
        std::string str("The quick brown fox jumped over the lazy dog near the bank of the river kwai.");
        variant v(str);
        auto vs = v.as_string();
        CHECK_EQ(vs.compare(str), 0);
    }
}

BOOST_AUTO_TEST_CASE(bool_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        variant v(true);
        CHECK_EQ(v.as_string(), "true");
    }

    {
        variant v(false);
        CHECK_EQ(v.as_string(), "false");
    }
}

BOOST_AUTO_TEST_CASE(tm_to_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    std::tm value = negerns::date::now();

    const unsigned char max_char = 20;
    char iso_dt[max_char];
    strftime(iso_dt, max_char, ISO_8601_DATETIME_NO_T, &value);
    std::string r_dt(iso_dt);

    variant v(value);
    CHECK_EQ(v.as_string(), r_dt);
}

#if 0

#include <negerns/test/core/experimental/internal/generate.h>

BOOST_AUTO_TEST_CASE(signed_to_string_timing)
{
    LEAVE_TEST_IF_DEBUGGING();
    const std::size_t count = TIMING_COUNT * 4;
    negerns::Timer timer;
    std::vector<std::string> label {
        "  ltoa()                    - ",
        "  cpp::iostreams            - ",
        "  boost::format 1.57        - ",
        "  boost::spirit::karma 1.57 - ",
        "  negerns::string::format   - ",
        "  negerns::to_string        - ",
    };

    std::srand(0);
    std::vector<int> v (10000000);
    std::generate(v.begin(), v.end(), random_fill());

    MESSAGE("Iterations: " << count);

    {
        char buffer[65]; // we don't expect more than 64 bytes to be generated here

        timer.restart();
        for (int i = 0; i < TIMING_COUNT; ++i) {
            ltoa(v[i], buffer, 10);
        }
        MESSAGE(timer.format(label[0]));
    }

    {
        std::stringstream str;

        timer.restart();
        for (int i = 0; i < TIMING_COUNT; ++i) {
            str.str("");
            str << v[i];
        }
        MESSAGE(timer.format(label[1]));
    }

    {
        std::string str;
        boost::format int_format("%d");

        timer.restart();
        for (int i = 0; i < TIMING_COUNT; ++i) {
            str = boost::str(int_format % v[i]);
        }
        MESSAGE(timer.format(label[2]));
    }

    {
        char buffer[65]; // we don't expect more than 64 bytes to be generated here

        timer.restart();
        for (int i = 0; i < TIMING_COUNT; ++i) {
            char *ptr = buffer;
            karma::generate(ptr, int_, v[i]);
            *ptr = '\0';
        }
        MESSAGE(timer.format(label[3]));
    }

    {
        namespace v5 = negerns::string::format;
        timer.restart();

        const char *ptr = nullptr;
        for (int i = 0; i < TIMING_COUNT; ++i) {
            ptr = variant(v[i]).as_string().c_str();
        }
        MESSAGE(timer.format(label[4]));
    }

    {
        timer.restart();

        std::string s;
        for (int i = 0; i < TIMING_COUNT; ++i) {
            s = negerns::string::to_string(v[i]);
        }
        MESSAGE(timer.format(label[5]));
    }

}
#endif

BOOST_AUTO_TEST_SUITE_END()
