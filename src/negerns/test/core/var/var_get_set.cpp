#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <negerns/core/var.h>

#include <negerns/test/global.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_core_var_get_set
// negerns_test --log_level=test_suite --run_test=suite_core_var_get_set

using variant = negerns::Var;

BOOST_AUTO_TEST_SUITE(suite_core_var_get_set)

BOOST_AUTO_TEST_CASE(get_set_bool)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    bool value = true;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_bool(), value);
    CHECK_EQ(v2.get_bool(), v.get_bool());
    CHECK_EQ(v3.get_bool(), value);
}

BOOST_AUTO_TEST_CASE(get_set_char)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    char value = 1;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_char(), value);
    CHECK_EQ(v2.get_char(), v.get_char());
    CHECK_EQ(v3.get_char(), value);
}

BOOST_AUTO_TEST_CASE(get_set_signed_char)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    signed char value = 1;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_schar(), value);
    CHECK_EQ(v2.get_schar(), v.get_schar());
    CHECK_EQ(v3.get_schar(), value);
}

BOOST_AUTO_TEST_CASE(get_set_unsigned_char)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    unsigned char value = 1;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_uchar(), value);
    CHECK_EQ(v2.get_uchar(), v.get_uchar());
    CHECK_EQ(v3.get_uchar(), value);
}

BOOST_AUTO_TEST_CASE(get_set_short)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    short value = -1;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_short(), value);
    CHECK_EQ(v2.get_short(), v.get_short());
    CHECK_EQ(v3.get_short(), value);
}

BOOST_AUTO_TEST_CASE(get_set_unsigned_short)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    unsigned short value = 1;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_ushort(), value);
    CHECK_EQ(v2.get_ushort(), v.get_ushort());
    CHECK_EQ(v3.get_ushort(), value);
}

BOOST_AUTO_TEST_CASE(get_set_int)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    int value = -1;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_int(), value);
    CHECK_EQ(v2.get_int(), v.get_int());
    CHECK_EQ(v3.get_int(), value);
}

BOOST_AUTO_TEST_CASE(get_set_unsigned_int)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    unsigned int value = 1U;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_uint(), value);
    CHECK_EQ(v2.get_uint(), v.get_uint());
    CHECK_EQ(v3.get_uint(), value);
}

BOOST_AUTO_TEST_CASE(get_set_long)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    auto value = -1L;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_long(), value);
    CHECK_EQ(v2.get_long(), v.get_long());
    CHECK_EQ(v3.get_long(), value);
}

BOOST_AUTO_TEST_CASE(get_set_unsigned_long)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    auto value = 1UL;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_ulong(), value);
    CHECK_EQ(v2.get_ulong(), v.get_ulong());
    CHECK_EQ(v3.get_ulong(), value);
}

BOOST_AUTO_TEST_CASE(get_set_long_long)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    long long value = -1LL;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_llong(), value);
    CHECK_EQ(v2.get_llong(), v.get_llong());
    CHECK_EQ(v3.get_llong(), value);
}

 BOOST_AUTO_TEST_CASE(get_set_unsigned_long_long)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    unsigned long long value = 1ULL;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_ullong(), value);
    CHECK_EQ(v2.get_ullong(), v.get_ullong());
    CHECK_EQ(v3.get_ullong(), value);
}

BOOST_AUTO_TEST_CASE(get_set_float)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    float value = -1.0f;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_float(), value);
    CHECK_EQ(v2.get_float(), v.get_float());
    CHECK_EQ(v3.get_float(), value);
}

BOOST_AUTO_TEST_CASE(get_set_double)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    double value = -1.0;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_double(), value);
    CHECK_EQ(v2.get_double(), v.get_double());
    CHECK_EQ(v3.get_double(), value);
}

BOOST_AUTO_TEST_CASE(get_set_long_double)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    long double value = -1.0L;
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_ldouble(), value);
    CHECK_EQ(v2.get_ldouble(), v.get_ldouble());
    CHECK_EQ(v3.get_ldouble(), value);
}

BOOST_AUTO_TEST_CASE(get_set_decimal)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    negerns::dec2_t value(negerns::dec2_t(1.0));
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_fixed2(), value);
    CHECK_EQ(v2.get_fixed2(), v.get_fixed2());
    CHECK_EQ(v3.get_fixed2(), value);
}

BOOST_AUTO_TEST_CASE(get_set_negative_decimal)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    negerns::dec2_t value(negerns::dec2_t(-1.0));
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_fixed2(), value);
    CHECK_EQ(v2.get_fixed2(), v.get_fixed2());
    CHECK_EQ(v3.get_fixed2(), value);
}

BOOST_AUTO_TEST_CASE(get_set_const_char_ptr)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    const char *value = "a";
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_string(), std::string(value));
    CHECK_EQ(v2.get_string(), v.get_string());
    CHECK_EQ(v3.get_string(), std::string(value));
}

BOOST_AUTO_TEST_CASE(get_set_char_array)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    char value[6] = "hello";
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_string(), std::string(value));
    CHECK_EQ(v2.get_string(), v.get_string());
    CHECK_EQ(v3.get_string(), std::string(value));
}

BOOST_AUTO_TEST_CASE(get_set_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    std::string value("a");
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK_EQ(v2.get_string(), value);
    CHECK_EQ(v2.get_string(), v.get_string());
    CHECK_EQ(v3.get_string(), value);
}

BOOST_AUTO_TEST_CASE(get_set_tm)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    std::tm value = negerns::date::now();
    variant v(value);
    variant v2;
    v2.set(value);
    variant v3;
    v3.set(v);
    CHECK(v2.get_tm() == value);
    CHECK(v2.get_tm() == v.get_tm());
    CHECK(v3.get_tm() == value);
}

BOOST_AUTO_TEST_SUITE_END()
