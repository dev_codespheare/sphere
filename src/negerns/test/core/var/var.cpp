#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <boost/any.hpp>
#include <negerns/core/var.h>
#include <boost/container/vector.hpp>

#include <negerns/test/global.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_core_var
// negerns_test --log_level=test_suite --run_test=suite_core_var

using variant = negerns::Var;

BOOST_AUTO_TEST_SUITE(suite_core_var)

BOOST_AUTO_TEST_CASE(test_sizeof)
{
    variant v;
    MESSAGE("variant: sizeof " << sizeof(v));
}

BOOST_AUTO_TEST_CASE(test_empty)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        variant v;
        CHECK(v.empty());
    }

    {
        variant v(nullptr);
        CHECK(v.empty());
    }
}

BOOST_AUTO_TEST_CASE(test_constructor)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        variant v(true);
        CHECK(v.is_bool());
    }

    {
        variant v('a');
        CHECK(v.is_char());
    }

    {
        variant v(static_cast<signed char>(-1));
        CHECK(v.is_schar());
    }

    {
        variant v(static_cast<short>(-1));
        CHECK(v.is_short());
    }

    {
        variant v(static_cast<unsigned short>(1));
        CHECK(v.is_ushort());
    }

    {
        variant v(-1);
        CHECK(v.is_int());
    }

    {
        variant v(1U);
        CHECK(v.is_uint());
    }

    {
        variant v(-1L);
        CHECK(v.is_long());
    }

    {
        variant v(1UL);
        CHECK(v.is_ulong());
    }

    {
        variant v(-1LL);
        CHECK(v.is_llong());
    }

    {
        variant v(1ULL);
        CHECK(v.is_ullong());
    }

    {
        variant v(-1.0f);
        CHECK(v.is_float());
    }

    {
        variant v(1.0f);
        CHECK(v.is_float());
    }

    {
        variant v(-1.0);
        CHECK(v.is_double());
    }

    {
        variant v(1.0);
        CHECK(v.is_double());
    }

    {
        variant v(-1.0L);
        CHECK(v.is_ldouble());
    }

    {
        variant v(1.0L);
        CHECK(v.is_ldouble());
    }

    {
        variant v(negerns::dec2_t(1.0));
        CHECK(v.is_fixed2());
    }

    {
        variant v(nullptr);
        CHECK(v.empty());
    }

    {
        variant v("");
        CHECK(v.is_shortstring());
    }

    {
        variant v("abc");
        CHECK(v.is_shortstring());
    }

    {
        variant v(std::string("abc"));
        CHECK(v.is_shortstring());
    }

    {
        char *s = new char[4];
        std::strcpy(s, "abc");
        variant v(s);
        CHECK(v.is_shortstring());
        delete [] s;
    }

    {
        variant v("hello world!");
        CHECK(v.is_string());
    }

    {
        variant v(std::string("hello world!"));
        CHECK(v.is_string());
    }

    {
        char *s = new char[13];
        std::strcpy(s, "hello world!");
        variant v(s);
        CHECK(v.is_string());
        delete [] s;
    }

    {
        std::tm tm_value;
        variant v(tm_value);
        CHECK(v.is_tm());
    }
}

BOOST_AUTO_TEST_CASE(test_copy_constructor)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        variant v(true);
        variant var(v);
        CHECK(var.is_bool());
    }

    {
        variant v('a');
        variant var(v);
        CHECK(var.is_char());
    }

    {
        variant v(static_cast<signed char>(-1));
        variant var(v);
        CHECK(var.is_schar());
    }

    {
        variant v(static_cast<short>(-1));
        variant var(v);
        CHECK(var.is_short());
    }

    {
        variant v(static_cast<unsigned short>(1));
        variant var(v);
        CHECK(var.is_ushort());
    }

    {
        variant v(-1);
        variant var(v);
        CHECK(var.is_int());
    }

    {
        variant v(1U);
        variant var(v);
        CHECK(var.is_uint());
    }

    {
        variant v(-1L);
        variant var(v);
        CHECK(var.is_long());
    }

    {
        variant v(1UL);
        variant var(v);
        CHECK(var.is_ulong());
    }

    {
        variant v(-1LL);
        variant var(v);
        CHECK(var.is_llong());
    }

    {
        variant v(1ULL);
        variant var(v);
        CHECK(var.is_ullong());
    }

    {
        variant v(-1.0f);
        variant var(v);
        CHECK(var.is_float());
    }

    {
        variant v(1.0f);
        variant var(v);
        CHECK(var.is_float());
    }

    {
        variant v(-1.0);
        variant var(v);
        CHECK(var.is_double());
    }

    {
        variant v(1.0);
        variant var(v);
        CHECK(var.is_double());
    }

    {
        variant v(-1.0L);
        variant var(v);
        CHECK(var.is_ldouble());
    }

    {
        variant v(1.0L);
        variant var(v);
        CHECK(var.is_ldouble());
    }

    {
        variant v(negerns::dec2_t(1.0));
        variant var(v);
        CHECK(var.is_fixed2());
    }

    {
        variant v(nullptr);
        variant var(v);
        CHECK(var.empty());
    }

    {
        variant v("");
        variant var(v);
        CHECK(var.is_shortstring());
        CHECK_EQ(var.get_string(), "");
    }

    {
        variant v("abc");
        variant var(v);
        CHECK(var.is_shortstring());
        CHECK_EQ(var.get_string(), "abc");
    }

    {
        variant v(std::string("abc"));
        variant var(v);
        CHECK(var.is_shortstring());
        CHECK_EQ(var.get_string(), "abc");
    }

    {
        char *s = new char[4];
        std::strcpy(s, "abc");
        variant v(s);
        variant var(v);
        CHECK(var.is_shortstring());
        CHECK_EQ(var.get_string(), "abc");
        delete [] s;
    }

    {
        variant v("hello world!");
        variant var(v);
        CHECK(var.is_string());
        CHECK_EQ(var.get_string(), "hello world!");
    }

    {
        variant v(std::string("hello world!"));
        variant var(v);
        CHECK(var.is_string());
        CHECK_EQ(var.get_string(), "hello world!");
    }

    {
        char *s = new char[13];
        std::strcpy(s, "hello world!");
        variant v(s);
        variant var(v);
        CHECK(var.is_string());
        CHECK_EQ(var.get_string(), "hello world!");
        delete [] s;
    }

    {
        std::tm tm_value;
        variant v(tm_value);
        variant var(v);
        CHECK(var.is_tm());
    }

} // test_copy_constructor

BOOST_AUTO_TEST_CASE(test_clear)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        bool value = true;
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }

    {
        char value = 1;
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }

    {
        signed char value = 1;
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }

    {
        unsigned char value = 1;
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }

    {
        short value = 1;
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }

    {
        unsigned short value = 1;
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }

    {
        int value = 1;
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }

    {
        unsigned int value = 1U;
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }

    {
        long long value = 1;
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }

    {
        unsigned long long value = 1;
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }

    {
        float value = 1.0f;
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }

    {
        double value = 1.0;
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }

    {
        long double value = 1.0L;
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }
} // test_clear

BOOST_AUTO_TEST_CASE(test_clear_fixed)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    negerns::dec2_t value(negerns::dec2_t(1.0));
    variant v(value);
    CHECK_EQ(v.empty(), false);
    v.clear();
    CHECK_EQ(v.empty(), true);
}

BOOST_AUTO_TEST_CASE(test_clear_charptr)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        const char *value = "";
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }

    {
        const char *value = "abc";
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }

    {
        const char *value = "abc";
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }
}

BOOST_AUTO_TEST_CASE(test_clear_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        variant v("");
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }
    {
        std::string value;
        variant v(value);
        CHECK_EQ(v.empty(), false);
        v.clear();
        CHECK_EQ(v.empty(), true);
    }
}

BOOST_AUTO_TEST_CASE(test_clear_tm)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    std::tm value;
    variant v(value);
    CHECK_EQ(v.empty(), false);
    v.clear();
    CHECK_EQ(v.empty(), true);
}

BOOST_AUTO_TEST_CASE(test_assignment)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        bool value = true;
        variant v;
        v = value;
        CHECK_EQ(v.get_bool(), value);
    }

    {
        char value = 1;
        variant v;
        v = value;
        CHECK_EQ(v.get_char(), value);
    }

    {
        signed char value = 1;
        variant v;
        v = value;
        CHECK_EQ(v.get_schar(), value);
    }

    {
        unsigned char value = 1;
        variant v;
        v = value;
        CHECK_EQ(v.get_uchar(), value);
    }

    {
        short value = -1;
        variant v;
        v = value;
        CHECK_EQ(v.get_short(), value);
    }

    {
        unsigned short value = 1;
        variant v;
        v = value;
        CHECK_EQ(v.get_ushort(), value);
    }

    {
        int value = -1;
        variant v;
        v = value;
        CHECK_EQ(v.get_int(), value);
    }

    {
        unsigned int value = 1U;
        variant v;
        v = value;
        CHECK_EQ(v.get_uint(), value);
    }

    {
        long value = -1L;
        variant v;
        v = value;
        CHECK_EQ(v.get_long(), value);
    }

    {
        unsigned long value = 1UL;
        variant v;
        v = value;
        CHECK_EQ(v.get_ulong(), value);
    }

    {
        long long value = -1LL;
        variant v;
        v = value;
        CHECK_EQ(v.get_llong(), value);
    }

    {
        unsigned long long value = 1ULL;
        variant v;
        v = value;
        CHECK_EQ(v.get_ullong(), value);
    }

    {
        float value = -1.0f;
        variant v;
        v = value;
        CHECK_EQ(v.get_float(), value);
    }

    {
        double value = -1.0;
        variant v;
        v = value;
        CHECK_EQ(v.get_double(), value);
    }

    {
        long double value = -1.0L;
        variant v;
        v = value;
        CHECK_EQ(v.get_ldouble(), value);
    }

    {
        negerns::dec2_t value(negerns::dec2_t(1.0));
        variant v;
        v = value;
        CHECK_EQ(v.get_fixed2(), value);
    }

    {
        negerns::dec2_t value(negerns::dec2_t(-1.0));
        variant v;
        v = value;
        CHECK_EQ(v.get_fixed2(), value);
    }

    {
        const char *value = "a";
        variant v;
        v = value;
        CHECK_EQ(v.get_string(), value);
    }

    {
        char value[6] = "hello";
        variant v;
        v = value;
        CHECK_EQ(v.get_string(), value);
    }

    {
        std::string value("a");
        variant v;
        v = value;
        CHECK_EQ(v.get_string(), value);
    }

    {
        std::time_t rawtime;
        std::time(&rawtime);
        std::tm value = *std::localtime(&rawtime);
        variant v;
        v = value;
        CHECK(v.get_tm() == value);
    }

} // test_assignment

BOOST_AUTO_TEST_CASE(test_equality)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    {
        variant v;
        variant v2;
        CHECK(v == v2);
    }

    {
        bool value = true;
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_bool());
        CHECK(v2 == v);
    }

    {
        char value = 1;
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_char());
        CHECK(v2 == v);
    }

    {
        unsigned char value = 1;
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_uchar());
        CHECK(v2 == v);
    }

    {
        short value = -1;
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_short());
        CHECK(v2 == v);
    }

    {
        unsigned short value = 1;
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_ushort());
        CHECK(v2 == v);
    }

    {
        int value = -1;
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_int());
        CHECK(v2 == v);
    }

    {
        unsigned int value = 1U;
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_uint());
        CHECK(v2 == v);
    }

    {
        auto value = -1L;
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_long());
        CHECK(v2 == v);
    }

    {
        auto value = 1UL;
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_ulong());
        CHECK(v2 == v);
    }

    {
        long long value = -1LL;
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_llong());
        CHECK(v2 == v);
    }

    {
        unsigned long long value = 1ULL;
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_ullong());
        CHECK(v2 == v);
    }

    {
        float value = -1.0f;
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_float());
        CHECK(v2 == v);
    }

    {
        double value = -1.0;
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_double());
        CHECK(v2 == v);
    }

    {
        long double value = -1.0L;
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_ldouble());
        CHECK(v2 == v);
    }

    {
        variant value(negerns::dec2_t(1.0));
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_fixed2());
        CHECK(v2 == v);
    }

    {
        const char *value = "a";
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == std::string(value));
        CHECK(v2 == v.get_string());
        CHECK(v2 == v);
    }

    {
        char value[2] = "a";
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == std::string(value));
        CHECK(v2 == v.get_string());
        CHECK(v2 == v);
    }

    {
        std::string value("a");
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2.is_shortstring());
        CHECK(v2 == value);
        CHECK(v2 == v.get_string());
        CHECK(v2 == v);
    }

    {
        const char *value = "hello world!";
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == std::string(value));
        CHECK(v2 == v.get_string());
        CHECK(v2 == v);
    }

    {
        char value[13] = "hello world!";
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == std::string(value));
        CHECK(v2 == v.get_string());
        CHECK(v2 == v);
    }

    {
        std::string value("hello world!");
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_string());
        CHECK(v2 == v);
    }

    {
        std::tm value = negerns::date::now();
        variant v(value);
        variant v2;
        v2.set(value);
        CHECK(v2 == value);
        CHECK(v2 == v.get_tm());
        CHECK(v2 == v);
    }

} // test_equality

BOOST_AUTO_TEST_CASE(test_inequality)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    variant v;

    {
        variant vv(true);
        BOOST_CHECK(v != true);
        BOOST_CHECK(v != vv);
    }

    {
        variant vv('a');
        BOOST_CHECK(v != 'a');
        BOOST_CHECK(v != vv);
    }

    {
        variant vv((char)-1);
        BOOST_CHECK(v != (char)-1);
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(-1);
        BOOST_CHECK(v != -1);
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(1U);
        BOOST_CHECK(v != 1U);
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(-1L);
        BOOST_CHECK(v != -1L);
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(1UL);
        BOOST_CHECK(v != 1UL);
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(-1LL);
        BOOST_CHECK(v != -1LL);
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(1ULL);
        BOOST_CHECK(v != 1ULL);
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(-1.0f);
        BOOST_CHECK(v != -1.0f);
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(1.0f);
        BOOST_CHECK(v != 1.0f);
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(-1.0);
        BOOST_CHECK(v != -1.0);
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(1.0);
        BOOST_CHECK(v != 1.0);
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(-1.0L);
        BOOST_CHECK(v != -1.0L);
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(1.0L);
        BOOST_CHECK(v != 1.0L);
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(negerns::dec2_t(-1.0));
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(negerns::dec2_t(1.0));
        BOOST_CHECK(v != vv);
    }

    {
        variant vv("");
        BOOST_CHECK(v != "");
        BOOST_CHECK(v != vv);
    }

    {
        variant vv("abc");
        BOOST_CHECK(v != "abc");
        BOOST_CHECK(v != vv);
    }

    {
        variant vv(std::string("abc"));
        BOOST_CHECK(v != std::string("abc"));
        BOOST_CHECK(v != vv);
    }

    {
        std::tm value;
        variant vv(value);
        BOOST_CHECK(v != value);
        BOOST_CHECK(v != vv);
    }

} // test_inequality

BOOST_AUTO_TEST_CASE(test_reuse)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    variant v;

    {
        v = true;
        BOOST_CHECK(v.get_bool() == true);
        v = 'a';
        BOOST_CHECK(v.get_char() == 'a');
        v = 1;
        BOOST_CHECK(v.get_int() == 1);
        v = 1U;
        BOOST_CHECK(v.get_uint() == 1U);
        v = 1L;
        BOOST_CHECK(v.get_long() == 1L);
        v = 1LL;
        BOOST_CHECK(v.get_llong() == 1LL);
        v = 1.0f;
        BOOST_CHECK(v.get_float() == 1.0f);
        v = 1.0;
        BOOST_CHECK(v.get_double() == 1.0);
        v = negerns::dec2_t(1.0);
        BOOST_CHECK(v.get_fixed2().getAsDouble() == 1.0);
        v = "abc";
        BOOST_CHECK(v.get_string() == "abc");
        v = "hello world!";
        BOOST_CHECK(v.get_string() == "hello world!");
        std::tm value;
        v = value;
        BOOST_CHECK(v.get_tm() == value);
    }

} // test_reuse

BOOST_AUTO_TEST_CASE(test_promotion)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    variant v;

    {
        v = true;
        CHECK_EQ(v.get_bool(), true);
        CHECK_EQ(v.as_char(), static_cast<char>(true));
        CHECK_EQ(v.as_int(), static_cast<int>(true));
        CHECK_EQ(v.as_uint(), static_cast<unsigned int>(true));
        CHECK_EQ(v.as_long(), static_cast<long>(true));
        CHECK_EQ(v.as_ulong(), static_cast<unsigned long>(true));
        CHECK_EQ(v.as_llong(), static_cast<long long>(true));
        CHECK_EQ(v.as_ullong(), static_cast<unsigned long long>(true));
        v = 'a';
        CHECK_EQ(v.get_char(), 'a');
        CHECK_EQ(v.as_int(), static_cast<int>('a'));
        CHECK_EQ(v.as_uint(), static_cast<unsigned int>('a'));
        CHECK_EQ(v.as_long(), static_cast<long>('a'));
        CHECK_EQ(v.as_ulong(), static_cast<unsigned long>('a'));
        CHECK_EQ(v.as_llong(), static_cast<long long>('a'));
        CHECK_EQ(v.as_ullong(), static_cast<unsigned long long>('a'));
        v = -'a';
        CHECK_EQ(v.get_char(), -'a');
        CHECK_EQ(v.as_int(), static_cast<int>(-'a'));
        CHECK_EQ(v.as_long(), static_cast<long>(-'a'));
        CHECK_EQ(v.as_llong(), static_cast<long long>(-'a'));
        v = 1;
        CHECK_EQ(v.get_int(), 1);
        CHECK_EQ(v.as_uint(), static_cast<unsigned int>(1));
        CHECK_EQ(v.as_long(), static_cast<long>(1));
        CHECK_EQ(v.as_ulong(), static_cast<unsigned long>(1));
        CHECK_EQ(v.as_llong(), static_cast<long long>(1));
        CHECK_EQ(v.as_ullong(), static_cast<unsigned long long>(1));
        v = -1;
        CHECK_EQ(v.get_int(), -1);
        CHECK_EQ(v.as_long(), static_cast<long>(-1));
        CHECK_EQ(v.as_llong(), static_cast<long long>(-1));
        v = 1L;
        CHECK_EQ(v.get_long(), 1L);
        CHECK_EQ(v.as_ulong(), static_cast<unsigned long>(1));
        CHECK_EQ(v.as_llong(), static_cast<long long>(1));
        CHECK_EQ(v.as_ullong(), static_cast<unsigned long long>(1));
        v = -1L;
        CHECK_EQ(v.get_long(), -1L);
        CHECK_EQ(v.as_llong(), static_cast<long long>(-1));
    }

} // test_promotion

BOOST_AUTO_TEST_CASE(test_vectorize)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    std::vector<variant> vars;
    variant v(1);
    v.set("hello");
    vars.push_back(v);
    auto s = vars[0];
    BOOST_CHECK(s.get_string() == "hello");
}

BOOST_AUTO_TEST_CASE(test_timing)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

#ifdef _DEBUG
    const std::size_t count = 1;
#else
    const std::size_t count = 1000000;
#endif
    negerns::Timer timer;
    std::vector<std::string> label {
        "  negerns::variant - ",
        "  Boost::Variant   - "
    };

    MESSAGE("Iterations: " << count);

    std::string ss("random long string the quick brown fox.");

    {
        std::vector<variant> vec;
        vec.reserve(count);

        timer.restart();
        for (std::size_t i = 0; i < count; ++i) {
            vec.emplace_back(ss);
            vec.emplace_back(3.14159);
            vec.emplace_back(i);
            vec.emplace_back(false);
            vec.emplace_back("small");
            vec.emplace_back("random long string the quick brown fox.");
            vec.emplace_back(ss);
        }
        MESSAGE(timer.format(label[0]));
    }

    {
        std::vector<boost::any> vec;
        vec.reserve(count);

        timer.restart();
        for (std::size_t i = 0; i < count; ++i) {
            vec.emplace_back(ss);
            vec.emplace_back(3.14159);
            vec.emplace_back(i);
            vec.emplace_back(false);
            vec.emplace_back("small");
            vec.emplace_back("random long string the quick brown fox.");
            vec.emplace_back(ss);
        }
        MESSAGE(timer.format(label[1]));
    }
}

BOOST_AUTO_TEST_SUITE_END()
