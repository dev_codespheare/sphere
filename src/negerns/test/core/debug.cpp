#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <negerns/test/global.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_core_debug

BOOST_AUTO_TEST_SUITE(suite_core_debug)

BOOST_AUTO_TEST_CASE(trigger_assertions)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    int i = 0;
    WARNING(i > 0, "This is a warning assertion. i = %1", i);
    DEBUG(i > 0, "This is a debug assertion. i = %1", i);
    ASSERT(i > 0, "This is an error assertion. i = %1", i);
    FAIL("This is a fatal assertion.");
}

BOOST_AUTO_TEST_CASE(disable_assert)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    int i = 0;
    negerns::assert::enable(false);
    // Do not trigger an assertion but log it
    ASSERT(i == 1);
}

BOOST_AUTO_TEST_CASE_EXPECTED_FAILURES(debug_assert_gui, 2)
BOOST_AUTO_TEST_CASE(assert_no_gui)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    int i = 0;
    // Trigger an assertion but do not show assertion window
    ASSERT(i == 1);
    ASSERT(i == 2, "Expected assertion.");
}

BOOST_AUTO_TEST_SUITE_END()
