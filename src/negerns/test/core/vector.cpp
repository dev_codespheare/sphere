#include <negerns/core/vector.h>
#include <negerns/test/global.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_core_vector

BOOST_AUTO_TEST_SUITE(suite_core_vector)

BOOST_AUTO_TEST_CASE(test_vector)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    n::Vector<std::string> a;

    CHECK_EQ(a.Count(), 0);
    CHECK(a.Empty());
    CHECK(a.IsEmpty());

    a.Add("first");
    a.Add("second");

    CHECK_EQ(a.Count(), 2);
    CHECK(!a.Empty());
    CHECK(!a.IsEmpty());

    CHECK_EQ(a.Get(0), "first");
    CHECK_EQ(a.Get(1), "second");

    CHECK(a.IsFound("first"));
    CHECK(a.IsFound("second"));
    CHECK(!a.IsFound("third"));

    CHECK_EQ(a.Index("first"), 0);

    std::size_t n = 0;
    for (auto &b : a) {
        CHECK_EQ(b, a.Get(n));
        n++;
    }

    a.Clear();

    CHECK_EQ(a.Count(), 0);
    CHECK(a.Empty());
    CHECK(a.IsEmpty());
}

BOOST_AUTO_TEST_SUITE_END()
