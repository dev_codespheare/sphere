#include <negerns/core/version.h>

#include <negerns/test/global.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_core_version

BOOST_AUTO_TEST_SUITE(suite_core_version)

BOOST_AUTO_TEST_CASE(all)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    CHECK(n::Version::at_least(0, 1));
    CHECK(n::Version::is(0, 1, 0));
    CHECK_EQ(n::Version::to_int(), 10000);
    CHECK_EQ(n::Version::to_string(), "0.1.0.0");
    CHECK_EQ(n::Version::to_int(99, 99, 99, 99), 99999999);
}

BOOST_AUTO_TEST_SUITE_END()
