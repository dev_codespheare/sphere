#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <negerns/core/string/format.h>
#include <negerns/test/global.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_core_format

BOOST_AUTO_TEST_SUITE(suite_core_format)

BOOST_AUTO_TEST_CASE(test_format_no_index)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(Format("%0",  7), "");
    CHECK_EQ(Format("%2",  7), "");
    CHECK_EQ(Format("%15", 7), "");
}

BOOST_AUTO_TEST_CASE(format_specifier_boolean)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(Format("%1",   true),  "true");
    CHECK_EQ(Format("%1",   false), "false");

    CHECK_EQ(Format("%1$5", true),  "true ");
    CHECK_EQ(Format("%1<2", true),  "true");

    CHECK_EQ(Format("%1>5", true),  " true");
    CHECK_EQ(Format("%1<5", true),  "true ");

    CHECK_EQ(Format("%1$10", true),  "true      ");
    CHECK_EQ(Format("%1$10", false), "false     ");

    CHECK_EQ(Format("%1<$10", true),  "true      ");
    CHECK_EQ(Format("%1<$10", false), "false     ");

    CHECK_EQ(Format("%1>$10", true),  "      true");
    CHECK_EQ(Format("%1>$10", false), "     false");
}

BOOST_AUTO_TEST_CASE(format_specifier_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(Format("%1",    "a"),     "a");
    CHECK_EQ(Format("%1$4",  "a"),     "a   ");
    CHECK_EQ(Format("%1<$4", "a"),     "a   ");
    CHECK_EQ(Format("%1>$4", "a"),     "   a");
    CHECK_EQ(Format("%1$8",  "hello"), "hello   ");
    CHECK_EQ(Format("%1<$8", "hello"), "hello   ");
    CHECK_EQ(Format("%1>$8", "hello"), "   hello");

    // Special cases

    CHECK_EQ(Format("%1$2",  "hello"), "hello");
    CHECK_EQ(Format("%1<$2", "hello"), "hello");
    CHECK_EQ(Format("%1>$2", "hello"), "hello");

    // With new line character(s)
    CHECK_EQ(Format("%1", "hello\nworld"),   "hello\nworld");
    CHECK_EQ(Format("%1", "hello\rworld"),   "hello\rworld");
    CHECK_EQ(Format("%1", "hello\r\nworld"), "hello\r\nworld");

    // Long string
    std::string s("The quick brown fox jumped over the lazy dog near the bank of the river kwai.");
    CHECK_EQ(Format("%1", s), s);
}

BOOST_AUTO_TEST_CASE(format_specifier_unsigned_integer)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(Format("%1",    7), "7");
    CHECK_EQ(Format("%1$2",  7), "7 ");
    CHECK_EQ(Format("%1<2",  7), "7 ");
    CHECK_EQ(Format("%1>2",  7), " 7");
    CHECK_EQ(Format("%1<-2", 7), "7 ");
    CHECK_EQ(Format("%1>-2", 7), " 7");

    CHECK_EQ(Format("%1+",   7), "+7");
    CHECK_EQ(Format("%1<+2", 7), "+7");
    CHECK_EQ(Format("%1>+2", 7), "+7");

    CHECK_EQ(Format("%1=",   7), " 7");
    CHECK_EQ(Format("%1<=2", 7), " 7");
    CHECK_EQ(Format("%1>=2", 7), " 7");

    CHECK_EQ(Format("%1",    1234), "1234");
    CHECK_EQ(Format("%1$2",  1234), "1234");
    CHECK_EQ(Format("%1<2",  1234), "1234");
    CHECK_EQ(Format("%1>2",  1234), "1234");
    CHECK_EQ(Format("%1<-2", 1234), "1234");
    CHECK_EQ(Format("%1>-2", 1234), "1234");

    CHECK_EQ(Format("%1+",   1234), "+1234");
    CHECK_EQ(Format("%1<+2", 1234), "+1234");
    CHECK_EQ(Format("%1>+2", 1234), "+1234");

    CHECK_EQ(Format("%1$5",  1234), "1234 ");
    CHECK_EQ(Format("%1<5",  1234), "1234 ");
    CHECK_EQ(Format("%1>5",  1234), " 1234");
    CHECK_EQ(Format("%1<-5", 1234), "1234 ");
    CHECK_EQ(Format("%1>-5", 1234), " 1234");

    CHECK_EQ(Format("%1<+5", 1234), "+1234");
    CHECK_EQ(Format("%1>+5", 1234), "+1234");

    CHECK_EQ(Format("%1<+6", 1234), "+1234 ");
    CHECK_EQ(Format("%1>+6", 1234), " +1234");

    CHECK_EQ(Format("%1<=6", 1234), " 1234 ");
    CHECK_EQ(Format("%1>=6", 1234), "  1234");
}

BOOST_AUTO_TEST_CASE(format_specifier_integer)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(Format("%1",    -7), "-7");
    CHECK_EQ(Format("%1$2",  -7), "-7");
    CHECK_EQ(Format("%1<2",  -7), "-7");
    CHECK_EQ(Format("%1>2",  -7), "-7");
    CHECK_EQ(Format("%1<-2", -7), "-7");
    CHECK_EQ(Format("%1>-2", -7), "-7");

    CHECK_EQ(Format("%1+",   -7), "-7");
    CHECK_EQ(Format("%1<+2", -7), "-7");
    CHECK_EQ(Format("%1>+2", -7), "-7");

    CHECK_EQ(Format("%1<=2", -7), "-7");
    CHECK_EQ(Format("%1>=2", -7), "-7");

    CHECK_EQ(Format("%1",    -1234), "-1234");
    CHECK_EQ(Format("%1$2",  -1234), "-1234");
    CHECK_EQ(Format("%1<2",  -1234), "-1234");
    CHECK_EQ(Format("%1>2",  -1234), "-1234");
    CHECK_EQ(Format("%1<-2", -1234), "-1234");
    CHECK_EQ(Format("%1>-2", -1234), "-1234");

    CHECK_EQ(Format("%1+",   -1234), "-1234");
    CHECK_EQ(Format("%1<+2", -1234), "-1234");
    CHECK_EQ(Format("%1>+2", -1234), "-1234");

    CHECK_EQ(Format("%1$6",  -1234), "-1234 ");
    CHECK_EQ(Format("%1<6",  -1234), "-1234 ");
    CHECK_EQ(Format("%1>6",  -1234), " -1234");
    CHECK_EQ(Format("%1<-6", -1234), "-1234 ");
    CHECK_EQ(Format("%1>-6", -1234), " -1234");

    CHECK_EQ(Format("%1<+6", -1234), "-1234 ");
    CHECK_EQ(Format("%1>+6", -1234), " -1234");

    CHECK_EQ(Format("%1<=6", -1234), "-1234 ");
    CHECK_EQ(Format("%1>=6", -1234), " -1234");
}

BOOST_AUTO_TEST_CASE(format_specifier_float)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(Format("%1$4", 1.0f), "1.000000");
    CHECK_EQ(Format("%1$4", -1.0f), "-1.000000");
    CHECK_EQ(Format("%1$10.2", 1.0f), "1.00      ");
    CHECK_EQ(Format("%1$10.2", -1.0f), "-1.00     ");
    CHECK_EQ(Format("%1$4.", 1.0f), "1.000000");
    CHECK_EQ(Format("%1$.2", 1.0f), "1.00");
    CHECK_EQ(Format("%1$.", 1.0f), "1.000000");
    CHECK_EQ(Format("%1$.", 1.0f), "1.000000");
    CHECK_EQ(Format("%1$.", -1.0f), "-1.000000");
    CHECK_EQ(Format("%1$.2", 3.14f), "3.14");
    CHECK_EQ(Format("%1$.4", 3.14159f), "3.1416");

    CHECK_EQ(Format("%1-$10.2", 1.0f), "1.00      ");
    CHECK_EQ(Format("%1+$10.2", 1.0f), "+1.00     ");
    CHECK_EQ(Format("%1=$10.2", 1.0f), " 1.00     ");
}

BOOST_AUTO_TEST_CASE(format_specifier_decimal)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(Format("%1$4", n::dec2_t(1.0f)),  "1.00");
    CHECK_EQ(Format("%1$4", n::dec2_t(-1.0f)), "-1.00");
    CHECK_EQ(Format("%1$6", n::dec2_t(1.0f)),  "1.00  ");
    CHECK_EQ(Format("%1$6", n::dec2_t(-1.0f)), "-1.00 ");
}

BOOST_AUTO_TEST_CASE(format_specifier_tm)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    std::tm value = negerns::date::now();

    const unsigned char max_char = 20;
    char iso_dt[max_char];
    strftime(iso_dt, max_char, ISO_8601_DATETIME_NO_T, &value);

    CHECK_EQ(Format("%1$4", value), std::string(iso_dt));
}

BOOST_AUTO_TEST_CASE(test_format_to_hex)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(Format("%1x", 57005), "0xdead");
    CHECK_EQ(Format("%1X", 57005), "0XDEAD");

    CHECK_EQ(Format("%1<10x", 57005), "0xdead    ");
    CHECK_EQ(Format("%1>10X", 57005), "    0XDEAD");
}

BOOST_AUTO_TEST_CASE(test_format_to_bin)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(Format("%1b", 1), "0b1");
    CHECK_EQ(Format("%1b", 2), "0b10");

    CHECK_EQ(Format("%1<5b", 1), "0b1  ");
    CHECK_EQ(Format("%1>5b", 2), " 0b10");
}

BOOST_AUTO_TEST_CASE(test_format_to_octal)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    CHECK_EQ(Format("%1o", 1),  "o1");
    CHECK_EQ(Format("%1o", 2),  "o2");
    CHECK_EQ(Format("%1o", 16), "o20");

    CHECK_EQ(Format("%1<5o", 1),  "o1   ");
    CHECK_EQ(Format("%1>5o", 2),  "   o2");
    CHECK_EQ(Format("%1>5o", 16), "  o20");
}


#if 0
BOOST_AUTO_TEST_CASE(format_specifier_grouping)
{
    LEAVE_TEST_IF_DEBUGGING();
    {
        auto a = n::string::Format("Test: %'1$4", 1000.0f);
        MESSAGE("Format %1$4");
        MESSAGE("Result: [" + a + "]");
        CHECK_EQ(a, "Test: 1,000.000000");
    }
}
#endif

BOOST_AUTO_TEST_CASE(format_specifier_multiple)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::string;
    set_assert_setting();
    set_log_setting();

    {
        std::string s = Format("%1%2%3", "one", 5, 3.14159);
        CHECK_EQ(s, "one53.141590");
    }
    {
        std::string s = Format("%1\n%2\n%3", "one", 5, 3.14159);
        CHECK_EQ(s, "one\n5\n3.141590");
    }
    {
        std::string s = Format("Prefix string: "
            "%1\n%2\n%3", "one", 5, 3.14159);
        CHECK_EQ(s, "Prefix string: one\n5\n3.141590");
    }
    {
        std::string s("dylan");
        std::string result("hello = one int = 7 float = 1.200000 bool = false singer = dylan char = a");

        std::string b = Format("hello = %1 int = %2 float = %3$.6 bool = %4 singer = %5 char = %6", "one", 7, 1.2, false, s, "a");
        CHECK_EQ(b, result);
    }
}

BOOST_AUTO_TEST_CASE(negerns_format_exceptions)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();

    std::string s("dylan");

    auto a = n::string::Format("%1 %2 %3", "hello", "world");
    CHECK_EQ(a, "hello world ");

    auto b = n::string::Format("%1%2%3", "hello", "world");
    CHECK_EQ(b, "helloworld");
}

BOOST_AUTO_TEST_CASE(format_timing)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting(n::assert::Status::Disable);
    set_log_setting();

#ifdef _DEBUG
    const std::size_t count = 1;
#else
    const std::size_t count = 2000000;
#endif

    // CPPFormat benchmark format string
    //
    // Format:    %0.10f:%04d:%+g:%s:%#X:%c:%%:%%asdf
    // Arguments: 1.234, 42, 3.13, "str", 0XDEAD, (int)'X'
    // Output:    1.2340000000:0042:+3.13:str:0XDEAD:X:%:%asdf
    //
    // https://github.com/cppformat/format-benchmark/blob/master/tinyformat_test.cpp

    std::string b;
    std::string result("1.234000000:  42:3.13:str:0XDEAD:X:%");

    std::vector<std::string> label {
        "negerns::string::Format - ",
        "wxString::Format        - "
    };

    MESSAGE("Iterations: " << count);

    {
        n::Timer sw;
        sw.start();
        for (std::size_t i = 0; i < count; ++i) {
            b = n::string::Format("%1$.9:%2>4:%3$.2:%4:%5X:%6:%%", 1.234000000, 42, 3.13f, "str", 57005, "X");
        }
        sw.stop();
        auto times = sw.format(label[0]);
        MESSAGE(times);
        CHECK_EQ(b, result);
    }
}

BOOST_AUTO_TEST_SUITE_END()
