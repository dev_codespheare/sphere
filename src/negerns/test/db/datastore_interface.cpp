#include <string>
#include <vector>
#include <map>

#include <negerns/data/dataaccess.h>
#include <negerns/data/datasource.h>
#include <negerns/data/datastore.h>

#include <negerns/negerns.h>
#include <negerns/poco.h>

#include <boost/test/unit_test.hpp>
#include <negerns/test/global.h>

using namespace boost::unit_test;

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=Suite_DataStore/interface

BOOST_AUTO_TEST_SUITE(Suite_DataStore)
BOOST_FIXTURE_TEST_SUITE(interface, negerns_data)

BOOST_AUTO_TEST_CASE(column)
{
    n::data::DataStore ds(da->GetTransaction());
    ds.sqlSelect.Set("select * from test_status");
    std::size_t rowcount = 0;

    ds.Execute("delete from test_status");
    ds.Execute("select test_status_insert((0, 'hello 1'))");
    ds.Execute("select test_status_insert((0, 'hello 2'))");
    ds.Execute("select test_status_insert((0, 'hello 3'))");

    rowcount = ds.Retrieve();
    BOOST_CHECK(rowcount == 3);

    BOOST_CHECK(ds.column.Count() == 2);

    ds.column.Get(0).position;
    ds.column.Get(0).name;
    ds.column.Get(0).type;
    ds.column.Get(0).length;
    ds.column.Get(0).precision;
    ds.column.Get(0).nullable;

    ds.column.Get(1).position;
    ds.column.Get(1).name;
    ds.column.Get(1).type;
    ds.column.Get(1).length;
    ds.column.Get(1).precision;
    ds.column.Get(1).nullable;

    ds.column[0].position;
    ds.column[0].name;
    ds.column[0].type;
    ds.column[0].length;
    ds.column[0].precision;
    ds.column[0].nullable;

    ds.column[1].position;
    ds.column[1].name;
    ds.column[1].type;
    ds.column[1].length;
    ds.column[1].precision;
    ds.column[1].nullable;

    ds.column["id"].position;
    ds.column["id"].name;
    ds.column["id"].type;
    ds.column["id"].length;
    ds.column["id"].precision;
    ds.column["id"].nullable;

    ds.column["name"].position;
    ds.column["name"].name;
    ds.column["name"].type;
    ds.column["name"].length;
    ds.column["name"].precision;
    ds.column["name"].nullable;
}

BOOST_AUTO_TEST_CASE(original)
{
    n::data::DataStore ds(da->GetTransaction());
    ds.sqlSelect.Set("select * from test_status");
    std::size_t rowcount = 0;

    ds.Execute("delete from test_status");
    ds.Execute("select test_status_insert((0, 'create'))");

    rowcount = ds.Retrieve();
    BOOST_CHECK(rowcount == 1);

    auto type_1 = ds.column["id"].type;
    auto type_2 = ds.column["name"].type;

    {
        auto a = ds.original(0, 0);
        auto b = ds.original(0, 1);
    }

    BOOST_CHECK(ds.original(0, 1) == "create");

    {
        auto a = ds.original(0, "id");
        auto b = ds.original(0, "name");
    }
}

BOOST_AUTO_TEST_CASE(inserted)
{
    n::data::DataStore ds(da->GetTransaction());
    ds.sqlSelect.Set("select * from test_status");
    ds.sqlInsert.Set("select test_status_insert((?,?))");
    std::size_t rowcount = 0;
    n::data::Parameters p;

    ds.Execute("delete from test_status");
    rowcount = ds.Retrieve();
    BOOST_CHECK(rowcount == 0);

    ds.InsertRow(1);
    ds.inserted.IfNull(0, 0, 6);
    ds.inserted.IfNull(0, 1, "first");

    {
        auto a = ds.inserted(0, 0);
        auto b = ds.inserted(0, 1);
    }

    BOOST_CHECK(ds.inserted(0, 0) == 6);
    BOOST_CHECK(ds.inserted(0, 1) == "first");

    {
        auto a = ds.inserted(0, "id");
        auto b = ds.inserted(0, "name");
    }
}

BOOST_AUTO_TEST_CASE(modified)
{
    n::data::DataStore ds(da->GetTransaction());
    ds.sqlSelect.Set("select * from test_status");
    ds.sqlUpdate.Set("select test_status_update((?,?))");
    std::size_t rowcount = 0;
    n::data::Parameters p;

    ds.Execute("delete from test_status");
    ds.Execute("select test_status_insert((0, 'create'))");
    ds.Execute("select test_status_insert((0, 'read'))");
    ds.Execute("select test_status_insert((0, 'edit'))");
    ds.Execute("select test_status_insert((0, 'delete'))");

    rowcount = ds.Retrieve();
    BOOST_CHECK(rowcount == 4);

    BOOST_CHECK(ds.original(0, "name") == "create");
    BOOST_CHECK(ds.original(1, "name") == "read");
    BOOST_CHECK(ds.original(2, "name") == "edit");
    BOOST_CHECK(ds.original(3, "name") == "delete");

    ds.ModifyRow(1);
    ds.ModifyRow(3);

    BOOST_CHECK(ds.modified.RowCount() == 2);

    ds.modified(1, "name", "changed");
    ds.modified(3, "name", "modified");

    {
        auto a = ds.modified(1, 1);
        auto b = ds.modified(3, 1);
    }

    BOOST_CHECK(ds.modified(1, 1) == "changed");
    BOOST_CHECK(ds.modified(3, 1) == "modified");

    ds.modified(1, 1, "changed 2");
    ds.modified(3, 1, "modified 2");

    {
        auto a = ds.modified(1, 1);
        auto b = ds.modified(3, 1);
    }

    BOOST_CHECK(ds.modified(1, "name") == "changed 2");
    BOOST_CHECK(ds.modified(3, "name") == "modified 2");

    BOOST_CHECK(ds.original(0, "name") == "create");
    BOOST_CHECK(ds.original(1, "name") == "read");
    BOOST_CHECK(ds.original(2, "name") == "edit");
    BOOST_CHECK(ds.original(3, "name") == "delete");

    rowcount = ds.Update();

    BOOST_CHECK(rowcount == 2);
}

BOOST_AUTO_TEST_CASE(deleted)
{
    n::data::DataStore ds(da->GetTransaction());
    ds.sqlSelect.Set("select * from test_status");
    ds.sqlDelete.Set("delete from test_status where id = ?");
    ds.SetPrimaryKeys({0});
    std::size_t rowcount = 0;
    n::data::Parameters p;

    ds.Execute("delete from test_status");
    ds.Execute("select test_status_insert((0, 'create'))");
    ds.Execute("select test_status_insert((0, 'read'))");
    ds.Execute("select test_status_insert((0, 'edit'))");
    ds.Execute("select test_status_insert((0, 'delete'))");

    rowcount = ds.Retrieve();
    BOOST_CHECK(rowcount == 4);

    ds.DeleteRow(0);
    BOOST_CHECK(ds.deleted.RowCount() == 1);

    BOOST_CHECK(ds.deleted(0, 1) == "create");
    BOOST_CHECK(ds.deleted(0, "name") == "create");

    ds.Delete();
    BOOST_CHECK(ds.deleted.RowCount() == 0);

    rowcount = ds.Retrieve();
    BOOST_CHECK(rowcount == 3);
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
