#include <negerns/db/row.h>
#include <negerns/db/column/properties.h>
#include <negerns/db/connection/pool.h>

#include <negerns/test/global.h>
#include <negerns/test/db/fixture.h>

#ifdef _DEBUG
#include <negerns/core/datetime.h>

#include <negerns/db/pool.h>
#include <negerns/db/backend/odbc/sql.h>
#include <negerns/db/sql.h>
#endif
#include <sqlext.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_statement

#if 0
BOOST_AUTO_TEST_SUITE(suite_db_statement)

BOOST_AUTO_TEST_CASE(test_sql)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();
    negerns::log::enable(false);

    auto parameters = get_connection_parameters(true);
    bool status = n::db::Pool::add(parameters, 1);
    REQUIRE_MSG(status, "Connection is not added to the pool.");

    REQUIRE(!n::db::Pool::empty());
    REQUIRE_NEQ(n::db::Pool::count(), 0);
    REQUIRE(n::db::Pool::get(0) != nullptr);

    negerns::db::Sql sql("select id from internal.test");

    REQUIRE(sql.prepare());
    MESSAGE("SQL:    " << sql.get_sql());
    MESSAGE("Native: " << sql.get_native_sql());

    sql.execute();

    n::db::Pool::close();
}

BOOST_AUTO_TEST_CASE(test_native_sql)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    auto parameters = get_connection_parameters();

    auto pool = std::make_unique<Pool>(parameters);
    auto conn = pool->get();
    CHECK(conn != nullptr);
    bool status = conn->is_connected();
    MESSAGE("Connected: " << (status ? "true" : "false"));

    std::string sql("select id, firstname, sex, birthday from internal.person");
    std::string nativeSql;
    auto statement = conn->create_statement();
    REQUIRE(statement->prepare(sql));
    nativeSql = statement->get_native_sql();
    MESSAGE("SQL:    " << sql);
    MESSAGE("Native: " << nativeSql);
}

BOOST_AUTO_TEST_CASE(test_describe_sql)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    auto parameters = get_connection_parameters();

    auto pool = std::make_unique<Pool>(parameters);

    negerns::db::Columns columns;
    {
        MESSAGE("Inner block");
        auto conn = pool->get();
        std::string sql("select id, firstname, sex, birthday from internal.person");
        MESSAGE("SQL: " << sql);
        auto st = conn->create_statement();
        REQUIRE(st->prepare(sql));
        columns = st->get_columns();
        MESSAGE("Inner block end");
    }

    CHECK_EQ(columns.count(), 4);
    MESSAGE("Column count: " << columns.count());
    std::size_t size = 0;
    for (auto &column : columns) {
        MESSAGE("Name:        " << column.name);
        MESSAGE("  SQL Type:  " << n::db::column::sql_type_name(column.type));
        MESSAGE("  C++ Type:  " << n::db::column::type_name(column.type));
        MESSAGE("  Scale:     " << column.scale);

        switch (column.type) {
            case negerns::db::column::Type::Date:
                size = sizeof(DATE_STRUCT);
                break;
            case negerns::db::column::Type::Time:
                size = sizeof(TIME_STRUCT);
                break;
            case negerns::db::column::Type::Timestamp:
                size = sizeof(TIMESTAMP_STRUCT);
                break;
            case negerns::db::column::Type::Numeric:
            case negerns::db::column::Type::Decimal:
                size = sizeof(SQL_NUMERIC_STRUCT);
                break;
            case negerns::db::column::Type::Float:
            case negerns::db::column::Type::Double:
                size = sizeof(double);
                break;
            case negerns::db::column::Type::Real:
                size = sizeof(float);
                break;
            case negerns::db::column::Type::UInt8:
            case negerns::db::column::Type::Int8:
                size = sizeof(char);
                break;
            case negerns::db::column::Type::UInt16:
            case negerns::db::column::Type::Int16:
                size = sizeof(short);
                break;
            case negerns::db::column::Type::UInt32:
            case negerns::db::column::Type::Int32:
                size = sizeof(long);
                break;
            case negerns::db::column::Type::UInt64:
            case negerns::db::column::Type::Int64:
                size = sizeof(_int64);
                break;
            case negerns::db::column::Type::Char:
                size = sizeof(unsigned char);
                break;
            default:
                size = 0;
        }
        MESSAGE("  sizeof:    " << size);

        MESSAGE("  Precision: " << column.precision);
        MESSAGE("  Nullable:  " << n::string::to_string(column.nullable));
        MESSAGE("  Identity:  " << n::string::to_string(column.identity));
        MESSAGE("  Unsigned:  " << n::string::to_string(column.is_unsigned));
        MESSAGE("  Position:  " << n::string::to_string(column.position));
    }
    // TODO: Move test for columns to file columns.cpp
    //MESSAGE("Column Type: " << n::db::column::type_name(columns("firstname").type));
}

BOOST_AUTO_TEST_CASE(test_describe_stored_procedure)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    auto parameters = get_connection_parameters();

    auto pool = std::make_unique<Pool>(parameters);
    auto conn = pool->get();

    {
        std::string insertSql("select test_insert(('rim'::varchar(10), null, '1972-10-10'::date, null, '10:30:00'::time, '8:00:00'::time, 5, 5.87, 1))");
        conn->execute(insertSql);
    }
}

BOOST_AUTO_TEST_CASE(test_transform)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    auto parameters = get_connection_parameters();

    auto pool = std::make_unique<Pool>(parameters);
    auto conn = pool->get();
}

BOOST_AUTO_TEST_CASE(test_statement)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    auto pool = std::make_unique<Pool>(get_connection_parameters());
    auto conn = pool->get();

    auto statement = conn->create_statement();
    //std::string query("select id, name, birthday, current_ttime from internal.test");
    //std::string query("select amount from internal.test");
    std::string sql("select amount from test_select()");
    REQUIRE(statement->prepare(sql));
    auto nativeSql = statement->get_native_sql();
    MESSAGE("SQL:    " << sql);
    MESSAGE("Native: " << nativeSql);

    negerns::db::ExecuteStatus estatus = statement->execute();

    if (runtime_config::log_level() <= log_messages) {
        auto count = statement->get_column_count();
        REQUIRE(count > 0);
        MESSAGE("Count: " << count);
        auto columns = statement->get_columns();
        negerns::Var var;
        for (auto &column : columns) {
            MESSAGE("Name:        " << column.name);
            MESSAGE("  SQL Type:  " << n::db::column::sql_type_name(column.type));
            MESSAGE("  C++ Type:  " << n::db::column::type_name(column.type));
            MESSAGE("  Scale:     " << column.scale);
            MESSAGE("  Precision: " << column.precision);
            MESSAGE("  Nullable:  " << n::string::to_string(column.nullable));
            MESSAGE("  Identity:  " << n::string::to_string(column.identity));
            MESSAGE("  Unsigned:  " << n::string::to_string(column.is_unsigned));
            MESSAGE("  Position:  " << n::string::to_string(column.position));
        }
    }

    REQUIRE(!is_execute_error(estatus));

    //auto rowsfetched = statement->fetch();
    auto rowsfetched = statement->get_fetched_rows();
    REQUIRE_GT(rowsfetched, 0);
#ifdef _DEBUG
    auto count = statement->get_column_count();
    REQUIRE_GT(count, 0);
    auto columns = statement->get_columns();
    auto *is = static_cast<negerns::db::backend::odbc::Statement*>(statement->get_statement());
    std::string s;
    for (std::size_t i = 0; i < count; ++i) {
        negerns::Var var(is->data(i));
        if (var.is_tm()) {
            s = negerns::Format("%1: %2", columns(i).name, var.get_tm());
        } else if (var.is_int()) {
            s = negerns::Format("%1: %2", columns(i).name, var.get_int());
        } else if (var.is_uint()) {
            s = negerns::Format("%1: %2", columns(i).name, var.get_uint());
        } else if (var.is_float()) {
            s = negerns::Format("%1: [%2.2]", columns(i).name, var.get_float());
        } else if (var.is_double()) {
            s = negerns::Format("%1: [%2.2] %3", columns(i).name, var.get_double(), var.get_double());
        } else if (var.is_string()) {
            //s = negerns::Format("%1: %2", columns(i).name, var.get_string());
            auto s2(var.get_string());
            MESSAGE("Length: " << s2.length());
            s.assign(columns(i).name);
            s.append(": ");
            s.append(var.get_string());
        }
        MESSAGE(s);
    }
#endif
    MESSAGE("Done.");
}

BOOST_AUTO_TEST_CASE(test_execute)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    auto parameters = get_connection_parameters();

    auto pool = std::make_unique<Pool>(parameters);
    auto conn = pool->get();

    std::string querySql("select id, name from internal.test");
    auto statement = conn->create_statement(querySql);
    statement->execute();
    auto count = statement->get_column_count();
    REQUIRE(count > 0);
    n::db::Rows rows(std::move(statement));
    auto affectedRows = rows.get_affected_rows();
    auto rowCount = rows.get_row_count();
    MESSAGE("Affected rows: " << affectedRows);
    MESSAGE("Row count:     " << rowCount);
    auto columns = rows.get_columns();
    MESSAGE("SQL: " << querySql);
    MESSAGE("Column count: " << columns.count());
    for (auto &column : columns) {
        MESSAGE("Name:        " << column.name);
        MESSAGE("  SQL Type:  " << n::db::column::sql_type_name(column.type));
        MESSAGE("  C++ Type:  " << n::db::column::type_name(column.type));
        MESSAGE("  Scale:     " << column.scale);
        MESSAGE("  Precision: " << column.precision);
        MESSAGE("  Nullable:  " << n::string::to_string(column.nullable));
        MESSAGE("  Identity:  " << n::string::to_string(column.identity));
        MESSAGE("  Unsigned:  " << n::string::to_string(column.is_unsigned));
        MESSAGE("  Position:  " << n::string::to_string(column.position));
    }
}

BOOST_AUTO_TEST_SUITE_END()
#endif
