#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <string>
#include <negerns/db/connection.h>
#include <negerns/db/connection/parameters.h>
#include <negerns/db/connection/odbc/postgresql/options.h>

#include <negerns/test/global.h>
#include <negerns/test/db/fixture.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_connection_parameters
// negerns_test --log_level=test_suite --run_test=suite_db_connection_parameters

BOOST_AUTO_TEST_SUITE(suite_db_connection_parameters)

BOOST_AUTO_TEST_CASE(test_interface)
{
    LEAVE_TEST_IF_DEBUGGING();
    ::set_assert_setting();
    ::set_log_setting();

    std::string driver("driver");
    std::string server("server");
    std::string port("port");
    std::string database("database");
    std::string user("user");
    std::string password("password");
    std::string connectionName("connectionname");
    std::string connectionTimeOut("timeout");

    namespace pg = negerns::db::connection::odbc::postgresql;

    auto options = std::make_unique<pg::Options>();
    //auto *options = new pg::Options();
    options->set(pg::Option::Driver, driver);
    options->set(pg::Option::Server, server);
    options->set(pg::Option::Port, port);
    options->set(pg::Option::Database, database);
    options->set(pg::Option::User, user);
    options->set(pg::Option::Password, password);
    options->set(pg::Option::ConnectionName, connectionName);
    options->set(pg::Option::ConnectionTimeOut, connectionTimeOut);

    using namespace negerns::db::connection;

    Parameters p(Source::ODBC, std::move(options));
    BOOST_CHECK(p.empty() == false);

    {
        BOOST_MESSAGE("Copy");
        Parameters p2 = p;
        BOOST_CHECK(p2.empty() == false);
    }

    BOOST_CHECK(p.empty() == false);

    {
        BOOST_MESSAGE("Move");
        Parameters p2(p);
        BOOST_CHECK(p2.empty() == false);
    }

    p.clear();
    BOOST_CHECK(p.empty() == true);
}

BOOST_AUTO_TEST_CASE(test_connection_string_1)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    ::set_assert_setting();
    ::set_log_setting();

#ifdef __WINDOWS__
#   ifdef _MSC_VER
    // Must check for 64-bit defines first because wxWidgets
    // also defines 32-bit symbols even when building 64-bit
#       if defined(__WIN64__) || defined(_WIN64)
    odbc::Driver driver(odbc::Driver::PostgreSQL_ANSI_64Bit);
#       else
    odbc::Driver driver(odbc::Driver::PostgreSQL_ANSI_32Bit);
#       endif
#   endif
#endif

    namespace pg = negerns::db::connection::odbc::postgresql;

    auto options = std::make_unique<pg::Options>();
    //auto *options = new pg::Options();
    options->set(pg::Option::Driver, odbc::get_driver_string(driver));
    options->set(pg::Option::Server, "localhost");
    options->set(pg::Option::Port, odbc::get_default_port(driver));
    options->set(pg::Option::Database, "sphere");
    options->set(pg::Option::User, "sphere");
    options->set(pg::Option::Password, "sphere");
#if 0
    options->set(pg::Option::ConnectionName, "boost-test");
    options->set(pg::Option::ReadOnly, "0");
    options->set(pg::Option::LFConversion, "1");
    options->set(pg::Option::UseServerSidePrepare, "1");

    options->set(pg::Option::BoolsAsChar, "1");
    options->set(pg::Option::MaxVarcharSize, "255");
    options->set(pg::Option::UnknownsAsLongVarchar, "1");
    options->set(pg::Option::TextAsLongVarchar, "1");
#endif
    Parameters parameters(Source::ODBC, std::move(options));
    auto cs = parameters.get_connection_string();
    BOOST_MESSAGE("ConnectionString: " << cs);
}

BOOST_AUTO_TEST_CASE(test_connection_string)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    ::set_assert_setting();
    ::set_log_setting();

    auto parameters = get_connection_parameters(true);
    auto cs = parameters.get_connection_string();
    BOOST_MESSAGE("ConnectionString: " << cs);
}

BOOST_AUTO_TEST_SUITE_END()
