#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <memory>
#include <string>
#include <thread>
#include <negerns/db/error.h>
#include <negerns/db/connection/pool.h>
#include <negerns/test/global.h>
#include <negerns/test/db/fixture.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_connection_pool
// negerns_test --log_level=test_suite --run_test=suite_db_connection_pool

BOOST_AUTO_TEST_SUITE(suite_db_connection_pool)

BOOST_AUTO_TEST_CASE_EXPECTED_FAILURES(test_pool_creation_failed, 1)
BOOST_AUTO_TEST_CASE(test_pool_creation_failed)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    auto parameters = get_connection_parameters(false);
    CHECK(!Pool::test(parameters));

    auto pool = std::make_unique<Pool>(parameters);
    CHECK(!pool->empty());
    CHECK_EQ(pool->count(), 1);
    CHECK_EQ(pool->count_free(), 1);
    pool.reset();
}

BOOST_AUTO_TEST_CASE(pool_test)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    auto parameters = get_connection_parameters(true);
    CHECK(Pool::test(parameters));
}

BOOST_AUTO_TEST_CASE(pool_get_connection)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    auto parameters = get_connection_parameters(true);
    CHECK(Pool::test(parameters));

    auto pool = std::make_unique<Pool>(parameters);
    CHECK(pool != nullptr);
    CHECK(!pool->empty());
    CHECK_EQ(pool->count(), 1);
    CHECK_EQ(pool->count_free(), 1);
    {
        Pool::ItemRef iref = pool->get();
        CHECK(iref.first != nullptr);
        CHECK_EQ(pool->count_free(), 1);
    }
    CHECK_EQ(pool->count_free(), 1);
}

BOOST_AUTO_TEST_SUITE_END()
