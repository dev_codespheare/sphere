#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <string>
#include <negerns/db/connection/odbc/driver.h>

#include <negerns/test/global.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_connection_odbc_driver
// negerns_test --log_level=test_suite --run_test=suite_db_connection_odbc_driver

BOOST_AUTO_TEST_SUITE(suite_db_connection_odbc_driver)

BOOST_AUTO_TEST_CASE(test_interface)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection::odbc;

    Driver driver = Driver::None;
    BOOST_CHECK(get_driver_string(driver) == "");
    BOOST_CHECK(get_default_port(driver) == "");

    driver = Driver::PostgreSQL_ANSI_32Bit;
    BOOST_CHECK(get_driver_string(driver) == "{PostgreSQL ANSI}");
    BOOST_CHECK(get_default_port(driver) == "5432");

    driver = Driver::PostgreSQL_Unicode_32Bit;
    BOOST_CHECK(get_driver_string(driver) == "{PostgreSQL Unicode}");
    BOOST_CHECK(get_default_port(driver) == "5432");

    driver = Driver::PostgreSQL_ANSI_64Bit;
    BOOST_CHECK(get_driver_string(driver) == "{PostgreSQL ANSI(x64)}");
    BOOST_CHECK(get_default_port(driver) == "5432");

    driver = Driver::PostgreSQL_Unicode_64Bit;
    BOOST_CHECK(get_driver_string(driver) == "{PostgreSQL Unicode(x64)}");
    BOOST_CHECK(get_default_port(driver) == "5432");
}

BOOST_AUTO_TEST_SUITE_END()
