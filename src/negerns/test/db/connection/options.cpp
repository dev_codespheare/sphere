#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <string>
#include <negerns/db/connection.h>
#include <negerns/db/connection/options.h>
#include <negerns/db/connection/odbc/postgresql/options.h>
#include <negerns/test/global.h>
#include <negerns/test/db/fixture.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_connection_options
// negerns_test --log_level=test_suite --run_test=suite_db_connection_options

BOOST_AUTO_TEST_SUITE(suite_db_connection_options)

BOOST_AUTO_TEST_CASE(test_interface)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;

    std::string driver("driver");
    std::string server("server");
    std::string port("port");
    std::string database("database");
    std::string user("user");
    std::string password("password");
    std::string connectionName("connectionname");
    std::string connectionTimeOut("timeout");

    Options options;

    options.clear();
    BOOST_CHECK(options.empty() == true);

    {
        namespace pg = negerns::db::connection::odbc::postgresql;

        pg::Options options;
        options.set(pg::Option::Driver, "");
        BOOST_CHECK(options.count() == 0);
        BOOST_CHECK(options.exists(pg::Option::Driver) == false);
        BOOST_CHECK(options.get(pg::Option::Driver) == "");

        options.clear();
        BOOST_CHECK(options.empty() == true);
    }

    //. TODO: Move this to specific postgresql test implementation.

    {
        namespace pg = negerns::db::connection::odbc::postgresql;

        pg::Options options;
        options.set(pg::Option::Driver, driver);
        options.set(pg::Option::Server, server);
        options.set(pg::Option::Port, port);
        options.set(pg::Option::Database, database);
        options.set(pg::Option::User, user);
        options.set(pg::Option::Password, password);
        options.set(pg::Option::ConnectionName, connectionName);
        options.set(pg::Option::ConnectionTimeOut, connectionTimeOut);

        BOOST_CHECK(options.count() == 8);

        BOOST_CHECK(options.exists(pg::Option::Driver) == true);
        BOOST_CHECK(options.exists(pg::Option::Server) == true);
        BOOST_CHECK(options.exists(pg::Option::Port) == true);
        BOOST_CHECK(options.exists(pg::Option::Database) == true);
        BOOST_CHECK(options.exists(pg::Option::User) == true);
        BOOST_CHECK(options.exists(pg::Option::Password) == true);
        BOOST_CHECK(options.exists(pg::Option::ConnectionName) == true);
        BOOST_CHECK(options.exists(pg::Option::ConnectionTimeOut) == true);

        BOOST_CHECK(options.get(pg::Option::Driver) == driver);
        BOOST_CHECK(options.get(pg::Option::Server) == server);
        BOOST_CHECK(options.get(pg::Option::Port) == port);
        BOOST_CHECK(options.get(pg::Option::Database) == database);
        BOOST_CHECK(options.get(pg::Option::User) == user);
        BOOST_CHECK(options.get(pg::Option::Password) == password);
        BOOST_CHECK(options.get(pg::Option::ConnectionName) == connectionName);
        BOOST_CHECK(options.get(pg::Option::ConnectionTimeOut) == connectionTimeOut);

        options.clear();
        BOOST_CHECK(options.empty() == true);
    }
}

BOOST_AUTO_TEST_CASE(test_move)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    {
        Options options;
        options.set("one", "1");
        BOOST_CHECK(options.empty() == false);

        auto options2 = options;
        BOOST_CHECK(options2.empty() == false);
    }

    {
        namespace pg = negerns::db::connection::odbc::postgresql;

        pg::Options options;
        options.set(pg::Option::Driver, "driver");
        BOOST_CHECK(options.empty() == false);

        auto options2(options);
        BOOST_CHECK(options2.empty() == false);

        auto options3 = options;
        BOOST_CHECK(options3.empty() == false);
    }
}

BOOST_AUTO_TEST_SUITE_END()
