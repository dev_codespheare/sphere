#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <string>
#include <negerns/core/timer.h>
#include <negerns/db/pool.h>
#include <negerns/db/connection.h>
#include <negerns/db/connection/pool.h>

#include <negerns/test/global.h>
#include <negerns/test/db/fixture.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_connection
// negerns_test --log_level=test_suite --run_test=suite_db_connection

BOOST_AUTO_TEST_SUITE(suite_db_connection)

BOOST_AUTO_TEST_CASE(test_info_odbc_postgresql)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    n::db::Pool::add(get_connection_parameters(), 10);
    auto pool = n::db::Pool::get();

    auto pt = pool->get_pooling_type();
    REQUIRE(pt == pooling::Type::ODBCPooling);
    REQUIRE_EQ(pool->count(), 1);

    {
        std::vector<std::unique_ptr<n::db::Connection>> connections;
        for (int i = 0; i < 5; ++i) {
            connections.push_back(std::make_unique<n::db::Connection>());
        }

        for (int i = 0; i < 5; ++i) {
            connections.push_back(std::make_unique<n::db::Connection>());
        }

        REQUIRE_EQ(pool->count_free(), 1);
        REQUIRE_EQ(pool->count_free(), 1);
    }

    REQUIRE_EQ(pool->count_free(), 1);
    REQUIRE_EQ(pool->count(), 1);

    pool->close();
    pool.reset();

#if 0
    std::string sql(
        "drop table if exists internal.xxx cascade;"
        ""
        "create table internal.xxx ("
        "id serial NOT NULL, "
        "name character varying(10) NOT NULL, "
        "null_name character varying(10), "
        "birthday date NOT NULL, "
        "null_birthday date, "
        "default_time time without time zone DEFAULT '00:00:00', "
        "time_in time without time zone NOT NULL DEFAULT '09:50:25.354', "
        "current_ttime time without time zone DEFAULT 'now', "
        "count integer NOT NULL, "
        "amount money NOT NULL, "
        "active boolean NOT NULL DEFAULT true, "
        "status integer, "
        "CONSTRAINT pk_xxx PRIMARY KEY (id));"
    );
#endif
}

BOOST_AUTO_TEST_SUITE_END()
