#include <string>
#include <vector>
#include <map>

#include <negerns/data/dataaccess.h>
#include <negerns/data/datasource.h>
#include <negerns/data/datastore.h>

#include <negerns/negerns.h>
#include <negerns/poco.h>

#include <boost/test/unit_test.hpp>
#include <negerns/test/global.h>

using namespace boost::unit_test;

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=Suite_DataStore/row_operations

BOOST_AUTO_TEST_SUITE(Suite_DataStore)
BOOST_FIXTURE_TEST_SUITE(row_operations, negerns_data)

BOOST_AUTO_TEST_CASE(insert_rows)
{
    n::data::DataStore ds(da->GetTransaction());
    ds.sqlSelect.Set("select id, name from test_status");
    ds.sqlInsert.Set("select test_status_insert((?, ?))");
    std::size_t rowcount = 0;

    rowcount = ds.Execute("delete from test_status");
    rowcount = ds.Retrieve();
    BOOST_CHECK(rowcount == 0);
    ds.SetDefaults();

    ds.InsertRows(2);
    BOOST_CHECK(ds.inserted.RowCount() == 2);

    ds.inserted(0, 0, 0);
    ds.inserted(0, 1, "first");

    ds.inserted(1, 0, 0);
    ds.inserted(1, 1, "second");

    rowcount = ds.Insert();
    BOOST_CHECK(rowcount == 2);

    rowcount = ds.Execute("select * from test_status");
    BOOST_CHECK(rowcount == 2);

    rowcount = ds.Retrieve();
    BOOST_CHECK(ds.original(0, 1) == "first");
    BOOST_CHECK(ds.original(1, 1) == "second");

    ds.InsertRow(1);

    BOOST_CHECK(ds.inserted(1, 1) == "second");

    ds.DiscardChanges();

    ds.InsertRows(2, {0, 1});

    BOOST_CHECK(ds.inserted(0, 1) == "first");
    BOOST_CHECK(ds.inserted(1, 1) == "second");

    ds.DiscardChanges();

    ds.InsertRows(0, ds.original.RowCount());

    BOOST_CHECK(ds.inserted(0, 1) == "first");
    BOOST_CHECK(ds.inserted(1, 1) == "second");
}

BOOST_AUTO_TEST_CASE(retrieve)
{
    n::data::DataStore ds(da->GetTransaction());
    ds.sqlSelect.Set("select * from test_status order by id");
    std::size_t rowcount = 0;

    rowcount = ds.Execute("delete from test_status");
    rowcount = ds.Execute("select * from test_status");
    BOOST_CHECK(rowcount == 0);

    rowcount = ds.Execute("select test_status_insert((0, 'hello 1'))");
    rowcount = ds.Execute("select test_status_insert((0, 'hello 2'))");
    rowcount = ds.Execute("select test_status_insert((0, 'hello 3'))");

    rowcount = ds.Retrieve();
    BOOST_CHECK(rowcount == 3);

    BOOST_CHECK(ds.original(0, 1) == "hello 1");
    BOOST_CHECK(ds.original(1, 1) == "hello 2");
    BOOST_CHECK(ds.original(2, 1) == "hello 3");
}

BOOST_AUTO_TEST_CASE(update_rows)
{
    n::data::DataStore ds(da->GetTransaction());
    ds.sqlSelect.Set("select * from test_status order by id");
    ds.sqlUpdate.Set("select test_status_update((?,?))");
    std::size_t rowcount = 0;
    n::data::Parameters p;

    rowcount = ds.Execute("delete from test_status");
    rowcount = ds.Execute("select * from test_status");
    BOOST_CHECK(rowcount == 0);

    rowcount = ds.Execute("select test_status_insert((0, 'hello 1'))");
    rowcount = ds.Execute("select test_status_insert((0, 'hello 2'))");
    rowcount = ds.Execute("select test_status_insert((0, 'hello 3'))");
    rowcount = ds.Execute("select test_status_insert((0, 'hello 4'))");

    rowcount = ds.Retrieve();
    BOOST_CHECK(rowcount == 4);

    ds.ModifyRow(1);
    ds.ModifyRow(3);
    BOOST_CHECK(ds.modified.RowCount() == 2);

    ds.modified(1, "name", "create");
    ds.modified(3, "name", "edit");

    rowcount = ds.Update();
    BOOST_CHECK(rowcount == 2);

    rowcount = ds.Retrieve();
    BOOST_CHECK(rowcount == 4);

    BOOST_CHECK(ds.original(1, 1) == "create");
    BOOST_CHECK(ds.original(3, 1) == "edit");
}

BOOST_AUTO_TEST_CASE(delete_rows)
{
    n::data::DataStore ds(da->GetTransaction());
    ds.sqlSelect.Set("select * from test_status order by id");
    ds.sqlDelete.Set("delete from test_status where id = ?");
#ifndef NDEBUG
    {
        ds.SetPrimaryKeys({0});
        auto pki = ds.GetPrimaryKeys();
        BOOST_CHECK(pki.size() == 1);
    }
#endif
    std::size_t rowcount = 0;
    n::data::Parameters p;

    rowcount = ds.Execute("delete from test_status");
    rowcount = ds.Execute("select * from test_status order by id");
    BOOST_CHECK(rowcount == 0);

    rowcount = ds.Execute("select test_status_insert((0, 'hello 1'))");
    rowcount = ds.Execute("select test_status_insert((0, 'hello 2'))");
    rowcount = ds.Execute("select test_status_insert((0, 'hello 3'))");
    rowcount = ds.Execute("select test_status_insert((0, 'hello 4'))");

    rowcount = ds.Retrieve();
    BOOST_CHECK(rowcount == 4);

    ds.DeleteRow(1);
    ds.DeleteRow(3);
    BOOST_CHECK(ds.deleted.RowCount() == 2);

#if 0
#ifndef NDEBUG
    ds.SetPrimaryKeys({1});
    auto pkcols = ds.GetPrimaryKeyColumns(ds.deleted.GetRow(1));
    auto pk1 = pkcols[0];
#endif
#endif

    ds.SetPrimaryKeys({0});
    rowcount = ds.Delete();
    BOOST_CHECK(rowcount == 2);

    rowcount = ds.Retrieve();
    BOOST_CHECK(rowcount == 2);
}

BOOST_AUTO_TEST_CASE(discard_changes)
{
    n::data::DataStore ds(da->GetTransaction());
    ds.sqlSelect.Set("select * from test_status order by id");
    ds.sqlInsert.Set("select test_status_insert((?, ?))");
    ds.sqlUpdate.Set("select test_status_update((?,?))");
    ds.sqlDelete.Set("delete from test_status where id = ?");
    ds.SetPrimaryKeys({0});
    std::size_t rowcount = 0;
    n::data::Parameters p;

    rowcount = ds.Execute("delete from test_status");
    rowcount = ds.Execute("select * from test_status");
    BOOST_CHECK(rowcount == 0);

    rowcount = ds.Execute("select test_status_insert((0, 'hello 1'))");
    rowcount = ds.Execute("select test_status_insert((0, 'hello 2'))");
    rowcount = ds.Execute("select test_status_insert((0, 'hello 3'))");
    rowcount = ds.Execute("select test_status_insert((0, 'hello 4'))");

    rowcount = ds.Retrieve();
    BOOST_CHECK(rowcount == 4);

    ds.InsertRow(1);
    ds.inserted(0, 0) = 0;
    ds.inserted(0, 1) = "last";
    BOOST_CHECK(ds.inserted.RowCount() == 1);

    ds.ModifyRow(1);
    ds.ModifyRow(3);
    BOOST_CHECK(ds.modified.RowCount() == 2);

    ds.modified(1, "name", "create");
    ds.modified(3, "name", "edit");

    ds.DeleteRow(2);
    BOOST_CHECK(ds.deleted.RowCount() == 1);

    ds.DiscardChanges();

    rowcount = ds.UpdateAll();
    BOOST_CHECK(rowcount == 0);

    rowcount = ds.Retrieve();
    BOOST_CHECK(rowcount == 4);
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
