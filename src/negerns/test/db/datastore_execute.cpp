#include <string>
#include <vector>
#include <map>

#include <negerns/data/dataaccess.h>
#include <negerns/data/datasource.h>
#include <negerns/data/datastore.h>

#include <negerns/negerns.h>
#include <negerns/poco.h>

#include <boost/test/unit_test.hpp>
#include <negerns/test/global.h>

using namespace boost::unit_test;

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=Suite_DataStore/execute

BOOST_AUTO_TEST_SUITE(Suite_DataStore)
BOOST_FIXTURE_TEST_SUITE(execute, negerns_data)

BOOST_AUTO_TEST_CASE(execute_simple)
{
    n::data::DataStore ds(da->GetTransaction());
    std::size_t rowcount = 0;

    rowcount = ds.Execute("delete from test_status");
    rowcount = ds.Execute("select * from test_status where id = 0");
    BOOST_CHECK(rowcount == 0);

    rowcount = ds.Execute("select test_status_insert((0, 'hello 1'))");
    rowcount = ds.Execute("select test_status_insert((0, 'hello 2'))");
    rowcount = ds.Execute("select test_status_insert((0, 'hello 3'))");

    rowcount = ds.Execute("select * from test_status");
    BOOST_CHECK(rowcount == 3);

    rowcount = ds.Execute("delete from test_status");
    BOOST_CHECK(rowcount == 3);
}

BOOST_AUTO_TEST_CASE(execute_with_parameters)
{
    n::data::DataStore ds(da->GetTransaction());
    std::size_t rowcount = 0;
    n::data::Parameters p = {0, ""};

    BOOST_CHECK(p.size() == 2);

    rowcount = ds.Execute("delete from test_status");
    rowcount = ds.Execute("select * from test_status");
    BOOST_CHECK(rowcount == 0);

    p[1] = "create";
    rowcount = ds.Execute("select test_status_insert((?, ?))", p);
    p[1] = "read";
    rowcount = ds.Execute("select test_status_insert((?, ?))", p);
    p[1] = "edit";
    rowcount = ds.Execute("select test_status_insert((?, ?))", p);
    p[1] = "delete";
    rowcount = ds.Execute("select test_status_insert((?, ?))", p);

    rowcount = ds.Execute("select * from test_status");
    BOOST_CHECK(rowcount == 4);
    p.clear();
    p.push_back("insert");
    p.push_back("create");
    rowcount = ds.Execute("update test_status set name = ? where name = ?", p);
    BOOST_CHECK(rowcount == 1);

    p.clear();
    p.push_back("insert");
    rowcount = ds.Execute("select * from test_status where name = ?", p);
    BOOST_CHECK(rowcount == 1);

    rowcount = ds.Execute("delete from test_status where name = ?", p);
    BOOST_CHECK(rowcount == 1);

    rowcount = ds.Execute("select * from test_status");
    BOOST_CHECK(rowcount == 3);
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
