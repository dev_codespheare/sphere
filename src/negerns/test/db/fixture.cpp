#include <negerns/test/db/fixture.h>
#include <negerns/db/connection/odbc/driver.h>
#include <negerns/db/connection/odbc/postgresql/options.h>

namespace negerns {
namespace test {
namespace db {

negerns::db::connection::Parameters Fixture::get_connection_parameters(bool correct)
{
    using namespace negerns::db::connection;

#if defined(_MSC_VER) && defined(_WIN32)
    // Must check for 64-bit defines first because Microsoft C++ compiler
    // defines _WIN32 macro even when compiling for 64-bit platform.
#ifdef _WIN64
    odbc::Driver driver(odbc::Driver::PostgreSQL_ANSI_64Bit);
#else
    odbc::Driver driver(odbc::Driver::PostgreSQL_ANSI_32Bit);
#endif //_ #ifdef _WIN64
#endif //_ #if defined(_MSC_VER) && defined(_WIN32)

    namespace pg = negerns::db::connection::odbc::postgresql;

    auto options = std::make_unique<pg::Options>();
    options->set(pg::Option::Driver, odbc::get_driver_string(driver));
    options->set(pg::Option::Server, "localhost");
    options->set(pg::Option::Port, odbc::get_default_port(driver));
    options->set(pg::Option::Database, correct ? "sphere" : "x");
    options->set(pg::Option::User, "sphere");
    options->set(pg::Option::Password, "sphere");

    options->set(pg::Option::ConnectionName, "boost-test");
    options->set(pg::Option::ReadOnly, "0");
    options->set(pg::Option::LFConversion, "1");
    options->set(pg::Option::Parse, "1");
    options->set(pg::Option::UseServerSidePrepare, "1");

    options->set(pg::Option::BoolsAsChar, "1");
    options->set(pg::Option::MaxVarcharSize, "255");
    options->set(pg::Option::UnknownsAsLongVarchar, "1");
    options->set(pg::Option::TextAsLongVarchar, "1");

    return Parameters(Source::ODBC, std::move(options));
}

std::string Fixture::get_create_table()
{
    return "drop table if exists internal.test cascade;"
        "create table internal.test ("
        "   id serial NOT NULL, "
        "   name character varying(10) NOT NULL, "
        "   null_name character varying(10), "
        "   birthday date NOT NULL, "
        "   null_birthday date, "
        "   default_time time without time zone DEFAULT '00:00:00', "
        "   time_in time without time zone NOT NULL DEFAULT '09:50:25.354', "
        "   current_ttime time without time zone DEFAULT 'now', "
        "   count integer NOT NULL, "
        "   amount money NOT NULL, "
        "   active boolean NOT NULL DEFAULT true, "
        "   status integer, "
        "   CONSTRAINT pk_test PRIMARY KEY (id)"
        ");";
}

std::string Fixture::get_create_table_x()
{
    //return "drop table if exists internal.xxx cascade;"
    return    "create table internal.xxx ("
        "   id serial NOT NULL, "
        "   name character varying(10) NOT NULL, "
        "   CONSTRAINT pk_xxx PRIMARY KEY (id)"
        ");";
}

std::string Fixture::get_drop_table()
{
    return "drop table if exists internal.test cascade;";
}

std::string Fixture::get_drop_table_x()
{
    return "drop table if exists internal.xxx cascade;";
}

std::string Fixture::get_insert_row()
{
    // select * from test_insert((0, 'rim'::varchar(10), null, '1972-10-10'::date, null, '10:30:00', 5, '5.87'::float8::numeric::money, default));
    return R"**(insert into internal.test values(default, 'ricky', null, '1971-10-17', null, '10:30', default, 1, 5.87, default, 10))**";
}

} //_ namespace db
} //_ namespace test
} //_ namespace negerns



negerns::db::connection::Parameters get_connection_parameters(bool correct)
{
    return negerns::test::db::Fixture::get_connection_parameters(correct);
}
