#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <string>
#include <negerns/db/row.h>

#include <negerns/test/global.h>
#include <negerns/test/db/fixture.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_row
// negerns_test --log_level=test_suite --run_test=suite_db_row

BOOST_AUTO_TEST_SUITE(suite_db_row)

BOOST_AUTO_TEST_CASE(test_set_column)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db;
    set_assert_setting();
    set_log_setting();

    Columns columns;
    column::Properties p;

    {
        p.name = "id";
        p.type = column::Type::UInt32;
        p.precision = 10;
        p.is_unsigned = true;
        columns.add(p);
    }

    {
        p.name = "name";
        p.type = column::Type::String;
        p.precision = 10;
        p.is_unsigned = true;
        columns.add(p);
    }

    {
        p.name = "birthday";
        p.type = column::Type::Date;
        p.precision = 10;
        p.is_unsigned = true;
        columns.add(p);
    }

    {
        p.name = "count";
        p.type = column::Type::UInt32;
        p.precision = 10;
        p.is_unsigned = true;
        columns.add(p);
    }

    {
        p.name = "status";
        p.type = column::Type::Boolean;
        p.precision = 1;
        p.is_unsigned = true;
        columns.add(p);
    }

    n::db::Row row;
    row.set_columns(columns);
    CHECK_EQ(row.get_column_count(), 5);
#if 0
    auto count = row.get_column_count();
    for (int i = 0; i < count; ++i) {
        row(i)
    }
#endif
    CHECK(row(0) == 0);
    CHECK(row(1) == "");
    CHECK(row(3) == 0);

    row.set(0, 999);
    row.set(1, "hello");
    row.set(3, 555);
    row.set(4, true);

    CHECK_EQ(row(0).as_llong(), 999);
    CHECK_EQ(row(1).as_string(), "hello");
    CHECK_EQ(row(3).as_llong(), 555);

    for (auto &r : row) {
        MESSAGE("Column: " << r.as_string());
    }
}

BOOST_AUTO_TEST_CASE(test_row_copy)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db;
    set_assert_setting();
    set_log_setting();

    Columns columns;
    column::Properties p;

    {
        p.name = "name";
        p.type = column::Type::String;
        p.precision = 10;
        p.is_unsigned = true;
        columns.add(p);
    }

    n::db::Row row;
    row.set_columns(columns);
    CHECK_EQ(row.get_column_count(), 1);

    row.set(0, "hello");
    CHECK_EQ(row(0).as_string(), "hello");

    {
        n::db::Row r = row;
        CHECK_EQ(r(0).as_string(), "hello");
    }
}

BOOST_AUTO_TEST_SUITE_END()
