#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/string.h>
#include <negerns/db/backend/odbc/environment.h>

#include <negerns/test/global.h>
#include <negerns/test/db/fixture.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_backend_odbc_environment

BOOST_AUTO_TEST_SUITE(suite_db_backend_odbc_environment)

BOOST_AUTO_TEST_CASE(test_is_valid)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

    CHECK(!environment::is_valid());
}

BOOST_AUTO_TEST_CASE(test_assert_get)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

    CHECK(environment::get() == nullptr);
}

BOOST_AUTO_TEST_CASE(test_initialization)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

    CHECK(!environment::is_valid());

    static_cast<void>(environment::init());
    MESSAGE("Environment initialized.");

    REQUIRE(environment::is_valid());

    MESSAGE("Shutting down the environment...");
    environment::shutdown();
    ASSERT(!environment::is_valid());
}

BOOST_AUTO_TEST_CASE(test_get_attributes)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

    auto env = environment::init();
    REQUIRE(env != nullptr);

    {
        MESSAGE("Environment attribute query functions without argument:");
        auto ver = environment::get_odbc_version();
        auto cp = environment::get_connection_pooling();
        auto cpm = environment::get_connection_pooling_match();
        auto nts = environment::get_nts();
        MESSAGE("ODBC Version:                " << get_odbc_version_string(ver));
        MESSAGE("Connection Pooling:          " << connection_pooling_name(cp));
        MESSAGE("Connection Pooling Matching: " << connection_pooling_match_name(cpm));
        MESSAGE("Null-Terminated String:      " << negerns::string::to_string(nts));
    }

    environment::shutdown();
}

BOOST_AUTO_TEST_SUITE_END()
