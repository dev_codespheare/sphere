#include <negerns/test/db/backend/odbc/sql_fixture.h>

namespace negerns {
namespace test {
namespace db {
namespace backend {
namespace odbc {

Sql::Sql(negerns::db::connection::Pool *p,
    std::size_t i,
    negerns::db::backend::IBackend *be) :
    negerns::db::backend::odbc::Sql(p, i, be)
{ }

Sql::~Sql() { }

std::string Sql::transform(const std::string &s)
{
    return negerns::db::backend::odbc::Sql::transform(s);
}

} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace test
} //_ namespace negerns