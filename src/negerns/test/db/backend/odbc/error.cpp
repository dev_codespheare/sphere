#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <string>
#include <negerns/db/backend/odbc/error.h>

#include <negerns/test/global.h>
#include <negerns/test/db/fixture.h>

#include <negerns/negerns.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_backend_odbc_error
// negerns_test --log_level=test_suite --run_test=suite_db_backend_odbc_error

BOOST_AUTO_TEST_SUITE(suite_db_backend_odbc_error)

BOOST_AUTO_TEST_CASE(test_is_error)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

    CHECK(is_error(-1));
    CHECK(!is_error(0));
    CHECK(!is_error(1));
}

BOOST_AUTO_TEST_CASE(test_is_success)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

    CHECK(!is_success(-1));
    CHECK(is_success(0));
    CHECK(is_success(1));
}

BOOST_AUTO_TEST_CASE(test_is_success_with_info)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

    CHECK(!is_success_with_info(-1));
    CHECK(!is_success_with_info(0));
    CHECK(is_success_with_info(1));
}

BOOST_AUTO_TEST_CASE(test_is_no_data)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

    CHECK(!is_no_data(-1));
    CHECK(!is_no_data(0));
    CHECK(!is_no_data(1));
    CHECK(is_no_data(SQL_NO_DATA));
}

BOOST_AUTO_TEST_CASE(test_log_sqlreturn)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

    LOG_SQLRETURN(0, "ODBC Func");
    LOG_SQLRETURN(-1, "ODBC Func");
    LOG_SQLRETURN(1, "ODBC Func");

    Connection connection;
    LOG_SQLRETURN(0, connection, "Connection (Default ctor)");
    LOG_SQLRETURN(-1, connection, "Connection (Default ctor)");
    LOG_SQLRETURN(1, connection, "Connection (Default ctor)");
}

//BOOST_AUTO_TEST_CASE(test_log_sqlreturn_impl)

BOOST_AUTO_TEST_SUITE_END()
