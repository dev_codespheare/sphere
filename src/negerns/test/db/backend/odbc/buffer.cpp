#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/db/backend/odbc/buffer.h>
#include <negerns/db/backend/odbc/environment.h>
#include <negerns/db/backend/odbc/backend.h>
#include <negerns/db/backend/odbc/sql.h>
#include <negerns/db/connection/pool.h>

#include <negerns/test/global.h>
#include <negerns/test/db/fixture.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_backend_odbc_sql_buffer
// negerns_test --log_level=test_suite --run_test=suite_db_backend_odbc_sql_buffer

BOOST_AUTO_TEST_SUITE(suite_db_backend_odbc_sql_buffer)

BOOST_AUTO_TEST_CASE(test_buffer_init)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

    {
        Buffer buffer;
        buffer.init(1);
        CHECK_EQ(buffer.size(), 0);
    }
    {
        Buffer buffer(1);
        CHECK_EQ(buffer.size(), 0);
    }
}

BOOST_AUTO_TEST_CASE(test_buffer_clear)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

    {
        Buffer buffer;
        buffer.init(1);
        CHECK_EQ(buffer.size(), 0);
        buffer.clear();
        CHECK_EQ(buffer.size(), 0);
    }
    {
        Buffer buffer(1);
        CHECK_EQ(buffer.size(), 0);
        buffer.clear();
        CHECK_EQ(buffer.size(), 0);
    }
}

BOOST_AUTO_TEST_CASE(test_buffer_add)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

    environment::init();
    auto parameters = get_connection_parameters(true);
    auto pool = std::make_unique<negerns::db::connection::Pool>(parameters);
    REQUIRE(pool != nullptr);

    Backend be;
    auto status = be.open(parameters);
    REQUIRE(status == negerns::db::Error::None);

    auto sql = std::make_unique<Sql>(pool.get(), 0, &be);

    Buffer buffer;
    buffer.init(4);
    buffer.set_statement_handle(sql->get_statement_handle());

    CHECK_EQ(buffer.size(), 0);

    buffer.add(SQL_C_CHAR, 4);
    buffer.add(SQL_C_SLONG, 0);
    buffer.add(SQL_C_ULONG, 0);
    buffer.add(SQL_C_CHAR, 0);

    CHECK_EQ(buffer.size(), 4);

    buffer.clear();
    CHECK_EQ(buffer.size(), 0);

    // Freeing ODBC Environment variable requires the correct steps of
    // freeing other ODBC handle variables.

    // Force destruction of Sql instance
    //sql.reset();
    // Force closing of Backend
    //be.close();
    //be.close();
    // Force destruction of connection Pool instance
    //pool.reset();

    environment::shutdown();
}

BOOST_AUTO_TEST_CASE(test_buffer_get)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

    environment::init();
    auto parameters = get_connection_parameters(true);
    auto pool = std::make_unique<negerns::db::connection::Pool>(parameters);
    REQUIRE(pool != nullptr);

    Backend be;
    auto status = be.open(parameters);
    REQUIRE(status == negerns::db::Error::None);

    auto sql = std::make_unique<Sql>(pool.get(), 0, &be);

    Buffer buffer;
    buffer.init(4);
    buffer.set_statement_handle(sql->get_statement_handle());

    CHECK_EQ(buffer.size(), 0);

    buffer.add(SQL_C_CHAR, 4);
    buffer.add(SQL_C_SLONG, 0);
    buffer.add(SQL_C_ULONG, 0);
    buffer.add(SQL_C_CHAR, 8);

    CHECK_EQ(buffer.size(), 4);

    //? How do we set the values?

    buffer.clear();
    CHECK_EQ(buffer.size(), 0);

    environment::shutdown();
}

BOOST_AUTO_TEST_SUITE_END()
