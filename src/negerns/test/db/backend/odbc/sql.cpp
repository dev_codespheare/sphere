#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/db/connection/pool.h>
#include <negerns/db/backend/odbc/backend.h>
#include <negerns/db/backend/odbc/sql.h>
#include <negerns/db/connection/odbc/postgresql/options.h>

#include <negerns/test/global.h>
#include <negerns/test/db/fixture.h>
#include <negerns/test/db/backend/odbc/sql_fixture.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_backend_odbc_sql
// negerns_test --log_level=test_suite --run_test=suite_db_backend_odbc_sql

BOOST_AUTO_TEST_SUITE(suite_db_backend_odbc_sql)

BOOST_AUTO_TEST_CASE(test_sql_init)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db;
    set_assert_setting();
    set_log_setting();

    backend::odbc::environment::init();
    auto parameters = get_connection_parameters(true);
    auto pool = std::make_unique<connection::Pool>(parameters);
    REQUIRE(pool != nullptr);

    backend::odbc::Backend be;
    auto status = be.open(parameters);
    REQUIRE(status == n::db::Error::None);

    negerns::test::db::backend::odbc::Sql sql(pool.get(), 0, &be);

    backend::odbc::environment::shutdown();
    CHECK(backend::odbc::environment::is_valid() == false);
}

BOOST_AUTO_TEST_CASE(sql_from_pool)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db;
    set_assert_setting();
    set_log_setting();

    auto parameters = get_connection_parameters(true);
    auto pool = std::make_unique<connection::Pool>(parameters);
    REQUIRE(pool != nullptr);

    backend::odbc::Backend be;
    auto status = be.open(parameters);
    REQUIRE(status == n::db::Error::None);

    negerns::test::db::backend::odbc::Sql sql(pool.get(), 0, &be);
}

BOOST_AUTO_TEST_CASE(test_sql_transform)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db;
    set_assert_setting();
    set_log_setting();

    backend::odbc::environment::init();
    auto parameters = get_connection_parameters(true);
    auto pool = std::make_unique<connection::Pool>(parameters);
    REQUIRE(pool != nullptr);

    backend::odbc::Backend be;
    auto status = be.open(parameters);
    REQUIRE(status == n::db::Error::None);

    negerns::test::db::backend::odbc::Sql sql(pool.get(), 0, &be);
    {
        std::string str("select id from internal.test");
        CHECK(sql.transform(str) == str);
    }
    {
        std::string str("select * from internal.test where id = :id");
        std::string res("select * from internal.test where id = ?");
        std::string str2(sql.transform(str));
        CHECK(str2 != str);
        CHECK(str2 == res);
    }

    backend::odbc::environment::shutdown();
    CHECK(backend::odbc::environment::is_valid() == false);
}

BOOST_AUTO_TEST_CASE(test_sql_describe)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db;
    set_assert_setting();
    set_log_setting();

    std::string sqlSelect("select \
        id, \
        name, \
        null_name, \
        birthday, \
        null_birthday, \
        default_time, \
        time_in, \
        current_ttime, \
        count, \
        amount, \
        active, \
        status \
        from internal.test");

    backend::odbc::environment::init();
    auto parameters = get_connection_parameters(true);
    auto pool = std::make_unique<connection::Pool>(parameters);
    REQUIRE(pool != nullptr);

    backend::odbc::Backend be;
    auto status = be.open(parameters);
    REQUIRE(status == n::db::Error::None);

    negerns::test::db::backend::odbc::Sql sql(pool.get(), 0, &be);
    {
        REQUIRE(sql.prepare(negerns::test::db::Fixture::get_create_table(),
            backend::odbc::NoDescribe));
        sql.execute(0);
    }
    {
        bool status = sql.prepare(sqlSelect, backend::odbc::NoDescribe);
        REQUIRE(status);

        auto exec_rc = sql.execute(0);
        REQUIRE(exec_rc == n::db::ExecuteStatus::NoData);
        REQUIRE(sql.get_fetched_rows() == 0);
        if (exec_rc != n::db::ExecuteStatus::Error) {
            auto columns = sql.get_columns();

            CHECK_EQ(columns.count(), 12);
            for (auto &column : columns) {
                MESSAGE("Name:        " << column.name);
                MESSAGE("  SQL Type:  " << column::sql_type_name(column.type));
                MESSAGE("  C++ Type:  " << column::type_name(column.type));
                MESSAGE("  Scale:     " << column.scale);
                MESSAGE("  sizeof:    " << backend::odbc::get_data_size(column.type));
                MESSAGE("  Precision: " << column.precision);
                MESSAGE("  Nullable:  " << n::string::to_string(column.nullable));
                MESSAGE("  Identity:  " << n::string::to_string(column.identity));
                MESSAGE("  Unsigned:  " << n::string::to_string(column.is_unsigned));
                MESSAGE("  Position:  " << n::string::to_string(column.position));
            }
        }
    }

    {
        REQUIRE(sql.prepare(negerns::test::db::Fixture::get_drop_table(),
            backend::odbc::NoDescribe));
        sql.execute(0);
    }
    backend::odbc::environment::shutdown();
    CHECK(backend::odbc::environment::is_valid() == false);
}

BOOST_AUTO_TEST_CASE(test_sql_execute)
{
    //LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db;
    set_assert_setting();
    set_log_setting();

    backend::odbc::environment::init();
    auto parameters = get_connection_parameters(true);
    auto pool = std::make_unique<connection::Pool>(parameters);
    REQUIRE(pool != nullptr);

    backend::odbc::Backend be;
    auto status = be.open(parameters);
    REQUIRE(status == n::db::Error::None);

    negerns::test::db::backend::odbc::Sql sql(pool.get(), 0, &be);
    {
        REQUIRE(sql.prepare(negerns::test::db::Fixture::get_drop_table_x(),
            backend::odbc::NoDescribe));
        auto exec_rc = sql.execute(0);
        REQUIRE(exec_rc == n::db::ExecuteStatus::Success);

        REQUIRE(sql.prepare(negerns::test::db::Fixture::get_create_table_x(),
            backend::odbc::NoDescribe));
        exec_rc = sql.execute(0);
        REQUIRE(exec_rc == n::db::ExecuteStatus::Success);
    }

    {
        std::string sqlInsert(R"**(insert into internal.xxx values(default, 'ricky'))**");

        REQUIRE(sql.prepare(sqlInsert, backend::odbc::NoDescribe));
        auto exec_rc = sql.execute(0);
        REQUIRE(exec_rc == n::db::ExecuteStatus::Success);
        REQUIRE(sql.get_affected_rows() == 1);
    }
    {
        std::string sqlInsert(R"**(insert into internal.xxx values(default, 'ric'))**");

        REQUIRE(sql.prepare(sqlInsert, backend::odbc::NoDescribe));
        auto exec_rc = sql.execute(0);
        REQUIRE(exec_rc == n::db::ExecuteStatus::Success);
        REQUIRE(sql.get_affected_rows() == 1);
    }
    {
        std::string sqlInsert(R"**(insert into internal.xxx values(default, 'rim'))**");

        REQUIRE(sql.prepare(sqlInsert, backend::odbc::NoDescribe));
        auto exec_rc = sql.execute(0);
        REQUIRE(exec_rc == n::db::ExecuteStatus::Success);
        REQUIRE(sql.get_affected_rows() == 1);
    }
    {
        std::string sqlInsert(R"**(insert into internal.xxx values(default, 'ricardo'))**");

        REQUIRE(sql.prepare(sqlInsert, backend::odbc::NoDescribe));
        auto exec_rc = sql.execute(0);
        REQUIRE(exec_rc == n::db::ExecuteStatus::Success);
        REQUIRE(sql.get_affected_rows() == 1);
    }

    {
        std::string sqlSelect("select \
            id, \
            name \
            from internal.xxx");

        n::log::enable(true);
        n::log::tracing::enable(true);

        REQUIRE(sql.prepare(sqlSelect));

        auto exec_rc = sql.execute(0);
        REQUIRE(exec_rc == n::db::ExecuteStatus::Success);

        n::log::enable(false);
        n::log::tracing::enable(false);

        REQUIRE(sql.get_fetched_rows() == 1);
#if 0
        if (exec_rc != n::db::ExecuteStatus::Error) {
            auto columns = sql.get_columns();

            CHECK_EQ(columns.count(), 2);
            for (auto &column : columns) {
                MESSAGE("Name:        " << column.name);
                MESSAGE("  SQL Type:  " << column::sql_type_name(column.type));
                MESSAGE("  C++ Type:  " << column::type_name(column.type));
                MESSAGE("  Scale:     " << column.scale);
                MESSAGE("  sizeof:    " << backend::odbc::get_data_size(column.type));
                MESSAGE("  Precision: " << column.precision);
                MESSAGE("  Nullable:  " << n::string::to_string(column.nullable));
                MESSAGE("  Identity:  " << n::string::to_string(column.identity));
                MESSAGE("  Unsigned:  " << n::string::to_string(column.is_unsigned));
                MESSAGE("  Position:  " << n::string::to_string(column.position));
            }
        }
#endif

        auto v = sql.get_row_values();

        auto columns = sql.get_columns();
        for (std::size_t i = 0; i < columns.count(); ++i) {
            MESSAGE("Column: " << columns.get_name(i));
            MESSAGE("Value:  " << v[i].as_string());
        }
    }
#if 0
    {
        REQUIRE(sql.prepare(negerns::test::db::Fixture::get_drop_table_x(),
            backend::odbc::NoDescribe));
        auto exec_rc = sql.execute(0);
        REQUIRE(exec_rc == n::db::ExecuteStatus::Success);
    }
#endif

    // Freeing ODBC Environment variable requires the correct steps of
    // freeing other ODBC handle variables.

    // Force destruction of Sql instance
    //sql.reset();
    // Force closing of Backend
    //be.close();
    //be.close();
    // Force destruction of connection Pool instance
    //pool.reset();

    backend::odbc::environment::shutdown();
    CHECK_EQ(backend::odbc::environment::is_valid(), false);
    n::log::tracing::enable(false);
}

BOOST_AUTO_TEST_SUITE_END()
