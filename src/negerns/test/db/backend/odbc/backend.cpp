#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <string>
#include <negerns/db/backend/odbc/handles.h>
#include <negerns/db/backend/odbc/backend.h>
#include <negerns/db/connection/odbc/postgresql/options.h>
#include <negerns/test/global.h>
#include <negerns/test/db/fixture.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_backend_odbc_backend
// negerns_test --log_level=test_suite --run_test=suite_db_backend_odbc_backend

BOOST_AUTO_TEST_SUITE(suite_db_backend_odbc_backend)

BOOST_AUTO_TEST_CASE(test_open_failed)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db;
    set_assert_setting(n::assert::Status::Disable);
    set_log_setting();

    backend::odbc::environment::init();
    backend::odbc::Backend be;

    auto parameters = get_connection_parameters(false);
    auto status = be.open(parameters);
    CHECK(status != n::db::Error::None);
    be.close();

    backend::odbc::environment::shutdown();
    CHECK(!backend::odbc::environment::is_valid());
}

BOOST_AUTO_TEST_CASE(test_open)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db;
    set_assert_setting(n::assert::Status::Disable);
    set_log_setting();

    backend::odbc::environment::init();
    backend::odbc::Backend be;

    auto parameters = get_connection_parameters();
    auto status = be.open(parameters);
    CHECK(status == n::db::Error::None);
    be.close();

    backend::odbc::environment::shutdown();
    CHECK(!backend::odbc::environment::is_valid());
}

BOOST_AUTO_TEST_CASE(test_open_twice)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db;
    set_assert_setting(n::assert::Status::Disable);
    set_log_setting();

    backend::odbc::environment::init();
    backend::odbc::Backend be;

    negerns::db::Error status;

    auto parameters = get_connection_parameters();
    status = be.open(parameters);
    CHECK(status == n::db::Error::None);

    backend::odbc::Backend be2;
    status = be2.open(parameters);
    CHECK(status == n::db::Error::None);

    be.close();
    be2.close();

    backend::odbc::environment::shutdown();
    CHECK(!backend::odbc::environment::is_valid());
}

BOOST_AUTO_TEST_CASE(test_info)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db;
    set_assert_setting();
    set_log_setting();

    backend::odbc::environment::init();
    backend::odbc::Backend be;

    auto parameters = get_connection_parameters();
    auto status = be.open(parameters);
    CHECK(status == n::db::Error::None);
    MESSAGE("Product: " << connection::source_name(be.get_product()));
    MESSAGE("Database Name: " << be.get_database_name());
    MESSAGE("Database Version: " << be.get_database_version());

    CHECK(be.get_product() == connection::Source::PostgreSQL);

    namespace pg = negerns::db::connection::odbc::postgresql;
    std::string keystr(pg::Options::get_string(pg::Option::Database));
    CHECK(be.get_database_name() == parameters.get(keystr));

    be.close();

    backend::odbc::environment::shutdown();
    CHECK(!backend::odbc::environment::is_valid());
}

BOOST_AUTO_TEST_SUITE_END()
