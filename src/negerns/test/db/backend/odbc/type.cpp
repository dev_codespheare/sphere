#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/db/backend/odbc/helper.h>

#include <negerns/test/global.h>
#include <negerns/test/db/fixture.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_backend_odbc_type
// negerns_test --log_level=test_suite --run_test=suite_db_backend_odbc_type

BOOST_AUTO_TEST_SUITE(suite_db_backend_odbc_type)

BOOST_AUTO_TEST_CASE(test_to_column_type)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

}

BOOST_AUTO_TEST_CASE(test_to_sql_type)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::backend::odbc;
    set_assert_setting();
    set_log_setting();

}

BOOST_AUTO_TEST_SUITE_END()
