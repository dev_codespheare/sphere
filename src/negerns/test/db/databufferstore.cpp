#include <string>
#include <vector>
#include <map>
#include <negerns/core/variant.h>

#include <negerns/data/bufferstore.h>

#include <negerns/negerns.h>
#include <negerns/poco.h>

#include <boost/test/unit_test.hpp>

using namespace boost::unit_test;

BOOST_AUTO_TEST_SUITE(Suite_DataBufferStore)

BOOST_AUTO_TEST_CASE(all)
{
    using namespace negerns::data;
    BufferStore row;

#if 0
    //row.column[0].name;
    //row.column[""].name;
    //row.original.value[0];
    //row.original.value[""];
    //row.modified.value[0];
    //row.modified.value[""];
#endif
}

BOOST_AUTO_TEST_SUITE_END()
