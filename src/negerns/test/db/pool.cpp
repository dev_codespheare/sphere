#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <memory>
#include <string>
#include <thread>
#include <negerns/db/pool.h>
#include <negerns/db/backend/ibackend.h>

#include <negerns/test/global.h>
#include <negerns/test/db/fixture.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_pool
// negerns_test --log_level=test_suite --run_test=suite_db_pool

BOOST_AUTO_TEST_SUITE(suite_db_pool)

BOOST_AUTO_TEST_CASE_EXPECTED_FAILURES(test_interface_connection_failed, 1)
BOOST_AUTO_TEST_CASE(test_interface_connection_failed)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    // Clear the pool since other tests may have used the pool.
    n::db::Pool::close();

    auto parameters = get_connection_parameters(false);
    bool status = n::db::Pool::add(parameters, 1);
    CHECK_MSG(status, "Connection is not added to the pool.");

    CHECK(n::db::Pool::empty());
    CHECK_EQ(n::db::Pool::count(), 0);
    CHECK(n::db::Pool::get(0) == nullptr);

    n::db::Pool::close();
}

BOOST_AUTO_TEST_CASE(test_pool_get)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    auto parameters = get_connection_parameters();
    bool status = n::db::Pool::add(parameters, 1);
    CHECK_MSG(status, "Connection is not added to the pool.");
    REQUIRE(n::db::Pool::get() != nullptr);
    n::db::Pool::close();
}

BOOST_AUTO_TEST_CASE(test_connection_info)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    auto parameters = get_connection_parameters();
    bool status = n::db::Pool::add(parameters, 1);
    CHECK_MSG(status, "Connection is not added to the pool.");

    auto pool = n::db::Pool::get(0);
    REQUIRE(pool != nullptr);

    // Destroy the connection object before the pool.
    // Otherwise, the Pool::free() function assert will complain.

    {
        Pool::ItemRef iref = pool->get();
        auto be = iref.first;
        REQUIRE(be != nullptr);
        CHECK(be->get_source() == Source::ODBC);
        CHECK(be->get_product() == Source::PostgreSQL);
        CHECK(be->is_connected());
    }

    n::db::Pool::close();
    CHECK(n::db::Pool::empty());
    CHECK_EQ(n::db::Pool::count(), 0);
}

BOOST_AUTO_TEST_CASE(test_odbc_postgresql_pooling)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::connection;
    set_assert_setting();
    set_log_setting();

    auto parameters = get_connection_parameters();
    bool status = n::db::Pool::add(parameters, 2);
    CHECK_MSG(status, "Connection is not added to the pool.");

    auto pool = n::db::Pool::get(0);

    {
        Pool::ItemRef iref = pool->get();
        auto be = iref.first;
        REQUIRE(be != nullptr);
        CHECK(be->get_source() == Source::ODBC);
        CHECK(be->get_product() == Source::PostgreSQL);

        CHECK_EQ(pool->count(), 1);
        CHECK_EQ(pool->count_free(), 1);

        Pool::ItemRef iref2 = pool->get();
        auto be2 = iref2.first;
        REQUIRE(be2 != nullptr);
        CHECK(be2->get_source() == Source::ODBC);
        CHECK(be2->get_product() == Source::PostgreSQL);

        CHECK_EQ(pool->count(), 1);
        CHECK_EQ(pool->count_free(), 1);
    }

    CHECK_EQ(pool->count(), 1);
    CHECK_EQ(pool->count_free(), 1);

    n::db::Pool::close();
    CHECK(n::db::Pool::empty());
    CHECK_EQ(n::db::Pool::count(), 0);
    CHECK(n::db::Pool::get(0) == nullptr);
}

BOOST_AUTO_TEST_SUITE_END()
