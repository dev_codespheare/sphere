#include <string>
#include <vector>
#include <map>
#include <negerns/core/memory.h>
#include <negerns/data/rowiface/attachedrowsbase.h>
#include <negerns/negerns.h>
#include <negerns/poco.h>

#include <boost/test/unit_test.hpp>

using namespace boost::unit_test;

BOOST_AUTO_TEST_SUITE(Suite_AttachedRowBase)

BOOST_AUTO_TEST_CASE(test_attachedrowbase)
{
#if 0
    //auto rs = n::make_unique(new n::data::Rowset(nullptr));
    //AttachedRow row(rs.get());
    AttachedRow row;
    row.column[0].name;
    row.column[""].name;
    row.original[0].value[0];
    row.original[0].value[""];
    //row.modified.value[0];
    //row.modified.value[""];
#endif
}

BOOST_AUTO_TEST_SUITE_END()
