#include <negerns/db/column/properties.h>
#include <negerns/test/global.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_column_properties
// negerns_test --log_level=test_suite --run_test=suite_db_column_properties

BOOST_AUTO_TEST_SUITE(suite_db_column_properties)

BOOST_AUTO_TEST_CASE(test_mutability)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::column;
    set_assert_setting();
    set_log_setting();

    Properties p1;
    p1.name = "First";

    Properties p2;
    p2.name = "Second";

    p1 = p2;
}

BOOST_AUTO_TEST_CASE(test_type_name)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db::column;
    CHECK_EQ(type_name(Type::Char), "char");
}

BOOST_AUTO_TEST_SUITE_END()
