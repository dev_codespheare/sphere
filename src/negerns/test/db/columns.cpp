#include <negerns/db/columns.h>
#include <negerns/test/global.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_db_columns
// negerns_test --log_level=test_suite --run_test=suite_db_columns

BOOST_AUTO_TEST_SUITE(suite_db_columns)

BOOST_AUTO_TEST_CASE(test_add)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db;
    set_assert_setting();
    set_log_setting();

    Columns columns;
    column::Properties p;

    {
        p.name = "id";
        p.type = column::Type::UInt32;
        p.precision = 10;
        p.is_unsigned = true;
        columns.add(p);
    }

    {
        p.name = "name";
        p.type = column::Type::String;
        p.precision = 10;
        p.is_unsigned = true;
        columns.add(p);
    }

    {
        p.name = "birthday";
        p.type = column::Type::Date;
        p.precision = 10;
        p.is_unsigned = true;
        columns.add(p);
    }

    {
        p.name = "count";
        p.type = column::Type::UInt32;
        p.precision = 10;
        p.is_unsigned = true;
        columns.add(p);
    }

    {
        p.name = "status";
        p.type = column::Type::Boolean;
        p.precision = 1;
        p.is_unsigned = true;
        columns.add(p);
    }

    //. TODO: Add columns for all data types.

    CHECK_EQ(columns.count(), 5);
}

BOOST_AUTO_TEST_CASE_EXPECTED_FAILURES(test_get, 1)
BOOST_AUTO_TEST_CASE(test_get)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db;
    set_assert_setting();
    set_log_setting();

    column::Properties p;
    p.name = "First";

    Columns columns;
    columns.add(p);
    CHECK_EQ(columns.count(), 1);

    CHECK_EQ(columns(0).name, p.name);
    CHECK(!columns(1).is_valid());

    column::Properties p2;
    p2.name = "Second";
    columns.add(p2);
    CHECK_EQ(columns.count(), 2);

    CHECK_EQ(columns(0).name, p.name);
    CHECK_EQ(columns(1).name, p2.name);
}

BOOST_AUTO_TEST_CASE(test_move)
{
    LEAVE_TEST_IF_DEBUGGING();
    using namespace negerns::db;
    set_assert_setting();
    set_log_setting();

    Columns columns;
    column::Properties p;

    {
        p.name = "id";
        p.type = column::Type::UInt32;
        p.precision = 10;
        p.is_unsigned = true;
        columns.add(p);
    }
    CHECK_EQ(columns.count(), 1);

    Columns c(std::move(columns));
    CHECK_EQ(columns.count(), 0);
    CHECK_EQ(c.count(), 1);

}

BOOST_AUTO_TEST_SUITE_END()
