#include <memory>

#include <soci.h>
#include <connection-parameters.h>
#include <backends/odbc/soci-odbc.h>

#include <boost/test/unit_test.hpp>

#include <negerns/core/string.h>
#include <negerns/negerns.h>

using namespace boost::unit_test;

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_soci_odbc

BOOST_AUTO_TEST_SUITE(suite_soci_odbc)

BOOST_AUTO_TEST_CASE(test_connect)
{
#if 0
    soci::connection_parameters parameters("odbc", "Driver={PostgreSQL ANSI (x64)};Database=sphere;UID=sphere;PWD=sphere");
    parameters.set_option(soci::odbc_option_driver_complete, "0" /* SQL_DRIVER_NOPROMPT */);
    soci::session sql(parameters);

    soci::rowset<soci::row> rs = (sql.prepare << "select id, name from internal.test");

    for (soci::rowset<soci::row>::const_iterator it = rs.begin(); it != rs.end(); ++it)
    {
        soci::row const& row = *it;

        // dynamic data extraction from each row:
        BOOST_MESSAGE("ID: " << row.get<int>(0));
        BOOST_MESSAGE("Name: " << row.get<std::string>(1));
    }
#endif
}

BOOST_AUTO_TEST_SUITE_END()
