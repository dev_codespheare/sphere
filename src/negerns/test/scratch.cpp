#ifndef _SCL_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <string>

#include <negerns/test/global.h>
#include <negerns/test/db/fixture.h>

#include <negerns/negerns.h>

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_x_scratch
// negerns_test --log_level=test_suite --run_test=suite_scratch

BOOST_AUTO_TEST_SUITE(suite_x_scratch)

BOOST_AUTO_TEST_CASE(test_is_error)
{
    LEAVE_TEST_IF_DEBUGGING();
    set_assert_setting();
    set_log_setting();
}

BOOST_AUTO_TEST_SUITE_END()
