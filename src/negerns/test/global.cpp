#include <negerns/test/global.h>

void set_assert_setting(n::assert::Status status, n::assert::Output output)
{
    n::assert::status(status);
    n::assert::output(output);
    std::cout << "          Assertion settings:\n";
    std::cout << "            - Enabled: " << n::string::to_string(n::assert::is_enabled()) << "\n";
    std::cout << "            - Console: " << n::string::to_string(!n::assert::is_gui()) << "\n";
}

void set_log_setting(n::log::Status status, n::log::tracing::Status trstatus)
{
    auto rt_log_level = runtime_config::log_level();
    if (rt_log_level == invalid_log_level || rt_log_level == log_nothing) {
        n::log::enable(false);
        n::log::tracing::enable(false);
    } else {
        n::log::status(status);
        n::log::tracing::status(trstatus);
    }
    n::log::stdoutput::enable(true);
    n::log::stdoutput::only(true);
    std::cout << "          Log settings:\n";
    std::cout << "            - Enabled: " << n::string::to_string(n::log::is_enabled()) << "\n";
    std::cout << "            - Trace:   " << n::string::to_string(n::log::tracing::is_enabled()) << "\n";
    std::cout << "            - Console: " << n::string::to_string(n::log::stdoutput::is_enabled()) << "\n";
    std::cout << "          ------------------\n";
}
