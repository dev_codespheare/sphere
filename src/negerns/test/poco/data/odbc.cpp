#include <memory>

#include <Poco/Data/Session.h>
#include <Poco/Data/ODBC/Connector.h>
#include <Poco/Data/Statement.h>
#include <Poco/Data/RecordSet.h>

#include <boost/test/unit_test.hpp>

#include <negerns/core/string.h>
#include <negerns/negerns.h>
#include <negerns/poco.h>

using namespace boost::unit_test;

// negerns_test --build_info=yes --detect_memory_leaks=0 --log_level=test_suite --run_test=suite_poco_data_odbc

BOOST_AUTO_TEST_SUITE(suite_poco_data_odbc)

std::unique_ptr<poco::data::Session> connect() {
#ifdef __WINDOWS__
#   ifdef _MSC_VER
    // Must check for 64-bit defines first because wxWidgets
    // also defines 32-bit symbols even when building 64-bit
#       if defined(__WIN64__) || defined(_WIN64)
    //std::string dsn("sphere-client-64U");
    std::string dsn("sphere-client-64");
#       else
    std::string dsn("sphere-client-32U");
#       endif
#   endif
#endif
    auto connector = poco::data::odbc::Connector::KEY;
    auto cs(n::string::Format("DSN=%1;UID=sphere;PWD=sphere", dsn));
    return std::make_unique<poco::data::Session>(connector, cs);
}

BOOST_AUTO_TEST_CASE(test_connection)
{
    auto session = connect();
    BOOST_CHECK(session->isConnected() == true);
}

BOOST_AUTO_TEST_CASE(test_sql_select)
{
    auto session = connect();
    BOOST_CHECK(session->isConnected() == true);
    poco::data::Statement select(*(session.get()));
    //select << "select id, firstname, sex, birthday from internal.person", poco::data::keywords::now;
    select << "select amount from internal.test", poco::data::keywords::now;
    poco::data::RecordSet rs(select);
    std::size_t cols = rs.columnCount();
    BOOST_MESSAGE("Columns: " << cols);

    // print all column names
    for (std::size_t col = 0; col < cols; ++col)
        BOOST_MESSAGE(rs.columnName(col));

    // iterate over all rows and columns
    for (poco::data::RecordSet::Iterator it = rs.begin(); it != rs.end(); ++it)
        BOOST_MESSAGE(*it);
}

BOOST_AUTO_TEST_SUITE_END()
