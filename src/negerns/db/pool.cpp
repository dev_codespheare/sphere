#include <negerns/core/debug.h>
#include <negerns/db/pool.h>

namespace negerns {
namespace db {

std::vector<connection::PoolPtr> Pool::pool;
std::size_t Pool::defaultIndex = 0;

bool Pool::add(const connection::Parameters &params, std::size_t count)
{
    bool status = false;
    if (connection::Pool::test(params)) {
        pool.emplace_back(std::make_shared<connection::Pool>(params, count));
        ASSERT(pool.size() > 0, "Pool is empty.");
        status = true;
    }
    return status;
}

connection::PoolPtr Pool::get(std::size_t n)
{
    ASSERT(pool.size() > 0, "Pool is empty.");
    ASSERT(n < pool.size(), "Index out of range. Index: %1 Size: %2", n, pool.size());
    if (pool.size() == 0) {
        return nullptr;
    } else {
        return pool.at(n);
    }
}

connection::PoolPtr Pool::get()
{
    return Pool::get(defaultIndex);
}

void Pool::close()
{
    for (auto p : pool) {
        p->close();
    }
    pool.clear();
}

bool Pool::empty()
{
    return pool.size() == 0;
}

std::size_t Pool::count()
{
    return pool.size();
}

void Pool::set_default_index(std::size_t n)
{
    defaultIndex = n;
}

} //_ namespace db
} //_ namespace negerns
