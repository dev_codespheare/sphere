#include <memory>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/db/error.h>
#include <negerns/db/connection.h>
#include <negerns/db/connection/pool.h>
#include <negerns/db/backend/odbc/handles.h>
#include <negerns/db/backend/odbc/backend.h>
#include <negerns/db/backend/postgresql/backend.h>
#include <negerns/db/backend/sqlite/backend.h>
#include <negerns/db/backend/ibackend.h>

#include <negerns/negerns.h>

namespace negerns {
namespace db {
namespace connection {

class Pool::Impl final
{
public:
    Impl(const Parameters &);
    ~Impl();

    void close();

    std::size_t count_free() const;

    //? Do we really need a copy constructor and a copy assignment?
    struct PoolItem {
        Source source;
        backend::IBackend *backend = { nullptr };
        bool free = { true };

        PoolItem(Source s, backend::IBackend *be) :
            source(s),
            backend(be)
        { }

        PoolItem(const PoolItem &o) : source(o.source), free(o.free) {
            if (source == Source::ODBC) {
                backend = new backend::odbc::Backend();
            } else if (source == Source::PostgreSQL) {
                backend = new backend::postgresql::Backend();
            } else if (source == Source::SQLite) {
                backend = new backend::sqlite::Backend();
            }
        }

        PoolItem(PoolItem &&o) :
            source(o.source),
            backend(o.backend),
            free(o.free)
        {
            o.source = Source::None;
            o.free = true;
        }

        PoolItem & operator = (const PoolItem &rhs) {
            if (this != &rhs) {
                source = rhs.source;
                if (source == Source::ODBC) {
                    backend = new backend::odbc::Backend();
                } else if (source == Source::PostgreSQL) {
                    backend = new backend::postgresql::Backend();
                } else if (source == Source::SQLite) {
                    backend = new backend::sqlite::Backend();
                }
                free = rhs.free;
            }
            return *this;
        }

        PoolItem & operator = (PoolItem &&rhs) {
            source = rhs.source;
            backend = rhs.backend;
            free = rhs.free;

            rhs.source = Source::None;
            rhs.free = true;
            return *this;
        }

    }; //_ struct PoolItem

private:
    void create_pool(std::size_t = 0);

private:
    // Permit access to Pool::get()
    friend Pool;

    Parameters parameters;
    pooling::Type poolingType = pooling::Type::None;
    std::vector<PoolItem> pool;

}; //_ class Pool::Impl



Pool::Impl::Impl(const Parameters &params) :
    parameters(params)
{ }

Pool::Impl::~Impl()
{
    close();
}

void Pool::Impl::create_pool(std::size_t count)
{
    LOG_FUNCTION();
    Source source = parameters.get_source();
    pool.reserve(count);
    if (source == Source::ODBC) {
        for (std::size_t i = 0; i < count; ++i) {
            pool.emplace_back(source, new backend::odbc::Backend());
        }
    } else if (source == Source::PostgreSQL) {
        for (std::size_t i = 0; i < count; ++i) {
            pool.emplace_back(source, new backend::postgresql::Backend());
        }
    } else if (source == Source::SQLite) {
        for (std::size_t i = 0; i < count; ++i) {
            pool.emplace_back(source, new backend::sqlite::Backend());
        }
    } else {
        std::string msg("Should not execute this code.");
        FAIL(msg);
        throw std::runtime_error(msg);
    }
}

void Pool::Impl::close()
{
    LOG_FUNCTION();
    if (pool.empty()) return;
    for (auto &item : pool) {
        item.backend->close();
        delete item.backend;
        item.backend = nullptr;
    }
    pool.clear();
}

std::size_t Pool::Impl::count_free() const
{
    if (poolingType == pooling::Type::None) {
        return 1;
    } else if (poolingType == pooling::Type::ODBCPooling) {
        // Always at least one is available
        return 1;
    } else {
        return std::count_if (
            pool.begin(),
            pool.end(),
            [](PoolItem item) { return item.free; }
        );
    }
}



// ----------------------------------------------------------------------------------------



bool Pool::test(const Parameters &params)
{
    // An ODBC environment instance will be created if there is none yet.

    LOG_FUNCTION();

    // Perform checks first

    // Initialize the backend

    Source source = params.get_source();
    if (source == Source::ODBC) {
        auto be = std::make_unique<backend::odbc::Backend>();
        return be->open(params) == n::db::Error::None;
    } else if (source == Source::PostgreSQL) {
        auto be = std::make_unique<backend::postgresql::Backend>();
        return be->open(params) == n::db::Error::None;
    } else if (source == Source::SQLite) {
        auto be = std::make_unique<backend::sqlite::Backend>();
        return be->open(params) == n::db::Error::None;
    } else {
        // TODO: Use macro?
        std::string msg("Should not execute this code.");
        FAIL(msg);
        throw std::runtime_error(msg);
    }
}

Pool::Pool(const Parameters &params, std::size_t count) :
    pimpl(std::make_unique<Pool::Impl>(params))
{
    if (!backend::odbc::environment::is_valid()) {
        static_cast<void>(backend::odbc::environment::init());
    }

    if (count > 1) {
        // Client wants connection pooling.
        // Check if connection pooling is supported. If it is then we just
        // maintain a single backend connection and let the environment handle
        // the connection pooling. Otherwise, use the library connection pooling.
        Source source = params.get_source();
        if (source == Source::ODBC) {
            // Microsoft ODBC Driver Manager implements connection pooling.
            // ODBC Connection pooling was introduced with version 3.0.
            // https://msdn.microsoft.com/en-us/library/ms810829.aspx
            auto cp = backend::odbc::environment::get_connection_pooling();
            if (cp != backend::odbc::environment::ConnectionPooling::Off) {
                pimpl->poolingType = pooling::Type::ODBCPooling;
                count = 1;
            }
        } else {
            pimpl->poolingType = pooling::Type::LibraryPooling;
        }
    } else if (count == 1) {
        pimpl->poolingType = pooling::Type::None;
    }

    pimpl->create_pool(count);
}

Pool::~Pool()
{
    close();
}

Pool::ItemRef Pool::get()
{
    const Pool::ItemRef defaultReturn = std::make_pair(nullptr, 0);
    std::size_t index = 0;
    if (pimpl->poolingType == pooling::Type::None
        || pimpl->poolingType == pooling::Type::ODBCPooling) {

        WARNING(!pimpl->pool.empty());
        if (pimpl->pool.empty()) return defaultReturn;

        auto be = pimpl->pool[0].backend;
        db::Error error = db::Error::None;
        if (!be->is_connected()) {
            error = be->open(pimpl->parameters);
        }
        if (error == n::db::Error::None) {
            return std::make_pair(be, 0);
        } else {
            FAIL(n::db::get_error_string(error));
        }
    } else if (pimpl->poolingType == pooling::Type::LibraryPooling) {
        for (auto &item : pimpl->pool) {
            if (!item.free) {
                index++;
                continue;
            }
            item.free = false;
            auto error = item.backend->open(pimpl->parameters);
            if (error == n::db::Error::None) {
                return std::make_pair(item.backend, index);
            } else {
                FAIL(n::db::get_error_string(error));
            }
            index++;
        }
    }
    return defaultReturn;
}

void Pool::free(std::size_t i)
{
    ASSERT(!pimpl->pool.empty(),
        "Connection pool is empty. The pool may have been accidentally "
        "cleared prior to the destruction of the Connection object. This "
        "function is only called from the Connection object destructor. "
        "This will not cause undefined behavior but the cleanup code must "
        "be called in the right order.");
    ASSERT(i < pimpl->pool.size());
    pimpl->pool[i].free = true;
}

#if 0
void Pool::recreate(std::size_t n)
{
    pimpl->close();
    pimpl->create_pool(n);
}
#endif

void Pool::close()
{
    pimpl->close();
}

bool Pool::empty() const
{
    return pimpl->pool.empty();
}

std::size_t Pool::count() const
{
    return pimpl->pool.size();
}

std::size_t Pool::count_free() const
{
    return pimpl->count_free();
}

pooling::Type Pool::get_pooling_type() const
{
    return pimpl->poolingType;
}

} //_ namespace connection
} //_ namespace db
} //_ namespace negerns
