#include <negerns/core/string.h>
#include <negerns/core/log.h>
#include <negerns/db/connection/odbc/postgresql/options.h>

#include <negerns/negerns.h>

namespace negerns {
namespace db {
namespace connection {
namespace odbc {
namespace postgresql {

std::map<Option, std::string> Options::names = {
    { Option::None,                     "" },

    { Option::Driver,                   "Driver" },
    { Option::Server,                   "Server" },
    { Option::Port,                     "Port" },
    { Option::Database,                 "Database" },
    { Option::User,                     "UID" },
    { Option::Password,                 "PWD" },

    { Option::ConnectionName,           "ConnectionName" },
    { Option::ConnectionTimeOut,        "Connection Timeout" },
    { Option::SSLMode,                  "SSLMode" },
    { Option::ReadOnly,                 "ReadOnly" },
    { Option::FakeOIDIndex,             "FakeOIDIndex" },
    { Option::ShowOIDColumn,            "ShowOIDColumn" },
    { Option::RowVersioning,            "RowVersioning" },
    { Option::ShowSystemTables,         "ShowSystemTables" },
    { Option::Fetch,                    "Fetch" },
    { Option::MaxVarcharSize,           "MaxVarcharSize" },
    { Option::MaxLongVarcharSize,       "MaxLongVarcharSize" },
    { Option::TextAsLongVarchar,        "TextAsLongVarchar" },
    { Option::UnknownsAsLongVarchar,    "UnknownsAsLongVarchar" },
    { Option::BoolsAsChar,              "BoolsAsChar" },
    { Option::ByteaAsLongVarBinary,     "ByteaAsLongVarBinary" },
    { Option::Debug,                    "Debug" },
    { Option::CommLog,                  "CommLog" },
    { Option::Optimizer,                "Optimizer" },
    { Option::UseDeclareFetch,          "UseDeclareFetch" },
    { Option::Parse,                    "Parse" },
    { Option::ExtraSysTablePrefixes,    "ExtraSysTablePrefixes" },
    { Option::LFConversion,             "LFConversion" },
    { Option::UpdatableCursors,         "UpdatableCursors" },
    { Option::UseServerSidePrepare,     "UseServerSidePrepare" },
    { Option::LowerCaseIdentifier,      "LowerCaseIdentifier" },
    { Option::GssAuthUseGSS,            "GssAuthUseGSS" },
    { Option::XaOpt,                    "XaOpt" },
#if 0
    { Option::Socket,                   "Socket" },
    { Option::UnknownSizes,             "UnknownSizes" },
    { Option::CancelAsFreeStmt,         "CancelAsFreeStmt" },
    { Option::BI,                       "BI" },
    { Option::KSQO,                     "KSQO" },
    { Option::Protocol,                 "Protocol" },
    { Option::DisallowPremature,        "DisallowPremature" },
    { Option::TrueIsMinus1,             "TrueIsMinus1" },
#endif
};



Option Options::get_option(const std::string &s)
{
    for (auto &opt : names) {
        if (opt.second == s) {
            return opt.first;
        }
    }
    return Option::None;
}

std::string Options::get_string(Option o)
{
    return names[o];
}

Options::Options(const std::map<std::string, std::string> &data) :
    db::connection::Options(data)
{ }

Options::~Options() { }

Options & Options::operator = (const Options &rhs)
{
    if (this != &rhs) {
        db::connection::Options::operator=(rhs);
    }
    return *this;
}

Options & Options::operator = (Options &&rhs)
{
    if (this != &rhs) {
        db::connection::Options::operator=(std::move(rhs));
    }
    return *this;
}

std::unique_ptr<db::connection::Options> Options::clone() const
{
    return std::make_unique<Options>(db::connection::Options::get());
}

std::string Options::get_connection_string()
{
    if (db::connection::Options::empty()) {
        return std::string();
    } else {
        std::string cs;
        auto options = db::connection::Options::get();
        for (auto &opt : options) {
            cs.append(opt.first);
            cs.append("=");
            cs.append(opt.second);
            cs.append(";");
        }
        return cs;
    }
}

void Options::log()
{
    log::debug("Options: %1", count());
    auto options = db::connection::Options::get();
    for (auto &opt : options) {
        auto option = Options::get_option(opt.first);
        std::string name(Options::get_string(option));
        log::debug("  %1: [%3]", name, opt.second);
    }
}

} //_ namespace postgresql
} //_ namespace odbc
} //_ namespace connection
} //_ namespace db
} //_ namespace negerns
