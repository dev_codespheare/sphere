#include <negerns/db/connection/odbc/driver.h>

namespace negerns {
namespace db {
namespace connection {
namespace odbc {

std::string get_driver_string(Driver driver)
{
    static std::map<Driver, std::string> names {
        { Driver::None,                         "" },
        { Driver::PostgreSQL_ANSI_32Bit,        "{PostgreSQL ANSI}" },
        { Driver::PostgreSQL_Unicode_32Bit,     "{PostgreSQL Unicode}" },
        { Driver::PostgreSQL_ANSI_64Bit,        "{PostgreSQL ANSI(x64)}" },
        { Driver::PostgreSQL_Unicode_64Bit,     "{PostgreSQL Unicode(x64)}" },
    };

    std::string s;
    if (driver != Driver::None) {
        //s.append("Driver=");
        s.append(names[driver]);
    }
    return s;
}

std::string get_default_port(Driver driver)
{
    std::string port;
    switch (driver) {
        case Driver::PostgreSQL_ANSI_32Bit:
        case Driver::PostgreSQL_ANSI_64Bit:
        case Driver::PostgreSQL_Unicode_32Bit:
        case Driver::PostgreSQL_Unicode_64Bit:
            port.assign("5432");
            break;
    }
    return port;
}

} //_ namespace odbc
} //_ namespace connection
} //_ namespace db
} //_ namespace negerns