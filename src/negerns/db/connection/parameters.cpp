#include <negerns/core/log.h>
#include <negerns/db/connection/parameters.h>

#include <negerns/negerns.h>

namespace negerns {
namespace db {
namespace connection {

Parameters::Parameters()
{
    LOG_FUNCTION();
}

Parameters::Parameters(const Parameters &o) :
    source(o.source),
    options(o.options->clone())
{
    LOG_FUNCTION();
}

Parameters::Parameters(Parameters &&o) :
    source(o.source),
    options(std::move(o.options))
{
    LOG_FUNCTION();
}

Parameters::Parameters(Source s, std::unique_ptr<Options> o) :
    source(s),
    options(std::move(o))
{
    LOG_FUNCTION();
}

Parameters::~Parameters()
{
    LOG_FUNCTION();
    clear();
}

} //_ namespace connection
} //_ namespace db
} //_ namespace negerns
