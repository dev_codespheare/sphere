// NOTE: Just to test compile time errors with ioptions.h

#include <negerns/db/connection/options.h>

namespace negerns {
namespace db {
namespace connection {

Options::Options() { }

Options::Options(const std::map<std::string, std::string> &data) :
    options(data)
{ }

Options::Options(const Options &o) :
    options(o.options)
{ }

Options::Options(Options &&o) :
    options(std::move(o.options))
{ }

Options::~Options()
{
    clear();
}

Options & Options::operator = (const Options &rhs)
{
    if (this != &rhs) {
        options = rhs.options;
    }
    return *this;
}

Options & Options::operator = (Options &&rhs)
{
    if (this != &rhs) {
        rhs.swap(*this);
        Options().swap(rhs);
    }
    return *this;
}

std::unique_ptr<Options> Options::clone() const
{
    throw std::logic_error("Function not implemented.");
}

std::string Options::get_connection_string()
{
    throw std::logic_error("Function not implemented.");
}

Options & Options::swap(Options &rhs)
{
    std::swap(options, rhs.options);
    return *this;
}

} //_ namespace connection
} //_ namespace db
} //_ namespace negerns
