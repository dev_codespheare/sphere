#include <map>
#include <negerns/core/string.h>
#include <negerns/db/connection/source.h>

namespace negerns {
namespace db {
namespace connection {

std::string source_name(Source s)
{
    static std::map<Source, std::string> names {
        { Source::None,                 "None" },
        { Source::ODBC,                 "ODBC" },
        { Source::Firebird,             "Firebird" },
        { Source::MicrosoftSQLServer,   "Microsoft SQL Server" },
        { Source::MySQL,                "MySQL" },
        { Source::MariaDB,              "MariaDB" },
        { Source::Oracle,               "Oracle" },
        { Source::PostgreSQL,           "PostgreSql" },
        { Source::SQLite,               "SQLite" },
        { Source::SybaseSQLAnwhere,     "Sybase SQL Anywhere" }
    };
    return names[s];
}

Source source_type(const std::string &s)
{
    static std::map<std::string, Source> types {
        { "none",                 Source::None               },
        { "odbc",                 Source::ODBC               },
        { "firebird",             Source::Firebird           },
        { "microsoft sql server", Source::MicrosoftSQLServer },
        { "mysql",                Source::MySQL              },
        { "mariadb",              Source::MariaDB            },
        { "oracle",               Source::Oracle             },
        { "postgresql",           Source::PostgreSQL         },
        { "sqlite",               Source::SQLite             },
        { "sybase sql anywhere",  Source::SybaseSQLAnwhere   }
    };
    if (s.empty()) {
        return Source::None;
    } else {
        std::string str(s);
        return types[string::to_lower(str)];
    }
}

bool is_odbc(Source s)
{
    return s == Source::ODBC;
}

} //_ namespace connection
} //_ namespace db
} //_ namespace negerns
