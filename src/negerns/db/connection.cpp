#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/db/backend/odbc/backend.h>
#include <negerns/db/backend/postgresql/backend.h>
#include <negerns/db/backend/sqlite/backend.h>
#include <negerns/db/pool.h>
#include <negerns/db/connection.h>
#include <negerns/db/connection/pool.h>

namespace negerns {
namespace db {

class Connection::Impl
{
public:
    Impl();
    Impl(connection::Pool *p, std::size_t i, backend::IBackend *be) :
        pool(p),
        poolIndex(i),
        backend(be)
    { }
    Impl(const Impl &);
    Impl(Impl &&);
    virtual ~Impl();

    Impl & operator = (const Impl &);
    Impl & operator = (Impl &&);

    db::Error reconnect();
    bool is_connected() const;

    std::size_t get_pool_index() const;

    //! Acquire the backend Source type.
    connection::Source get_source() const;

    //! Acquire connection parameters.
    connection::Parameters get_parameters() const;

    //! Acquire the backend Source type or the backend Source type that ODBC
    //! is connected to.
    //!
    //! If the Source is ODBC then this will return the actual database
    //! backend Source type. Otherwise, it returns what get_source returns.
    connection::Source get_product() const;

    //! Get a pointer to the backend instance.
    backend::IBackend * get_backend() const;

private:
    friend Connection;

    //! Pointer to the connection \c Pool instance.
    //! Used when setting a connection \c Pool element status flag.
    //!
    //! NOTE: Just a reference. Not destroyed here.
    connection::Pool *pool { nullptr };

    //! Index of the backend instance in the connection \c Pool.
    //!
    //! Used when setting a connection \c Pool element status flag.
    //!
    //! \see Pool::free()
    std::size_t poolIndex = 0;

    //! Pointer to a backend instance in the connection \c Pool.
    //!
    //! NOTE: Just a reference. Not destroyed here.
    backend::IBackend *backend { nullptr };

    //! SQL string.
    std::string sql;

}; //_ class Connection::Impl



Connection::Impl::Impl() { }

Connection::Impl::Impl(const Impl &o) :
    pool(o.pool),
    poolIndex(o.poolIndex),
    backend(o.backend),
    sql(o.sql)
{ }

Connection::Impl::Impl(Impl &&o) :
    pool(o.pool),
    poolIndex(o.poolIndex),
    backend(o.backend),
    sql(o.sql)
{ }

Connection::Impl::~Impl()
{
    LOG_FUNCTION();
    ASSERT(pool != nullptr);
    if (pool != nullptr) {
        pool->free(poolIndex);
    }
}

inline
Connection::Impl & Connection::Impl::operator = (const Impl &rhs)
{
    pool = rhs.pool;
    poolIndex = rhs.poolIndex;
    backend = rhs.backend;
    sql = rhs.sql;
    return *this;
}

inline
Connection::Impl & Connection::Impl::operator = (Impl &&rhs)
{
    if (this != &rhs) {
        pool = rhs.pool;
        poolIndex = rhs.poolIndex;
        backend = rhs.backend;
        sql = rhs.sql;
    }
    return *this;
}

inline
db::Error Connection::Impl::reconnect()
{
    ASSERT(backend, "Backend is null.\nUse open() to create the Backend.");
    return backend->reconnect();
}

inline
bool Connection::Impl::is_connected() const
{
    ASSERT(backend, "Backend is null.");
    return backend->is_connected();
}

inline
std::size_t Connection::Impl::get_pool_index() const
{
    return poolIndex;
}

inline
connection::Source Connection::Impl::get_source() const
{
    ASSERT(backend, "Backend is null.");
    return backend->get_source();
}

inline
connection::Parameters Connection::Impl::get_parameters() const
{
    ASSERT(backend, "Backend is null.");
    return backend->get_parameters();
}

inline
connection::Source Connection::Impl::get_product() const
{
    ASSERT(backend, "Backend is null.");
    connection::Source source = backend->get_source();
    if (source == connection::Source::ODBC) {
        return backend->get_product();
    } else {
        return source;
    }
}

inline
backend::IBackend * Connection::Impl::get_backend() const
{
    return backend;
}


// -----------------------------------------------------------------------------



Connection::Connection()
{
    auto pool = db::Pool::get();
    connection::Pool::ItemRef iref = pool->get();
    auto be = iref.first;
    auto index = iref.second;
    if (be == nullptr && index == 0) {
        //. TODO: All Connections in the pool are being used. See Pool class.
    } else {
        pimpl = std::make_unique<Connection::Impl>(pool.get(), index, be);
    }
}

// When Using the Pimpl Idiom, define special member functions in the
// implementation file
// Chapter 4, Item 22
// Effectie Modern C++, 2014, Scott Meyers

Connection::Connection(const Connection &rhs) :
    pimpl(std::make_unique<Connection::Impl>(rhs.pimpl->pool,
        rhs.pimpl->poolIndex,
        rhs.pimpl->backend))
{
    LOG_FUNCTION();
}

Connection::Connection(Connection &&other)
{
    LOG_FUNCTION();
    *pimpl = *other.pimpl;
}

Connection::~Connection()
{
    LOG_FUNCTION();
}

Connection & Connection::operator = (const Connection &rhs)
{
    LOG_FUNCTION();
    pimpl = std::make_unique<Connection::Impl>(rhs.pimpl->pool,
        rhs.pimpl->poolIndex,
        rhs.pimpl->backend);
    return *this;
}

Connection & Connection::operator = (Connection &&rhs)
{
    LOG_FUNCTION();
    if (this != &rhs) {
        pimpl = std::move(rhs.pimpl);
    }
    return *this;
}

db::Error Connection::reconnect()
{
    ASSERT(pimpl, "Impl is null.");
    return pimpl->reconnect();
}

bool Connection::is_connected() const
{
    ASSERT(pimpl, "Impl is null.");
    return pimpl->is_connected();
}

std::size_t Connection::get_pool_index() const
{
    ASSERT(pimpl, "Impl is null.");
    return pimpl->get_pool_index();
}

connection::Source Connection::get_source() const
{
    ASSERT(pimpl, "Impl is null.");
    return pimpl->get_source();
}

connection::Parameters Connection::get_parameters() const
{
    ASSERT(pimpl, "Impl is null.");
    return pimpl->get_parameters();
}

connection::Source Connection::get_product() const
{
    ASSERT(pimpl, "Impl is null.");
    return pimpl->get_product();
}

Connection & Connection::swap(Connection &rhs)
{
    std::swap(pimpl, rhs.pimpl);
    return *this;
}

} //_ namespace db
} //_ namespace negerns
