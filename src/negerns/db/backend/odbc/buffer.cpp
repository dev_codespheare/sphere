#include <negerns/db/backend/odbc/error.h>
#include <negerns/db/backend/odbc/buffer.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {

BufferData::~BufferData()
{
    if (type == BufferData::Type::String) {
        delete [] char_;
    } else if (type == BufferData::Type::Numeric) {
        delete numeric_;
    }
}



// -----------------------------------------------------------------------------



Buffer::Buffer() { }

Buffer::Buffer(size_t n)
{
    init(n);
}

Buffer::~Buffer()
{
    clear();
}

void Buffer::init(std::size_t n)
{
    LOG_FUNCTION();
    clear();
    data.reserve(n);
    ptrs.reserve(n);
}

void Buffer::clear()
{
    LOG_FUNCTION();
    std::size_t count = data.size();
    if (count == 0) return;

    for (std::size_t i = 0; i < count; ++i) {
        ptrs[i] = nullptr;
    }

    data.clear();
    ptrs.clear();
}

SQLRETURN Buffer::add(SQLSMALLINT type, SQLLEN len)
{
    LOG_FUNCTION();
    auto i = data.size();
    switch (type) {
        case SQL_C_TYPE_DATE:
            data.emplace_back(new char[len]);
            ptrs.emplace_back(&data[i].char_);
            break;
        case SQL_C_TYPE_TIME:
            data.emplace_back(new char[len]);
            ptrs.emplace_back(&data[i].char_);
            break;
        case SQL_C_SLONG:
            data.emplace_back(0);
            ptrs.emplace_back(&data[i].int_);
            break;
        case SQL_C_ULONG:
            data.emplace_back(0u);
            ptrs.emplace_back(&data[i].uint_);
            break;
        case SQL_C_NUMERIC:
            data.emplace_back(new SQL_NUMERIC_STRUCT);
            ptrs.emplace_back(&data[i].numeric_);
            break;
        case SQL_C_CHAR:
            data.emplace_back(new char[len]);
            ptrs.emplace_back(&data[i].char_[0]);
            break;
        default:
            FAIL("No switch case available for value %1", type);
            return SQL_ERROR;
    }

    // Number of the result set column to bind.
    // Columns are numbered in increasing column order starting at 0,
    // where column 0 is the bookmark column. If bookmarks are not used �
    // that is, the SQL_ATTR_USE_BOOKMARKS statement attribute is set to
    // SQL_UB_OFF � then column numbers start at 1.
    //
    // https://msdn.microsoft.com/en-us/library/ms711010%28v=vs.85%29.aspx
    SQLUSMALLINT column = i + 1;
    SQLLEN indicator = 0;
    SQLRETURN rc = SQLBindCol(stmt, column, type, ptrs[i], len, &indicator);
    WARNING(is_success(rc), "SQLBindCol: Failed.");

    data[i].length = indicator;
    if (is_success(rc)) {
        if (indicator == SQL_NO_TOTAL) {
        } else if (indicator == SQL_NULL_DATA) {
        } else {
        }
    }
    return rc;
}

} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns
