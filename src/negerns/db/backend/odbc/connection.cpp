#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/db/backend/odbc/error.h>
#include <negerns/db/backend/odbc/handles.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {

Connection::Connection(const Environment &e)
{
    // Allocate connection handle
    SQLHENV env = e.get();
    SQLHDBC dbc = nullptr;
    rc = SQLAllocHandle(SQL_HANDLE_DBC, env, &dbc);
    connection::log("SQLAllocHandle (SQL_HANDLE_DBC)", env, rc);
    if (!is_error(rc)) {
        Handle::set(dbc);
    }
}

Connection::~Connection()
{
}

} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns
