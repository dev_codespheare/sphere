#include <sqlext.h>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/db/types.h>
#include <negerns/db/backend/odbc/error.h>
#include <negerns/db/backend/odbc/helper.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {
namespace helper {

// SQL Data Types
//
// Each DBMS defines its own SQL types. Each ODBC driver exposes only
// those SQL data types that the associated DBMS defines. Information
// about how a driver maps DBMS SQL types to the ODBC-defined SQL type
// identifiers and how a driver maps DBMS SQL types to its own driver-
// specific SQL type identifiers is returned through a call to
// SQLGetTypeInfo. A driver also returns the SQL data types when
// describing the data types of columns and parameters through calls
// to SQLColAttribute, SQLColumns, SQLDescribeCol, SQLDescribeParam,
// SQLProcedureColumns, and SQLSpecialColumns.
//
// https://msdn.microsoft.com/en-us/library/ms710150%28v=vs.85%29.aspx
//
// SQL_CHAR
// Character string of fixed string length n.
//
// SQL_VARCHAR
// Variable-length character string with a maximum string length n.
//
// SQL_LONGVARCHAR
// Variable length character data. Maximum length is data source-dependent.[9]
//
// SQL_WCHAR
// Unicode character string of fixed string length n
//
// SQL_WVARCHAR
// Unicode variable-length character string with a maximum string length n
//
// SQL_WLONGVARCHAR
// Unicode variable-length character data. Maximum length is data source-
// dependent
//
// SQL_DECIMAL
// Signed, exact, numeric value with a precision of at least p and scale s.
// (The maximum precision is driver-defined.) (1 <= p <= 15; s <= p).[4]
//
// SQL_NUMERIC
// Signed, exact, numeric value with a precision p and scale s
// (1 <= p <= 15; s <= p).[4]
//
// SQL_SMALLINT
// Exact numeric value with precision 5 and scale 0
// (signed: -32,768 <= n <= 32,767, unsigned: 0 <= n <= 65,535)[3].
//
// SQL_INTEGER
// Exact numeric value with precision 10 and scale 0
// (signed: -2[31] <= n <= 2[31] - 1, unsigned: 0 <= n <= 2[32] - 1)[3].
//
// SQL_REAL
// Signed, approximate, numeric value with a binary precision 24
// (zero or absolute value 10[-38] to 10[38]).
//
// SQL_FLOAT
// Signed, approximate, numeric value with a binary precision of at least p.
// (The maximum precision is driver-defined.)[5]
//
// SQL_DOUBLE
// Signed, approximate, numeric value with a binary precision 53
// (zero or absolute value 10[-308] to 10[308]).
//
// SQL_BIT
// Single bit binary data.[8]
//
// SQL_TINYINT
// Exact numeric value with precision 3 and scale 0
// (signed: -128 <= n <= 127, unsigned: 0 <= n <= 255)[3].
//
// SQL_BIGINT
// Exact numeric value with precision 19 (if signed) or 20 (if unsigned) and
// scale 0 (signed: -2[63] <= n <= 2[63] - 1, unsigned: 0 <= n <= 2[64] - 1)
// [3],[9].
//
// SQL_BINARY
// Binary data of fixed length n.[9]
//
// SQL_VARBINARY
// Variable length binary data of maximum length n. The maximum is set by the
// user.[9]
//
// SQL_LONGVARBINARY
// Variable length binary data. Maximum length is data source-dependent.[9]
//
// SQL_TYPE_DATE[6]
// Year, month, and day fields, conforming to the rules of the Gregorian
// calendar. (See Constraints of the Gregorian Calendar)
//
// SQL_TYPE_TIME[6]
// Hour, minute, and second fields, with valid values for hours of 00 to 23,
// valid values for minutes of 00 to 59, and valid values for seconds of 00 to
// 61. Precision p indicates the seconds precision.
//
// SQL_TYPE_TIMESTAMP[6]
// Year, month, day, hour, minute, and second fields, with valid values as
// defined for the DATE and TIME data types.
//
// SQL_TYPE_UTCDATETIME
// Year, month, day, hour, minute, second, utchour, and utcminute fields. The
// utchour and utcminute fields have 1/10 microsecond precision.
//
// SQL_TYPE_UTCTIME
// Hour, minute, second, utchour, and utcminute fields. The utchour and
// utcminute fields have 1/10 microsecond precision..
//
// SQL_INTERVAL_MONTH[7]
// Number of months between two dates; p is the interval leading precision.
//
// SQL_INTERVAL_YEAR[7]
// Number of years between two dates; p is the interval leading precision.
//
// SQL_INTERVAL_YEAR_TO_MONTH[7]
// Number of years and months between two dates; p is the interval leading
// precision.
//
// SQL_INTERVAL_DAY[7]
// Number of days between two dates; p is the interval leading precision.
//
// SQL_INTERVAL_HOUR[7]
// Number of hours between two date/times; p is the interval leading precision.
//
// SQL_INTERVAL_MINUTE[7]
// Number of minutes between two date/times; p is the interval leading
// precision.
//
// SQL_INTERVAL_SECOND[7]
// Number of seconds between two date/times; p is the interval leading
// precision and q is the interval seconds precision.
//
// SQL_INTERVAL_DAY_TO_HOUR[7]
// Number of days/hours between two date/times; p is the interval leading
// precision.
//
// SQL_INTERVAL_DAY_TO_MINUTE[7]
// Number of days/hours/minutes between two date/times; p is the interval
// leading precision.
//
// SQL_INTERVAL_DAY_TO_SECOND[7]
// Number of days/hours/minutes/seconds between two date/times; p is the
// interval leading precision and q is the interval seconds precision.
//
// SQL_INTERVAL_HOUR_TO_MINUTE[7]
// Number of hours/minutes between two date/times; p is the interval leading
// precision.
//
// SQL_INTERVAL_HOUR_TO_SECOND[7]
// Number of hours/minutes/seconds between two date/times; p is the interval
// leading precision and q is the interval seconds precision.
//
// SQL_INTERVAL_MINUTE_TO_SECOND[7]
// Number of minutes/seconds between two date/times; p is the interval leading
// precision and q is the interval seconds precision.
//
// SQL_GUID
// Fixed length GUID.
//
// [1] This is the value returned in the DATA_TYPE column by a call to
//     SQLGetTypeInfo.
// [2] This is the value returned in the NAME and CREATE PARAMS column by a
//     call to SQLGetTypeInfo. The NAME column returns the designation-for
//     example, CHAR-whereas the CREATE PARAMS column returns a comma-separated
//     list of creation parameters such as precision, scale, and length.
// [3] An application uses SQLGetTypeInfo or SQLColAttribute to determine
//     whether a particular data type or a particular column in a result set is
//     unsigned.
// [4] SQL_DECIMAL and SQL_NUMERIC data types differ only in their precision.
//     The precision of a DECIMAL(p,s) is an implementation-defined decimal
//     precision that is no less than p, whereas the precision of a NUMERIC(p,s)
//     is exactly equal to p.
// [5] Depending on the implementation, the precision of SQL_FLOAT can be either
//     24 or 53: if it is 24, the SQL_FLOAT data type is the same as SQL_REAL;
//     if it is 53, the SQL_FLOAT data type is the same as SQL_DOUBLE.
// [6] In ODBC 3.x, the SQL date, time, and timestamp data types are
//     SQL_TYPE_DATE, SQL_TYPE_TIME, and SQL_TYPE_TIMESTAMP, respectively; in
//     ODBC 2.x, the data types are SQL_DATE, SQL_TIME, and SQL_TIMESTAMP.
// [7] For more information about the interval SQL data types, see the Interval
//     Data Types section, later in this appendix.
// [8] The SQL_BIT data type has different characteristics than the BIT type in
//     SQL-92.
// [9] This data type has no corresponding data type in SQL-92.

column::Type to_column_type(const SQLSMALLINT t, bool isUnsigned)
{
    column::Type type;
    switch (t) {

#if (ODBCVER >= 0x0300)
        case SQL_TYPE_DATE:
            type = column::Type::Date;
            break;
        case SQL_TYPE_TIME:
            type = column::Type::Time;
            break;
        case SQL_TYPE_TIMESTAMP:
            type = column::Type::Timestamp;
            break;
#endif
        case SQL_NUMERIC:
            type = column::Type::Numeric;
            break;
        case SQL_DECIMAL:
            type = column::Type::Decimal;
            break;

        case SQL_FLOAT:
            type = column::Type::Float;
            break;
        case SQL_REAL:
            type = column::Type::Real;
            break;
        case SQL_DOUBLE:
            type = column::Type::Double;
            break;

        case SQL_TINYINT:
            type = isUnsigned ? column::Type::UInt8 : column::Type::Int8;
            break;
        case SQL_SMALLINT:
            type = isUnsigned ? column::Type::UInt16 : column::Type::Int16;
            break;
        case SQL_INTEGER:
            type = isUnsigned ? column::Type::UInt32 : column::Type::Int32;
            break;
        case SQL_BIGINT:
            type = isUnsigned ? column::Type::UInt64 : column::Type::Int64;
            break;

        case SQL_CHAR:
            type = column::Type::Char;
            break;
        case SQL_WCHAR:
            type = column::Type::WChar;
            break;
        case SQL_VARCHAR:
            type = column::Type::String;
            break;
        case SQL_WVARCHAR:
            type = column::Type::WString;
            break;
        case SQL_LONGVARCHAR:
            type = column::Type::Clob;
            break;
        case SQL_WLONGVARCHAR:
            type = column::Type::WClob;
            break;
        default:
            type = column::Type::Unknown;
            break;
    }

    return type;
}

SQLSMALLINT to_sql_type(const column::Type t)
{
    SQLSMALLINT type = SQL_UNKNOWN_TYPE;
    switch (t) {

#if (ODBCVER >= 0x0300)
        case column::Type::Date:
            type = SQL_TYPE_DATE;
            break;
        case column::Type::Time:
            type = SQL_TYPE_TIME;
            break;
        case column::Type::Timestamp:
            type = SQL_TYPE_TIMESTAMP;
            break;
#endif
        case column::Type::Numeric:
            type = SQL_NUMERIC;
            break;
        case column::Type::Decimal:
            type = SQL_DECIMAL;
            break;

        case column::Type::Float:
            type = SQL_FLOAT;
            break;
        case column::Type::Real:
            type = SQL_REAL;
            break;
        case column::Type::Double:
            type = SQL_DOUBLE;
            break;

        case column::Type::Int8:
        case column::Type::UInt8:
            type = SQL_TINYINT;
            break;
        case column::Type::Int16:
        case column::Type::UInt16:
            type = SQL_SMALLINT;
            break;
        case column::Type::Int32:
        case column::Type::UInt32:
            type = SQL_INTEGER;
            break;
        case column::Type::Int64:
        case column::Type::UInt64:
            type = SQL_BIGINT;
            break;

        case column::Type::Char:
            type = SQL_CHAR;
            break;
        case column::Type::WChar:
            type = SQL_WCHAR;
            break;
        case column::Type::String:
            type = SQL_VARCHAR;
            break;
        case column::Type::WString:
            type = SQL_WVARCHAR;
            break;
        case column::Type::Clob:
            type = SQL_LONGVARCHAR;
            break;
        case column::Type::WClob:
            type = SQL_WLONGVARCHAR;
            break;
        default:
            type = SQL_UNKNOWN_TYPE;
            break;
    }

    return type;
}

// https://msdn.microsoft.com/en-us/library/ms714556%28v=vs.85%29.aspx
SQLSMALLINT to_c_type(const column::Type t)
{
    SQLSMALLINT type = SQL_UNKNOWN_TYPE;
    switch (t) {

#if (ODBCVER >= 0x0300)
        case column::Type::Date:
            type = SQL_C_TYPE_DATE;
            break;
        case column::Type::Time:
            type = SQL_C_TYPE_TIME;
            break;
        case column::Type::Timestamp:
            type = SQL_C_TYPE_TIMESTAMP;
            break;
#endif
        case column::Type::Numeric:
            type = SQL_C_NUMERIC;
            break;
        case column::Type::Decimal:
            type = SQL_C_NUMERIC;
            break;

        case column::Type::Float:
            type = SQL_C_DOUBLE;
            break;
        case column::Type::Real:
            type = SQL_C_FLOAT;
            break;
        case column::Type::Double:
            type = SQL_C_DOUBLE;
            break;

        case column::Type::Int8:
            type = SQL_C_STINYINT;
            break;
        case column::Type::UInt8:
            type = SQL_C_UTINYINT;
            break;
        case column::Type::Int16:
            type = SQL_C_SSHORT;
            break;
        case column::Type::UInt16:
            type = SQL_C_USHORT;
            break;
        case column::Type::Int32:
            type = SQL_C_SLONG;
            break;
        case column::Type::UInt32:
            type = SQL_C_ULONG;
            break;
        case column::Type::Int64:
            type = SQL_C_SBIGINT;
            break;
        case column::Type::UInt64:
            type = SQL_C_UBIGINT;
            break;

        case column::Type::Char:
            type = SQL_C_UTINYINT;
            break;
        case column::Type::WChar:
            type = SQL_C_WCHAR;
            break;
        case column::Type::String:
            type = SQL_C_CHAR;
            break;
        case column::Type::WString:
            type = SQL_C_WCHAR;
            break;
        case column::Type::Clob:
            type = SQL_C_BINARY;
            break;
        case column::Type::WClob:
            type = SQL_C_WCHAR;
            break;
        default:
            type = SQL_UNKNOWN_TYPE;
            break;
    }

    return type;
}



bool is_identity(SQLHSTMT stmt, const SQLUSMALLINT col)
{
    ASSERT(stmt != nullptr, "Statement is null.");

    SQLPOINTER charAttrPtr;
    SQLSMALLINT bufLen = 0;
    SQLSMALLINT stringLenPtr;
    SQLLEN numericAttrPtr;

    SQLRETURN rc = SQLColAttribute(stmt,
        col,
        SQL_DESC_AUTO_UNIQUE_VALUE,
        &charAttrPtr,
        bufLen,
        &stringLenPtr,
        &numericAttrPtr);
    if (is_error(rc)) {
        FAIL();
        return false;
    } else {
        return numericAttrPtr == SQL_TRUE;
    }
}

bool is_unsigned(SQLHSTMT stmt, const SQLUSMALLINT col)
{
    ASSERT(stmt != nullptr, "Statement is null.");

    SQLPOINTER charAttrPtr;
    SQLSMALLINT bufLen = 0;
    SQLSMALLINT stringLenPtr;
    SQLLEN numericAttrPtr;

    SQLRETURN rc = SQLColAttribute(stmt,
        col,
        SQL_DESC_UNSIGNED,
        &charAttrPtr,
        bufLen,
        &stringLenPtr,
        &numericAttrPtr);
    if (is_error(rc)) {
        FAIL();
        return false;
    } else {
        return numericAttrPtr == SQL_TRUE;
    }
}

column::Properties describe_column(SQLHSTMT stmt,
    const SQLUSMALLINT col,
    column::Properties p)
{
    ASSERT(stmt != nullptr, "Statement is null.");

    static const unsigned short NAME_BUFFER_MAX = 2048;

    SQLCHAR nameBuf[NAME_BUFFER_MAX];
    SQLSMALLINT nameBufOverflow;
    SQLSMALLINT dataType;
    SQLULEN colSize;
    SQLSMALLINT decimalDigits;
    SQLSMALLINT isNullable;

    SQLRETURN rc = SQLDescribeColA(stmt,
        col,
        nameBuf,
        NAME_BUFFER_MAX,
        &nameBufOverflow,
        &dataType,
        &colSize,
        &decimalDigits,
        &isNullable);
    if (is_error(rc)) {
        FAIL();
    } else {
        char const *s = reinterpret_cast<char const *>(nameBuf);
        p.name.assign(s, std::strlen(s));

        //* IMPORTANT: PostgreSQL Unicode returns -9 for varchar data type

        //. TODO: Fix this when using PostgreSQL Unicode

        p.type = to_column_type(dataType, p.is_unsigned);
        p.sqlType = dataType;
        p.precision = colSize;
        p.scale = decimalDigits;
        if (isNullable == SQL_NO_NULLS) {
            p.nullable = false;
        } else if (isNullable == SQL_NULLABLE) {
            p.nullable = true;
        } else {

            //. TODO: Determine what default to use or if we need a tribool type.
            //. For now, in PostgreSQL, we set it to not nullable.

            p.nullable = (isNullable == SQL_NO_NULLS);
        }
    }
    return p;
}

long strtohextoval(const SQL_NUMERIC_STRUCT &ns)
{
    // Code from: http://support.microsoft.com/kb/222831
    //
    // Code that implements the conversion from little endian mode to the
    // scaled integer.
    //
    // Please note that it is up to the application developer to implement this
    // functionality. The example here is just one of the many possible ways.

    long value = 0;
    int i = 1;
    int last = 1;

    for (i = 0; i <= 15; i++) {
        int current = (int) ns.val[i];
        int a = current % 16; //Obtain LSD
        int b = current / 16; //Obtain MSD

        value += last * a;
        last = last * 16;
        value += last * b;
        last = last * 16;
    }
    return value;
}

double to_double(const SQL_NUMERIC_STRUCT &ns)
{
    // Code from: http://support.microsoft.com/kb/222831

    // Call to convert the little endian mode data into numeric data.
    long val = helper::strtohextoval(ns);

    // The returned value in the above code is scaled to the value specified
    // in the scale field of the numeric structure. For example 25.212 would
    // be returned as 25212. The scale in this case is 3 hence the integer
    // value needs to be divided by 1000.

    long divisor = 1;
    if (ns.scale > 0) {
        for (int i = 0; i < ns.scale; i++)
            divisor = divisor * 10;
    }
    double final_val = (float) val / (float) divisor;

    // Examine the sign value returned in the sign field for the numeric
    // structure.

    // NOTE: The ODBC 3.0 spec required drivers to return the sign as
    // 1 for positive numbers and 2 for negative number. This was changed in the
    // ODBC 3.5 spec to return 0 for negative instead of 2.

#if (ODBCVER >= 0x0350)
    if (ns.sign == 0) {
#elif (ODBCVER > 0x0300)
    if (ns.sign > 1) {
#endif
        final_val *= -1;
    }
    return final_val;
}

} //_ namespace helper
} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns