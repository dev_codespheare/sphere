#include <map>
#include <negerns/db/column/properties.h>
#include <negerns/db/backend/odbc/error.h>
#include <negerns/db/backend/odbc/backend.h>
#include <negerns/db/backend/odbc/sqlimpl.h>
#include <negerns/db/backend/odbc/helper.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {

Sql::Implementation::Implementation(Sql *s, connection::Pool *p,
    std::size_t i, backend::IBackend *be) :
    Statement(),
    sql(s),
    pool(p),
    poolIndex(i),
    backend(static_cast<Backend *>(be))
{
    // Allocate statement handle.
    SQLHDBC dbc = backend->Statement::get();
    DEBUG(dbc != nullptr);
    SQLHSTMT stmt = SQL_NULL_HSTMT;
    SQLRETURN rc = SQLAllocHandle(SQL_HANDLE_STMT, dbc, &stmt);
    LOG_SQLRETURN(rc, *backend, "SQLAllocHandle (SQL_HANDLE_STMT)");
    if (is_error(rc)) {
        FAIL();
    } else {
        Statement::set(stmt);
        buffer.set_statement_handle(stmt);
    }
}

Sql::Implementation::~Implementation()
{
    LOG_FUNCTION();
    DEBUG(Statement::is_valid());
    SQLHSTMT stmt = Statement::get();
    SQLCloseCursor(stmt);

    SQLRETURN rc;
    rc = SQLFreeStmt(stmt, SQL_CLOSE);
    rc = SQLFreeStmt(stmt, SQL_UNBIND);
    rc = SQLFreeStmt(stmt, SQL_RESET_PARAMS);

    SQLFreeHandle(SQL_HANDLE_STMT, stmt);
    Statement::set(NULL);

    buffer.set_statement_handle(nullptr);
    buffer.clear();

    WARNING(pool != nullptr);
    if (pool != nullptr) {
        pool->free(poolIndex);
    }

    WARNING(backend != nullptr,
        "Backend pointer cannot be null. "
        "Connection Pool manages all backend instances.");
}

std::string Sql::Implementation::transform(const std::string &s)
{
    LOG_FUNCTION();
    enum class State {
        Normal,
        InQuotes,
        InName,
        InAccessDate
    } state = State::Normal;

    std::string name;
    std::string temp;
    temp.reserve(s.length());

    for (auto it = s.begin(), end = s.end(); it != end; ++it) {
        switch (state) {
            case State::Normal:
                if (*it == '\'') {
                    temp += *it;
                    state = State::InQuotes;
                } else if (*it == '#') {
                    temp += *it;
                    state = State::InAccessDate;
                } else if (*it == ':') {
                    state = State::InName;
                } else { // regular character, stay in the same state
                    temp += *it;
                }
                break;
            case State::InQuotes:
                if (*it == '\'') {
                    temp += *it;
                    state = State::Normal;
                } else { // regular quoted character
                    temp += *it;
                }
                break;
            case State::InName:
                if (std::isalnum(*it) || *it == '_') {
                    name += *it;
                } else { // end of name
                    names.push_back(name);
                    name.clear();
                    temp += "?";
                    temp += *it;
                    state = State::Normal;
                }
                break;
            case State::InAccessDate:
                if (*it == '#') {
                    temp += *it;
                    state = State::Normal;
                } else { // regular quoted character
                    temp += *it;
                }
                break;
        }
    }

    if (state == State::InName) {
        names.push_back(name);
        temp += "?";
    }
    return temp;
}

bool Sql::Implementation::prepare(const std::string &s, bool flag)
{
    LOG_FUNCTION();
    auto str(transform(s));

    WARNING(Statement::is_valid());
    SQLHSTMT stmt = Statement::get();
    SQLRETURN rc = SQL_SUCCESS;

    // If we are called twice for the same statement we need to close the open
    // cursor or an "invalid cursor state" error will occur on execute
    rc = SQLCloseCursor(stmt);
    LOG_SQLRETURN(rc, *this, "SQLCloseCursor");
    FATAL(is_error(rc), "Fatal Error on SQLCloseCursor.");

    // Avoid invalid string or buffer length error so we add 1 to the length.
    // HY090: Invalid string or buffer length.
    // (DM) The argument TextLength was less than or equal to 0 but not equal
    // to SQL_NTS.
    SQLINTEGER length = (SQLINTEGER) s.size() + 1;
    SQLCHAR *sqlstr = (SQLCHAR *) str.c_str();
    rc = SQLPrepareA(stmt, sqlstr, length);
    LOG_SQLRETURN(rc, *this, "SQLPrepare");
    FATAL(is_error(rc), "Fatal Error on SQLPrepareA.");

    sql->set_sql(str);

    return flag ? describe() : is_success(rc);
}

bool Sql::Implementation::describe()
{
    LOG_FUNCTION();
    //. TODO: What data sources cannot easily describe result sets before execution.
    //. The application can call SQLNumResultCols at any time after the
    //. statement is prepared or executed. However, because some data sources
    //. cannot easily describe the result sets that will be created by prepared
    //. statements, performance will suffer if SQLNumResultCols is called after
    //. a statement is prepared but before it is executed.
    //.
    //. SQLNumResultCols
    //. https://msdn.microsoft.com/en-us/library/ms711684%28v=vs.85%29.aspx

    // Determine number of columns so we know how many to lookup later

    WARNING(Statement::is_valid());
    SQLHSTMT stmt = Statement::get();
    SQLSMALLINT columnCount = 0;
    SQLNumResultCols(stmt, &columnCount);
    if (columnCount == 0) return false;

    // Get basic and extended column info and add to Column collection

    column::Properties property;
    sql->columns.clear();
    for (SQLUSMALLINT i = 1; i <= columnCount; ++i) {
        property.clear();
        property.identity = helper::is_identity(stmt, i);
        property.is_unsigned = helper::is_unsigned(stmt, i);
        property = helper::describe_column(stmt, i, property);
        sql->columns.add(property);
    }
    data.clear();
    return true;
}

db::ExecuteStatus Sql::Implementation::execute(unsigned int)
{
    LOG_FUNCTION();
    WARNING(Statement::is_valid());
    SQLHSTMT stmt = Statement::get();
    // If we are called twice for the same statement we need to close the open
    // cursor or an "invalid cursor state" error will occur on execute
    SQLCloseCursor(stmt);

    SQLRETURN rc = SQLExecute(stmt);
    LOG_SQLRETURN(rc, *this, "SQLExecute");
    FATAL(is_error(rc), "Fatal Error on SQLExecute.");

    if (is_resultset_available(stmt)) {
        bool success = describe();
        if (!success) {
            return db::ExecuteStatus::Error;
        }
        bind();
        auto rows = fetch();
        if (rows == 0) {
            return db::ExecuteStatus::NoData;
        } else if (rows > 0) {
            return db::ExecuteStatus::Success;
        } else {
            return db::ExecuteStatus::Error;
        }
    } else {
        set_affected_rows(stmt);
    }

    return db::ExecuteStatus::Success;
}

std::size_t Sql::Implementation::fetch()
{
    LOG_FUNCTION();
    WARNING(Statement::is_valid());
    SQLHSTMT stmt = Statement::get();
    SQLRETURN rc = SQL_SUCCESS;

    SQLULEN const row_array_size = static_cast<SQLULEN>(1);
    rc = SQLSetStmtAttr(stmt, SQL_ATTR_ROW_ARRAY_SIZE, (SQLPOINTER)row_array_size, 0);
    LOG_SQLRETURN(rc, *this, "SQLSetStmtAttr (SQL_ATTR_ROW_ARRAY_SIZE)");

    SQLULEN rowsFetched = 0;
    rc = SQLSetStmtAttr(stmt, SQL_ATTR_ROWS_FETCHED_PTR, &rowsFetched, 0);
    LOG_SQLRETURN(rc, *this, "SQLSetStmtAttr (SQL_ATTR_ROWS_FETCHED_PTR)");

    rc = SQLFetch(stmt);
    LOG_SQLRETURN(rc, *this, "SQLFetch");
    if (is_success(rc)) {
        post_fetch();
        sql->set_fetched_rows(rowsFetched);
    } else if (is_no_data(rc)) {
        sql->set_fetched_rows(0);
    } else {
        sql->set_fetched_rows(0);
        // For SQL_ERROR || SQL_STILL_EXECUTING || SQL_INVALID_HANDLE
        FAIL();
    }
    return sql->get_fetched_rows();
}

std::string Sql::Implementation::get_native_sql() const
{
    const SQLINTEGER Length = 1024;
    SQLCHAR outSql[Length];
    SQLINTEGER outSqlLen;
    auto dbc = static_cast<Backend *>(backend)->Statement::get();
    WARNING(dbc != nullptr);
    auto sql(sql->get_sql());
    SQLRETURN rc = SQLNativeSqlA(
        dbc,
        (SQLCHAR*)sql.c_str(),
        (SQLINTEGER)sql.length(),
        outSql, Length, &outSqlLen);
    //? Fix this.
    //? LOG_SQLRETURN(rc, *this, "SQLNativeSql", dbc, rc);
    FATAL(is_error(rc), "Fatal Error on SQLNativeSqlA.");
    return std::string((const char *)outSql, outSqlLen);
}

void Sql::Implementation::bind()
{
    LOG_FUNCTION();
    auto count = static_cast<SQLUSMALLINT>(sql->columns.count());

    // Prepare the temporary buffer where to store the retrieved row values

    SQLHSTMT stmt = Statement::get();
    buffer.set_statement_handle(stmt);
    buffer.init(count);

    // Bind column values to the temporary buffer

    SQLLEN bufLen = 0;

    for (SQLUSMALLINT i = 0; i < count; ++i) {
        column::Type type = sql->columns.get_type(i);
        //SQLUSMALLINT sqltype = sql->columns.get_sqltype(i);
        SQLUSMALLINT ctype = helper::to_c_type(type);

        if (type == column::Type::Date) {

            bufLen = sizeof(TIMESTAMP_STRUCT);
            buffer.add(ctype, bufLen);

        } else if (type == column::Type::Time) {

            bufLen = sizeof(TIME_STRUCT);
            buffer.add(ctype, bufLen);

        } else if (type == column::Type::UInt32 ||
            type == column::Type::Int32) {

            bufLen = sizeof(int);
            buffer.add(ctype, bufLen);

#if 0
        } else if (p.type == column::Type::Float ||
            p.type == column::Type::Double ||
#endif
        } else if (type == column::Type::Numeric ||
            type == column::Type::Decimal) {

            //. TODO: How to get money type from PostgreSQL.
            //. PostgreSQL money type prefixes the data with a monetary symbol.

            buffer.add(ctype, bufLen);

        } else if (type == column::Type::String) {

            bufLen = sql->columns.get_length(i) + string::null_size;
            buffer.add(SQL_C_CHAR, bufLen);

        } else {
            FAIL("Type %1 not caught in if/else statement.", static_cast<int>(type));
        }
#if 0
            case column::Type::Timestamp:
                bufLen = sizeof(TIMESTAMP_STRUCT);
                break;
            case negerns::db::column::Type::Float:
            case negerns::db::column::Type::Double:
                bufLen = sizeof(double);
                break;
            case negerns::db::column::Type::Real:
                bufLen = sizeof(float);
                break;
            case negerns::db::column::Type::UInt8:
            case negerns::db::column::Type::Int8:
                bufLen = sizeof(char);
                break;
            case negerns::db::column::Type::UInt16:
            case negerns::db::column::Type::Int16:
                bufLen = sizeof(short);
                break;
            case negerns::db::column::Type::UInt32:
            case negerns::db::column::Type::Int32:
                bufLen = sizeof(int);
#if 0
                buf.emplace_back(new char[1]); // dummy
                //bufPtr.emplace_back(buf[i]);
                bufPtr.emplace_back(nullptr);
                rc = SQLBindCol(stmt, i + 1, type, bufPtr[i], bufLen, &indicator);
#endif
                break;
            case negerns::db::column::Type::UInt64:
            case negerns::db::column::Type::Int64:
                bufLen = sizeof(_int64);
                break;
            case negerns::db::column::Type::Char:
                bufLen = sizeof(unsigned char);
                break;
            default:
                bufLen = 0;
        }
#endif

    } //_ for
    ASSERT(buffer.data.size() == count);
}

void Sql::Implementation::post_fetch()
{
    LOG_FUNCTION();
    WARNING(buffer.size() > 0, "Temporary buffer is zero.");
    auto count = static_cast<SQLUSMALLINT>(sql->columns.count());
    WARNING(buffer.size() == count, "Fetch buffer size != column size.");

    // Convert to proper type

    column::Properties p;

    for (SQLUSMALLINT i = 0; i < count; ++i) {

        //. TODO: Use data instead of accessing columns from Sql instance.
        p = sql->columns(i);

        if (p.type == column::Type::Date) {
            TIMESTAMP_STRUCT * ts = reinterpret_cast<TIMESTAMP_STRUCT*>(
                buffer.data[i].char_);
            std::tm value;
            value.tm_isdst = -1;
            value.tm_year = ts->year - 1900;
            value.tm_mon = ts->month - 1;
            value.tm_mday = ts->day;
            value.tm_hour = 0;
            value.tm_min = 0;
            value.tm_sec = 0;

            data.emplace_back(value);
        } else if (p.type == column::Type::Time) {
            TIME_STRUCT * ts = reinterpret_cast<TIME_STRUCT*>(
                buffer.data[i].char_);
            std::tm value;
            value.tm_isdst = -1;
            value.tm_year = 0;  // 1900
            value.tm_mon = 0;   // jan
            value.tm_mday = 1;
            value.tm_hour = ts->hour;
            value.tm_min = ts->minute;
            value.tm_sec = ts->second;

            data.emplace_back(value);
        } else if (p.type == column::Type::UInt32 ||
            p.type == column::Type::Int32) {
            int value = buffer.data[i].int_;
            data.emplace_back(value);
        } else if (p.type == column::Type::Float ||
            p.type == column::Type::Double ||
            p.type == column::Type::Numeric ||
            p.type == column::Type::Decimal) {

            WARNING(Statement::is_valid());
            SQLHSTMT stmt = Statement::get();
            SQLLEN a;
            SQLRETURN rc = SQLGetData(stmt, i + 1, SQL_ARD_TYPE,
                buffer.data[i].numeric_, 19, &a);
            LOG_SQLRETURN(rc, *this, "SQLGetData");
            if (is_success(rc)) {
                // Check for null indicator.
                if (SQL_NULL_DATA == a) {
                    //. TODO: What do we do if data retrieved is null?
                    continue;
                }
            } else if (is_error(rc)) {
                FAIL();
                return;
            }
            double value = helper::to_double(*(buffer.data[i].numeric_));
            data.emplace_back(value);
        } else if (p.type == column::Type::String) {
            std::string value(buffer.data[i].char_);
            data.emplace_back(value);
        } else {
            FAIL("Type %1 not caught in if/else statement.", static_cast<int>(p.type));
        }
    }
}

SQLHSTMT Sql::Implementation::get_statement_handle()
{
    return Statement::get();
}

//. TODO: Move this to a class which deals with SQL statements.
//. Or if CRUD operations are in there own classes we would not need this function.
bool Sql::Implementation::is_select_statement() const
{
    auto s = string::to_lower(sql->get_sql());
    s = string::trim(s);
    return s.find("select") == 0;
}

bool Sql::Implementation::is_resultset_available(SQLHSTMT stmt) const
{
    SQLSMALLINT columnCount;
    SQLRETURN rc = SQLNumResultCols(stmt, &columnCount);
    LOG_SQLRETURN(rc, *this, "SQLNumResultCols");
    FATAL(is_error(rc), "Fatal Error on SQLNumResultCols.");
    return columnCount > 0;
}

void Sql::Implementation::set_affected_rows(SQLHSTMT stmt)
{
    SQLLEN rows = 0;
    SQLRETURN rc = SQLRowCount(stmt, &rows);
    LOG_SQLRETURN(rc, *this, "SQLRowCount");
    sql->set_affected_rows(rows);
    FATAL(is_error(rc), "Fatal Error on SQLRowCount.");
}

} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns
