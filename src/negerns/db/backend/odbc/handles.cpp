#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/db/backend/odbc/error.h>
#include <negerns/db/backend/odbc/handles.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {

Environment::Environment() :
    Handle(SQL_HANDLE_ENV)
{
    LOG_FUNCTION();
    SQLRETURN rc = SQL_SUCCESS;

    // Set connection pooling
    SQLPOINTER val = (SQLPOINTER) SQL_CP_ONE_PER_HENV;
    rc = SQLSetEnvAttr(SQL_NULL_HANDLE, SQL_ATTR_CONNECTION_POOLING, val, 0);
    LOG_SQLRETURN(rc, "SQLSetEnvAttr (SQL_ATTR_CONNECTION_POOLING)");

    // Allocate environment handle
    SQLHENV env = SQL_NULL_HENV;
    rc = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &env);
    LOG_SQLRETURN(rc, "SQLAllocHandle (SQL_HANDLE_ENV)");
    if (is_success(rc)) {
        Handle::set(env);
        // Set the ODBC version environment attribute
        SQLPOINTER odbcVersion = (SQLPOINTER)SQL_OV_ODBC3;
        rc = SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, odbcVersion, 0);
        LOG_SQLRETURN(rc, *this, "SQLSetEnvAttr (SQL_ATTR_ODBC_VERSION)");
    }
}

Environment::~Environment()
{
    LOG_FUNCTION();
    SQLHENV env = Handle::get();
    ASSERT(env != nullptr, "ODBC Environment is null.");
    SQLRETURN rc = SQLFreeHandle(SQL_HANDLE_ENV, env);
    LOG_SQLRETURN(rc, *this, "SQLFreeHandle (SQL_HANDLE_ENV)");
    if (is_success(rc)) {
        Handle::set(SQL_NULL_HENV);
    }
}



// -----------------------------------------------------------------------------



Connection::Connection() :
    Handle(SQL_HANDLE_DBC)
{ }

Connection::Connection(SQLHDBC h) :
    Handle(SQL_HANDLE_DBC, h)
{ }

Connection::~Connection() { }



// -----------------------------------------------------------------------------



Statement::Statement() :
    Handle(SQL_HANDLE_STMT)
{ }

Statement::~Statement() { }



} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns
