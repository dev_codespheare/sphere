#include <map>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/db/backend/odbc/error.h>
#include <negerns/db/backend/odbc/environment.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {
namespace environment {

Environment * implementation(EnvironmentPtr, bool);

std::string get_odbc_version_string(Version v)
{
    static std::map<Version, std::string> versionstr {
        { Version::ODBCUnknown, "Uknown" },
        { Version::ODBC380,     "3.80" },
        { Version::ODBC300,     "3.x0" },
        { Version::ODBC200,     "2.00" }
    };
    return versionstr[v];
}

std::string connection_pooling_name(ConnectionPooling cp)
{
    static std::map<ConnectionPooling, std::string> names {
        { ConnectionPooling::Off,               "Off"},
        { ConnectionPooling::OnePerDriver,      "One per Driver"},
        { ConnectionPooling::OnePerEnvironment, "One per Environment"},
        { ConnectionPooling::DriverAware,       "Driver-Aware"},
    };
    return names[cp];
}

std::string connection_pooling_match_name(ConnectionPoolingMatch cpm)
{
    static std::map<ConnectionPoolingMatch, std::string> names {
        { ConnectionPoolingMatch::Strict,  "Strict"},
        { ConnectionPoolingMatch::Relaxed, "Relaxed"},
    };
    return names[cpm];
}

Environment * implementation(EnvironmentPtr p, bool force)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static EnvironmentPtr instance = nullptr;

    if (force && p == nullptr && instance != nullptr) {
        instance.reset();
        instance = nullptr;
        return nullptr;
    }

    if (p != nullptr && instance == nullptr) {
        // Assignment
        instance = std::move(p);
        return nullptr;
    } else if (p == nullptr && instance != nullptr) {
        // Query
        return instance.get();
    } else if (p != nullptr && instance != nullptr) {
        // Reassignment
        return nullptr;
    } else if (p == nullptr && instance == nullptr) {
        // Possible query or an attempt to initialize object with a null value.
        // But we do not allow initialization to null, so, this must be a query
        // returning the uninitialized value (nullptr).
        return nullptr;
    } else {
        std::string msg("Should not execute this code.");
        FAIL(msg);
        throw std::runtime_error(msg);
    }
}



SQLHENV init()
{
    LOG_FUNCTION();
    auto ptr = implementation();
    log::debug_if(ptr != nullptr, "Environment::Impl is already created.");
    if (ptr == nullptr) {
        implementation(std::make_unique<Environment>());
        log::debug("Environment::Impl initialization: Success.");
    }
    return implementation()->get();
}

void shutdown()
{
    LOG_FUNCTION();
    auto ptr = implementation();
    WARNING(ptr != nullptr, "ODBC Environment is null. "
        "Could ODBC Environment shutdown been called before?");
    if (ptr != nullptr) {
        implementation(nullptr, true);
    }
    ptr = implementation();
    ASSERT(ptr == nullptr, "ODBC Environment should now be null.");
}

bool is_valid()
{
    auto ptr = implementation();
    if (ptr == nullptr) return false;
    return ptr->get() != nullptr;
}

Version get_odbc_version()
{
#if (ODBCVER >= 0x0300)
    Environment *ptr = implementation();
    ASSERT(ptr != nullptr, "ODBC Environment is null.");
    SQLHENV env = ptr->get();

    SQLINTEGER value;
    SQLINTEGER attr = SQL_ATTR_ODBC_VERSION;
    SQLRETURN rc = SQLGetEnvAttr(env, attr, (SQLPOINTER)&value, 0, 0);
    LOG_SQLRETURN(rc, *ptr, "SQLGetEnvAttr (SQL_ATTR_ODBC_VERSION)");
    Version version = Version::ODBCUnknown;
    if (!is_error(rc)) {
        switch (value) {
            case SQL_OV_ODBC3_80:
                version = Version::ODBC380;
                break;
            case SQL_OV_ODBC3:
                version = Version::ODBC300;
                break;
            case SQL_OV_ODBC2:
                version = Version::ODBC200;
                break;
            default:
                WARNING("Unknown ODBC version.");
        }
    }
    return version;
#else
    return Version::Unknown;
#endif
}

ConnectionPooling get_connection_pooling()
{
#if (ODBCVER >= SQL_OV_ODBC3_80)
    Environment *ptr = implementation();
    ASSERT(ptr != nullptr, "ODBC Environment is null.");
    SQLHENV env = ptr->get();

    SQLPOINTER value = nullptr;
    SQLINTEGER attr = SQL_ATTR_CONNECTION_POOLING;
    SQLRETURN rc = SQLGetEnvAttr(env, attr, &value, NULL, NULL);
    LOG_SQLRETURN(rc, *ptr, "SQLGetEnvAttr (SQL_ATTR_CONNECTION_POOLING)");
    if (is_error(rc) || value == nullptr) {
        return ConnectionPooling::Off;
    } else {
        return ConnectionPooling((SQLUINTEGER)value);
    }
#else
    return ConnectionPooling::Off;
#endif
}

ConnectionPoolingMatch get_connection_pooling_match()
{
#if (ODBCVER >= SQL_OV_ODBC3)
    Environment *ptr = implementation();
    ASSERT(ptr != nullptr, "ODBC Environment is null.");
    SQLHENV env = ptr->get();

    SQLINTEGER attr = SQL_ATTR_CP_MATCH;
    SQLPOINTER value = nullptr;
    SQLRETURN rc = SQLGetEnvAttr(env, attr, &value, NULL, NULL);
    LOG_SQLRETURN(rc, *ptr, "SQLGetEnvAttr (SQL_ATTR_CP_MATCH)");
    if (is_error(rc) || value == nullptr) {
        return ConnectionPoolingMatch::Strict;
    } else {
        return ConnectionPoolingMatch((SQLUINTEGER)value);
    }
#else
    return ConnectionPoolingMatch::Strict;
#endif
}

bool get_nts()
{
#if (ODBCVER >= SQL_OV_ODBC3)
    Environment *ptr = implementation();
    ASSERT(ptr != nullptr, "ODBC Environment is null.");
    SQLHENV env = ptr->get();

    SQLINTEGER attr = SQL_ATTR_OUTPUT_NTS;
    SQLPOINTER value = nullptr;
    SQLRETURN rc = SQLGetEnvAttr(env, attr, value, NULL, NULL);
    LOG_SQLRETURN(rc, *ptr, "SQLGetEnvAttr (SQL_ATTR_OUTPUT_NTS)");
    if (is_error(rc) || value == nullptr) {
        return true;
    } else {
        return (SQLUINTEGER)value == SQL_TRUE;
    }
#else
    return true;
#endif
}

SQLHENV get()
{
    auto p = implementation();
    return p == nullptr ? nullptr : p->get();
}

} //_ namespace environment
} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns
