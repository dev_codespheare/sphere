#include <sql.h>

#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/db/row.h>
#include <negerns/db/backend/odbc/backend.h>
#include <negerns/db/backend/odbc/error.h>
#include <negerns/db/backend/odbc/helper.h>
#include <negerns/db/backend/odbc/sql.h>
#include <negerns/db/backend/odbc/sqlimpl.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {

Sql::Sql(Sql &&o) :
    pimpl(std::move(o.pimpl))
{ }

Sql::Sql(negerns::db::connection::Pool *p, std::size_t i, backend::IBackend *be) :
    ISql(),
    pimpl(std::make_unique<Sql::Implementation>(this, p, i, be))
{ }

Sql::~Sql()
{
    WARNING(pimpl != nullptr);
    pimpl.reset();
    // SOCI sets this to -1LL
    // SOCI data type is long long
    ISql::set_affected_rows(0);
}

Sql & Sql::operator = (Sql &&rhs)
{
    if (this != &rhs) {
        pimpl = std::move(rhs.pimpl);
    }
    return *this;
}

std::string Sql::transform(const std::string &s)
{
    return pimpl->transform(s);
}

bool Sql::prepare(const std::string &s, bool flag)
{
    return pimpl->prepare(s, flag);
}

bool Sql::describe()
{
    return pimpl->describe();
}

negerns::db::ExecuteStatus Sql::execute(unsigned int n)
{
    return pimpl->execute(n);
}

std::size_t Sql::fetch()
{
    return pimpl->fetch();
}

negerns::Vars Sql::get_row_values() const
{
    return pimpl->get_row_values();
}

std::string Sql::get_native_sql() const
{
    return pimpl->get_native_sql();
}

SQLHSTMT Sql::get_statement_handle()
{
    return pimpl->get_statement_handle();
}

Sql & Sql::swap(Sql &rhs)
{
    std::swap(pimpl, rhs.pimpl);
    return *this;
}



// -----------------------------------------------------------------------------



std::size_t get_data_size(column::Type t) {
    static std::map<column::Type, std::size_t> sizes {
        { column::Type::Boolean,      sizeof(int8_t) },
        { column::Type::Int8,         sizeof(int8_t) },
        { column::Type::UInt8,        sizeof(int8_t) },
        { column::Type::Int16,        sizeof(int16_t) },
        { column::Type::UInt16,       sizeof(int16_t) },
        { column::Type::Int32,        sizeof(int32_t) },
        { column::Type::UInt32,       sizeof(int32_t) },
        { column::Type::Int64,        sizeof(int64_t) },
        { column::Type::UInt64,       sizeof(int64_t) },
        { column::Type::Float,        sizeof(SQL_NUMERIC_STRUCT) },
        { column::Type::Double,       sizeof(SQL_NUMERIC_STRUCT) },
        { column::Type::Real,         sizeof(SQL_NUMERIC_STRUCT) },
        { column::Type::Numeric,      sizeof(SQL_NUMERIC_STRUCT) },
        { column::Type::Decimal,      sizeof(SQL_NUMERIC_STRUCT) },
        { column::Type::Char,         0 },
        // String length is the table column length which is determined after
        // a call to the describe function.
        { column::Type::String,       0 },
        { column::Type::WString,      0 },
        { column::Type::Blob,         0 },
        { column::Type::Clob,         0 },
        { column::Type::Date,         sizeof(TIMESTAMP_STRUCT) },
        { column::Type::Time,         sizeof(TIME_STRUCT) },
        { column::Type::Timestamp,    sizeof(TIMESTAMP_STRUCT) },
        { column::Type::TimeNTZ,      sizeof(TIMESTAMP_STRUCT) },
        { column::Type::TimeTZ,       sizeof(TIMESTAMP_STRUCT) },
        { column::Type::TimestampNTZ, sizeof(TIMESTAMP_STRUCT) },
        { column::Type::TimestampTZ,  sizeof(TIMESTAMP_STRUCT) },
        { column::Type::Unknown,      0 }
    };
    return sizes[t];
}

} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns
