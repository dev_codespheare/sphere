#include <negerns/core/string.h>
#include <negerns/core/log.h>
#include <negerns/db/backend/odbc/backend.h>
#include <negerns/db/backend/odbc/error.h>

#include <negerns/negerns.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {

Backend::Backend() :
    Connection(),
    IBackend(db::connection::Source::ODBC)
{ }

Backend::Backend(const Backend &) = default;

Backend::Backend(Backend &&o) :
    connectionString(o.connectionString),
    databaseName(o.databaseName),
    dbmsName(o.dbmsName),
    dbmsVersion(o.dbmsVersion)
{ }

Backend::~Backend()
{
    LOG_FUNCTION();
    if (!IBackend::is_closed()) {
        close(CloseAction::Commit);
        IBackend::set_closed(true);
    }
}

Backend & Backend::operator = (const Backend &) = default;

n::db::Error Backend::open()
{
    LOG_FUNCTION();
    SQLRETURN rc = SQL_SUCCESS;

    // Get the only ODBC environment pointer.
    // If it does not exist yet then create it first.
    SQLHENV env = environment::get();
    if (env == nullptr) {
        log::debug("ODBC environment is null. Initializing...");
        env = environment::init();
        if (env == nullptr) {
            return n::db::Error::ODBCEnvironmentError;
        }
    }

    // Allocate connection handle
    SQLHDBC dbc = SQL_NULL_HDBC;
    rc = SQLAllocHandle(SQL_HANDLE_DBC, env, &dbc);
    LOG_SQLRETURN(rc, *environment::implementation(), "SQLAllocHandle (SQL_HANDLE_DBC)");
    if (is_error(rc)) {
        return n::db::Error::ODBCDBCError;
    } else {
        Connection::set(dbc);
    }

    auto params = this->get_parameters();
    std::string cs(params.get_connection_string());

    {
        // Buffer should be at least 1024 (ODBC 1.0)
        const SQLSMALLINT maxBufLen = 1024;
        SQLCHAR buf[maxBufLen];
        SQLSMALLINT buflen;
        SQLHWND hwndForPrompt = nullptr;
        SQLUSMALLINT driverCompletion = SQL_DRIVER_NOPROMPT;
        rc = SQLDriverConnectA(dbc, hwndForPrompt,
                              (SQLCHAR *)cs.c_str(),
                              (SQLSMALLINT)cs.size(),
                              buf, maxBufLen, &buflen,
                              driverCompletion);
        LOG_SQLRETURN(rc, odbc::Connection(Connection::get()), "SQLDriverConnect");
        if (is_error(rc)) {
            rc = SQLFreeHandle(SQL_HANDLE_DBC, dbc);
            LOG_SQLRETURN(rc, *this, "SQLFreeHandle (SQL_HANDLE_DBC)");
            dbc = nullptr;
            IBackend::set_closed(true);
            return n::db::Error::ODBCDriverConnectError;
        }
        IBackend::set_closed(false);
        connectionString.assign((const char*)buf, buflen);
    }

    std::string db = get_info(SQL_DBMS_NAME);
    dbmsName = db::connection::source_type(db);
    databaseName = get_info(SQL_DATABASE_NAME);
    dbmsVersion = get_info(SQL_DBMS_VER);

    return n::db::Error::None;
}

negerns::db::Error Backend::open(const db::connection::Parameters &params)
{
    LOG_FUNCTION();
    if (is_connected()) {
        return n::db::Error::None;
    }
    this->set_parameters(params);
    return open();
}

bool Backend::close(CloseAction action)
{
    //* IMPORTANT: Multiple cleanups
    //*
    //* There is cleanup code in function open() on a failed connection.
    //* Make sure that the codes are in sync.

    LOG_FUNCTION();
    // Return a successful close operation when there is no connection.
    if (!is_connected()) {
        return true;
    }
    SQLRETURN rc = SQL_SUCCESS;

    if (action == CloseAction::Commit) {
    } else if (action == CloseAction::Rollback) {
    }

    SQLHDBC dbc = Connection::get();
    rc = SQLDisconnect(dbc);
    LOG_SQLRETURN(rc, odbc::Connection(dbc), "SQLDisconnect");
    if (is_error(rc)) {
        return false;
    }

    rc = SQLFreeHandle(SQL_HANDLE_DBC, dbc);
    LOG_SQLRETURN(rc, odbc::Connection(dbc), "SQLFreeHandle (SQL_HANDLE_DBC)");
    if (is_error(rc)) {
        return false;
    }

    IBackend::set_closed(true);

    //* IMPORTANT:
    //* Set only the ODBC connection handle to null if it is
    //* no longer needed like when the application is exiting.
    //* Otherwise, do not set it to null. The driver manager
    //* will try to reuse it if possible in connection pooling.
    //*
    //* https://msdn.microsoft.com/en-us/library/ms716319%28v=vs.85%29.aspx
    DEBUG(dbc != NULL);

    return true;
}

n::db::Error Backend::reconnect()
{
    LOG_FUNCTION();
    close(CloseAction::Commit);
    return open();
}

bool Backend::is_connected() const
{
    if (!Connection::is_valid()) return false;

    SQLHDBC dbc = Connection::get();
    SQLUINTEGER val = 0;
    SQLRETURN rc = SQLGetConnectAttr(dbc, SQL_ATTR_CONNECTION_DEAD, &val, 0, 0);
    LOG_SQLRETURN(rc, odbc::Connection(Connection::get()),
        "SQLGetConnectAttr (SQL_ATTR_CONNECTION_DEAD)");
    if (is_error(rc)) {
        return false;
    } else {
        return (SQL_CD_FALSE == val);
    }
}

void Backend::begin()
{
    LOG_FUNCTION();
    WARNING(Connection::is_valid());
    SQLHDBC dbc = Connection::get();
    SQLRETURN rc = SQLSetConnectAttr(dbc, SQL_ATTR_AUTOCOMMIT,
                    (SQLPOINTER)SQL_AUTOCOMMIT_OFF, 0 );
    LOG_SQLRETURN(rc, odbc::Connection(Connection::get()),
        "SQLSetConnectAttr (SQL_ATTR_AUTOCOMMIT)");
}

void Backend::commit()
{
    LOG_FUNCTION();
    WARNING(Connection::is_valid());
    SQLHDBC dbc = Connection::get();
    SQLRETURN rc = SQLEndTran(SQL_HANDLE_DBC, dbc, SQL_COMMIT);
    LOG_SQLRETURN(rc, odbc::Connection(Connection::get()),
        "SQLEndTran (SQL_COMMIT)");
    reset_transaction();
}

void Backend::rollback()
{
    LOG_FUNCTION();
    WARNING(Connection::is_valid());
    SQLHDBC dbc = Connection::get();
    SQLRETURN rc = SQLEndTran(SQL_HANDLE_DBC, dbc, SQL_ROLLBACK);
    LOG_SQLRETURN(rc, odbc::Connection(Connection::get()),
        "SQLEndTran (SQL_ROLLBACK)");
    reset_transaction();
}

void Backend::reset_transaction()
{
    LOG_FUNCTION();
    WARNING(Connection::is_valid());
    SQLHDBC dbc = Connection::get();
    SQLRETURN rc = SQLSetConnectAttr(dbc, SQL_ATTR_AUTOCOMMIT,
                    (SQLPOINTER)SQL_AUTOCOMMIT_ON, 0);
    LOG_SQLRETURN(rc, odbc::Connection(Connection::get()),
        "SQLSetConnectAttr (SQL_ATTR_AUTOCOMMIT)");
}

std::string Backend::get_info(SQLUSMALLINT infoType) const
{
    WARNING(Connection::is_valid());
    SQLHDBC dbc = Connection::get();
    // Buffer should be at least 1024 (ODBC 1.0)
    const SQLSMALLINT maxBufLen = 1024;
    char info[maxBufLen];
    SQLSMALLINT buflen = sizeof(info);
    SQLRETURN rc = SQLGetInfoA(dbc, infoType, info, buflen, &buflen);
    LOG_SQLRETURN(rc, odbc::Connection(Connection::get()), "SQLGetInfo");
    if (is_error(rc)) {
        return { };
    }
    return std::string(info, buflen);
}

std::size_t Backend::get_max_active_connections()
{
    {
        bool valid = Connection::is_valid();
        WARNING(valid);
        if (!valid) return 0;
    }
    SQLHDBC dbc = Connection::get();

    // Buffer should be at least 1024 (ODBC 1.0)
    const SQLSMALLINT maxBufLen = 1024;
    SQLUSMALLINT infoType = SQL_MAX_DRIVER_CONNECTIONS;
    char info[maxBufLen];
    SQLSMALLINT buflen = sizeof(info);
    SQLRETURN rc = SQLGetInfo(dbc, infoType, info, buflen, &buflen);
    LOG_SQLRETURN(rc, odbc::Connection(Connection::get()),
        "SQLGetInfo (SQL_MAX_DRIVER_CONNECTIONS)");
    if (is_error(rc)) {
        return 0;
    }
    //. TODO: Where do we get the result?
    return 0;
}

} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns
