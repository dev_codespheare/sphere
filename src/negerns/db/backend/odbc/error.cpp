#include <tuple>
#include <negerns/core/log.h>
#include <negerns/core/debug.h>
#include <negerns/core/string.h>
#include <negerns/db/backend/odbc/error.h>

namespace negerns {
namespace db {
namespace backend {
namespace odbc {

typedef std::tuple<std::string, std::string, std::string> ErrorInfo;

static const std::vector<std::string> label {
    "SQLState: ",
    "Native:   ",
    "Message:  "
};

static const std::string indent(9, ' ');

struct ODBCErrorInfo {
    std::string sqlState;
    std::string nativeCode;
    std::string message;
} odbcInfo;

ODBCErrorInfo get_info(SQLSMALLINT handleType, SQLHANDLE handle)
{
    SQLCHAR sqlState[SQL_SQLSTATE_SIZE + 1];
    SQLINTEGER nativeError;
    SQLCHAR msg[SQL_MAX_MESSAGE_LENGTH + 1];
    const SQLSMALLINT msgSize = sizeof(msg);
    SQLSMALLINT msgLength;
    const SQLSMALLINT recNumber = 1;

    std::string info;
    ODBCErrorInfo errorInfo;

    SQLRETURN rc = SQLGetDiagRecA(handleType, handle, recNumber, sqlState,
        &nativeError, msg, msgSize, &msgLength);

    errorInfo.sqlState = "01000";
    errorInfo.nativeCode = "0";

    switch (rc) {
        case SQL_SUCCESS:
            errorInfo.sqlState = std::string(reinterpret_cast<char *>(sqlState),
                SQL_SQLSTATE_SIZE + 1);
            errorInfo.nativeCode = string::to_string(nativeError);
            errorInfo.message = std::string(reinterpret_cast<char*>(msg),
                msgSize + 1);
            break;

        case SQL_INVALID_HANDLE:
            errorInfo.message = "[Sphere] Invalid handle.";
            break;

        case SQL_ERROR:
            errorInfo.message = "[Sphere] SQLGetDiagRec() error.";
            break;

        case SQL_SUCCESS_WITH_INFO:
            errorInfo.message = "[Sphere] Error message too long.";
            break;

        case SQL_NO_DATA:
            errorInfo.message = "[Sphere] No data.";
            break;

        default:
            errorInfo.message = negerns::Format(
                "[Sphere] SQLGetDiagRec() returned %1.", rc);
            break;
    }
    return errorInfo;
}

void log_internal(const std::string &, SQLSMALLINT, SQLHANDLE, SQLRETURN) { }

void log_sqlreturn_impl(const char *file, int line, const char *func,
    SQLRETURN rc, const std::string &s,
    const std::vector<::negerns::Var> &)
{
    // Ignore contents of the vector

    log::tracing::utility::Info tri(func, file, line);
    if (rc == SQL_SUCCESS) {
        std::string msg = s.empty() ? "[Unknown]" : s;
        msg.append(": Success.");
        log::trace(tri, msg.c_str());
    } else {
        auto err = get_info(SQL_HANDLE_ENV, SQL_NULL_HANDLE);
        log::Priority p = log::Priority::Debug;
        std::string prefix = s;
        if (rc == SQL_SUCCESS_WITH_INFO) {
            prefix.append(": Success (Info).");
        } else if (rc == SQL_ERROR) {
            p = log::Priority::Error;
            prefix.append(": Failed.");
        }
        log::trace(p, tri, "%1\n%2%3%4\n%5%6%7\n%8%9%10",
            prefix,
            indent, label[0], err.sqlState,
            indent, label[1], err.nativeCode,
            indent, label[2], err.message);
    }
}

void log_sqlreturn_impl(const char *file, int line, const char *func,
    SQLRETURN rc, const Handle &handle,
    const std::vector<::negerns::Var> &v)
{
    // Process only the first vector element and ignore the rest

    log::tracing::utility::Info tri(func, file, line);
    if (rc == SQL_SUCCESS) {
        std::string msg = v.empty() ? "[Unknown]" : v[0].as_string();
        msg.append(": Success.");
        log::trace(tri, msg.c_str());
    } else {
        auto err = get_info(handle.get_handle_type_id(), handle.get());
        log::Priority p = log::Priority::Debug;
        std::string prefix = v.empty() ? "[Unknown]" : v[0].as_string();
        if (rc == SQL_SUCCESS_WITH_INFO) {
            prefix.append(": Success (Info).");
        } else if (rc == SQL_ERROR) {
            p = log::Priority::Error;
            prefix.append(": Failed.");
        }
        log::trace(p, tri, "%1\n%2%3%4\n%5%6%7\n%8%9%10",
            prefix,
            indent, label[0], err.sqlState,
            indent, label[1], err.nativeCode,
            indent, label[2], err.message);
    }
}

} //_ namespace odbc
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns
