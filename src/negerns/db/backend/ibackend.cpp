#include <negerns/db/backend/ibackend.h>

namespace negerns {
namespace db {
namespace backend {

IBackend::IBackend() { }

IBackend::IBackend(connection::Source src) :
    source(src)
{ }

IBackend::IBackend(const IBackend &o) :
    source(o.source),
    parameters(o.parameters),
    closed(o.closed)
{ }

IBackend::IBackend(IBackend &&o) :
    source(o.source),
    parameters(o.parameters),
    closed(o.closed)
{
    if (this != &o) {
        o.source = db::connection::Source::None;
        o.parameters = db::connection::Parameters();
        o.closed = false;
    }
}

IBackend::~IBackend() { }

IBackend & IBackend::operator = (const IBackend &rhs)
{
    source = rhs.source;
    parameters = rhs.parameters;
    closed = rhs.closed;
    return *this;
}

IBackend & IBackend::operator = (IBackend &&rhs)
{
    if (this != &rhs) {
        source = rhs.source;
        parameters = rhs.parameters;
        closed = rhs.closed;
        rhs.source = db::connection::Source::None;
        rhs.parameters = db::connection::Parameters();
        rhs.closed = false;
    }
    return *this;
}

} //_ namespace backend
} //_ namespace db
} //_ namespace negerns
