#include <negerns/db/backend/sqlite/backend.h>

#include <negerns/negerns.h>

namespace negerns {
namespace db {
namespace backend {
namespace sqlite {

Backend::Backend() :
    IBackend(db::connection::Source::SQLite)
{ }

Backend::Backend(Backend &&) { }

Backend::~Backend() { }

n::db::Error Backend::open(const db::connection::Parameters &)
{
    std::cout << "Open" << std::endl;
    return n::db::Error::None;
}

bool Backend::close(CloseAction)
{
    std::cout << "Close" << std::endl;
    return false;
}

negerns::db::Error Backend::reconnect()
{
    std::cout << "Reconnect" << std::endl;
    return negerns::db::Error::None;
}

bool Backend::is_connected() const
{
    std::cout << "Reconnect" << std::endl;
    return true;
}

void Backend::begin()
{
    std::cout << "Transaction Begin" << std::endl;
}

void Backend::commit()
{
    std::cout << "Transaction Commit" << std::endl;
}

void Backend::rollback()
{
    std::cout << "Transaction Rollback" << std::endl;
}

} //_ namespace sqlite
} //_ namespace backend
} //_ namespace db
} //_ namespace negerns
