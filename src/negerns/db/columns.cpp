#include <negerns/db/columns.h>

namespace negerns {
namespace db {

Columns::Columns(const Columns &o)
{
    if (this != &o) {
        properties = o.properties;
        string_map = o.string_map;
    }
}

Columns::Columns(Columns &&o) :
    properties(o.properties),
    string_map(o.string_map)
{
    o.properties.clear();
    o.string_map.clear();
}

Columns::~Columns()
{
    properties.clear();
    string_map.clear();
}

Columns & Columns::operator = (const Columns &o) {
    if (this != &o) {
        properties = o.properties;
        string_map = o.string_map;
    }
    return *this;
}
Columns & Columns::operator = (Columns &&o) {
    properties = o.properties;
    string_map = o.string_map;
    o.properties.clear();
    o.string_map.clear();
    return *this;
}

void Columns::add(const column::Properties &p)
{
    ASSERT(!p.name.empty(), "Properties.name is empty.");
    properties.emplace_back(p);
    string_map.emplace(std::make_pair(p.name, string_map.size()));
}

column::Properties Columns::operator () (const std::string &s)
{
    if (string_map.empty()) {
        return column::Properties();
    }
    auto pos = string_map.find(s);
    if (pos != string_map.end()) {
        return properties.at(pos->second);
    } else {
        return column::Properties();
    }
}

const column::Properties Columns::operator () (const std::string &s) const
{
    if (string_map.empty()) {
        return column::Properties();
    }
    auto pos = string_map.find(s);
    if (pos != string_map.end()) {
        return properties.at(pos->second);
    } else {
        return column::Properties();
    }
}

std::pair<bool, std::size_t> Columns::get_index(const std::string &s) const
{
    if (string_map.empty()) {
        return std::make_pair(false, 0);
    }
    auto pos = string_map.find(s);
    if (pos != string_map.end()) {
        return std::make_pair(true, pos->second);
    } else {
        return std::make_pair(false, 0);
    }
}

#if 0
inline
bool Columns::is_found(const std::string &s) const
{
    if (string_map.size() > 0) {
        string_map.find(s) != string_map.end();
    } else {
        return false;
    }
}
#endif

} //_ namespace db
} //_ namespace negerns
