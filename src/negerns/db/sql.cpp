#include <negerns/core/debug.h>
#include <negerns/db/pool.h>
#include <negerns/db/backend/ibackend.h>
#include <negerns/db/backend/odbc/sql.h>
#include <negerns/db/sql.h>

namespace negerns {
namespace db {

Sql::Sql()
{
    auto pool = db::Pool::get();
    connection::Pool::ItemRef iref = pool->get();
    auto be = iref.first;
    auto index = iref.second;
    if (be == nullptr && index == 0) {
        //. TODO: All Connections in the pool are being used. See Pool class.
    } else {
        isql = std::make_unique<backend::odbc::Sql>(pool.get(), index, be);
    }
}

Sql::Sql(const std::string &s)
{
    auto pool = db::Pool::get();
    connection::Pool::ItemRef iref = pool->get();
    auto be = iref.first;
    auto index = iref.second;
    if (be == nullptr && index == 0) {
        //. TODO: All Connections in the pool are being used. See Pool class.
    } else {
        isql = std::make_unique<backend::odbc::Sql>(pool.get(), index, be);
        static_cast<void>(isql->prepare(s, true));
    }
}

} //_ namespace db
} //_ namespace negerns
