#include <negerns/core/decimal.h>
#include <negerns/core/datetime.h>
#include <negerns/db/row.h>

namespace negerns {
namespace db {

Row::Row(const Columns &c) : columns(c)
{
    set_default_values();
}

void Row::set_columns(const Columns &cols)
{
    ASSERT(!is_linked(), "Row references Rows instance.");
    columns = cols;
    set_default_values();
}

void Row::set_default_values()
{
    values.clear();
    for (auto &col : columns) {
        switch (col.type) {
            case column::Type::Boolean:
                values.emplace_back(false);
                break;
            case column::Type::Int8:
            case column::Type::UInt8:
            case column::Type::Int16:
            case column::Type::UInt16:
            case column::Type::Int32:
            case column::Type::UInt32:
            case column::Type::Int64:
            case column::Type::UInt64:
                values.emplace_back(0);
                break;
            case column::Type::Float:
            case column::Type::Double:
            case column::Type::Real:
            case column::Type::Numeric:
            case column::Type::Decimal:
                values.emplace_back(decimal::to_dec2("0.00"));
                break;
            case column::Type::Char:       // Fixed length char (ODBC)
            case column::Type::String:
            case column::Type::WString:
                values.emplace_back("");
                break;
#if 0
            case column::Type::Blob,
            case column::Type::Clob,
#endif
            case column::Type::Date:
            case column::Type::Time:
            case column::Type::Timestamp:
                values.emplace_back(date::now());
                break;
#if 0
            case column::Type::TimeNTZ,
            case column::Type::TimeTZ,
            case column::Type::TimestampNTZ,
            case column::Type::TimestampTZ,
#endif
            case column::Type::Unknown:
                values.emplace_back("");
                break;
        }
    }
}


Rows::Rows(std::unique_ptr<Sql> s) : sql(std::move(s)) { }

Rows::~Rows() { }

} //_ namespace db
} //_ namespace negerns
