#include <sql.h>
#include <negerns/db/column/properties.h>

namespace negerns {
namespace db {
namespace column {

std::string type_name(Type t)
{
    static std::map<Type, std::string> names {
        {Type::Boolean,      "bool"},
        {Type::Int8,         "int8_t"},
        {Type::UInt8,        "uint8_t"},
        {Type::Int16,        "int16_t"},
        {Type::UInt16,       "uint16_t"},
        {Type::Int32,        "int32_t"},
        {Type::UInt32,       "uint32_t"},
        {Type::Int64,        "int64_t"},
        {Type::UInt64,       "uint64_t"},
        {Type::Float,        "float"},
        {Type::Double,       "double"},
        {Type::Real,         "double"},
        {Type::Numeric,      "double"},
        {Type::Decimal,      "double"},
        {Type::Char,         "char"},
        {Type::String,       "string"},
        {Type::WString,      "wstring"},
        {Type::Blob,         "BLOB *"},
        {Type::Clob,         "CLOB *"},
        {Type::Date,         "std::tm (date)"},
        {Type::Time,         "std::tm (time)"},
        {Type::Timestamp,    "std::tm"},
        {Type::TimeNTZ,      "TimeNTZ *"},
        {Type::TimeTZ,       "TimeTZ *"},
        {Type::TimestampNTZ, "TimestampNTZ *"},
        {Type::TimestampTZ,  "TimestampTZ *"},
        {Type::Unknown,      "Unknown"}

    };

    return names[t];
}

std::string sql_type_name(Type t)
{
    static std::map<Type, std::string> names {
        {Type::Boolean,      "Boolean"},
        {Type::Int8,         "TinyInt"},
        {Type::UInt8,        "UTinyInt"},
        {Type::Int16,        "SmallInt"},
        {Type::UInt16,       "USmallInt"},
        {Type::Int32,        "Integer"},
        {Type::UInt32,       "UInteger"},
        {Type::Int64,        "BigInt"},
        {Type::UInt64,       "UBigInt"},
        {Type::Float,        "Float"},
        {Type::Double,       "Double"},
        {Type::Real,         "Real"},
        {Type::Numeric,      "Numeric"},
        {Type::Decimal,      "Decimal"},
        {Type::Char,         "Char"},
        {Type::String,       "VarChar"},
        {Type::WString,      "WVarChar"},
        {Type::Blob,         "BLOB *"},
        {Type::Clob,         "CLOB *"},
        {Type::Date,         "Date"},
        {Type::Time,         "Time"},
        {Type::Timestamp,    "Timestamp"},
        {Type::TimeNTZ,      "TimeNTZ *"},
        {Type::TimeTZ,       "TimeTZ *"},
        {Type::TimestampNTZ, "TimestampNTZ *"},
        {Type::TimestampTZ,  "TimestampTZ *"},
        {Type::Unknown,      "Unknown"}

    };

    return names[t];
}



Properties::Properties() { }

Properties::Properties(const Properties &p) :
    position(p.position),
    name(p.name),
    type(p.type),
    sqlType(p.sqlType),
    scale(p.scale),
    precision(p.precision),
    identity(p.identity),
    nullable(p.nullable),
    valid(p.valid)
{ }

Properties::Properties(Properties &&p) :
    position(p.position),
    name(p.name),
    type(p.type),
    sqlType(p.sqlType),
    scale(p.scale),
    precision(p.precision),
    identity(p.identity),
    nullable(p.nullable),
    valid(p.valid)
{ }

Properties::~Properties() { }

Properties& Properties::operator = (const Properties &p)
{
    if (this != &p) {
        position = p.position;
        name = p.name;
        type = p.type;
        sqlType = p.sqlType,
        scale = p.scale;
        precision = p.precision;
        identity = p.identity;
        nullable = p.nullable;
        valid = p.valid;
    }
    return *this;
}

Properties& Properties::operator = (Properties &&p)
{
    position = p.position;
    name = p.name;
    type = p.type;
    sqlType = p.sqlType,
    scale = p.scale;
    precision = p.precision;
    identity = p.identity;
    nullable = p.nullable;
    valid = p.valid;
    return *this;
}

void Properties::clear()
{
    position = 0;
    name = "";
    type = Type::Unknown;
    sqlType = SQL_UNKNOWN_TYPE,
    scale = 0;
    precision = 0;
    identity = false;
    nullable = false;
    is_unsigned = false;
    valid = false;
}

bool Properties::is_valid() const
{
    return valid;
}

} //_ namespace column
} //_ namespace db
} //_ namespace negerns
