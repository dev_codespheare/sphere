#include <map>
#include <negerns/db/types.h>

namespace negerns {
namespace db {

bool is_execute_error(ExecuteStatus s)
{
    return db::ExecuteStatus::Error == s;
}

std::string get_error_string(Error err)
{
    static std::map<Error, std::string> description {
        { Error::None,                          "None" },
        { Error::GenericError,                  "Generic Error" },
        { Error::ODBCEnvironmentError,          "ODBC Environment Error" },
        { Error::ODBCDBCError,                  "ODBC DBC Error" },
        { Error::ODBCDriverConnectError,        "ODBC Driver Connect Error" },
        { Error::ODBCDataSourceNameNotFound,    "ODBC Data Source Name Not Found" }
    };

    return description[err];
}

} //_ namespace db
} //_ namespace negerns
