#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <wx/stdpaths.h>
#include <wx/filename.h>
#include <negerns/core/log.h>
#include <negerns/core/path.h>

namespace negerns {

Path::Path()
{
    SetExecutablePath(wxStandardPaths::Get().GetExecutablePath());
}

void Path::SetExecutablePath(const wxString& p) {
    wxFileName filename(p);
    executablePath = filename.GetPath();
}

wxString Path::BuildFilename(const wxString& file)
{
    if (file.IsEmpty()) {
        log::warning("Cannot get the full path. Parameter is empty.");
        return file;
    }
    wxFileName filename(wxFileName::DirName(executablePath));
    filename.SetFullName(file);
    // The path may contain a parent path symbol '..' so force to
    // make an absolute path.
    if (!filename.MakeAbsolute()) {
        log::warning("Cannot convert to absolute path: %1", file.ToStdString());
    }
    return filename.GetFullPath();
}

wxString Path::MakeAbsolute(const wxString& file)
{
    if (file.IsEmpty()) {
        log::warning("Cannot get the full path. Parameter is empty.");
        return file;
    }
    wxFileName filename(file);
    if (filename.IsRelative()) {
        if (!filename.MakeAbsolute()) {
            log::warning("Cannot convert to absolute path: %1", file.ToStdString());
        }
    }
    return filename.GetFullPath();
}

} //_ namespace negerns
