#include <negerns/core/decimal.h>

namespace negerns {
namespace decimal {

dec2_t to_dec2(const std::string &arg)
{
    return ::dec::fromString<dec2_t>(arg);
}

dec4_t to_dec4(const std::string &arg)
{
    return ::dec::fromString<dec4_t>(arg);
}

dec6_t to_dec6(const std::string &arg)
{
    return ::dec::fromString<dec6_t>(arg);
}



dec2_t to_dec2r(const std::string &arg)
{
    return ::dec::decimal_cast<2, dec6_t>(::dec::fromString<dec6_t>(arg));
}

dec4_t to_dec4r(const std::string &arg)
{
    return ::dec::decimal_cast<4, dec6_t>(::dec::fromString<dec6_t>(arg));
}

dec6_t to_dec6r(const std::string &arg)
{
    return ::dec::decimal_cast<6, dec6_t>(::dec::fromString<dec6_t>(arg));
}



std::string to_string(dec2_t arg)
{
    // Code from dec::toString()

    std::ostringstream out;
    toStream(arg, out);
    return std::string(out.str());
}

std::string to_string(dec4_t arg)
{
    // Code from dec::toString()

    std::ostringstream out;
    toStream(arg, out);
    return std::string(out.str());
}

std::string to_string(dec6_t arg)
{
    // Code from dec::toString()

    std::ostringstream out;
    toStream(arg, out);
    return std::string(out.str());
}

} //_ namespace decimal
} //_ namespace negerns
