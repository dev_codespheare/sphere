#include <cctype>
#include <negerns/core/string.h>

namespace negerns {

std::string Format(const char *fmt, const std::vector<Var> &vars)
{
    return string::format::formatx(fmt, vars);
}

std::string Format(const std::string &fmt, const std::vector<Var> &vars)
{
    return string::format::formatx(fmt.c_str(), vars);
}

} //_ namespace negerns

namespace negerns {
namespace string {

const char * newline(OSType t)
{
    if (t == OSType::Windows) {
        return "\r\n";
    } else if (t == OSType::Macintosh) {
        return "\r";
    } else if (t == OSType::Linux || t == OSType::Unix) {
        // Mac OS X uses \n
        return "\n";
    } else {
        return "\n";
    }
}

std::size_t rfind_first_not_of(const std::string &s, char c,
    std::size_t idx)
{
    std::size_t len = s.length();
    if (idx == 0 || idx >= len) {
        idx = len - 1;
    }
    std::size_t i = idx;
    while (i != 0) {
        if (s[i] != c) {
            return i;
        }
        i--;
    }
    return std::string::npos;
}

std::string clean(const std::string &s)
{
    std::string str(s);
    str.erase(std::remove(str.begin(), str.end(), '\n'), str.end());
    return str;
}

void replace_inplace(std::string &str, const std::string &from,
    const std::string &to)
{
    if(from.empty())
        return;
    std::string wsRet;
    wsRet.reserve(str.length());
    size_t start_pos = 0, pos;
    while((pos = str.find(from, start_pos)) != std::string::npos) {
        wsRet += str.substr(start_pos, pos - start_pos);
        wsRet += to;
        pos += from.length();
        start_pos = pos;
    }
    wsRet += str.substr(start_pos);
    str.swap(wsRet); // faster than str = wsRet;
}

std::string replace(const std::string &str, const std::string &from,
    const std::string &to)
{
    if(from.empty())
        return str;
    std::string wsRet;
    wsRet.reserve(str.length());
    size_t start_pos = 0, pos;
    while((pos = str.find(from, start_pos)) != std::string::npos) {
        wsRet += str.substr(start_pos, pos - start_pos);
        wsRet += to;
        pos += from.length();
        start_pos = pos;
    }
    wsRet += str.substr(start_pos);
    return wsRet;
}

char* replace(const char *str, const char *old, const char *new_s)
{
    char *ret, *r;
    const char *p, *q;
    size_t oldlen = strlen(old);
    size_t retlen, newlen = strlen(new_s);

    if (oldlen != newlen) {
        size_t count;
        for (count = 0, p = str; (q = strstr(p, old)) != NULL; p = q + oldlen)
            count++;
        /* this is undefined if p - str > PTRDIFF_MAX */
        retlen = p - str + strlen(p) + count * (newlen - oldlen);
    } else
        retlen = strlen(str);

    /* rmaicle */
    /* Added the char* cast for compiling in C++11 */
    if ((ret = (char*) malloc(retlen + 1)) == NULL)
        return NULL;

    for (r = ret, p = str; (q = strstr(p, old)) != NULL; p = q + oldlen) {
        /* this is undefined if q - p > PTRDIFF_MAX */
        ptrdiff_t l = q - p;
        memcpy(r, p, l);
        r += l;
        memcpy(r, new_s, newlen);
        r += newlen;
    }
    strcpy(r, p);

    return ret;
}

void reverse(char* begin, char* end)
{
    char aux;
    while (end > begin)
        aux = *end, *end-- = *begin, *begin++ = aux;
}



std::string trim_left(const std::string &s)
{
    std::string r(s);
    r.erase(r.begin(), find_if_not(r.begin(), r.end(),
        [](int c) {
            return std::isspace(c);
        })
    );
    return std::move(r);
}

std::string trim_right(const std::string &s)
{
    std::string r(s);
    r.erase(find_if_not(r.rbegin(), r.rend(),
        [](int c) {
            return std::isspace(c);
        }).base(),
    r.end());
    return std::move(r);
}

std::string trim(const std::string &s)
{
    std::string r(s);
    return trim_left(trim_right(r));
}

void trim_right_if(std::string &s, char c, std::size_t idx)
{
    std::size_t p = rfind_first_not_of(s, c, idx);
    if (p != std::string::npos) {
        s = s.substr(0, p + 1);
    }
}

std::string concat_i(int repeat, char delim, int count, ...)
{
    std::string buf;
    buf.reserve(254);
    std::string delimiter;
    va_list args;
    va_start(args, count);
    if (repeat > 0) {
        count--;
        delimiter.append(repeat, delim);
        for (int i = 0; i < count; ++i) {
            buf.append(va_arg(args, std::string));
            buf.append(delimiter);
        }
        buf.append(va_arg(args, std::string));
    } else {
        for (int i = 0; i < count; ++i) {
            buf.append(va_arg(args, std::string));
        }
    }
    va_end(args);
    return buf;
}

std::vector<std::string> split(const std::string &s,
    char delim,
    bool rep,
    const std::string &prefix,
    const std::string &suffix)
{
    std::vector<std::string> strvec;
    if (s.empty()) {
        return strvec;
    }
    std::size_t sufflen = suffix.length();
    const char *work = s.data();
    std::string buf;
    if (!prefix.empty()) {
        buf.append(prefix);
    }
    int i = 0;
    while (i < s.length()) {
        if (work[i] != delim) {
            buf += work[i];
        } else if (rep) {
            if (sufflen > 0) buf.append(suffix);
            strvec.push_back(buf);
            buf = prefix;
        } else if (buf.length() > 0) {
            if (sufflen > 0) buf.append(suffix);
            strvec.push_back(buf);
            buf = prefix;
        }
        i++;
    }
    if (!buf.empty()) {
        if (sufflen > 0) buf.append(suffix);
        strvec.push_back(buf);
    }
    return strvec;
}

std::string join(const std::vector<std::string> &vec, int repeat, char delim)
{
    auto count = vec.size();
    std::string buf;
    buf.reserve(254);
    if (count > 0) {
        // Avoid comparing value with count - 1 more than once
        if (repeat > 0) {
            count--;
            std::string delimeter(repeat, delim);
            for (std::size_t i = 0; i < count; ++i) {
                buf.append(vec[i]);
                buf.append(delimeter);
            }
            buf.append(vec[count]);
        } else {
#if 1
            for (std::size_t i = 0; i < count; ++i) {
                buf.append(vec[i]);
            }
#endif
        }
    }
    return buf;
}

} //_ namespace string
} //_ namespace negerns
