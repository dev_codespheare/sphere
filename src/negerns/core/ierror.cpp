#include <negerns/core/debug.h>
#include <negerns/core/ierror.h>

namespace negerns {

IError::IError() :
    msgFormat("Code: %1\nError: %2\n\n%3")
{ }

std::string IError::get_string()
{
    // TODO: Add check for %3 and %%3 to determine if detail is to be included
    if (code.empty()) {
        code.assign("None.");
    }
    if (message.empty()) {
        message.assign("None.");
    }
    if (detail.empty()) {
        detail.assign("None.");
    }
    return negerns::Format(msgFormat, code, message, detail);
}

} //_ namespace negerns
