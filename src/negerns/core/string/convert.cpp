#include <boost/algorithm/string.hpp>
#include <boost/spirit/include/qi_parse.hpp>
#include <boost/spirit/include/qi_numeric.hpp>
#include <negerns/core/string/convert.h>
#include <negerns/core/internal/modp_numtoa.h>

namespace negerns {
namespace string {

enum { FRACTION_LENGTH = 9 };

static const char DIGITS[] =
    "0001020304050607080910111213141516171819"
    "2021222324252627282930313233343536373839"
    "4041424344454647484950515253545556575859"
    "6061626364656667686970717273747576777879"
    "8081828384858687888990919293949596979899";

// Powers of 10
// 10^0 to 10^9
static const double pow10[] = {
    1,
    10,
    100,
    1000,
    10000,
    100000,
    1000000,
    10000000,
    100000000,
    1000000000
};

static const double thres_max = (double) (0x7FFFFFFF);

uint32_t count_digits(uint64_t n)
{
    // Three Optimization Tips for C++
    // By Andrei Alexandrescu
    // https://www.facebook.com/notes/facebook-engineering/three-optimization-tips-for-c/10151361643253920
    uint32_t count = 1;
    for (;;) {
        if (n < 10) return count;
        if (n < 100) return count + 1;
        if (n < 1000) return count + 2;
        if (n < 10000) return count + 3;
        // Skip ahead by 4 orders of magnitude
        n /= 10000u;
        count += 4;
    }
}



// -----------------------------------------------------------------------------



std::string to_lower(const std::string &s)
{
    std::string r(s);
    std::transform(r.cbegin(), r.cend(), r.begin(),
        [](char c) { return std::tolower(c); }
    );
    return std::move(r);
}

std::string to_upper(const std::string &s)
{
    std::string r(s);
    std::transform(r.cbegin(), r.cend(), r.begin(),
        [](char c) { return std::toupper(c); }
    );
    return std::move(r);
}

std::string to_case(const std::string &s, Case c)
{
    switch (c) {
        case Case::Lower:
            return to_lower(s);
            break;
        case Case::Upper:
            return to_upper(s);
            break;
        default:
            return to_lower(s);
    }
}



// -----------------------------------------------------------------------------



bool stos(const std::string &str, short &result)
{
    std::string::const_iterator i = str.begin();
    if (!boost::spirit::qi::parse(i, str.end(), boost::spirit::short_, result))
        return false;
    return i == str.end(); // ensure the whole string was parsed
}

bool stous(const std::string &str, unsigned short &result)
{
    std::string::const_iterator i = str.begin();
    if (!boost::spirit::qi::parse(i, str.end(), boost::spirit::ushort_, result))
        return false;
    return i == str.end(); // ensure the whole string was parsed
}

bool stoi(const std::string& str, int &result)
{
    std::string::const_iterator i = str.begin();
    if (!boost::spirit::qi::parse(i, str.end(), boost::spirit::int_, result))
        return false;
    return i == str.end(); // ensure the whole string was parsed
}

bool stoui(const std::string &str, unsigned int &result)
{
    std::string::const_iterator i = str.begin();
    if (!boost::spirit::qi::parse(i, str.end(), boost::spirit::uint_, result))
        return false;
    return i == str.end(); // ensure the whole string was parsed
}

bool stol(const std::string &str, long &result)
{
    std::string::const_iterator i = str.begin();
    if (!boost::spirit::qi::parse(i, str.end(), boost::spirit::long_, result))
        return false;
    return i == str.end(); // ensure the whole string was parsed
}

bool stoul(const std::string &str, unsigned long &result)
{
    std::string::const_iterator i = str.begin();
    if (!boost::spirit::qi::parse(i, str.end(), boost::spirit::ulong_, result))
        return false;
    return i == str.end(); // ensure the whole string was parsed
}

bool stoll(const std::string &str, long long &result)
{
    std::string::const_iterator i = str.begin();
    if (!boost::spirit::qi::parse(i, str.end(), boost::spirit::long_long, result))
        return false;
    return i == str.end(); // ensure the whole string was parsed
}

bool stoull(const std::string &str, unsigned long long &result)
{
    std::string::const_iterator i = str.begin();
    if (!boost::spirit::qi::parse(i, str.end(), boost::spirit::ulong_long, result))
        return false;
    return i == str.end(); // ensure the whole string was parsed
}



// -----------------------------------------------------------------------------



short stos(const std::string &str)
{
    short number = 0;
    stos(str, number);
    return number;
}

unsigned short stous(const std::string &str)
{
    unsigned short number = 0;
    stous(str, number);
    return number;
}

int stoi(const std::string &str)
{
    int number = 0;
    stoi(str, number);
    return number;
}

unsigned int stoui(const std::string &str)
{
    unsigned int number = 0;
    stoui(str, number);
    return number;
}

long stol(const std::string &str)
{
    long number = 0;
    stol(str, number);
    return number;
}

unsigned long stoul(const std::string &str)
{
    unsigned long number = 0;
    stoul(str, number);
    return number;
}

long long stoll(const std::string &str)
{
    long long number = 0;
    stoll(str, number);
    return number;
}

unsigned long long stoull(const std::string &str)
{
    unsigned long long number = 0;
    stoull(str, number);
    return number;
}



// -----------------------------------------------------------------------------



std::string to_string(bool b)
{
    auto strPair = boolean::bool_alpha_default();
    return b ? strPair.first : strPair.second;
}

std::string to_string(bool b, std::pair<std::string, std::string> strPair)
{
    return b ? strPair.first : strPair.second;
}

std::string to_string(int32_t v)
{
    // Buffer should be large enough to hold all digits, sign and null char
    enum { Limit = std::numeric_limits<int32_t>::digits10 + 2 };
    char buf[Limit];
    modp_itoa10(v, buf);
    return buf;
}

std::string to_string(uint32_t v)
{
    // Buffer should be large enough to hold all digits and null char
    enum { Limit = std::numeric_limits<uint32_t>::digits10 + 1 };
    char buf[Limit];
    modp_uitoa10(v, buf);
    return buf;
}

std::string to_string(int64_t v)
{
    // Buffer should be large enough to hold all digits, sign and null char
    enum { Limit = std::numeric_limits<int64_t>::digits10 + 2 };
    char buf[Limit];
    modp_litoa10(v, buf);
    return buf;
}

std::string to_string(uint64_t v)
{
    // Buffer should be large enough to hold all digits and null char
    enum { Limit = std::numeric_limits<uint64_t>::digits10 + 1 };
    char buf[Limit];
    modp_ulitoa10(v, buf);
    return buf;
}

std::string to_string(float v, int prec)
{
    // Buffer should be large enough to hold all digits, sign, decimal and
    // null char
    enum { Limit = std::numeric_limits<float>::digits10 + 3 };
    char buf[Limit];
    modp_dtoa(v, buf, prec);
    return buf;
}

std::string to_string(double v, int prec)
{
    // Buffer should be large enough to hold all digits, sign, decimal and
    // null char
    enum { Limit = std::numeric_limits<double>::digits10 + 3 };
    char buf[Limit];
    modp_dtoa(v, buf, prec);
    return buf;
}

std::string to_string(long double v, int prec)
{
    // Buffer should be large enough to hold all digits, sign, decimal and
    // null char
    enum { Limit = std::numeric_limits<long double>::digits10 + 3 };
    char buf[Limit];
    modp_dtoa(v, buf, prec);
    return buf;
}

std::string utos(uint64_t v, boost::logic::tribool sign)
{
    // Buffer should be large enough to hold all digits, sign and null char.
    enum { Limit = std::numeric_limits<uint64_t>::digits10 + 2 };
    char buf[Limit];
    char *ptr = buf + Limit - 1;
    *ptr = '\0';

    unsigned index = 0;
    // Integer division is slow so do it for a group of two digits
    // instead of for every digit. The idea comes from the talk by
    // Alexandrescu; "Three Optimization Tips for C++".
    // https://www.facebook.com/notes/facebook-engineering/10151361643253920
    while (v >= 100) {
        index = (v % 100) * 2;
        v /= 100;
        *--ptr = DIGITS[index + 1];
        *--ptr = DIGITS[index];
    }

    if (v < 10) {
        *--ptr = static_cast<char>('0' + v);

        if (!boost::logic::indeterminate(sign)) {
            *--ptr = sign ? '+' : '-';
        }

        return std::string(static_cast<const char *>(ptr));
    }

    index = v * 2u;
    *--ptr = DIGITS[index + 1];
    *--ptr = DIGITS[index];

    if (!boost::logic::indeterminate(sign)) {
        *--ptr = sign ? '+' : '-';
    }

    return std::string(static_cast<const char *>(ptr));
}

std::string ftos(long double value, uint8_t prec)
{
    if (!std::isfinite(value)) {
        if (std::isnan(value)) {
            return "NaN";
        } else if (std::isinf(value)) {
            return "INF";
        } else {
            return "!Nan && !INF";
        }
    }

    // We'll work in positive values and deal with the negative later
    bool positive = true;
    if (value < 0.0L) {
        positive = false;
        value = -value;     // make positive
    }

    // If input is larger than thres_max, revert to exponential.
    // For very large numbers switch back to native sprintf for
    // exponentials. Anyone want to write code to replace this?
    //
    // Normal printf behavior is to print EVERY whole number digit
    // which can be 100s of characters overflowing your buffers == bad
    if (value > thres_max) {
        //: FIXME: The following code produces an error.
        enum { Limit = 23 };
        char buf[Limit];
        char *ptr = &buf[0];
        if (positive) {
            sprintf(ptr, "%e", value);
        } else {
            sprintf(ptr, "%e", -value);
        }
        return ptr;
    }

    enum { FRACTION_LENGTH = 9 };
    if (prec > FRACTION_LENGTH) {
        // Precision of >= 10 can lead to overflow errors
        prec = FRACTION_LENGTH;
    }

    // Can we make whole unsigned? Or maybe because int is faster.
    int whole = (int) value;
    double tmp = (value - whole) * pow10[prec];
    uint32_t frac = (uint32_t) (tmp);
    double diff = tmp - frac;

    if (diff > 0.5) {
        ++frac;
        // Handle rollover, e.g.  case 0.99 with prec 1 is 1.0
        if (frac >= pow10[prec]) {
            frac = 0;
            ++whole;
        }
    } else if (diff == 0.5 && ((frac == 0) || (frac & 1))) {
        // If halfway, round up if odd, or if last digit is 0.
        // That last part is strange.
        ++frac;
    }

    // Buffer should be large enough to hold all digits, sign, decimal and
    // null char
    enum { Limit = std::numeric_limits<long double>::digits10 + 3 };
    char buf[Limit];
    char *ptr = buf + Limit - 1;
    *ptr = '\0';

    if (prec == 0) {
        diff = value - whole;
        if (diff > 0.5) {
            // Greater than 0.5, round up, e.g. 1.6 -> 2
            ++whole;
        } else if (diff == 0.5 && (whole & 1)) {
            // Exactly 0.5 and ODD, then round up
            // 1.5 -> 2, but 2.5 -> 2
            ++whole;
        }
    } else {

        // Now do fractional part, as an unsigned number

        if (frac <= pow10[8] - 1) {
            // Add trailing zeroes
            int zeroes = prec - count_digits(frac);
            while (zeroes-- > 0) *--ptr = '0';
        }

        // Integer division is slow so do it for a group of two digits
        // instead of for every digit. The idea comes from the talk by
        // Alexandrescu; "Three Optimization Tips for C++".
        // https://www.facebook.com/notes/facebook-engineering/10151361643253920
        while (frac >= 100) {
            unsigned index = (frac % 100) * 2;
            frac /= 100;
            *--ptr = DIGITS[index + 1];
            *--ptr = DIGITS[index];
        }

        if (frac < 10) {
            *--ptr = static_cast<char>('0' + frac);
        } else {
            unsigned index = static_cast<unsigned>(frac * 2);
            *--ptr = DIGITS[index + 1];
            *--ptr = DIGITS[index];
        }

        // Add decimal point. Move one character before since str points
        // to the first character.
        *--ptr = '.';
    }

    // Do whole part
    // Take care of sign

    // Integer division is slow so do it for a group of two digits
    // instead of for every digit. The idea comes from the talk by
    // Alexandrescu; "Three Optimization Tips for C++".
    // https://www.facebook.com/notes/facebook-engineering/10151361643253920

    while (whole >= 100) {
        unsigned index = (whole % 100) * 2;
        whole /= 100;
        *--ptr = DIGITS[index + 1];
        *--ptr = DIGITS[index];
    }
    if (whole < 10) {
        *--ptr = static_cast<char>('0' + whole);
    } else {
        unsigned index = static_cast<unsigned>(whole * 2);
        *--ptr = DIGITS[index + 1];
        *--ptr = DIGITS[index];
    }

    if (!positive) {
        *--ptr = '-';
    }

    return std::string(static_cast<const char *>(ptr));
}

inline
unsigned get_xdigit_count(uint64_t n, const uint8_t shift)
{
    unsigned digitCount = 0;
    do {
        ++digitCount;
    } while (n >>= shift);
    return digitCount;
}


std::string to_hex(uint64_t n, char symbol)
{
    //. TODO: Add sign
    // Buffer should be large enough to hold all digits, hex prefix symbol '0x'
    // and null char
    enum { Limit = std::numeric_limits<uint64_t>::digits10 + 3 };
    char buf[Limit];
    char *ptr = buf;
    auto num_digits = get_xdigit_count(n, 4);

    // Set hexadecimal prefix
#if 1
    // For string format use
    *ptr++ = '0';
    *ptr++ = symbol;
#else
    if (symbol == 'x' || symbol == 'X' || symbol == 'h') {
        *ptr++ = '0';
        *ptr++ = symbol;
    } else if (symbol == '#') {
        *ptr++ = '#';
    } else if (symbol == '\\') {
        *ptr++ = symbol;
        *ptr++ = 'x';
    } else {
        *ptr++ = '#';
    }
#endif

    // Move to the right
    ptr += num_digits;
    *ptr-- = '\0';

    const char *hex = symbol == 'x' ? "0123456789abcdef" : "0123456789ABCDEF";
    // Convert and move left
    do {
        *ptr-- = hex[n & 0xf];
    } while (n >>= 4);

    ptr = &buf[0];
    return std::string(static_cast<const char *>(ptr));
}

std::string to_bin(uint64_t n)
{
    //. TODO: Add sign
    // Accomodate 64-bits, binary prefix symbol '0b' and null char.
    enum { Limit = std::numeric_limits<uint64_t>::digits + 3 };
    char buf[Limit];
    char *ptr = buf;
    auto num_digits = get_xdigit_count(n, 1);

    // For string format use
    *ptr++ = '0';
    *ptr++ = 'b';

    // Move to the right
    ptr += num_digits;
    *ptr-- = '\0';

    do {
        *ptr-- = '0' + (n & 1);
    } while (n >>= 1);

    ptr = &buf[0];
    return std::string(static_cast<const char *>(ptr));
}

std::string to_octal(uint64_t n)
{
    //. TODO: Add sign
    // Buffer should be large enough to hold all digits, octal prefix symbol 'o'
    // and null char
    enum { Limit = 23 + 2 };
    char buf[Limit];
    char *ptr = buf;
    auto num_digits = get_xdigit_count(n, 3);

    // For string format use
    *ptr++ = 'o';

    // Move to the right
    ptr += num_digits;
    *ptr-- = '\0';

    do {
        *ptr-- = '0' + (n & 7);
    } while (n >>= 3);

    ptr = &buf[0];
    return std::string(static_cast<const char *>(ptr));
}

} //_ namespace string
} //_ namespace negerns


// -----------------------------------------------------------------------------


namespace negerns {
namespace string {
namespace boolean {

StringPair no_bool_alpha()
{
    return std::make_pair("1", "0");
}

StringPair bool_alpha_default()
{
    return std::make_pair("true", "false");
}

StringPair bool_alpha(Case c)
{
    static std::string tval = "true";
    static std::string fval = "false";
    std::string trueValue(c == Case::None ? tval : to_case(tval, c));
    std::string falseValue(c == Case::None ? fval : to_case(fval, c));
    return std::make_pair(trueValue, falseValue);
}

StringPair yes_no(Case c)
{
    static std::string tval = "yes";
    static std::string fval = "no";
    std::string trueValue(c == Case::None ? tval : to_case(tval, c));
    std::string falseValue(c == Case::None ? fval : to_case(fval, c));
    return std::make_pair(trueValue, falseValue);
}

StringPair success_failed(Case c)
{
    static std::string tval = "success";
    static std::string fval = "failed";
    std::string trueValue(c == Case::None ? tval : to_case(tval, c));
    std::string falseValue(c == Case::None ? fval : to_case(fval, c));
    return std::make_pair(trueValue, falseValue);
}

StringPair custom(const std::string &tval, const std::string &fval, Case c)
{
    std::string trueValue(c == Case::None ? tval : to_case(tval, c));
    std::string falseValue(c == Case::None ? fval : to_case(fval, c));
    return std::make_pair(trueValue, falseValue);
}

} //_ namespace boolean
} //_ namespace string
} //_ namespace negerns
