#include <negerns/core/debug.h>
#include <negerns/core/output.h>

namespace negerns {

bool Output::initialized = false;

Output::OutputImp* Output::Implementation(Output::OutputImp *p)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static Output::OutputImp*instance = nullptr;

    if (p != nullptr && instance == nullptr) {
        // Assignment
        instance = p;
        return nullptr;
    } else if (p == nullptr && instance != nullptr) {
        // Query
        return instance;
    } else if (p != nullptr && instance != nullptr) {
        // Reassignment
        FAIL("Attempt to reassign a value to a single-instance object.");
        return nullptr;
    } else if (p == nullptr && instance == nullptr) {
        // Possible query or an attempt to initialize object with a null value.
        // But we do not allow initialization to null, so, this must be a query
        // returning the uninitialized value (nullptr).
        return nullptr;
    } else {
        throw std::runtime_error("Should not execute this code.");
    }
}

Output::Output()
{
    // Create implementation object on first instance of this class.
    // This allows output messages to be buffered.
    if (Output::Implementation() == nullptr) {
        Output::Implementation(new Output::OutputImp());
    }
}

void Output::Init(OutputSink *sink)
{
    auto *ptr = Output::Implementation();
    ASSERT(ptr, "Output implementation is null.\n"
        "Create a Log object first to initialize the implementation object.");

    ptr->sink = sink;
    ptr->sink->Init();
    if (ptr->buffer.size()) {
        for (auto &msg : ptr->buffer) {
            ptr->sink->Send(msg);
        }
        ptr->sink->Send("");
        ptr->buffer.clear();
    }
}

void Output::Shutdown()
{
    auto *ptr = Output::Implementation();
    if (ptr != nullptr) {
        ptr->sink->Shutdown();
        // IMPORTANT:
        //
        // Do not delete output control and therefore also the implementation
        // instance. This causes the application to hang. Let wxWidgets destroy
        // the output control as it advised.
#if 0
        negerns::reset(ptr->sink);
        negerns::reset(ptr);
#endif
    }
}

void Output::Send()
{
    auto *ptr = Output::Implementation();
    if (ptr != nullptr) {
        ptr->sink->Send("");
    }
}

void Output::SendThis(const std::string &s)
{
    auto *ptr = Output::Implementation();
    if (ptr != nullptr) {
        ptr->sink->Send(s);
    }
}

void Output::Clear()
{
    auto *ptr = Output::Implementation();
    if (ptr != nullptr) {
        ptr->sink->Clear();
    }
}

} //_ namespace negerns
