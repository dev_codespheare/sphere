#include <Poco/DateTimeFormat.h>
#include <Poco/DateTimeParser.h>
#include <Poco/DateTimeFormatter.h>
#include <Poco/Exception.h>
#include <negerns/core/debug.h>
#include <negerns/core/datetime.h>

bool operator == (const std::tm &lhs, const std::tm &rhs)
{
    auto left = negerns::date::to_time(lhs);
    auto right = negerns::date::to_time(rhs);
    auto diff = std::difftime(left, right);
    return diff == 0.00;
}

bool operator != (const std::tm &lhs, const std::tm &rhs)
{
    auto left = negerns::date::to_time(lhs);
    auto right = negerns::date::to_time(rhs);
    auto diff = std::difftime(left, right);
    return diff != 0.00;
}



// ----------------------------------------------------------------------------



namespace negerns {
namespace date {

std::tm now()
{
    std::time_t rawtime;
    std::time(&rawtime);
    return *std::localtime(&rawtime);
}

std::vector<std::string> get_month_names(MonthName name)
{
    std::vector<std::string> names;
    int m;
    wxString s;
    for (m = wxDateTime::Month::Jan; m < wxDateTime::Month::Inv_Month; ++m) {
        s = wxDateTime::GetMonthName(wxDateTime::Month(m),
            wxDateTime::NameFlags(static_cast<int>(name)));
        names.push_back(s.ToStdString());
    }
    return names;
}

bool to_tm(const std::string &s, std::tm &r, const char *fmt)
{
    wxDateTime dt;
    bool status = dt.ParseFormat(s, fmt);
    ASSERT(status, "Parsing Date/Time string error.");
    auto wxtm = dt.GetTm();
    ASSERT(wxtm.IsValid(), "Invalid Date/Time structure.");
    r.tm_sec = wxtm.sec;
    r.tm_min = wxtm.min;
    r.tm_hour = wxtm.hour;
    r.tm_mday = wxtm.mday;
    r.tm_mon = static_cast<int>(wxtm.mon);
    r.tm_year = wxtm.year;

    //r.tm_wday == wxtm.tm_wday;
    //r.tm_yday == wxtm.tm_yday;
    //r.tm_isdst == wxtm.tm_isdst;
    return status;
}

std::tm to_tm(const std::string &s, const char *fmt)
{
    std::tm t;
    to_tm(s, t, fmt);
    return t;
}

std::tm to_tm(const std::time_t &t)
{
    return *std::localtime(&t);
}

std::tm to_tm(const wxDateTime &dt)
{
    wxDateTime::Tm d = dt.GetTm();
    std::tm t;
    t.tm_sec = d.sec;
    t.tm_min = d.min;
    t.tm_hour = d.hour;
    t.tm_mday = d.mday;
    t.tm_mon = static_cast<int>(d.mon);
    t.tm_year = d.year;
    return t;
}

std::string to_string(const std::tm &t, const char *fmt)
{
    const int max = 20;
    char buf[max];
    strftime(buf, max, fmt, &t);
    return std::string(buf);
}

std::time_t to_time(const std::tm &t)
{
    std::tm time = t;
    return std::mktime(&time);
}

wxDateTime to_wxdatetime(const std::tm &t)
{
    return wxDateTime(t);
}



// ----------------------------------------------------------------------------------------



Date::Date(const std::tm &t) :
    dt(t)
{ }

Date::Date(const std::time_t &t) :
    dt(*std::localtime(&t))
{ }

Date::~Date() { }

bool Date::operator == (const std::tm &rhs)
{
    auto left = date::to_time(dt);
    auto right = date::to_time(rhs);
    auto diff = std::difftime(left, right);
    return diff == 0.00;
}

bool Date::operator == (const std::time_t &rhs)
{
    auto left = date::to_time(dt);
    auto diff = std::difftime(left, rhs);
    return diff == 0.00;
}

bool Date::operator == (const date::Date &rhs)
{
    auto left = date::to_time(dt);
    auto right = date::to_time(rhs.dt);
    auto diff = std::difftime(left, right);
    return diff == 0.00;
}

bool Date::operator < (const std::tm &rhs)
{
    auto left = date::to_time(dt);
    auto right = date::to_time(rhs);
    auto diff = std::difftime(left, right);
    return diff < 0.00;
}

bool Date::operator < (const std::time_t &rhs)
{
    auto left = date::to_time(dt);
    auto diff = std::difftime(left, rhs);
    return diff < 0.00;
}

bool Date::operator < (const Date &rhs)
{
    auto left = date::to_time(dt);
    auto right = date::to_time(rhs.dt);
    auto diff = std::difftime(left, right);
    return diff < 0.00;
}

bool Date::operator > (const std::tm &rhs)
{
    auto left = date::to_time(dt);
    auto right = date::to_time(rhs);
    auto diff = std::difftime(left, right);
    return diff > 0.00;
}

bool Date::operator > (const std::time_t &rhs)
{
    auto left = date::to_time(dt);
    auto diff = std::difftime(left, rhs);
    return diff > 0.00;
}

bool Date::operator > (const Date &rhs)
{
    auto left = date::to_time(dt);
    auto right = date::to_time(rhs.dt);
    auto diff = std::difftime(left, right);
    return diff > 0.00;
}

Date & Date::now()
{
    std::time_t rawtime;
    std::time(&rawtime);
    dt = *std::localtime(&rawtime);
    return *this;
}

void Date::set_now()
{
    std::time_t rawtime;
    std::time(&rawtime);
    dt = *std::localtime(&rawtime);
}

} //_ namespace date
} //_ namespace negerns



// ----------------------------------------------------------------------------------------


namespace negerns {

void DateTime::Default(poco::Timestamp &ts)
{
    ts = poco::Timestamp::fromEpochTime(0);
}

void DateTime::Default(poco::DateTime &dt)
{
    poco::Timestamp pts(poco::Timestamp::fromEpochTime(0));
    dt = pts;
}

void DateTime::Default(wxDateTime &dt)
{
    poco::Timestamp pts(poco::Timestamp::fromEpochTime(0));
    dt.Set(pts.epochTime());
}

bool DateTime::Convert(const poco::Timestamp &pts, wxDateTime &wxdt)
{
    std::string src(DateTime::ToString(pts));
    bool status = DateTime::Convert(src, wxdt);
    ASSERT(wxdt.IsValid(), "DateTime conversion from Poco Timestamp to "
        "wxDateTime failed.");
    return status;
}

bool DateTime::Convert(const poco::DateTime &pdt, wxDateTime &wxdt)
{
    std::string src(DateTime::ToString(pdt));
    bool status = DateTime::Convert(src, wxdt);
    ASSERT(wxdt.IsValid(), "DateTime conversion from Poco DateTime to "
        "wxDateTime failed.");
    return status;
}

bool DateTime::Convert(const wxDateTime &wxdt, poco::Timestamp &pts)
{
    poco::DateTime pdt;
    bool status = DateTime::Convert(wxdt, pdt);
    pts = pdt.timestamp();
    return status;
}

bool DateTime::Convert(const wxDateTime &wxdt, poco::DateTime &pdt)
{
    ASSERT(wxdt.IsValid(), "wxDateTime is invalid.");
    std::string src(wxdt.Format(NEGERNS_DATETIME_FORMAT));
    return DateTime::Convert(src, pdt);
}

bool DateTime::Convert(const wxDateTime &wxdt, poco::data::Date &pddt)
{
    poco::DateTime pdt;
    DateTime::Convert(wxdt, pdt, NEGERNS_DATE_FORMAT);
    //poco::data::Date date(pdt);
    pddt = pdt;
    return true;
}

bool DateTime::Convert(const wxDateTime &wxdt, poco::DateTime &pdt, const std::string &fmt)
{
    std::string tmp = DateTime::ToString(wxdt, fmt);
    return DateTime::Convert(tmp, pdt, fmt);
}

bool DateTime::Convert(const std::string &source, poco::Timestamp &ts, const std::string &fmt)
{
    poco::DateTime pdt;
    bool status = DateTime::Convert(source, pdt, fmt);
    ts = pdt.timestamp();
    return status;
}

bool DateTime::Convert(const std::string &source, poco::DateTime &dt, const std::string &fmt)
{
    bool status = false;
    ASSERT(fmt.length() > 0, "Format string not specified.");
    try {
        int tzdiff;
        dt = poco::DateTimeParser::parse(fmt, source, tzdiff);
        status = true;
    } catch (poco::SyntaxException ex) {
        ASSERT(false, ex.displayText());
    }
    return status;
}

bool DateTime::Convert(const std::string &source, wxDateTime &dt, const std::string &fmt)
{
    return dt.ParseFormat(source, fmt);
}

std::string DateTime::ToString(const poco::Timestamp &ts, const std::string &fmt)
{
    return poco::DateTimeFormatter::format(ts, fmt);
}

std::string DateTime::ToString(const poco::DateTime &dt, const std::string &fmt)
{
    return poco::DateTimeFormatter::format(dt, fmt);
}

std::string DateTime::ToString(const poco::data::Date &dt, const std::string &fmt)
{
    unsigned short day = dt.day();
    wxDateTime::Month month = static_cast<wxDateTime::Month>(dt.month() - 1);
    int year = dt.year();
    wxDateTime wxdt(day, month, year);
    return wxdt.Format(fmt).ToStdString();
}

std::string DateTime::ToString(const wxDateTime &dt, const std::string &fmt)
{
    return dt.Format(fmt).ToStdString();
}

std::string DateTime::ToISODate(const wxDateTime &dt)
{
    return dt.FormatISODate().ToStdString();
}

std::string DateTime::ToISODate(const poco::Timestamp &ts)
{
    return poco::DateTimeFormatter::format(ts, "%Y-%m-%d");
}

std::string DateTime::ToISODate(const poco::DateTime &dt)
{
    return poco::DateTimeFormatter::format(dt, "%Y-%m-%d");
}

std::string DateTime::ToISOTime(const wxDateTime &dt)
{
    return dt.FormatISOTime().ToStdString();
}

std::string DateTime::ToISOTime(const poco::Timestamp &ts)
{
    return poco::DateTimeFormatter::format(ts, "%H:%M:%S");
}

std::string DateTime::ToISOTime(const poco::DateTime &dt)
{
    return poco::DateTimeFormatter::format(dt, "%H:%M:%S");
}

#if 0
bool DateTime::ConvertDate(const poco::DateTime &pdt, wxDateTime &wxdt, const std::string &fmt)
{
    std::string s(poco::DateTimeFormatter::format(pdt, fmt));
    return wxdt.ParseISOCombined(s);
}

bool DateTime::ConvertDate(const wxDateTime &wxdt, poco::DateTime &pdt, const std::string &fmt)
{
    std::string s(wxdt.FormatISOCombined());
    int timeZoneDifferential = 0;
    bool status = false;
    try {
        pdt = poco::DateTimeParser::parse(fmt, s, timeZoneDifferential);
        status = true;
    } catch (poco::SyntaxException ex) {
        wxASSERT_MSG(false, ex.displayText());
    }
    return status;
}
#endif

std::vector<std::string> DateTime::GetMonthNames(Name name)
{
    std::vector<std::string> month_names;
    int m;
    wxString s;
    for (m = wxDateTime::Month::Jan; m < wxDateTime::Month::Inv_Month; ++m) {
        s = wxDateTime::GetMonthName(wxDateTime::Month(m), wxDateTime::NameFlags(static_cast<int>(name)));
        month_names.push_back(s.ToStdString());
    }
    return month_names;
}

} //_ namespace negerns
