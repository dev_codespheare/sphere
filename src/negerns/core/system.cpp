#include <negerns/gui/app.h>
#include <negerns/core/debug.h>
#include <negerns/core/event.h>
#include <negerns/core/output.h>
#include <negerns/core/configuration.h>
#include <negerns/core/system.h>
#include <negerns/data/dataaccess.h>

namespace negerns {

gui::Application * System::Application(gui::Application *p)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html

    // NOTE: Application instance must not be managed by a smart pointer.
    //       wxWidgets will take care of it.
    static gui::Application *instance = nullptr;

    if (p != nullptr && instance == nullptr) {
        // Assignment
        instance = p;
        return nullptr;
    } else if (p == nullptr && instance != nullptr) {
        // Query
        return instance;
    } else if (p != nullptr && instance != nullptr) {
        // Reassignment
        throw std::runtime_error("Object reinitialization.");
    } else if (p == nullptr && instance == nullptr) {
        // Possible query or an attempt to initialize object with a null value.
        // But we do not allow initialization to null, so, this must be a query
        // returning the uninitialized value (nullptr).
        return nullptr;
    } else {
        throw std::runtime_error("Should not execute this code.");
    }
}

negerns::Configuration* System::Configuration(negerns::Configuration *p)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static std::unique_ptr<negerns::Configuration> instance = nullptr;

    if (p != nullptr && instance == nullptr) {
        // Assignment
        instance.reset(p);
        return nullptr;
    } else if (p == nullptr && instance != nullptr) {
        // Query
        return instance.get();
    } else if (p != nullptr && instance != nullptr) {
        // Reassignment
        throw std::runtime_error("Object reinitialization.");
    } else if (p == nullptr && instance == nullptr) {
        // Possible query or an attempt to initialize object with a null value.
        // But we do not allow initialization to null, so, this must be a query
        // returning the uninitialized value (nullptr).
        return nullptr;
    } else {
        throw std::runtime_error("Should not execute this code.");
    }
}

EventManager* System::EventManager(negerns::EventManager *p)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static std::unique_ptr<negerns::EventManager> instance = nullptr;

    if (p != nullptr && instance == nullptr) {
        // Assignment
        instance.reset(p);
        return nullptr;
    } else if (p == nullptr && instance != nullptr) {
        // Query
        return instance.get();
    } else if (p != nullptr && instance != nullptr) {
        // Reassignment
        throw std::runtime_error("Object reinitialization.");
    } else if (p == nullptr && instance == nullptr) {
        // Possible query or an attempt to initialize object with a null value.
        // But we do not allow initialization to null, so, this must be a query
        // returning the uninitialized value (nullptr).
        return nullptr;
    } else {
        throw std::runtime_error("Should not execute this code.");
    }
}

gui::Pane* System::Pane(std::size_t index)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    //static negerns::gui::Pane *instance = nullptr;

    auto *app = System::Application();
    auto *fm = app->GetFrameManager();
    ASSERT(fm->GetPaneCount() > 0, "No Pane object registered.");
    ASSERT(fm->GetPaneCount() > index, "Specified Pane index does not exist.");
    return fm->gui::manager::Pane::GetPane(index);
}

void System::set_loading_state(boost::logic::tribool b)
{
    // Application cannot be both in the loading state and exit state.
    // If we are in the application loading state, make it clear that the
    // application is not exiting.

    bool exitState = System::is_exiting();
// Warning 4800: forcing value to bool 'true' or 'false' (performace warning)
#pragma warning(disable:4800)
    // Development guide
    ASSERT(exitState && b == true, "Application canot be both in the loading and exit state.");
#pragma warning(default:4800)
    if (b) {
        System::set_exit_state(false);
    }
    // Set the state using the argument
    (void) System::is_loading(b);
}

bool System::is_loading(boost::logic::tribool b)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static bool loadingState = false;
    if (boost::logic::indeterminate(b)) {
        return loadingState;
    } else {
        loadingState = b;
        // TODO: Check if this is what we really wanted.
        return loadingState;
    }
}

void System::set_exit_state(boost::logic::tribool b)
{
    // Application cannot be both in the loading state and exit state.
    // If we are exiting the application, make it explicit that application
    // is not in the loading state.

    bool loadingState = System::is_loading();
// Warning 4800: forcing value to bool 'true' or 'false' (performace warning)
#pragma warning(disable:4800)
    // Development guide
    ASSERT(loadingState && b, "Application canot be both in the loading and exit state.");
#pragma warning(default:4800)
    if (b) {
        System::set_loading_state(false);
    }
    // Set the state using the argument
    (void) System::is_exiting(b);
}

bool System::is_exiting(boost::logic::tribool b)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static bool exitState = false;
    if (boost::logic::indeterminate(b)) {
        return exitState;
    } else {
        exitState = b;
        // TODO: Check if this is what we really wanted.
        return exitState;
    }
}

} //_ namespace negerns
