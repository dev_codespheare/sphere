#include <cctype>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/output.h>
#include <negerns/core/string.h>
#include <negerns/core/filesystem.h>
#include <negerns/core/config/property.h>

namespace negerns {
namespace config {

Property::Property() : Base() { }
Property::Property(const std::string &filename) : Base(filename) { }
Property::Property(const std::string &filename, bool is_updateable) :
    Base(filename, is_updateable)
{ }

Property::~Property()
{
    save();
}

void Property::read()
{
    static const int eof = std::char_traits<char>::eof();

    std::string filename(Base::get_filename());
    if (!filesystem::exists(filename)) {
        log::warning("Configuration file does not exist %1.", filename);
        return;
    }
    auto is = std::ifstream(filename);
    if (!is) {
        std::string msg(Format("Could not open ini file: %1.", filename));
        Output::Send(msg);
        FAIL(msg);
        return;
    }

    keyvalues.clear();
    std::string key;
    std::string value;
    while (!is.eof()) {
	    int c = is.get();
	    while (c != eof && std::isspace((char) c)) {
            c = is.get();
        }
	    if (c != eof) {
		    if (c == '#' || c == '!') {
			    while (c != eof && c != '\n' && c != '\r') {
                    c = is.get();
                }
		    } else {
			    //std::string key;
                key.clear();
			    while (c != eof && c != '=' && c != ':' && c != '\r' && c != '\n') {
                    key += (char) c; c = is.get();
                }
			    //std::string value;
                value.clear();
			    if (c == '=' || c == ':') {
				    c = read_char(is);
				    while (c != eof && c) {
                        value += (char) c;
                        c = read_char(is);
                    }
			    }

                keyvalues.emplace(std::make_pair(string::trim(key),
                    string::trim(value)));
		    }
	    }
	}
#ifdef _DEBUG
    for (auto &item : keyvalues) {
        log::debug("  Item: %1 = %2", item.first, item.second);
    }
#endif
}

void Property::save()
{
    if (Base::is_updateable()) {

    }
}

int Property::read_char(std::istream& is)
{
	for (;;) {
		int c = is.get();
		if (c == '\\') {
			c = is.get();
			switch (c) {
			case 't':
				return '\t';
			case 'r':
				return '\r';
			case 'n':
				return '\n';
			case 'f':
				return '\f';
			case '\r':
				if (is.peek() == '\n')
					is.get();
				continue;
			case '\n':
				continue;
			default:
				return c;
			}
		}
		else if (c == '\n' || c == '\r')
			return 0;
		else
			return c;
	}
}

} //_ namespace config
} //_ namespace negerns
