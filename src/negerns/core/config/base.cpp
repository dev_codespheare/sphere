#include <boost/spirit/include/qi_numeric.hpp>
#include <negerns/core/debug.h>
#include <negerns/core/config/base.h>

namespace negerns {
namespace config {

Base::Base() :
    filename(std::string()), updateable(false)
{ }

Base::Base(const std::string &f) :
    filename(f), updateable(false)
{ }

Base::Base(const std::string &f, bool is_updateable) :
    filename(f), updateable(is_updateable)
{
    ASSERT(!filename.empty(), "Filename cannot be empty.");
}

Base::~Base() { }

bool Base::find(const std::string &key, KeyValues::iterator &pos)
{
    ASSERT(!key.empty(), "Key cannot be empty.");
    pos = keyvalues.find(key);
    return pos != keyvalues.end();
}

bool Base::has_key(const std::string& key)
{
    ASSERT(!key.empty(), "Key cannot be empty.");
    return keyvalues.empty() ? false : keyvalues.count(key) > 0;
}

bool Base::get_bool(const std::string& key, const bool def)
{
    ASSERT(!key.empty(), "Key cannot be empty.");
    auto pos = keyvalues.find(key);
    if (pos != keyvalues.end()) {
        std::string s(string::to_lower(pos->second));
        if (s == "true" || s == "yes" || s == "on") {
            return true;
        }
        if (s == "false" || s == "no" || s == "off") {
            return false;
        }
        unsigned long n;
        return string::stoul(s, n) ? n > 0 : def;
    } else {
        return def;
    }
}

std::string Base::get_string(const std::string& key, const std::string &def)
{
    ASSERT(!key.empty(), "Key cannot be empty.");
    auto pos = keyvalues.find(key);
    return pos != keyvalues.end() ? pos->second : def;
}

int Base::get_int(const std::string& key, const int def)
{
    ASSERT(!key.empty(), "Key cannot be empty.");
    auto pos = keyvalues.find(key);
    if (pos != keyvalues.end()) {
        int n = 0;
        return string::stoi(std::string(pos->second), n) ? n : def;
    } else {
        return def;
    }
}

unsigned int Base::get_uint(const std::string& key, const unsigned int def)
{
    ASSERT(!key.empty(), "Key cannot be empty.");
    auto pos = keyvalues.find(key);
    if (pos != keyvalues.end()) {
        unsigned int n = 0;
        return string::stoui(std::string(pos->second), n) ? n : def;
    } else {
        return def;
    }
}

double Base::get_double(const std::string& key, const double def)
{
    ASSERT(!key.empty(), "Key cannot be empty.");
    auto pos = keyvalues.find(key);
    if (pos != keyvalues.end()) {
        double n = 0.0;
        try {
            n = std::stod(std::string(pos->second));
        } catch (const std::invalid_argument &e) {
            return def;
        }
        return n;
    } else {
        return def;
    }
}

bool Base::set(const std::string& key, bool value)
{
    ASSERT(!key.empty(), "Key cannot be empty.");
    auto pos = keyvalues.find(key);
    if (pos != keyvalues.end()) {
        pos->second = value;
        return true;
    } else {
        return false;
    }
}

bool Base::set(const std::string& key, const std::string& value)
{
    ASSERT(!key.empty(), "Key cannot be empty.");
    auto pos = keyvalues.find(key);
    if (pos != keyvalues.end()) {
        pos->second = value;
        return true;
    } else {
        return false;
    }
}

bool Base::set(const std::string& key, int value)
{
    ASSERT(!key.empty(), "Key cannot be empty.");
    auto pos = keyvalues.find(key);
    if (pos != keyvalues.end()) {
        pos->second = value;
        return true;
    } else {
        return false;
    }
}

bool Base::set(const std::string& key, unsigned int value)
{
    ASSERT(!key.empty(), "Key cannot be empty.");
    auto pos = keyvalues.find(key);
    if (pos != keyvalues.end()) {
        pos->second = value;
        return true;
    } else {
        return false;
    }
}

bool Base::set(const std::string& key, double value)
{
    ASSERT(!key.empty(), "Key cannot be empty.");
    auto pos = keyvalues.find(key);
    if (pos != keyvalues.end()) {
        pos->second = value;
        return true;
    } else {
        return false;
    }
}

} //_ namespace config
} //_ namespace negerns
