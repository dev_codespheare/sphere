#include <cctype>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/output.h>
#include <negerns/core/string.h>
#include <negerns/core/filesystem.h>
#include <negerns/core/config/ini.h>

namespace negerns {
namespace config {

Ini::Ini() : Base() { }
Ini::Ini(const std::string &filename) : Base(filename) { }
Ini::Ini(const std::string &filename, bool is_updateable) :
    Base(filename, is_updateable)
{ }

Ini::~Ini()
{
    save();
}

void Ini::read()
{
    static const int eof = std::char_traits<char>::eof();

    std::string filename(Base::get_filename());
    if (!filesystem::exists(filename)) {
        log::warning("Configuration file does not exist %1.", filename);
        return;
    }

    auto is = std::ifstream(filename);
    if (!is) {
        std::string msg(Format("Could not open ini file: %1.", filename));
        Output::Send(msg);
        FAIL(msg);
        return;
    }

    keyvalues.clear();
    section.clear();
    std::string key;
    std::string value;
    while (!is.eof()) {
	    int c = is.get();
	    while (c != eof && std::isspace((char) c)) {
            c = is.get();
        }
	    if (c != eof) {
		    if (c == ';') {
			    while (c != eof && c != '\n') c = is.get();
		    } else if (c == '[') {
			    // std::string key;
                key.clear();
			    c = is.get();
			    while (c != eof && c != ']' && c != '\n') {
                    key += (char) c;
                    c = is.get();
                }
			    section = string::trim(key);
		    } else {
			    // std::string key;
                key.clear();
			    while (c != eof && c != '=' && c != '\n') {
                    key += (char) c;
                    c = is.get();
                }
			    // std::string value;
                value.clear();
			    if (c == '=') {
				    c = is.get();
				    while (c != eof && c != '\n') {
                        value += (char) c;
                        c = is.get();
                    }
			    }
			    std::string fullKey = section;
			    if (!fullKey.empty()) fullKey += '.';
			    fullKey.append(string::trim(key));
                keyvalues.emplace(std::make_pair(fullKey, string::trim(value)));
		    }
	    }
	}
#ifdef _DEBUG
    for (auto &item : keyvalues) {
        log::debug("  Item: %1 = %2", item.first, item.second);
    }
#endif
}

void Ini::save()
{
    if (Base::is_updateable()) {

    }
}

} //_ namespace config
} //_ namespace negerns
