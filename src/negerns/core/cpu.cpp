#include <negerns/core/cpu.h>

namespace negerns {

CPUInfo::CPUInfo()
{
#ifdef __WXMSW__
    GetWindowsCPUInfo();
#endif
}

#ifdef __WXMSW__

void CPUInfo::GetWindowsCPUInfo()
{
    char brandString[0x40];
    int CPUInfo[4] = {-1};

    // Calling __cpuid with 0x80000000 as the InfoType argument
    // gets the number of valid extended IDs.
    __cpuid(CPUInfo, 0x80000000);
    unsigned nExIds = CPUInfo[0];
    memset(brandString, 0, sizeof(brandString));

    // Get the information associated with each extended ID.
    for (unsigned i = 0x80000000; i <= nExIds; ++i)
    {
        __cpuid(CPUInfo, i);
        /*
        printf_s("\nFor InfoType %x\n", i); 
        printf_s("CPUInfo[0] = 0x%x\n", CPUInfo[0]);
        printf_s("CPUInfo[1] = 0x%x\n", CPUInfo[1]);
        printf_s("CPUInfo[2] = 0x%x\n", CPUInfo[2]);
        printf_s("CPUInfo[3] = 0x%x\n", CPUInfo[3]);
        */

        // Interpret CPU brand string and cache information.
        if  (i == 0x80000002)
            memcpy(brandString, CPUInfo, sizeof(CPUInfo));
        else if  (i == 0x80000003)
            memcpy(brandString + 16, CPUInfo, sizeof(CPUInfo));
        else if  (i == 0x80000004)
            memcpy(brandString + 32, CPUInfo, sizeof(CPUInfo));
        else if  (i == 0x80000006)
        {
            //nCacheLineSize = CPUInfo[2] & 0xff;
            //nL2Associativity = (CPUInfo[2] >> 12) & 0xf;
            //nCacheSizeK = (CPUInfo[2] >> 16) & 0xffff;
        }
    }
    wxString tmp(brandString);
    CPUBrandString = tmp.Trim(false);

    SYSTEM_INFO sysInfo;
    GetSystemInfo(&sysInfo);
    processorCount = sysInfo.dwNumberOfProcessors;
#if EXCLUDE_TEST
    architecture = sysInfo.wProcessorArchitecture;
#endif
    level = sysInfo.wProcessorLevel;
}

#endif

} //_ namespace negerns
