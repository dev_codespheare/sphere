#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <cstdint>
#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/message.h>
#include <negerns/gui/component.h>
#include <negerns/core/event.h>

namespace negerns {

EventData::EventData() { }

EventData::EventData(negerns::data::Row r) :
    row(r)
{ }

EventData::~EventData() { }

void EventData::SetRow(std::size_t i, negerns::data::Row r)
{
    rowIndex = i;
    row = r;
}

std::size_t EventData::GetIndex() const
{
    return rowIndex;
}

negerns::data::Row & EventData::GetRow()
{
    return row;
}



Event::Event(event::ContextId c, event::Type t) :
    context(c),
    type(t),
    func(nullptr)
{ }

Event::Event(event::ContextId c, event::Type t, EventFunction f) :
    context(c),
    type(t),
    func(std::forward<EventFunction>(f))
{ }


Event::~Event() { }

void Event::operator()(EventData *data)
{
    func(data);
}



EventManager::EventManager() { }

EventManager::~EventManager() { }

#if INCLUDE_EXPERIMENT
bool EventManager::IsContextValid(event::ContextId c)
{
    return (Count(c) == 0);
}

bool EventManager::ContextExists(event::ContextId c)
{
    return (Count(c) > 0);
}
#endif

void EventManager::Set(event::ContextId c, event::Type t)
{
    context = c;
    type = t;
}

size_t EventManager::Add(event::ContextId c, event::Type t)
{
    LOG_FUNCTION();
    if (c > highestContext) {
        highestContext = c;
    }
    auto *e = new Event(c, t);
    events.push_back(e);
    return events.size() - 1;
}

size_t EventManager::Add(event::ContextId c, event::Type t, EventFunction f)
{
    LOG_FUNCTION();
    if (c > highestContext) {
        highestContext = c;
    }
    auto *e = new Event(c, t, std::forward<EventFunction>(f));
    events.push_back(e);
    return events.size() - 1;
}

size_t EventManager::Add(EventFunction f)
{
    LOG_FUNCTION();
    if (context > highestContext) {
        highestContext = context;
    }
    auto *e = new Event(context, type, std::forward<EventFunction>(f));
    events.push_back(e);
    return events.size() - 1;
}

void EventManager::Remove(event::ContextId c)
{
    LOG_FUNCTION();
    auto pos = std::remove_if(events.begin(), events.end(),
        [c](const Event* e) {
        return (c == e->context);
    });
    if (pos != std::end(events)) {
        events.erase(pos, events.end());
    }
}

void EventManager::Remove(event::ContextId c, event::Type t)
{
    LOG_FUNCTION();
    auto pos = std::remove_if(events.begin(), events.end(),
        [c, t](const Event* e) {
        return (c == e->context && t == e->type);
    });
    if (pos != std::end(events)) {
        events.erase(pos, events.end());
    }
}

void EventManager::Remove(EventPositions &positions)
{
    LOG_FUNCTION();
    std::sort(positions.rbegin(), positions.rend());
    for (auto n : positions) {
        events.erase(events.begin() + n);
    }
}

void EventManager::Clear()
{
    events.clear();
}

void EventManager::Call(event::ContextId c, event::Type t, negerns::EventData *data)
{
    LOG_FUNCTION();
    // It is not an error if there is no event handler so do not assert.
    if (Count(c) == 0) {
        return;
    }
    ASSERT(t > event::None && t < event::Max, "EventType out of range.");

    auto pos = std::remove_if(events.begin(), events.end(),
        [c, t](Event* e) { return (c == e->context && t == e->type);
    });

    if (pos != events.end()) {
        std::for_each(pos, events.end(), [data](Event* e) {
            e->operator()(data);
        });
    }
}

void EventManager::Pass(event::ContextId c, EventData *data)
{
    LOG_FUNCTION();
    // It is not an error if there is no event handler so do not assert.
    if (Count(c) == 0) {
        return;
    }
    auto type = event::Type::DataReceive;
    for (auto *e : events) {
        if (c == e->context && type == e->type) {
            e->operator()(data);
        }
    }
}

size_t EventManager::Count()
{
    return events.size();
}

size_t EventManager::Count(event::ContextId c)
{
    return std::count_if(events.begin(), events.end(), [c](const Event* e) {
        return (c == e->context);
    });
}

size_t EventManager::Count(event::ContextId c, event::Type t)
{
    return std::count_if(events.begin(), events.end(), [c, t](const Event* e) {
        return (c == e->context && t == e->type);
    });
}

event::ContextId EventManager::GetNextContextID()
{
    unsigned char pass = 0;
    while (Count(highestContext)) {
        if (highestContext < UINT_MAX) {
            highestContext++;
        } else {
            highestContext = 0;
            pass++;
        }
        if (pass > 1) {
            log::error("EventManager::GetNextContextID is exhausted.");
            highestContext = 0;
        }
    }
    return highestContext;
}

void EventManager::Log()
{
    LOG_FUNCTION();
    auto count = events.size();
    log::info("Events: %1", count);
    for (auto *e : events) {
        log::info("Context: %1, Type: %2", e->context, e->type);
    }
}

} //_ namespace negerns
