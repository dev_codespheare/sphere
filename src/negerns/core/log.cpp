#include <negerns/core/memory.h>
#include <negerns/core/debug.h>
#include <negerns/core/log/ilogger.h>
#if 0
#include <negerns/core/log/poco.h>
#endif
#include <negerns/core/log/boost.h>
#include <negerns/core/log.h>

namespace negerns {
namespace log {
namespace internal {

static std::map<Priority, std::string> LogPrefix {
    { Priority::Trace,       "       T: " },
    { Priority::Debug,       "   Log D: " },
    { Priority::Information, "   Log I: " },
    { Priority::Notice,      "*  Log N: " },
    { Priority::Warning,     "*  Log W: " },
    { Priority::Error,       "** Log E: " },
    { Priority::Critical,    "** Log C: " },
    { Priority::Fatal,       "** Log F: " }
};

bool enable_state(boost::logic::tribool b)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static bool enabled = true;

    if (boost::logic::indeterminate(b)) {
        // Query
        return enabled;
    } else {
        // Assignment
        bool old_value(enabled);
        enabled = b;
        return old_value;
    }
}

} //_ namespace internal
} //_ namespace log
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace log {
namespace tracing {
namespace internal {

bool trace_state(boost::logic::tribool b)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static bool enabled = true;

    if (boost::logic::indeterminate(b)) {
        // Query
        return enabled;
    } else {
        // Assignment
        bool old_value(enabled);
        enabled = b;
        return old_value;
    }
}

} //_ namespace internal
} //_ namespace tracing
} //_ namespace log
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace log {
namespace stdoutput {
namespace internal {

bool stdoutput_state(boost::logic::tribool b)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static bool enabled = false;

    if (boost::logic::indeterminate(b)) {
        // Query
        return enabled;
    } else {
        // Assignment
        bool old_value(enabled);
        enabled = b;
        return old_value;
    }
}

bool only_state(boost::logic::tribool b)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static bool enabled = false;

    if (boost::logic::indeterminate(b)) {
        // Query
        return enabled;
    } else {
        // Assignment
        bool old_value(enabled);
        enabled = b;
        return old_value;
    }
}

} //_ namespace internal
} //_ namespace stdoutput
} //_ namespace log
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace log {

void enable(bool b)
{
    internal::enable_state(b);
}

void status(Status status)
{
    internal::enable_state(status == Status::Enable);
}

bool is_enabled()
{
    return internal::enable_state();
}

} //_ namespace log
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace log {
namespace tracing {

void enable(bool b)
{
    internal::trace_state(b);
}

void status(Status status)
{
    internal::trace_state(status == Status::Enable);
}

bool is_enabled()
{
    return internal::trace_state();
}

} //_ namespace tracing
} //_ namespace log
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace log {
namespace stdoutput {

void enable(bool b)
{
    internal::stdoutput_state(b);
}

void status(Status status)
{
    internal::stdoutput_state(status == Status::Enable);
}

bool is_enabled()
{
    return internal::stdoutput_state();
}

void only(bool b)
{
    internal::only_state(b);
}

bool is_only()
{
    return internal::only_state();
}

} //_ namespace stdoutput
} //_ namespace log
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {

Log::LogImp* Log::Implementation(Log::LogImp *p)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static Log::LogImp *instance = nullptr;

    if (p != nullptr && instance == nullptr) {
        // Assignment
        instance = p;
        return nullptr;
    } else if (p == nullptr && instance != nullptr) {
        // Query
        return instance;
    } else if (p != nullptr && instance != nullptr) {
        // Reassignment
        FAIL("Attempt to reassign a value to a single-instance object.");
        return nullptr;
    } else if (p == nullptr && instance == nullptr) {
        // Possible query or an attempt to initialize object with a null value.
        // But we do not allow initialization to null, so, this must be a query
        // returning the uninitialized value (nullptr).
        return nullptr;
    } else {
        throw std::runtime_error("Should not execute this code.");
    }
}

Log::Log()
{
    // Create implementation object on first instance of this class.
    // This allows log messages to be buffered.
    if (Log::Implementation() == nullptr) {
        Log::Implementation(new Log::LogImp());
    }
}

void Log::Init(Type t)
{
    auto *ptr = Log::Implementation();
    ASSERT(ptr != nullptr, "Logger implementation is null.\n"
        "Create a Log object first to initialize the implementation object.");

    if (t == Type::Poco) {
#if 0
        ptr->logger = new log::Poco();
#endif
    } else if (t == Type::Boost) {
        ptr->logger = new log::Boost();
    } else {
        FAIL("Unknown Logger type.");
        return;
    }

    if (ptr->logger == nullptr) {
        throw std::runtime_error("Failed creating ILogger instance.");
    }

    ptr->logger->set_file(ptr->path, ptr->file);
    ptr->logger->Init();
    ptr->initDone = true;
    Log::log(log::Priority::Information, "-----");
    Log::log(log::Priority::Information, "Log start.");

    // Send the contents of the temporary log buffer to the actual logger

    if (ptr->buffer.empty()) return;
    for (auto &msg : ptr->buffer) {
        ptr->logger->Log(log::Priority(msg.priority),
            msg.message,
            msg.file,
            msg.line);
    }
    ptr->buffer.clear();
}

void Log::Shutdown()
{
    auto *ptr = Log::Implementation();
    ASSERT(ptr != nullptr, "Logger implementation is null.");
    ASSERT(ptr->logger != nullptr, "ILogger instance is null.");
    if (ptr != nullptr) {
        if (ptr->logger != nullptr) {
            ptr->logger->Shutdown();
            reset(ptr->logger);
            reset(ptr);
        }
    }
}

void Log::send_to_stdout(log::Priority p, const std::string &s)
{
    DEBUG(!s.empty());
    auto msg(log::ILogger::format_message(s));
    std::cout << log::internal::LogPrefix[p] << msg << "\n";
}

void Log::send_to_stdout(log::Priority priority,
        const log::tracing::utility::Info &trinfo,
        const std::string &s)
{
    static const std::vector<const char *> label {
        "Function:  ",
        "File:      "
    };
    const auto spaces(std::string(7, ' '));

    std::string fstr(string::replace(string::replace(trinfo.file, "..\\"),
        "src\\negerns\\"));
    std::string msg;

    if (!s.empty()) {
        msg.assign(s + "\n" + spaces);
    }
    msg.append(std::string(label[0])
        + trinfo.function + "\n"
        + spaces
        + label[1]
        + fstr
        + " ("
        + trinfo.get_line_as_string()
        + ")"
    );
    send_to_stdout(priority, msg);
}

void Log::send_to_logger(log::Priority priority, const std::string &s)
{
    auto *ptr = Log::Implementation();
    //* IMPORTANT: Using ASSERT here causes stack overflow when testing with
    //* logging. Use WARNING instead of ASSERT and provide a conditional
    //* statement to skip over the routine if the implementation is not yet
    //* initialized.
    WARNING(ptr != nullptr, "Logger implementation is null.");
    if (ptr != nullptr) {
        if (ptr->initDone) {
            ptr->logger->Log(priority, s);
        } else {
            ptr->buffer.push_back(log::Message("app", priority, s));
        }
    }
}

void Log::send_to_logger(
    log::Priority priority,
    const log::tracing::utility::Info &trinfo,
    const std::string &s)
{
    static const char *source = "app";

    auto *ptr = Log::Implementation();
    ASSERT(ptr != nullptr, "Logger implementation is null.");
    if (ptr->initDone) {
        ptr->logger->Log(priority, s, trinfo.file.c_str(), trinfo.line);
    } else {
        auto logMsg = log::Message(source, priority, s, trinfo.file.c_str(),
            trinfo.line);
        ptr->buffer.push_back(logMsg);
    }
}



Log::~Log() { }

void Log::SetFile(const std::string &p, const std::string &f)
{
    auto *ptr = Log::Implementation();
    ASSERT(ptr != nullptr, "Logger implementation is null.");
    if (ptr != nullptr) {
        if (ptr->logger == nullptr) {
            ptr->file = f;
            ptr->path = p;
        } else {
            ptr->logger->set_file(p, f);
        }
    }
}

std::string Log::GetFullPath()
{
    auto *ptr = Log::Implementation();
    ASSERT(ptr != nullptr, "Logger implementation is null.");
    if (ptr != nullptr) {
        if (ptr->logger == nullptr) {
            return ptr->file;
        } else {
            return ptr->logger->get_full_path();
        }
    } else {
        // Avoid compiler warning on not having all control paths return a value
        return std::string();
    }
}

} //_ namespace negerns
