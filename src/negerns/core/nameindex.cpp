#include <negerns/core/debug.h>
#include <negerns/core/nameindex.h>

namespace negerns {

NameIndexContainer::~NameIndexContainer()
{
    Clear();
}
void NameIndexContainer::Add(const std::string &s)
{
    if (!name.empty()) {
        ASSERT(!IsFound(s));
    }
    Vector::Add(s);
    auto i = name.size();
    name.emplace(std::make_pair(s, i));
}

#if 0
bool NameIndexContainer::IsFound(const std::string& s)
{
    return name.size() > 0 ? name.find(s) != name.end() : false;
}

std::size_t& NameIndexContainer::GetIndex(const std::string &s)
{
    if (name.size() > 0) {
        ASSERT(IsFound(s));
    }
    return name[s];
}

#if 0
std::map<std::string, std:size_t> NameIndexContainer::Get()
{
    return name;
}
#endif

std::size_t NameIndexContainer::Count()
{
    return name.size();
}

bool NameIndexContainer::Empty() const
{
    return name.size() == 0;
}

bool NameIndexContainer::IsEmpty() const
{
    return name.size() == 0;
}
#endif

std::string& NameIndexContainer::GetName(std::size_t i)
{
    ASSERT(Vector::Count() > 0, "NameIndexContainer is empty.");
    ASSERT(i < Vector::Count(), "Index is out of range.");
    return Vector::Get(i);
}

void NameIndexContainer::Clear()
{
    Vector::Clear();
    name.clear();
}

} //_ namespace negerns
