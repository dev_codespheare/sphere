#if 0
#include <malloc.h>
#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <Poco/Logger.h>
#include <Poco/FileChannel.h>
#include <Poco/PatternFormatter.h>
#include <Poco/FormattingChannel.h>
#include <Poco/AsyncChannel.h>
#include <Poco/Path.h>
#include <negerns/core/filesystem.h>
#include <negerns/core/system.h>
#include <negerns/core/configuration.h>
#include <negerns/core/log/poco.h>

namespace negerns {
namespace log {

// Convert priority level to POCO equivalent.
// POCO priority level starts with Fatal with integer number 1.
//
// Fatal = 1   - A fatal error. The application will most likely terminate. This is the highest priority.
// Critical    - A critical error. The application might not be able to continue running successfully.
// Error       - An error. An operation did not complete successfully, but the application as a whole is not affected.
// Warning     - A warning. An operation completed with an unexpected result.
// Notice      - A notice, which is an information with just a higher priority.
// Information - An informational message, usually denoting the successful completion of an operation.
// Debug       - A debugging message.
// Trace       - A tracing message. This is the lowest priority.
poco::Message::Priority ConvertPriority(Priority priority)
{
    return poco::Message::Priority(poco::Message::PRIO_TRACE - priority);
}


// ----------------------------------------------------------------------------------------



poco::Logger* Poco::logger = nullptr;

Poco::Poco() { }

Poco::~Poco() { }

void Poco::Init()
{
#if 0
#ifdef _DEBUG
    const int defaultLogLevel = Priority::Trace;
#else
    const int defaultLogLevel = Priority::Information;
#endif

    // NOTE:
    //
    // All calls to log messages will be via the Log interface since the
    // actual logging mechanism has not yet been initialized until after
    // this function.

    auto *config = System::Configuration();
    int logLevel = config->GetInt("logging.level", defaultLogLevel);
    if (logLevel == 0) {
        logLevel = defaultLogLevel;
    }

    std::string path = GetPath();
    if (!filesystem::exists(path)) {
        bool success = filesystem::create_directory(path);
        if (!success) {
            Log::Warning("Cannot create Log output path (%1).", path);
            path = filesystem::current_path();
            SetPath(path);
            Log::Info("  Using '%1'.", path);
        }
    }

    std::string file(path);
    file.append(GetFilename());

    poco::AutoPtr<poco::FileChannel> fileChannel(new poco::FileChannel());
    fileChannel->setProperty("path", file);
    fileChannel->setProperty("rotation", "daily");
    fileChannel->setProperty("rotateOnOpen", "false");
    fileChannel->setProperty("archive", "timestamp");
    fileChannel->setProperty("compress", "false");

    Log::Info("Log configuration:");
    Log::Info("  Level: %1", logLevel);
    Log::Info("  Rotation: %1", fileChannel->getProperty("rotation"));
    Log::Info("  Rotate on Open: %1", fileChannel->getProperty("rotateOnOpen"));
    Log::Info("  Archive file: %1", fileChannel->getProperty("archive"));
    Log::Info("  Compress: %1", fileChannel->getProperty("compress"));

    poco::AutoPtr<poco::PatternFormatter> formatter(new poco::PatternFormatter());
    formatter->setProperty("times", "local");
    formatter->setProperty("pattern", "%Y%m%d %H%M%S.%F %v[6] %q   %t");

    Log::Info("  Format: %1", formatter->getProperty("pattern"));

    poco::AutoPtr<poco::FormattingChannel> formattingChannel(new poco::FormattingChannel(formatter, fileChannel));
    poco::AutoPtr<poco::AsyncChannel> asyncChannel(new poco::AsyncChannel(formattingChannel));

    Poco::logger = &poco::Logger::get("app");
    Poco::logger->setChannel(asyncChannel);
    Poco::logger->setLevel(logLevel);
#endif
}

void Poco::Shutdown()
{
#if 0
#if 1
    Log(Priority::Trace,
        string::Format("Log channel ref count: %1",
            Poco::logger->getChannel()->referenceCount()
        )
    );
    Log(Priority::Information, "Log stop.");
#endif
    if (Poco::logger == nullptr) return;
    Poco::logger->setLevel("none");

    // TODO: Calling poco::Logger::shutdown delays the application termination
    //       for quite an amount of time. Not calling it causes an exception
    //       only in Debug 64-bit build. Although the other builds does not
    //       exhibit the exception does not necessarily mean the application
    //       termination did not encounter any inconsistencies. This has to be
    //       investigated thoroughly.

    // NOTE: The following is an attempt to solve the issue above.
    //       So far, the scenario stated above is not happening.

    Poco::logger->close();
    poco::Logger::shutdown();
#endif
}

void Poco::Log(Priority priority, const std::string &s)
{
#if 0
    ASSERT(Poco::logger, "Logger object is null.");
    Log(priority, s, "", 0);
#endif
}

void Poco::Log(Priority priority, const std::string &s,
    const char* file, int line)
{
#if 0
    ASSERT(Poco::logger, "Logger object is null.");
    std::string tmp(s);
    unsigned short len = 0;

    // Indent multiple lines
    if (tmp.find('\n') != std::string::npos) {
        auto lines = string::split(tmp, '\n', false);
        // If the 'indented' first line goes beyond the starting column for
        // the file information, truncate it and append an ellipsis.
        if (lines[0].length() + Poco::indent > fileColumn) {
            lines[0] = lines[0].substr(0, fileColumn - Poco::indent - 4);
            lines[0].append("...");
        }
        const std::size_t count = lines.size();
        for (std::size_t i = 1; i < count; ++i) {
            lines[i].insert(0, indent, ' ');
            // Truncate if text goes beyond the starting column for the file
            // information.
            if (lines[i].length() > fileColumn) {
                lines[i] = lines[i].substr(0, fileColumn);
            }
        }
        tmp = string::join(lines, 1, '\n');
        len = lines.back().length();
    } else {
        // Truncate if text goes beyond the starting column for the file
        // information.
        if (tmp.length() + Poco::indent > fileColumn) {
            tmp = tmp.substr(0, fileColumn - Poco::indent - 4);
            tmp.append("...");
        }
        len = tmp.length() + Poco::indent;
    }

    // Append the filename and line number
    if (Poco::logger->trace() && line > 0) {
        tmp.append((fileColumn > len) ? fileColumn - len : fileColumn, ' ');
        tmp.append(string::replace(file, "..\\", ""));
        if (line > 0) {
            tmp.append(", ");
            tmp.append(string::to_string(line));
        }
    }

    // Build the message object and send it to the logging facility
    poco::Message message(Poco::logger->name(), tmp, ConvertPriority(priority));
    Poco::logger->log(message);
#endif
}

} //_ namespace log
} //_ namespace negerns

#endif
