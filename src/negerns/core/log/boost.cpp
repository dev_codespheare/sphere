#include <malloc.h>
#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <boost/log/core.hpp>

#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

#include <boost/log/sources/record_ostream.hpp>

#include <negerns/core/debug.h>
#include <negerns/core/system.h>
#include <negerns/core/configuration.h>
#include <negerns/core/filesystem.h>
#include <negerns/core/log/boost.h>

using namespace boost::log;

namespace negerns {
namespace log {

sources::severity_logger<trivial::severity_level> *Boost::logger = nullptr;

Boost::Boost() { }

Boost::~Boost() { }

void Boost::Init()
{
#ifdef _DEBUG
    const int defaultLogLevel = Priority::Trace;
#else
    const int defaultLogLevel = Priority::Information;
#endif

    // NOTE:
    //
    // All calls to log messages will be via the Log interface since the
    // actual logging mechanism has not yet been initialized until after
    // this function.

    // Log level

    auto *config = System::Configuration();
    int logLevel = config->get_int("logging.level", defaultLogLevel);
    if (logLevel == 0) {
        logLevel = defaultLogLevel;
    }

    boost::log::core::get()->set_filter(trivial::severity >= trivial::info);

    // Log filepath

    std::string path = get_path();
    if (!filesystem::exists(path)) {
        bool success = filesystem::create_directory(path);
        if (!success) {
            log::warning("Cannot create Log output path (%1).", path);
            path = filesystem::current_path();
            set_path(path);
            log::info("  Using '%1'.", path);
        }
    }
    std::string file(path);
    file.append(get_filename());

    boost::log::add_file_log(
        keywords::file_name = "file_%Y%m%d.log",                                      /*< file name pattern >*/
        keywords::rotation_size = 10 * 1024 * 1024,                                   /*< rotate files every 10 MiB... >*/
        keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0), /*< ...or at midnight >*/
        keywords::format = "[%TimeStamp%]: %Message%"                                 /*< log record format >*/
    );

    boost::log::add_common_attributes();
}

void Boost::Shutdown()
{
    Boost::logger = nullptr;
}

void Boost::Log(Priority priority, const std::string &s)
{
    ASSERT(Boost::logger, "Logger object is null.");
    Log(priority, s, "", 0);
}

void Boost::Log(Priority priority, const std::string &s, const char* file, int line)
{
    ASSERT(Boost::logger, "Logger object is null.");
    if (priority == Priority::Notice) {
        priority = Priority::Info;
    } else if (priority == Priority::Critical) {
        priority = Priority::Fatal;
    }
    auto level = static_cast<trivial::severity_level>(priority);
    BOOST_LOG_SEV(*Boost::logger, level) << s;
}

} //_ namespace log
} //_ namespace negerns
