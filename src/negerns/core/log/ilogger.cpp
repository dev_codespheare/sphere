#include <negerns/core/string.h>
#include <negerns/core/debug.h>
#include <negerns/core/log/ilogger.h>

namespace negerns {
namespace log {

static std::map<Priority, std::string> PriorityString {
    { Priority::Trace,       "Trace"       },
    { Priority::Debug,       "Debug"       },
    { Priority::Information, "Information" },
    { Priority::Notice,      "Notice"      },
    { Priority::Warning,     "Warning"     },
    { Priority::Error,       "Error"       },
    { Priority::Critical,    "Critical"    },
    { Priority::Fatal,       "Fatal"       }
};

std::string priority_abbr(Priority p)
{
    return PriorityString[p].substr(0, 1);
}

std::string priority_name(Priority p)
{
    return PriorityString[p];
}

std::string ILogger::format_message(const std::string &s, int col)
{
    DEBUG(!s.empty());
    static const int fileColumn = 512;
    static const int indent = 3;
    int column = col == 0 ? fileColumn : col;
    unsigned short len = 0;

    std::string tmp(s);
    // Indent multiple lines
    if (tmp.find('\n') != std::string::npos) {
        auto lines = string::split(tmp, '\n', false);
        // If the 'indented' first line goes beyond the starting column for
        // the file information, truncate it and append an ellipsis.
        if (lines[0].length() + indent > column) {
            lines[0] = lines[0].substr(0, column - indent - 4);
            lines[0].append("...");
        }
        const std::size_t count = lines.size();
        for (std::size_t i = 1; i < count; ++i) {
            lines[i].insert(0, indent, ' ');
            // Truncate if text goes beyond the starting column for the file
            // information.
            if (lines[i].length() > column) {
                lines[i] = lines[i].substr(0, column);
            }
        }
        tmp = string::join(lines, 1, '\n');
        len = lines.back().length();
    } else {
        // Truncate if text goes beyond the starting column for the file
        // information.
        if (tmp.length() + indent > column) {
            tmp = tmp.substr(0, column - indent - 4);
            tmp.append("...");
        }
        len = tmp.length() + indent;
    }
    return std::move(tmp);
}

void ILogger::set_file(const std::string &p, const std::string &f)
{
    path = p;
    if (path.back() != '\\') {
        path.append("\\");
    }
    filename = f;
    filename.append(".log");
}

} //_ namespace log
} //_ namespace negerns
