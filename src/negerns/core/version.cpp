#include <negerns/core/version.h>

namespace negerns {

bool Version::at_least(const int major, const int minor, const int revision)
{
    int majorv = major * NEGERNS_MAJOR_MULTIPLIER;
    int minorv = minor * NEGERNS_MINOR_MULTIPLIER;
    int revisionv = revision * NEGERNS_REVISION_MULTIPLIER;

    return NEGERNS_VERSION >= majorv + minorv + revisionv;
}

bool Version::is(const int major, const int minor, const int revision)
{
    int majorv = major * NEGERNS_MAJOR_MULTIPLIER;
    int minorv = minor * NEGERNS_MINOR_MULTIPLIER;
    int revisionv = revision * NEGERNS_REVISION_MULTIPLIER;

    return NEGERNS_VERSION == majorv + minorv + revisionv;
}

int Version::to_int(const int major, const int minor, const int revision, const int internal)
{
    int majorv = major * NEGERNS_MAJOR_MULTIPLIER;
    int minorv = minor * NEGERNS_MINOR_MULTIPLIER;
    int revisionv = revision * NEGERNS_REVISION_MULTIPLIER;
    int internalv = internal * NEGERNS_INTERNAL_MULTIPLIER;

    return majorv + minorv + revisionv + internalv;
}

const int Version::to_int()
{
    return NEGERNS_VERSION;
}

std::string Version::to_string()
{
    return std::move(std::string(VER_VERSION_STR));
}

} //_ namespace negerns
