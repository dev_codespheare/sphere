#include <negerns/core/message.h>
#include <negerns/core/icrud.h>

namespace negerns {

ICrud::ICrud() { }
ICrud::ICrud(const ICrud &) { }
ICrud::~ICrud() { }

void ICrud::Create()
{
    wxCommandEvent e;
    Create(e);
}

void ICrud::Read()
{
    wxCommandEvent e;
    Read(e);
}

void ICrud::Update()
{
    wxCommandEvent e;
    Update(e);
}

void ICrud::Delete()
{
    wxCommandEvent e;
    Delete(e);
}

void ICrud::Refresh()
{
    wxCommandEvent e;
    Refresh(e);
}

void ICrud::Edit()
{
    wxCommandEvent e;
    Edit(e);
}

void ICrud::Create(wxCommandEvent &e)
{
    if (bool status = PreCreate(e)) {
        if (status = OnCreate(e)) {
            existence = Existence::New;
            state = State::Unmodified;
            status = PostCreate(e);
        }
    }
}

void ICrud::Read(wxCommandEvent &e)
{
    if (bool status = PreRead(e)) {
        if (status = OnRead(e)) {
            existence = Existence::Old;
            state = State::Unmodified;
            status = PostRead(e);
        }
    }
}

void ICrud::Update(wxCommandEvent &e)
{
    if (bool status = PreUpdate(e)) {
        if (status = OnUpdate(e)) {
            existence = Existence::Old;
            state = State::Unmodified;
            status = PostUpdate(e);
        }
    }
}

void ICrud::Delete(wxCommandEvent &e)
{
    if (bool status = PreDelete(e)) {
        if (status = OnDelete(e)) {
            status = PostDelete(e);
        }
    }
}

void ICrud::Refresh(wxCommandEvent &e)
{
    if (bool status = PreRefresh(e)) {
        if (status = OnRefresh(e)) {
            existence = Existence::Old;
            state = State::Unmodified;
            status = PostRefresh(e);
        }
    }
}

void ICrud::Edit(wxCommandEvent &e)
{
    OnEdit(e);
}

void ICrud::Undo(wxCommandEvent &e)
{
    OnUndo(e);
}

void ICrud::Redo(wxCommandEvent &e)
{
    OnRedo(e);
}

bool ICrud::PreCreate(wxCommandEvent &e)
{
    return true;
}

bool ICrud::PreRead(wxCommandEvent &e)
{
    return true;
}

bool ICrud::PreUpdate(wxCommandEvent &e)
{
    return true;
}

bool ICrud::PreDelete(wxCommandEvent &e)
{
    return true;
}

bool ICrud::PreRefresh(wxCommandEvent &e)
{
    return true;
}

bool ICrud::OnCreate(wxCommandEvent &e)
{
    return true;
}

bool ICrud::OnRead(wxCommandEvent &e)
{
    return true;
}

bool ICrud::OnUpdate(wxCommandEvent &e)
{
    return true;
}

bool ICrud::OnDelete(wxCommandEvent &e)
{
    return true;
}

bool ICrud::OnRefresh(wxCommandEvent &e)
{
    return true;
}

bool ICrud::PostCreate(wxCommandEvent &e)
{
    return true;
}

bool ICrud::PostRead(wxCommandEvent &e)
{
    return true;
}

bool ICrud::PostUpdate(wxCommandEvent &e)
{
    return true;
}

bool ICrud::PostDelete(wxCommandEvent &e)
{
    return true;
}

bool ICrud::PostRefresh(wxCommandEvent &e)
{
    return true;
}

void ICrud::OnEdit(wxCommandEvent&)
{
    negerns::Message::Warning("Function is not implemented.", __FUNCSIG__);
}

void ICrud::OnUndo(wxCommandEvent&)
{
    negerns::Message::Warning("Function is not implemented.", __FUNCSIG__);
}

void ICrud::OnRedo(wxCommandEvent&)
{
    negerns::Message::Warning("Function is not implemented.", __FUNCSIG__);
}

void ICrud::SetState(State s)
{
    state = s;
}

void ICrud::SetModified(bool b)
{
    state = b ? State::Modified : State::Unmodified;
}

void ICrud::SetUnmodified(bool b)
{
    state = b ? State::Unmodified : State::Modified;
}

} //_ namespace negerns
