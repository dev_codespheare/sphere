#include <negerns/core/debug.h>
#include <negerns/core/identifier.h>

#include <negerns/negerns.h>

namespace negerns {

Identifier::Identifier() :
    unique(false)
{ }

Identifier::Identifier(const Identifier& id)
{
    base = id.base;
    suffix = id.suffix;
    unique = id.unique;
#ifdef _DEBUG

#endif
}

Identifier::Identifier(const std::string &b, const std::string &s, bool u) :
    base(b),
    suffix(s),
    unique(u)
{ }

Identifier::~Identifier() { }

void Identifier::set(const std::string &b, const std::string &s, bool u)
{
    base = b;
    suffix = s;
    unique = u;
}

std::string Identifier::get(const n::Var &v) const
{
    ASSERT(v.is_type_integral(), "Wrong variant type.");
    std::string s = base;
    s.append("_").append(n::string::to_string(v.get_uint()));
    return s;
}

std::string Identifier::get(const n::Vars &vars) const
{
    std::string s;
    for (auto &v : vars) {
        s.append("_").append(n::string::to_string(v.get_uint()));
    }
    return s;
}

void Identifier::set_suffix(const n::Vars &vars)
{
    std::string s;
    for (auto &v : vars) {
        s.append(n::string::to_string(v.get_uint())).append("_");
    }
    if (!s.empty()) {
        s.pop_back();
        suffix = s;
    }
}



#ifdef _DEBUG

bool Identifier::is_present(const std::string &)
{
    throw std::runtime_error("Not implemented yet.");
}

bool Identifier::is_unique(const std::string &)
{
    throw std::runtime_error("Not implemented yet.");
}

#endif

} //_ namespace negerns
