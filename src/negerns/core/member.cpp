#include <negerns/core/member.h>

namespace negerns {
namespace member {

bool String::Empty() const { return Get().empty(); }

bool String::IsEmpty() const { return Get().empty(); }

} //_ namespace member
} //_ namespace negerns
