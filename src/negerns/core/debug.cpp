#include <wx/debug.h>
#include <wx/string.h>
#include <negerns/core/log.h>
#include <negerns/core/debug.h>

namespace negerns {
namespace assert {
namespace {

bool enable_state(boost::logic::tribool b)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static bool enabled = true;

    if (boost::logic::indeterminate(b)) {
        // Query
        return enabled;
    } else {
        // Assignment
        bool old_value(enabled);
        enabled = b;
        return old_value;
    }
}

bool use_gui_state(boost::logic::tribool b)
{
    // Avoid static initialization problem that might occur in the future
    // By hiding the static variable inside a function.
    // http://www.parashift.com/c++-faq/static-init-order-on-first-use.html
    static bool enabled = true;

    if (boost::logic::indeterminate(b)) {
        // Query
        return enabled;
    } else {
        // Assignment
        bool old_value(enabled);
        enabled = b;
        return old_value;
    }
}

} //_ anonymous namespace
} //_ namespace assert
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {
namespace assert {

void enable(bool b)
{
    enable_state(b);
}

void status(Status status)
{
    enable_state(status == Status::Enable);
}

bool is_enabled()
{
    return enable_state();
}

void use_gui(bool b)
{
    use_gui_state(b);
}

void output(Output output)
{
    use_gui_state(output == Output::GUI);
}

bool is_gui()
{
    return use_gui_state();
}

bool is_console()
{
    return !use_gui_state();
}

std::string get_level_string(AssertLevel level)
{
    static std::map<AssertLevel, std::string> name {
        { AssertLevel::Test,    "Test:      " },
        { AssertLevel::Warning, "Warning:   " },
        { AssertLevel::Debug,   "Debug:     " },
        { AssertLevel::Error,   "Error:     " },
        { AssertLevel::Fatal,   "FATAL:     " }
    };
    return name[level];
}

void process(
    AssertLevel level,
    const char *file,
    int line,
    const char *func,
    const char *cond,
    std::vector<::negerns::Var> &&v)
{
    static const std::vector<std::string> label {
        "Condition: ",
        "Function:  ",
        "File:      "
    };
    std::string msg;
    if (!v.empty()) {
        auto value = v.at(0);
        if (v.size() > 1) {
            v.erase(v.begin());
            std::string fmt = value.as_string();
            msg = ::negerns::Format(fmt, v);
        } else {
            msg = value.as_string();
        }
    }

    // Delete leading ..\ noise from file
    std::string fstr(string::replace(string::replace(file, "..\\"),
        "src\\negerns\\"));
    std::string condition(strlen(cond) == 0 ? "[no condition]" : cond);

    if (is_enabled() && is_gui()) {
        if (wxTheAssertHandler != NULL) {
            wxOnAssert(fstr, line, func, cond, wxString(msg));
            if (wxTrapInAssert) {
                wxTrapInAssert = false;
                wxTrap();
            }
        }
    }

    if (level == AssertLevel::Error || level == AssertLevel::Fatal) {
        // Log error and fatal assertions
        const auto spaces(std::string(7, ' '));
        std::string s;
        if (msg.empty()) {
            msg.assign("[no info]");
        }
        s.append(get_level_string(level)).append(msg).append("\n");
        s.append(spaces).append(label[0]).append(cond).append("\n");
        s.append(spaces).append(label[1]).append(func).append("\n");
        s.append(spaces).append(label[2]).append(fstr).
            append(" (").append(string::to_string(line)).append(")");
        if (level == AssertLevel::Error) {
            log::error(s);
        } else if (level == AssertLevel::Fatal) {
            log::fatal(s);
        }
    } else if (!is_gui()) {
        // Send to std output
        const auto spaces(std::string(10, ' '));
        std::vector<std::string> vecstr;
        // The double new line characters at the end. This is an intended
        // visual line separator for multiple consecutive assertion messages.
        vecstr.push_back(get_level_string(level).append(msg).append("\n"));
        vecstr.push_back(std::string(label[0]).append(cond).append("\n"));
        vecstr.push_back(std::string(label[1]).append(func).append("\n"));
        vecstr.push_back(std::string(label[2]).append(fstr).
            append(" (").append(string::to_string(line)).append(")\n\n"));
        for (auto &str: vecstr) {
            std::cout << spaces << str;
        }
    }
}

} //_ namespace assert
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {

#ifdef _DEBUG
bool is_debug() { return true; }
#else
bool is_debug() { return false; }
#endif

bool is_debugging()
{
    return wxIsDebuggerRunning();
}

} //_ namespace negerns
