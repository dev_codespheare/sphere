#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/string/convert.h>
#include <negerns/core/var.h>

namespace negerns {
namespace variant {

char get_as_char(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            return v->char_;
        case Type::SChar:
            if (std::is_signed<char>::value) {
                return v->schar_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UChar:
            if (!std::is_signed<char>::value) {
                return v->uchar_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::Bool:
            return static_cast<char>(v->uint_ != 0 ? 1 : 0);
        default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

int8_t get_as_int8(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            if (std::is_signed<char>::value) {
                return v->char_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::SChar:
            return v->schar_;
        case Type::Bool:
            return static_cast<int8_t>(v->uint_ != 0 ? 1 : 0);
        default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

uint8_t get_as_uint8(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            if (std::is_signed<char>::value) {
                if (v->char_ >= 0) {
                    return static_cast<uint8_t>(v->char_);
                } else {
                    throw std::logic_error({"Invalid type ", TypeName(v->type)});
                }
            } else {
                return v->char_;
            }
        case Type::SChar:
            if (v->schar_ >= 0) {
                return static_cast<uint8_t>(v->schar_);
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UChar:
            return v->uchar_;
        case Type::Bool:
            return static_cast<uint8_t>(v->uint_ != 0 ? 1 : 0);
         default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

int16_t get_as_int16(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            return v->char_;
        case Type::SChar:
            return v->schar_;
        case Type::UChar:
            return v->uchar_;
        case Type::Bool:
            return static_cast<int16_t>(v->uint_ != 0 ? 1 : 0);
        case Type::Short:
            return v->short_;
        case Type::UShort:
            return v->ushort_;
        default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

uint16_t get_as_uint16(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            if (v->char_ >= 0) {
                return v->char_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::SChar:
            if (v->schar_ >= 0) {
                return v->schar_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UChar:
            return v->uchar_;
        case Type::Bool:
            return static_cast<uint16_t>(v->uint_ != 0 ? 1 : 0);
        case Type::Short:
            if (v->short_ >= 0) {
                return v->short_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UShort:
            return v->ushort_;
         default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

int32_t get_as_int32(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            return v->char_;
        case Type::SChar:
            return v->schar_;
        case Type::UChar:
            return v->uchar_;
        case Type::Bool:
            return static_cast<int32_t>(v->uint_ != 0 ? 1 : 0);
        case Type::Short:
            return v->short_;
        case Type::UShort:
            return v->ushort_;
        case Type::Integer:
            return v->int_;
        case Type::UInteger:
            return v->uint_;
        case Type::Long:
            return v->long_;
        default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

uint32_t get_as_uint32(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            if (v->char_ >= 0) {
                return v->char_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::SChar:
            if (v->schar_ >= 0) {
                return v->schar_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UChar:
            return v->uchar_;
        case Type::Bool:
            return static_cast<uint32_t>(v->uint_ != 0 ? 1 : 0);
        case Type::Short:
            if (v->short_ >= 0) {
                return v->short_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UShort:
            return v->ushort_;
        case Type::Integer:
            if (v->int_ >= 0) {
                return v->int_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UInteger:
            return v->uint_;
        case Type::Long:
            if (v->long_ >= 0) {
                return v->long_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::ULong:
            return v->ulong_;
        default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

int64_t get_as_int64(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            return v->char_;
        case Type::SChar:
            return v->schar_;
        case Type::UChar:
            return v->uchar_;
        case Type::Bool:
            return v->uint_ != 0 ? 1 : 0;
        case Type::Short:
            return v->short_;
        case Type::UShort:
            return v->ushort_;
        case Type::Integer:
            return v->int_;
        case Type::UInteger:
            return v->uint_;
        case Type::Long:
            return v->long_;
        case Type::ULong:
            return v->ulong_;
        case Type::LLong:
            return v->llong_;
        default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

uint64_t get_as_uint64(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            if (v->char_ >= 0) {
                return v->char_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::SChar:
            if (v->schar_ >= 0) {
                return v->schar_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UChar:
            return v->uchar_;
        case Type::Bool:
            return static_cast<uint64_t>(v->uint_ != 0 ? 1 : 0);
        case Type::Short:
            if (v->short_ >= 0) {
                return v->short_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UShort:
            return v->ushort_;
        case Type::Integer:
            if (v->int_ >= 0) {
                return v->int_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UInteger:
            return v->uint_;
        case Type::Long:
            if (v->long_ >= 0) {
                return v->long_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::ULong:
            return v->ulong_;
        case Type::LLong:
            if (v->llong_ >= 0) {
                return v->llong_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::ULLong:
            return v->ullong_;
        default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

std::string get_as_string(const Var *arg, uint8_t prec)
{
    static const bool negative = false;

    switch (arg->get_type()) {
        case variant::Type::Bool:
            return arg->get_uint() != 0 ? "true" : "false";
        case variant::Type::Char:
        case variant::Type::SChar:
        case variant::Type::Short:
        case variant::Type::Integer:
        case variant::Type::Long:
        case variant::Type::LLong: {
            auto n = arg->as_llong();
            return n < 0LL ? string::utos(-n, negative) : string::utos(n);
        }
        case variant::Type::UChar:
        case variant::Type::UShort:
        case variant::Type::UInteger:
        case variant::Type::ULong:
        case variant::Type::ULLong:
            return string::utos(arg->as_ullong());
        // Must be checked ahead of the other floating point
        // types because we do not want a decimal value being
        // rounded off when converted to a floating point type.
        case variant::Type::Fixed2:
            return negerns::decimal::to_string(arg->get_fixed2());
        case variant::Type::Float:
        case variant::Type::Double:
        case variant::Type::LDouble: {
            auto n = arg->as_ldouble();
            return string::ftos(n, prec);
        }
        case variant::Type::ShortString:
        case variant::Type::String:
            return arg->get_charptr();
        case Type::Tm:
            return negerns::date::to_string(arg->get_tm());
        default:
            NO_SWITCH_DEFAULT;
    }
    return std::string();
}

} //_ namespace variant
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negerns {

Var::Var(const char *v)
{
    if (v == nullptr) {
        content = nullptr;
    } else {
        auto len = std::strlen(v);
        if (len < sizeof(uint64_t)) {
            type = variant::Type::ShortString;
            // Copy including the null terminating character
            std::copy_n(v, len + string::null_size, shortstr_);
        } else {
            type = variant::Type::String;
            content = new CharPtrHolder_t(v, len);
        }
    }
}

Var::Var(const std::string &v)
{
    auto len = v.length();
    if (len < sizeof(uint64_t)) {
        type = variant::Type::ShortString;
        v.copy(shortstr_, len);
        shortstr_[len] = string::null_char;
     } else {
        type = variant::Type::String;
        content = new CharPtrHolder_t(v.c_str(), len);
     }
}

Var::Var(const Var &o)
{
    if (this != &o) {
        type = o.type;
        if (is_type_object()) {
            content = o.content->clone();
        } else {
            double_ = o.double_;
        }
    }
}

Var::Var(Var &&o)
{
    if (this != &o) {
        type = o.type;
        if (is_type_object()) {
            content = o.content;
        } else {
            double_ = o.double_;
        }
        o.type = variant::Type::Empty;
        o.content = nullptr;
    }
}

bool Var::operator == (const Var &rhs) const
{
    if (empty() && rhs.empty()) {
        return true;
    } else if ((empty() && !rhs.empty()) || (!empty() && rhs.empty())) {
        return false;
    } else if (is_char() && rhs.is_char()) {
        return get_char() == rhs.get_char();
    } else if (is_schar() && rhs.is_schar()) {
        return get_schar() == rhs.get_schar();
    } else if (is_uchar() && rhs.is_uchar()) {
        return get_uchar() == rhs.get_uchar();
    } else if (is_bool() && rhs.is_bool()) {
        return get_bool() == rhs.get_bool();
    } else if (is_short() && rhs.is_short()) {
        return get_short() == rhs.get_short();
    } else if (is_ushort() && rhs.is_ushort()) {
        return get_ushort() == rhs.get_ushort();
    } else if (is_int() && rhs.is_int()) {
        return get_int() == rhs.get_int();
    } else if (is_uint() && rhs.is_uint()) {
        return get_uint() == rhs.get_uint();
    } else if (is_long() && rhs.is_long()) {
        return get_long() == rhs.get_long();
    } else if (is_ulong() && rhs.is_ulong()) {
        return get_ulong() == rhs.get_ulong();
    } else if (is_llong() && rhs.is_llong()) {
        return get_llong() == rhs.get_llong();
    } else if (is_ullong() && rhs.is_ullong()) {
        return get_ullong() == rhs.get_ullong();
    } else if (is_float() && rhs.is_float()) {
        return get_float() == rhs.get_float();
    } else if (is_double() && rhs.is_double()) {
        return get_double() == rhs.get_double();
    } else if (is_ldouble() && rhs.is_ldouble()) {
        return get_ldouble() == rhs.get_ldouble();
    } else if (is_fixed2() && rhs.is_fixed2()) {
        return get_fixed2() == rhs.get_fixed2();
    } else if (is_type_string()) {
        return get_string() == rhs.get_string();
    } else if (is_tm()) {
        return get_tm() == rhs.get_tm();
    } else {
        throw std::logic_error("Unknown type");
    }
}

} //_ namespace negerns
