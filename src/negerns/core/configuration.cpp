#include <negerns/core/log.h>
#include <negerns/core/configuration.h>
#include <negerns/negerns.h>

namespace negerns {

Configuration::Configuration() { }

Configuration::~Configuration()
{
    save();
    for (auto &item : configs) {
        delete item.ptr;
        item.ptr = nullptr;
    }
    configs.clear();
}

bool Configuration::has_key(const std::string &key)
{
    return find(key).ptr != nullptr;
}

bool Configuration::get_bool(const std::string &key, const bool def)
{
    auto config = find(key);
    return config.ptr != nullptr ? config.ptr->get_bool(config.pos, def) : def;
}

std::string Configuration::get_string(const std::string &key,
    const std::string &def)
{
    auto config = find(key);
    return config.ptr != nullptr ? config.ptr->get_string(config.pos) : def;
}

int Configuration::get_int(const std::string &key, const int def)
{
    auto config = find(key);
    return config.ptr != nullptr ? config.ptr->get_int(config.pos, def) : def;
}

unsigned int Configuration::get_uint(const std::string &key,
    const unsigned int def)
{
    auto config = find(key);
    return config.ptr != nullptr ? config.ptr->get_uint(config.pos, def) : def;
}

double Configuration::get_double(const std::string &key,
    const double def)
{
    auto config = find(key);
    return config.ptr != nullptr ? config.ptr->get_double(config.pos, def) : def;
}

bool Configuration::set(const std::string &key, bool value)
{
    auto config = find(key);
    if (config.ptr != nullptr) {
        config.ptr->set(config.pos, value);
    }
    return config.ptr != nullptr;
}

bool Configuration::set(const std::string &key,
    const std::string &value)
{
    auto config = find(key);
    if (config.ptr != nullptr) {
        config.ptr->set(config.pos, value);
    }
    return config.ptr != nullptr;
}

bool Configuration::set(const std::string &key, int value)
{
    auto config = find(key);
    if (config.ptr != nullptr) {
        config.ptr->set(config.pos, value);
    }
    return config.ptr != nullptr;
}

bool Configuration::set(const std::string &key, unsigned int value)
{
    auto config = find(key);
    if (config.ptr != nullptr) {
        config.ptr->set(config.pos, value);
    }
    return config.ptr != nullptr;
}

bool Configuration::set(const std::string &key, double value)
{
    auto config = find(key);
    if (config.ptr != nullptr) {
        config.ptr->set(config.pos, value);
    }
    return config.ptr != nullptr;
}

void Configuration::read()
{
    for (auto &item : configs) {
        item.ptr->read();
    }
}

void Configuration::save()
{
    for (auto &item : configs) {
        item.ptr->save();
    }
}

void Configuration::read(const std::string &f)
{
    LOG_FUNCTION();
    auto item = std::find_if(configs.begin(), configs.end(),
        [&f](ConfigurationInput &e) { return e.filename == f; });
    if (item != configs.end()) {
        log::debug("Reading configuration file: %1", f);
        item->ptr->read();
    } else {
        std::string msg("Configuration file '%1' could not be found in the " \
            "configuration map container.\nPossibly the file has not been " \
            "added.");
        log::error(msg, f);
    }
}

void Configuration::save(const std::string &f)
{
    LOG_FUNCTION();
    auto item = std::find_if(configs.begin(), configs.end(),
        [&f](ConfigurationInput &e) { return e.filename == f; });
    if (item != configs.end()) {
        if (item->ptr->is_updateable()) {
            log::debug("Saving configuration file: %1", f);
            item->ptr->save();
        } else {
            log::warning("Attempt to save non-updateable " \
                "configuration file: %1", f);
        }
    } else {
        std::string msg("Configuration file '%1' could not be found in the " \
            "configuration map container.\nPossibly the file has not been " \
            "added.");
        log::error(msg, f);
    }
}

void Configuration::add(config::Base *ptr,
    unsigned int p)
{
    LOG_FUNCTION();
	std::list<ConfigurationInput>::iterator it = configs.begin();
    // If the configuration being added has an equal priority then it is
    // inserted after the last equal priority.
	while (it != configs.end() && it->priority <= p) {
		++it;
    }
    std::string fn(ptr->get_filename());
    bool found = false;
    for (auto &item : configs) {
        if (item.filename == fn) {
            found = true;
            break;
        }
    }
    if (found) {
        log::info("Duplicate configuraiton file: %1\n  Priority: %2", fn, p);
    } else {
        log::info("Add configuraiton file: %1\n  Priority: %2", fn, p);
	    configs.insert(it, ConfigurationInput(fn, ptr, p));
    }
}

config::ConfigItem Configuration::find(const std::string &key)
{
    std::list<ConfigurationInput>::reverse_iterator it = configs.rbegin();
    n::config::KeyValues::iterator pos;
	for ( ; it != configs.rend(); ++it) {
        if (it->ptr->find(key, pos)) {
            return n::config::ConfigItem(it->ptr, pos);
        }
    }
    return n::config::ConfigItem(nullptr, pos);
}

} //_ namespace negerns
