#include <negerns/core/timer.h>

namespace negerns {

#define DEFAULT_TIMER_OUTPUT_FORMAT "CPU %ts, USR %us, SYS %ss, (%p%%)"

Timer::Timer() :
    boost::timer::cpu_timer(),
    outputFormat(DEFAULT_TIMER_OUTPUT_FORMAT)
{
    // Boost timer is automatically started in the constructor.
    // We stop it here because we wanted the client to explicitly
    // start the timer.
    boost::timer::cpu_timer::stop();
}

Timer::Timer(bool b) :
    boost::timer::cpu_timer(),
    outputFormat(DEFAULT_TIMER_OUTPUT_FORMAT)
{
    // Boost timer is automatically started in the constructor.
    // We stop it here because we wanted the client to explicitly
    // start the timer.
    boost::timer::cpu_timer::stop();

    // We restart the timer here to exclude other processing done above.
    if (b) start();
}

Timer::~Timer() { }

void Timer::start()
{
    boost::timer::cpu_timer::start();
}

void Timer::stop()
{
    boost::timer::cpu_timer::stop();
}

void Timer::resume()
{
    boost::timer::cpu_timer::resume();
}

void Timer::restart()
{
    stop();
    start();
}

std::string Timer::format(
    const timer::CPUTimeNanoseconds &t,
    short places) const
{
    return ::boost::timer::format(t, places, outputFormat);
}

std::string Timer::format(
    const std::string &f,
    const timer::CPUTimeNanoseconds &t,
    short places) const
{
    std::string s(f);
    s.append(outputFormat);
    return ::boost::timer::format(t, places, s);
}

std::string Timer::format(short places) const
{
    auto t = boost::timer::cpu_timer::elapsed();
    return ::boost::timer::format(t, places, outputFormat);
}

std::string Timer::format(const std::string &f, short places) const
{
    auto t = boost::timer::cpu_timer::elapsed();
    std::string s(f);
    s.append(outputFormat);
    return ::boost::timer::format(t, places, s);
}

void Timer::set_format_string(const std::string &s)
{
    outputFormat = s;
}

bool Timer::is_stopped() const
{
    return boost::timer::cpu_timer::is_stopped();
}

negerns::timer::CPUTimeNanoseconds Timer::elapsed() const
{
    return boost::timer::cpu_timer::elapsed();
}

negerns::timer::CPUTimeSeconds Timer::elapsed_seconds() const
{
    return negerns::timer::CPUTimeSeconds(boost::timer::cpu_timer::elapsed());
}

} //_ namespace negerns
