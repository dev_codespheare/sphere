#include <filesystem>
#include <negerns/core/debug.h>
#include <negerns/core/filesystem.h>

namespace negerns {
namespace filesystem {

bool exists(const std::string &f)
{
    std::tr2::sys::path p(f);
    return std::tr2::sys::exists(p);
}

std::string current_path()
{
    auto path(std::tr2::sys::current_path<std::tr2::sys::path>());
    return path.string();
}

bool create_directory(const std::string &s)
{
    std::tr2::sys::path p(s);
    return std::tr2::sys::create_directory<std::tr2::sys::path>(p);
}

std::string parent_path()
{
    auto curr_path(std::tr2::sys::current_path<std::tr2::sys::path>());
    return curr_path.parent_path().string();
}

std::string parent_path(const std::string &s)
{
    std::tr2::sys::path p(s);
    if (std::tr2::sys::exists(p)) {
        return p.parent_path().string();
    } else {
        FAIL("Path: '%1' does not exist.", s);
        return std::string();
    }
}

} //_ namespace filesystem
} //_ namespace negerns
