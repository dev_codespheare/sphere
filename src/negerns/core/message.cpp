#include <windows.h>
// Get rid of the troublesome Windows header file (windows.h)
#include <wx/msw/winundef.h>

#include <negerns/core/debug.h>
#include <negerns/core/message.h>

namespace negerns {

wxWindow* Message::parent = nullptr;
std::string Message::title = "No Title";
wxNotificationMessage Message::note;

void Message::Notify(const std::string &title, const std::string &msg)
{
    note.SetTitle(title);
    note.SetMessage(msg);
    note.Show();
}

int Message::Assert(const std::string &msg, const std::string &emsg,
    long button, long icon)
{
    long style = button | wxCENTER | icon;
    wxMessageDialog msgdlg(Message::parent, msg, Message::title, style);
    msgdlg.SetYesNoLabels("Stop", "Suppress");
    if (!emsg.empty()) {
        msgdlg.SetExtendedMessage(emsg);
    }
    return msgdlg.ShowModal();
}

int Message::Info(const std::string &msg, const std::string &emsg,
    long button, long icon)
{
    long style = button | wxCENTER | icon;
    wxMessageDialog msgdlg(Message::parent, msg, Message::title, style);
    if (!emsg.empty()) {
        msgdlg.SetExtendedMessage(emsg);
    }
    return msgdlg.ShowModal();
}

int Message::Warning(const std::string &msg, const std::string &emsg,
    long button, long icon)
{
    long style = button | wxCENTER | icon;
    wxMessageDialog msgdlg(Message::parent, msg, Message::title, style);
    if (!emsg.empty()) {
        msgdlg.SetExtendedMessage(emsg);
    }
    return msgdlg.ShowModal();
}

int Message::Error(const std::string &msg, const std::string &emsg,
    long button, long icon)
{
    long style = button | wxCENTER | icon;
    wxMessageDialog msgdlg(Message::parent, msg, Message::title, style);
    if (!emsg.empty()) {
        msgdlg.SetExtendedMessage(emsg);
    }
    return msgdlg.ShowModal();
}

int Message::Question(const std::string &msg, const std::string &emsg,
        long button, long icon)
{
    long style = button | wxCENTER | icon;
    wxMessageDialog msgdlg(Message::parent, msg, Message::title, style);
    if (!emsg.empty()) {
        msgdlg.SetExtendedMessage(emsg);
    }
    return msgdlg.ShowModal();
}

void Message::NotifyIf(bool b, const std::string &title, const std::string &msg)
{
    if (b) {
        note.SetTitle(title);
        note.SetMessage(msg);
        note.Show();
    }
}

int Message::InfoIf(bool b, const std::string &msg, const std::string &emsg)
{
    if (b) {
        long style = wxOK | wxCENTER | Message::information;
        wxMessageDialog msgdlg(Message::parent, msg, Message::title, style);
        if (!emsg.empty()) {
            msgdlg.SetExtendedMessage(emsg);
        }
        return msgdlg.ShowModal();
    } else {
        return 0;
    }
}

int Message::WarningIf(bool b, const std::string &msg, const std::string &emsg)
{
    if (b) {
        long style = wxOK | wxCENTER | Message::exclamation;
        wxMessageDialog msgdlg(Message::parent, msg, Message::title, style);
        if (!emsg.empty()) {
            msgdlg.SetExtendedMessage(emsg);
        }
        return msgdlg.ShowModal();
    } else {
        return 0;
    }
}

int Message::ErrorIf(bool b, const std::string &msg, const std::string &emsg)
{
    if (b) {
        long style = wxOK | wxCENTER | Message::error;
        wxMessageDialog msgdlg(Message::parent, msg, Message::title, style);
        if (!emsg.empty()) {
            msgdlg.SetExtendedMessage(emsg);
        }
        return msgdlg.ShowModal();
    } else {
        return 0;
    }
}

int Message::QuestionIf(bool b, const std::string &msg, const std::string &emsg,
        long button, long icon)
{
    if (b) {
        long style = button | wxCENTER | icon;
        wxMessageDialog msgdlg(Message::parent, msg, Message::title, style);
        if (!emsg.empty()) {
            msgdlg.SetExtendedMessage(emsg);
        }
        return msgdlg.ShowModal();
    } else {
        return 0;
    }
}

Message::Message()
{
    iconStyle = Message::information;
    buttonsStyle = wxOK;
}

Message::Message(const std::string& m, const std::string& em, long b, long icon) :
    msg(m), emsg(em)
{
    iconStyle = Message::information;
    buttonsStyle = wxOK;
}

Message::~Message() { }

void Message::SetParent(wxWindow* p)
{
    ASSERT(p, "Message dialog parent should not be null.");
    parent = p;
    note.SetParent(p);
}

void Message::SetTitle(const std::string& s)
{
    title = s;
}

void Message::SetMessage(const std::string &s)
{
    msg = s;
}

void Message::SetExtMessage(const std::string &s)
{
    emsg = s;
}

void Message::SetIcon(long n)
{
    iconStyle = n;
}

void Message::SetButtons(long n)
{
    buttonsStyle = n;
}

int Message::Show()
{
    long style = wxCENTER | iconStyle | buttonsStyle;
    wxMessageDialog msgdlg(Message::parent, msg, Message::title, style);
    if (emsg.length() > 0) {
        msgdlg.SetExtendedMessage(emsg);
    }
    return msgdlg.ShowModal();
}

int Message::ShowIf(bool b)
{
    return b ? Show() : 0;
}

} //_ namespace negerns
