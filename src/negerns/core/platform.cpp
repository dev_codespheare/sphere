#include <wx/string.h>
#include <negerns/core/platform.h>

namespace negerns {

PlatformInfo::PlatformInfo()
{
#ifdef __WXMSW__
    SYSTEM_INFO si;
    OSVERSIONINFOEX osvi;

    ZeroMemory(&si, sizeof(SYSTEM_INFO));
    // Before calling the GetVersionEx function, set the dwOSVersionInfoSize
    // member of the structure as appropriate to indicate which data structure
    // is being passed to this function (OSVERSIONINFO or OSVERSIONINFOEX).
    ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

    // MSDN http://msdn.microsoft.com/en-us/library/windows/desktop/ms724451%28v=vs.85%29.aspx
    BOOL status = GetVersionEx((OSVERSIONINFO*) &osvi);
    if (status) {
        PGNSI pGNSI;
        // Call GetNativeSystemInfo if supported or GetSystemInfo otherwise.
        pGNSI = (PGNSI) GetProcAddress(GetModuleHandle(TEXT("kernel32.dll")), "GetNativeSystemInfo");
        if (NULL != pGNSI) {
            pGNSI(&si);
        } else {
            GetSystemInfo(&si);
        }
        GetWindowsPlatformInfo(osvi, si);
    }
#endif
}

#ifdef __WXMSW__

void PlatformInfo::GetWindowsPlatformInfo(const OSVERSIONINFOEX& osvi, const SYSTEM_INFO& si)
{
    versionMajor = osvi.dwMajorVersion;
    versionMinor = osvi.dwMinorVersion;
    versionRevision = osvi.dwBuildNumber;

    if (osvi.dwMajorVersion >= 6) {
        if (si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64) {
            architecture = "64-bit";
        } else if (si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_INTEL) {
            architecture = "32-bit";
        }
    }

    wxString tmp(osvi.szCSDVersion);
    servicePack =  tmp.ToStdString();

    if (VER_PLATFORM_WIN32_NT == osvi.dwPlatformId && osvi.dwMajorVersion > 4) {
        osName = "Microsoft ";

        // Test for the specific product.

        if (osvi.dwMajorVersion == 6) {
            if (osvi.dwMinorVersion == 0) {
                if (osvi.wProductType == VER_NT_WORKSTATION) {
                    osName.append("Windows Vista ");
                } else {
                    osName.append("Windows Server 2008 ");
                }
            }

            if (osvi.dwMinorVersion == 1) {
                if (osvi.wProductType == VER_NT_WORKSTATION) {
                    osName.append("Windows 7 ");
                } else {
                    osName.append("Windows Server 2008 R2 ");
                }
            }

            PGPI pGPI;
            unsigned long dwType;
            pGPI = (PGPI) GetProcAddress(GetModuleHandle(TEXT("kernel32.dll")), "GetProductInfo");
            pGPI(osvi.dwMajorVersion, osvi.dwMinorVersion, 0, 0, &dwType);

            switch (dwType) {
                case PRODUCT_ULTIMATE:
                    osName.append("Ultimate Edition");
                    break;
// MSDN http://msdn.microsoft.com/en-us/library/windows/desktop/ms724358%28v=vs.85%29.aspx
#ifndef PRODUCT_PROFESSIONAL
#define PRODUCT_PROFESSIONAL 0x00000030
                case PRODUCT_PROFESSIONAL:
                    osName.append("Professional");
                    break;
#endif
                case PRODUCT_HOME_PREMIUM:
                    osName.append("Home Premium Edition");
                    break;
                case PRODUCT_HOME_BASIC:
                    osName.append("Home Basic Edition");
                    break;
                case PRODUCT_ENTERPRISE:
                    osName.append("Enterprise Edition");
                    break;
                case PRODUCT_BUSINESS:
                    osName.append("Business Edition");
                    break;
                case PRODUCT_STARTER:
                    osName.append("Starter Edition");
                    break;
                case PRODUCT_CLUSTER_SERVER:
                    osName.append("Cluster Server Edition");
                    break;
                case PRODUCT_DATACENTER_SERVER:
                    osName.append("Datacenter Edition");
                    break;
                case PRODUCT_DATACENTER_SERVER_CORE:
                    osName.append("Datacenter Edition (core installation)");
                    break;
                case PRODUCT_ENTERPRISE_SERVER:
                    osName.append("Enterprise Edition");
                    break;
                case PRODUCT_ENTERPRISE_SERVER_CORE:
                    osName.append("Enterprise Edition (core installation)");
                    break;
                case PRODUCT_ENTERPRISE_SERVER_IA64:
                    osName.append("Enterprise Edition for Itanium-based Systems");
                    break;
                case PRODUCT_SMALLBUSINESS_SERVER:
                    osName.append("Small Business Server");
                    break;
                case PRODUCT_SMALLBUSINESS_SERVER_PREMIUM:
                    osName.append("Small Business Server Premium Edition");
                    break;
                case PRODUCT_STANDARD_SERVER:
                    osName.append("Standard Edition");
                    break;
                case PRODUCT_STANDARD_SERVER_CORE:
                    osName.append("Standard Edition (core installation)");
                    break;
                case PRODUCT_WEB_SERVER:
                    osName.append("Web Server Edition");
                    break;
            }
        }

        if (osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2) {
#ifndef VER_SUITE_WH_SERVER
#define VER_SUITE_WH_SERVER 0x00008000
            if (GetSystemMetrics(SM_SERVERR2)) {
                osName.append("Windows Server 2003 R2, ");
            } else if (osvi.wSuiteMask & VER_SUITE_STORAGE_SERVER) {
                osName.append("Windows Storage Server 2003");
            } else if (osvi.wSuiteMask & VER_SUITE_WH_SERVER) {
                osName.append("Windows Home Server");
            } else if (osvi.wProductType == VER_NT_WORKSTATION && si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64) {
                osName.append("Windows XP Professional x64 Edition");
            } else {
                osName.append("Windows Server 2003, ");
            }
#endif

            // Test for the server type.
            if (osvi.wProductType != VER_NT_WORKSTATION) {
                if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_IA64) {
                    if (osvi.wSuiteMask & VER_SUITE_DATACENTER) {
                        osName.append("Datacenter Edition for Itanium-based Systems");
                    } else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE) {
                        osName.append("Enterprise Edition for Itanium-based Systems");
                    }
                } else if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64) {
                    if (osvi.wSuiteMask & VER_SUITE_DATACENTER) {
                        osName.append("Datacenter x64 Edition");
                    } else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE) {
                        osName.append("Enterprise x64 Edition");
                    } else {
                        osName.append("Standard x64 Edition");
                    }
                } else {
                    if (osvi.wSuiteMask & VER_SUITE_COMPUTE_SERVER) {
                        osName.append("Compute Cluster Edition");
                    } else if (osvi.wSuiteMask & VER_SUITE_DATACENTER) {
                        osName.append("Datacenter Edition");
                    } else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE) {
                        osName.append("Enterprise Edition");
                    } else if (osvi.wSuiteMask & VER_SUITE_BLADE) {
                        osName.append("Web Edition");
                    } else {
                        osName.append("Standard Edition");
                    }
                }
            }
        }

        if (osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1) {
            osName.append("Windows XP ");
            if (osvi.wSuiteMask & VER_SUITE_PERSONAL) {
                osName.append("Home Edition");
            } else {
                osName.append("Professional");
            }
        }

        if (osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0) {
            osName.append("Windows 2000 ");
            if (osvi.wProductType == VER_NT_WORKSTATION) {
                osName.append("Professional");
            } else  {
                if (osvi.wSuiteMask & VER_SUITE_DATACENTER) {
                    osName.append("Datacenter Server");
                } else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE) {
                    osName.append("Advanced Server");
                } else {
                    osName.append("Server");
                }
            }
        }
    }
}

#endif

} //_ namespace negerns
