#include <iostream>
#include <string>
#include <vector>

#include <negerns/core/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/string/const.h>
#include <negerns/core/string/format.h>

#include <negerns/negerns.h>

#include <negernsx/core/string/format_main.h>
#include <negernsx/core/var_main.h>

using namespace std;

void func()
{
    vector<char> arr;
    arr.reserve(6);
    const char *ptr = "hello";
    arr.insert(arr.end(), ptr, ptr + 5);
    std::string s(arr.begin(), arr.end());
    cout << "Value: " << s << "\n";
}


int main()
{
    n::assert::enable(true);
    n::assert::use_gui(false);
    std::cout << "          Assertion settings:\n";
    std::cout << "            - Enabled: " << n::string::to_string(n::assert::is_enabled()) << "\n";
    std::cout << "            - Console: " << n::string::to_string(!n::assert::is_gui()) << "\n";

    n::log::enable(false);
    n::log::tracing::enable(false);
    n::log::stdoutput::enable(true);
    n::log::stdoutput::only(true);

    std::cout << "          Log settings:\n";
    std::cout << "            - Enabled: " << n::string::to_string(n::log::is_enabled()) << "\n";
    std::cout << "            - Trace:   " << n::string::to_string(n::log::tracing::is_enabled()) << "\n";
    std::cout << "            - Console: " << n::string::to_string(n::log::stdoutput::is_enabled()) << "\n";
    std::cout << "          ------------------\n";

    cout << "Negerns Experimental" << n::string::eol;
    cout << "--------------------" << n::string::eol;

    negernsx::string::format_main();
    negernsx::var_main();

    cout << std::endl << "End." << std::endl;
    return 0;
}
