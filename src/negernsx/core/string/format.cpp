#include <boost/logic/tribool.hpp>
#include <negerns/core/utility.h>
#include <negerns/core/debug.h>

#include <negernsx/support/modp_numtoa.h>
#include <negernsx/core/string/format.h>

#include <negerns/negerns.h>

namespace negernsx {
namespace string {
namespace format {

static const char DIGITS[] =
    "0001020304050607080910111213141516171819"
    "2021222324252627282930313233343536373839"
    "4041424344454647484950515253545556575859"
    "6061626364656667686970717273747576777879"
    "8081828384858687888990919293949596979899";

// Powers of 10
// 10^0 to 10^9
static const double pow10[] = {
    1,
    10,
    100,
    1000,
    10000,
    100000,
    1000000,
    10000000,
    100000000,
    1000000000
};

static const double thres_max = (double) (0x7FFFFFFF);

typedef std::map<char, Sign> SignMap;
typedef std::map<char, Conversion> ConversionMap;

static SignMap signMap = {
    { '-', Sign::Negative },
    { '+', Sign::Positive },
    { '=', Sign::Space }
};

static ConversionMap conversionMap = {
    { 'b', Conversion::Binary },
    { 'x', Conversion::HexLowerCase },
    { 'X', Conversion::HexUpperCase },
    { 'o', Conversion::Octal },
    { 't', Conversion::TrueFalseLowerCase },
    { 'T', Conversion::TrueFalseUpperCase },
    { 'y', Conversion::YesNoLowerCase },
    { 'Y', Conversion::YesNoUpperCase }
};

#if 0
static void strreverse(char* begin, char* end)
{
    char aux;
    while (end > begin)
        aux = *end, *end-- = *begin, *begin++ = aux;
}
#endif



// -----------------------------------------------------------------------------



#if 0
// Format the number with grouping
// From: http://stackoverflow.com/questions/7257956/how-to-insert-spaces-in-a-big-number-to-make-it-more-readable
//
// \see http://www.gnu.org/software/libc/manual/html_node/Formatting-Numbers.html
void fmtn_i(std::string &s, char sep = ',')
{
    // Loop until the end of the string and use j to keep track of every
    // third loop starting taking into account the leading x digits (this probably
    // can be rewritten in terms of just i, but it seems more clear when you use
    // a seperate variable)
    for (int i = 0, j = 3 - s.length() % 3; i < s.length(); ++i, ++j)
        if (i != 0 && j % 3 == 0)
            s.insert(i++, 1, sep);
}
#endif

inline
unsigned get_xdigit_count(uint64_t n, const uint8_t shift)
{
    unsigned digitCount = 0;
    do {
        ++digitCount;
    } while (n >>= shift);
    return digitCount;
}

std::size_t parse_bool(Buffer &v, bool b)
{
    char *ptr = &v[0];
    if (b) {
        v.resize(4);
        *ptr = 't';
        *(ptr + 1) = 'r';
        *(ptr + 2) = 'u';
        *(ptr + 3) = 'e';
        return 4;
    } else {
        v.resize(5);
        *ptr = 'f';
        *(ptr + 1) = 'a';
        *(ptr + 2) = 'l';
        *(ptr + 3) = 's';
        *(ptr + 4) = 'e';
        return 5;
    }
}

//! Set the sign to the buffer pointer.
//! Return 0 if there is a sign. Otherwise, return -1.
int set_sign(char *ptr, bool polarity, Sign sign)
{
    switch (sign) {
        case Sign::Negative:
            if (polarity == negative) {
                *ptr++ = '-';
                return 0;
            }
            break;
        case Sign::Positive:
            *ptr++ = polarity ? '+' : '-';
            return 0;
        case Sign::Space:
            *ptr++ = polarity ? ' ' : '-';
            return 0;
        default:
            NO_SWITCH_DEFAULT;
    }
    return -1;
}

std::size_t parse_unsigned(Buffer &v, uint64_t value, bool polarity, Sign sign)
{
    char *ptr = &v[0];
    auto length = n::string::count_digits(value) + 1;
    v.resize(length);
    length += set_sign(ptr, polarity, sign);
    ptr += length;

    unsigned index = 0;
    // Integer division is slow so do it for a group of two digits
    // instead of for every digit. The idea comes from the talk by
    // Alexandrescu; "Three Optimization Tips for C++".
    // https://www.facebook.com/notes/facebook-engineering/10151361643253920
    while (value >= 100) {
        index = (value % 100) * 2;
        value /= 100;
        *--ptr = DIGITS[index + 1];
        *--ptr = DIGITS[index];
    }

    if (value < 10) {
        *--ptr = static_cast<char>('0' + value);
        return length;
    }

    index = value * 2u;
    *--ptr = DIGITS[index + 1];
    *--ptr = DIGITS[index];

    return length;
}

std::size_t parse_float(Buffer &v, long double value, uint8_t prec, Sign sign)
{
    if (!std::isfinite(value)) {
        if (std::isnan(value)) {
            v.resize(3);
            v[0] = 'N';
            v[1] = 'a';
            v[2] = 'N';
            return 3;
        } else if (std::isinf(value)) {
            v.resize(3);
            v[0] = 'I';
            v[1] = 'N';
            v[2] = 'F';
            return 3;
        } else {
            const char *p = "!Nan && !INF";
            auto len = std::strlen(p);
            v.resize(len);
            char *ptr = &v[0];
            std::copy_n(p, len, ptr);
            return len;
        }
    }

    // We'll work in positive values and deal with the negative later
    bool positive = true;
    if (value < 0.0L) {
        positive = false;
        value = -value;     // make positive
    }

    // If input is larger than thres_max, revert to exponential.
    // For very large numbers switch back to native sprintf for
    // exponentials. Anyone want to write code to replace this?
    //
    // Normal printf behavior is to print EVERY whole number digit
    // which can be 100s of characters overflowing your buffers == bad
    if (value > thres_max) {
        //: FIXME: The following code produces an error.
        v.resize(13);
        char *ptr = &v[0];
        if (positive) {
            sprintf(ptr, "%e", value);
        } else {
            *ptr++ = '-';
            sprintf(ptr, "%e", -value);
        }
        return v.size();
    }

    enum { FRACTION_LENGTH = 9 };
    if (prec > FRACTION_LENGTH) {
        // Precision of >= 10 can lead to overflow errors
        prec = FRACTION_LENGTH;
    }

    // Can we make whole unsigned? Or maybe because int is faster.
    int whole = static_cast<int>(value);
    double tmp = (value - whole) * pow10[prec];
    uint32_t frac = static_cast<uint32_t>(tmp);
    double diff = tmp - frac;

    if (diff > 0.5) {
        ++frac;
        // Handle rollover, e.g.  case 0.99 with prec 1 is 1.0
        if (frac >= pow10[prec]) {
            frac = 0;
            ++whole;
        }
    } else if (diff == 0.5 && ((frac == 0u) || (frac & 1u))) {
        // If halfway, round up if odd, or if last digit is 0.
        // That last part is strange.
        ++frac;
    }

    // Whole number, precision, sign and decimal point.
    char *ptr = &v[0];
    auto length = n::string::count_digits(whole) + prec + 2;
    v.resize(length);
    length += set_sign(ptr, positive, sign);
    ptr += length;

    if (prec == 0) {
        diff = value - whole;
        if (diff > 0.5) {
            // Greater than 0.5, round up, e.g. 1.6 -> 2
            ++whole;
        } else if (diff == 0.5 && (whole & 1)) {
            // Exactly 0.5 and ODD, then round up
            // 1.5 -> 2, but 2.5 -> 2
            ++whole;
        }
    } else {

        // Now do fractional part, as an unsigned number

        if (frac <= pow10[8] - 1) {
            int zeroes = prec - n::string::count_digits(frac);
            while (zeroes-- > 0) *--ptr = '0';
#if 0
            ptr -= zeroes;
            std::fill_n(ptr, zeroes, '0');
            ptr -= zeroes;
#endif
        }

        // Integer division is slow so do it for a group of two digits
        // instead of for every digit. The idea comes from the talk by
        // Alexandrescu; "Three Optimization Tips for C++".
        // https://www.facebook.com/notes/facebook-engineering/10151361643253920
        while (frac >= 100) {
            unsigned index = (frac % 100) * 2;
            frac /= 100;
            *--ptr = DIGITS[index + 1];
            *--ptr = DIGITS[index];
        }

        if (frac < 10) {
            *--ptr = static_cast<char>('0' + frac);
        } else {
            unsigned index = static_cast<unsigned>(frac * 2);
            *--ptr = DIGITS[index + 1];
            *--ptr = DIGITS[index];
        }

        // Add decimal point. Move one character before since str points
        // to the first character.
        *--ptr = '.';
    }

    // Do whole part
    // Take care of sign

    // Integer division is slow so do it for a group of two digits
    // instead of for every digit. The idea comes from the talk by
    // Alexandrescu; "Three Optimization Tips for C++".
    // https://www.facebook.com/notes/facebook-engineering/10151361643253920
    while (whole >= 100) {
        unsigned index = (whole % 100) * 2;
        whole /= 100;
        *--ptr = DIGITS[index + 1];
        *--ptr = DIGITS[index];
    }

    if (whole < 10) {
        *--ptr = static_cast<char>('0' + whole);
    } else {
        unsigned index = static_cast<unsigned>(whole * 2);
        *--ptr = DIGITS[index + 1];
        *--ptr = DIGITS[index];
    }

    return length;
}

std::size_t parse_to_hex(Buffer &v, uint64_t n, char symbol)
{
    //. TODO: Add sign
    auto num_digits = get_xdigit_count(n, 4) + 2;
    v.resize(num_digits);
    char *ptr = &v[0];

    v[0] = '0';
    v[1] = symbol;

    const char *hex = symbol == 'x' ? "0123456789abcdef" : "0123456789ABCDEF";
    ptr += num_digits - 1;
    do {
        *ptr-- = hex[n & 0xf];      // Convert and move left
    } while (n >>= 4);

    return v.size();
}

std::size_t parse_to_bin(Buffer &v, uint64_t n)
{
    //. TODO: Add sign
    auto num_digits = get_xdigit_count(n, 1) + 2;
    v.resize(num_digits);
    char *ptr = &v[0];

    v[0] = '0';
    v[1] = 'b';

    ptr += num_digits - 1;
    do {
        *ptr-- = '0' + (n & 1);     // Convert and move left
    } while (n >>= 1);

    return v.size();
}

std::size_t parse_to_octal(Buffer &v, uint64_t n)
{
    //. TODO: Add sign
    auto num_digits = get_xdigit_count(n, 3) + 1;
    v.resize(num_digits);
    char *ptr = &v[0];

    v[0] = 'o';

    ptr += num_digits - 1;
    do {
        *ptr-- = '0' + (n & 7);     // Convert and move left
    } while (n >>= 3);

    return v.size();
}

std::size_t get_as_fstring(Buffer &v, const n::Var *arg, uint8_t prec, FormatSpec spec)
{
    //. TODO: Add type category so we can use it here get_type_category.
    switch (arg->get_type()) {
        case n::variant::Type::Bool:
            return parse_bool(v, arg->get_uint() != 0);
        case n::variant::Type::Char:
        case n::variant::Type::SChar:
        case n::variant::Type::Short:
        case n::variant::Type::Integer:
        case n::variant::Type::Long:
        case n::variant::Type::LLong: {
            auto n = arg->as_llong();
            switch (spec.conversion) {
                case Conversion::None:
                    if (n < 0LL) {
                        return parse_unsigned(v, -n, negative, spec.sign);
                    } else {
                        return parse_unsigned(v, n, positive, spec.sign);
                    }
                case Conversion::HexLowerCase:
                    return parse_to_hex(v, n, 'x');
                case Conversion::HexUpperCase:
                    return parse_to_hex(v, n, 'X');
                case Conversion::Binary:
                    return parse_to_bin(v, n);
                case Conversion::Octal:
                    return parse_to_octal(v, n);
                default:
                    NO_SWITCH_DEFAULT;
            }
        }
        case n::variant::Type::UChar:
        case n::variant::Type::UShort:
        case n::variant::Type::UInteger:
        case n::variant::Type::ULong:
        case n::variant::Type::ULLong: {
            auto n = arg->get_ullong();
            switch (spec.conversion) {
                case Conversion::None:
                    return parse_unsigned(v, n, positive, spec.sign);
                case Conversion::HexLowerCase:
                    return parse_to_hex(v, n, 'x');
                case Conversion::HexUpperCase:
                    return parse_to_hex(v, n, 'X');
                case Conversion::Binary:
                    return parse_to_bin(v, n);
                case Conversion::Octal:
                    return parse_to_octal(v, n);
                default:
                    NO_SWITCH_DEFAULT;
            }
        }
        // Must be checked ahead of the other floating point
        // types because we do not want a decimal value being
        // rounded off when converted to a floating point type.
        case n::variant::Type::Fixed2: {
            auto n = arg->get_fixed2();
            std::string s(n::decimal::to_string(n));
            auto len = s.length();
            v.resize(len);
            char *ptr = &v[0];
            std::copy(s.begin(), s.end(), ptr);
            return len;
        }
        case n::variant::Type::Float:
        case n::variant::Type::Double:
        case n::variant::Type::LDouble: {
            auto n = arg->as_ldouble();
            return parse_float(v, n, prec, spec.sign);
        }
        case n::variant::Type::ShortString:
        case n::variant::Type::String: {
            const char *p = arg->get_charptr();
            auto len = std::strlen(p);
            if (len > v.capacity()) {
                len = v.capacity();
            }
            v.resize(len);
            char *ptr = &v[0];
            std::copy_n(p, len, ptr);
            return len;
        }
        case n::variant::Type::Tm: {
            auto time = arg->get_tm();
            auto s(negerns::date::to_string(time));
            auto len = s.length();
            v.resize(len);
            char *ptr = &v[0];
            std::copy(s.begin(), s.end(), ptr);
            return len;
        }
        default:
            NO_SWITCH_DEFAULT;
    } //_ switch type
    return 0;
}

static const unsigned int maxWidth = 20;
static const unsigned int maxPrecision = 9;
static const unsigned int MAX_BUF = 1023;

std::string formatx(const char *p, const std::vector<n::Var> &args)
{
    char *bufptr = nullptr;
    size_t argsCount = args.size() - 1;
    FormatSpec spec;
    bool needsWSep = true;

    boost::container::static_vector<char, MAX_BUF> buf;
    Buffer temp;

    bufptr = &buf[0];

    while (*p != '\0') {

        // Get leading string before argument
        // -----------------------------------------

        if (*p != '%') {
            *bufptr++ = *p++;
            continue;
        }

        ++p;

        if (*p == '%') {
            // Double percent %% encountered, send the percent symbol %
            // to the output buffer
            *bufptr++ = *p++;
            continue;
        } else if (*p >= '0' && *p <= '9') {

            needsWSep = true;
            spec.alignment = Alignment::Left;
            spec.sign = Sign::Default;
            spec.width = 0;
            spec.precision = static_cast<uint8_t>(Precision::Default);
            spec.conversion = Conversion::None;

            // Get argument index
            // -----------------------------------------

            //' NOTE:
            //' If the index is zero or beyond the number of passed
            //' arguments then we just pass through all the format
            //' specifiers. The argument formatting will be skipped
            //' below.

            {
                spec.index = (*p - '0');
                ++p;
                int_fast32_t digit_mult = 10;
                // Seldom does a specified index is more than 1 digit
                // So process other digits here.
                while (*p >= '0' && *p <= '9') {
                    spec.index *= digit_mult;
                    if (*p > '0') {
                        spec.index += (*p - '0');
                    }
                    digit_mult *= 10;
                    ++p;
                }
            }

            --spec.index;

            // Get alignment
            // Initial value already set above
            // -----------------------------------------

            if (*p == '<') {
                needsWSep = false;
                ++p;
            } else if (*p == '>') {
                needsWSep = false;
                spec.alignment = Alignment::Right;
                ++p;
            }

            // Get sign
            // Initial value already set above
            // -----------------------------------------

            if (*p == '-' || *p == '+' || *p == '=') {
                needsWSep = false;
                spec.sign = signMap[*p];
                ++p;
            }

            // Get width
            // -----------------------------------------

            if ((needsWSep && *p == '$') || (!needsWSep && *p == '$')) {
                ++p;
            }
            if (*p >= '0' && *p <= '9') {
                int_fast32_t digit_mult = 1;
                while (*p >= '0' && *p <= '9') {
                    spec.width *= digit_mult;
                    if (*p > '0') {
                        spec.width += (*p - '0');
                    }
                    digit_mult *= 10;
                    ++p;
                }
                if (spec.width > maxWidth) {
                    spec.width = maxWidth;
                }
            }

            // Get precision
            // -----------------------------------------

            if (*p == '.') {
                ++p;
                // Consider that precision is seldom more than 1 digit
                // Get only 1 digit (max is precision 9) and skip the rest
                if (*p >= '0' && *p <= '9') {
                    spec.precision = (*p - '0');
                    ++p;
                    while (*p >= '0' && *p <= '9') {
                        ++p;
                    }
                }
            }

            // Get conversion
            // -----------------------------------------

            // Conversion character is optional. Check for end of string.
            if (*p != '\0' && (*p == 'b' || *p == 'x' || *p == 'X' || *p == 'o'
                || *p == 't' || *p == 'T' || *p == 'y' || *p == 'Y')) {
                spec.conversion = conversionMap[*p];
                ++p;
            }
    #if 0
            if (is_end(p)) {
                p++;
            }
    #endif

            // Get argument as string
            // -----------------------------------------

            if (spec.index > argsCount) {
                continue;
            }

            using namespace negerns::string;
            const n::Var *arg = &args[spec.index];

            size_t len = get_as_fstring(temp, arg, spec.precision, spec);

            // Apply width and right alignment
            if (spec.width > len && spec.alignment == Alignment::Right) {
                int n = spec.width - len;
                std::fill_n(bufptr, n, ' ');
                bufptr += n;
            }

            std::copy(temp.begin(), temp.end(), bufptr);
            bufptr += len;

            // Apply width and left alignment
            if (spec.width > len && spec.alignment == Alignment::Left) {
                int n = spec.width - len;
                std::fill_n(bufptr, n, ' ');
                bufptr += n;
            }
        }
    } // while (true)

    *bufptr = '\0';
    bufptr = &buf[0];

    return std::string(static_cast<const char *>(bufptr));
}

} //_ namespace format
} //_ namespace string
} //_ namespace negernsx



namespace negernsx {
namespace string {

} //_ namespace string
} //_ namespace negernsx
