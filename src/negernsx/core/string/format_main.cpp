#include <iostream>
#include <limits>
#include <string>
#include <boost/logic/tribool.hpp>

#include <negerns/core/debug.h>
#include <negerns/core/decimal.h>
#include <negerns/core/timer.h>
#include <negerns/core/string.h>

#include <negerns/negerns.h>

#include <negernsx/core/string/format.h>
#include <negernsx/core/string/format_main.h>

namespace negernsx {
namespace string {

void format_main()
{
    std::cout << n::string::eol << "String Format:" << n::string::eol << n::string::eol;

    std::cout << "Bit sizes:" << n::string::eol << n::string::eol;

    std::cout << "char:      " << std::numeric_limits<char>::digits + 1 << n::string::eol;
    std::cout << "short:     " << std::numeric_limits<short>::digits + 1 << n::string::eol;
    std::cout << "int:       " << std::numeric_limits<int>::digits + 1 << n::string::eol;
    std::cout << "long:      " << std::numeric_limits<long>::digits + 1 << n::string::eol;
    std::cout << "long long: " << std::numeric_limits<long long>::digits + 1 << n::string::eol;
    std::cout << n::string::eol;

#if 0
    enum { Limit = std::numeric_limits<double>::digits10 + 3 + 100 };
    char buf[Limit];
    char *ptr = buf;

    size_t len = 0;

    {
        ptr = buf;
        memset(buf, 0, Limit);
        len = negernsx::string::format::parse_unsigned(ptr, 1234U, negernsx::string::format::positive);
        std::cout << "unsigned_to_string(): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        len = negernsx::string::format::parse_unsigned(ptr, 1234, negernsx::string::format::negative);
        std::cout << "unsigned_to_string(): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        len = negernsx::string::format::parse_float(ptr, 3.14159, 6);
        std::cout << "float_to_string(): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        len = negernsx::string::format::parse_float(ptr, 3.14159, 6);
        std::cout << "float_to_string(): " << ptr << std::endl;
    }

    // -------------------------------------------------------------------------

    {
        ptr = buf;
        memset(buf, 0, Limit);
        char ch = std::numeric_limits<char>::max();
        n::Var v(ch);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "char:                  " << negerns::string::to_string(ch) << std::endl;
        std::cout << "get_as_fstring (char): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        signed char ch = std::numeric_limits<signed char>::max();
        n::Var v(ch);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "signed char:                  " << negerns::string::to_string(ch) << std::endl;
        std::cout << "get_as_fstring (signed char): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        unsigned char ch = std::numeric_limits<unsigned char>::max();
        n::Var v(ch);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "unsigned char:                  " << negerns::string::to_string(ch) << std::endl;
        std::cout << "get_as_fstring (unsigned char): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        short n = std::numeric_limits<short>::max();
        n::Var v(n);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "short:                  " << negerns::string::to_string(n) << std::endl;
        std::cout << "get_as_fstring (short): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        ushort n = std::numeric_limits<unsigned short>::max();
        n::Var v(n);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "unsigned short:                  " << negerns::string::to_string(n) << std::endl;
        std::cout << "get_as_fstring (unsigned short): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        int n = std::numeric_limits<int>::max();
        n::Var v(n);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "int:                  " << negerns::string::to_string(n) << std::endl;
        std::cout << "get_as_fstring (int): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        unsigned int n = std::numeric_limits<unsigned int>::max();
        n::Var v(n);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "unsigned int:                  " << negerns::string::to_string(n) << std::endl;
        std::cout << "get_as_fstring (unsigned int): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        long n = std::numeric_limits<long>::max();
        n::Var v(n);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "long:                  " << negerns::string::to_string(n) << std::endl;
        std::cout << "get_as_fstring (long): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        unsigned long n = std::numeric_limits<unsigned long>::max();
        n::Var v(n);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "unsigned long:                  " << negerns::string::to_string(static_cast<uint64_t>(n)) << std::endl;
        std::cout << "get_as_fstring (unsigned long): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        long long n = std::numeric_limits<long long>::max();
        n::Var v(n);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "long long:                  " << negerns::string::to_string(n) << std::endl;
        std::cout << "get_as_fstring (long long): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        unsigned long long n = std::numeric_limits<unsigned long long>::max();
        n::Var v(n);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "usigned long long:                  " << negerns::string::to_string(n) << std::endl;
        std::cout << "get_as_fstring (unsigned long long): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        float n = 3.14159f;
        n::Var v(n);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "get_as_fstring (float): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        double n = 3.14159;
        n::Var v(n);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "get_as_fstring (double): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        n::Var v("abcdefg");
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "get_as_fstring (short string): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        long double n = 3.14159L;
        n::Var v(n);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "get_as_fstring (long double): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        n::dec2_t n(3.14159);
        n::Var v(n);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "get_as_fstring (decimal): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);
        n::Var v("abcdefghijklmnopqrstuvwxyz");
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "get_as_fstring (string): " << ptr << std::endl;
    }

    {
        ptr = buf;
        memset(buf, 0, Limit);

        std::time_t rawtime;
        std::time(&rawtime);
        std::tm tm_value = *std::localtime(&rawtime);

        n::Var v(tm_value);
        len = negernsx::string::format::get_as_fstring(ptr, &v, 6);
        std::cout << "get_as_fstring (tm): " << ptr << std::endl;
    }
#endif

    // -------------------------------------------------------------------------

#if 0
    {
        std::string s;
        DEBUG(negernsx::string::Format("%1",   true) ==  "true");
        DEBUG(negernsx::string::Format("%1",   false) == "false");

        DEBUG(negernsx::string::Format("%1$5", true) ==  "true ");
        DEBUG(negernsx::string::Format("%1<2", true) ==  "true");
        DEBUG(negernsx::string::Format("%1>5", true) ==  " true");
        DEBUG(negernsx::string::Format("%1<5", true) ==  "true ");

        DEBUG(negernsx::string::Format("%1$10", true) ==   "true      ");
        DEBUG(negernsx::string::Format("%1$10", false) ==  "false     ");
        DEBUG(negernsx::string::Format("%1<$10", true) ==  "true      ");
        DEBUG(negernsx::string::Format("%1<$10", false) == "false     ");

        DEBUG(negernsx::string::Format("%1>$10", true) ==  "      true");
        DEBUG(negernsx::string::Format("%1>$10", false) == "     false");
    }

    {
        std::string s;
        s = negernsx::string::Format("%1",  1);
        DEBUG(s ==  "1", "Value: [%1]", s);
        DEBUG(negernsx::string::Format("%1+", 1) ==  "+1");
        DEBUG(negernsx::string::Format("%1-", 1) ==  "1");
        DEBUG(negernsx::string::Format("%1=", 1) ==  " 1");

        DEBUG(negernsx::string::Format("%1+",  -1) == "-1");
        DEBUG(negernsx::string::Format("%1-",  -1) == "-1");
        DEBUG(negernsx::string::Format("%1=",  -1) == "-1");
    }

    {
        std::string s;
        s = negernsx::string::Format("%1",  1.0);
        DEBUG(s ==  "1.000000", "Value: [%1]", s);
        DEBUG(negernsx::string::Format("%1+", 1.0) ==  "+1.000000");
        DEBUG(negernsx::string::Format("%1-", 1.0) ==  "1.000000");
        DEBUG(negernsx::string::Format("%1=", 1.0) ==  " 1.000000");

        DEBUG(negernsx::string::Format("%1+",  -1.0) == "-1.000000");
        DEBUG(negernsx::string::Format("%1-",  -1.0) == "-1.000000");
        DEBUG(negernsx::string::Format("%1=",  -1.0) == "-1.000000");
    }
#endif

    {
        std::string s;

        s = negernsx::string::Format("%1$.9", 1.234000000);

        auto f = std::numeric_limits<double>::min();
        s = negernsx::string::Format("%1", std::numeric_limits<double>::min());
        std::cout << "double (min): " << s << std::endl;
        s = negernsx::string::Format("%1", std::numeric_limits<double>::max());
        std::cout << "double (max): " << s << std::endl;

        s = negernsx::string::Format("%1", std::numeric_limits<long double>::min());
        std::cout << "long double (min): " << s << std::endl;
        s = negernsx::string::Format("%1", std::numeric_limits<long double>::max());
        std::cout << "long double (max): " << s << std::endl;
    }

    {
        std::string s;
        s = negernsx::string::Format("%1",  "This is a very long string which should trigger something. I'm not sure what it is yet. So, here goes.");
        std::cout << "Long String: " << s << std::endl;
    }

    // -------------------------------------------------------------------------

    {
        auto n = std::numeric_limits<unsigned long long>::max();
        auto s = negernsx::string::Format("%1 %1b %1x %1o", n);
        std::cout << "Format: " << s << std::endl;
    }

    {
        auto s = negernsx::string::Format("%1$.2", 3.13f);
        std::cout << "Format: " << s << std::endl;
    }

    {
        auto amount = n::decimal::to_dec2("5.50");

        auto s1 = negerns::string::Format("%1$.9:%2>4:%3$.2:%4:%5X:%6:%7%%", 1.234000000, 42, 3.13f, "str", 57005, "X", amount);
        auto s2 = negerns::string::Format("%1$.9:%2>4:%3$.2:%4:%5X:%6:%7%%", 1.234000000, 42, 3.13f, "str", 57005, "X", amount);
        std::cout << "negerns::Format:  " << s1 << std::endl;
        std::cout << "negernsx::Format: " << s2 << std::endl;
        std::cout << "Result:           1.234000000:  42:3.13:str:0XDEAD:X:5.50%" << std::endl;

#if 0
        for (int i = 0; i < 1000; ++i) {
            negerns::string::Format("%1$.9:%2>4:%3$.2:%4:%5X:%6:%%", 1.234000000, 42, 3.13f, "str", 57005, "X");
        }
#endif
    }



    // -------------------------------------------------------------------------

#if 1
    {
#ifdef _DEBUG
        const std::size_t count = 100000;
#else
        const std::size_t count = 2000000;
#endif
        std::string r;

        {
            n::Timer sw;
            sw.start();
            for (std::size_t i = 0; i < count; ++i) {
                r = negerns::string::Format("%1$.9:%2>4:%3$.2:%4:%5X:%6%%", 1.234000000, 42, 3.13f, "str", 57005, "X");
            }
            sw.stop();
            auto times = sw.format("negerns::Format:  ");
            std::cout << times << std::endl;
        }

        {
            n::Timer sw;
            sw.start();
            for (std::size_t i = 0; i < count; ++i) {
                r = negernsx::string::Format("%1$.9:%2>4:%3$.2:%4:%5X:%6%%", 1.234000000, 42, 3.13f, "str", 57005, "X");
            }
            sw.stop();
            auto times = sw.format("negernsx::Format: ");
            std::cout << times << std::endl;
        }
    }
#endif
}

} //_ namespace string
} //_ namespace negernsx