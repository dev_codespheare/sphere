#include <negerns/core/debug.h>
#include <negerns/core/log.h>

#include <negernsx/core/var.h>

namespace negernsx {
namespace variant {

enum { FRACTION_LENGTH = 9 };

static const char DIGITS[] =
    "0001020304050607080910111213141516171819"
    "2021222324252627282930313233343536373839"
    "4041424344454647484950515253545556575859"
    "6061626364656667686970717273747576777879"
    "8081828384858687888990919293949596979899";

// Powers of 10
// 10^0 to 10^9
static const double pow10[] = {
    1,
    10,
    100,
    1000,
    10000,
    100000,
    1000000,
    10000000,
    100000000,
    1000000000
};

static const double thres_max = (double) (0x7FFFFFFF);

uint32_t count_digits(uint64_t n)
{
    // Three Optimization Tips for C++
    // By Andrei Alexandrescu
    // https://www.facebook.com/notes/facebook-engineering/three-optimization-tips-for-c/10151361643253920
    uint32_t count = 1;
    for (;;) {
        if (n < 10) return count;
        if (n < 100) return count + 1;
        if (n < 1000) return count + 2;
        if (n < 10000) return count + 3;
        // Skip ahead by 4 orders of magnitude
        n /= 10000u;
        count += 4;
    }
    //return count;
}

std::string unsigned_to_string(uint64_t value, boost::logic::tribool sign)
{
    // Buffer should be large enough to hold all digits (digits10 + 1),
    // a sign and a null character.
    enum { Limit = std::numeric_limits<uint64_t>::digits10 + 3 };
    char buf[Limit];
    char *ptr = buf + Limit - 1;
    *ptr = '\0';

    auto n = value;
    int length = 1;
    for (;;) {
        if (n < 10) { break; }
        if (n < 100) { length += 1; break; }
        if (n < 1000) { length += 2; break; }
        if (n < 10000) { length += 3; break; }
        // Skip ahead by 4 orders of magnitude
        n /= 10000u;
        length += 4;
    }
    length++;

    unsigned index = 0;
    while (value >= 100) {
        // Integer division is slow so do it for a group of two digits instead
        // of for every digit. The idea comes from the talk by Alexandrescu
        // "Three Optimization Tips for C++". See speed-test for a comparison.
        index = (value % 100) * 2;
        value /= 100;
        *--ptr = DIGITS[index + 1];
        *--ptr = DIGITS[index];
    }
    if (value < 10) {
        *--ptr = static_cast<char>('0' + value);
        if (!boost::logic::indeterminate(sign)) {
            *--ptr = sign ? '+' : '-';
        }
        return std::string(static_cast<const char *>(ptr));
    }
    index = static_cast<unsigned>(value * 2);
    *--ptr = DIGITS[index + 1];
    *--ptr = DIGITS[index];
    if (!boost::logic::indeterminate(sign)) {
        *--ptr = sign ? '+' : '-';
    }
    return std::string(static_cast<const char *>(ptr));
}

std::string float_to_string(double value, uint8_t prec, boost::logic::tribool sign)
{
    // Buffer should be large enough to hold all digits (digits10 + 1),
    // a sign and a null character.
    enum { Limit = std::numeric_limits<double>::digits10 + 3 };
    char buf[Limit];
    char *ptr = buf + Limit - 1;
    *ptr = '\0';

    // Hacky test for NaN
    // Under -fast-math this won't work, but then you also won't
    // have correct nan values anyways.  The alternative is
    // To link with libmath (bad) or hack IEEE double bits (bad)
    if (!(value == value)) {
        *--ptr = 'n';
        *--ptr = 'a';
        *--ptr = 'n';
        return std::string(static_cast<const char *>(ptr));
    }

    if (prec > FRACTION_LENGTH) {
        // Precision of >= 10 can lead to overflow errors
        prec = FRACTION_LENGTH;
    }

    // We'll work in positive values and deal with the negative later
    bool positive = true;
    if (value < 0) {
        positive = false;
        value = -value;     // make positive
    }

    // Can we make whole unsigned? Or maybe because int is faster.
    int whole = (int) value;
    double tmp = (value - whole) * pow10[prec];
    uint32_t frac = (uint32_t) (tmp);
    double diff = tmp - frac;

    if (diff > 0.5) {
        ++frac;
        // Handle rollover, e.g.  case 0.99 with prec 1 is 1.0
        if (frac >= pow10[prec]) {
            frac = 0;
            ++whole;
        }
    } else if (diff == 0.5 && ((frac == 0) || (frac & 1))) {
        // If halfway, round up if odd, or
        // If last digit is 0.  That last part is strange
        ++frac;
    }

    // If input is larger than thres_max, revert to exponential.
    // For very large numbers switch back to native sprintf for
    // exponentials. Anyone want to write code to replace this?
    //
    // Normal printf behavior is to print EVERY whole number digit
    // which can be 100s of characters overflowing your buffers == bad
    if (value > thres_max) {
        // IMPORTANT:
        // Is this safe? It will be writing values to str past the buffer.
        sprintf(ptr, "%e", positive ? value : -value);
        FAIL("value (%1) > thres_max", value);
        return std::string(static_cast<const char *>(ptr));
    }

    if (prec == 0) {
        diff = value - whole;
        if (diff > 0.5) {
            // Greater than 0.5, round up, e.g. 1.6 -> 2
            ++whole;
        } else if (diff == 0.5 && (whole & 1)) {
            // Exactly 0.5 and ODD, then round up
            // 1.5 -> 2, but 2.5 -> 2
            ++whole;
        }
    } else {

        // Now do fractional part, as an unsigned number

        if (frac <= pow10[8] - 1) {
            // Add trailing zeroes
            int zeroes = prec - count_digits(frac);
            while (zeroes-- > 0) *--ptr = '0';
        }

        while (frac >= 100) {
            // Integer division is slow so do it for a group of two digits instead
            // of for every digit. The idea comes from the talk by Alexandrescu
            // "Three Optimization Tips for C++". See speed-test for a comparison.
            unsigned index = (frac % 100) * 2;
            frac /= 100;
            *--ptr = DIGITS[index + 1];
            *--ptr = DIGITS[index];
        }
        if (frac < 10) {
            *--ptr = static_cast<char>('0' + frac);
        } else {
            unsigned index = static_cast<unsigned>(frac * 2);
            *--ptr = DIGITS[index + 1];
            *--ptr = DIGITS[index];
        }
        // Add decimal point. Move one character before since str points
        // to the first character.
        *--ptr = '.';
    }

    // Do whole part
    // Take care of sign

    // Algorithm adopted from Andrei Alexandrescu's Three Optimization Tips for C++
    // https://www.facebook.com/notes/facebook-engineering/three-optimization-tips-for-c/10151361643253920

    while (whole >= 100) {
        unsigned index = (whole % 100) * 2;
        whole /= 100;
        *--ptr = DIGITS[index + 1];
        *--ptr = DIGITS[index];
    }
    if (whole < 10) {
        *--ptr = static_cast<char>('0' + whole);
    } else {
        unsigned index = static_cast<unsigned>(whole * 2);
        *--ptr = DIGITS[index + 1];
        *--ptr = DIGITS[index];
    }
    if (!boost::logic::indeterminate(sign)) {
        *--ptr = positive ? '+' : '-';
    }
    return std::string(static_cast<const char *>(ptr));
}



char get_as_char(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            return v->char_;
        case Type::SChar:
            if (std::is_signed<char>::value) {
                return v->schar_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UChar:
            if (!std::is_signed<char>::value) {
                return v->uchar_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::Bool:
            return static_cast<char>(v->uint_ != 0 ? 1 : 0);
        default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

int8_t get_as_int8(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            if (std::is_signed<char>::value) {
                return v->char_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::SChar:
            return v->schar_;
        case Type::Bool:
            return static_cast<int8_t>(v->uint_ != 0 ? 1 : 0);
        default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

uint8_t get_as_uint8(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            if (std::is_signed<char>::value) {
                if (v->char_ >= 0) {
                    return static_cast<uint8_t>(v->char_);
                } else {
                    throw std::logic_error({"Invalid type ", TypeName(v->type)});
                }
            } else {
                return v->char_;
            }
        case Type::SChar:
            if (v->schar_ >= 0) {
                return static_cast<uint8_t>(v->schar_);
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UChar:
            return v->uchar_;
        case Type::Bool:
            return static_cast<uint8_t>(v->uint_ != 0 ? 1 : 0);
         default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

int16_t get_as_int16(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            return v->char_;
        case Type::SChar:
            return v->schar_;
        case Type::UChar:
            return v->uchar_;
        case Type::Bool:
            return static_cast<int16_t>(v->uint_ != 0 ? 1 : 0);
        case Type::Short:
            return v->short_;
        case Type::UShort:
            return v->ushort_;
        default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

uint16_t get_as_uint16(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            if (v->char_ >= 0) {
                return v->char_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::SChar:
            if (v->schar_ >= 0) {
                return v->schar_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UChar:
            return v->uchar_;
        case Type::Bool:
            return static_cast<uint16_t>(v->uint_ != 0 ? 1 : 0);
        case Type::Short:
            if (v->short_ >= 0) {
                return v->short_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UShort:
            return v->ushort_;
         default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

int32_t get_as_int32(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            return v->char_;
        case Type::SChar:
            return v->schar_;
        case Type::UChar:
            return v->uchar_;
        case Type::Bool:
            return static_cast<int32_t>(v->uint_ != 0 ? 1 : 0);
        case Type::Short:
            return v->short_;
        case Type::UShort:
            return v->ushort_;
        case Type::Integer:
            return v->int_;
        case Type::UInteger:
            return v->uint_;
        case Type::Long:
            return v->long_;
        default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

uint32_t get_as_uint32(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            if (v->char_ >= 0) {
                return v->char_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::SChar:
            if (v->schar_ >= 0) {
                return v->schar_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UChar:
            return v->uchar_;
        case Type::Bool:
            return static_cast<uint32_t>(v->uint_ != 0 ? 1 : 0);
        case Type::Short:
            if (v->short_ >= 0) {
                return v->short_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UShort:
            return v->ushort_;
        case Type::Integer:
            if (v->int_ >= 0) {
                return v->int_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UInteger:
            return v->uint_;
        case Type::Long:
            if (v->long_ >= 0) {
                return v->long_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::ULong:
            return v->ulong_;
        default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

int64_t get_as_int64(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            return v->char_;
        case Type::SChar:
            return v->schar_;
        case Type::UChar:
            return v->uchar_;
        case Type::Bool:
            return v->uint_ != 0 ? 1 : 0;
        case Type::Short:
            return v->short_;
        case Type::UShort:
            return v->ushort_;
        case Type::Integer:
            return v->int_;
        case Type::UInteger:
            return v->uint_;
        case Type::Long:
            return v->long_;
        case Type::ULong:
            return v->ulong_;
        case Type::LLong:
            return v->llong_;
        default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

uint64_t get_as_uint64(const Var *v)
{
    switch (v->type) {
        case Type::Char:
            if (v->char_ >= 0) {
                return v->char_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::SChar:
            if (v->schar_ >= 0) {
                return v->schar_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UChar:
            return v->uchar_;
        case Type::Bool:
            return static_cast<uint64_t>(v->uint_ != 0 ? 1 : 0);
        case Type::Short:
            if (v->short_ >= 0) {
                return v->short_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UShort:
            return v->ushort_;
        case Type::Integer:
            if (v->int_ >= 0) {
                return v->int_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::UInteger:
            return v->uint_;
        case Type::Long:
            if (v->long_ >= 0) {
                return v->long_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::ULong:
            return v->ulong_;
        case Type::LLong:
            if (v->llong_ >= 0) {
                return v->llong_;
            } else {
                throw std::logic_error({"Invalid type ", TypeName(v->type)});
            }
        case Type::ULLong:
            return v->ullong_;
        default:
            throw std::logic_error({"Invalid type ", TypeName(v->type)});
    }
}

uint64_t get_as_abs(const Var *v)
{
    DEBUG(v->is_type_integral());
    if (v->is_type_signed()) {
        auto n = v->as_llong();
        return n < 0 ? -n : n;
    } else {
        return v->as_ullong();
    }
}

std::string get_as_string(const Var *v, uint8_t prec)
{
    switch (v->type) {
        case Type::Char:
            return negerns::string::to_string(v->char_);
        case Type::SChar:
            return negerns::string::to_string(v->schar_);
        case Type::UChar:
            return negerns::string::to_string(v->uchar_);
        case Type::Bool:
            return v->uint_ != 0 ? "true" : "false";
        case Type::Short:
            if (v->short_ < 0) {
                return unsigned_to_string(-v->short_, negative);
            } else {
                return unsigned_to_string(v->short_);
            }
        case Type::UShort:
            return unsigned_to_string(v->ushort_);
        case Type::Integer:
            if (v->int_ < 0) {
                return unsigned_to_string(-v->int_, negative);
            } else {
                return unsigned_to_string(v->int_);
            }
        case Type::UInteger:
            return unsigned_to_string(v->uint_);
        case Type::Long:
            if (v->long_ < 0) {
                return unsigned_to_string(-v->long_, negative);
            } else {
                return unsigned_to_string(v->long_);
            }
        case Type::ULong:
            return unsigned_to_string(v->ulong_);
        case Type::LLong:
            if (v->llong_ < 0) {
                return unsigned_to_string(-v->llong_, negative);
            } else {
                return unsigned_to_string(v->llong_);
            }
        case Type::ULLong:
            return unsigned_to_string(v->ullong_);
        case Type::Float:
            if (v->float_ < 0.0f) {
                return float_to_string(v->float_, prec, negative);
            } else {
                return float_to_string(v->float_, prec);
            }
        case Type::Double:
            if (v->double_ < 0.0) {
                return float_to_string(v->double_, prec, negative);
            } else {
                return float_to_string(v->double_, prec);
            }
        case Type::ShortString:
            return &v->shortstr_[0];
        case Type::LDouble: {
                auto ld = static_cast<LDoubleHolder_t *>(v->content)->get();
                if (ld < 0.0L) {
                    return float_to_string(ld, prec, negative);
                } else {
                    return float_to_string(ld, prec);
                }
            }
        case Type::Fixed2:
            return negerns::decimal::to_string(static_cast<Dec2Holder_t *>(v->content)->get());
        case Type::String:
            return static_cast<CharPtrHolder_t*>(v->content)->get();
        case Type::Tm:
            return negerns::date::to_string(static_cast<TmHolder_t *>(v->content)->get());
        default:
            return std::string();
    }
}

std::string get_as_fstring(const Var *v, uint8_t prec)
{
    switch (v->type) {
        case Type::Char:
            return negerns::string::to_string(v->char_);
        case Type::SChar:
            return negerns::string::to_string(v->schar_);
        case Type::UChar:
            return negerns::string::to_string(v->uchar_);
        case Type::Bool:
            return v->uint_ != 0 ? "true" : "false";
        case Type::Short:
            if (v->short_ < 0) {
                return unsigned_to_string(-v->short_, negative);
            } else {
                return unsigned_to_string(v->short_, positive);
            }
        case Type::UShort:
            return unsigned_to_string(v->ushort_, positive);
        case Type::Integer:
            if (v->int_ < 0) {
                return unsigned_to_string(-v->int_, negative);
            } else {
                return unsigned_to_string(v->int_, positive);
            }
        case Type::UInteger:
            return unsigned_to_string(v->uint_, positive);
        case Type::Long:
            if (v->long_ < 0) {
                return unsigned_to_string(-v->long_, negative);
            } else {
                return unsigned_to_string(v->long_, positive);
            }
        case Type::ULong:
            return unsigned_to_string(v->ulong_, positive);
        case Type::LLong:
            if (v->llong_ < 0) {
                return unsigned_to_string(-v->llong_, negative);
            } else {
                return unsigned_to_string(v->llong_, positive);
            }
        case Type::ULLong:
            return unsigned_to_string(v->ullong_, positive);
        case Type::Float:
            if (v->float_ < 0.0f) {
                return float_to_string(v->float_, prec, negative);
            } else {
                return float_to_string(v->float_, prec, positive);
            }
        case Type::Double:
            if (v->double_ < 0.0f) {
                return float_to_string(v->double_, prec, negative);
            } else {
                return float_to_string(v->double_, prec, positive);
            }
        case Type::ShortString:
            return &v->shortstr_[0];
        case Type::LDouble: {
                auto ld = static_cast<LDoubleHolder_t *>(v->content)->get();
                if (ld < 0.0L) {
                    return float_to_string(ld, prec, negative);
                } else {
                    return float_to_string(ld, prec, positive);
                }
            }
        case Type::Fixed2:
            return negerns::decimal::to_string(static_cast<Dec2Holder_t *>(v->content)->get());
        case Type::String:
            return static_cast<CharPtrHolder_t*>(v->content)->get();
        case Type::Tm:
            return negerns::date::to_string(static_cast<TmHolder_t *>(v->content)->get());
        default:
            return std::string();
    }
}

} //_ namespace variant
} //_ namespace negerns



// -----------------------------------------------------------------------------



namespace negernsx {

Var::Var(const char *v)
{
    if (v == nullptr) {
        content = nullptr;
    } else {
        auto size = std::strlen(v);
        if (size < 8) {
            type = variant::Type::ShortString;
            std::memcpy(shortstr_, v, size + 1);
        } else {
            type = variant::Type::String;
            content = new CharPtrHolder_t(v);
        }
    }

}

Var::Var(const std::string &v)
{
    auto size = v.length();
    if (size < 8) {
        type = variant::Type::ShortString;
        v.copy(shortstr_, size);
        shortstr_[size] = string::null_char;
    } else {
        type = variant::Type::String;
        content = new CharPtrHolder_t(v.c_str());
    }
}

Var::Var(const Var &o)
{
    if (this != &o) {
        type = o.type;
        if (is_type_object()) {
            content = o.content->clone();
        } else {
            double_ = o.double_;
        }
    }
}

Var::Var(Var &&o)
{
    if (this != &o) {
        type = o.type;
        if (is_type_object()) {
            content = o.content;
        } else {
            double_ = o.double_;
        }
        o.type = variant::Type::Empty;
        o.content = nullptr;
    }
}

bool Var::operator == (const Var &rhs) const
{
    if (empty() && rhs.empty()) {
        return true;
    } else if ((empty() && !rhs.empty()) || (!empty() && rhs.empty())) {
        return false;
    } else if (is_char() && rhs.is_char()) {
        return get_char() == rhs.get_char();
    } else if (is_schar() && rhs.is_schar()) {
        return get_schar() == rhs.get_schar();
    } else if (is_uchar() && rhs.is_uchar()) {
        return get_uchar() == rhs.get_uchar();
    } else if (is_bool() && rhs.is_bool()) {
        return get_bool() == rhs.get_bool();
    } else if (is_short() && rhs.is_short()) {
        return get_short() == rhs.get_short();
    } else if (is_ushort() && rhs.is_ushort()) {
        return get_ushort() == rhs.get_ushort();
    } else if (is_int() && rhs.is_int()) {
        return get_int() == rhs.get_int();
    } else if (is_uint() && rhs.is_uint()) {
        return get_uint() == rhs.get_uint();
    } else if (is_long() && rhs.is_long()) {
        return get_long() == rhs.get_long();
    } else if (is_ulong() && rhs.is_ulong()) {
        return get_ulong() == rhs.get_ulong();
    } else if (is_llong() && rhs.is_llong()) {
        return get_llong() == rhs.get_llong();
    } else if (is_ullong() && rhs.is_ullong()) {
        return get_ullong() == rhs.get_ullong();
    } else if (is_float() && rhs.is_float()) {
        return get_float() == rhs.get_float();
    } else if (is_double() && rhs.is_double()) {
        return get_double() == rhs.get_double();
    } else if (is_ldouble() && rhs.is_ldouble()) {
        return get_ldouble() == rhs.get_ldouble();
    } else if (is_fixed2() && rhs.is_fixed2()) {
        return get_fixed2() == rhs.get_fixed2();
    } else if (is_type_string()) {
        return get_string() == rhs.get_string();
    } else if (is_tm()) {
        return get_tm() == rhs.get_tm();
    } else {
        throw std::logic_error("Unknown type");
    }
}

} //_ namespace negernsx

