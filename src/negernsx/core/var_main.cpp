#include <iostream>
#include <negerns/core/string/const.h>

#include <negerns/negerns.h>

#include <negernsx/core/var_main.h>

namespace negernsx {

void var_main()
{
    std::cout << n::string::eol << "Var:" << n::string::eol << n::string::eol;


}

} //_ namespace negernsx
