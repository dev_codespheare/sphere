#include <iostream>
#include <version.h>

// Outputs doxygen tag names with corresponding values.
// It is passed to doxygen as commandline arguments (see the project
// post-build script).
//
// This executable has been created just to ensure that project
// documentation is in sync with other application information.

int main()
{
    printf("PROJECT_NAME=\"%s\"\n", VER_SPHERE_NAME);
    printf("PROJECT_BRIEF=\"%s\"\n", VER_SPHERE_EDITION);
    printf("PROJECT_NUMBER=%s\n", VER_SPHERE_VERSION_STR);
    printf("ALIASES+=\"project_author=%s\" \"project_date=%s\"", VER_SPHERE_AUTHOR, VER_SPHERE_DATE);
}
