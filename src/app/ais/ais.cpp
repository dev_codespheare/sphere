#include <thread>
#include <wx/string.h>
#include <wx/xrc/xmlres.h>
#include <wx/xml/xml.h>
#include <wx/debug.h>
#include <negerns/core/log.h>
#include <negerns/core/output.h>
#include <negerns/core/debug.h>
#include <negerns/core/platform.h>
#include <negerns/core/system.h>
#include <negerns/core/config/property.h>
#include <negerns/gui/resource.h>
#include <version.h>
#include <sphere/ais/constants.h>
#include <sphere/ais/mediator.h>
#include <app/ais/ais.h>
#include <app/ais/version.h>

#include <negerns/negerns.h>

using namespace sphere::ais;

IMPLEMENT_APP(Ais)

const char * Ais::configFile = "ais.conf";
#ifdef _DEBUG
const char * Ais::traceFile = "ais_trace.conf";
#endif

Ais::Ais() :
    Application()
{
    SetAppName(VER_NAME_SHORT);
    SetAppDisplayName(VER_NAME_LONG);

    aboutInfo.SetName(GetAppDisplayName().ToStdString());
    aboutInfo.SetDescription(VER_FILEDESCRIPTION_STR);
    aboutInfo.SetEdition(VER_EDITION);
#ifdef _DEBUG
    auto s = n::Format("Version %1 %2 (%3)", VER_VERSION_STR,
        VER_SPHERE_SPECIALBUILD_STR,
        aboutInfo.GetConfiguration());
#else
    auto s = n::Format("Version %1 %2", aboutInfo.GetVersion(),
        VER_SPHERE_SPECIALBUILD_STR);
#endif
    aboutInfo.SetVersion(VER_VERSION_STR, s);
    aboutInfo.SetCopyright(VER_LEGAL_COPYRIGHT_STR);
    aboutInfo.SetWebSite(VER_SPHERE_WEBSITE);
#ifdef _DEBUG
    aboutInfo.SetEvaluation(true);
    aboutInfo.SetLicensee("Client");
#else
    aboutInfo.SetEvaluation(false);
#endif

    n::gui::Resource::Add(CONST_RESOURCE_XRC, CONST_RESOURCE_XRC_FILE_AIS);
    n::gui::Resource::Add(CONST_RESOURCE_IMAGE, CONST_RESOURCE_IMAGE_FILE_AIS);
}

bool Ais::OnInit(void)
{
    // Allow command line processing provided by wxWidgets
    if (!Application::OnInit()) {
        return false;
    }

    n::Log::Info("Loading application resources...");
    std::string url = n::gui::Resource::BuildUrl(CONST_RESOURCE_IMAGE, CONST_RESOURCE_ICON);
    n::Log::Info("Loading resource: %1", url);
    bool status = wxXmlResource::Get()->Load(url);
    ASSERT(status, "Could not load resource: %1", url);

    const int xrcCount = 5;
    const std::string xrc[xrcCount] = {
        CONST_RESOURCE_SYSTEM,
        CONST_RESOURCE_MENU,
        CONST_RESOURCE_MESSAGE,
        CONST_RESOURCE_ABOUT,
        CONST_RESOURCE_PANEL_EMPLOYEE_DETAILS
    };

    for (int i = 0; i < xrcCount; ++i) {
        url = n::gui::Resource::BuildUrl(CONST_RESOURCE_XRC, xrc[i]);
        n::Log::Info("Loading resource: %1", url);
        status = wxXmlResource::Get()->Load(url);
        ASSERT(status, "Could not load resource: %1", url);
    }
    n::Log::Info("Done loading application resources.");

    {
        auto *resource = wxXmlResource::Get();
        const wxXmlNode *node = resource->GetResourceNode("client");
        if (node) {
            std::string s = node->GetNodeContent().ToStdString();
            aboutInfo.SetLicensee(s);
        }
    }

    SetMediator(new Mediator());
    mediator->CreateControl();
    mediator->PostCreateControl();
    mediator->ApplyConfiguration();

    n::PlatformInfo platformInfo;
    n::Output::Send("%1", aboutInfo.GetName());
    n::Output::Send("Version: %1", aboutInfo.GetVersion());
    n::Output::Send("Architecture: %1 %2", aboutInfo.GetPlatform(),
        aboutInfo.GetConfiguration());
    n::Output::Send("Operating System: %1", platformInfo.get_os_name());
    n::Output::Send("Version: %1.%2.%3 %4", platformInfo.get_major_version(),
        platformInfo.get_minor_version(),
        platformInfo.get_revision(),
        platformInfo.get_service_pack());
#if 0
    n::Output::Send("Architecture: %6", platformInfo.GetArchitecture());
#endif
    n::Output::Send("Thread Count: %1\n", std::thread::hardware_concurrency());

#ifdef _DEBUG
    mediator->AutoLogIn();
#endif
    return true;
}

int Ais::OnExit()
{
    LOG_FUNCTION();
    auto *config = n::System::Configuration();
    ASSERT(config != nullptr, "Configuration instance is null.");
    if (config != nullptr) {
        config->Save(Ais::configFile);
    }
    return Application::OnExit();
}

void Ais::OnFatalException()
{
    LOG_FUNCTION();
    auto *config = n::System::Configuration();
    ASSERT(config != nullptr, "Configuration instance is null.");
    if (config != nullptr) {
        config->Save(Ais::configFile);
    }
    Application::OnFatalException();
}

void Ais::InitConfiguration()
{
    LOG_FUNCTION();
    negerns::Log::Info("AIS configuration initialization...");
    auto *config = n::System::Configuration();
    auto *pfile = new negerns::config::Property(Ais::configFile, true);
    config->Add(pfile, Priority::Configuration);
    config->Read(Ais::configFile);
#ifdef _DEBUG
    negerns::Log::Debug("AIS trace configuration initialization...");
    auto *tfile = new negerns::config::Property(Ais::traceFile);
    config->Add(tfile, Priority::Trace);
    config->Read(Ais::traceFile);
#endif
    sphere::common::Application::InitConfiguration();
}
