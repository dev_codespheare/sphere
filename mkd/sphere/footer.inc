</div>
</div>

<div id="bottom-wrapper">
<div id="bottom-wrapper-line" />
<div id="bottom-container">
<div id="bottom-left" class="small-text">

<section class="indent">

| About         | Help          | Community           | Products    |
|---------------|---------------|---------------------|-------------|
| Company       | User Manual   | Mailing List        | Cooperative |
| News          | Guide Books   | Report a Bug        | ERP         |
| Contact       | How-to        | Enhancement         |             |
|               | Demo          |                     |             |
|               | Tutorials     |                     |             |
|               | Presentations |                     |             |

\
\
All original content is licensed under the [Creative Commons Attribution-ShareAlike
3.0 Unported License][CCL] except that which is quoted from elsewhere or attributed
to others. In short, you may reproduce, reblog, and modify my content, but you must
provide proper attribution.

[![Creative Commons License][CCLi]] [CCL]

</div>
<div id="bottom-center"></div>
<div id="bottom-right" class="small-text">
Theme designed by negerns.
</div>
</div>
</div>

[CCL]: http://creativecommons.org/licenses/by-sa/3.0/
[CCLi]: http://i.creativecommons.org/l/by-sa/3.0/88x31.png style="border-width:0"
