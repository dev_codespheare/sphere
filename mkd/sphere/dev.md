CSS: layout.css
CSS: sphere.css

{{header.inc}}
#Development

<section id="left-column">

###wxWidgets

The project uses customized Visual Studio project files for building the library.
The customization was done during version 2.9.4. It prefixes the library files
with 'wx'.

In wxWidgets 3.0.0, the build has been customized to prefix library names
with 'wx' and try to ease the transition to future releases. This resulted in the
use of property sheets.

wxWidgets 3.0.1 started using property sheets for Visual Studio projects. This
is in line with the experiment with the wxWidgets 3.0.0 build. The following is
are the changes to the project files.

* Target Name to $(wxToolkitDllNamePrefix)_$(wxComponentName)$(wxToolkitDllNameSuffix) for lib/dll configurations
* Output File to $(OutDir)$(wxToolkitDllNamePrefix)_$(wxComponentName)$(wxToolkitDllNameSuffix)$(TargetExt) for lib/dll configurations
* Linker Input $(wxToolkitDllNamePrefix)_$(wxComponentName)$(wxToolkitDllNameSuffix).lib;... for dll configurations
* Linker Import Library $(OutDir)$(TargetName).lib for dll configurations

**Building wxWidgets**

* Go to `build/msw` directory.
* Load the Visual Studio solution.
* Build. (*Output library files will be under the lib directory*)

###Boost

The project uses Boost version 1.55:

* Go to boost root directory;
* Prepare the Boost.Build system by executing `bootstrap.bat`;
* Invoke `Boost.Build` by running `b2.exe`

<div class="indent">

**Note**: Do not use multiple options on Windows like this:

    b2 variant=debug,release link=static,shared address-model=32,64 threading=single,multi

Preferred invocations:

    b2 variant=debug   link=static address-model=32 threading=multi runtime-link=shared
    b2 variant=release link=static address-model=32 threading=multi runtime-link=shared
    b2 variant=debug   link=shared address-model=32 threading=multi runtime-link=shared
    b2 variant=release link=shared address-model=32 threading=multi runtime-link=shared
    b2 variant=debug   link=static address-model=64 threading=multi runtime-link=shared
    b2 variant=release link=static address-model=64 threading=multi runtime-link=shared
    b2 variant=debug   link=shared address-model=64 threading=multi runtime-link=shared
    b2 variant=release link=shared address-model=64 threading=multi runtime-link=shared

Boost::Log configuration macros:

`BOOST_LOG_WITHOUT_DEBUG_OUTPUT`\
`BOOST_LOG_WITHOUT_EVENT_LOG`\
`BOOST_LOG_WITHOUT_SYSLOG`

    b2 variant=debug link=static address-model=64 threading=multi define=BOOST_LOG_WITHOUT_DEBUG_OUTPUT

    b2 variant=debug link=static address-model=64 threading=multi define=BOOST_LOG_WITHOUT_DEBUG_OUTPUT define=BOOST_LOG_WITHOUT_EVENT_LOG

Boost::UTF configuration macros:

Note that this macro must be defined during boost and test module compilation.

`BOOST_TEST_DYN_LINK`

</div>

**Boost build output**

`The Boost C++ Libraries were successfully built!`
`The following directory should be added to compiler include paths:`
`    D:/projects/sphere/dev/extern/boost`
`The following directory should be added to linker library paths:`
`    D:\projects\sphere\dev\extern\boost\stage\lib`

* Add Directory

    In the test project configuration, add the directory where the boost unit test framework library is.

    `$(BoostDir)bin.v2\libs\test\build\msvc-12.0\debug\address-model-64\asynch-exceptions-on\link-static\threading-multi`

* Add Library File

    In the test project configuration, add a reference to the boost unit test framework library file.

    `libboost_unit_test_framework-vc120-mt-gd-1_55.lib`

Reference:

[Setup Boost test Framework][]

###MPFR Multi-Precision Library

* [MPFR] (http://www.mpfr.org/)
* [MPIR] (http://mpir.org/)
* [MPFR C++] (http://www.holoborodko.com/pavel/mpfr/)

MPFR requires MPFR (at least 3.1.2) which in turn uses MPIR (at least 2.7). MPFR and MPIR sources are in GitHub. Both projects require YASM. Download vsyasm from http://brgladman.org/oldsite/computing/gmp4win.php. Here is the direct link to the vsyasm file:  http://www.tortall.net/projects/yasm/releases/vsyasm-1.0.1-win64.zip.

#### Building MPIR
MPIR have a Visual Studio solution/project files for Visual Studio 2013. Build using sandybridge project for Corei7.

#### Building MPFR
MPFR have an external Visual Studio solution/project files for building. This one is taken from http://brgladman.org (http://brgladman.org/oldsite/computing/mpfr.svn9046.build.vc12.zip). Another requirement for building

NOTE: MPFR C++ is GPL.

###SQLite

[SQLite][]

**SQLite ODBC Driver**

The [SQLite ODBC Driver][] version is 0.998. The sources are hosted at
[GitHub](https://github.com/softace/sqliteodbc).

###Firebird

You can store the Firebird ODBC driver files (OdbcFb.dll, OdbcFb.chm) together
with your application, but the driver registration in system is required.

Command:

    regsvr32.exe /s {dir}\OdbcFb.dll

###Building ZeroMQ

*   Load the Visual Studio solution.
*   Build. (*Output library files are under the lib directory*)

###Semantic Versioning

[Semantic Version][] 2.0.0

* Allow version up to `99.99.99.99` integer value

<div class="indent">

    major     * 1,000,000 = 99,000,000
    minor     *    10,000 =    990,000
    revision  *       100 =      9,900
    internal  *         1 =         99
    max numeric value     = 99,999,999
</div>

* Provide functions for accessing and checking the version number:
    - equal
    - at least



</section>

{{sidebar.inc}}

{{footer.inc}}

[SQLite]: http://www.sqlite.org/
[SQLite ODBC Driver]: http://www.ch-werner.de/sqliteodbc/
[Setup Boost test Framework]: http://niuquant.wordpress.com/2013/11/16/setup-boost-1-55-0-unit-test-framework-with-visual-stuido-2010-on-windows-8-desktop/
[Semantic Version]: http://semver.org/spec/v2.0.0.html
