CSS: layout.css
CSS: sphere.css

{{header.inc}}

<section id="left-column">
The **SPHERE** project is an [Enterprise Resource Planning][] (ERP) solution
focused on [Small and Medium Enterprises][] (SMEs) and Cooperatives in the
Philippines. It aims to position SMEs towards a more pro-active and competitive
advantage through better and more efficient information processing and
management support.

Apache OFBiz is a foundation and starting point for reliable, secure and
scalable enterprise solutions. Use it out-of-the-box (OOTB) or customize to suit
even your most challenging business needs. With OFBiz in place, you can get
started right away and then grow your operations as your business grows, without
the huge deployment and maintenance costs of traditional enterprise automation
systems.

<div class="indent">

**Powerful**

**Flexible**

**Scalable**

**Modular**\
Acquire or use only the modules or components clients
actually need. Integration with other systems are possible
using APIs or through database integration.

**Database Agnostic**\
Connects to PostgreSQL, Sybase, Mircosoft SQL and Oracle
backends. Clients are given flexibility to choose their
desired database product or leverage on existing back-end
systems.

**Multi-platform**\
Front-end systems can run on different operating systems.
Initially targeted for Windows and Linux (Gnome). Allows
clients to use a proprietary operating system or an open
source alternative.
</div>

Components

<div class="indent">

**CRM**

**Point of Sale**

**Billing**

**Accounting**

**Warehouse Management**

**Supply Chain**

[odoo](https://www.odoo.com/apps)

</div>

The SPHERE project is currently in its development stage. Stay tune for
development status and information.

Download the [Brochure][]. You will need a PDF viewer like ![PDF_Image][]
[Adobe Reader][]
</section>

{{sidebar.inc}}

{{footer.inc}}

[Enterprise Resource Planning]: http://en.wikipedia.org/wiki/Enterprise_resource_planning
[Small and Medium Enterprises]: http://en.wikipedia.org/wiki/Small_and_medium_enterprises
[Brochure]: https://dl.dropboxusercontent.com/u/47611946/sphere/downloads/sphere-brochure-20130608.pdf
[PDF_Image]: http://www.adobe.com/images/pdficon_small.png
[Adobe Reader]: http://get.adobe.com/reader/
