CSS: layout.css
CSS: sphere.css

{{header.inc}}

<section id="left-column">
The **SPHERE** project is an [Enterprise Resource Planning][] (ERP) solution focused
on [Small and Medium Enterprises][] (SMEs) and Cooperatives in the Philippines.
It aims to position SMEs towards a more pro-active and competitive advantage
through better and more efficient information processing and management support.

<div class="indent">

**Accounting and Finance**<br/>
Core information management such as chart of accounts,
journals and ledgers, fixed asset, payables, receivables,
cash and internal control management, fixed asset
management, payroll.

**Inventory and Products**<br/>
Inventory control with multi-warehouse management like
back orders, refunds and spoilage. Integrates with sales,
purchasing and accounting system.

**Sales**<br/>
Customer information management, invoicing, order entry,
shipping. Integrates with inventory and accounting system.

**Purchasing**<br/>
Supplier information and purchase management, receiving,
Integrates with inventory and accounting system.

**Manufacturing**<br/>
Manufacturing orders, bill of materials. Integrates with
inventory and sales.

**Cooperative**<br/>
Philippine Cooperative Development Authority (CDA)
compliant information and reporting requirements.

</div>

The SPHERE project is currently in its development stage. Stay tune for
development status and information.

Download the [Brochure][]. You will need a PDF viewer like ![PDF_Image][]
[Adobe Reader][]
</section>

<section id="right-column">
**Modular**
Acquire or use only the modules or components clients
actually need. Integration with other systems are possible
using APIs or through database integration.

**Database Agnostic**
Connects to PostgreSQL, Sybase, Mircosoft SQL and Oracle
backends. Clients are given flexibility to choose their
desired database product or leverage on existing back-end
systems.

**Multi-platform**
Front-end systems can run on different operating systems.
Initially targeted for Windows and Linux (Gnome). Allows
clients to use a proprietary operating system or an open
source alternative.
</section>

{{footer.inc}}

[Enterprise Resource Planning]: http://en.wikipedia.org/wiki/Enterprise_resource_planning
[Small and Medium Enterprises]: http://en.wikipedia.org/wiki/Small_and_medium_enterprises
[Brochure]: https://dl.dropboxusercontent.com/u/47611946/sphere/downloads/sphere-brochure-20130608.pdf
[PDF_Image]: http://www.adobe.com/images/pdficon_small.png
[Adobe Reader]: http://get.adobe.com/reader/
