# Negerns project
# Makefile to build binary resource file(s)

all: "$(DEST_PATH)\negerns.bin"

"$(DEST_PATH)\negerns.bin" : ..\..\..\res\negerns\icons.xrc \
	..\..\..\res\negerns\blank.ico \
	..\..\..\res\negerns\negerns.ico \
	..\..\..\res\negerns\home.ico \
	..\..\..\res\negerns\new.ico \
	..\..\..\res\negerns\file-close.ico \
	..\..\..\res\negerns\file-close-all.ico \
	..\..\..\res\negerns\edit.ico \
	..\..\..\res\negerns\editundo.ico \
	..\..\..\res\negerns\editredo.ico \
	..\..\..\res\negerns\save.ico \
	..\..\..\res\negerns\refresh.ico \
	..\..\..\res\negerns\clear.ico \
	..\..\..\res\negerns\expand.ico \
	..\..\..\res\negerns\collapse.ico
	zip -9 -j $(*R).bin $**
