# AIS project
# Makefile to build binary resource file(s)

all: "$(DEST_PATH)\aisxrc.bin" "$(DEST_PATH)\aisimg.bin"

"$(DEST_PATH)\aisxrc.bin" : ..\..\res\ais\system.xrc \
    ..\..\res\ais\message.xrc \
    ..\..\res\ais\about.xrc \
    ..\..\res\ais\menu.xrc \
    ..\..\res\ais\employeedetails.xrc
    zip -9 -j $(*R).bin $**

"$(DEST_PATH)\aisimg.bin" : ..\..\res\ais\icon.xrc \
    ..\..\res\ais\ais16.ico \
    ..\..\res\ais\ais.ico \
    ..\..\res\ais\folder.ico \
    ..\..\res\ais\ais.png \
    ..\..\res\ais\new.ico \
    ..\..\res\ais\edit.ico \
    ..\..\res\ais\refresh.ico
    zip -9 -j $(*R).bin $**
